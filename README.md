EXPORT:
=> Export / Import / Cronjob
Aufru:
Datei Export: https://[account].[domain.de]/file/[account_id]/[platform_id]/ab12345_12x12.jpg (alt)
ToDo: Account_id über die Subdomain
#-Datei Export: https://[account].[domain.de]/file/[PLATFORM_ID]/[FILE_ID]/[FILENAME]=12x12.jpg (neu)
Datei Export: https://[account].[domain.de]/file/[PLATFORM_ID]/[FILE_ID]/[FILENAME].pdf (neu)

Feed Export: https://[account].[domain.de]/file/[PLATFORM_ID]/[FEED_ID]/[KEY]_[FILENAME].[TYPE]

PLATFORM_ID: Platform ID oder 0 wenn globaler aufruff.
FEED_ID: Feed ID
KEY: Frei definierbarer Export Schlüssel.
FILENAME: Frei definierbare Dateiname
TYPE: Datei Endungstype. bei einigen Type könnten Inhalte konvertiert werden. Z.b. bei pdf wird aus html erzeugt.
 - .io : für Import und Exports.
 - .csv : Datei wird als Download Datei angebotten
 - .txt : Datei wird als Download Datei angebotten
 - .pdf # wandert html in pdf Vormat um 
 - .xml, .html, .jpg, ... : Dateien werden u.a. dierekt im Browser angezeigt. 
 - .task für Cronjobs
  ! .php # php Endungen sind verboten, bzw. werden nicht ausgeführt
  
 ToDos:
  - .webp # Bild umwandlung in webp Format