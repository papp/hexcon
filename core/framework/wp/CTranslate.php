<?php
#https://github.com/Baby-Markt/deepl-php-lib/blob/master/src/DeepL.php
class CTranslate
{


	function __construct(&$D = null)
	{
		$this->API_URL_V1               = 'https://api.deepl.com/v1/translate';
		$this->API_URL_V2               = 'https://api.deepl.com/v2/translate';
		$this->API_URL_AUTH_KEY         = 'auth_key=%s';
		$this->API_URL_TEXT             = 'text=%s';
		$this->API_URL_SOURCE_LANG      = 'source_lang=%s';
		$this->API_URL_DESTINATION_LANG = 'target_lang=%s';
		$this->API_URL_TAG_HANDLING     = 'tag_handling=%s';
		$this->API_URL_IGNORE_TAGS      = 'ignore_tags=%s';
		
		#login api
		$this->API_URL = 'https://api.deepl.com/v2/translate';
		#$this->API_URL_AUTH_KEY = '123456';
		$this->apiVersion = 2;
		
		#$this->authKey	= 123456;
		$this->curl		= curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
	}

	function __destruct()
	{
		if ($this->curl && is_resource($this->curl)) {
			curl_close($this->curl);
		}
	}
	

	public function translate(
		$text,
		$sourceLanguage = 'de',
		$destinationLanguage = 'en',
		array $tagHandling = array(),
		array $ignoreTags = array()
	)
	{
		// make sure we only accept supported languages
		#$this->checkLanguages($sourceLanguage, $destinationLanguage);
		$url  = $this->buildUrl($sourceLanguage, $destinationLanguage, $tagHandling, $ignoreTags);
		// build the DeepL API request url
		$body = $this->buildBody($text);
		// request the DeepL API
		$translationsArray = $this->request($url, $body);
		$translationsCount = count($translationsArray['translations']);
		if ($translationsCount == 0) {
			echo('No translations found.');
		}
		else if ($translationsCount == 1) {
			return $translationsArray['translations'][0]['text'];
		}
		else {
			return $translationsArray['translations'];
		}
	}
	protected function buildUrl($sourceLanguage, $destinationLanguage, array $tagHandling = array(), array $ignoreTags = array())
	{
		// select correct api url
		switch ($this->apiVersion) {
			case 1:
				$url = $this->API_URL_V1;
				break;
			case 2:
				$url = $this->API_URL_V2;
				break;
			default:
				$url = $this->API_URL_V1;
		}
		$url .= '?' . sprintf($this->API_URL_AUTH_KEY, $this->authKey);
		$url .= '&' . sprintf($this->API_URL_SOURCE_LANG, strtolower($sourceLanguage));
		$url .= '&' . sprintf($this->API_URL_DESTINATION_LANG, strtolower($destinationLanguage));
		if (!empty($tagHandling)) {
			$url .= '&' . sprintf($this->API_URL_TAG_HANDLING, implode(',', $tagHandling));
		}
		if (!empty($ignoreTags)) {
			$url .= '&' . sprintf($this->API_URL_IGNORE_TAGS, implode(',', $ignoreTags));
		}
		return $url;
	}
	
	protected function buildBody($text)
	{
		$body  = '';
		$first = true;
		if (!is_array($text)) {
			$text = (array)$text;
		}
		foreach ($text as $textElement) {
			$body .= ($first ? '' : '&') . sprintf($this->API_URL_TEXT, rawurlencode($textElement));
			if ($first) {
				$first = false;
			}
		}
		return $body;
	}

	protected function request($url, $body)
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		$response = curl_exec($this->curl);
		if (!curl_errno($this->curl)) {
			$httpCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
			if ($httpCode != 200 && array_key_exists($httpCode, $this->errorCodes)) {
				echo($this->errorCodes[$httpCode]. $httpCode);
			}
		}
		else {
			echo('There was a cURL Request Error.');
		}
		$translationsArray = json_decode($response, true);
		if (!$translationsArray) {
			echo('The Response seems to not be valid JSON.');
		}
		return $translationsArray;
	}
}