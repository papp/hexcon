<?php
class CCache
{
	public function __construct($D=null)
    {
		$this->db_root = 'tmp/';
		$this->db_name = $D['DB'].'.cache.sqlite3';
		$this->db = new SQLite3($this->db_root.$this->db_name);
		
		#$this->db = new SQLite3(':memory:');
		##$this->db->exec('PRAGMA journal_mode = wal;');
		#$this->db->exec("PRAGMA journal_mode = MEMORY;");
		##$this->db->exec("PRAGMA cache_size = 10000;");
		##$this->db->exec("PRAGMA synchronous = OFF;");
		##$this->db->exec("PRAGMA foreign_keys = ON;");
		##$this->db->exec("PRAGMA temp_store = MEMORY;");
		##$this->db->exec("PRAGMA default_temp_store = MEMORY;");
	  ##$this->db->exec("PRAGMA read_uncommitted = true;");
		#$this->db->exec("PRAGMA locking_mode = EXCLUSIVE;");
		#$this->db->exec("PRAGMA secure_delete = ON;");
		#$this->db->exec("PRAGMA auto_vacuum = INCREMENTAL;");
		#$this->db->busyTimeout(5000);
		$this->db->exec('
			PRAGMA busy_timeout = 5000;
			PRAGMA cache_size = 10000;
			PRAGMA synchronous = OFF;
			PRAGMA foreign_keys = ON;
			PRAGMA temp_store = MEMORY;
			PRAGMA default_temp_store = MEMORY;
			PRAGMA read_uncommitted = true;
			PRAGMA journal_mode = wal;
		');

		$this->install();
	}
	
	/**
	 * @param string $data [id (max 64) => value,...]
	 * @return bool
	 */
	public function set($id,$data,$slot = 0, $expiration=86400)#24h
	{
		$data = serialize($data);
		$stmt = $this->db->prepare("REPLACE INTO cache (id, slot, value, lifetime) VALUES (:id, :slot, :value, DATETIME(CURRENT_TIMESTAMP, '+{$expiration} Second')  )");
		$stmt->bindParam(':id', $id, SQLITE3_TEXT);
		$stmt->bindParam(':slot', $slot, SQLITE3_TEXT);
		$stmt->bindParam(':value', $data, SQLITE3_BLOB);
		return $stmt->execute();
	}

	public function set_mulit($data,$slot = 0, $expiration=86400)#24h
	{
		foreach((array)$data AS $k => $v)
		{
			$this->set($k,$v,$slot,$expiration);
		}
		return $ret;
	}

	/**
	 * @param array|string $id [id,...]
	 * @param array|string &$data [id => value,...]
	 * @return bool|array|string
	 */
	public function get($id, &$data2=null, $utimestamp = null, $slot=0)
	{
		$IDs = (is_array($id))? implode("','",array_values((array)$id)) : $id;
		$W .= ($slot)? "AND slot = '{$slot}'":'';
		$W .= ($utimestamp)? "AND strftime('%s', itimestamp)*1 > ".strtotime($utimestamp):'';
		try {
			$this->db->enableExceptions(true);
			
			$result = $this->db->query("SELECT id, value, strftime('%s', itimestamp)*1 AS aa FROM cache WHERE id IN ('{$IDs}') AND CURRENT_TIMESTAMP < lifetime {$W}");
			while($ary = $result->fetchArray(SQLITE3_ASSOC))
			{# echo strtotime($utimestamp).' > '.$ary['aa']."|";
				$data2[$ary['id']] = $data[$ary['id']] = unserialize($ary['value']);
			}
		}
		catch (Exception $e) {
			$this->deinstall(); //Datenbank deinstallieren, damit spätter diese neu angelegt wird
		}
		
        return ($data)? ((is_array($id))? $data : $data[$id]):false;
	}
/*
	public function get($id, &$data = null)
	{
		$IDs = (is_array($id))? implode("','",array_values((array)$id)) : $id;
		$result = $this->db->query("SELECT id, value FROM cache WHERE id IN ('{$IDs}')");
		while($ary = $result->fetchArray(SQLITE3_ASSOC))
		{
			$data[$ary['id']] = unserialize($ary['value']);
		}
        return ($data)? ((is_array($id))? $data : $data[$id]):false;
	}
*/
	/**
	 * @param array|string $id [id,...]
	 * @return bool
	 */
	public function delete($id)
	{
		$IDs = (is_array($id))? implode("','",array_values((array)$id)) : $id;
		$this->db->exec("DELETE FROM cache WHERE id IN ('{$IDs}')");
		$this->optimizing();
		return true;
	}

	public function install()
	{
		if(!(filesize($this->db_root.$this->db_name) > 4096))
		{
			return $this->db->exec("CREATE TABLE IF NOT EXISTS cache (
										id VARCHAR(64) NOT NULL,
										slot VARCHAR(64) NOT NULL,
										value LONGBLOB NOT NULL,
										lifetime TIME NULL,
										itimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
										PRIMARY KEY (id, slot)
									);
									CREATE INDEX id ON cache (id);
									CREATE INDEX slot ON cache (slot);
									CREATE INDEX itimestamp ON cache (itimestamp);
									");
		}
		return false;
	}
	/*
	public function install()
	{
		if(!(filesize($this->db_root.$this->db_name) > 4096))
		{
			return $this->db->exec("CREATE TABLE IF NOT EXISTS cache (
									id VARCHAR(64) NOT NULL UNIQUE PRIMARY KEY,
									value LONGBLOB NOT NULL
									);");
		}
		return false;
	}
*/
/*NEW VERSION
public function get($id, $utimestamp = null)
	{
		$IDs = (is_array($id))? implode("','",array_values((array)$id)) : $id;
		$utimestamp = strtotime($utimestamp);
		$result = $this->db->query("SELECT id, value, strftime('%s', itimestamp)*1 AS aa FROM cache WHERE id IN ('{$IDs}') AND strftime('%s', itimestamp)*1 > {$utimestamp}");
		while($ary = $result->fetchArray(SQLITE3_ASSOC))
		{ #echo strtotime($utimestamp).' > '.$ary['aa']."|";
			$data[$ary['id']] = unserialize($ary['value']);
		}
        return ($data)? ((is_array($id))? $data : $data[$id]):false;
	}
public function install()
	{
		if(!(filesize($this->db_root.$this->db_name) > 4096))
		{
			return $this->db->exec("CREATE TABLE IF NOT EXISTS cache (
										id VARCHAR(64) NOT NULL UNIQUE PRIMARY KEY,
										value LONGBLOB NOT NULL,
										itimestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
										);
										CREATE INDEX idx_itimestamp ON cache (itimestamp);
									");
		}
		return false;
	}
*/


	public function deinstall()
	{
		return (file_exists($this->db_root.$this->db_name)) ? unlink($this->db_root.$this->db_name) : false;
	}

	public function optimizing()
	{
		#Defragmentiere die Datenbank
		$this->db->exec("PRAGMA auto_vacuum = FULL;");
		$this->db->exec("VACUUM;");
	}
}