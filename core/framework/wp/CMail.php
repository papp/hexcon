<?php
class CMail
{
	/*
	[
		'CONNECT'		=> [ 'METHOD' => 'SMTP|PHP', 'AUTH'=>1, 'HOST', 'USERNAME', 'PASSWORD' ],
		'FROM'			=> ['EMAIL', 'NAME'],
		'TO'			=> [ ['EMAIL'],['EMAIL'],... ],
		'BCC'			=> [ ['EMAIL'],['EMAIL'],... ],
		'CC'			=> [ ['EMAIL'],['EMAIL'],... ],
		'MAIL'			=> ['SUBJECT', 'BODY'],
		'ATTACHMENT'	=> [ ['PATH'], ['PATH'],... ],
	]
	return true/false
	*/
	function send($D)
	{
		require('core/framework/phpmailer/PHPMailerAutoload.php');
		//Instanz von PHPMailer bilden
		$mail = new PHPMailer();

		if($D['CONNECT']['METHOD'] == 'SMTP')
		{
			$mail->IsSMTP(); //Versand �ber SMTP festlegen
			$mail->Host = $D['CONNECT']['HOST']; //SMTP-Server setzen
			$mail->SMTPAuth = ($D['CONNECT']['AUTH'])?true:false;     //Authentifizierung aktivieren
			$mail->Username = $D['CONNECT']['USERNAME'];  // SMTP Benutzername
			$mail->Password = $D['CONNECT']['PASSWORD']; // SMTP Passwort 
			$mail->CharSet  = "utf-8"; //UTF-8 Kodierung festlegen 
		}

		
		$mail->From = $D['FROM']['EMAIL'];//Absenderadresse der Email setzen
		$mail->FromName = $D['FROM']['NAME'];//Name des Abenders setzen
		
		$mail->AddAddress($D['TO'][0]['EMAIL']);//Empf�ngeradresse setzen

		if($D['BCC'][0]['EMAIL'])
			$mail->AddBCC($D['BCC'][0]['EMAIL']);//Empf�nger einer Kopie setzen
		if($D['CC'][0]['EMAIL'])
			$mail->AddCC($D['CC'][0]['EMAIL']);//Empf�nger einer Blindkopie setzen
		
		$mail->Subject = $D['MAIL']['SUBJECT'];//Betreff der Email setzen
		$mail->Body = $D['MAIL']['BODY'];//Text der EMail setzen

		foreach((array)$D['ATTACHMENT'] AS $k => $v)
		{
			$dom = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
			$main_url = str_replace(basename($dom),'',$dom);
			if(get_headers($main_url.$v['PATH']))#Dint dazu damit die datei im tmp ordner erstellt wird
			{
				file_put_contents("test/invoice.pdf", fopen($v['PATH'], 'r'));
				$mail->addAttachment("test/invoice.pdf");
			}
		}
		/*
		//EMail senden und �berpr�fen ob sie versandt wurde
		if(!$mail->Send())
		{
		 //$mail->Send() liefert FALSE zur�ck: Es ist ein Fehler aufgetreten
		 echo "Die Email konnte nicht gesendet werden";
		 echo "Fehler: " . $mail->ErrorInfo; #ToDo: Log
		}
		else
		{
		 //$mail->Send() liefert TRUE zur�ck: Die Email ist unterwegs
		 echo "Die Email wurde versandt.";
		}
		*/
		
		return $mail->Send();
	}
	
	/*
	[
		'CONNECT'		=> ['METHOD' => 'IMAP', 'SERVER', 'PORT', 'USERNAME', 'PASSWORD'],
	]
	*/
	function receive($D)
	{
		$imap = imap_open( "{" . $D['CONNECT']['SERVER'] . ":" . $D['CONNECT']['PORT'] . "}INBOX", $D['CONNECT']['USERNAME'], $D['CONNECT']['PASSWORD'] );
		if($imap)
		{
			#$a =  imap_sort($imap,SORTARRIVAL,false,SE_UID);
			#print_r($a);
			$totalrows = imap_num_msg($imap);

			
			for($index = (($totalrows>10)?$totalrows-10:0); $index < $totalrows; $index++)
			{
				
				$header = imap_header($imap, $index + 1);
				#echo "<pre>";print_R($header);exit;
				$prettydate = date('YmdHis' , $header->udate);
				
				if($prettydate >= $D['DATETIME'])
				{
					$email = "{$header->from[0]->mailbox}@{$header->from[0]->host}";
					$mailID = md5($header->udate.$email);
					
					#$A = imap_fetchstructure($imap,$index+1);
					#$B = imap_fetchbody($imap,$index+1,1);
					#$B = imap_qprint($B);
					#echo $B;
					#echo "<pre>";
					#print_R($A);
					#imap_body($imap,$index+1);exit;
					#http://stackoverflow.com/questions/15539902/php-imap-decoding-messages
					$structure = imap_fetchstructure($imap, $index+1);
						
						$parts = count($structure->parts);
						$TEXT = imap_fetchbody($imap,$index+1,($parts>0)?$parts:1 );#?"2".imap_fetchbody($imap,$index+1,2):"1".imap_fetchbody($imap,$index+1,1);
						$SUBJECT = $header->subject;
						$SUBJECT = imap_utf8($SUBJECT);
						if(isset($structure->parts) && is_array($structure->parts) ) 
						{
							$part = $structure->parts[$parts];
						}
						else
							$part = $structure;
						
							switch($part->encoding)
							{
								#case 0:
								#	break;
								case 1:
									$TEXT = imap_8bit($TEXT);
									break;
								case 3:
									$TEXT = imap_base64($TEXT);
									break;
								default:
									$TEXT = imap_qprint($TEXT);
									break;
							}
						#$TEXT = imap_qprint($TEXT);
						
						if($structure->parts[$parts-1]->subtype == 'PLAIN')
							$TEXT = '<plaintext>'.$TEXT;

						#if(strpos($email, '@members.ebay.de'))
							$D['EMAIL']['D'][ $email ][ $mailID ] = [
								'FROM'		=> $email,
								'SUBJECT'	=> $SUBJECT,
								'BODY'		=> $TEXT,
								#'TYPE'		=> $part->subtype,
								'DATETIME'	=> $prettydate,
							];
							/*
							$D['EMAIL']['D'][ $index+1 ] = [
								'FROM'		=> $email,
								'SUBJECT'	=> $SUBJECT,
								'BODY'		=> $TEXT,	#HTML
								'PLAIN'		=> $PLAIN,	#TEXT
								'DATETIME'	=> $prettydate,
								'UDATE'	=> $header->udate,
							];
							*/
						#echo "<pre>";print_r($structure);
						#echo $TEXT.'|'.$part->encoding;
						#exit;

				}
			}
			imap_close($imap);
		}
		else
			echo("Can't connect: " . imap_last_error());#ToDo: Log
		return $D;
	}
}