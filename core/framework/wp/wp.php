<?php
#Version 1.0.0.2
class CWP
{
	/**
	W�hlt zufallswerte defnierte Inhalte im Text, Difiniert kann im Text wie folgt [A|B|C]
	*/
	/*
	static function rand_choice_str($T,$D=null)
	{
		$D['DELIMITER'] = (!$D['DELIMITER'])?array('LEFT' => '{', 'RIGHT' => '}'):$D['DELIMITER'];

		$sS = CWP::stripos($T,'<script>');
		$sE = CWP::stripos($T,'</script>');

		$S = CWP::stripos($T,$D['DELIMITER']['LEFT']);
		$E = CWP::stripos($T,$D['DELIMITER']['RIGHT']);
		
		$lenL = strlen($D['DELIMITER']['LEFT']);
		$lenR = strlen($D['DELIMITER']['RIGHT']);
		if(count($S) == count($E))
		{
			for($i=count($S)-1;$i >= 0;$i--)
			{
				###Pr�fe ob es im verbotenen bereich sich befindet
				$ok=1;
				for($j=0;$j < count($sS);$j++)
				{
					if($S[$i]>$sS[$j] && $S[$i]<$sE[$j])
					{
						$ok=0;
						break;
					}
				}
				
				if($ok)
				{
					$SubT = substr($T,$S[$i]+$lenL,$E[$i]-$S[$i]-$lenL);
					$Wort = explode('|',$SubT);
					$POS = mt_rand(0,count($Wort)-1);
					$T = substr_replace($T,$Wort[$POS],$S[$i],$E[$i]-$S[$i]+$lenR);
				}
			}
		}
		return $T;
	}
	*/

	/** 
	 * Eindeutige ID
	*/
	static function uuid() {
		$s = uniqid();
		$num = hexdec(str_replace(".","",(string)$s));
		$index = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$base = strlen($index);
		$out = '';
			for($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
				$a = floor($num / pow($base,$t));
				$out = $out.substr($index,$a,1);
				$num = $num-($a*pow($base,$t));
			}
		return $out;
	}

	static function rand_choice_str($T,$D=null)
	{
		return preg_replace_callback('/\[\((((?>[^\[\(\)\]]+)|(?R))*)\)\]/x',
			function ($T)
			{
				$CLASS = get_class();
				$T = $CLASS::rand_choice_str($T[1],$D);
				$parts = explode('|', $T);
				return $parts[array_rand($parts)];
			},$T
		);
	}

	
	/**
	Gibt alle Positionen eines Strings in einem String als Array
	*/
	static function stripos($T,$Was)
	{
		$anz = substr_count($T,$Was,0);
		for($i=0;$i < $anz;$i++)
		{
			$P[$i] = stripos($T,$Was,(($i>0)?$P[$i-1]+1:0));
		}
		return $P;
	}
 

	static function base_url() {
		$path_parts = pathinfo($_SERVER['REQUEST_URI']);
		return "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$path_parts['dirname']}";
	}

	static function read($URL)
	{
		$CFile = new CFile();
		return $CFile::read($URL);
	}
	
	static function URL($URL,$D=null)
	{
		$CFile = new CFile();
		
		return $CFile::URL($URL,$D);
	}
	
	/** aus Array in ein URL Parameter*/	
	static function http_build_query($array)
	{
		return http_build_query($array);
	}
	
	static function concat()
	{
		return implode(func_get_args());
	}
	
	static function str_replace($seacht,$replace,$subject)
	{
		return str_replace($seacht,$replace,$subject);
	}
	
	#veraltet, botte uuid() nutzen
	static function uniqid()
	{
		return uniqid();
	}
	
	
	#Gibt ein Skala aus
	static function scale($D)
	{
		$start=date("Y-m-d",$D['START']);
		$end=date("Y-m-d",$D['END']);

		while($start<=$end) { //Solange $start < $end
			$x = str_replace('-','',$start);
			$D['SCALE'][] = array(
				'Y' => substr($x,0,4),
				'M' => substr($x,4,2),
				'D' => substr($x,6,2),
				);
			$start = date("Y-m-d",strtotime($start."+1 day")); //Z�hle $start um einen Tag hoch
		}
		#echo count($D['SCALE']).'XXX<pre>';
		#print_r($D['SCALE']);
			return $D;
	}

	/*$P Parameter array �bersetzung; $W Array mit dem Filter*/
	static function sql_get_where($P,$W)
	{
		$K = array_keys($P);
		$V = array_values($P);
		
		$wK = array_keys((array)$W);
		$wW = '';
		for($x=0;$x<count($wK);$x++)
		{
			if(strpos($wK[$x],'|') !== false ) #| = OR
			{
				$p = explode('|',$wK[0]);
				$wW .= " AND ( 1=0";
				for($w=0;$w<count($p);$w++)
				{
					$oK = explode(':',$p[$w]);
					if(in_array($oK[0],$K))
					{
						$wW .=  " OR {$oK[0]} {$oK[1]} '".$W[$wK[0]]."'";
					}	
				}
				$wW .= ")";
			}
		}

		$wW = str_replace($K,$V,$wW);
		$wW = str_replace(array('*'),array('%'),$wW);
		return $wW;
	}
	
	static function sql_get_where2($P,$W)
	{
		$K = array_keys($P);
		$V = array_values($P);
		
		$wK = array_keys((array)$W);
		$wW = '';
		for($x=0;$x<count($wK);$x++)
		{
			if(strpos($wK[$x],'|') !== false ) #| = OR
			{
				$p = explode('|',$wK[0]);
				$wW .= " AND ( 1=0";
				for($w=0;$w<count($p);$w++)
				{
					$oK = explode(':',$p[$w]);
					if(in_array($oK[0],$K))
					{
						#$wW .=  " OR {$oK[0]} {$oK[1]} '".$W[$wK[0]]."'";
						#$wW .= " OR ".str_replace(["[1]","[V]"],[ $oK[1], $W[$wK[0]] ],$oK[0]);
						$wW .= " OR ".str_replace(["[1]","[V]"],[ $oK[1], $W[$wK[0]] ],$P[$oK[0]]);
					}	
				}
				$wW .= ")";
			}
		}

		$wW = str_replace($K,$V,$wW);
		$wW = str_replace(array('*'),array('%'),$wW);
		return $wW;
	}
	
	/**
	#$P = [MAP => 'ing', 'ABC' => 'feld_abc', 'XXX' => "(SELECT ggg WHERE d = '[VALUE]')"]
	#$W = [FELD:LIKE|FELD2:IN|...] = "VALUE"*/
	static function where_interpreter($P,$W)
	{
		$K = array_keys($P);
		$V = array_values($P);
		
		foreach((array)$W as $kW => $vW ) #AND
		{
			if(strpos($kW,'|') !== false ) #| = OR
			{
				$p = explode('|',$kW);
				$wW .= " AND (";
				for($w=0;$w<count($p);$w++)
				{
					$vW = str_replace(['*'],["%"],$vW);
					$wW .= (($w>0)?' OR ':' '). str_replace(["[{$p[$w]}]"],[$vW],$P[$p[$w]]); #Suchbegriff muß mit [] umklamert werden
					#$wW .= (($w>0)?' OR ':' '). str_replace([$p[$w]],[$vW],$P[$p[$w]]);#Alt aber kompatible
				}
				$wW .= ")";
			}
			else
			{
				$vW = str_replace(['*'],["%"],$vW);
				$wW .= ' AND '. str_replace(["[{$kW}]"],[$vW],$P[$kW]); #Suchbegriff muß mit [] umklamert werden
				#$wW .= ' AND '. str_replace([$kW],[$vW],$P[$kW]);#Alt aber kompatible
			}
		}
		return $wW;
	}

	/**
	 * Vergleicht zwei Multidimisionale arrays und gibt Differenz vom ersten array aus
	 */
	static function array_diff_assoc_recursive($array1, $array2)
	{
		$difference=array();
		foreach((array)$array1 as $key => $value) {
			if( is_array($value) ) {
				if( !isset($array2[$key]) || !is_array($array2[$key]) ) {
					$difference[$key] = $value;
				} else {
					$new_diff = array_diff_assoc_recursive($value, $array2[$key]);
					if( !empty($new_diff) )
						$difference[$key] = $new_diff;
				}
			} else if( !array_key_exists($key,$array2) || $array2[$key] !== $value ) {
				$difference[$key] = $value;
			}
		}
		return $difference;
	}
	
	static function getBrowserLang() {
		$lang_variable = explode(',',explode(';',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0]);
		return strtoupper($lang_variable[1]);
	}

	/**
	 * BaseCurrencyCode = EUR
	 * CurrencyCode = andere Währung; EUR = 1; Wenn = Null; dann return array
	 * Wenn $Currency nicht übergeben wird, wird eine Array übergeben
	 * return float|array
	 * ToDo: Muss Umrechnung hinzugefügt werden wenn BaseCurrency != EUR ist!!
	 */
	static function CurrencyCode($BaseCurrency, $Currency = null) {
		if($Currency) {
			$rate = 1;
		}
		else {
			$rate['EUR'] = 1;
			$rate[NULL] = 1;
		}
		if($BaseCurrency != $Currency) {
			$sXMLRates = file_get_contents( 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml' );
			libxml_use_internal_errors(1);
			$aCurrency = json_decode(json_encode(simplexml_load_string($sXMLRates, NULL, LIBXML_NOCDATA)),1);
			
			foreach((array) $aCurrency['Cube']['Cube']['Cube'] AS $kC => $C) {
				if($Currency) {
					if($aCurrency['Cube']['Cube']['Cube'][$kC]['@attributes']['currency'] == $Currency) {
						$rate = $aCurrency['Cube']['Cube']['Cube'][$kC]['@attributes']['rate'];
					}
				}
				else {
					$rate[$aCurrency['Cube']['Cube']['Cube'][$kC]['@attributes']['currency']] = $aCurrency['Cube']['Cube']['Cube'][$kC]['@attributes']['rate'];
				}
			}
		}
		return $rate;
	}
	
}