<?php
/**
* Zusätzliche Funktionen für das Template
*/
class CSExt {
	static function uuid() {
		$CWP = new CWP();
		return $CWP::uuid();
	}
	static function concat() {
		return implode(func_get_args());
	}
	
	static function url($URL, $D=null) {
		$CFile = new CFile();
		return $CFile::url($URL,$D);
	}
}