<?php
/**
* Class platform ist eine Globale Platform Klasse
*/
#ToDo: cache entfernen, account hinzufügen!
class platform #extends CData #extends cache
{
	public $ACCOUNT,$SQL,$SQL_Data,$Platform_ID;

	function __construct($account_id, $platform_id)
	{
		global $ACCOUNT,$SQL, $SQL_Data;
		$this->ACCOUNT = $ACCOUNT;
		$this->SQL = $SQL;
		$this->SQL_Data = $SQL_Data;
		$this->Platform_ID = $platform_id;

		#ToDo: Pattern besser unterbringen
		$D['PATTERN']['PLATFORM'] = [
			'Title'			=> ['Type' => 'text', 'Length' => 200],
			'Active'		=> ['Type' => 'checkbox'],
			'ParentId'		=> ['Type' => 'text'],
			'Sort'			=> ['Type' => 'number'],
			'ClassId'		=> ['Type' => 'select', 'Option' => [] ], #ToDo: Nur beim Erstellen wählbar, danach nicht!
		];
		
		$D['PATTERN']['PLATFORM']['D']['SUPPLIER'] = [
			'Active'		=> ['Type' => 'checkbox'],
			'Title'			=> ['Type' => 'text', 'Length' => 200],
			'Company'		=> ['Type' => 'text', 'Length' => 200],
			'Comment'		=> ['Type' => 'textarea'],
			'CurrencyId'	=> ['Type' => 'select', 'Option'=> ['USD','PLN','EUR'] ],
			'CountryId'		=> ['Type' => 'select', 'Option'=> ['DE|Deutshcland','CN|China'] ],# 'Option' => &$D['SETTING']['D']['AvailableContentLanguage']['VALUE'],
		];
		
		$D['PATTERN']['PLATFORM']['D']['ARTICLE'] = [
			'Active'		=> [/*'Row'=> 0, 'Slot' => 0,*/ 'Type' => 'checkbox' ],
			'ParentId'		=> ['Type' => 'id', 'ForeignKey' => 1],
		];
		
		$D['PATTERN']['PLATFORM']['D']['SUPPLIER']['D']['ARTICLE'] = [
			'ArticleId'		=> ['Type' => 'id', 'ForeignKey' => 1 ],
			'Active'		=> ['Type' => 'checkbox',],
			'Number'		=> ['Type' => 'text', 'Length' => 100],
			'Title'			=> ['Type' => 'text', 'Length' => 200],
			'Price'			=> ['Type' => 'number'],
			'Comment'		=> ['type' => 'textarea'],
		];
		#ToDo: für CData Erweiterung: ForegnKey darf true/false, 1/0 oder Array sein. Bei array wird ein Pfad zum PrimeryKey angegeben. Dadurch darf nur ein Wert bereits vorhandenes Wert angegeben werden.
		#$D['PATTERN']['PLATFORM']['D']['SUPPLIER']['D']['ARTICLE']['ArticleId']['ForeignKey']['PLATFORM']['ARTICLE'] = null;



		$D['PATTERN']['PLATFORM']['D']['ATTRIBUTE'] = [
			'Active'		=> ['Type' => 'checkbox'],
			'ParentId'		=> ['Type' => 'id', 'ForeignKey' => 1],
			'Sort'			=> ['Type' => 'number'],
			'Type'			=> ['Type' => 'text'], #text, textarea,file,...
			'I18n'			=> ['Type' => 'checkbox'],
		];
		$D['PATTERN']['ATTRIBUTE']['D']['LANGUAGE'] = [
			'Active'		=> ['Type' => 'checkbox'],
			'Title'			=> ['Type' => 'text'],
		];

		$D['PATTERN']['PLATFORM']['D']['SUPPLIER']['D']['ORDER'] = [
			'Active'		=> ['Type' => 'checkbox',],
			'Status'		=> ['Type' => 'select', 'Option' => ['1|Open', '2|in Work', '3|bay it', '4|closed']],
			'Number'		=> ['Type' => 'text', 'Length' => 100],
			'Comment'		=> ['type' => 'textarea'],
		];
		
		$D['PATTERN']['PLATFORM']['D']['SUPPLIER']['D']['ORDER']['D']['ARTICLE'] = [
			'ArticleId'			=> ['Type' => 'id', 'ForeignKey' => 1], #Suplier-artikel-Id
			'Active'			=> ['Type' => 'checkbox',],
			'Stock'				=> ['Type' => 'number'],
			'Comment'			=> ['type' => 'textarea'],
		];
		
		
		$D['PATTERN']['PLATFORM']['D']['WAREHOUSE'] = [
			#'PLATFORM'		=> ['Type' => 'id'],
			'Active'		=> ['Type' => 'checkbox' ],
			'Title'			=> ['Type' => 'text', 'Length' => 200],
		];
		
		
		$D['PATTERN']['PLATFORM']['D']['WAREHOUSE']['D']['STORAGE'] = [	
			#'WAREHOUSE'		=> ['Type' => 'id'],
			'Active'		=> [/*'Row' => 0, 'Slot' => 0,*/ 'Type' => 'checkbox'],
			'Title'			=> ['Type' => 'text', 'Length' => 200],
		];
		
		
		$D['PATTERN']['PLATFORM']['D']['WAREHOUSE']['D']['STORAGE']['D']['ARTICLE'] = [
			'ArticleId'		=> ['Type' => 'id'],
			'Active'		=> ['Type' => 'checkbox'],
			'Stock'			=> ['Type' => 'number'],
		];
		
		$D['PATTERN']['PLATFORM']['D']['INVOICE'] = [
			#'PLATFORM'				=> ['Type' => 'id'],
			'OrderId'				=> ['Type' => 'id'],
			'PaymentId'				=> ['Type' => 'id'],
			'Active'				=> [/*'Row' => 0, 'Slot' => 0,*/ 'Type' => 'checkbox' ],
			'Status'				=> [/*'Row' => 0, 'Slot' => 1,*/ 'Type' => 'number', 'Length' => 200],
			'Date'					=> [/*'Row' => 0, 'Slot' => 2,*/ 'Type' => 'date'],
			'DatePaid'				=> [/*'Row' => 0, 'Slot' => 3,*/ 'Type' => 'date'],
			'Currencycode'			=> [/*'Row' => 0, 'Slot' => 4,*/ 'Type' => 'select', 'Option'=> ['EUR','USD','JPY']],#Soll eine Referenz zur CURRENCYCODE herstellen
			'Conversionrate'		=> [/*'Row' => 0, 'Slot' => 5,*/ 'Type' => 'number'],
			'Number'				=> [/*'Row' => 0, 'Slot' => 6,*/ 'Type' => 'number'],
			'Nickname'				=> [/*'Row' => 0, 'Slot' => 7,*/ 'Type' => 'text', 'Length' => 200],
			'Email'					=> [/*'Row' => 0, 'Slot' => 8,*/ 'Type' => 'text', 'Length' => 200],
			'BillingCompany'		=> [/*'Row' => 0, 'Slot' => 9,*/ 'Type' => 'text', 'Length' => 200],
			'BillingVatId'			=> [/*'Row' => 1, 'Slot' => 0,*/ 'Type' => 'text', 'Length' => 200],
			'BillingFName'			=> [/*'Row' => 1, 'Slot' => 1,*/ 'Type' => 'text', 'Length' => 200],
			'BillingName'			=> [/*'Row' => 1, 'Slot' => 2,*/ 'Type' => 'text', 'Length' => 200],
			'BillingStreet'			=> [/*'Row' => 1, 'Slot' => 3,*/ 'Type' => 'text', 'Length' => 200],
			'BillingStreetNo'		=> [/*'Row' => 1, 'Slot' => 4,*/ 'Type' => 'text', 'Length' => 200],
			'BillingZip'			=> [/*'Row' => 1, 'Slot' => 5,*/ 'Type' => 'text', 'Length' => 200],
			'BillingCity'			=> [/*'Row' => 1, 'Slot' => 6,*/ 'Type' => 'text', 'Length' => 200],
			'BillingAddition'		=> [/*'Row' => 1, 'Slot' => 7,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryCompany'		=> [/*'Row' => 1, 'Slot' => 8,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryFName'			=> [/*'Row' => 1, 'Slot' => 9,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryName'			=> [/*'Row' => 2, 'Slot' => 0,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryStreet'		=> [/*'Row' => 2, 'Slot' => 1,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryStreetNo'		=> [/*'Row' => 2, 'Slot' => 2,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryZip'			=> [/*'Row' => 2, 'Slot' => 3,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryCity'			=> [/*'Row' => 2, 'Slot' => 4,*/ 'Type' => 'text', 'Length' => 200],
			'DeliveryAddition'		=> [/*'Row' => 2, 'Slot' => 5,*/ 'Type' => 'text', 'Length' => 200],
			'Comment'				=> [/*'Row' => 2, 'Slot' => 6,*/ 'Type' => 'text', 'Length' => 200],
		];
		
		$D['PATTERN']['PLATFORM']['D']['INVOICE']['D']['POSITION'] = [
			'Active'				=> [/*'Row' => 0, 'Slot' => 1,*/ 'Type' => 'checkbox'],
			'Number'				=> [/*'Row' => 0, 'Slot' => 2,*/ 'Type' => 'number'],
			'Title'					=> [/*'Row' => 0, 'Slot' => 3,*/ 'Type' => 'text', 'Length' => 200],
			'Stock'					=> [/*'Row' => 0, 'Slot' => 4,*/ 'Type' => 'number'],
			'Weight'				=> [/*'Row' => 0, 'Slot' => 5,*/ 'Type' => 'number'],
			'Price'					=> [/*'Row' => 0, 'Slot' => 6,*/ 'Type' => 'number'],
			'Vat'					=> [/*'Row' => 0, 'Slot' => 7,*/ 'Type' => 'number'],
		];
		##parent::__construct($SQL, ['PATTERN' => &$D['PATTERN']]);
		$this->SQL_Data->set_Pattern($D);
	}

	function get_object(&$D, $F) {
		$F['PLATFORM']['W'][0]['ID'] = $this->Platform_ID;
		
		$this->SQL_Data->get_object($D, $F);

	}
	function set_object(&$D) {
		$this->SQL_Data->set_object($D);
	}
	function get_Pattern(&$D) {
		$this->SQL_Data->get_Pattern($D);
	}
	function get_warehouse(&$D=null) {

	}
	function get_storage(&$D=null) {

	}

	#Export Import
	function get_feed(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID:IN'		=> " id IN ('[ID:IN]')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['FEED']['W']);
		$qry = $this->SQL->query("SELECT id ID, active ACTIVE, name NAME, text TEXT, comment COMMENT, filename FILENAME, filetype FILETYPE, key KEY, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_feed
							WHERE platform_id = '{$this->platform_id}' {$W} ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['FEED']['D'][ $a['ID'] ] = $a;
		}
	}
	
	function set_feed(&$D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['FEED']['D'] AS $kFEED => $FEED) {
			if($FEED['ACTIVE'] != -2) {
				$IU_FEED .= (($IU_FEED)?',':'')."('{$kFEED}','{$this->platform_id}'";
				$IU_FEED .= (isset($FEED['ACTIVE']))? ",'{$FEED['ACTIVE']}'":",NULL";
				$IU_FEED .= (isset($FEED['NAME']))? ",'".$this->SQL->escapeString($FEED['NAME'])."'":",NULL";
				$IU_FEED .= (isset($FEED['TEXT']))? ",'".$this->SQL->escapeString($FEED['TEXT'])."'":",NULL";
				$IU_FEED .= (isset($FEED['COMMENT']))? ",'{$FEED['COMMENT']}'":",NULL";
				$IU_FEED .= (isset($FEED['FILENAME']))? ",'".$this->SQL->escapeString($FEED['FILENAME'])."'":",NULL";
				$IU_FEED .= (isset($FEED['FILETYPE']))? ",'{$FEED['FILETYPE']}'":",NULL";
				$IU_FEED .= (isset($FEED['KEY']))? ",'{$FEED['KEY']}'":",NULL";
				$IU_FEED .= ")";
			}
			else {
				$D_FEED .= (($D_FEED)?',':'')."'{$kFEED}'";
			}
		}
		if($IU_FEED) {
			$this->SQL->query("INSERT INTO wp_feed (id, platform_id, active, name, text, comment, filename, filetype, key) VALUES {$IU_FEED} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							
							active =		CASE WHEN excluded.active IS NOT NULL		AND ifnull(active,'')  <> excluded.active			THEN excluded.active ELSE active END,
							name =			CASE WHEN excluded.name IS NOT NULL			AND ifnull(name ,'') <> excluded.name				THEN excluded.name ELSE name END,
							text =			CASE WHEN excluded.text IS NOT NULL			AND ifnull(text,'') <> excluded.text				THEN excluded.text ELSE text END,
							comment =		CASE WHEN excluded.comment IS NOT NULL		AND ifnull(comment,'') <> excluded.comment			THEN excluded.comment ELSE comment END,
							filename =		CASE WHEN excluded.filename IS NOT NULL		AND ifnull(filename,'') <> excluded.filename		THEN excluded.filename ELSE filename END,
							filetype =		CASE WHEN excluded.filetype IS NOT NULL		AND ifnull(filetype,'') <> excluded.filetype		THEN excluded.filetype ELSE filetype END,
							key =			CASE WHEN excluded.key IS NOT NULL			AND ifnull(key,'') <> excluded.key					THEN excluded.key ELSE key END,
							utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
													OR excluded.name IS NOT NULL		AND ifnull(name,'') <> excluded.name
													OR excluded.text IS NOT NULL		AND ifnull(text,'') <> excluded.text
													OR excluded.comment IS NOT NULL		AND ifnull(comment,'') <> excluded.comment
													OR excluded.filename IS NOT NULL	AND ifnull(filename,'') <> excluded.filename
													OR excluded.filetype IS NOT NULL	AND ifnull(filetype,'') <> excluded.filetype
													OR excluded.key IS NOT NULL			AND ifnull(key,'') <> excluded.key
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($D_FEED) {
			$this->SQL->query("DELETE FROM wp_feed WHERE id IN ({$D_FEED}) AND platform_id = '{$this->platform_id}'");
		}

	}
	
		function get_file(&$D=null) {
		$W = (isset($D['PLATFORM']['D'][ $this->platform_id ]['FILE']['W']['ID']))?" AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['FILE']['W']['ID']}')":'';
		
		$qry = $this->SQL->query("SELECT id, active, strftime('%Y%m',itimestamp) date, name, size, itimestamp*1 AS itimestamp, utimestamp*1 AS utimestamp
							FROM wp_file
							WHERE platform_id = '{$this->platform_id}' {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$ext = pathinfo($a['name'], PATHINFO_EXTENSION);
			$D['PLATFORM']['D'][ $this->platform_id ]['FILE']['D'][ $a['id'] ] = [
				'ACTIVE'		=> $a['active'],
				'SIZE'			=> $a['size'],
				'NAME'			=> $a['name'],
				'EXTENDSION'	=> $ext,
				'URL'			=> "{$a['date']}/{$a['name']}",
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
			];
		}
		#return $D;
	}
	
	function set_file(&$D) {
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['FILE']['D'] AS $kFIL => $FIL) {
			if($FIL['ACTIVE'] != -2) {
				$IU_FIL = ((isset($IU_FIL))?',':'')."('{$kFIL}','{$this->platform_id}'";
				$IU_FIL .= (isset($FIL['ACTIVE']))? ",'{$FIL['ACTIVE']}'":",NULL";
				$IU_FIL .= (isset($FIL['NAME']))? ",'{$FIL['NAME']}'":",NULL";
				$IU_FIL .= (isset($FIL['SIZE']))? ",'{$FIL['SIZE']}'":",NULL";
				$IU_FIL .= ")";
			}
			else {
				$D_FIL = ((isset($D_FIL))?',':'')."'{$kFIL}'";
			}
		}
		
		if(isset($IU_FIL)) {
			$this->SQL->query("INSERT INTO wp_file (id, platform_id, active, name, size) VALUES {$IU_FIL} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active	THEN excluded.active ELSE active END,
							name =			CASE WHEN excluded.name IS NOT NULL			AND name <> excluded.name		THEN excluded.name ELSE name END,
							size =			CASE WHEN excluded.size IS NOT NULL			AND size <> excluded.size		THEN excluded.size ELSE size END,
							utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL			AND active <> excluded.active
													OR excluded.name IS NOT NULL		AND name <> excluded.name
													OR excluded.size IS NOT NULL		AND size <> excluded.size
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if(isset($D_FIL)) {
			$this->SQL->query("DELETE FROM wp_file WHERE id IN ({$D_FIL}) AND platform_id = '{$this->platform_id}'");
		}
	}
	
	function get_task(&$D=null) {
		#$d['TASK'] = $D['PLATFORM']['D'][ $this->platform_id ]['TASK'];
		#account::get_task(&$d);
		#$D['PLATFORM']['D'][ $this->platform_id ]['TASK'] = $d['TASK'];
		
		$W .= CWP::where_interpreter([
			'ID'				=> " id IN ('[ID]')",
			'PLATFORM_ID:IN'	=> " platfomr_id IN ('[PLATFORM_ID:IN]')",
			'FEED_ID:IN'		=> " feed_id IN ('[FEED_ID:IN]')",
			'ACTIVE'			=> " active IN ('[ACTIVE]')",
			'FAIL:IN'			=> " fail IN ('[FAIL:IN]')",
			#'FAIL:<'			=> " IIF(fail,fail,0) < [FAIL:<]", #compatible mit sqlite 3.32
			'FAIL:<'			=> " CASE WHEN fail THEN fail ELSE 0 END < [FAIL:<]", #compatible mit sqlite 3.27
			'START_TIME:<='		=> " start_time <= '[START_TIME:<=]'",
			'START_TIME:>'		=> " start_time > '[START_TIME:>]'",
			'UTIMESTAMP:<'		=> " DATETIME(utimestamp, 'localtime') < '[UTIMESTAMP:<]'",
			'START_TIME'		=> " start_time IN ('[START_TIME]')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['TASK']['W']);
		$L .= (isset($D['TASK']['L']['START']) && $D['TASK']['L']['STEP'])? " LIMIT {$D['TASK']['L']['START']},{$D['TASK']['L']['STEP']} ":'';
		$O .= (isset($D['TASK']['O']['UTIMESTAMP']))?" utimestamp {$D['TASK']['O']['UTIMESTAMP']}, ":'';
		$qry = $this->SQL->query("SELECT id, platform_id PLATFORM_ID, feed_id FEED_ID, active ACTIVE, fail FAIL, start_time START_TIME, itimestamp ITIMESTAMP, utimestamp UTIMESTAMP
							FROM  wp_task
							WHERE 1 {$W}
							ORDER BY {$O} 1 {$L}");

		while($a = $qry->fetchArray(SQLITE3_ASSOC) ) {
			$D['PLATFORM']['D'][ $this->platform_id ]['TASK']['D'][ $a['id'] ] = $a;
		}
	}
	
	function set_task(&$D=null) {
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['TASK']['D'] AS $kC => $C) {
			if($C['ACTIVE'] != -2) {
				
				$IU_CRN .= (($IU_CRN)?',':'')."('{$kC}','{$C['PLATFORM_ID']}'";
				$IU_CRN .= (isset($C['FEED_ID']))? ",'{$C['FEED_ID']}'":",NULL";
				$IU_CRN .= (isset($C['ACTIVE']))? ",'{$C['ACTIVE']}'":",NULL";
				$IU_CRN .= (isset($C['FAIL']))? ",'{$C['FAIL']}'":",NULL";
				$IU_CRN .= (isset($C['START_TIME']))? ",'{$C['START_TIME']}'":",NULL";
				$IU_CRN .= ")";
			}
			else {
				$D_CRN .= (($D_CRN)?',':'')."'{$kC}'";
			}
		}
		if($IU_CRN) {
			$this->SQL->query("INSERT INTO wp_task (id, platform_id, feed_id, active, fail, start_time) VALUES {$IU_CRN}
							ON CONFLICT(id,platform_id) DO UPDATE SET
								active =		CASE WHEN excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active			THEN excluded.active ELSE active END,
								platform_id =	CASE WHEN excluded.platform_id IS NOT NULL	AND ifnull(platform_id,'') <> excluded.platform_id	THEN excluded.platform_id ELSE platform_id END,
								feed_id =		CASE WHEN excluded.feed_id IS NOT NULL		AND ifnull(feed_id,'') <> excluded.feed_id			THEN excluded.feed_id ELSE feed_id END,
								fail =			CASE WHEN excluded.fail IS NOT NULL			AND ifnull(fail,'') <> excluded.fail				THEN excluded.fail ELSE fail END,
								start_time =	CASE WHEN excluded.start_time IS NOT NULL	AND ifnull(start_time,'') <> excluded.start_time	THEN excluded.start_time ELSE start_time END,

								utimestamp =	CASE WHEN 
															   excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
															OR excluded.platform_id IS NOT NULL	AND ifnull(platform_id,'') <> excluded.platform_id
															OR excluded.feed_id IS NOT NULL		AND ifnull(feed_id,'') <> excluded.feed_id
															OR excluded.fail IS NOT NULL		AND ifnull(fail,'') <> excluded.fail
															OR excluded.start_time IS NOT NULL	AND ifnull(start_time,'') <> excluded.start_time
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
		}
		if($D_CRN) {
			$this->SQL->query("DELETE FROM wp_task WHERE id IN ({$D_CRN})");
		}
	}
	
	
	function set_log(&$D=null)
	{
		foreach((array)$D['PLATFORM']['D'] AS $kPLA => $PLA)
		{
			foreach((array)$PLA['LOG']['D'] AS $kLOG => $LOG)
			{
				if($LOG['ACTIVE'] != -2)
				{ #ACHTUNG: ID = microtime
					$IU_LOG .= (($IU_LOG)?',':'')."('{$kLOG}','{$D['SESSION']['USER']['ID']}','{$this->account_id}','{$kPLA}'";
					$IU_LOG .= (isset($LOG['RID']))? ",'{$LOG['RID']}'":",''";
					$IU_LOG .= (isset($LOG['RTYPE']))? ",'{$LOG['RTYPE']}'":",''";
					$IU_LOG .= (isset($LOG['TYPE']))? ",'{$LOG['TYPE']}'":",''";
					$IU_LOG .= (isset($LOG['ACTIVE']))? ",'{$LOG['ACTIVE']}'":",NULL";
					$IU_LOG .= (isset($LOG['TITLE']))? ",'".$this->SQL->escapeString($LOG['TITLE'])."'":",NULL";
					$IU_LOG .= (isset($LOG['TEXT']))? ",'".$this->SQL->escapeString($LOG['TEXT'])."'":",NULL";
					$IU_LOG .= (isset($LOG['DEBUG_TEXT']))? ",'".$this->SQL->escapeString($LOG['DEBUG_TEXT'])."'":",NULL";
					$IU_LOG .= ")";
				}
				else
					$D_LOG .= (($D_LOG)?",":'')."'{$kLOG}'";
			}
		}

		if($IU_LOG)
			$this->SQL->query("INSERT INTO wp_log (id, user_id, account_id, platform_id, rid, rtype, type, active, title, text, debug_text) VALUES {$IU_LOG} 
				ON DUPLICATE KEY UPDATE 
					rid = CASE WHEN VALUES(rid) IS NOT NULL THEN VALUES(rid) ELSE wp_log.rid END,
					rtype = CASE WHEN VALUES(rtype) IS NOT NULL THEN VALUES(rtype) ELSE wp_log.rtype END,
					type = CASE WHEN VALUES(type) IS NOT NULL THEN VALUES(type) ELSE wp_log.type END,
					active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_log.active END,
					title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_log.title END,
					text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE wp_log.text END
			");
		
		if($D_LOG)
			$this->SQL->query("DELETE FROM wp_log WHERE id IN ({$D_LOG}) AND account_id = '{$this->account_id}' ");
	}

	function get_log(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'RID:IN'	=> " AND rid IN ([RID:IN])",#z.b. Artikel_id
			'RTYPE:IN'	=> " AND rid IN ([RTYPE:IN])",#z.b. 'ARTICLE'
		],$ARTICLE['W']);
		$qry = $this->SQL->query("SELECT id AS ID, user_id AS USER_ID, rid AS RID, rtype AS RTYPE, type AS TYPE, active AS ACTIVE, title AS TITLE, text AS TEXT, debug_text AS DEBUG_TEXT, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_log
							WHERE platform_id = '{$this->platform_id}' {$W} ORDER BY itimestamp DESC, id DESC LIMIT 100");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['LOG']['D'][ $a['ID'] ] = $a;

			if($a['RTYPE'] == 'article' || $a['RTYPE'] == 'ARTICLE')
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['RID'] ]['LOG']['D'][ $a['ID'] ] = $a;
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['RID'] ]['ERROR']['D'][ 'xxx' ]['LOG']['D'][ $a['ID'] ] = $a;
				$D['PLATFORM']['D'][ $this->platform_id ]['ERROR']['D'][ 'xxx' ]['ARTICLE']['D'][ $a['RID'] ]['LOG']['D'][ $a['ID'] ] = $a;
		}
	}

	#fügt Attribute der zugeordneten Platformen zu
	function get_attribute(&$D=null)
	{
		#ToDo: Steuerklasse, vorselektiert aus Einstellungen:StandardTaxClass
		$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] = [
			'TaxClass'				=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,  'REQUIRED' => 1, "TYPE"=>"select", "VALUE" => "zero\nnormal\nreduced1\nreduced2", 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Steuerklasse']]] ],
		];
		#Individuelle Attribute
		#$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['ID']))? " AND attribute_id IN ({$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['ID']})":'';
		#$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['PARENT_ID']))? " AND parent_id IN ({$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['PARENT_ID']})":'';
		$W .= CWP::where_interpreter([
			'ID'					=> "attribute_id IN ([ID])",
			'PARENT_ID'				=> "parent_id IN ([PARENT_ID])",
			'CATEGORIE_ID'			=> "id IN (SELECT attribute_id FROM wp_attribute_to_categorie WHERE categorie_id IN ([CATEGORIE_ID]) )",
		],$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']);
		
		$qry = $this->SQL->query("SELECT id, parent_id AS PARENT_ID, active AS ACTIVE, type AS TYPE, topin AS TOPIN, i18n AS I18N, sort AS SORT, value AS VALUE, value_option AS VALUE_OPTION, itimestamp*1 ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute att
							WHERE platform_id = '{$this->platform_id}'  {$W}
							ORDER BY parent_id, sort, (SELECT title FROM wp_attribute_language WHERE language_id = 'DE' AND att.id = attribute_id AND platform_id = att.platform_id)
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['id'] ] = $a;#Neue Version
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['id'] ];#Neue Version

			#$ATT[ $a['id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['id'] ];
		}

		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D']));
		$qry = $this->SQL->query("SELECT attribute_id, language_id, active AS ACTIVE, title AS TITLE, value AS VALUE, value_option AS VALUE_OPTION, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute_language
							WHERE platform_id = '{$this->platform_id}' AND attribute_id IN ('{$ID}')
							ORDER BY language_id,title
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['attribute_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;
		}
		
		$qry = $this->SQL->query("SELECT from_platform_id, from_attribute_id AS ATTRIBUTE_ID, to_platform_id, to_attribute_id, active AS ACTIVE, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute_to_platform_attribute 
							WHERE from_categorie_id = '' AND from_platform_id = '{$this->platform_id}' AND from_attribute_id IN ('{$ID}')
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ] = $a;
		}

		
	}
	
	function set_attribute(&$D)
	{
		#foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTETYPE']['D'] AS $kATTT => $ATTT)
		#{
			foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] AS $kATT => $ATT) {
				if($ATT['ACTIVE'] != -2) {
					$IU_ATT .= (($IU_ATT)?',':'')."('{$kATT}','{$this->platform_id}'";
					$IU_ATT .= (isset($ATT['PARENT_ID']))? ",'{$ATT['PARENT_ID']}'":",NULL";
					$IU_ATT .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
					$IU_ATT .= (isset($ATT['VALUE']))? ",'".$this->SQL->escapeString ($ATT['VALUE'])."'":",NULL";
					$IU_ATT .= (isset($ATT['VALUE_OPTION']))? ",'".$this->SQL->escapeString ($ATT['VALUE_OPTION'])."'":",NULL";
					$IU_ATT .= (isset($ATT['TYPE']))? ",'{$ATT['TYPE']}'":",NULL";
					$IU_ATT .= (isset($ATT['TOPIN']))? ",'{$ATT['TOPIN']}'":",NULL";
					$IU_ATT .= (isset($ATT['I18N']))? ",'{$ATT['I18N']}'":",''";
					$IU_ATT .= (isset($ATT['SORT']))? ",'{$ATT['SORT']}'":",''";
					$IU_ATT .= ")";
					
					foreach((array)$ATT['LANGUAGE']['D'] AS $kLAN => $LAN) {
						if($LAN['ACTIVE'] != -2) {
							$IU_ATT_LG .= (($IU_ATT_LG)?',':'')."('{$kATT}','{$this->platform_id}','{$kLAN}'";
							$IU_ATT_LG .= (isset($LAN['ACTIVE']))? ",'{$LAN['ACTIVE']}'":",NULL";
							$IU_ATT_LG .= (isset($LAN['TITLE']))? ",'".$this->SQL->escapeString ($LAN['TITLE'])."'":",NULL";
							$IU_ATT_LG .= (isset($LAN['VALUE']))? ",'".$this->SQL->escapeString ($LAN['VALUE'])."'":",NULL";
							$IU_ATT_LG .= (isset($LAN['VALUE_OPTION']))? ",'".$this->SQL->escapeString ($LAN['VALUE_OPTION'])."'":",NULL";
							$IU_ATT_LG .= ")";
						}
						else {
							$D_ATT_LG .= (($D_ATT_LG)?',':'')."'{$kATT}{$kLAN}'";
						}
					}
					
					#Att_to_Platform_att START ======================
					foreach((array)$ATT['TO']['PLATFORM']['D'] AS $kPAL => $PLA) {
							if($PLA['ACTIVE'] != -2) {
								$IU_ATT2 .= (($IU_ATT2)?',':'')."('{$this->platform_id}','{$kATT}','{$kPAL}'";
								$IU_ATT2 .= (isset($PLA['ACTIVE']))? ",'{$PLA['ACTIVE']}'":",NULL";
								$IU_ATT2 .= (isset($PLA['ATTRIBUTE_ID']))? ",'{$PLA['ATTRIBUTE_ID']}'":",NULL";
								$IU_ATT2 .= ")";
							}
							else {
								$D_ATT2 .= (($D_ATT2)?',':'')."'{$kATT}{$kPAL}'";
							}
					}
					#Att_to_Platform_att ENDE ======================
				}
				else {
					$D_ATT .= (($D_ATT)?',':'')."'{$kATT}'";
				}
			}
		#}
		if($IU_ATT) {
			$this->SQL->query("INSERT INTO wp_attribute (id, platform_id, parent_id, active, value, value_option, type, topin, i18n,sort) VALUES {$IU_ATT} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
						parent_id =			CASE WHEN excluded.parent_id IS NOT NULL			AND parent_id <> excluded.parent_id	THEN excluded.parent_id ELSE parent_id END,
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active		THEN excluded.active ELSE active END,
						value =				CASE WHEN excluded.value IS NOT NULL				AND value <> excluded.value			THEN excluded.value ELSE value END,
						value_option =		CASE WHEN excluded.value_option IS NOT NULL			AND value <> excluded.value_option	THEN excluded.value_option ELSE value_option END,
						type =				CASE WHEN excluded.type IS NOT NULL					AND type <> excluded.type			THEN excluded.type ELSE type END,
						topin =				CASE WHEN excluded.topin IS NOT NULL				AND topin <> excluded.topin			THEN excluded.topin ELSE topin END,
						i18n =				CASE WHEN excluded.i18n IS NOT NULL					AND i18n <> excluded.i18n			THEN excluded.i18n ELSE i18n END,
						sort =				CASE WHEN excluded.sort IS NOT NULL					AND sort <> excluded.sort			THEN excluded.sort ELSE sort END,
						utimestamp =		CASE WHEN 
													   excluded.parent_id IS NOT NULL	AND parent_id <> excluded.parent_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.value IS NOT NULL				AND value <> excluded.value
													OR excluded.value_option IS NOT NULL		AND value_option <> excluded.value_option
													OR excluded.type IS NOT NULL				AND type <> excluded.type
													OR excluded.topin IS NOT NULL				AND topin <> excluded.topin
													OR excluded.i18n IS NOT NULL				AND i18n <> excluded.i18n
													OR excluded.sort IS NOT NULL				AND sort <> excluded.sort
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}

		if($IU_ATT_LG) {
			$this->SQL->query("INSERT INTO wp_attribute_language (attribute_id, platform_id, language_id, active, title, value, value_option) VALUES {$IU_ATT_LG} 
						ON CONFLICT(attribute_id, platform_id, language_id) DO UPDATE SET
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active		THEN excluded.active ELSE active END,
						title =				CASE WHEN excluded.title IS NOT NULL				AND title <> excluded.title			THEN excluded.title ELSE title END,
						value =				CASE WHEN excluded.value IS NOT NULL				AND value <> excluded.value			THEN excluded.value ELSE value END,
						value_option =		CASE WHEN excluded.value_option IS NOT NULL			AND value <> excluded.value_option	THEN excluded.value_option ELSE value_option END,
						utimestamp =		CASE WHEN 
													   excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.title IS NOT NULL				AND title <> excluded.title
													OR excluded.value IS NOT NULL				AND value <> excluded.value
													OR excluded.value_option IS NOT NULL		AND value <> excluded.value_option
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($IU_ATT2) {
			$this->SQL->query("INSERT INTO wp_attribute_to_platform_attribute (from_platform_id,from_attribute_id, to_platform_id, active, to_attribute_id) VALUES {$IU_ATT2} 
				ON CONFLICT(from_platform_id, from_attribute_id, to_platform_id, from_categorie_id) DO UPDATE SET
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active						THEN excluded.active ELSE active END,
						to_attribute_id =	CASE WHEN excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id		THEN excluded.to_attribute_id ELSE to_attribute_id END,
						utimestamp =		CASE WHEN 
													   excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
											
				");
		}
		if($D_ATT_LG) {
			$this->SQL->query("DELETE FROM wp_attribute_language WHERE (attribute_id || language_id) IN ({$D_ATT_LG}) AND platform_id = '{$this->platform_id}'");
		}

		if($D_ATT) {
			$this->SQL->query("DELETE FROM wp_attribute WHERE id IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_language WHERE attribute_id IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_categorie_id = '' AND from_attribute_id IN ({$D_ATT}) AND from_platform_id = '{$this->platform_id}'");
		}

		if($D_ATT2) {
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_categorie_id = '' AND (from_attribute_id || to_platform_id) IN ({$D_ATT2}) AND from_platform_id = '{$this->platform_id}'");
		}
		#Attribute Korektur i18n, wenn ein Attribute von i18n gewechselt wird dann müssen alle Artikel zuweisungen entsprechend umgestelt werden
		$this->SQL->query("DELETE FROM wp_article_attribute WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = ''
							AND attribute_id || article_id IN (SELECT attribute_id || article_id FROM wp_article_attribute WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = 'DE')
							");#bereinige vorher, falls es Language_id = '' und 'DE' beinhaltet
		$this->SQL->query("UPDATE wp_article_attribute SET language_id = '' WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = 'DE'");
		
		###$this->SQL->query("UPDATE wp_article_attribute SET language_id = 'DE' WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 1 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}'");
	}

	function set_order($D) {
		$k = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']);
		for($i=0; $i< count($k); $i++) {
			$ORDER = $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$k[$i]];
			if($ORDER['ACTIVE'] != -2) {
				#SET_CUSTOMER START ========================
				if($ORDER['CUSTOMER_ID'])
				$D['PLATFORM']['D'][ $this->platform_id ]['CUSTOMER']['D'][$ORDER['CUSTOMER_ID']] = [
					'MESSAGE_GROUP_ID'=> $ORDER['MESSAGE_GROUP_ID'],
					'NICKNAME'		=> $ORDER['CUSTOMER_NICKNAME'],
					'EMAIL'			=> $ORDER['CUSTOMER_EMAIL'],
					'COMPANY'		=> $ORDER['BILLING']['COMPANY'],
					'NAME'			=> $ORDER['BILLING']['NAME'],
					'FNAME'			=> $ORDER['BILLING']['FNAME'],
					'STREET'		=> $ORDER['BILLING']['STREET'],
					'STREET_NO'		=> $ORDER['BILLING']['STREET_NO'],
					'ZIP'			=> $ORDER['BILLING']['ZIP'],
					'CITY'			=> $ORDER['BILLING']['CITY'],
					'COUNTRY_ID'	=> $ORDER['BILLING']['COUNTRY_ID'],
					'ADDITION'		=> $ORDER['BILLING']['ADDITION'],
				];
				#SET_CUSTOMER END   ========================
				
				$IU_ORDER .= (($IU_ORDER)?',':'')."('{$k[$i]}','{$this->platform_id}'";
				$IU_ORDER .= (isset($ORDER['PAYMENT_ID']))? ",'{$ORDER['PAYMENT_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['ACTIVE']))? ",'{$ORDER['ACTIVE']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['STATUS']))? ",'{$ORDER['STATUS']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_ID']))? ",'{$ORDER['CUSTOMER_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_NICKNAME']))? ",'".$this->SQL->escapeString ($ORDER['CUSTOMER_NICKNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_EMAIL']))? ",'{$ORDER['CUSTOMER_EMAIL']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['PAID']))? ",'{$ORDER['PAID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CURRENCYCODE']))? ",'{$ORDER['CURRENCYCODE']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CONVERSIONRATE']))? ",'{$ORDER['CONVERSIONRATE']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COMPANY']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['COMPANY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['NAME']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['FNAME']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET_NO']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['STREET_NO'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ZIP']))? ",'{$ORDER['BILLING']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['CITY']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COUNTRY_ID']))? ",'{$ORDER['BILLING']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ADDITION']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['ADDITION'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['PHONE']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['PHONE'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COMPANY']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['COMPANY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['NAME']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['FNAME']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET_NO']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['STREET_NO'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ZIP']))? ",'{$ORDER['DELIVERY']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['CITY']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COUNTRY_ID']))? ",'{$ORDER['DELIVERY']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ADDITION']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['ADDITION'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['PHONE']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['PHONE'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['WAREHOUSE_ID']))? ",'{$ORDER['WAREHOUSE_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['SHIPPING_ID']))? ",'{$ORDER['SHIPPING_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['COMMENT']))? ",'".$this->SQL->escapeString ($ORDER['COMMENT'])."'":",NULL";
				$IU_ORDER .= ")";
				
				foreach((array)$ORDER['ARTICLE']['D'] AS $kA => $vA) {
					if($vA['ACTIVE'] != -2) {
						$IU_ARTICLE .= (($IU_ARTICLE)?',':'')."('{$kA}','{$k[$i]}','{$this->platform_id}'";
						$IU_ARTICLE .= (isset($vA['ACTIVE']))? ",'{$vA['ACTIVE']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['NUMBER']))? ",'{$vA['NUMBER']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['TITLE']))? ",'".$this->SQL->escapeString ($vA['TITLE'])."'":",NULL";
						$IU_ARTICLE .= (isset($vA['STOCK']))? ",'{$vA['STOCK']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['WEIGHT']))? ",'{$vA['WEIGHT']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['PRICE']))? ",'{$vA['PRICE']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['VAT']))? ",'{$vA['VAT']}'":",NULL";
						$IU_ARTICLE .= ")";
					}
					else {
						$D_ARTICLE .= (($D_ARTICLE)?',':'')."'{$kA}{$k[$i]}'";
					}
				}
			}
			else {
				$D_ORDER .= (($D_ORDER)?',':'')."'{$k[$i]}'";
			}
		}
		
		
		if($IU_ORDER) {
			$this->SQL->query("INSERT INTO wp_order (id,platform_id, payment_id, active, status, customer_id, customer_nickname, customer_email, paid, currencycode, conversionrate
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition,billing_phone
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition,delivery_phone
								,warehouse_id,shipping_id,comment) VALUES {$IU_ORDER} 

						ON CONFLICT(id, platform_id) DO UPDATE SET
							payment_id =			CASE WHEN excluded.payment_id IS NOT NULL			AND ifnull(payment_id,'') <> excluded.payment_id					THEN excluded.payment_id ELSE payment_id END,
							active =				CASE WHEN excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active							THEN excluded.active ELSE active END,
							status =				CASE WHEN excluded.status IS NOT NULL				AND ifnull(status,'') <> excluded.status							THEN excluded.status ELSE status END,
							customer_id =			CASE WHEN excluded.customer_id IS NOT NULL			AND ifnull(customer_id,'') <> excluded.customer_id					THEN excluded.customer_id ELSE customer_id END,
							customer_nickname =		CASE WHEN excluded.customer_nickname IS NOT NULL	AND ifnull(customer_nickname,'') <> excluded.customer_nickname		THEN excluded.customer_nickname ELSE customer_nickname END,
							customer_email =		CASE WHEN excluded.customer_email IS NOT NULL		AND ifnull(customer_email,'') <> excluded.customer_email			THEN excluded.customer_email ELSE customer_email END,
							paid =					CASE WHEN excluded.paid IS NOT NULL					AND ifnull(paid,'') <> excluded.paid								THEN excluded.paid ELSE paid END,
							currencycode =			CASE WHEN excluded.currencycode IS NOT NULL			AND ifnull(currencycode,'') <> excluded.currencycode				THEN excluded.currencycode ELSE currencycode END,
							conversionrate =		CASE WHEN excluded.conversionrate IS NOT NULL		AND ifnull(conversionrate,'') <> excluded.conversionrate			THEN excluded.conversionrate ELSE conversionrate END,
							billing_company =		CASE WHEN excluded.billing_company IS NOT NULL		AND ifnull(billing_company,'') <> excluded.billing_company			THEN excluded.billing_company ELSE billing_company END,
							billing_name =			CASE WHEN excluded.billing_name IS NOT NULL			AND ifnull(billing_name,'') <> excluded.billing_name				THEN excluded.billing_name ELSE billing_name END,
							billing_fname =			CASE WHEN excluded.billing_fname IS NOT NULL		AND ifnull(billing_fname,'') <> excluded.billing_fname				THEN excluded.billing_fname ELSE billing_fname END,
							billing_street =		CASE WHEN excluded.billing_street IS NOT NULL		AND ifnull(billing_street,'') <> excluded.billing_street			THEN excluded.billing_street ELSE billing_street END,
							billing_street_no =		CASE WHEN excluded.billing_street_no IS NOT NULL	AND ifnull(billing_street_no,'') <> excluded.billing_street_no		THEN excluded.billing_street_no ELSE billing_street_no END,
							billing_zip =			CASE WHEN excluded.billing_zip IS NOT NULL			AND ifnull(billing_zip,'') <> excluded.billing_zip					THEN excluded.billing_zip ELSE billing_zip END,
							billing_city =			CASE WHEN excluded.billing_city IS NOT NULL			AND ifnull(billing_city,'') <> excluded.billing_city				THEN excluded.billing_city ELSE billing_city END,
							billing_country_id =	CASE WHEN excluded.billing_country_id IS NOT NULL	AND ifnull(billing_country_id,'') <> excluded.billing_country_id	THEN excluded.billing_country_id ELSE billing_country_id END,
							billing_addition =		CASE WHEN excluded.billing_addition IS NOT NULL		AND ifnull(billing_addition,'') <> excluded.billing_addition		THEN excluded.billing_addition ELSE billing_addition END,
							billing_phone =			CASE WHEN excluded.billing_phone IS NOT NULL		AND ifnull(billing_phone,'') <> excluded.billing_phone				THEN excluded.billing_phone ELSE billing_phone END,
							delivery_company =		CASE WHEN excluded.delivery_company IS NOT NULL		AND ifnull(delivery_company,'') <> excluded.delivery_company		THEN excluded.delivery_company ELSE delivery_company END,
							delivery_name =			CASE WHEN excluded.delivery_name IS NOT NULL		AND ifnull(delivery_name,'') <> excluded.delivery_name				THEN excluded.delivery_name ELSE delivery_name END,
							delivery_fname =		CASE WHEN excluded.delivery_fname IS NOT NULL		AND ifnull(delivery_fname,'') <> excluded.delivery_fname			THEN excluded.delivery_fname ELSE delivery_fname END,
							delivery_street =		CASE WHEN excluded.delivery_street IS NOT NULL		AND ifnull(delivery_street,'') <> excluded.delivery_street			THEN excluded.delivery_street ELSE delivery_street END,
							delivery_street_no =	CASE WHEN excluded.delivery_street_no IS NOT NULL	AND ifnull(delivery_street_no,'') <> excluded.delivery_street_no	THEN excluded.delivery_street_no ELSE delivery_street_no END,
							delivery_zip =			CASE WHEN excluded.delivery_zip IS NOT NULL			AND ifnull(delivery_zip,'') <> excluded.delivery_zip				THEN excluded.delivery_zip ELSE delivery_zip END,
							delivery_city =			CASE WHEN excluded.delivery_city IS NOT NULL		AND ifnull(delivery_city,'') <> excluded.delivery_city				THEN excluded.delivery_city ELSE delivery_city END,
							delivery_country_id =	CASE WHEN excluded.delivery_country_id IS NOT NULL	AND ifnull(delivery_country_id,'') <> excluded.delivery_country_id	THEN excluded.delivery_country_id ELSE delivery_country_id END,
							delivery_addition =		CASE WHEN excluded.delivery_addition IS NOT NULL	AND ifnull(delivery_addition,'') <> excluded.delivery_addition		THEN excluded.delivery_addition ELSE delivery_addition END,
							delivery_phone =		CASE WHEN excluded.delivery_phone IS NOT NULL		AND ifnull(delivery_phone,'') <> excluded.delivery_phone			THEN excluded.delivery_phone ELSE delivery_phone END,
							warehouse_id =			CASE WHEN excluded.warehouse_id IS NOT NULL			AND ifnull(warehouse_id,'') <> excluded.warehouse_id				THEN excluded.warehouse_id ELSE warehouse_id END,
							shipping_id =			CASE WHEN excluded.shipping_id IS NOT NULL			AND ifnull(shipping_id,'') <> excluded.shipping_id					THEN excluded.shipping_id ELSE shipping_id END,
							comment =				CASE WHEN excluded.comment IS NOT NULL				AND ifnull(comment,'') <> excluded.comment							THEN excluded.comment ELSE comment END,
							utimestamp =		CASE WHEN 
													   excluded.payment_id IS NOT NULL			AND payment_id <> excluded.payment_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.status IS NOT NULL				AND status <> excluded.status
													OR excluded.customer_id IS NOT NULL			AND customer_id <> excluded.customer_id
													OR excluded.customer_nickname IS NOT NULL	AND customer_nickname <> excluded.customer_nickname
													OR excluded.customer_email IS NOT NULL		AND customer_email <> excluded.customer_email
													OR excluded.paid IS NOT NULL				AND paid <> excluded.paid
													OR excluded.currencycode IS NOT NULL		AND currencycode <> excluded.currencycode
													OR excluded.conversionrate IS NOT NULL		AND conversionrate <> excluded.conversionrate
													OR excluded.billing_company IS NOT NULL		AND billing_company <> excluded.billing_company
													OR excluded.billing_name IS NOT NULL		AND billing_name <> excluded.billing_name
													OR excluded.billing_fname IS NOT NULL		AND billing_fname <> excluded.billing_fname
													OR excluded.billing_street IS NOT NULL		AND billing_street <> excluded.billing_street
													OR excluded.billing_street_no IS NOT NULL	AND billing_street_no <> excluded.billing_street_no
													OR excluded.billing_zip IS NOT NULL			AND billing_zip <> excluded.billing_zip
													OR excluded.billing_city IS NOT NULL		AND billing_city <> excluded.billing_city
													OR excluded.billing_country_id IS NOT NULL	AND billing_country_id <> excluded.billing_country_id
													OR excluded.billing_addition IS NOT NULL	AND billing_addition <> excluded.billing_addition
													OR excluded.billing_phone IS NOT NULL		AND billing_phone <> excluded.billing_phone
													OR excluded.delivery_company IS NOT NULL	AND delivery_company <> excluded.delivery_company
													OR excluded.delivery_name IS NOT NULL		AND delivery_name <> excluded.delivery_name
													OR excluded.delivery_fname IS NOT NULL		AND delivery_fname <> excluded.delivery_fname
													OR excluded.delivery_street IS NOT NULL		AND delivery_street <> excluded.delivery_street
													OR excluded.delivery_street_no IS NOT NULL	AND delivery_street_no <> excluded.delivery_street_no
													OR excluded.delivery_zip IS NOT NULL		AND delivery_zip <> excluded.delivery_zip
													OR excluded.delivery_city IS NOT NULL		AND delivery_city <> excluded.delivery_city
													OR excluded.delivery_country_id IS NOT NULL	AND delivery_country_id <> excluded.delivery_country_id
													OR excluded.delivery_addition IS NOT NULL	AND delivery_addition <> excluded.delivery_addition
													OR excluded.delivery_phone IS NOT NULL		AND delivery_phone <> excluded.delivery_phone
													OR excluded.warehouse_id IS NOT NULL		AND warehouse_id <> excluded.warehouse_id
													OR excluded.shipping_id IS NOT NULL			AND shipping_id <> excluded.shipping_id
													OR excluded.comment IS NOT NULL				AND comment <> excluded.comment
										THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($IU_ARTICLE) {
			$this->SQL->query("INSERT INTO wp_order_article (id,order_id,platform_id,active,number,title,stock,weight,price,vat) VALUES {$IU_ARTICLE} 
						ON CONFLICT(id, order_id, platform_id) DO UPDATE SET
							order_id =		CASE WHEN excluded.order_id IS NOT NULL		AND order_id <> excluded.order_id	THEN excluded.order_id ELSE order_id END,
							active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active		THEN excluded.active ELSE active END,
							number =		CASE WHEN excluded.number IS NOT NULL		AND number <> excluded.number		THEN excluded.number ELSE number END,
							title =			CASE WHEN excluded.title IS NOT NULL		AND title <> excluded.title			THEN excluded.title ELSE title END,
							stock =			CASE WHEN excluded.stock IS NOT NULL		AND stock <> excluded.stock			THEN excluded.stock ELSE stock END,
							weight =		CASE WHEN excluded.weight IS NOT NULL		AND weight <> excluded.weight		THEN excluded.weight ELSE weight END,
							price =			CASE WHEN excluded.price IS NOT NULL		AND price <> excluded.price			THEN excluded.price ELSE price END,
							vat =			CASE WHEN excluded.vat IS NOT NULL			AND vat <> excluded.vat				THEN excluded.vat ELSE vat END,
							utimestamp =	CASE WHEN
													   excluded.order_id IS NOT NULL	AND order_id <> excluded.order_id
													OR excluded.active IS NOT NULL		AND active <> excluded.active
													OR excluded.number IS NOT NULL		AND number <> excluded.number
													OR excluded.title IS NOT NULL		AND title <> excluded.title
													OR excluded.stock IS NOT NULL		AND stock <> excluded.stock
													OR excluded.weight IS NOT NULL		AND weight <> excluded.weight
													OR excluded.price IS NOT NULL		AND price <> excluded.price
													OR excluded.vat IS NOT NULL			AND vat <> excluded.vat
							THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($D_ORDER) {
			$this->SQL->query("DELETE FROM wp_order WHERE id IN ({$D_ORDER})");
		}
		if($D_ARTICLE) {
			$this->SQL->query("DELETE FROM wp_order_article WHERE (id || order_id) IN ({$D_ARTICLE})");
		}
		return $D;
	}
	
	
	function get_order(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ITIMESTAMP'])? " AND DATE(itimestamp) LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ITIMESTAMP']}'":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ID']}')":'';
		#$W .= ($this->platform_id)? " AND platform_id = '{$this->platform_id}'":'';
		$W .= ($D['ORDER']['W']['ID'])? " AND id IN ('{$D['ORDER']['W']['ID']}')":'';

		$O .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['O']['ITIMESTAMP'])?" itimestamp {$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['O']['ITIMESTAMP']},":" itimestamp DESC,";
		$qry = $this->SQL->query("SELECT id,platform_id,payment_id, active, status, customer_id, customer_nickname, customer_email, paid, currencycode, conversionrate
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition,billing_phone
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition,delivery_phone
								,warehouse_id,shipping_id, comment,itimestamp,utimestamp,
								DATE(itimestamp) date
							FROM wp_order o
							WHERE platform_id = '{$this->platform_id}'
							{$W} ORDER BY {$O} 1 LIMIT 1100");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['platform_id'], #Platform_from_id
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'PAYMENT_ID'			=> $a['payment_id'],
				'DATE'					=> $a['date'],
				'PAID'					=> $a['paid'],
				'CURRENCYCODE'			=> $a['currencycode'],
				'CONVERSIONRATE'		=> $a['conversionrate'],
				'BILLING'				=> [
						'COMPANY'		=> $a['billing_company'],
						'NAME'			=> $a['billing_name'],
						'FNAME'			=> $a['billing_fname'],
						'STREET'		=> $a['billing_street'],
						'STREET_NO'		=> $a['billing_street_no'],
						'ZIP'			=> $a['billing_zip'],
						'CITY'			=> $a['billing_city'],
						'COUNTRY_ID'	=> $a['billing_country_id'],
						'ADDITION'		=> $a['billing_addition'],
						'PHONE'			=> $a['billing_phone'],
				],
				'DELIVERY'				=> [
						'COMPANY'		=> $a['delivery_company'],	
						'NAME'			=> $a['delivery_name'],
						'FNAME'			=> $a['delivery_fname'],
						'STREET'		=> $a['delivery_street'],
						'STREET_NO'		=> $a['delivery_street_no'],
						'ZIP'			=> $a['delivery_zip'],
						'CITY'			=> $a['delivery_city'],
						'COUNTRY_ID'	=> $a['delivery_country_id'],
						'ADDITION'		=> $a['delivery_addition'],
						'PHONE'			=> $a['delivery_phone'],
				],
				'WAREHOUSE_ID'			=> $a['warehouse_id'],
				'SHIPPING_ID'			=> $a['shipping_id'],
				'COMMENT'				=> $a['comment'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
		}
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']));
		$qry = $this->SQL->query("SELECT id,platform_id,order_id,active,title,stock,price,vat,itimestamp,utimestamp
							FROM wp_order_article
							WHERE platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}') ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['from_platform_id'],
				'PARENT_ID'				=> $a['parent_id'],
				#'ARTICLE_ID'			=> $a['from_article_id'],
				#'PARENT_ID'				=> $a['from_parent_id'],
				'ACTIVE'				=> $a['active'],
				#'NUMBER'				=> $a['number'],
				'TITLE'					=> $a['title'],
				'STOCK'					=> $a['stock'],
				#'WEIGHT'				=> $a['weight'],
				'PRICE'					=> $a['price'],
				'VAT'					=> $a['vat'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
		
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
		}
		
		#Versand wp_delivery
		$qry = $this->SQL->query("SELECT id,platform_id,order_id, active ACTIVE, status STATUS, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_delivery
							WHERE platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}') ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['D'][ $a['id'] ] = $a;
		}
	}

	function get_categorie(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID'					=> "c.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['ID']}')",
			'ID:IN'					=> "c.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['ID:IN']}')",
			'PARENT_ID'				=> "c.parent_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['PARENT_ID']}')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']);

		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['STEP'])? " LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['START']},{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['STEPP']} ":'';
		$qry = $this->SQL->query("SELECT id, parent_id, active, title, breadcrumb_id, breadcrumb_title, itimestamp, utimestamp,
							(SELECT COUNT(*) FROM wp_categorie b WHERE b.platform_id = c.platform_id AND b.parent_id = c.id) count_cat,
							(SELECT COUNT(*) FROM wp_article_attribute ca WHERE ca.platform_id = c.platform_id AND ca.attribute_id = 'CategorieId' AND ca.value = c.id) count_art
							FROM wp_categorie c
							WHERE platform_id = '{$this->platform_id}' 
							{$W} ORDER BY title {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ] = [
				'PARENT_ID'					=> $a['parent_id'],
				'BREADCRUMB_ID'				=> $a['breadcrumb_id'],
				'ACTIVE'					=> $a['active'],
				'COUNT'						=> $a['count_cat'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['LANGUAGE']['D']['DE'] = [
				'TITLE'				=> $a['title'],
				'BREADCRUMB_TITLE'	=> $a['breadcrumb_title'],
			];
			
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['PARENT']['D'][ $a['parent_id'] ]['CHILD']['D'][ $a['id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ];

			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['ARTICLE']['COUNT'] = $a['count_art'];
		}
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D']));
		#REFERENZ START =================
		$qry = $this->SQL->query("SELECT from_categorie_id, to_platform_id, to_categorie_id, active, utimestamp, itimestamp
						FROM wp_categorie_to_categorie c2c
						WHERE from_platform_id = '{$this->platform_id}'
						
						AND from_categorie_id IN ('{$ID}')");
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['CATEGORIE']['D'][ $a['to_categorie_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
		}
		#REFERENZ ENDE =================
		# ['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]
		/*
		$k = array_keys($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D']);
		for($i=0;$i < count($k); $i++)
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['ARTICLE']['COUNT'] += $a['count_art'];
		}
		*/
		
		#Categorie-ATTRIBUTE
		$qry = $this->SQL->query("SELECT attribute_id, categorie_id, platform_id, active ACTIVE, required REQUIRED, value VALUE, value_option VALUE_OPTION, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
						FROM wp_attribute_to_categorie
						WHERE platform_id = '{$this->platform_id}'
						AND categorie_id IN ('{$ID}')");
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['categorie_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['ATTRIBUTE']['D'][ $a['attribute_id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['categorie_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ];
		}
		#Categorie-ATTRIBUTE Languge
		#ToDo: Attribute für Multilanguage muss realisiert werden, wegen value_option


		#ATTRIBUTE Platform zuordnung
		$qry = $this->SQL->query("SELECT from_platform_id, from_categorie_id, from_attribute_id, to_platform_id, to_attribute_id, active ACTIVE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
							FROM wp_attribute_to_platform_attribute 
							WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id IN ('{$ID}')
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			#$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['ATTRIBUTE']['D'][ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ]['ATTRIBUTE']['D'][ $a['to_attribute_id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['ATTRIBUTE']['D'][ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ] = $a;
		}

		#CATEGORIE_TO_CATEGORIE START =====================
		
		$qry = $this->SQL->query("SELECT from_platform_id,to_platform_id,from_categorie_id,to_categorie_id,active,itimestamp,utimestamp
								FROM wp_categorie_to_categorie
								WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id IN ('{$ID}') ");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ]['CATEGORIE']['D'][ $a['to_categorie_id'] ] = [
				'ACTIVE'		=> $a['active'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'LANGUAGE'		=> $PL['PLATFORM']['D'][ $a['from_platform_id'] ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['LANGUAGE'],
			];
		}
		#CATEGORIE_TO_CATEGORIE END =======================
	}
	
	function set_categorie(&$D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'] AS $kCAT => $CAT)
		{
			if($CAT['ACTIVE'] != -2)
			{
				$IU_CAT .= (($IU_CAT)?',':'')."('{$kCAT}','{$this->platform_id}','DE'";
				$IU_CAT .= (isset($CAT['PARENT_ID']))? ",'{$CAT['PARENT_ID']}'":",NULL";
				$IU_CAT .= (isset($CAT['ACTIVE']))? ",'{$CAT['ACTIVE']}'":",NULL";
				$IU_CAT .= (isset($CAT['LANGUAGE']['D']['DE']['TITLE']))? ",'".$this->SQL->escapeString($CAT['LANGUAGE']['D']['DE']['TITLE'])."'":",NULL";
				$IU_CAT .= (isset($CAT['BREADCRUMB_ID']))? ",'".$this->SQL->escapeString ($CAT['BREADCRUMB_ID'])."'":",NULL";
				$IU_CAT .= (isset($CAT['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE']))? ",'".$this->SQL->escapeString ($CAT['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE'])."'":",NULL";
				$IU_CAT .= ")";
				
				foreach((array)$CAT['ATTRIBUTE']['D'] AS $kATT => $ATT)
				{
					if($ATT['ACTIVE'] != -2)
					{
						$IU_ATT .= (($IU_ATT)?',':'')."('{$kATT}','{$kCAT}','{$this->platform_id}'";
						$IU_ATT .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
						$IU_ATT .= (isset($ATT['REQUIRED']))? ",'{$ATT['REQUIRED']}'":",NULL";
						$IU_ATT .= (isset($ATT['VALUE']))? ",'{$ATT['VALUE']}'":",NULL";#ToDo: Select Auswahl für Language Attribute muss implementiert werden!
						$IU_ATT .= (isset($ATT['VALUE_OPTION']))? ",'{$ATT['VALUE_OPTION']}'":",NULL";
						$IU_ATT .= ")";
						
						foreach((array)$ATT['TO']['PLATFORM']['D'] AS $ktoPLA => $toPLA)
						{
							#foreach($toPLA['ATTRIBUTE']['D'] AS $ktoATT => $toATT)
							#{
								if($toATT['ACTIVE'] != -2)
								{
									$IU_to_ATT .= (($IU_to_ATT)?',':'')."('{$this->platform_id}','{$kCAT}','{$kATT}','{$ktoPLA}'";
									$IU_to_ATT .= (isset($toPLA['ATTRIBUTE_ID']))? ",'{$toPLA['ATTRIBUTE_ID']}'":",NULL";
									$IU_to_ATT .= (isset($toPLA['ACTIVE']))? ",'{$toPLA['ACTIVE']}'":",NULL";
									$IU_to_ATT .= ")";
								}
								else
								{
									$D_to_ATT .= (($D_to_ATT)?',':'')."'{$kCAT}{$kATT}{$ktoPLA}'";	
								}
							#}
						}
					}
					else
					{
						$D_ATT .= (($D_ATT)?',':'')."'{$kATT}{$kCAT}'";
					}
				}
				
				
				#CATEGORIE_TO_CATEGORIE START =====================
				foreach((array)$CAT['TO']['PLATFORM']['D'] AS $kPLA => $PLA)
				{
					foreach((array)$PLA['CATEGORIE']['D'] AS $kToCAT => $ToCAT)
					{
						if($ToCAT['ACTIVE'] != -2)
						{
							$IU_CTC .= (($IU_CTC)?',':'')."('{$this->platform_id}','{$kCAT}','{$kPLA}','{$kToCAT}'";
							$IU_CTC .= (isset($ToCAT['ACTIVE']))? ",'{$ToCAT['ACTIVE']}'":",NULL";
							$IU_CTC .= ")";
						}
						else
						{
							$D_CTC .= (($D_CTC)?',':'')."'{$this->platform_id}{$kCAT}{$kPLA}{$kToCAT}'";
						}
					}
				}
				#CATEGORIE_TO_CATEGORIE END =======================
				
				
			}
			else
			{
				$D_CAT .= (($D_CAT)?',':'')."'{$kCAT}'";
			}
		}
		
		if($IU_CAT)
			$this->SQL->query("INSERT INTO wp_categorie (id, platform_id,language_id,parent_id,active,title,breadcrumb_id,breadcrumb_title) VALUES {$IU_CAT} 
						ON CONFLICT(id, language_id, platform_id) DO UPDATE SET
							parent_id =			CASE WHEN excluded.parent_id IS NOT NULL		AND parent_id <> excluded.parent_id					THEN excluded.parent_id ELSE parent_id END,
							active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
							title =				CASE WHEN excluded.title IS NOT NULL			AND title <> excluded.title							THEN excluded.title ELSE title END,
							breadcrumb_id =		CASE WHEN excluded.breadcrumb_id IS NOT NULL	AND breadcrumb_id <> excluded.breadcrumb_id			THEN excluded.breadcrumb_id ELSE breadcrumb_id END,
							breadcrumb_title =	CASE WHEN excluded.breadcrumb_title IS NOT NULL	AND breadcrumb_title <> excluded.breadcrumb_title	THEN excluded.breadcrumb_title ELSE breadcrumb_title END,
							utimestamp =		CASE WHEN
													   excluded.parent_id IS NOT NULL			AND parent_id <> excluded.parent_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.title IS NOT NULL				AND title <> excluded.title
													OR excluded.breadcrumb_id IS NOT NULL		AND breadcrumb_id <> excluded.breadcrumb_id
													OR excluded.breadcrumb_title IS NOT NULL	AND breadcrumb_title <> excluded.breadcrumb_title
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		
		if($IU_ATT)
			$this->SQL->query("INSERT INTO wp_attribute_to_categorie (attribute_id, categorie_id, platform_id, active, required, value, value_option) VALUES {$IU_ATT} 
						ON CONFLICT(attribute_id, categorie_id, platform_id) DO UPDATE SET
							active =			CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active						THEN excluded.active ELSE active END,
							required =			CASE WHEN excluded.required IS NOT NULL			AND ifnull(required,'') <> excluded.required					THEN excluded.required ELSE required END,
							value =				CASE WHEN excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value							THEN excluded.value ELSE value END,
							value_option =		CASE WHEN excluded.value_option IS NOT NULL		AND ifnull(value_option,'') <> excluded.value_option			THEN excluded.value_option ELSE value_option END,
							utimestamp =		CASE WHEN
													   excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active
													OR excluded.required IS NOT NULL			AND ifnull(required,'') <> excluded.required
													OR excluded.value IS NOT NULL				AND ifnull(value,'') <> excluded.value
													OR excluded.value_option IS NOT NULL		AND ifnull(value_option,'') <> excluded.value_option
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						"); 
		
		if($D_CAT)
		{
			$this->SQL->query("DELETE FROM wp_categorie WHERE id IN ({$D_CAT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_categorie WHERE platform_id = '{$this->platform_id}' AND categorie_id NOT IN (SELECT id FROM wp_categorie WHERE platform_id = '{$this->platform_id}')");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id NOT IN (SELECT id FROM wp_categorie WHERE platform_id = '{$this->platform_id}') ");
		}
		
		if($D_ATT)
		{
			$this->SQL->query("DELETE FROM wp_attribute_to_categorie WHERE (attribute_id || categorie_id) IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_platform_id = '{$this->platform_id}' AND (from_attribute_id || from_platform_id) NOT IN (SELECT (attribute_id || categorie_id) FROM wp_attribute_to_categorie WHERE platform_id = '{$this->platform_id}') ");
		}
		
		if($IU_to_ATT)
			$this->SQL->query("INSERT INTO wp_attribute_to_platform_attribute ( from_platform_id, from_categorie_id, from_attribute_id, to_platform_id, to_attribute_id, active) VALUES {$IU_to_ATT} 
							ON CONFLICT( from_platform_id, from_attribute_id, to_platform_id, from_categorie_id) DO UPDATE SET
								active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
								to_attribute_id =	CASE WHEN excluded.to_attribute_id IS NOT NULL	AND to_attribute_id <> excluded.to_attribute_id		THEN excluded.to_attribute_id ELSE to_attribute_id END,
								utimestamp =		CASE WHEN
														   excluded.active IS NOT NULL				AND active <> excluded.active
														OR excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
			"); 
		if($D_to_ATT)
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE (from_categorie_id || from_attribute_id || to_platform_id) IN ({$D_to_ATT}) AND from_platform_id = '{$this->platform_id}'");
	
		if($IU_CTC)
			$this->SQL->query("INSERT INTO wp_categorie_to_categorie (from_platform_id,from_categorie_id,to_platform_id,to_categorie_id,active) VALUES {$IU_CTC} 
						ON CONFLICT(from_platform_id, from_categorie_id, to_platform_id, to_categorie_id) DO UPDATE SET
							active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
							utimestamp =		CASE WHEN
													   excluded.active IS NOT NULL				AND active <> excluded.active
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($D_CTC)
			$this->SQL->query("DELETE FROM wp_categorie_to_categorie WHERE (from_platform_id || from_categorie_id || to_platform_id || to_categorie_id) IN ({$D_CTC})");
	}
	
	#Cat_to_Cat
	function relation_categorie(&$D=null)
	{
		$W = ($D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['TO_CATEGORIE_ID'])?"AND to_categorie_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['TO_CATEGORIE_ID']}')":'';
		$W = ($D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['FROM_CATEGORIE_ID'])?"AND from_categorie_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['FROM_CATEGORIE_ID']}')":'';
		$W = ($D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['FROM_PLATFORM_ID'])?"AND to_platform_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['CATEGORIE']['W']['FROM_PLATFORM_ID']}')":'';
		
		$qry = $this->SQL->query("SELECT from_platform_id,to_platform_id,from_categorie_id,to_categorie_id,ctc.active,ctc.itimestamp,ctc.utimestamp
								,breadcrumb_id,breadcrumb_title, title
								FROM wp_categorie_to_categorie ctc
								LEFT JOIN wp_categorie c ON ctc.from_categorie_id = c.id AND ctc.account_id = c.account_id AND from_platform_id = c.platform_id AND c.language_id = 'DE'
								WHERE ctc.account_id = '{$this->account_id}' AND to_platform_id = '{$this->platform_id}' {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['to_categorie_id'] ]['FROM']['PLATFORM']['D'][ $a['from_platform_id'] ]['CATEGORIE']['D'][ $a['from_categorie_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'BREADCRUMB_ID'	=> $a['breadcrumb_id'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				);
			
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['to_categorie_id'] ]['FROM']['PLATFORM']['D'][ $a['from_platform_id'] ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['LANGUAGE']['D']['DE'] = array(
				'TITLE'				=> $a['title'],
				'BREADCRUMB_TITLE'	=> $a['breadcrumb_title'],
				);
		}
		#return $D;
	}
	
	
	function get_setting(&$D=null)
	{
		$this->ACCOUNT->get_setting($d);#ToDo: Soll Settings überschreiben die nicht verändert werden dürfen.
		
		foreach((array)$d['SETTING']['D'] AS $kSET => $SET) {
			if($SET['READWRITE'] >= 30) {#Nur Platform Einstellungen an Platform übergeben
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][$kSET] = $SET;
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['PARENT']['D'][ $SET['PARENT_ID'] ]['CHILD']['D'][ $kSET ] = &$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][$kSET];
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['CHILD']['D'][ $kSET ] = &$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['PARENT']['D'][ $SET['PARENT_ID'] ]['CHILD']['D'][ $kSET ];
			}
		}



		#Von Platform + Gibt die Struktur vor!
		$this->info($info);
		$cK = array_keys((array)$info['CLASS']);//Class NAME
		$F = CFile::read("core/platform/{$cK[0]}/setting.xml");
		$X = CFile::xml($F);
		foreach((array) $X['SETTING']['D'] AS $kSET => $SET) {
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $kSET ] = $SET;
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $kSET ]['READWRITE'] = ($SET['READWRITE'])??33;
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['PARENT']['D'][ $SET['PARENT_ID']?: NULL ]['CHILD']['D'][ $kSET ] = &$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $kSET ];
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['CHILD']['D'][ $kSET ] = &$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['PARENT']['D'][ $SET['PARENT_ID']?: NULL ]['CHILD']['D'][ $kSET ];
		}
		
		#Aus DB
		#Hat keine Struktur! nur unterteilung ist Platform Einstellung oder global
		$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']))? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id AS ID, parent_id AS PARENT_ID, platform_id AS PLATFORM_ID, active AS ACTIVE, value AS VALUE, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
			FROM wp_setting
			WHERE platform_id = '{$this->platform_id}' {$W}
		");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			if($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['ID'] ]['READWRITE']>32) {
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['ID'] ] = array_replace_recursive((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['ID'] ],$a);
			}
		}

###echo "<pre>";print_R($D['PLATFORM']['D'][ $this->platform_id ]['SETTING'] );echo "</pre>";
		
	
	}

	function set_setting(&$D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'] AS $kSET => $SET) {
			if($SET['ACTIVE'] != -2) {
					$IU_SET .= (($IU_SET)?',':'')."('{$kSET}','{$this->platform_id}'";
					$IU_SET .= (isset($SET['PARENT_ID']))? ",'{$SET['PARENT_ID']}'":",NULL";
					$IU_SET .= (isset($SET['ACTIVE']))? ",'{$SET['ACTIVE']}'":",NULL";
					$IU_SET .= (isset($SET['VALUE']))? ",'{$SET['VALUE']}'":",NULL";
					$IU_SET .= ")";
			}
			else {
				$D_SET .= (($D_SET)?',':'')."'{$kSET}'";
			}
		}	
			if($IU_SET) {
				$this->SQL->query("INSERT INTO wp_setting (id, platform_id, parent_id, active, value) VALUES {$IU_SET} 
							ON CONFLICT(id, platform_id) DO UPDATE SET
								platform_id =	CASE WHEN excluded.platform_id IS NOT NULL		AND ifnull(platform_id,'') <> excluded.platform_id		THEN excluded.platform_id ELSE platform_id END,
								parent_id =		CASE WHEN excluded.parent_id IS NOT NULL		AND ifnull(parent_id,'') <> excluded.parent_id			THEN excluded.parent_id ELSE parent_id END,
								active =		CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'')  <> excluded.active				THEN excluded.active ELSE active END,
								value =			CASE WHEN excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value					THEN excluded.value ELSE value END,
								utimestamp =	CASE WHEN 
														excluded.parent_id IS NOT NULL			AND ifnull(parent_id ,'')<> excluded.parent_id
														OR excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active
														OR excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value
												THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
			}
			if($D_SET) {
				$this->SQL->query("DELETE FROM wp_setting WHERE id IN ({$D_SET}) AND platform_id = '{$this->platform_id}'");
			}

	}

	function get_statistic(&$D=null)
	{
		#$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']))? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id AS ID, platform_id AS PLATFORM_ID, date AS DATE, active AS ACTIVE, value AS VALUE, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
			FROM wp_statistic
			WHERE platform_id = '{$this->platform_id}' {$W}
		");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			#$D['PLATFORM']['D'][ $this->platform_id ]['STATISTIC']['D'][ $a['ID'] ]['DATE']['D'][$a['DATE'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['STATISTIC']['D'][ $a['ID'] ]['DATE']['D'][$a['DATE'] ] = unserialize($a['VALUE']);
		}
	}

	function set_statistic(&$D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['STATISTIC']['D'] AS $kSET => $SET) {
			foreach((array)$SET['DATE']['D'] AS $kDATE => $DATE) {
				
				if($SET['ACTIVE'] != -2) {
						$IU_SET .= (($IU_SET)?',':'')."('{$kSET}','{$this->platform_id}','{$kDATE}'";
						$IU_SET .= (isset($DATE['ACTIVE']))? ",'{$DATE['ACTIVE']}'":",NULL";
						$IU_SET .= (isset($DATE))? ",'".serialize($DATE)."'":",NULL";
						$IU_SET .= ")";
				}
				else {
					$D_SET .= (($D_SET)?',':'')."'{$kSET}'";
				}
			}
		}
			if($IU_SET) {
				$this->SQL->query("INSERT INTO wp_statistic (id, platform_id, date, active, value) VALUES {$IU_SET} 
							ON CONFLICT(id, platform_id, date) DO UPDATE SET
								active =		CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'')  <> excluded.active				THEN excluded.active ELSE active END,
								value =			CASE WHEN excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value					THEN excluded.value ELSE value END,
								utimestamp =	CASE WHEN 
														excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active
														OR excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value
												THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
			}
			if($D_SET) {
				$this->SQL->query("DELETE FROM wp_statistic WHERE id IN ({$D_SET}) AND platform_id = '{$this->platform_id}'");
			}
			
		
	}
}