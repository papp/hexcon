<?php
#ToDo: Veraltet!! -> löschen
class cache
{
	function __construct()
	{
		global $SQL;
		$this->SQL = $SQL;
		#$this->SET = $this->get_setting();
	}
	
	function get_categorie(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID'					=> "c.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['ID']}')",
			'ID:IN'					=> "c.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['ID:IN']}')",
			'PARENT_ID'				=> "c.parent_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']['PARENT_ID']}')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['W']);

		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['STEP'])? " LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['START']},{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['L']['STEPP']} ":'';
		$qry = $this->SQL->query("SELECT id, parent_id, active, title, breadcrumb_id, breadcrumb_title, itimestamp, utimestamp,
							(SELECT COUNT(*) FROM wp_categorie b WHERE b.platform_id = c.platform_id AND b.parent_id = c.id) count_cat,
							(SELECT COUNT(*) FROM wp_article_attribute ca WHERE ca.platform_id = c.platform_id AND ca.attribute_id = 'CategorieId' AND ca.value = c.id) count_art
							FROM wp_categorie c
							WHERE platform_id = '{$this->platform_id}' 
							{$W} ORDER BY title {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ] = [
				'PARENT_ID'					=> $a['parent_id'],
				'BREADCRUMB_ID'				=> $a['breadcrumb_id'],
				'ACTIVE'					=> $a['active'],
				'COUNT'						=> $a['count_cat'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['LANGUAGE']['D']['DE'] = [
				'TITLE'				=> $a['title'],
				'BREADCRUMB_TITLE'	=> $a['breadcrumb_title'],
			];
			
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['PARENT']['D'][ $a['parent_id'] ]['CHILD']['D'][ $a['id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ];

			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['ARTICLE']['COUNT'] = $a['count_art'];
		}
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D']));
		#REFERENZ START =================
		$qry = $this->SQL->query("SELECT from_categorie_id, to_platform_id, to_categorie_id, active, utimestamp, itimestamp
						FROM wp_categorie_to_categorie c2c
						WHERE from_platform_id = '{$this->platform_id}'
						
						AND from_categorie_id IN ('{$ID}')");
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['CATEGORIE']['D'][ $a['to_categorie_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
		}
		#REFERENZ ENDE =================
		# ['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]
		/*
		$k = array_keys($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D']);
		for($i=0;$i < count($k); $i++)
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['id'] ]['ARTICLE']['COUNT'] += $a['count_art'];
		}
		*/
		
		#Categorie-ATTRIBUTE
		$qry = $this->SQL->query("SELECT attribute_id, categorie_id, platform_id, active ACTIVE, required REQUIRED, value VALUE, value_option VALUE_OPTION, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
						FROM wp_attribute_to_categorie
						WHERE platform_id = '{$this->platform_id}'
						AND categorie_id IN ('{$ID}')");
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['categorie_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['ATTRIBUTE']['D'][ $a['attribute_id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['categorie_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ];
		}
		#Categorie-ATTRIBUTE Languge
		#ToDo: Attribute für Multilanguage muss realisiert werden, wegen value_option


		#ATTRIBUTE Platform zuordnung
		$qry = $this->SQL->query("SELECT from_platform_id, from_categorie_id, from_attribute_id, to_platform_id, to_attribute_id, active ACTIVE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
							FROM wp_attribute_to_platform_attribute 
							WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id IN ('{$ID}')
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			#$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['ATTRIBUTE']['D'][ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ]['ATTRIBUTE']['D'][ $a['to_attribute_id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['ATTRIBUTE']['D'][ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ] = $a;
		}

		#CATEGORIE_TO_CATEGORIE START =====================
		
		$qry = $this->SQL->query("SELECT from_platform_id,to_platform_id,from_categorie_id,to_categorie_id,active,itimestamp,utimestamp
								FROM wp_categorie_to_categorie
								WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id IN ('{$ID}') ");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ]['CATEGORIE']['D'][ $a['to_categorie_id'] ] = [
				'ACTIVE'		=> $a['active'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'LANGUAGE'		=> $PL['PLATFORM']['D'][ $a['from_platform_id'] ]['CATEGORIE']['D'][ $a['from_categorie_id'] ]['LANGUAGE'],
			];
		}
		#CATEGORIE_TO_CATEGORIE END =======================
	}
	
	function set_categorie(&$D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'] AS $kCAT => $CAT)
		{
			if($CAT['ACTIVE'] != -2)
			{
				$IU_CAT .= (($IU_CAT)?',':'')."('{$kCAT}','{$this->platform_id}','DE'";
				$IU_CAT .= (isset($CAT['PARENT_ID']))? ",'{$CAT['PARENT_ID']}'":",NULL";
				$IU_CAT .= (isset($CAT['ACTIVE']))? ",'{$CAT['ACTIVE']}'":",NULL";
				$IU_CAT .= (isset($CAT['LANGUAGE']['D']['DE']['TITLE']))? ",'".$this->SQL->escapeString($CAT['LANGUAGE']['D']['DE']['TITLE'])."'":",NULL";
				$IU_CAT .= (isset($CAT['BREADCRUMB_ID']))? ",'".$this->SQL->escapeString ($CAT['BREADCRUMB_ID'])."'":",NULL";
				$IU_CAT .= (isset($CAT['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE']))? ",'".$this->SQL->escapeString ($CAT['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE'])."'":",NULL";
				$IU_CAT .= ")";
				
				foreach((array)$CAT['ATTRIBUTE']['D'] AS $kATT => $ATT)
				{
					if($ATT['ACTIVE'] != -2)
					{
						$IU_ATT .= (($IU_ATT)?',':'')."('{$kATT}','{$kCAT}','{$this->platform_id}'";
						$IU_ATT .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
						$IU_ATT .= (isset($ATT['REQUIRED']))? ",'{$ATT['REQUIRED']}'":",NULL";
						$IU_ATT .= (isset($ATT['VALUE']))? ",'{$ATT['VALUE']}'":",NULL";#ToDo: Select Auswahl für Language Attribute muss implementiert werden!
						$IU_ATT .= (isset($ATT['VALUE_OPTION']))? ",'{$ATT['VALUE_OPTION']}'":",NULL";
						$IU_ATT .= ")";
						
						foreach((array)$ATT['TO']['PLATFORM']['D'] AS $ktoPLA => $toPLA)
						{
							#foreach($toPLA['ATTRIBUTE']['D'] AS $ktoATT => $toATT)
							#{
								if($toATT['ACTIVE'] != -2)
								{
									$IU_to_ATT .= (($IU_to_ATT)?',':'')."('{$this->platform_id}','{$kCAT}','{$kATT}','{$ktoPLA}'";
									$IU_to_ATT .= (isset($toPLA['ATTRIBUTE_ID']))? ",'{$toPLA['ATTRIBUTE_ID']}'":",NULL";
									$IU_to_ATT .= (isset($toPLA['ACTIVE']))? ",'{$toPLA['ACTIVE']}'":",NULL";
									$IU_to_ATT .= ")";
								}
								else
								{
									$D_to_ATT .= (($D_to_ATT)?',':'')."'{$kCAT}{$kATT}{$ktoPLA}'";	
								}
							#}
						}
					}
					else
					{
						$D_ATT .= (($D_ATT)?',':'')."'{$kATT}{$kCAT}'";
					}
				}
				
				
				#CATEGORIE_TO_CATEGORIE START =====================
				foreach((array)$CAT['TO']['PLATFORM']['D'] AS $kPLA => $PLA)
				{
					foreach((array)$PLA['CATEGORIE']['D'] AS $kToCAT => $ToCAT)
					{
						if($ToCAT['ACTIVE'] != -2)
						{
							$IU_CTC .= (($IU_CTC)?',':'')."('{$this->platform_id}','{$kCAT}','{$kPLA}','{$kToCAT}'";
							$IU_CTC .= (isset($ToCAT['ACTIVE']))? ",'{$ToCAT['ACTIVE']}'":",NULL";
							$IU_CTC .= ")";
						}
						else
						{
							$D_CTC .= (($D_CTC)?',':'')."'{$this->platform_id}{$kCAT}{$kPLA}{$kToCAT}'";
						}
					}
				}
				#CATEGORIE_TO_CATEGORIE END =======================
				
				
			}
			else
			{
				$D_CAT .= (($D_CAT)?',':'')."'{$kCAT}'";
			}
		}
		
		if($IU_CAT)
			$this->SQL->query("INSERT INTO wp_categorie (id, platform_id,language_id,parent_id,active,title,breadcrumb_id,breadcrumb_title) VALUES {$IU_CAT} 
						ON CONFLICT(id, language_id, platform_id) DO UPDATE SET
							parent_id =			CASE WHEN excluded.parent_id IS NOT NULL		AND parent_id <> excluded.parent_id					THEN excluded.parent_id ELSE parent_id END,
							active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
							title =				CASE WHEN excluded.title IS NOT NULL			AND title <> excluded.title							THEN excluded.title ELSE title END,
							breadcrumb_id =		CASE WHEN excluded.breadcrumb_id IS NOT NULL	AND breadcrumb_id <> excluded.breadcrumb_id			THEN excluded.breadcrumb_id ELSE breadcrumb_id END,
							breadcrumb_title =	CASE WHEN excluded.breadcrumb_title IS NOT NULL	AND breadcrumb_title <> excluded.breadcrumb_title	THEN excluded.breadcrumb_title ELSE breadcrumb_title END,
							utimestamp =		CASE WHEN
													   excluded.parent_id IS NOT NULL			AND parent_id <> excluded.parent_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.title IS NOT NULL				AND title <> excluded.title
													OR excluded.breadcrumb_id IS NOT NULL		AND breadcrumb_id <> excluded.breadcrumb_id
													OR excluded.breadcrumb_title IS NOT NULL	AND breadcrumb_title <> excluded.breadcrumb_title
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		
		if($IU_ATT)
			$this->SQL->query("INSERT INTO wp_attribute_to_categorie (attribute_id, categorie_id, platform_id, active, required, value, value_option) VALUES {$IU_ATT} 
						ON CONFLICT(attribute_id, categorie_id, platform_id) DO UPDATE SET
							active =			CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active						THEN excluded.active ELSE active END,
							required =			CASE WHEN excluded.required IS NOT NULL			AND ifnull(required,'') <> excluded.required					THEN excluded.required ELSE required END,
							value =				CASE WHEN excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value							THEN excluded.value ELSE value END,
							value_option =		CASE WHEN excluded.value_option IS NOT NULL		AND ifnull(value_option,'') <> excluded.value_option			THEN excluded.value_option ELSE value_option END,
							utimestamp =		CASE WHEN
													   excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active
													OR excluded.required IS NOT NULL			AND ifnull(required,'') <> excluded.required
													OR excluded.value IS NOT NULL				AND ifnull(value,'') <> excluded.value
													OR excluded.value_option IS NOT NULL		AND ifnull(value_option,'') <> excluded.value_option
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						"); 
		
		if($D_CAT)
		{
			$this->SQL->query("DELETE FROM wp_categorie WHERE id IN ({$D_CAT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_categorie WHERE platform_id = '{$this->platform_id}' AND categorie_id NOT IN (SELECT id FROM wp_categorie WHERE platform_id = '{$this->platform_id}')");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_platform_id = '{$this->platform_id}' AND from_categorie_id NOT IN (SELECT id FROM wp_categorie WHERE platform_id = '{$this->platform_id}') ");
		}
		
		if($D_ATT)
		{
			$this->SQL->query("DELETE FROM wp_attribute_to_categorie WHERE (attribute_id || categorie_id) IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_platform_id = '{$this->platform_id}' AND (from_attribute_id || from_platform_id) NOT IN (SELECT (attribute_id || categorie_id) FROM wp_attribute_to_categorie WHERE platform_id = '{$this->platform_id}') ");
		}
		
		if($IU_to_ATT)
			$this->SQL->query("INSERT INTO wp_attribute_to_platform_attribute ( from_platform_id, from_categorie_id, from_attribute_id, to_platform_id, to_attribute_id, active) VALUES {$IU_to_ATT} 
							ON CONFLICT( from_platform_id, from_attribute_id, to_platform_id, from_categorie_id) DO UPDATE SET
								active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
								to_attribute_id =	CASE WHEN excluded.to_attribute_id IS NOT NULL	AND to_attribute_id <> excluded.to_attribute_id		THEN excluded.to_attribute_id ELSE to_attribute_id END,
								utimestamp =		CASE WHEN
														   excluded.active IS NOT NULL				AND active <> excluded.active
														OR excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
			"); 
		if($D_to_ATT)
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE (from_categorie_id || from_attribute_id || to_platform_id) IN ({$D_to_ATT}) AND from_platform_id = '{$this->platform_id}'");
	
		if($IU_CTC)
			$this->SQL->query("INSERT INTO wp_categorie_to_categorie (from_platform_id,from_categorie_id,to_platform_id,to_categorie_id,active) VALUES {$IU_CTC} 
						ON CONFLICT(from_platform_id, from_categorie_id, to_platform_id, to_categorie_id) DO UPDATE SET
							active =			CASE WHEN excluded.active IS NOT NULL			AND active <> excluded.active						THEN excluded.active ELSE active END,
							utimestamp =		CASE WHEN
													   excluded.active IS NOT NULL				AND active <> excluded.active
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($D_CTC)
			$this->SQL->query("DELETE FROM wp_categorie_to_categorie WHERE (from_platform_id || from_categorie_id || to_platform_id || to_categorie_id) IN ({$D_CTC})");
	}
	
	function get_attribute(&$D=null)
	{
		#Individuelle Attribute
		#$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['ID']))? " AND attribute_id IN ({$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['ID']})":'';
		#$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['PARENT_ID']))? " AND parent_id IN ({$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']['PARENT_ID']})":'';
		$W .= CWP::where_interpreter([
			'ID'					=> "attribute_id IN ([ID])",
			'PARENT_ID'		=> "parent_id IN ([PARENT_ID])",
			'CATEGORIE_ID'			=> "id IN (SELECT attribute_id FROM wp_attribute_to_categorie WHERE categorie_id IN ([CATEGORIE_ID]) )",
		],$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['W']);
		
		$qry = $this->SQL->query("SELECT id, parent_id, active AS ACTIVE, type AS TYPE, topin AS TOPIN, i18n AS I18N, sort AS SORT, value AS VALUE, value_option AS VALUE_OPTION, itimestamp*1 ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute att
							WHERE platform_id = '{$this->platform_id}'  {$W}
							ORDER BY parent_id, sort, (SELECT title FROM wp_attribute_language WHERE language_id = 'DE' AND att.id = attribute_id AND platform_id = att.platform_id)
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['id'] ] = $a;
			$ATT[ $a['id'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $a['id'] ];
		}

		$ID = implode("','",array_keys((array)$ATT));
		$qry = $this->SQL->query("SELECT attribute_id, language_id, active AS ACTIVE, title AS TITLE, value AS VALUE, value_option AS VALUE_OPTION, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute_language
							WHERE platform_id = '{$this->platform_id}' AND attribute_id IN ('{$ID}')
							ORDER BY language_id,title
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$ATT[ $a['attribute_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;
		}
		
		$qry = $this->SQL->query("SELECT from_platform_id, from_attribute_id AS ATTRIBUTE_ID, to_platform_id, to_attribute_id, active AS ACTIVE, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_attribute_to_platform_attribute 
							WHERE from_categorie_id = '' AND from_platform_id = '{$this->platform_id}' AND from_attribute_id IN ('{$ID}')
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$ATT[ $a['from_attribute_id'] ]['TO']['PLATFORM']['D'][ $a['to_platform_id'] ] = $a;
		}
	}
	
	function set_attribute(&$D)
	{
		#foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTETYPE']['D'] AS $kATTT => $ATTT)
		#{
			foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] AS $kATT => $ATT)
			{
				if($ATT['ACTIVE'] != -2)
				{
					$IU_ATT .= (($IU_ATT)?',':'')."('{$kATT}','{$this->platform_id}'";
					$IU_ATT .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
					$IU_ATT .= (isset($ATT['VALUE']))? ",'".$this->SQL->escapeString ($ATT['VALUE'])."'":",NULL";
					$IU_ATT .= (isset($ATT['VALUE_OPTION']))? ",'".$this->SQL->escapeString ($ATT['VALUE_OPTION'])."'":",NULL";
					$IU_ATT .= (isset($ATT['TYPE']))? ",'{$ATT['TYPE']}'":",NULL";
					$IU_ATT .= (isset($ATT['TOPIN']))? ",'{$ATT['TOPIN']}'":",NULL";
					$IU_ATT .= (isset($ATT['I18N']))? ",'{$ATT['I18N']}'":",''";
					$IU_ATT .= (isset($ATT['SORT']))? ",'{$ATT['SORT']}'":",''";
					$IU_ATT .= ")";
					
					foreach((array)$ATT['LANGUAGE']['D'] AS $kLAN => $LAN)
					{
						if($LAN['ACTIVE'] != -2)
						{
							$IU_ATT_LG .= (($IU_ATT_LG)?',':'')."('{$kATT}','{$this->platform_id}','{$kLAN}'";
							$IU_ATT_LG .= (isset($LAN['ACTIVE']))? ",'{$LAN['ACTIVE']}'":",NULL";
							$IU_ATT_LG .= (isset($LAN['TITLE']))? ",'".$this->SQL->escapeString ($LAN['TITLE'])."'":",NULL";
							$IU_ATT_LG .= (isset($LAN['VALUE']))? ",'".$this->SQL->escapeString ($LAN['VALUE'])."'":",NULL";
							$IU_ATT_LG .= (isset($LAN['VALUE_OPTION']))? ",'".$this->SQL->escapeString ($LAN['VALUE_OPTION'])."'":",NULL";
							$IU_ATT_LG .= ")";
						}
						else
							$D_ATT_LG .= (($D_ATT_LG)?',':'')."'{$kATT}{$kLAN}'";
					}
					
					#Att_to_Platform_att START ======================
					foreach((array)$ATT['TO']['PLATFORM']['D'] AS $kPAL => $PLA)
					{
							if($PLA['ACTIVE'] != -2)
							{
								$IU_ATT2 .= (($IU_ATT2)?',':'')."('{$this->platform_id}','{$kATT}','{$kPAL}'";
								$IU_ATT2 .= (isset($PLA['ACTIVE']))? ",'{$PLA['ACTIVE']}'":",NULL";
								$IU_ATT2 .= (isset($PLA['ATTRIBUTE_ID']))? ",'{$PLA['ATTRIBUTE_ID']}'":",NULL";
								$IU_ATT2 .= ")";
							}
							else
								$D_ATT2 .= (($D_ATT2)?',':'')."'{$kATT}{$kPAL}'";
					}
					#Att_to_Platform_att ENDE ======================
				}
				else
				{
					$D_ATT .= (($D_ATT)?',':'')."'{$kATT}'";
				}
			}
		#}
		if($IU_ATT)
			$this->SQL->query("INSERT INTO wp_attribute (id, platform_id, parent_id, active, value, value_option, type, topin, i18n,sort) VALUES {$IU_ATT} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
						parent_id =	CASE WHEN excluded.parent_id IS NOT NULL		AND parent_id <> excluded.parent_id	THEN excluded.parent_id ELSE parent_id END,
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active		THEN excluded.active ELSE active END,
						value =				CASE WHEN excluded.value IS NOT NULL				AND value <> excluded.value			THEN excluded.value ELSE value END,
						value_option =		CASE WHEN excluded.value_option IS NOT NULL			AND value <> excluded.value_option	THEN excluded.value_option ELSE value_option END,
						type =				CASE WHEN excluded.type IS NOT NULL					AND type <> excluded.type			THEN excluded.type ELSE type END,
						topin =				CASE WHEN excluded.topin IS NOT NULL				AND topin <> excluded.topin			THEN excluded.topin ELSE topin END,
						i18n =				CASE WHEN excluded.i18n IS NOT NULL					AND i18n <> excluded.i18n			THEN excluded.i18n ELSE i18n END,
						sort =				CASE WHEN excluded.sort IS NOT NULL					AND sort <> excluded.sort			THEN excluded.sort ELSE sort END,
						utimestamp =		CASE WHEN 
													   excluded.parent_id IS NOT NULL	AND parent_id <> excluded.parent_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.value IS NOT NULL				AND value <> excluded.value
													OR excluded.value_option IS NOT NULL		AND value_option <> excluded.value_option
													OR excluded.type IS NOT NULL				AND type <> excluded.type
													OR excluded.topin IS NOT NULL				AND topin <> excluded.topin
													OR excluded.i18n IS NOT NULL				AND i18n <> excluded.i18n
													OR excluded.sort IS NOT NULL				AND sort <> excluded.sort
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");

		if($IU_ATT_LG)
			$this->SQL->query("INSERT INTO wp_attribute_language (attribute_id, platform_id, language_id, active, title, value, value_option) VALUES {$IU_ATT_LG} 
						ON CONFLICT(attribute_id, platform_id, language_id) DO UPDATE SET
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active		THEN excluded.active ELSE active END,
						title =				CASE WHEN excluded.title IS NOT NULL				AND title <> excluded.title			THEN excluded.title ELSE title END,
						value =				CASE WHEN excluded.value IS NOT NULL				AND value <> excluded.value			THEN excluded.value ELSE value END,
						value_option =		CASE WHEN excluded.value_option IS NOT NULL			AND value <> excluded.value_option	THEN excluded.value_option ELSE value_option END,
						utimestamp =		CASE WHEN 
													   excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.title IS NOT NULL				AND title <> excluded.title
													OR excluded.value IS NOT NULL				AND value <> excluded.value
													OR excluded.value_option IS NOT NULL		AND value <> excluded.value_option
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
						
		if($IU_ATT2)
			$this->SQL->query("INSERT INTO wp_attribute_to_platform_attribute (from_platform_id,from_attribute_id, to_platform_id, active, to_attribute_id) VALUES {$IU_ATT2} 
				ON CONFLICT(from_platform_id, from_attribute_id, to_platform_id, from_categorie_id) DO UPDATE SET
						active =			CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active						THEN excluded.active ELSE active END,
						to_attribute_id =	CASE WHEN excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id		THEN excluded.to_attribute_id ELSE to_attribute_id END,
						utimestamp =		CASE WHEN 
													   excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.to_attribute_id IS NOT NULL		AND to_attribute_id <> excluded.to_attribute_id
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
											
				");

		if($D_ATT_LG)
			$this->SQL->query("DELETE FROM wp_attribute_language WHERE (attribute_id || language_id) IN ({$D_ATT_LG}) AND platform_id = '{$this->platform_id}'");

		if($D_ATT)
		{
			$this->SQL->query("DELETE FROM wp_attribute WHERE id IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_language WHERE attribute_id IN ({$D_ATT}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_categorie_id = '' AND from_attribute_id IN ({$D_ATT}) AND from_platform_id = '{$this->platform_id}'");
		}

		if($D_ATT2)
			$this->SQL->query("DELETE FROM wp_attribute_to_platform_attribute WHERE from_categorie_id = '' AND (from_attribute_id || to_platform_id) IN ({$D_ATT2}) AND from_platform_id = '{$this->platform_id}'");
	
		#Attribute Korektur i18n, wenn ein Attribute von i18n gewechselt wird dann müssen alle Artikel zuweisungen entsprechend umgestelt werden
		$this->SQL->query("DELETE FROM wp_article_attribute WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = ''
							AND attribute_id || article_id IN (SELECT attribute_id || article_id FROM wp_article_attribute WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = 'DE')
							");#bereinige vorher, falls es Language_id = '' und 'DE' beinhaltet
		$this->SQL->query("UPDATE wp_article_attribute SET language_id = '' WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 0 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}' AND language_id = 'DE'");
		
		###$this->SQL->query("UPDATE wp_article_attribute SET language_id = 'DE' WHERE EXISTS (SELECT 1 FROM wp_attribute WHERE platform_id = '{$this->platform_id}' AND i18n = 1 AND id = wp_article_attribute.attribute_id) AND platform_id = '{$this->platform_id}'");
	}
	
	#Veraltet
	function set_payment($P)
	{
		$kPAY = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['PAYMENT']['D']);
		for($a=0; $a< count($kPAY); $a++)
		{
			$PAYMENT = $D['PLATFORM']['D'][ $this->platform_id ]['PAYMENT']['D'][$kPAY[$a]];
			if($PAYMENT['ACTIVE'] != -2)
			{
				$IU_PAY .= (($IU_PAY)?',':'')."('{$kPAY[$a]}','{$this->platform_id}'";
				$IU_PAY .= (isset($PAYMENT['ACTIVE']))? ",'{$PAYMENT['ACTIVE']}'":",NULL";
				$IU_PAY .= (isset($PAYMENT['TITLE']))? ",'{$PAYMENT['TITLE']}'":",NULL";
				$IU_PAY .= ")";
			}
			else
			{
				$D_PAY .= (($D_PAY)?',':'')."'{$kPAY[$a]}'";
			}
		}
		
		if($IU_PAY)
			$this->SQL->query("INSERT INTO wp_payment (id, active, title) VALUES {$IU_PAY} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_payment.active END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_payment.title END
						");
		if($D_PAY)
			$this->SQL->query("DELETE FROM wp_payment WHERE id IN ({$D_PAY}) AND platform_id = '{$this->platform_id}'");
		return $D;
	}
	
	
	
	function set_order($D)
	{
		$k = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$ORDER = $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$k[$i]];
			if($ORDER['ACTIVE'] != -2)
			{
				#SET_CUSTOMER START ========================
				if($ORDER['CUSTOMER_ID'])
				$D['PLATFORM']['D'][ $this->platform_id ]['CUSTOMER']['D'][$ORDER['CUSTOMER_ID']] = [
					'MESSAGE_GROUP_ID'=> $ORDER['MESSAGE_GROUP_ID'],
					'NICKNAME'		=> $ORDER['CUSTOMER_NICKNAME'],
					'EMAIL'			=> $ORDER['CUSTOMER_EMAIL'],
					'COMPANY'		=> $ORDER['BILLING']['COMPANY'],
					'NAME'			=> $ORDER['BILLING']['NAME'],
					'FNAME'			=> $ORDER['BILLING']['FNAME'],
					'STREET'		=> $ORDER['BILLING']['STREET'],
					'STREET_NO'		=> $ORDER['BILLING']['STREET_NO'],
					'ZIP'			=> $ORDER['BILLING']['ZIP'],
					'CITY'			=> $ORDER['BILLING']['CITY'],
					'COUNTRY_ID'	=> $ORDER['BILLING']['COUNTRY_ID'],
					'ADDITION'		=> $ORDER['BILLING']['ADDITION'],
				];
				#SET_CUSTOMER END   ========================
				
				$IU_ORDER .= (($IU_ORDER)?',':'')."('{$k[$i]}','{$this->platform_id}'";
				$IU_ORDER .= (isset($ORDER['PAYMENT_ID']))? ",'{$ORDER['PAYMENT_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['ACTIVE']))? ",'{$ORDER['ACTIVE']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['STATUS']))? ",'{$ORDER['STATUS']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_ID']))? ",'{$ORDER['CUSTOMER_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_NICKNAME']))? ",'".$this->SQL->escapeString ($ORDER['CUSTOMER_NICKNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_EMAIL']))? ",'{$ORDER['CUSTOMER_EMAIL']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['PAID']))? ",'{$ORDER['PAID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COMPANY']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['COMPANY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['NAME']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['FNAME']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET_NO']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['STREET_NO'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ZIP']))? ",'{$ORDER['BILLING']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['CITY']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COUNTRY_ID']))? ",'{$ORDER['BILLING']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ADDITION']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['ADDITION'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['PHONE']))? ",'".$this->SQL->escapeString ($ORDER['BILLING']['PHONE'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COMPANY']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['COMPANY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['NAME']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['FNAME']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET_NO']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['STREET_NO'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ZIP']))? ",'{$ORDER['DELIVERY']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['CITY']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COUNTRY_ID']))? ",'{$ORDER['DELIVERY']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ADDITION']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['ADDITION'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['PHONE']))? ",'".$this->SQL->escapeString ($ORDER['DELIVERY']['PHONE'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['WAREHOUSE_ID']))? ",'{$ORDER['WAREHOUSE_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['SHIPPING_ID']))? ",'{$ORDER['SHIPPING_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['COMMENT']))? ",'".$this->SQL->escapeString ($ORDER['COMMENT'])."'":",NULL";
				$IU_ORDER .= ")";
				
				foreach((array)$ORDER['ARTICLE']['D'] AS $kA => $vA)
				{
					if($vA['ACTIVE'] != -2)
					{
						$IU_ARTICLE .= (($IU_ARTICLE)?',':'')."('{$kA}','{$k[$i]}','{$this->platform_id}'";
						$IU_ARTICLE .= (isset($vA['ACTIVE']))? ",'{$vA['ACTIVE']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['NUMBER']))? ",'{$vA['NUMBER']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['TITLE']))? ",'".$this->SQL->escapeString ($vA['TITLE'])."'":",NULL";
						$IU_ARTICLE .= (isset($vA['STOCK']))? ",'{$vA['STOCK']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['WEIGHT']))? ",'{$vA['WEIGHT']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['PRICE']))? ",'{$vA['PRICE']}'":",NULL";
						$IU_ARTICLE .= (isset($vA['VAT']))? ",'{$vA['VAT']}'":",NULL";
						$IU_ARTICLE .= ")";
					}
					else
					{
						$D_ARTICLE .= (($D_ARTICLE)?',':'')."'{$kA}{$k[$i]}'";
					}
				}
			}
			else
			{
				$D_ORDER .= (($D_ORDER)?',':'')."'{$k[$i]}'";
			}
		}
		
		
		if($IU_ORDER)
			$this->SQL->query("INSERT INTO wp_order (id,platform_id, payment_id, active, status, customer_id, customer_nickname, customer_email, paid
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition,billing_phone
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition,delivery_phone
								,warehouse_id,shipping_id,comment) VALUES {$IU_ORDER} 

						ON CONFLICT(id, platform_id) DO UPDATE SET
							payment_id =			CASE WHEN excluded.payment_id IS NOT NULL			AND payment_id <> excluded.payment_id					THEN excluded.payment_id ELSE payment_id END,
							active =				CASE WHEN excluded.active IS NOT NULL				AND active <> excluded.active							THEN excluded.active ELSE active END,
							status =				CASE WHEN excluded.status IS NOT NULL				AND status <> excluded.status							THEN excluded.status ELSE status END,
							customer_id =			CASE WHEN excluded.customer_id IS NOT NULL			AND customer_id <> excluded.customer_id					THEN excluded.customer_id ELSE customer_id END,
							customer_nickname =		CASE WHEN excluded.customer_nickname IS NOT NULL	AND customer_nickname <> excluded.customer_nickname		THEN excluded.customer_nickname ELSE customer_nickname END,
							customer_email =		CASE WHEN excluded.customer_email IS NOT NULL		AND customer_email <> excluded.customer_email			THEN excluded.customer_email ELSE customer_email END,
							paid =					CASE WHEN excluded.paid IS NOT NULL					AND paid <> excluded.paid								THEN excluded.paid ELSE paid END,
							billing_company =		CASE WHEN excluded.billing_company IS NOT NULL		AND billing_company <> excluded.billing_company			THEN excluded.billing_company ELSE billing_company END,
							billing_name =			CASE WHEN excluded.billing_name IS NOT NULL			AND billing_name <> excluded.billing_name				THEN excluded.billing_name ELSE billing_name END,
							billing_fname =			CASE WHEN excluded.billing_fname IS NOT NULL		AND billing_fname <> excluded.billing_fname				THEN excluded.billing_fname ELSE billing_fname END,
							billing_street =		CASE WHEN excluded.billing_street IS NOT NULL		AND billing_street <> excluded.billing_street			THEN excluded.billing_street ELSE billing_street END,
							billing_street_no =		CASE WHEN excluded.billing_street_no IS NOT NULL	AND billing_street_no <> excluded.billing_street_no		THEN excluded.billing_street_no ELSE billing_street_no END,
							billing_zip =			CASE WHEN excluded.billing_zip IS NOT NULL			AND billing_zip <> excluded.billing_zip					THEN excluded.billing_zip ELSE billing_zip END,
							billing_city =			CASE WHEN excluded.billing_city IS NOT NULL			AND billing_city <> excluded.billing_city				THEN excluded.billing_city ELSE billing_city END,
							billing_country_id =	CASE WHEN excluded.billing_country_id IS NOT NULL	AND billing_country_id <> excluded.billing_country_id	THEN excluded.billing_country_id ELSE billing_country_id END,
							billing_addition =		CASE WHEN excluded.billing_addition IS NOT NULL		AND billing_addition <> excluded.billing_addition		THEN excluded.billing_addition ELSE billing_addition END,
							billing_phone =			CASE WHEN excluded.billing_phone IS NOT NULL		AND billing_phone <> excluded.billing_phone				THEN excluded.billing_phone ELSE billing_phone END,
							delivery_company =		CASE WHEN excluded.delivery_company IS NOT NULL		AND delivery_company <> excluded.delivery_company		THEN excluded.delivery_company ELSE delivery_company END,
							delivery_name =			CASE WHEN excluded.delivery_name IS NOT NULL		AND delivery_name <> excluded.delivery_name				THEN excluded.delivery_name ELSE delivery_name END,
							delivery_fname =		CASE WHEN excluded.delivery_fname IS NOT NULL		AND delivery_fname <> excluded.delivery_fname			THEN excluded.delivery_fname ELSE delivery_fname END,
							delivery_street =		CASE WHEN excluded.delivery_street IS NOT NULL		AND delivery_street <> excluded.delivery_street			THEN excluded.delivery_street ELSE delivery_street END,
							delivery_street_no =	CASE WHEN excluded.delivery_street_no IS NOT NULL	AND delivery_street_no <> excluded.delivery_street_no	THEN excluded.delivery_street_no ELSE delivery_street_no END,
							delivery_zip =			CASE WHEN excluded.delivery_zip IS NOT NULL			AND delivery_zip <> excluded.delivery_zip				THEN excluded.delivery_zip ELSE delivery_zip END,
							delivery_city =			CASE WHEN excluded.delivery_city IS NOT NULL		AND delivery_city <> excluded.delivery_city				THEN excluded.delivery_city ELSE delivery_city END,
							delivery_country_id =	CASE WHEN excluded.delivery_country_id IS NOT NULL	AND delivery_country_id <> excluded.delivery_country_id	THEN excluded.delivery_country_id ELSE delivery_country_id END,
							delivery_addition =		CASE WHEN excluded.delivery_addition IS NOT NULL	AND delivery_addition <> excluded.delivery_addition		THEN excluded.delivery_addition ELSE delivery_addition END,
							delivery_phone =		CASE WHEN excluded.delivery_phone IS NOT NULL		AND delivery_phone <> excluded.delivery_phone			THEN excluded.delivery_phone ELSE delivery_phone END,
							warehouse_id =			CASE WHEN excluded.warehouse_id IS NOT NULL			AND warehouse_id <> excluded.warehouse_id				THEN excluded.warehouse_id ELSE warehouse_id END,
							shipping_id =			CASE WHEN excluded.shipping_id IS NOT NULL			AND shipping_id <> excluded.shipping_id					THEN excluded.shipping_id ELSE shipping_id END,
							comment =				CASE WHEN excluded.comment IS NOT NULL				AND comment <> excluded.comment							THEN excluded.comment ELSE comment END,
							utimestamp =		CASE WHEN 
													   excluded.payment_id IS NOT NULL			AND payment_id <> excluded.payment_id
													OR excluded.active IS NOT NULL				AND active <> excluded.active
													OR excluded.status IS NOT NULL				AND status <> excluded.status
													OR excluded.customer_id IS NOT NULL			AND customer_id <> excluded.customer_id
													OR excluded.customer_nickname IS NOT NULL	AND customer_nickname <> excluded.customer_nickname
													OR excluded.customer_email IS NOT NULL		AND customer_email <> excluded.customer_email
													OR excluded.paid IS NOT NULL				AND paid <> excluded.paid
													OR excluded.billing_company IS NOT NULL		AND billing_company <> excluded.billing_company
													OR excluded.billing_name IS NOT NULL		AND billing_name <> excluded.billing_name
													OR excluded.billing_fname IS NOT NULL		AND billing_fname <> excluded.billing_fname
													OR excluded.billing_street IS NOT NULL		AND billing_street <> excluded.billing_street
													OR excluded.billing_street_no IS NOT NULL	AND billing_street_no <> excluded.billing_street_no
													OR excluded.billing_zip IS NOT NULL			AND billing_zip <> excluded.billing_zip
													OR excluded.billing_city IS NOT NULL		AND billing_city <> excluded.billing_city
													OR excluded.billing_country_id IS NOT NULL	AND billing_country_id <> excluded.billing_country_id
													OR excluded.billing_addition IS NOT NULL	AND billing_addition <> excluded.billing_addition
													OR excluded.billing_phone IS NOT NULL		AND billing_phone <> excluded.billing_phone
													OR excluded.delivery_company IS NOT NULL	AND delivery_company <> excluded.delivery_company
													OR excluded.delivery_name IS NOT NULL		AND delivery_name <> excluded.delivery_name
													OR excluded.delivery_fname IS NOT NULL		AND delivery_fname <> excluded.delivery_fname
													OR excluded.delivery_street IS NOT NULL		AND delivery_street <> excluded.delivery_street
													OR excluded.delivery_street_no IS NOT NULL	AND delivery_street_no <> excluded.delivery_street_no
													OR excluded.delivery_zip IS NOT NULL		AND delivery_zip <> excluded.delivery_zip
													OR excluded.delivery_city IS NOT NULL		AND delivery_city <> excluded.delivery_city
													OR excluded.delivery_country_id IS NOT NULL	AND delivery_country_id <> excluded.delivery_country_id
													OR excluded.delivery_addition IS NOT NULL	AND delivery_addition <> excluded.delivery_addition
													OR excluded.delivery_phone IS NOT NULL		AND delivery_phone <> excluded.delivery_phone
													OR excluded.warehouse_id IS NOT NULL		AND warehouse_id <> excluded.warehouse_id
													OR excluded.shipping_id IS NOT NULL			AND shipping_id <> excluded.shipping_id
													OR excluded.comment IS NOT NULL				AND comment <> excluded.comment
										THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($IU_ARTICLE)
			$this->SQL->query("INSERT INTO wp_order_article (id,order_id,platform_id,active,number,title,stock,weight,price,vat) VALUES {$IU_ARTICLE} 
						ON CONFLICT(id, order_id, platform_id) DO UPDATE SET
							order_id =		CASE WHEN excluded.order_id IS NOT NULL		AND order_id <> excluded.order_id	THEN excluded.order_id ELSE order_id END,
							active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active		THEN excluded.active ELSE active END,
							number =		CASE WHEN excluded.number IS NOT NULL		AND number <> excluded.number		THEN excluded.number ELSE number END,
							title =			CASE WHEN excluded.title IS NOT NULL		AND title <> excluded.title			THEN excluded.title ELSE title END,
							stock =			CASE WHEN excluded.stock IS NOT NULL		AND stock <> excluded.stock			THEN excluded.stock ELSE stock END,
							weight =		CASE WHEN excluded.weight IS NOT NULL		AND weight <> excluded.weight		THEN excluded.weight ELSE weight END,
							price =			CASE WHEN excluded.price IS NOT NULL		AND price <> excluded.price			THEN excluded.price ELSE price END,
							vat =			CASE WHEN excluded.vat IS NOT NULL			AND vat <> excluded.vat				THEN excluded.vat ELSE vat END,
							utimestamp =	CASE WHEN
													   excluded.order_id IS NOT NULL	AND order_id <> excluded.order_id
													OR excluded.active IS NOT NULL		AND active <> excluded.active
													OR excluded.number IS NOT NULL		AND number <> excluded.number
													OR excluded.title IS NOT NULL		AND title <> excluded.title
													OR excluded.stock IS NOT NULL		AND stock <> excluded.stock
													OR excluded.weight IS NOT NULL		AND weight <> excluded.weight
													OR excluded.price IS NOT NULL		AND price <> excluded.price
													OR excluded.vat IS NOT NULL			AND vat <> excluded.vat
							THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($D_ORDER)
			$this->SQL->query("DELETE FROM wp_order WHERE id IN ({$D_ORDER})");
		if($D_ARTICLE)
			$this->SQL->query("DELETE FROM wp_order_article WHERE (id || order_id) IN ({$D_ARTICLE})");
		return $D;
	}
	
	
	function get_order(&$D=null)
	{
		
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ITIMESTAMP'])? " AND DATE(itimestamp) LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ITIMESTAMP']}'":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['W']['ID']}')":'';
		#$W .= ($this->platform_id)? " AND platform_id = '{$this->platform_id}'":'';
		$W .= ($D['ORDER']['W']['ID'])? " AND id IN ('{$D['ORDER']['W']['ID']}')":'';

		$O .= ($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['O']['ITIMESTAMP'])?" itimestamp {$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['O']['ITIMESTAMP']},":" itimestamp DESC,";
		$qry = $this->SQL->query("SELECT id,platform_id,payment_id, active, status, customer_id, customer_nickname, customer_email, paid
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition,billing_phone
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition,delivery_phone
								,warehouse_id,shipping_id, comment,itimestamp,utimestamp,
								DATE(itimestamp) date
							FROM wp_order o
							WHERE platform_id = '{$this->platform_id}'
							{$W} ORDER BY {$O} 1 LIMIT 1100");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['platform_id'], #Platform_from_id
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'PAYMENT_ID'			=> $a['payment_id'],
				'DATE'					=> $a['date'],
				'PAID'					=> $a['paid'],
				'BILLING'				=> [
						'COMPANY'		=> $a['billing_company'],
						'NAME'			=> $a['billing_name'],
						'FNAME'			=> $a['billing_fname'],
						'STREET'		=> $a['billing_street'],
						'STREET_NO'		=> $a['billing_street_no'],
						'ZIP'			=> $a['billing_zip'],
						'CITY'			=> $a['billing_city'],
						'COUNTRY_ID'	=> $a['billing_country_id'],
						'ADDITION'		=> $a['billing_addition'],
						'PHONE'			=> $a['billing_phone'],
				],
				'DELIVERY'				=> [
						'COMPANY'		=> $a['delivery_company'],	
						'NAME'			=> $a['delivery_name'],
						'FNAME'			=> $a['delivery_fname'],
						'STREET'		=> $a['delivery_street'],
						'STREET_NO'		=> $a['delivery_street_no'],
						'ZIP'			=> $a['delivery_zip'],
						'CITY'			=> $a['delivery_city'],
						'COUNTRY_ID'	=> $a['delivery_country_id'],
						'ADDITION'		=> $a['delivery_addition'],
						'PHONE'			=> $a['delivery_phone'],
				],
				'WAREHOUSE_ID'			=> $a['warehouse_id'],
				'SHIPPING_ID'			=> $a['shipping_id'],
				'COMMENT'				=> $a['comment'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
		}
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']));
		$qry = $this->SQL->query("SELECT id,platform_id,order_id,active,title,stock,price,vat,itimestamp,utimestamp
							FROM wp_order_article
							WHERE platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}') ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['from_platform_id'],
				'PARENT_ID'				=> $a['parent_id'],
				#'ARTICLE_ID'			=> $a['from_article_id'],
				#'PARENT_ID'				=> $a['from_parent_id'],
				'ACTIVE'				=> $a['active'],
				#'NUMBER'				=> $a['number'],
				'TITLE'					=> $a['title'],
				'STOCK'					=> $a['stock'],
				#'WEIGHT'				=> $a['weight'],
				'PRICE'					=> $a['price'],
				'VAT'					=> $a['vat'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
		
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
		}
		
		#Versand wp_delivery
		$qry = $this->SQL->query("SELECT id,platform_id,order_id, active ACTIVE, status STATUS, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_delivery
							WHERE platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}') ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $a['platform_id'] ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['D'][ $a['id'] ] = $a;
		}
	}
	
	#Veraltet
	function get_language(&$D=null)
	{/*
		$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['W']['ID']))? " AND pl.language_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, pl.active, title, pl.itimestamp, pl.utimestamp 
							FROM wp_language l, wp_platform_language pl
							WHERE l.id = pl.language_id
							AND	pl.platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ $a['id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		*/
		#return $D;
	}
	#Veraltet
	function set_language($D)
	{
		$kLAN = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D']);
		for($a=0; $a< count($kLAN); $a++)
		{
			$LANGUAGE = $D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][$kLAN[$a]];
			if($LANGUAGE['ACTIVE'] != -2)
			{
				$IU_LAN .= (($IU_LAN)?',':'')."('{$kLAN[$a]}','{$this->platform_id}'";
				$IU_LAN .= (isset($LANGUAGE['ACTIVE']))? ",'{$LANGUAGE['ACTIVE']}'":",NULL";
				$IU_LAN .= ")";
			}
			else
			{
				$D_LAN .= (($D_LAN)?',':'')."'{$kLAN[$a]}'";
			}
		}
		
		if($IU_LAN)
			$this->SQL->query("INSERT INTO wp_platform_language (language_id,platform_id,active) VALUES {$IU_LAN} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_platform_language.active END
						");
		if($D_LAN)
			$this->SQL->query("DELETE FROM wp_platform_language WHERE id IN ({$D_LAN}) AND platform_id = '{$this->platform_id}'");
		return $D;
	}
	
	
	
	function get_setting(&$D=null)
	{
		/*
		$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']))? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['ID']}')":'';
		$W .= (isset($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['GROUP_ID']))? " AND group_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['W']['GROUP_ID']}')":'';
		$qry = $this->SQL->query("SELECT id, parent_id, group_id, active, title, type, value, value_option, itimestamp, utimestamp 
							FROM wp_setting 
							WHERE parent_id = '' AND platform_id = '{$this->platform_id}' {$W}
							ORDER BY title");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			if(!$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['id'] ])
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['id'] ] = [
					'GROUP_ID'					=> $a['group_id'],
					'ACTIVE'					=> $a['active'],
					'TITLE'						=> $a['title'],
					'TYPE'						=> $a['type'],
					'VALUE'						=> $a['value'],
					'OPTION'					=> $a['value_option'],
					'ITIMESTAMP'				=> $a['itimestamp'],
					'UTIMESTAMP'				=> $a['utimestamp'],
				];
			if(!$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['GROUP']['D'][ $a['group_id'] ])
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['GROUP']['D'][ $a['group_id'] ] = [
					'TITLE'	=> $a['group_id'],
				];
				
		}

		
		#VARIANTE START ==================
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']));
		$qry = $this->SQL->query("SELECT id, parent_id, group_id, active, title, type, value, value_option, itimestamp, utimestamp 
							FROM wp_setting 
							WHERE platform_id = '{$this->platform_id}' AND parent_id IN ('{$ID}')
							ORDER BY title");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$a['value_option2'] = $a['value_option'];
			if(strpos($a['value_option'],'SELECT') !== false)#Ist Datenbank Abfrage?
			{
				$a['value_option2'] = str_replace('{platform_id}',$this->platform_id,$a['value_option']);
				$qry2 = $this->SQL->query($a['value_option2']);
				$a['value_option2'] = '';
				while($aa = $qry2->fetchArray(SQLITE3_ASSOC))
				{
					$a['value_option2'] .= (($a['value_option2'])?'|':'')."{$aa['id']}:{$aa['value']}";
				}
			}
				
			
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['id'] ] = 
			array_replace_recursive((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['id'] ],
			(array)[
				'GROUP_ID'					=> $a['group_id'],
				'ACTIVE'					=> $a['active'],
				#'TITLE'						=> $a['title'],
				'TYPE'						=> $a['type'],
				'VALUE'						=> $a['value'],
				'OPTION'					=> $a['value_option2'],
				'RAW_OPTION'				=> $a['value_option'], #Unver�nderte Option z.B. als SELECT Abfrage
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			]);
		}
		#VARIANTE END ====================
		*/
	}
	
	function set_setting(&$D)
	{
		$kSET = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']);
		for($a=0; $a< count($kSET); $a++)
		{
			$SETTING = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][$kSET[$a]];
			if($SETTING['ACTIVE'] != -2)
			{
				$IU_SET .= (($IU_SET)?',':'')."('{$kSET[$a]}','','{$this->account_id}','{$this->platform_id}'";
				$IU_SET .= (isset($SETTING['GROUP_ID']))? ",'{$SETTING['GROUP_ID']}'":",NULL";
				$IU_SET .= (isset($SETTING['ACTIVE']))? ",'{$SETTING['ACTIVE']}'":",NULL";
				$IU_SET .= (isset($SETTING['TITLE']))? ",'{$SETTING['TITLE']}'":",NULL";
				$IU_SET .= (isset($SETTING['TYPE']))? ",'{$SETTING['TYPE']}'":",NULL";
				$IU_SET .= (isset($SETTING['VALUE']))? ",'".$this->SQL->escapeString ($SETTING['VALUE'])."'":",NULL";
				$IU_SET .= (isset($SETTING['OPTION']))? ",'".$this->SQL->escapeString ($SETTING['OPTION'])."'":",NULL";
				$IU_SET .= ")";
				
				#VARIANTE START =================
				$kVAR = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D'][$kSET[$a]]['VARIANTE']['D']);
				for($v=0; $v< count($kVAR); $v++)
				{
					$VARIANTE = $SETTING['VARIANTE']['D'][$kVAR[$v]];
					if($VARIANTE['ACTIVE'] != -2)
					{
						$IU_SET .= (($IU_SET)?',':'')."('{$kVAR[$v]}','{$kSET[$a]}','{$this->account_id}','{$this->platform_id}'";
						$IU_SET .= (isset($VARIANTE['GROUP_ID']))? ",'{$VARIANTE['GROUP_ID']}'":",NULL";
						$IU_SET .= (isset($VARIANTE['ACTIVE']))? ",'{$VARIANTE['ACTIVE']}'":",NULL";
						$IU_SET .= (isset($VARIANTE['TITLE']))? ",'{$VARIANTE['TITLE']}'":",NULL";
						$IU_SET .= (isset($VARIANTE['TYPE']))? ",'{$VARIANTE['TYPE']}'":",NULL";
						$IU_SET .= (isset($VARIANTE['VALUE']))? ",'".$this->SQL->escapeString ($VARIANTE['VALUE'])."'":",NULL";
						$IU_SET .= (isset($VARIANTE['OPTION']))? ",'".$this->SQL->escapeString ($VARIANTE['OPTION'])."'":",NULL";
						$IU_SET .= ")";
					}
					else
						$D_SET .= (($D_SET)?',':'')."'{$kVAR[$v]}'";
				}
				#VARIANTE END ===================
			}
			else
			{
				$D_SET .= (($D_SET)?',':'')."'{$kSET[$a]}'";
			}
		}
		
		if($IU_SET)
		$this->SQL->query("INSERT INTO wp_setting (id, parent_id, account_id, platform_id, group_id, active, title, type, value, value_option) VALUES {$IU_SET} 
						ON DUPLICATE KEY UPDATE 
							group_id = CASE WHEN VALUES(group_id) IS NOT NULL THEN VALUES(group_id) ELSE wp_setting.group_id END,
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_setting.active END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_setting.title END,
							type = CASE WHEN VALUES(type) IS NOT NULL THEN VALUES(type) ELSE wp_setting.type END,
							value = CASE WHEN VALUES(value) IS NOT NULL THEN VALUES(value) ELSE wp_setting.value END,
							value_option = CASE WHEN VALUES(value_option) IS NOT NULL THEN VALUES(value_option) ELSE wp_setting.value_option END
						");
		if($D_SET)
			$this->SQL->query("DELETE FROM wp_setting WHERE id IN ({$D_SET}) AND platform_id = '{$this->platform_id}'");
	}
	
}