<?php
#require_once(__DIR__ . '/selling-partner-api/vendor/autoload.php');

use SellingPartnerApi\Api\SellersV1Api as SellersApi;
use SellingPartnerApi\Api\UploadsV20201101Api;
use SellingPartnerApi\Configuration;
use SellingPartnerApi\Endpoint;

use SellingPartnerApi\Api\FeedsV20210630Api as FeedsApi;
use SellingPartnerApi\FeedType;
use SellingPartnerApi\Model\FeedsV20210630 as Feeds;


class amazon extends platform
{
	static function info(&$D=null)
	{
		$CLASS = get_class();
		$FUNCTION = get_class_methods($CLASS);
		$D['CLASS'][ $CLASS ] = [
			'NAME'		=> 'AmazonSP',
			'VERSION'	=> 1.04,
			'ICON'		=> 'icon.svg',
			'FUNCTION'	=> $FUNCTION,
		];
	}

	function __construct($account_id,$platform_id)
	{
		parent::__construct($account_id, $platform_id);
		global $smarty;
		#global $CCache; 
		$this->smarty = $smarty;
		#$this->CCache = $CCache;
		$smarty->debugging = false;
		$smarty->caching = false;
		
		$this->platform_id = $platform_id;
		$this->account_id = $account_id;
		#cache::__construct();
	}
	
	#Amazon MWS Kontakt: https://sellercentral.amazon.de/gp/mws/contactus.html
	#https://github.com/dmichael/amazon-mws/blob/master/examples/xsd/Product/Lighting.xsd
	#aktueller: 
	#			http://g-ecx.images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/Lighting.xsd
	#			https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/AutoAccessory.xsd
	#			https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/Office.xsd
	#http://g-ecx.images-amazon.com/images/G/01/mwsportal/doc/en_US/products/default.xsd
	#https://searchcode.com/codesearch/view/78756127/
	#http://www.hsn-tsn.info/
	#http://docs.developer.amazonservices.com/en_US/feeds/Feeds_FeedType.html
	#alle xsd https://sellercentral.amazon.com/forums/thread.jspa?threadID=12565
	#error Code Beschreibung: https://docs.wplab.com/article/94-amazon-error-90057
	#Kategorie Listen download in Google Suchen nach "images-na.ssl-images-amazon.com/images/G/01/rainier/help/btg/"
	#https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/btg/de_lighting_browse_tree_guide._TTH_.xls
	
	
	function get_setting(&$D=null) {
		parent::get_setting($D);
		
		$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['READWRITE'] = 30; #nur Leserechte
		$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['READWRITE'] = 30; #nur Leserechte
		switch( $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MarketplaceId']['VALUE']) { 
			case 'A1PA6795UKMFR9':#DE
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'DE';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'DE';
				break;
			case 'A1RKKUPIHCS9HS':#ES
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'ES';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'ES';
				break;
			case 'A13V1IB3VIYZZH':#FR
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'FR';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'FR';
				break;
			case 'APJ6JRA9NG5V4':#IT
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'IT';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'IT';
				break;
			case 'A1805IZSGTT6HS':#NL
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'NL';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'NL';
				break;
			case 'A2NODRKZP88ZB9':#SE
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'SE';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'SE';
				break;
			case 'A1C3SOZRARQ6R3':#PL
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'PL';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'PL';
				break;
			case 'A1F83G8C2ARO7P':#EN
			default:
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AvailableContentLanguage']['VALUE'] = 'EN';
				$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'] = 'EN';
				break;
		}
		
	}
	


	function set_article3(&$D=null) {
		$this->get_setting($D);
		#https://github.com/jlevers/selling-partner-api/blob/main/docs/Api/OrdersV0Api.md#getOrderItems
		
		$connector = SellingPartnerApi\SellingPartnerApi::seller(
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],
			SellingPartnerApi\Enums\Endpoint::EU,  // Or Endpoint::EU, Endpoint::FE, Endpoint::NA_SANDBOX, etc.
		);

		$_sellerid = 'AXXXX';
		$_marketplace_ids = ['A1PA6795UKMFR9'];

		#Categorie ------------------------
		$report_type = "GET_XML_BROWSE_TREE_DATA";
		$reportsApi = $connector->reportsV20210630();
		$c = new SellingPartnerApi\Seller\ReportsV20210630\Dto\CreateReportSpecification($report_type,$_marketplace_ids);
		$result = $reportsApi->createReport($c);
		$requestId = $result->dto()->reportId;
		/* #Wichtig um Report erstmal zu erstellen!
		do {
			sleep(20);
			$result_get_report = $reportsApi->getReport($requestId)->dto();
			##var_dump($result_get_report->processingStatus);

			if (!$result_get_report) return false;
		} while ($result_get_report->processingStatus != "DONE");

		$result_get_report = $reportsApi->getReport($requestId)->dto();
		$reportDocumentId = $result_get_report->reportDocumentId;
		*/
		$reportDocumentId = "amzn1.spdoc.1.4.eu.854ac263-60de-4181-bfa7-b728bd9b3e17.T380Z31PF3C99T.316"; #ToDo: Erstellung der Reports ggf. auf einmal im Monat beschränken??
		try {
                $responseDocument = $connector->ReportsV20210630()->getReportDocument($reportDocumentId, $report_type);
				$array = json_decode($responseDocument,true);
				
                ##echo '<pre>'; var_dump($responseDocument); echo '</pre>'; die;
                $reportDocument = $responseDocument->dto();
				$contents = $reportDocument->download($report_type);
				file_put_contents("report.txt",print_r($contents,true));
				#print_R($contents);
            } catch (\Throwable $th) {
                // file_put_contents("aaaaaaaaaaaaaaaa.txt", '<b>aaaaaaaaaaaaaaaaaa</b><br>'.$th);
                echo '<pre>'; var_dump($th ); echo '</pre>'; die;
            }
		
		
		exit;
		#----------------------------------


		#producttype ausgeben -------------
		$productTypeDefination = $connector->productTypeDefinitionsV20200901();
		$result = $productTypeDefination->searchDefinitionsProductTypes($_marketplace_ids);
		$array = json_decode($result,true);
		##print_r($array);exit;
		#--------------------------------


		#Product-Type shema ausgeben --------------
		
		##$productTypeDefination = $connector->productTypeDefinitionsV20200901();
		$marketplace_ids = $_marketplace_ids; // Vorausgesetzt, dies ist bereits festgelegt
		$seller_id = $_sellerid ; // Durch tatsächliche Verkäufer-ID ersetzen
		$product_type = 'HOME';
		$product_type_version = "LATEST";
		$requirements = "LISTING";
		$requirements_enforced = "ENFORCED";
		$locale = "DEFAULT";
		$result = $productTypeDefination->getDefinitionsProductType(
			$product_type, $marketplace_ids, $seller_id, $product_type_version, 
			$requirements, $requirements_enforced, $locale
		);
		$array = json_decode($result,true);
		#print_r($array);exit;
		
		#-------------------------------------------


		#Artikel Einstellen ----------------------
		$product_type = 'HOME';
		$requirements = "LISTING";
		$ListingsItems = $connector->listingsItemsV20210801();
		$attributes = [

			"item_name" => [ ["language_tag" => "de_DE","marketplace_id" => $_marketplace_ids[0],"value" => 'Kabel Schalter 100Watt NEU Aktuallisiert'],],
			'model_number' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "test100"], ],
			
			"list_price" => [["marketplace_id" => $_marketplace_ids[0], "currency" => "EUR", "value_with_tax" => "10.00"]],
			
			'part_number' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "test101"], ],
			'country_of_origin' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "CN"], ],
			'is_fragile' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "no"], ],
			'power_plug_type' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "no_plug"], ],
			'color' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "red"], ],
			
			'bullet_point' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],  "value" => "100 Gram"    ], ],
			
			'supplier_declared_dg_hz_regulation' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "other"], ],
			'manufacturer' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "Laurchen"], ],
			'item_package_dimensions' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"width"=> [ "value" => "60", "unit" => "millimeters"], "length"=> [ "value" => "60", "unit" => "millimeters"], "height"=> [ "value" => "60", "unit" => "millimeters"],  ]],
			'product_description' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "Beschreribung"], ],
			'brand' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "fuchswerk"], ],
			'recommended_browse_nodes' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "7145557031"], ],
			'item_package_weight' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "1", "unit" => 'grams'], ],
			'number_of_boxes' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "1", ], ],
			'number_of_items' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "1", ], ],
			'size' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "1", ], ],
			'batteries_required' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "false", ], ],
			
			
			'supplier_declared_has_product_identifier_exemption' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "true", ], ],
			
			
			#'merchant_suggested_asin' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "", ], ], #ASIN oder 
			#'externally_assigned_product_identifier' => [ [ "language_tag" => "de_DE", "marketplace_id" => $_marketplace_ids[0],"value" => "upc"], ], #EAN
	
		];
		$product_type = 'HOME';
		$requirements = 'LISTING_PRODUCT_ONLY';

		$listingsItemPutRequest = new SellingPartnerApi\Seller\ListingsItemsV20210801\Dto\ListingsItemPutRequest($product_type,$attributes,$requirements);
		try {
			$result = $ListingsItems->putListingsItem($_sellerid,"test101",$listingsItemPutRequest,$_marketplace_ids);
			$array = json_decode($result,true);
		print_r($array);exit;
		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
			 if ($e instanceof \Jlevers\SellingPartnerApi\Exception\ApiException) {
				echo 'Response Body: ' . $e->getResponseBody() . "\n";
			}
		}
		#--------------------------

	}


	function set_article2(&$D=null)
	{
		#https://github.com/jlevers/selling-partner-api/issues/217
		$this->get_setting($D);
		
		$config = new Configuration([
			"lwaClientId"			=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],#"amzn1.application-oa2-client.XXXX",
			"lwaClientSecret"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],#"7904a8a808b6a564fe8deefaa9c63b2fc0dXXXX",
			"lwaRefreshToken"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],#"Atzr|IwEBIOlJyJPeaQlENAxas0DA5Y4JmvXXXX",
			"awsAccessKeyId"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['AccessKeyId']['VALUE'],#"AKIA4RPOTXXXXX",
			"awsSecretAccessKey"	=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['SecretAccessKey']['VALUE'],#"sLX3zog3eJRiNaXXXXXXXX",
			"endpoint"				=> Endpoint::EU, #(in_array($D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE'],['A1PA6795UKMFR9','AMEN7PMS3EDWL'])? Endpoint::EU : Endpoint::NA,  // or another endpoint from lib/Endpoint.php
			#"accessToken" => null,
			#"onUpdateCredentials" => $c,
		]);
		

		
		$_sellerid = 'AXXXX';
		$_marketplace_ids = 'A1PA6795UKMFR9';
		#producttype ausgeben -------------
		$apiInstance = new SellingPartnerApi\Api\ProductTypeDefinitionsV20200901Api($config);
		$result = $apiInstance->searchDefinitionsProductTypes($_marketplace_ids);
		print_r($result);
		#--------------------------------
		
		
		#Product-Type shema ausgeben --------------
		$apiInstance = new SellingPartnerApi\Api\ProductTypeDefinitionsV20200901Api($config);
		$marketplace_ids = $_marketplace_ids; // Vorausgesetzt, dies ist bereits festgelegt
$seller_id = $_sellerid ; // Durch tatsächliche Verkäufer-ID ersetzen
$product_type = 'HOME';
$product_type_version = "LATEST";
$requirements = "LISTING";
$requirements_enforced = "ENFORCED";
$locale = "DEFAULT";
		$result = $apiInstance->getDefinitionsProductType(
          $product_type, $marketplace_ids, $seller_id, $product_type_version, 
          $requirements, $requirements_enforced, $locale
      );
	  print_r($result);
		#-------------------------------------------
		
		#Artikel Einstellen ----------------------
		$listingsApi = new SellingPartnerApi\Api\ListingsV20210801Api($config);
		// Erstelle ein neues ListingsItem
		$body = new SellingPartnerApi\Model\ListingsV20210801\ListingsItemPutRequest();
		$body->setProductType('HOME');#productType
		$body->setRequirements('LISTING_PRODUCT_ONLY');
		$attributes = [
			'sku' => 'test100',
			'title' => 'Your Product Title',
			'brand' => 'Your Brand Name',
			'manufacturer' => 'Your Manufacturer Name',
			'productType' => 'PRODUCT',
			'condition' => 'NEW',
			'attributes' => [
				'item_type' => 'home',
				
				// Füge hier weitere erforderliche Attribute hinzu
			],
		];
		$body->setAttributes($attributes); #attributes


		try {
			// Sende das Listing an Amazon
			$response = $listingsApi->putListingsItem(
			$_sellerid,
			'test100',
			$_marketplace_ids, $body,
			'de_DE');
			print_r($response);
		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}
		#--------------------------
		
	}
	
	function set_article(&$D=null)
	{

		#https://github.com/jlevers/selling-partner-api/blob/main/README.md#uploading-a-feed-document
		$this->get_setting($D);
		$config = new Configuration([
			"lwaClientId"			=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],#"amzn1.application-oa2-client.XXXX",
			"lwaClientSecret"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],#"7904a8a808b6a564fe8deefaa9c63b2fc0dXXXX",
			"lwaRefreshToken"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],#"Atzr|IwEBIOlJyJPeaQlENAxas0DA5Y4JmvXXXX",
			"awsAccessKeyId"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['AccessKeyId']['VALUE'],#"AKIA4RPOTXXXXX",
			"awsSecretAccessKey"	=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['SecretAccessKey']['VALUE'],#"sLX3zog3eJRiNaXXXXXXXX",
			"endpoint"				=> Endpoint::EU, #(in_array($D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE'],['A1PA6795UKMFR9','AMEN7PMS3EDWL'])? Endpoint::EU : Endpoint::NA,  // or another endpoint from lib/Endpoint.php
			#"accessToken" => null,
			#"onUpdateCredentials" => $c,
		]);
		$feedsApi = new FeedsApi($config);


		#--alt------------
		#https://github.com/jlevers/selling-partner-api/blob/main/README.md#uploading-a-feed-document
		//$this->get_setting($D);
		/*
		$config = array (
			'ServiceURL' => "https://mws.amazonservices.de",
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'MaxErrorRetry' => 3,
			);
		$this->info($D);
		
		$service = new MarketplaceWebService_Client(
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['AccessKeyId']['VALUE'], #AWS Zugangsschlüssel-ID
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['SecretAccessKey']['VALUE'],#Client Secret
			$config,#APPLICATION_NAME,
			'hexcon',
			$D['CLASS'][get_class($this)]['VERSION']
			);
		*/
		######################################
		#$service = new MarketplaceWebService_Mock();
	if($_GET['log'])
	{
		#$feedType = FeedType::POST_PRODUCT_PRICING_DATA;
		$feedType = FeedType::POST_PRODUCT_DATA;
		$feedId = $_GET['log']; //'57289019438';  // From the createFeed call
		$feed = $feedsApi->getFeed($feedId);
		$feedResultDocumentId = $feed->resultFeedDocumentId;
		$feedResultDocument = $feedsApi->getFeedDocument($feedResultDocumentId);
		$docToDownload = new SellingPartnerApi\Document($feedResultDocument, $feedType);
		$contents = $docToDownload->download();  // The raw report data
		$data = $docToDownload->getData();  // Parsed/formatted report data
		#print_r($data);
		#echo "<br><textarea style='width:100%;height:300px;'>".$data."</textarea>";
		$this->_echo($data, "Result: {$feedId}");
		exit;
		/*
		file_put_contents('amazon_log.txt','');
		$parameters = array (
			  'Merchant' => $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'],
			  'FeedSubmissionId' => $_GET['log'],#'153717018283',#59456017511'<Feed Submission Id>',
			  'FeedSubmissionResult' => fopen('amazon_log.txt', 'rw+'),#@fopen('php://memory', 'w'),
			  #'MWSAuthToken' => '<MWS Auth Token>', // Optional
			);
			$request = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
				 
			$test = $this->_invoke($service->getFeedSubmissionResult($request));
			print_R($test);
			echo "<br><textarea style='width:100%;height:300px;'>".file_get_contents('amazon_log.txt')."</textarea>";
			exit;
		*/
	}
		#######################################	
		
		##$this->get_attribute($D);#erforderlich zur Attribute Kombination von Var-Kombi-Attributen
		##$PLF_ATT = array_keys($D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] );
						
		
		$D['PLATFORM_ID'] = $this->platform_id;
		$D['FEED']['ACCOUNT_ID'] = $this->account_id;
		$D['FEED']['ATTRIBUTE'] = $D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE'];
		$D['FEED']['PARENT'] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT'];
		
		$D['FEED']['LANG'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MainContentLanguage']['VALUE'];


		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][NULL]['CHILD']['D'] AS $kA => $ARTICLE) #Parent Artikel
		{
			$ARTICLE = &$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kA];
			if($ARTICLE['ACTIVE'] != 0)
			{
				$ARTICLE['NUMBER'] = $ARTICLE['ATTRIBUTE']['D']['Number']['VALUE']?:$ARTICLE['NUMBER'];
				if(!isset($ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE'])) #Wenn die Variation Gruppierung nicht verändert wurde dann weiter wie gewohnt!
				{
					if(1 || $D['UPDATE']) #Update Variante
					{
						echo "Insert<br>";
						$D['FEED']['FROM_PLATFORM_ID'] = $ARTICLE['PLATFORM_ID'];
						$D['FEED']['ARTICLE'] = $ARTICLE;
						$D['FEED']['ARTICLE']['NUMBER'] = $ARTICLE['NUMBER'];
						if($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['PRICE']['VALUE'])
							$D['FEED']['ARTICLE']['PRICE'] = $D['FEED']['ARTICLE']['ATTRIBUTE']['D']['PRICE']['VALUE'];
						###$D['FEED']['ARTICLE']['ATTRIBUTE'] = $this->_convert_attribute($ARTICLE['ATTRIBUTE']);
						$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
						$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
						
						$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DeliveryClass']['VALUE'] = $this->_getDeliveryClass($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DeliveryClass']['VALUE'],$D['FEED']['LANG']);#Gibt eine Versand art aus


						$D['FEED']['INDEX'] +=1;
						$D['FEED']['ARTICLE']['ID'] = $kA;
						$D['FEED']['OperationType'] = 'Update';
						$D['FEED']['ProductData'] = $this->_getMainCategorie($ARTICLE['CATEGORIE']['D']);
						if($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['AttributeGroup']['VALUE'])
							$D['FEED']['ProductData']['TYPE'] = $D['FEED']['ARTICLE']['ATTRIBUTE']['D']['AttributeGroup']['VALUE'];
						
						if($D['FEED']['ProductData']['TYPE'])#Categorie Gesetzt?
						{
							foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'] AS $kVAR => $VAR) #VARIANTEN
							{
								###$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'][$kVAR]['NUMBER'] = $VAR['ATTRIBUTE']['D']['Number']['VALUE']?:$VAR['NUMBER'];

								$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kVAR];
								$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['OperationType'] = ($VAR['ACTIVE'] != 0 && $D['FEED']['OperationType'] == 'Update')?'Update':'Delete';

								#ToDo: Normallerweise sollte control die Vererbung von vater auf Kind üernehmen, jedoch fuktioniert dies komischerweise per auto update nicht???!
								if($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['PRICE']['VALUE'] || $D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['PRICE']['VALUE'])
									$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['PRICE'] = ($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['PRICE']['VALUE'])?$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['PRICE']['VALUE']:$D['FEED']['ARTICLE']['PRICE'];

								$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
								$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
								

								
								#Var-ATT Kombinieren Start =====================
								if($D['FEED']['ProductData']['TYPE'] != 'Lighting') #ToDo: Veraltet, in lighting.tpl wird nun in der datei das mappen erstellt, nciht mehr über diese Stelle!
								{
									$aVAR_ATT = explode('|', $ARTICLE['VARIANTE_GROUP_ID']);
									$AKTIVE_ATT_ID = null;
									foreach($aVAR_ATT AS $VA)
									{
										if($VA == $aVAR_ATT[0])#wenn der erste VarAtt nicht zu geordnet kann, dann beim ersten hinzufügen
											$AKTIVE_ATT_ID = $D['FEED']['ProductData']['VariationTheme'][0];
										#if( in_array($VA,['SizeX','Color']) )#Überprüft nach Var.Attribute
										if( in_array($VA,(array)$D['FEED']['ProductData']['VariationTheme']))
										{
											$AKTIVE_ATT_ID = $VA;
											
										}
										elseif($AKTIVE_ATT_ID)
										{
											if(strpos($D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'],$AKTIVE_ATT_ID) === false)
												$D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'] .= (($D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'])?'|':'').$AKTIVE_ATT_ID;
											if($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'])
												$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'] .= (($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'])?'|':'').$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $VA ]['VALUE'];
											else
												$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] .= ($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']?'|':'').$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $VA ]['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'];
										}
									}
								}
								#Var-ATT Kombinieren Ende ======================
								###$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE'] = $this->_convert_attribute($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']);
							}

							$feed = $this->_add_artikel_to_feed($D,$feed);
							#print_r($feed);exit();
							$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kA ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = [
								'ACTIVE'		=> 1,/*
								'DATA'			=> [ 'ATTRIBUTE' => [ 'D' => [ 'ARTICLE' => [ 'ATTRIBUTE' => [ 'D' => ['TITLE' => [ 'LANGUAGE' => [ 'D' => [ 'DE' => [ 
													'VALUE' => $D['FEED']['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D']['DE']['VALUE']
													] ] ] ] ] ] ] ] ] ],*/
								'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
							];
							
							#################
							#ToDo: hotfix Referen für Ausländische amazon
							/*
							$aa  = ['amazon_it','amazon_fr','amazon_es','amazon_co_uk'];
							foreach($aa AS $kAA)
							{
								$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kA ]['PLATFORM']['D'][ $kAA ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = [
									'ACTIVE'		=> 1,
									'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
								];
							}
							*/
							#################
							
							#foreach((array)$ARTICLE['VARIANTE']['D'] AS $kV => $V)
							foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'] AS $kV => $V)
							{
								$V = &$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kV];
								$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kV ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $V['NUMBER'] ] = [
									'ACTIVE'		=> 1,/*
									'DATA'			=> [ 'ATTRIBUTE' => [ 'D' => [ 'ARTICLE' => [ 'ATTRIBUTE' => [ 'D' => ['TITLE' => [ 'LANGUAGE' => [ 'D' => [ 'DE' => [ 
										'VALUE' => $D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D']['DE']['VALUE']
										] ] ] ] ] ] ] ] ] ],*/
									'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
								];
								
								#################
								#ToDo: hotfix Referen für Ausländische amazon
								/*
								$aa  = ['amazon_it','amazon_fr','amazon_es','amazon_co_uk'];
								foreach($aa AS $kAA)
								{
									$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kV ]['PLATFORM']['D'][ $kAA ]['REFERENCE']['D'][ $V['NUMBER'] ] = [
										'ACTIVE'		=> 1,
										'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
									];
								}
								*/
								#################
							}
						}
						else
						{
							$D['PLATFORM']['D'][ $this->platform_id ]['LOG']['D'][microtime(true)] = [
								'RID'			=> $kA,
								'RTYPE'			=> 'article',
								'TYPE'			=> 'error',
								'TITLE'			=> "ArtikelID: {$kA}; NR {$ARTICLE['NUMBER']}; => Kategorie Attribut nicht zu gewiesen!",
								'DEBUG_TEXT'	=> null,
							];
							echo "{$ARTICLE['NUMBER']} - Kategorie nicht zu gewiesen!";
						}
					}
					
				}
				else #Variationen nach Parent splitten ===================================
				{

					#Variation Artikel Splitt Start============
					if(isset($ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE']))
					{
						$ChildGroupBy =  str_replace(["|{$ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE']}","{$ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE']}|","{$ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE']}"],"","{$ARTICLE['VARIANTE_GROUP_ID']}");
						$VARIANTE_GROUP_ID = $ARTICLE['ATTRIBUTE']['D']['ChildGroupBy']['VALUE'];#Neue Variation Gruppe
						$varParent = null;
						##ksort($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D']); #Die Kinder werden nach ID sortiert, damit die zuordnung immer gleich bleibt.
						
						uasort($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'], function($a,$b) { #Sortiert Nach Sort und behält die IDs
							return $a['SORT'] <=> $b['SORT'];
						});
						foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'] AS $kVART => $VART) #Variante Artikel
						{
							###$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kA]['CHILD']['D'][$kVART]['NUMBER'] = $VART['ATTRIBUTE']['D']['Number']['VALUE']?:$VART['NUMBER'];
							$aVAR_ATT = explode('|', $ChildGroupBy);
							$att = null;
							foreach((array)$aVAR_ATT AS $VA)
							{
								$_LANG = $D['FEED']['LANG'];
								$_LANG = 'DE'; #Nach Standard Sprache Sortieren, wie in Standard amazon.de 
								$att .= ($VART['ATTRIBUTE']['D'][ $VA ]['LANGUAGE']['D'][ $_LANG ]['VALUE'])? $VART['ATTRIBUTE']['D'][ $VA ]['LANGUAGE']['D'][ $_LANG ]['VALUE'] : $VART['ATTRIBUTE']['D'][ $VA ]['VALUE'];
							}
							$varParent[$ARTICLE['NUMBER'].'_'.$att]['VARIANTE']['D'][$kVART]=$VART; 
							
							#$_ARTICLE['ARTICLE']['D'][$kART]['REFERENCE']['D'][ $ART['NUMBER'].'_'.$att ]['VARIANTE']['D'][ $kVART ] = $VART;
						}
						if($ARTICLE['PLATFORM']['D'][$this->platform_id ]['REFERENCE']['D'][ $ARTICLE['NUMBER'] ])
							$_ARTICLE['ARTICLE']['D'][$kA]['REFERENCE']['D'][ $ARTICLE['NUMBER'] ]['ACTIVE'] = -1;#alten Vaterartikel ggf. vorher löschen
						$i=0;
						foreach((array)$varParent AS $_kP => $P)
						{
							$_ARTICLE['ARTICLE']['D'][$kA]['REFERENCE']['D'][ $ARTICLE['NUMBER'].'_'.$i ]['ACTIVE'] = ($ARTICLE['PLATFORM']['D'][$this->platform_id ]['REFERENCE']['D'][ $ARTICLE['NUMBER'].'_'.$i ]['ACTIVE'])?$ARTICLE['PLATFORM']['D'][$this->platform_id ]['REFERENCE']['D'][ $ARTICLE['NUMBER'].'_'.$i ]['ACTIVE']:1;
							$_ARTICLE['ARTICLE']['D'][$kA]['REFERENCE']['D'][ $ARTICLE['NUMBER'].'_'.$i ]['VARIANTE_GROUP_ID'] = $VARIANTE_GROUP_ID;
							foreach((array)$P['VARIANTE']['D'] AS $_kPV => $_PV)
							{
								$_ARTICLE['ARTICLE']['D'][$kA]['REFERENCE']['D'][ $ARTICLE['NUMBER'].'_'.$i ]['VARIANTE']['D'][ $_kPV ] = $_PV;
								
							}
							$i++;
						}
					}
					else #value = 0
					{

					}
					#Variation Artikel Splitt Ende============
							
						
							foreach((array)$_ARTICLE['ARTICLE']['D'][$kA]['REFERENCE']['D'] AS $_kART => $_ART)
							{
								
								$D['FEED']['FROM_PLATFORM_ID'] = $ARTICLE['PLATFORM_ID']; 
								echo "Insert<br>";
								$D['FEED']['ARTICLE'] = $ARTICLE;
								
								$D['FEED']['ARTICLE']['NUMBER'] = $_kART;
								$D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'] = $_ART['VARIANTE_GROUP_ID'];

								if($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['PRICE']['VALUE'])
									$D['FEED']['ARTICLE']['PRICE'] = $D['FEED']['ARTICLE']['ATTRIBUTE']['D']['PRICE']['VALUE'];
								###$D['FEED']['ARTICLE']['ATTRIBUTE'] = $this->_convert_attribute($ARTICLE['ATTRIBUTE']);
								$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
								$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
								
								$D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DeliveryClass']['VALUE'] = $this->_getDeliveryClass($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['DeliveryClass']['VALUE'],$D['FEED']['LANG']);#Gibt eine Versand art aus


								$D['FEED']['INDEX'] +=1;
								$D['FEED']['ARTICLE']['ID'] = $kA;
								$D['FEED']['OperationType'] = ($_ART['ACTIVE'] != -1 && $ARTICLE['ACTIVE'] != 0)?'Update':'Delete';
								
								$D['FEED']['ProductData'] = $this->_getMainCategorie($ARTICLE['CATEGORIE']['D']);
								if($D['FEED']['ARTICLE']['ATTRIBUTE']['D']['AttributeGroup']['VALUE'])
									$D['FEED']['ProductData']['TYPE'] = $D['FEED']['ARTICLE']['ATTRIBUTE']['D']['AttributeGroup']['VALUE'];

								
								if($D['FEED']['ProductData']['TYPE'])#Categorie Gesetzt?
								{
									
									foreach((array)$_ART['VARIANTE']['D'] AS $kVAR => $VAR) #VARIANTEN
									{
										$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kVAR];
										$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['OperationType'] = ($VAR['ACTIVE'] != 0 && $D['FEED']['OperationType'] == 'Update')?'Update':'Delete';
										#$aaa = array_keys($D['FEED']['ARTICLE']['VARIANTE']['D']);
										#echo "{$D['FEED']['ARTICLE']['NUMBER']} - ".count($aaa)." - {$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kVAR]['NUMBER']} : {$aaa[0]} - {$D['FEED']['ARTICLE']['VARIANTE']['D'][$aaa[0]]['NUMBER']} : {$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kVAR]['NUMBER']}\n";
										if($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['PRICE']['VALUE'])
											$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['PRICE'] = $D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['PRICE']['VALUE'];

										$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
										$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE'] = $this->_clearHTML($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ $D['FEED']['LANG'] ]['VALUE']);
										
										
										
										#Var-ATT Kombinieren Start =====================
										/*
										if($D['FEED']['ProductData']['TYPE'] != 'Lighting') #ToDo: Veraltet, in lighting.tpl wird nun in der datei das mappen erstellt, nciht mehr über diese Stelle!
										{
											$aVAR_ATT = explode('|', $_ART['VARIANTE_GROUP_ID']);
											$AKTIVE_ATT_ID = null;
											foreach($aVAR_ATT AS $VA)
											{
												if($VA == $aVAR_ATT[0])#wenn der erste VarAtt nicht zu geordnet kann, dann beim ersten hinzufügen
													$AKTIVE_ATT_ID = $D['FEED']['ProductData']['VariationTheme'][0];
												#if( in_array($VA,['SizeX','Color']) )#Überprüft nach Var.Attribute
												if( in_array($VA,(array)$D['FEED']['ProductData']['VariationTheme']))
												{
													$AKTIVE_ATT_ID = $VA;
													
												}
												elseif($AKTIVE_ATT_ID)
												{
													if(strpos($D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'],$AKTIVE_ATT_ID) === false)
														$D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'] .= (($D['FEED']['ARTICLE']['VARIANTE_GROUP_ID'])?'|':'').$AKTIVE_ATT_ID;
													if($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'])
														$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'] .= (($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['VALUE'])?'|':'').$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $VA ]['VALUE'];
													else
														$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['LANGUAGE']['D']['DE']['VALUE'] .= ($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $AKTIVE_ATT_ID ]['LANGUAGE']['D']['DE']['VALUE']?'|':'').$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']['D'][ $VA ]['LANGUAGE']['D']['DE']['VALUE'];
												}
											}
										}*/
										##echo $_kART."- {$kVAR} - {$VAR['NUMBER']}\n";
										#Var-ATT Kombinieren Ende ======================
										###$D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE'] = $this->_convert_attribute($D['FEED']['ARTICLE']['VARIANTE']['D'][$kVAR]['ATTRIBUTE']);
									}
									

									$feed = $this->_add_artikel_to_feed($D,$feed);
									
									#print_r($feed);exit();
									$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kA ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = [
										'ACTIVE'		=> ($_ART['ACTIVE'] == -1)?-2:$_ART['ACTIVE'],
										'UTIMESTAMP'	=> ($D['UPDATE'])?date('YmdHis'):'00000000000000',#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
									];
									
									#################
									#ToDo: hotfix Referen für Ausländische amazon
									/*
									$aa  = ['amazon_it','amazon_fr','amazon_es','amazon_co_uk'];
									foreach($aa AS $kAA)
									{
										$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kA ]['PLATFORM']['D'][ $kAA ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = [
											'ACTIVE'		=> ($_ART['ACTIVE'] == -1)?-2:$_ART['ACTIVE'],
											'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
										];
									}
									*/
									#################
									
									#$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kVAR]
									foreach((array)$D['FEED']['ARTICLE']['VARIANTE']['D'] AS $kV => $V)
									{
										$V = &$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kV];
										$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kV ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $V['NUMBER'] ] = [
											'ACTIVE'		=> ($_ART['ACTIVE'] == -1 || $_ART['VARIANTE']['D'][$kV]['ACTIVE'] == -1)?-2:$_ART['VARIANTE']['D'][$kV]['ACTIVE'],
											'UTIMESTAMP'	=> ($D['UPDATE'])?date('YmdHis'):'00000000000000',#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
										];
										
										#################
										#ToDo: hotfix Referen für Ausländische amazon
										/*
										$aa  = ['amazon_it','amazon_fr','amazon_es','amazon_co_uk'];
										foreach($aa AS $kAA)
										{
											$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kV ]['PLATFORM']['D'][ $kAA ]['REFERENCE']['D'][ $V['NUMBER'] ] = [
												'ACTIVE'		=> ($_ART['ACTIVE'] == -1 || $_ART['VARIANTE']['D'][$kV]['ACTIVE'] == -1)?-2:$_ART['VARIANTE']['D'][$kV]['ACTIVE'],
												'UTIMESTAMP'	=> date('YmdHis'),#ToDo: Cronjob wird erneut wegen Bilder Problem dass Feed für diesen Artikel senden
											];
										}
										*/
										#################
										
									}

								}
								else
								{
									$D['PLATFORM']['D'][ $this->platform_id ]['LOG']['D'][microtime(true)] = [
										'RID'	=> $_kART,
										'RTYPE'	=> 'article',
										'TYPE'	=> 'error',
										'TITLE' => "ArtikelID: {$_kART}; NR {$ARTICLE['NUMBER']}; => Kategorie Attribut nicht zu gewiesen!",
										'DEBUG_TEXT'	=> null,
									];
									echo "{$ARTICLE['NUMBER']}: Kategorie nicht zu gewiesen!";
								}
							}
					#	print_R($feed);
					#echo $feed;exit;
				}
				/*
				else #Update #ToDo: Update PRozedur ist fehlerhaft, muss üerarbeitet werden. Daher wird Add statt dessen ausgeführt!
				{
					#exit("Momentan nur ADD Funktion funktioniert als hinzufügen so wie aktuallisieren des Artikels.!!");
					echo "Update:<br>";
					
					foreach((array)$ARTICLE['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'] as $k => $v )
					{
						if($v['ACTIVE'] != 0)
						{
							$D['FEED']['INDEX'] +=1;
							$D['FEED']['ARTICLE'] = $ARTICLE;
							$D['FEED']['ARTICLE']['NUMBER'] = $k;
							$D['FEED']['ARTICLE']['ID'] = $kA;
							$D['FEED']['ARTICLE']['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'] = ($ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'])? $ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'] : $ARTICLE['LANGUAGE']['D']['DE']['DESCRIPTION']['TITLE'];
							$D['FEED']['ARTICLE']['LANGUAGE']['D']['DE']['ARTICLE']['D']['DESCRIPTION']['VALUE'] = $this->_clearHTML( ($ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['DESCRIPTION']['VALUE'])? $ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['DESCRIPTION']['VALUE'] : $ARTICLE['LANGUAGE']['D']['DE']['DESCRIPTION']['LONG_TEXT'] );
							$D['FEED']['OperationType'] = ($v['ACTIVE'] <= -1 )?'Delete':'Update';
							$feed = $this->_add_artikel_to_feed($D,$feed);
							
							
							$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kA ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = array(
								'ACTIVE'		=> ($v['ACTIVE']==-1)?-2:1,
								'UTIMESTAMP'	=> date('YmdHis'),
							);
						}
					}
					#print_r($feed);exit;
					#falls Variationen eigene Ref haben, dann als allein ständige Artikel aktuallisieren
					foreach((array)$ARTICLE['VARIANTE']['D'] AS $kV => $V)
					{
						if($V['ACTIVE'] != 0)
						{
							foreach((array)$V['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'] as $k => $v )
							{
								if($v['ACTIVE'] != 0)
								{
									$D['FEED']['INDEX'] +=1;
									$D['FEED']['ARTICLE'] = array_merge($V,[
										'ID'		=> $kV,
										'NUMBER'	=> $k,
										'PLATFORM'	=> $V['PLATFORM'],
										'LANGUAGE'	=> array_merge_recursive($V['LANGUAGE'],$ARTICLE['LANGUAGE']),
										'CATEGORIE' => $ARTICLE['CATEGORIE'],
										'FILE'		=> array_merge_recursive($V['FILE'],$ARTICLE['FILE']), 
									]);
									$D['FEED']['ARTICLE']['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'] = ($ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'])? $ARTICLE['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'] : $ARTICLE['LANGUAGE']['D']['DE']['DESCRIPTION']['TITLE'];
								
									foreach((array)$V['LANGUAGE']['D']['DE']['ATTRIBUTE']['D'] as $kAtt => $Att)
										$D['FEED']['ARTICLE']['LANGUAGE']['D']['DE']['ARTICLE']['D']['TITLE']['VALUE'] .= " | {$Att['VALUE']}";

									$feed = $this->_add_artikel_to_feed($D,$feed);
									$D['PLATFORM']['D'][ $ARTICLE['PLATFORM_ID'] ]['ARTICLE']['D'][ $kV ]['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'][ $D['FEED']['ARTICLE']['NUMBER'] ] = array(
										'ACTIVE'		=> ($v['ACTIVE']==-1)?-2:1,
										'UTIMESTAMP'	=> date('YmdHis'),
									);
								}
							}
						}
					}
					
				}*/
			}
		}
		#print_r($feed);exit;
		/********* Begin Comment Block *********/

		if(1 || $D['UPDATE']) #Feed nur beim Update senden
		{
			foreach((array)$feed as $k => $v)
			{
				$D['FEED'] = $v;
				$this->smarty->assign('D', $D);
				$HTML = CFile::read("core/platform/amazon/xml.tpl/HEADER_FOOTER.tpl");
				$feedContents =  $feed1 = $this->smarty->fetch('string:'.$HTML);
				#print_R($feed1);exit;
				
				#echo "{$k} => {$feed1} \r\n";
				if(trim($v['Message']) != '')
				{
					$this->_echo($feed1, "Sende FEED: {$k}");
					$feedHandle = @fopen('php://temp', 'rw+');
					fwrite($feedHandle, $feed1);
					rewind($feedHandle);

					if($k == '_POST_PRODUCT_DATA_') {
						$feedType = FeedType::POST_PRODUCT_DATA;
					} else if ($k == '_POST_PRODUCT_IMAGE_DATA_') {
						$feedType = FeedType::POST_PRODUCT_IMAGE_DATA;
					} else if ($k == '_POST_PRODUCT_PRICING_DATA_') {
						$feedType = FeedType::POST_PRODUCT_PRICING_DATA;
					} else if ($k == '_POST_INVENTORY_AVAILABILITY_DATA_') {
						$feedType = FeedType::POST_INVENTORY_AVAILABILITY_DATA;
					} else if ($k == '_POST_PRODUCT_RELATIONSHIP_DATA_') {
						$feedType = FeedType::POST_PRODUCT_RELATIONSHIP_DATA;
					}

					// Create feed document
					$createFeedDocSpec = new Feeds\CreateFeedDocumentSpecification(['content_type' => $feedType['contentType']]);
					$feedDocumentInfo = $feedsApi->createFeedDocument($createFeedDocSpec);
					$feedDocumentId = $feedDocumentInfo->getFeedDocumentId();

					// Upload feed contents to document
					#$feedContents = file_get_contents('<your/feed/file.xml>');
					// The Document constructor accepts a custom \GuzzleHttp\Client object as an optional 3rd parameter. If that
					// parameter is passed, your custom Guzzle client will be used when uploading the feed document contents to Amazon.
					$docToUpload = new SellingPartnerApi\Document($feedDocumentInfo, $feedType);
					$docToUpload->upload($feedContents);
					rewind($feedHandle);

					$createFeedSpec = new Feeds\CreateFeedSpecification();
					$marketplace_ids = [$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE']];
					$createFeedSpec->setMarketplaceIds($marketplace_ids);
					$createFeedSpec->setInputFeedDocumentId($feedDocumentId);
					$createFeedSpec->setFeedType($feedType['name']);

					$createFeedResult = $feedsApi->createFeed($createFeedSpec);
					$feedId = $createFeedResult->getFeedId();
					@fclose($feedHandle);
					
					$this->_echo($createFeedResult, "Antwort: <a target='_blank' href='?log={$createFeedResult['feed_id']}'> Log zeigen </a>");
					#-------
					/*
					$parameters = array (
						'Merchant'			=> $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'],
						'MarketplaceIdList'	=> array('Id' =>  array($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MarketplaceId']['VALUE']) ),
						'FeedType'			=> $k,
						'FeedContent'		=> $feedHandle,
						'PurgeAndReplace'	=> false,
						'ContentMd5'		=> base64_encode(md5(stream_get_contents($feedHandle), true)),
					);

					rewind($feedHandle);
					#print_R($parameters);exit;
					$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);

					//********* End Comment Block *********	
					###print_R($request);exit;					
					$test = $this->_invoke($service->submitFeed($request));

					@fclose($feedHandle);
					
					#echo"<pre>";print_R($test);
					$this->_echo($test, 'Antwort');
					*/
				}
				else
					$this->_echo($feed1, "FEED: {$k} is empty. not send.");
			}
		}
		#return $D;
	}
	
	#ToDo: Veraltewt, die convertierung sollte dierekt in den einzelnen templates durchgeführt werden
	/*
	function _convert_attribute($Attt)
	{
		foreach((array)$Attt['D'] AS $kATTT => $ATTT)
		{
			if($kATTT != 'ARTICLE') #Hotfix: Attributetype -> ARTICLE ausgeschloßen
				foreach((array)$ATTT['ATTRIBUTE']['D'] AS $kATT => $ATT)
				{
					$VALUE = strip_tags(str_replace(["\n","\r"],[', ',''],$VALUE));
					if($ATT['LANGUAGE']['D']['DE']['VALUE'])
						$VALUE = &$Attt['D'][$kATTT]['ATTRIBUTE']['D'][$kATT]['LANGUAGE']['D']['DE']['VALUE'];
					else 
						$VALUE = &$Attt['D'][$kATTT]['ATTRIBUTE']['D'][$kATT]['VALUE'];

					if( in_array($kATT,['Voltage','ColorTemperature','Wattage','LightOutputLuminance','BulbLifeSpan']) )
						$VALUE = ((int)$VALUE);

					if( in_array($kATT,['IncludedComponent']) )
						$VALUE = nl2br($VALUE);
				}
		}
		return $Attt;
	}
	*/
	
	function _add_artikel_to_feed($D = null,$feed = null)
	{
		$this->smarty->assign('D', $D);
		
		$feed['_POST_PRODUCT_DATA_']['MessageType'] = 'Product';
		$feed['_POST_PRODUCT_DATA_']['PurgeAndReplace'] = 'false';#($this->platform_id == 'amazon_es')?'true':'false';#Nicht auf True, werden sonnst alle Artikel entfernt!
		$feed['_POST_PRODUCT_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'];
		$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_PRODUCT_DATA_.tpl");
		$feed['_POST_PRODUCT_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);

		$feed['_POST_PRODUCT_IMAGE_DATA_']['MessageType'] = 'ProductImage';
		$feed['_POST_PRODUCT_IMAGE_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'];
		$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_PRODUCT_IMAGE_DATA_.tpl");			
		$feed['_POST_PRODUCT_IMAGE_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);
		
		$feed['_POST_PRODUCT_PRICING_DATA_']['MessageType'] = 'Price';
		$feed['_POST_PRODUCT_PRICING_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'];
		$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_PRODUCT_PRICING_DATA_.tpl");
		$feed['_POST_PRODUCT_PRICING_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);
	
		$feed['_POST_INVENTORY_AVAILABILITY_DATA_']['MessageType'] = 'Inventory';
		$feed['_POST_INVENTORY_AVAILABILITY_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'];
		$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_INVENTORY_AVAILABILITY_DATA_.tpl");
		$feed['_POST_INVENTORY_AVAILABILITY_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);
		
		$feed['_POST_PRODUCT_RELATIONSHIP_DATA_']['MessageType'] = 'Relationship';
		$feed['_POST_PRODUCT_RELATIONSHIP_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MerchantId']['VALUE'];
		$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_PRODUCT_RELATIONSHIP_DATA_.tpl");
		$feed['_POST_PRODUCT_RELATIONSHIP_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);
		#print_R($feed['_POST_PRODUCT_RELATIONSHIP_DATA_']['Message']);exit('|');
		return $feed;
	}
	
	function _getMainCategorie($arrArtCat)
	{
		$arCAT = array_keys((array)$arrArtCat);
		$arkCAT = explode('|',$arrArtCat[$arCAT[0]]['BREADCRUMB_ID']);
		#K�che, Haushalt & Wohnen
		$MAP['3169011'] = ['TYPE' => 'Home', 'ProductType' => 'Home', 'VariationTheme' => [ 'Color', 'Size' ] ];
		#Lighting
		$MAP['213084031'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightsAndFixtures', 'VariationTheme' => [ 'Color' ] ];
		/*$MAP['213084031|293332011'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightsAndFixtures', 'VariationTheme' => [ 'Color' ] ];
		$MAP['213084031|165192011'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightsAndFixtures', 'VariationTheme' => [ 'Color' ] ];
		$MAP['213084031|3780981'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightsAndFixtures', 'VariationTheme' => [ 'Color' ] ];
		$MAP['213084031|227261031'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightingAccessories', 'VariationTheme' => [ 'Color' ] ];
		$MAP['213084031|227261031'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightBulbs', 'VariationTheme' => [ 'Color' ] ];
		$MAP['213084031|2076989031'] = ['TYPE' => 'Lighting', 'ProductType' => 'LightBulbs', 'VariationTheme' => [ 'Color' ] ];
		*/
		#CE *?
		$MAP['569604'] = ['TYPE' => 'CE', 'ProductType' => 'ConsumerElectronics', 'VariationTheme' => [ 'Color', 'Size' ] ];
		#AutoAccessory
		$MAP['79899031'] = ['TYPE' => 'AutoAccessory', 'ProductType' => 'AutoAccessory', 'VariationTheme' => [ 'Size','Color',  /*'Size-Color'*/ ] ];
		
		$MAP[80085031] = ['TYPE' => 'HomeImprovement', 'ProductType' => 'Hardware', 'VariationTheme' => [ 'Color', 'Size', 'Material', 'ItemWeight', 'ItemPackageQuantity' ] ];#???
		#$MAP[78689031]['TYPE'] = 'Clothing';###! anderer Type muss an gleiche Categorie ID zugewiesen werden
		$MAP[78689031]['TYPE'] = 'ClothingAccessories';
		$MAP[192417031]['TYPE'] = 'Office';
		#ToDo: Alle Kategorien auflösen
		return $MAP[$arkCAT[0]];
	}
	
	function get_categorie(&$D=null)
	{
		$this->get_setting($D);
		/*
		#echo $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['LastUpdateDateCategorie']['VALUE'].'|';
		$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['LastUpdateDateCategorie']['VALUE'] = 'test';
		$this->set_setting($D);
		exit;
		

		$config = new Configuration([
			"lwaClientId"			=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],
			"lwaClientSecret"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],
			"lwaRefreshToken"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],
			"awsAccessKeyId"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['AccessKeyId']['VALUE'],
			"awsSecretAccessKey"	=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['SecretAccessKey']['VALUE'],
			"endpoint"				=> Endpoint::EU,
			#"accessToken" => null,
			#"onUpdateCredentials" => $c,
		]);

		$apiInstance = new SellingPartnerApi\Api\CatalogItemsV0Api($config);
		$marketplace_id = $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE'];
		$asin = 'B099ZRG67C';
		try {
			$result = $apiInstance->listCatalogCategories($marketplace_id, $asin, $seller_sku);
			echo "<pre>";print_r($result);echo "<pre>";
		} catch (Exception $e) {
			echo 'Exception when calling CatalogItemsV0Api->listCatalogCategories: ', $e->getMessage(), PHP_EOL;
		}

		exit();
*/



		$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ 'main'] = [
			'ACTIVE'	=> 1,
			'PARENT_ID'	=> ''
		];
		$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ 'main' ]['LANGUAGE']['D']['DE']['TITLE'] = 'amazon';

		$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ 'main' ]['ATTRIBUTE']['D'] = 
		[
			'TITLE'							=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'BulletPoint0'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'BulletPoint1'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'BulletPoint2'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'BulletPoint3'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'BulletPoint4'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'SearchTerms'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'DESCRIPTION'					=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'IsDiscontinuedByManufacturer'	=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'IncludedComponent'				=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'PRICE'							=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'Number'						=> ['ACTIVE' => 1,'REQUIRED' => 0],
			'AttributeGroup'				=> ['ACTIVE' => 1,'REQUIRED' => 1],
		];

		$cat = CFile::dir(array('PATH' => 'core/platform/amazon/categorie/'));
		foreach($cat['FILE'] AS $kF => $vF)
		{
			#break; #ToDo: Ist nicht performant, weil Kategorien immer eingelesen werden!! es werden bereits mit parent::get_categorie($D); ausgelesen!
			#$time = $this->CCache->get("{$D['PLATFORM_ID']}_CATEGORIE_{$kF}_update_time");
			#if($vF['EDIT_TIME'] > $time)
			#{
				$CAT = CFile::csv_to_array("core/platform/amazon/categorie/{$vF['NAME']}");
				foreach($CAT AS $kCAT => $vCAT)
				{
					$TITLE = explode('/',$vCAT['Kategorie']);
					$ID[ $vCAT['Kategorie'] ] = $vCAT['ID'];
					$PARENT_ID = (count($TITLE) > 1)?$ID[ str_replace('/'.end($TITLE),'',$vCAT['Kategorie']) ]:'main';

						$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vCAT['ID'] ] = [
							'ACTIVE'	=> 1,
							'PARENT_ID'	=> $PARENT_ID
						];
						$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vCAT['ID'] ]['LANGUAGE']['D']['DE']['TITLE'] = end($TITLE);

						if($PARENT_ID)
						{
							$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vCAT['ID'] ]['BREADCRUMB_ID'] .= $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['BREADCRUMB_ID'].(($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['BREADCRUMB_ID'])?'|':'').$PARENT_ID;
							$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vCAT['ID'] ]['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE'] .= $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE'].(($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['LANGUAGE']['D']['DE']['BREADCRUMB_TITLE'])?'|':'').$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['LANGUAGE']['D']['DE']['TITLE'];

							$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['PARENT']['D'][ $PARENT_ID ]['CHILD']['D'][ $vCAT['ID'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vCAT['ID'] ];
							$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['CHILD']['D'][ $vCAT['ID'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['PARENT']['D'][ $PARENT_ID ]['CHILD']['D'][ $vCAT['ID'] ];
							
							$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $PARENT_ID ]['COUNT'] ++;
						}
				}
				$_main_id = str_replace('.csv','',$vF['NAME']);
				$this->SQL->query("DELETE FROM wp_categorie WHERE breadcrumb_id LIKE ('{$_main_id}%') AND platform_id = '{$this->platform_id}'");#ToDo: Notlösung um den Cache zu leeren befor die Datenbank erneut gefüllt werden kann.
				
				##Categorie_to_attribute Start ================
				$ATT = CFile::csv_to_array("core/platform/amazon/categorie_to_attribute/{$vF['NAME']}");
				foreach($ATT AS $kATT => $vATT)
				{
					$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vATT['CID'] ]['ATTRIBUTE']['D'][ $vATT['ATTID'] ] = [
						'ACTIVE'	=> 1,
						'REQUIRED'	=> 0,
					];
					$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['ATTRIBUTE']['D'][ $vATT['ATTID'] ] = &$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $vATT['CID'] ]['ATTRIBUTE']['D'][ $vATT['ATTID'] ];
				}
				##Categorie_to_attribute Ende =================


				parent::set_categorie($D);
				###$this->CCache->set("{$D['PLATFORM_ID']}_CATEGORIE_{$kF}_update_time", date('YmdHis'));
			#}
		}

		parent::get_categorie($D);

		#platform2::get_categorie($D);
		/*
		if($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['CACHE'])
			cache::get_categorie($D);
		else
		{
			$cat = CFile::dir(array('PATH' => 'core/platform/amazon/categorie/'));
			for($f=0;$f<count($cat['FILE']);$f++)
			{
				$a = CFile::csv_to_array("core/platform/amazon/categorie/{$cat['FILE'][$f]['NAME']}");
				for($c=0; $c < count($a); $c++)
				{
					$TITLE = explode('/',$a[$c]['Kategorie']);
					$last[ $TITLE[ count($TITLE)-1 ] ] = $a[$c]['ID'];
					
					if(!isset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['PARENT_ID']) || ($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['PARENT_ID'] == $last[ $TITLE[ count($TITLE)-2 ] ]))
					{
						$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a[$c]['ID'] ] = array(
							'ACTIVE'					=> 1,
							'PARENT_ID'					=> (count($TITLE) > 1)?$last[ $TITLE[ count($TITLE)-2 ] ]:'',
							);
						$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $a[$c]['ID'] ]['LANGUAGE']['D']['DE']['TITLE'] = $TITLE[ count($TITLE)-1 ];
					}
					
					if($last[ $TITLE[ count($TITLE)-2 ] ] && (!isset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['PARENT_ID']) || $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['PARENT_ID'] == $last[ $TITLE[ count($TITLE)-3 ] ] ))
					$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ $last[ $TITLE[ count($TITLE)-2 ] ] ]['COUNT'] ++;
				}
			}
			cache::set_categorie($D);
		}*/
	}
	
	function set_categorie(&$D)
	{
		##cache::set_categorie($D);
	}
	

	function get_language(&$D=null)
	{
		$this->get_setting($D);
		#https://github.com/amzn/selling-partner-api-docs/blob/main/guides/en-US/developer-guide/SellingPartnerApiDeveloperGuide.md
		switch( $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['MarketplaceId']['VALUE'])
		{ 
			case 'A1PA6795UKMFR9':#DE
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'DE' ] = ['ACTIVE' => 1, 'TITLE' => 'Deutsch'];#ToDo: kann durch {i18n id="language_{$kLAN}"} abgelöst werden?
				break;
			case 'A1RKKUPIHCS9HS':#ES
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'ES' ] = ['ACTIVE' => 1, 'TITLE' => 'Spanisch'];
				break;
			case 'A13V1IB3VIYZZH':#FR
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'FR' ] = ['ACTIVE' => 1, 'TITLE' => 'Französich'];
				break;
			case 'APJ6JRA9NG5V4':#IT
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'IT' ] = ['ACTIVE' => 1, 'TITLE' => 'Italienisch'];
				break;
			case 'A1805IZSGTT6HS':#NL
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'NL' ] = ['ACTIVE' => 1, 'TITLE' => 'Nederlands'];
				break;
			case 'A2NODRKZP88ZB9':#SE
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'SE' ] = ['ACTIVE' => 1, 'TITLE' => 'Schwedisch'];
				break;
			case 'A1C3SOZRARQ6R3':#PL
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'PL' ] = ['ACTIVE' => 1, 'TITLE' => 'Polnisch'];
				break;
			case 'A1F83G8C2ARO7P':#EN
			default:
				$D['PLATFORM']['D'][ $this->platform_id ]['LANGUAGE']['D'][ 'EN' ] = ['ACTIVE' => 1, 'TITLE' => 'Englisch'];
				break;
		}
	}

	function get_order(&$D=null) {
		$this->get_setting($D);
		#https://github.com/jlevers/selling-partner-api/blob/main/docs/Api/OrdersV0Api.md#getOrderItems
		
		$connector = SellingPartnerApi\SellingPartnerApi::seller(
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],
			 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],
			SellingPartnerApi\Enums\Endpoint::EU,  // Or Endpoint::EU, Endpoint::FE, Endpoint::NA_SANDBOX, etc.
			['buyerInfo', 'shippingAddress'],
		);
		

		/*
		$config = new Configuration([
			"lwaClientId"			=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],#"amzn1.application-oa2-client.245cd4538be74cebaebcffc56d97a465",
			"lwaClientSecret"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],#"7904a8a808b6a564fe8deefaa9c63b2fc0dfbb6e8c7e09a24dd8f74f88a84724",
			"lwaRefreshToken"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],#"Atzr|IwEBIOlJyJPeaQlENAxas0DA5Y4JmvLqFU8LnEeOYlMAxYYKgW_C4VHvLaIvqnlW-33ZrYIiSoW2iK9oDKVLZF46JuI-3Hpi_26vQqRKOgexv_dw3tgx6-IkejaC0vUn-pIQkX6dY8cRkoIvAQT6hhGAkfY-0Np6ccedoxtV3Odz4RjOJxR7vrF2C5FLgREMq8pbMS_y-njMQS4-eq3rrNQs-1yBekyBp5N8ybMJb5Ih3kDl2KO4Dj7WhQFHIpQYLkJfLw56HetSI2xeJ85A38tjX2WWIv6BnEDI3RMtkLj2E0s-QSAgrpaud5BV-j8so-YxtFY",
			"awsAccessKeyId"		=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['AccessKeyId']['VALUE'],#"AKIA4RPOT4PKXKNTC6SG",
			"awsSecretAccessKey"	=> $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['SecretAccessKey']['VALUE'],#"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",
			"endpoint"				=> Endpoint::EU, #(in_array($D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE'],['A1PA6795UKMFR9','AMEN7PMS3EDWL'])? Endpoint::EU : Endpoint::NA,  // or another endpoint from lib/Endpoint.php
			#"accessToken" => null,
			#"onUpdateCredentials" => $c,
		]);
$config->setDebug(true);
$config->setDebugFile(__dir__.'/debug.log');
*/
$ordersApi = $connector->ordersV0();


###$apiInstance = new SellingPartnerApi\Api\OrdersV0Api($config);



#INFOS: https://github.com/jlevers/selling-partner-api/blob/main/docs/Api/OrdersV0Api.md#getOrderItems
#Get Orders--------
/*
$marketplace_ids = ['A1PA6795UKMFR9'];
$last_updated_after = '2023-01-25T01:01:01';
$data_elements = ['buyerInfo', 'shippingAddress'];
$order_statuses = ['Unshipped', 'PartiallyShipped','Shipped'];
try {
    $result = $apiInstance->getOrders($marketplace_ids, $created_after, $created_before, $last_updated_after, $last_updated_before, $order_statuses, $fulfillment_channels, $payment_methods, $buyer_email, $seller_order_id, $max_results_per_page, $easy_ship_shipment_statuses, $electronic_invoice_statuses, $next_token, $amazon_order_ids, $actual_fulfillment_supply_source_id, $is_ispu, $store_chain_store_id, $data_elements);
    echo "<pre>"; print_r($result); echo "</pre>";
} catch (Exception $e) {
    echo 'Exception when calling OrdersV0Api->getOrders: ', $e->getMessage(), PHP_EOL;
}
exit;
*/
#------------------
#Get Item -------------
/*
#max: 30 Abrufe
$order_id = '302-9328471-1693122'; // string | An Amazon-defined order identifier, in 3-7-7 format.
#$next_token = 'next_token_example'; // string | A string token returned in the response of your previous request.
$data_elements = ['buyerInfo']; // string[] | An array of restricted order data elements to retrieve (the only valid array element is \"buyerInfo\")

try {
    $result = $apiInstance->getOrderItems($order_id, $next_token, $data_elements);
    echo "<pre>"; print_r($result); echo "</pre>";
} catch (Exception $e) {
    echo 'Exception when calling OrdersV0Api->getOrderItems: ', $e->getMessage(), PHP_EOL;
}
exit;
*/
#----------------------


#Get Order------
/*
$order_id = '302-9328471-1693122';
$data_elements = ['buyerInfo', 'shippingAddress'];
try {
    $result = $apiInstance->getOrder($order_id, $data_elements);
    echo "<pre>"; print_r($result); echo "</pre>";
} catch (Exception $e) {
    echo 'Exception when calling OrdersV0Api->getOrderAddress: ', $e->getMessage(), PHP_EOL;
}
exit;
*/
#----------------


ini_set('max_execution_time', 300);

		if(0 && $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['CACHE'])
			cache::get_order($D);
		else
		{
			$this->get_setting($D);
			$this->info($D);

			if (!$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE']) {
				exit("Keine MarketplaceId");
			}
			$marketplace_ids = [$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE']];
			$last_updated_after = date("Y-m-d\\TH:i:s\\Z",time()-5.5*60*60);
			#$data_elements = ['buyerInfo', 'shippingAddress']; 
			$order_statuses = ['Unshipped', 'PartiallyShipped','Shipped'];
			try {

			#	$ary = $apiInstance->getOrders($marketplace_ids, $created_after, $created_before, $last_updated_after, $last_updated_before, $order_statuses, $fulfillment_channels, $payment_methods, $buyer_email, $seller_order_id, $max_results_per_page, $easy_ship_shipment_statuses, $electronic_invoice_statuses, $next_token, $amazon_order_ids, $actual_fulfillment_supply_source_id, $is_ispu, $store_chain_store_id, $data_elements);
				
				#https://github.com/jlevers/selling-partner-api/blob/v7.2.4/src/Seller/OrdersV0/Requests/GetOrders.php
				$_ary = $ordersApi->getOrders($marketplace_ids, $created_after, $created_before, $last_updated_after, $last_updated_before, 
				$order_statuses, $fulfillment_channels, $payment_methods, $buyer_email, $seller_order_id, $max_results_per_page, 
				$easy_ship_shipment_statuses, $electronic_invoice_statuses, $next_token, $amazon_order_ids, $actual_fulfillment_supply_source_id, $is_ispu, 
				$store_chain_store_id, $earliestDeliveryDateBefore , $earliestDeliveryDateAfter, $latestDeliveryDateBefore, $latestDeliveryDateAfter);
				
				$ary = json_decode($_ary,true);
			
				$this->_echo($ary, 'Antwort');

			} catch (Exception $e) {
				echo 'Exception when calling OrdersV0Api->getOrders: ', $e->getMessage(), PHP_EOL;
			}
			
 
			$_CurrencyCode = CWP::CurrencyCode('EUR'); #ToDo: Standard Währung
			foreach ((array) $ary['payload']['Orders'] as $kORD => $ORD) {
				$NAME = $this->_splitName($ORD['ShippingAddress']['Name']);
				$CUSTOMERNAME = $this->_splitName($ORD['BuyerInfo']['BuyerName']);
				$ID = $ORD['AmazonOrderId'];
				if ($ID) {
					$ADD = $this->_splitAdress($ORD['ShippingAddress']['AddressLine1'], $ORD['ShippingAddress']['AddressLine2']);
					$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID] = [
						#'ACTIVE'				=> 1,
						#'MESSAGE_GROUP_ID'		=> $CUSTOMER_ID[0],
						#'CUSTOMER_ID'			=> $CUSTOMER_ID[0],#Email ist die ID adresse des Users, mit Md5 wird es auf garantierte 32 Zeichen gebracht
						'CUSTOMER_NICKNAME' => $ORD['BuyerInfo']['BuyerName'],
						'CUSTOMER_EMAIL' => $ORD['BuyerInfo']['BuyerEmail'],
						'PAYMENT_ID' => 'AMAZON', #$ORDER['PaymentMethod'], #MoneyXferAcceptedInCheckout,
						'PAID' => (($ORD['FulfillmentChannel'] == 'AFN' || $ORD['OrderStatus'] == 'Shipped' || $ORD['OrderStatus'] == 'Unshipped') ? 1 : 0),
						'CURRENCYCODE' => $ORD['OrderTotal']['CurrencyCode'],
						'CONVERSIONRATE' => 1 / $_CurrencyCode[$ORD['OrderTotal']['CurrencyCode']], #CurencyCode Gibt Basis EURO aus, aber in Rechnung ist BasisWährung = CurrencyCode und kann eine adere Währung sein statt Euro die als Basis definiert ist 
						'SHIPPING_ID' => ($ORD['FulfillmentChannel'] == 'AFN') ? 'AFN' : 'DHL',
						#AFN(Amazon Fulfillment Network) |MFN
						#'BILLING_COMPANY'		=> $a['billing_company'],
						'BILLING' => [
							'COMPANY' => $ADD['COMPANY'], #(($ORD['ShippingAddress']['AddressLine2'])?$ORD['ShippingAddress']['AddressLine1']:''),	
							'NAME' => ($NAME[1]) ? $NAME[1] : $CUSTOMERNAME[1],
							'FNAME' => ($NAME[0]) ? $NAME[0] : $CUSTOMERNAME[0],
							'STREET' => $ADD['STREET'], #$STREET[0],
							'STREET_NO' => $ADD['STREET_NO'], #$STREET[1],
							'ZIP' => $ORD['ShippingAddress']['PostalCode'],
							'CITY' => $ORD['ShippingAddress']['City'],
							'COUNTRY_ID' => $ORD['ShippingAddress']['CountryCode'],
							'ADDITION' => $ADD['ADDITION'], #"Telefon: " .$ORD['ShippingAddress']['Phone'],
							'PHONE' => $ORD['ShippingAddress']['Phone'],
						],
						'DELIVERY' => [
							'COMPANY' => $ADD['COMPANY'], #(($ORD['ShippingAddress']['AddressLine2'])?$ORD['ShippingAddress']['AddressLine1']:''),	
							'NAME' => ($NAME[1]) ? $NAME[1] : $CUSTOMERNAME[1],
							'FNAME' => ($NAME[0]) ? $NAME[0] : $CUSTOMERNAME[0],
							'STREET' => $ADD['STREET'], #$STREET[0],
							'STREET_NO' => $ADD['STREET_NO'], #$STREET[1],
							'ZIP' => $ORD['ShippingAddress']['PostalCode'],
							'CITY' => $ORD['ShippingAddress']['City'],
							'COUNTRY_ID' => $ORD['ShippingAddress']['CountryCode'],
							'ADDITION' => $ADD['ADDITION'], #"Telefon: " .$ORD['ShippingAddress']['Phone'],
							'PHONE' => $ORD['ShippingAddress']['Phone'],
						],
						'WAREHOUSE_ID' => (($ORD['FulfillmentChannel'] == 'AFN') ? 'AMAZON_FBA' : NULL),
						#'COMMENT'				=> (($ORD['FulfillmentChannel']=='AFN')?'Amazon-FBA':'').$ORD['BuyerCheckoutMessage'],
						'SEND_INVOICE' => ($ORD['IsBusinessOrder'] == 'true') ? 1 : 0,
						'ITIMESTAMP' => substr(str_replace(['-', 'T', ':', 'Z'], '', $ORD['LastUpdateDate']), 0, 14)
						#'UTIMESTAMP'			=> $a['utimestamp'],
					];



					if ($ORD['FulfillmentChannel'] == 'AFN') { #Wen nicht FBA dann kein Status überhaupt setzen damit die nachträgliche statusänderug nicht überschrieben wird.
						$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['STATUS'] = 40;
					}
					if ($ORD['is_prime'] == 'true') {
						$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['STATUS'] = 20; #Versandfreigabe bei PRIME
						$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['COMMENT'] .= 'PRIME';
						$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['SHIPPING_ID'] = 'DPD';
					}
					if ($ORD['IsPremiumOrder'] == 'true') {
						$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['COMMENT'] .= ' PREMIUM';
					}


					#ARTICLE START==============================================
					$order_id = $ID; // string | An Amazon-defined order identifier, in 3-7-7 format.
					#$next_token = 'next_token_example'; // string | A string token returned in the response of your previous request.
					$data_elements = ['buyerInfo']; // string[] | An array of restricted order data elements to retrieve (the only valid array element is \"buyerInfo\")
					$resultArticle = null;
					try {
						usleep(500000);#0,5sec sleep um Timeout zu vermeiden
						#$resultArticle = $apiInstance->getOrderItems($order_id, $next_token, $data_elements);
						$connector = SellingPartnerApi\SellingPartnerApi::seller(
							 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],
							 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],
							 $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],
							SellingPartnerApi\Enums\Endpoint::EU,  // Or Endpoint::EU, Endpoint::FE, Endpoint::NA_SANDBOX, etc.
							['buyerInfo'],
						);
						$ordersApi = $connector->ordersV0();
		
						$_ary = $ordersApi->getOrderItems($order_id);
						$resultArticle = json_decode($_ary,true);
						$this->_echo($resultArticle, 'Antwort');

						if ($resultArticle) {
							#MwSt Auslesen aus den Einstellungen
							$Standard = $D['SETTING']['D']['StandardTaxClass']['VALUE']; #ToDo: aus den Einstellungen oder von Artikel Einstellugen auslesen!
							$_tax = json_decode($D['SETTING']['D']['TaxClassToCountry']['VALUE'],1);
							#$VAT = 19;
							$VAT = $_tax[$Standard][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $ID ]['DELIVERY']['COUNTRY_ID'] ];
							
							$SUM_SHIPPING_PRICE = 0;
							foreach ($resultArticle['payload']['OrderItems'] as $kA => $vA) {
								if ($vA['QuantityOrdered'] > 0) {
									if (!$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['D'][$vA['SellerSKU']]) { #Kunde kand zweimal vom gleichen Artikel bestellen, daher darf der Artikel nicht überschrieben werden um Bestand weiter hoch zu zählen
										$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['D'][$vA['SellerSKU']] = [
											'ACTIVE' => 1,
											#'ASIN'					=> $vA['ASIN'],
											'NUMBER' => $vA['SellerSKU'],
											'TITLE' => $vA['Title'],
											#'STOCK'					=> $vA['QuantityOrdered'], #Achtung Amazon kann ein Item mit QuantityOrdered = 0 senden, dieser scheint dann vom Kunden Storniert zu sein.
											#'WEIGHT'				=> 0,
											'PRICE' => (((float) $vA['ItemPrice']['Amount'] / $vA['QuantityOrdered']) / (100 + $VAT) * 100),
											'VAT' => $VAT,
											#'ITIMESTAMP'			=> $a['itimestamp'],
											#'UTIMESTAMP'			=> $a['utimestamp'],
										];
									}
									$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['D'][$vA['SellerSKU']]['STOCK'] += $vA['QuantityOrdered'];
	
									$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['PRICE'] += (((float) $vA['ItemPrice']['Amount']) / (100 + $VAT) * 100);
									$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['VAT'] += (((float) $vA['ItemPrice']['Amount']) / (100 + $VAT)) * $VAT;
									#($a['price']/100*$a['vat'])*$a['stock']
									$SUM_PREICE += $vA['ItemPrice']['Amount'];
									$SUM_SHIPPING_PRICE += $vA['ShippingPrice']['Amount'] - $vA['ShippingDiscount']['Amount'];
	
								}
								
							}
	
							if ($SUM_SHIPPING_PRICE > 0) {
								$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['D']['0000'] = [
									'ACTIVE' => 1,
									'NUMBER' => "0000",
									'TITLE' => "Versand",
									'STOCK' => 1,
									#'WEIGHT'				=> 0,
									'PRICE' => ($SUM_SHIPPING_PRICE) / (100 + $VAT) * 100,
									'VAT' => $VAT,
								];
								$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['PRICE'] += (((float) $SUM_SHIPPING_PRICE) / (100 + $VAT) * 100);
								$D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]['ARTICLE']['VAT'] += (((float) $SUM_SHIPPING_PRICE) / (100 + $VAT)) * $VAT;
							}
							
						}


					} catch (Exception $e) {
						echo 'Exception when calling OrdersV0Api->getOrderItems: ', $e->getMessage(), PHP_EOL;
						unset($D['PLATFORM']['D'][$this->platform_id]['ORDER']['D'][$ID]);#Lösche die letzte unvolständige Bestellung
						break;
					}

					
				}
			}#foreach end
			
			

			###echo $this->_echo($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']);

			
			


			/*
			echo "<pre>";
			print_R($ary);
			print_R($D);exit;*/
			parent::set_order($D); #ToDo:!
		}
	}
	
	function get_attribute(&$D=null)
	{
		##if(1 || $D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['CACHE'])
			parent::get_attribute($D);
		##else
		##{
			/*
			$att = CFile::dir(array('PATH' => 'core/platform/amazon/attribute/'));

			foreach($att['FILE'] AS $kATT => $vATT)
			{
				$a = CFile::csv_to_array("core/platform/amazon/attribute/{$vATT['NAME']}");
				foreach($a AS $k => $v)
				{
					$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ $v['id'] ]	= ['ACTIVE' => 1, 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => $v['title'], 'VALUE' => $v['option']]]] ];
				}
			}
			cache::set_attribute($D);
			*/
			
			
			#ARTICLE
			$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] = array_replace_recursive((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'],[
				'ARTICLE'						=> ['LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Artikel Attribute',]]] ],
					
				'TITLE' 						=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1, 'SORT' => -2, 'I18N' => 1, /*'MAXLENGTH' => 200,*/ 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Title']]] ],
				'DESCRIPTION' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'SORT' => -1, 'I18N' => 1, 'MAXLENGTH' => 1990, 'TYPE' => 'wysiwyg', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung', 'HELP' => 'Erlaubte HTML Zeichen:<br>'.htmlentities(htmlentities('<b><br><i><em><p><ul><li>')) ]]] ],
				'BulletPoint0' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 490, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung1']]] ],
				'BulletPoint1' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 490, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung2']]] ],
				'BulletPoint2' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 490, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung3']]] ],
				'BulletPoint3' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 490, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung4']]] ],
				'BulletPoint4' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 490, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beschreibung5']]] ],
				'SearchTerms' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'MAXLENGTH' => 200, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Keywords']]] ],
				'VehicleFitmentCode' 			=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'HSNTSN']]] ],
				'IsDiscontinuedByManufacturer'	=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1, 'TYPE' => 'checkbox', 'LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Auslaufartikel']]] ],
				'IncludedComponent' 			=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Lieferumfang', 'HELP' => 'Beinhaltet den Lieferumfang des Artikels, kann durch Umbrüche aufgezählt werden.' ]]] ],
				'Number' 						=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1, 'I18N' => 0, /*'MAXLENGTH' => 200,*/ 'LANGUAGE' =>['D'=>['DE'=>['TITLE' => 'Artikelnummer']]] ],
				'Price'							=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1, 'TYPE' => 'number', 'MIN' => '0', 'STEP' => '0.01',  'LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Preis']]] ],

				#ToDo: Vorübergehend => Muss von Attribute Gruppe abgelöst werden
				'AttributeGroup' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'REQUIRED' => 1, 'I18N' => 0, 'TYPE' => 'select', 'VALUE' => "AutoAccessory\nCE\nClothingAccessories\nHome\nHomeImprovement\nLighting\nOffice", 'VALUE_OPTION' => "AutoAccessory\nCE\nClothingAccessories\nHome\nHomeImprovement\nLighting\nOffice", 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'AttributeGruppe']]] ],	

				#'PARENT_GROUP_ID' 				=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 0, 'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'PARENT_GROUP_ID']]] ],
				'ChildGroupBy' 					=> ['PARENT_ID' => 'ARTICLE', 'ACTIVE' => 1,'I18N' => 0, 'TOPIN' => 'parent', 'TYPE' => 'text', 'VALUE_OPTION' => "\n0", 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Varianten gruppieren nach', 'HELP' => "Varianten können nach anderen Attributen gruppiert werden.<br> Werden weniger Variationsattribute gewählt, so werden mehrere Vaterartikel mit diesen Varationsgruppen erstellt. <br> Es müssen Attribute-IDs eingetragen werden.<br> Mehrere Attribute werden mit '|'-Zeichen getrennt.<br> '0' Zeichen bewirkt dass kein Attribute mehr zur Auswahl gestellt wird."]]] ],
			
				#ATTRIBUTE
				'ATTRIBUTE'						=> ['LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Attribute', ]]] ],
				'Brand'							=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1, 'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Marke']]] ],
				'Manufacturer'					=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1, 'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE'=>['D'=>['DE'=>['TITLE' => 'Hersteller']]] ],
				
				'Color'							=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Farbe' ]]] ],
				'ColorMap'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Farbenkarte' ]]] ],

				'Size'							=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Größe' ]]] ],

				'Efficiency'					=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1, 'I18N' => 0, 'TYPE' => 'select', 'VALUE_OPTION' => "A\nB\nC\nD\nE\nF", 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Energieeffizienz']]] ],
				'Material'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Material']]] ],
				
				'BulbType'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Lampentyp', 'HELP'=> 'z.B: LED']]] ],
				'PlugType'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Steckertyp', 'HELP'=> 'max 100 Zeichen']]] ],
				
				'CapType'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Fassung']]] ],
				'InternationalProtectionRating'	=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 0, 'TYPE' => 'select', 'VALUE_OPTION' => "IP00\nIP01\nIP02\nIP03\nIP04\nIP05\nIP06\nIP07\nIP08\nIP09\nIP10\nIP11\nIP12\nIP13\nIP14\nIP15\nIP16\nIP17\nIP18\nIP19\nIP20\nIP21\nIP22\nIP23\nIP24\nIP25\nIP26\nIP27\nIP28\nIP29\nIP30\nIP31\nIP32\nIP33\nIP34\nIP35\nIP36\nIP37\nIP38\nIP39\nIP40\nIP41\nIP42\nIP43\nIP44\nIP45\nIP46\nIP47\nIP48\nIP49\nIP50\nIP51\nIP52\nIP53\nIP54\nIP55\nIP56\nIP57\nIP58\nIP59\nIP60\nIP61\nIP62\nIP63\nIP64\nIP65\nIP66\nIP67\nIP68\nIP69", 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Schutzklasse']]] ],
				'LightingMethod'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Beleuchtungsart', 'HELP' => 'Antik,Modern,...' ]]] ],
				'LampStartupTime'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Schaltzeit' ]]] ],
				'LampWarmupTime'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Anlaufzeit' ]]] ],
				'LightOutputLuminance'			=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 0, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Lichtleistung', 'HELP' => 'Einheit in Lumen' ]]] ],
					

				'StyleName'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Stilrichtung', 'HELP' => 'Antik,Modern,...' ]]] ],
				'Voltage'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Spannung', 'HELP' => 'Einheit in Volt' ]]] ],
				'Wattage'						=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Watt', 'HELP' => 'Einheit in Watt' ]]] ],
				'FixtureType'					=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Befestigungsart' ]]] ],

				'ColorTemperature'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Farbtemperatur', 'HELP' => 'Einheit in Kelvin' ]]] ],
				'ColorRenderingIndex'			=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Farbwiedergabeindex' ]]] ],
				'BulbLifeSpan'					=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Lebensdauer' ]]] ],
				'BulbSpecialFeatures'			=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Leuchtmitteleigenschaften', 'HELP' => 'z.B: Dimmbar' ]]] ],
				
				'SpecialFeatures'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'textarea', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Besondere Merkmale' ]]] ],
				'SpecificUses'					=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 1, 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Spezifische Verwendung' ]]] ],

				'ItemDimensions'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 0, 'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Produkt Abmessung', 'HELP' => 'Länge X Breite X Höhe' ]]] ],
				'PackageDimensions'				=> ['PARENT_ID' => 'ATTRIBUTE','ACTIVE' => 1,'I18N' => 0, 'TOPIN' => 'parent', 'TYPE' => 'text', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Verpakung Abmessung', 'HELP' => 'Länge X Breite X Höhe' ]]] ],
			]);
			foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'] AS $kAtt => $Att) {
				$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['PARENT']['D'][ $Att['PARENT_ID'] ]['CHILD']['D'][$kAtt] = &$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][$kAtt];
			}
			##parent::set_attribute($D);
			#FILE
			##$D['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D'][ 'Swatch' ]						= ['ACTIVE' => 1,'I18N' => 0, 'TYPE' => 'file', 'LANGUAGE' => ['D' => ['DE' => ['TITLE' => 'Swatch' ]]] ];
		##}
	}

	function set_attribute(&$D)
	{
		##cache::set_attribute($D);
	}
	/*
	function get_group(&$D=null)
	{
			$D['PLATFORM']['D'][ $this->platform_id ]['GROUP']['D'] = [
				'LightsAndFixtures'		=> ['ACTIVE' => 1, 'TYPE' => 'type', 'TITLE' => 'LightsAndFixtures'],
				'LightingAccessories'	=> ['ACTIVE' => 1, 'TYPE' => 'type', 'TITLE' => 'LightingAccessories'],
				'LightBulbs'			=> ['ACTIVE' => 1, 'TYPE' => 'type', 'TITLE' => 'LightBulbs'],
			];
	}
	*/
	#https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/OrderFulfillment.xsd
	#https://m.media-amazon.com/images/G/01/B2B/DeveloperGuide/vat_calculation_service__dev_guide_H383rf73k4hsu1TYRH139kk134yzs.pdf  #-> Rechnung Import Doku
	function set_order($D=null)
	{
		$this->get_setting($D);
		$config = array (
			'ServiceURL' => "https://mws.amazonservices.de",
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'MaxErrorRetry' => 3,
			);
		$this->info($D);
		
		$service = new MarketplaceWebService_Client(
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['AccessKeyId']['VALUE'],
			$D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['SecretAccessKey']['VALUE'],
			$config,#APPLICATION_NAME,
			'hexcon',
			$D['CLASS'][get_class($this)]['VERSION']
		);
			
		######################################
		#$service = new MarketplaceWebService_Mock();
		/*
		$parameters = array (
			  'Merchant' => $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['MERCHANT_ID']['VALUE'],
			  'FeedSubmissionId' => '115559018106',#'<Feed Submission Id>',
			  'FeedSubmissionResult' => fopen('test/test.txt', 'rw+'),#@fopen('php://memory', 'w'),
			  #'MWSAuthToken' => '<MWS Auth Token>', // Optional
			);
			$request = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
				 
			$test = $this->_invoke($service->getFeedSubmissionResult($request));
			print_R($test);exit;
			#*/
		#######################################
		
		foreach($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'] AS $kO => $ORDER)
		{
			if($ORDER['ACTIVE'] == 1)
			{
				$D['FEED']['ORDER'] = $ORDER;
				$D['FEED']['INDEX'] +=1;
				$D['FEED']['ORDER']['ID'] = $kO;
				$this->smarty->assign('D', $D);
				
				$feed['_POST_ORDER_FULFILLMENT_DATA_']['MessageType'] = 'OrderFulfillment';
				$feed['_POST_ORDER_FULFILLMENT_DATA_']['MerchantIdentifier'] = $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['MERCHANT_IDENTIFIER']['VALUE'];
				$HTML = CFile::read("core/platform/amazon/xml.tpl/_POST_ORDER_FULFILLMENT_DATA_.tpl");
				$feed['_POST_ORDER_FULFILLMENT_DATA_']['Message'] .= $this->smarty->fetch('string:'.$HTML);
			}
		}
		#print_R($feed['_POST_ORDER_FULFILLMENT_DATA_']['Message']);exit;
				/********* Begin Comment Block *********/
		foreach($feed as $k => $v)
		{
			$D['FEED'] = $v;
			$this->smarty->assign('D', $D);
			$HTML = CFile::read("core/platform/amazon/xml.tpl/HEADER_FOOTER.tpl");
			$feed1 = $this->smarty->fetch('string:'.$HTML);
			#print_R($feed1);exit;
			
			#echo "{$k} => {$feed1} \r\n";
			$this->_echo($feed1, "Sende FEED:");
			$feedHandle = @fopen('php://temp', 'rw+');
			fwrite($feedHandle, $feed1);
			rewind($feedHandle);
			$parameters = array (
				'Merchant' => $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['SecretAccessKey']['VALUE'],
				'MarketplaceIdList' =>array('Id' =>  array($D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['MarketplaceId']['VALUE']) ),
				'FeedType' => $k,
				'FeedContent' => $feedHandle,
				'PurgeAndReplace' => false,
				'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
			);

			rewind($feedHandle);
	#print_R($parameters);exit;
			$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);

			/********* End Comment Block *********/		
	#print_R($request);exit;		
			$test = $this->_invoke($service->submitFeed($request));

			@fclose($feedHandle);
			#print_R($test);
			$this->_echo($test, "Emfange FEED:");
		}
		return $D;
	}
	
	function get_payment(&$D=null)
	{
		$D['PLATFORM']['D'][ $this->platform_id ]['PAYMENT']['D'] = array(
			'amazon' => ['ACTIVE' => 1, 'TITLE' => 'Amazon'],
		);
		#return $D;
	}
	
	function get_shipping(&$D=null)
	{
		$D['PLATFORM']['D'][ $this->platform_id ]['SHIPPING']['D'] = array(
			'DHL'			=> ['ACTIVE' => 1, 'TITLE' => 'DHL'],
			'DEUTSCHEPOST'	=> ['ACTIVE' => 1, 'TITLE' => 'Deutsche Post'],
			'GLS'			=> ['ACTIVE' => 1, 'TITLE' => 'GLS'],
			'UPS'			=> ['ACTIVE' => 1, 'TITLE' => 'UPS'],
			'TNT'			=> ['ACTIVE' => 1, 'TITLE' => 'TNT'],
			'FEDEX'			=> ['ACTIVE' => 1, 'TITLE' => 'FEDEX'],
			'HLG'			=> ['ACTIVE' => 1, 'TITLE' => 'Hermes'],
		);
		#return $D;
	}
	/**
	 * Die sInvoiceNummer darf nur einmalig übergeben werden!
	 * @param mixed $sFile
	 * @param mixed $sInvoiceNumber
	 * @param mixed $sOrderId
	 * @return void
	 */
	public function sendInvoice($sFile, $sInvoiceNumber, $sOrderId)
	{
		$this->get_setting($D);
		$connector = SellingPartnerApi\SellingPartnerApi::seller(
			$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientId']['VALUE'],
			$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaClientSecret']['VALUE'],
			$D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['lwaRefreshToken']['VALUE'],
			SellingPartnerApi\Enums\Endpoint::EU,  // Or Endpoint::EU, Endpoint::FE, Endpoint::NA_SANDBOX, etc.
			//['buyerInfo', 'shippingAddress'],
		);
	
		$file = $sFile;
		$feedType = 'UPLOAD_VAT_INVOICE';
		$markplatzID = $D['PLATFORM']['D'][$this->platform_id]['SETTING']['D']['MarketplaceId']['VALUE'];
		$Documentype = 'Invoice';
		$InvoiceNumber = $sInvoiceNumber;
		$OrderId = $sOrderId;
		$Feed = $connector->feedsV20210630();
		$docSpecs = new  SellingPartnerApi\Seller\FeedsV20210630\Dto\CreateFeedDocumentSpecification('application/pdf');
		$feedInfos = $Feed->createFeedDocument($docSpecs);


		$feedDocument = $feedInfos->dto();

		$feedContents = file_get_contents($file);
		$result = $feedDocument->upload($feedType, $feedContents);
	
		$inputFeedDocumentId = $feedDocument->feedDocumentId;
	
		$createFeedSpec = new SellingPartnerApi\Seller\FeedsV20210630\Dto\CreateFeedSpecification(
			'UPLOAD_VAT_INVOICE',
			[$markplatzID],
			 $inputFeedDocumentId,
			[
				'metadata:Documentype' => $Documentype,
				'metadata:InvoiceNumber' => (string)$InvoiceNumber,
				'metadata:OrderId' => $OrderId,
				]
			);

		$result = $Feed->createFeed($createFeedSpec);
		print_r($result);
	}

	#=================================================
	function _invoke($response)
	{
		try {
			#$response = $service->ListOrders($request);

			//echo ("Service Response\n");
			//echo ("=============================================================================\n");

			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$strListOrders = $dom->saveXML();
			#echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
			
		} catch (MarketplaceWebServiceOrders_Exception $ex) {
			echo("Caught Exception: " . $ex->getMessage() . "\n");
			echo("Response Status Code: " . $ex->getStatusCode() . "\n");
			echo("Error Code: " . $ex->getErrorCode() . "\n");
			echo("Error Type: " . $ex->getErrorType() . "\n");
			echo("Request ID: " . $ex->getRequestId() . "\n");
			echo("XML: " . $ex->getXML() . "\n");
			echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
		}
		return $strListOrders;
	}
	
	function _getDeliveryClass($str,$lg)
	{
		$arr = explode('|',$str);
		if( strpos("|{$str}|", "|PRIME|") !== false && $lg == 'DE') //#ToDo: Die Settings sind hier nicht vorhanden && $D['PLATFORM']['D'][ $this->platform_id ]['SETTING']['D']['CONNECT']['VARIANTE']['D']['MARKETPLACE_ID']['VALUE'] == 'A1PA6795UKMFR9') #PRIME bevorzugen #ToDo: Hotfix, Prime nur für Deutschland
		{
			return "PRIME";
		}
		else
			return $arr[0];
	}

	function _splitName($name)
	{
		$ar = explode(' ',trim($name),2);
		for($i=0;$i<count($ar);$i++)
			$ar[$i] = trim($ar[$i]);
		return $ar;
	}
	
	function _splitStreet($Str)
	{
		$fund = false;
		for ($i=strlen($Str)-1;$i > strlen($Str)-(strlen($Str)-5); $i--)
		{
			if(is_numeric($Str[$i]))
			{
				$Pos = $i;
				$fund = true;
			}
			elseif($fund && !is_numeric($Str[$i]) && ($i > 1 && !is_numeric($Str[$i-1])) )
				break;
		}
		$return[0] = trim( ($fund)?substr($Str,0,$Pos):$Str );
		$return[1] = trim( ($fund)?substr($Str,$Pos):'' );

		if (!$fund) {#Ausland?
			#kein Fund,
			for ($i=0;$i < strlen($Str)-1; $i++) {
				if (is_numeric($Str[$i])) {
					$pos = $i;
					$fund = true;
				} elseif ($fund && $Str[$i] != ' ') {
					$pos = $i;
				} elseif ($Str[$i] == ' ' || ($fund && $i < strlen($Str)-2  && !is_numeric($Str[$i+1]))) {
					break;
				}
			}
			$return[0] = trim(($fund)?substr($Str, $pos+1):$Str);
			$return[1] = trim(($fund)?substr($Str, 0, $pos+1):'');
			#$return[2] = $fund;
		}
		return $return;
	}

	function _splitAdress($A,$B)
	{
		$A = trim($A);
		$B = trim($B);
		if(strpos(strtolower($A),'packstation') !== false || strpos(strtolower($B),'packstation') !== false)
		{#Packstation
			$ret['STREET'] = 'Packstation';
				preg_match("/\d+/",$A,$result);
				if( strlen($result[0]) == 3 )
					$ret['STREET_NO'] = $result[0];
				elseif(strlen($result[0]) >=8 && strlen($result[0]) < 10)
					$ret['ADDITION'] = $result[0];
				#else error
				
				preg_match("/\d+/",$B,$result);
				if( strlen($result[0]) == 3 )
					$ret['STREET_NO'] = $result[0];
				elseif(strlen($result[0]) >=8 && strlen($result[0]) < 10)
					$ret['ADDITION'] = $result[0];
		}
		else 
		{
			$s = $this->_splitStreet( (($B)?$B:$A) );
			$ret['STREET'] = $s[0];
			$ret['STREET_NO'] = $s[1];
			$ret['COMPANY'] = (($B)?$A:'');
			$ret['ADDITION'] = '';
		}
		return $ret;
	}
	
	/*Läscht unerlaubte HTML Syntax*/
	function _clearHTML($text)
	{
		$text = str_replace(['strong>','<br />','<br/>','\n','\r','	','   ','  ','<br> <br>','	','„','“'],['b>','<br>','<br>','','','',' ',' ','<br>','','"','"'],$text);
		$text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);#Lösche Attribute: style
		$text = preg_replace('/(<[^>]+) style=\'.*?\'/i', '$1', $text);
		$text = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $text);#Lösche Attribute: class
		$text = preg_replace('/(<[^>]+) class=\'.*?\'/i', '$1', $text);
		$text = preg_replace('/(<[^>]+) lang=".*?"/i', '$1', $text);#Lösche Attribute: lang
		$text = strip_tags( $text,'<b><br><i><em><p><ul><li>');
		return $text;
	}
	
	function _echo($d, $title = null)
	{
		echo "<div style='border:solid 1px #000;background:#eee;margin:5px;padding:5px;'>{$title}<br><textarea style='min-height:200px;max-height:400px;width:100%;border:1px solid #999;background:oldlace;overflow:scroll;'>";
		print_r($d);
		echo "</textarea></div>";
	}
}