<Message>
	<MessageID>{$D.FEED.INDEX}</MessageID>
	<OperationType>Update</OperationType>
	<Inventory>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<Quantity>{if $D.FEED.ARTICLE.STOCK !== null && $D.FEED.ARTICLE.STOCK > 0}{$D.FEED.ARTICLE.STOCK}{else}0{/if}</Quantity>
	</Inventory>
</Message>
{if $D.FEED.ARTICLE.VARIANTE.D}
	{foreach name=VAR key=kVAR item=VAR from=$D.FEED.ARTICLE.VARIANTE.D}
		{if $VAR.ACTIVE > 0}
		<Message>
			<MessageID>{$D.FEED.INDEX}</MessageID>
			<OperationType>Update</OperationType>
			<Inventory>
				<SKU>{$VAR.NUMBER}</SKU>
				<Quantity>{if $VAR.STOCK !== null && $VAR.STOCK > 0}{$VAR.STOCK}{else}0{/if}</Quantity>
			</Inventory>
		</Message>
		{/if}
	{/foreach}
{/if}