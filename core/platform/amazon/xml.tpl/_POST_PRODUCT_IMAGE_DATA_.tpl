{*DOKU: https://developer-docs.amazon.com/sp-api/docs/feed-type-values*}
	{*$aREF = $D.FEED.ARTICLE.PLATFORM.D[$D.PLATFORM_ID].REFERENCE.D|array_keys}
	{if $D.FEED.ARTICLE.PLATFORM.D[$D.PLATFORM_ID].REFERENCE.D|count > 1 && $aREF[0] !== $D.FEED.ARTICLE.NUMBER}
		{$x = $PIC|shuffle}
	{/if*}
{*
<Message>
	<MessageID>{$D.FEED.INDEX}100</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Main</ImageType>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad(1,3,'0',STR_PAD_LEFT)}10</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Swatch</ImageType>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad(1,3,'0',STR_PAD_LEFT)}20</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>BKLB</ImageType>
	</ProductImage> 
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad(1,3,'0',STR_PAD_LEFT)}30</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Search</ImageType>
	</ProductImage> 
</Message>
{section name=run start=1 loop=9 step=1}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.section.run.index,3,'0',STR_PAD_LEFT)}00</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>PT{$smarty.section.run.index}</ImageType>
	</ProductImage>
</Message>
{/section}
{* Bild von der Verpakung, maximal 6 Bilder
{section name=run start=1 loop=6 step=1}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.section.run.index,3,'0',STR_PAD_LEFT)}00</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>PS{$smarty.section.run.index}</ImageType>
	</ProductImage>
</Message>
{/section}
*}
*}
	{foreach name=FIL key=kFIL item=FIL from=$D.FEED.ARTICLE.FILE.D}
		{$PPIC[($smarty.foreach.FIL.iteration-1)] = $kFIL}
		{$PIC_IsChange[($smarty.foreach.FIL.iteration-1)] = ($FIL.UTIMESTAMP >= $D.FEED.ARTICLE.PLATFORM.D[ $D.PLATFORM_ID ].REFERENCE.D[ $D.FEED.ARTICLE.NUMBER ].UTIMESTAMP)?1:0}
	{/foreach}
	{foreach name=FIL key=kFIL item=FIL from=$D.FEED.ARTICLE.FILE.D}
		{if $smarty.foreach.FIL.iteration < 9}
			{if $smarty.foreach.FIL.iteration == 1}
				{if $PIC_IsChange[$smarty.section.run.index]}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Main</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[$smarty.foreach.FIL.iteration-1]}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}1</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Swatch</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{if $D.FEED.ARTICLE.ATTRIBUTE.D['Swatch'].VALUE}{$D.FEED.ARTICLE.ATTRIBUTE.D['Swatch'].VALUE}{else}{$PPIC[$smarty.foreach.FIL.iteration-1]}{/if}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}2</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>BKLB</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[$smarty.foreach.FIL.iteration-1]}_1500x1500.jpg</ImageLocation>
	</ProductImage> 
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}3</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>Search</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[$smarty.foreach.FIL.iteration-1]}_1500x1500.jpg</ImageLocation>
	</ProductImage> 
</Message>
				{/if}
			{else}
				{if $PIC_IsChange[$smarty.foreach.FIL.iteration-1]}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>PT{$smarty.foreach.FIL.iteration-1}</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[$smarty.foreach.FIL.iteration-1]}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
				{/if}
			{/if}
		{/if}
	{/foreach}
{*
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>PFDE</ImageType>
		<ImageLocation>https://data.XX.de/B08XXX.DEPF.jpg</ImageLocation>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.FIL.iteration,3,'0',STR_PAD_LEFT)}1</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		<ImageType>EEGL</ImageType>
		<ImageLocation>https://data.XX.de/B08XXX.GLEE.jpg</ImageLocation>
	</ProductImage>
</Message>
*}
{*VARIANTE============================================*}
{foreach name=VAR key=kVAR item=VAR from=$D.FEED.ARTICLE.VARIANTE.D}
	{if $VAR.ACTIVE > 0}
{*
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad(1,3,'0',STR_PAD_LEFT)}00</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Main</ImageType>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad(1,3,'0',STR_PAD_LEFT)}10</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Swatch</ImageType>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad(1,3,'0',STR_PAD_LEFT)}20</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>BKLB</ImageType>
	</ProductImage> 
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad(1,3,'0',STR_PAD_LEFT)}30</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Search</ImageType>
	</ProductImage> 
</Message>
{section name=run start=1 loop=9 step=1}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad({$smarty.section.run.index},3,'0',STR_PAD_LEFT)}00</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>PT{$smarty.section.run.index}</ImageType>
	</ProductImage>
</Message>
{/section}
*}
	{$PIC = null}
	{foreach name=FIL key=kFIL item=FIL from=$VAR.FILE.D}
		{$PIC[($smarty.foreach.FIL.iteration-1)] = $kFIL}
		{$PIC_IsChange[($smarty.foreach.FIL.iteration-1)] = ($FIL.UTIMESTAMP >= $VAR.PLATFORM.D[ $D.PLATFORM_ID ].REFERENCE.D[ $VAR.NUMBER ].UTIMESTAMP)?1:0}
	{/foreach}

	{section name=run start=0 loop=8 step=1}
		{if $smarty.section.run.index == 0}
			{if $PIC_IsChange[$smarty.section.run.index]}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Main</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PIC[$smarty.section.run.index]}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}1</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Swatch</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{if $VAR.ATTRIBUTE.D['Swatch'].VALUE}{$VAR.ATTRIBUTE.D['Swatch'].VALUE}{elseif $D.FEED.ARTICLE.ATTRIBUTE.D['Swatch'].VALUE}{$D.FEED.ARTICLE.ATTRIBUTE.D['Swatch'].VALUE}{else}{$PIC[$smarty.section.run.index]}{/if}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}2</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>BKLB</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PIC[$smarty.section.run.index]}_1500x1500.jpg</ImageLocation>
	</ProductImage> 
</Message>
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}3</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>Search</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[$smarty.section.run.index]}_1500x1500.jpg</ImageLocation>
	</ProductImage> 
</Message>
			{/if}
		{elseif $smarty.section.run.index < COUNT((array)$PIC)}
			{if $PIC_IsChange[$smarty.section.run.index]}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>PT{$smarty.section.run.index}</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PIC[$smarty.section.run.index]}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
			{/if}
{*
		{elseif $smarty.section.run.index < COUNT((array)$PPIC) && COUNT((array)$PIC) < 1}{*WENN keine Variationsbilder gibt, dann wird Parent Bilder verwendet.*-}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Update</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>PT{$smarty.section.run.index}</ImageType>
		<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/file/{$D.FEED.ACCOUNT_ID}/{$D.FEED.FROM_PLATFORM_ID}/{$PPIC[($smarty.section.run.index-(count((array)$PIC)))]}_1500x1500.jpg</ImageLocation>
	</ProductImage>
</Message>
*}
		{else}
<Message>
	<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}{str_pad($smarty.section.run.index+1,3,'0',STR_PAD_LEFT)}0</MessageID>
	<OperationType>Delete</OperationType>
	<ProductImage>
		<SKU>{$VAR.NUMBER}</SKU>
		<ImageType>PT{$smarty.section.run.index}</ImageType>
	</ProductImage>
</Message>
		{/if}

	{/section}

		{if $D.FEED.ARTICLE.ATTRIBUTE.D['Efficiency'] || $VAR.ATTRIBUTE.D['Efficiency']}
			{$EFF = $D.FEED.ARTICLE.ATTRIBUTE.D['Efficiency'].VALUE}
			{if $VAR.ATTRIBUTE.D['Efficiency']}{$EFF = $VAR.ATTRIBUTE.D['Efficiency']}{/if}
		<Message>
			<MessageID>{$D.FEED.INDEX}{str_pad($smarty.foreach.VAR.iteration,3,'0',STR_PAD_LEFT)}000</MessageID>
			<OperationType>Update</OperationType>
			<ProductImage>
				<SKU>{$VAR.NUMBER}</SKU>
				<ImageType>EEGL</ImageType>
				<ImageLocation>https://{$D.FEED.ACCOUNT_ID}.hexcon.de/data/ATTRIBUTE/efficiency_{$EFF}.jpg?n={$VAR.UTIMESTAMP}</ImageLocation>
			</ProductImage>
		</Message>
		{/if}
	{/if}
{/foreach}

{*Info: ImageType: DEEE = Energielabel; DEPF Datenblatt*}