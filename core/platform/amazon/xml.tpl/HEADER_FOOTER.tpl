<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>{$D.FEED.MerchantIdentifier}</MerchantIdentifier>
    </Header>
	<MessageType>{$D.FEED.MessageType}</MessageType>
	{if $D.FEED.PurgeAndReplace}<PurgeAndReplace>{$D.FEED.PurgeAndReplace}</PurgeAndReplace>{/if}
	{$D.FEED.Message}
</AmazonEnvelope>