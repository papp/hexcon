{function name=attribute}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE}
		<{$el}>{$var.LANGUAGE.D[$lg].VALUE}</{$el}>
	{else}
		<{$el}>{$var.VALUE}</{$el}>
	{/if}
{elseif $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $parent.LANGUAGE.D[$lg].VALUE}
		<{$el}>{$parent.LANGUAGE.D[$lg].VALUE}</{$el}>
	{else}
		<{$el}>{$parent.VALUE}</{$el}>
	{/if}
{/if}
{/function}

<AutoAccessory>
	<ProductType>
		<AutoPart>
			{if $D.FEED.ARTICLE.VARIANTE}
				{if !$kVAR}{*PARENT ARTIKEL*}
					<VariationData>
						<Parentage>parent</Parentage>
						<VariationTheme>{if strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color') !== false && strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Size') !== false}Size-Color{elseif strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color')!== false}Color{else}Size{/if}</VariationTheme>
					</VariationData>
				{else}{*KIND ARTIKEL*}
					<VariationData>
						<Parentage>child</Parentage>
						<VariationTheme>{if strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color') !== false && strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Size') !== false}Size-Color{elseif strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color') !== false}Color{else}Size{/if}</VariationTheme>
					</VariationData>
					{if $VAR.ATTRIBUTE.D['Color'].LANGUAGE.D['DE'].VALUE}
					<ColorSpecification>
						{*
						{$aVAR = explode('|',$D.FEED.ARTICLE.VARIANTE_GROUP)}
						{for $i=0 to count($aVAR)-1}
							{$VAR_ATT[$aVAR[$i]] = $VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE']}
						{/for}
						<Color>{foreach name=ATT key=kATT item=ATT from=$VAR_ATT}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</Color>
						<ColorMap>{foreach name=ATT key=kATT item=ATT from=$VAR_ATT}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</ColorMap>
						*}
						<Color>{str_replace('|',' - ',$VAR.ATTRIBUTE.D['Color'].LANGUAGE.D['DE'].VALUE)}</Color>
						<ColorMap>{*TODO*}{str_replace('|',' - ',$VAR.ATTRIBUTE.D['Color'].LANGUAGE.D['DE'].VALUE)}</ColorMap>
					</ColorSpecification>
					{/if}
				{/if}
			{/if}
			{if $D.FEED.ARTICLE.ATTRIBUTE.D['Size'].VALUE || $VAR.ATTRIBUTE.D['Size'].LANGUAGE.D['DE'].VALUE}{*HSNTSN*}
				{attribute el='Size' lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Size'] var=$VAR.ATTRIBUTE.D['Size']}
			{/if}
			{if $D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode'].VALUE}{*HSNTSN*}
				{$D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode'].VALUE = ($D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode'].VALUE|truncate:1999:"")}
				{attribute el='VehicleFitmentCode' lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode']}{*max2000*}
				<VehicleFitmentStandard>KBA</VehicleFitmentStandard>
			{/if}
		</AutoPart>
	</ProductType>
</AutoAccessory>