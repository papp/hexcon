{*https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/Product.xsd*}
{if !$D.FEED.ARTICLE.VARIANTE}
	<Message>
		<MessageID>{$D.FEED.INDEX}0</MessageID>
		<OperationType>{$D.FEED.OperationType}</OperationType>
		<Product>
			<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
			{if $D.FEED.ARTICLE.EAN}{*ToDo: Funktioniert nicht!*}
			<StandardProductID>
				<Type>EAN</Type>
				<Value>{$D.FEED.ARTICLE.EAN}</Value>
			</StandardProductID>
			{/if}
			<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
			<LaunchDate>2005-07-26T00:00:01</LaunchDate>
			<DescriptionData>
				<Title><![CDATA[{$CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['TITLE'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)|replace:"&":"und"|replace:"  ":" "|replace:"  ":" "|truncate:200:"":false}]]></Title>
				
				<Brand>{if $D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{$D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{else}Generisch{/if}</Brand>
				<Description>{strip}<![CDATA[<p></p>{($D.FEED.ARTICLE.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|replace:"\n":""|replace:"\r":""|replace:"	":""|replace:"  ":""|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>")|truncate:1990:"":false}
				
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{*if $ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}<br>{/if*}
					{/foreach}
					{*if $D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
						<br><b>Lieferumfang:</b><br>
						{nl2br($D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)}
					{/if*}
				]]>{/strip}</Description>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
					<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>
				{elseif 0 && $D.FEED.ARTICLE.ATTRIBUTE.D}
					<BulletPoint>{strip}<![CDATA[
					{$Bullet5 = ''}
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{if $D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}
						{$Bullet5 = $CWP->concat($Bullet5,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,': ',str_replace('|',', ',($ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE)?$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE:$ATT.VALUE),' | ')}
						{/if}
						{*if $D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}
							{$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {if $ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$ATT.VALUE}{/if}
							{if !$smarty.foreach.ATT.last} | {/if}
						{/if*}
					{/foreach}{$Bullet5|truncate:490:"":false}]]>{/strip}</BulletPoint>
				{/if}
				{if 0 && $D.FEED.ARTICLE.WEIGHT}{* || $D.FEED.ARTICLE.ATTRIBUTE.D['PackageDimensions'].VALUE*}
				<ItemDimensions>
					{*<Length unitOfMeasure="CM">10</Length>
					<Width unitOfMeasure="CM">10</Width>
					<Height unitOfMeasure="CM">10</Height>*}
					{if $D.FEED.ARTICLE.WEIGHT}<Weight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT/1000}</Weight>{/if}
				</ItemDimensions>
				{/if}
				{if 0 && $D.FEED.ARTICLE.WEIGHT}{* || $D.FEED.ARTICLE.ATTRIBUTE.D['PackageDimensions'].VALUE*}
				<PackageDimensions>
					{*<Length unitOfMeasure="CM">10</Length>
					<Width unitOfMeasure="CM">10</Width>
					<Height unitOfMeasure="CM">10</Height>*}
					{if $D.FEED.ARTICLE.WEIGHT}<Weight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT/1000}</Weight>{/if}
				</PackageDimensions>
				{/if}
				{*if $D.FEED.ARTICLE.WEIGHT >0}<PackageWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</PackageWeight>{/if}
				{if $D.FEED.ARTICLE.WEIGHT >0}<ShippingWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</ShippingWeight>{/if*}
				<Manufacturer><![CDATA[{$D.FEED.ARTICLE.ATTRIBUTE.D['manufacturer'].VALUE}]]></Manufacturer>
				<MfrPartNumber><![CDATA[{if !$D.FEED.ARTICLE.EAN}{str_pad($D.FEED.ARTICLE.NUMBER, 8, '0', STR_PAD_LEFT)}{else}{$D.FEED.ARTICLE.EAN}{/if}]]></MfrPartNumber>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<SearchTerms><![CDATA[{$D.FEED.ARTICLE.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:200:"":false}]]></SearchTerms>
				{elseif $SearchTerms}<SearchTerms><![CDATA[{$SearchTerms|truncate:200:"":false}]]></SearchTerms>{/if}
				<ItemType>flat-sheets</ItemType>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['ProductType'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<OtherItemAttributes>{$D.FEED.ARTICLE.ATTRIBUTE.D['ProductType'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:500:"":false}</OtherItemAttributes>{/if}
				<IsGiftWrapAvailable>false</IsGiftWrapAvailable>
				<IsGiftMessageAvailable>false</IsGiftMessageAvailable>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}<IsDiscontinuedByManufacturer>{$D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}</IsDiscontinuedByManufacturer>{/if}
				{foreach name=CAT key=kCAT item=CAT from=$D.FEED.ARTICLE.CATEGORIE.D}
					{if $smarty.foreach.CAT.iteration < 3}<RecommendedBrowseNode>{$kCAT}</RecommendedBrowseNode>{/if}
				{/foreach}
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}
				<MerchantShippingGroupName>{$D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}</MerchantShippingGroupName>
				{/if}
				<CountryOfOrigin>CN</CountryOfOrigin>
			</DescriptionData>
			<ProductData>
				{include D=$D file=$CWP->concat('core/platform/amazon/xml.tpl/inc_ProductData_',{$D.FEED.ProductData.TYPE},'.tpl')}
			</ProductData>
		</Product>
	</Message>
{else}{*Artikel mit Variation*}
	<Message>
		<MessageID>{$D.FEED.INDEX}0</MessageID>
		<OperationType>{$D.FEED.OperationType}</OperationType>
		<Product>
			<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
			<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
			<LaunchDate>2005-07-26T00:00:01</LaunchDate>
			<DescriptionData>
				<Title><![CDATA[{$CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['TITLE'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)|truncate:200:"":false} | {$D.FEED.ARTICLE.NUMBER}]]></Title>
				<Brand>{if $D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{$D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{else}Generisch{/if}</Brand>
				<Description>{strip}<![CDATA[<p></p>{*($D.FEED.ARTICLE.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|replace:"\n":""|replace:"\r":""|replace:"	":""|replace:"  ":""|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>")|truncate:1990:"":false*}
				{$D.FEED.ARTICLE.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:1990:"":false}
				{*str_replace("</ul><br>","</ul>",str_replace(['<h1>','</h1>','<h2>','</h2>','<h3>','</h3>','<strong>','</strong>',"\n","\r",' >',"</ul><br>"],['<br><b>','</b>','<br><b>','</b>','<b>','</b> ','<b>','</b>','','','>',"</ul>"],str_replace(["\n\r\n\r\n\r\n\r","\n\r\n\r\n\r","\n\r\n\r","\n\r"],"<br>",strip_tags(preg_replace('/class=".*?"/', '',$D.FEED.ARTICLE.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE),'<h1><h2><h3><br><ul><li><strong><p>') ) ) )|truncate:1990:"":false*}
				
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{*if $ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}<br>{/if*}
					{/foreach}
					{*if $D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
						<br><b>Lieferumfang:</b><br>
						{nl2br($D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)}
					{/if*}
				]]>{/strip}</Description>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>{/if}
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
					<BulletPoint><![CDATA[{($CWP->rand_choice_str($D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE))|truncate:490:"":false}]]></BulletPoint>
				{elseif 0 && $D.FEED.ARTICLE.ATTRIBUTE.D}
					<BulletPoint>{strip}<![CDATA[
					{$Bullet5 = ''}
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{if $D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}
							{$Bullet5 = $CWP->concat($Bullet5,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,': ',str_replace('|',', ',($ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE)?$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE:$ATT.VALUE),' | ')}
							{*$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {if $ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$ATT.VALUE}{/if}
							{if !$smarty.foreach.ATT.last} | {/if*}
						{/if}
					{/foreach}{$Bullet5|truncate:490:"":false}]]>{/strip}</BulletPoint>
				{/if}
				{if 0 && $D.FEED.ARTICLE.WEIGHT}{* || $D.FEED.ARTICLE.ATTRIBUTE.D['PackageDimensions'].VALUE*}
				<ItemDimensions>
					{*<Length unitOfMeasure="CM">10</Length>
					<Width unitOfMeasure="CM">10</Width>
					<Height unitOfMeasure="CM">10</Height>*}
					{if $D.FEED.ARTICLE.WEIGHT}<Weight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT/1000}</Weight>{/if}
				</ItemDimensions>
				{/if}
				{if 0 && $D.FEED.ARTICLE.WEIGHT}{* || $D.FEED.ARTICLE.ATTRIBUTE.D['PackageDimensions'].VALUE*}
				<PackageDimensions>
					{*<Length unitOfMeasure="CM">10</Length>
					<Width unitOfMeasure="CM">10</Width>
					<Height unitOfMeasure="CM">10</Height>*}
					{if $D.FEED.ARTICLE.WEIGHT}<Weight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT/1000}</Weight>{/if}
				</PackageDimensions>
				{/if}
				{*if $D.FEED.ARTICLE.WEIGHT >0}<PackageWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</PackageWeight>{/if}
				{if $D.FEED.ARTICLE.WEIGHT >0}<ShippingWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</ShippingWeight>{/if*}
				<Manufacturer><![CDATA[{$D.FEED.ARTICLE.ATTRIBUTE.D['manufacturer'].VALUE}]]></Manufacturer>
				<MfrPartNumber><![CDATA[{str_pad($D.FEED.ARTICLE.NUMBER, 8, '0', STR_PAD_LEFT)}]]></MfrPartNumber>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}<SearchTerms><![CDATA[{$D.FEED.ARTICLE.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:200:"":false}]]></SearchTerms>{/if}
				{*if $SearchTerms}<SearchTerms><![CDATA[{$SearchTerms|truncate:200:"":false}]]></SearchTerms>{/if*}
				<ItemType>flat-sheets</ItemType>
				<IsGiftWrapAvailable>false</IsGiftWrapAvailable>
				<IsGiftMessageAvailable>false</IsGiftMessageAvailable>
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}<IsDiscontinuedByManufacturer>{$D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}</IsDiscontinuedByManufacturer>{/if}
				{foreach name=CAT key=kCAT item=CAT from=$D.FEED.ARTICLE.CATEGORIE.D}
					{if $smarty.foreach.CAT.iteration < 3}<RecommendedBrowseNode>{$kCAT}</RecommendedBrowseNode>{/if}
				{/foreach}
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}
				<MerchantShippingGroupName>{$D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}</MerchantShippingGroupName>
				{/if}
			</DescriptionData>
			<ProductData>
				{include D=$D file=$CWP->concat('core/platform/amazon/xml.tpl/inc_ProductData_',{$D.FEED.ProductData.TYPE},'.tpl')}
			</ProductData>
		</Product>
	</Message>
	{foreach name=VAR key=kVAR item=VAR from=$D.FEED.ARTICLE.VARIANTE.D}
		{if $VAR.ACTIVE > 0}
	<Message>
		<MessageID>{$D.FEED.INDEX}0{$smarty.foreach.VAR.iteration}</MessageID>
		<OperationType>{$VAR.OperationType}</OperationType>
		<Product>
			<SKU>{$VAR.NUMBER}</SKU>
			{if $VAR.EAN}{*ToDo: Funktioniert nicht!*}
			<StandardProductID>
				<Type>EAN</Type>
				<Value>{$VAR.EAN}</Value>
			</StandardProductID>
			{/if}
			<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
			<LaunchDate>2005-07-26T00:00:01</LaunchDate>
			<DescriptionData>
				{if $VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$TITLE = $VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$TITLE = $D.FEED.ARTICLE.ATTRIBUTE.D['TITLE'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$DESCRIPTION = $VAR.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$DESCRIPTION = $D.FEED.ARTICLE.ATTRIBUTE.D['DESCRIPTION'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				
				{if $VAR.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$BulletPoint0 = $VAR.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$BulletPoint0 = $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint0'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$BulletPoint1 = $VAR.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$BulletPoint1 = $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint1'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$BulletPoint2 = $VAR.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$BulletPoint2 = $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint2'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$BulletPoint3 = $VAR.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$BulletPoint3 = $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint3'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$BulletPoint4 = $VAR.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$BulletPoint4 = $D.FEED.ARTICLE.ATTRIBUTE.D['BulletPoint4'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{/if}
				{if $VAR.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$SearchTerms0 = $VAR.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:200:"":false}{else}{$SearchTerms0 = $D.FEED.ARTICLE.ATTRIBUTE.D['SearchTerms'].LANGUAGE.D[ $D.FEED.LANG ].VALUE|truncate:200:"":false}{/if}
			
				{$aVAR = explode('|',$D.FEED.ARTICLE.VARIANTE_GROUP_ID)}
				{$addTITLE = ""}
				{for $i=0 to count($aVAR)-1}
					{if $D.FEED.ProductData.TYPE != 'Lighting'}{*ToDo: Vorübergehende weiche*}
						{*if in_array($aVAR[$i],['Size','Color'])*}
							{if $addTITLE}
								{$addTITLE = $CWP->concat($addTITLE, " - ",str_replace('|',' - ', $VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D[ $D.FEED.LANG ].VALUE) )}
							{else}
								{$addTITLE = $CWP->concat($addTITLE, str_replace('|',' - ', $VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D[ $D.FEED.LANG ].VALUE) )}
							{/if}
						{*/if*}
					{else}
						{if $addTITLE}
							{$addTITLE = $CWP->concat($addTITLE, ", ",str_replace('|',' - ', $VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D[ $D.FEED.LANG ].VALUE) )}
						{else}
							{$addTITLE = $CWP->concat($addTITLE, str_replace('|',' - ', $VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D[ $D.FEED.LANG ].VALUE) )}
						{/if}
					{/if}
				{/for}
				{$max_Length = 200-(strlen($addTITLE)+3)}
				{if $VAR.IS_TITLE_REPLACE_ATTRIBUTE == true}
				<Title><![CDATA[{$CWP->rand_choice_str($TITLE)|replace:"&":"und"|replace:"  ":" "|replace:"  ":" "|truncate:$max_Length:"":false}]]></Title>
				{else}
				<Title><![CDATA[{$CWP->rand_choice_str($TITLE)|replace:"&":"und"|replace:"  ":" "|replace:"  ":" "|truncate:$max_Length:"":false} | {$addTITLE}]]></Title>
				{/if}
				<Brand>{if $D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{$D.FEED.ARTICLE.ATTRIBUTE.D['brand'].VALUE}{else}Generisch{/if}</Brand>
				<Description>{strip}<![CDATA[<p></p>{*($D.FEED.ARTICLE.LANGUAGE.D[ $D.FEED.LANG ].DESCRIPTION.LONG_TEXT|replace:"\n":""|replace:"\r":""|replace:"	":""|replace:"  ":""|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>")|truncate:1990:"":false*}
				{($DESCRIPTION|replace:"\n":""|replace:"\r":""|replace:"	":""|replace:"  ":""|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>"|replace:"<br> <br>":"<br>")|truncate:1990:"":false}
				{*str_replace("</ul><br>","</ul>",str_replace(['<h1>','</h1>','<h2>','</h2>','<h3>','</h3>','<strong>','</strong>',"\n","\r",' >',"</ul><br>"],['<br><b>','</b>','<br><b>','</b>','<b>','</b> ','<b>','</b>','','','>',"</ul>"],str_replace(["\n\r\n\r\n\r\n\r","\n\r\n\r\n\r","\n\r\n\r","\n\r"],"<br>",strip_tags(preg_replace('/class=".*?"/', '',$D.FEED.ARTICLE.LANGUAGE.D[ $D.FEED.LANG ].ARTICLE.D.DESCRIPTION.VALUE),'<h1><h2><h3><br><ul><li><strong><p>') ) ) )|truncate:1990:"":false*}
				
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{*if $ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}<br>{/if*}
					{/foreach}
					{*if $D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
						<br><b>Lieferumfang:</b><br>
						{nl2br($D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)}
					{/if*}
				]]>{/strip}</Description>
				{if $BulletPoint0}<BulletPoint><![CDATA[{($CWP->rand_choice_str($BulletPoint0))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $BulletPoint1}<BulletPoint><![CDATA[{($CWP->rand_choice_str($BulletPoint1))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $BulletPoint2}<BulletPoint><![CDATA[{($CWP->rand_choice_str($BulletPoint2))|truncate:490:"":false}]]></BulletPoint>{/if}
                {if $BulletPoint3}<BulletPoint><![CDATA[{($CWP->rand_choice_str($BulletPoint3))|truncate:490:"":false}]]></BulletPoint>{/if}
				{if $BulletPoint4}
					<BulletPoint><![CDATA[{($CWP->rand_choice_str($BulletPoint4))|truncate:490:"":false}]]></BulletPoint>
				{elseif 0 && $D.FEED.ARTICLE.ATTRIBUTE.D}
					<BulletPoint>{strip}<![CDATA[
					{$Bullet5 = ''}
					{foreach name=ATT key=kATT item=ATT from=$D.FEED.ARTICLE.ATTRIBUTE.D}
						{$SearchTerms = $CWP->concat($SearchTerms,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,' ',$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE,' ')}
						{if $D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}
						{$Bullet5 = $CWP->concat($Bullet5,$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE,': ',str_replace('|',', ',($ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE)?$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE:$ATT.VALUE),' | ')}
						{/if}
						{*if $D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}
							{$D.FEED.ATTRIBUTE.D[$kATT].LANGUAGE.D[ $D.FEED.LANG ].TITLE}: {if $ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{$ATT.LANGUAGE.D[ $D.FEED.LANG ].VALUE}{else}{$ATT.VALUE}{/if}
							{if !$smarty.foreach.ATT.last} | {/if}
						{/if*}
					{/foreach}{$Bullet5|truncate:490:"":false}]]>{/strip}</BulletPoint>
				{/if}
				{*if $D.FEED.ARTICLE.WEIGHT >0}<PackageWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</PackageWeight>{/if}
				{if $D.FEED.ARTICLE.WEIGHT >0}<ShippingWeight unitOfMeasure="KG">{$D.FEED.ARTICLE.WEIGHT}</ShippingWeight>{/if*}
				<Manufacturer><![CDATA[{$D.FEED.ARTICLE.ATTRIBUTE.D['manufacturer'].VALUE}]]></Manufacturer>
				<MfrPartNumber><![CDATA[{if !$VAR.EAN}{str_pad($VAR.NUMBER, 8, '0', STR_PAD_LEFT)}{else}{$VAR.EAN}{/if}]]></MfrPartNumber>
				{if $SearchTerms0}<SearchTerms><![CDATA[{$SearchTerms0|truncate:200:"":false}]]></SearchTerms>{/if}
				{*if $SearchTerms}<SearchTerms><![CDATA[{$SearchTerms|truncate:200:"":false}]]></SearchTerms>{/if*}
				<ItemType>flat-sheets</ItemType>
				<IsGiftWrapAvailable>false</IsGiftWrapAvailable>
				<IsGiftMessageAvailable>false</IsGiftMessageAvailable>
				{if $VAR.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}<IsDiscontinuedByManufacturer>{$VAR.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}</IsDiscontinuedByManufacturer>{elseif $D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}<IsDiscontinuedByManufacturer>{$D.FEED.ARTICLE.ATTRIBUTE.D['IsDiscontinuedByManufacturer'].VALUE}</IsDiscontinuedByManufacturer>{/if}
				{foreach name=CAT key=kCAT item=CAT from=$D.FEED.ARTICLE.CATEGORIE.D}
					{if $smarty.foreach.CAT.iteration < 3}<RecommendedBrowseNode>{$kCAT}</RecommendedBrowseNode>{/if}
				{/foreach}
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}
				<MerchantShippingGroupName>{$D.FEED.ARTICLE.ATTRIBUTE.D['DeliveryClass'].VALUE}</MerchantShippingGroupName>
				{/if}
				<CountryOfOrigin>CN</CountryOfOrigin>
			</DescriptionData>
			<ProductData>
				{include D=$D file=$CWP->concat('core/platform/amazon/xml.tpl/inc_ProductData_',{$D.FEED.ProductData.TYPE},'.tpl')}
			</ProductData>
		</Product>	
	</Message>
		{/if}
	{/foreach}
{/if}