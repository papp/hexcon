{if $D.FEED.ARTICLE.VARIANTE.D}
<Message>
	<MessageID>{$D.FEED.INDEX}</MessageID>
	<OperationType>{$D.FEED.OperationType}</OperationType>
	<Relationship>
		<ParentSKU>{$D.FEED.ARTICLE.NUMBER}</ParentSKU>
		{foreach name=VAR key=kVAR item=VAR from=$D.FEED.ARTICLE.VARIANTE.D}
		{if $VAR.ACTIVE > 0}
		<Relation>
			<SKU>{$VAR.NUMBER}</SKU>
			<Quantity>{if $VAR.STOCK>0}{$VAR.STOCK}{else}1{/if}</Quantity>
			<Type>Variation</Type>
		</Relation>
		{/if}
		{/foreach}
	</Relationship>
</Message>
{/if}