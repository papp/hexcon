{function name=attribute}{strip}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE || $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
		{$VALUE = ($var.LANGUAGE.D[$lg].VALUE)?$var.LANGUAGE.D[$lg].VALUE:$var.VALUE}
	{else}
		{$VALUE = ($parent.LANGUAGE.D[$lg].VALUE)?$parent.LANGUAGE.D[$lg].VALUE:$parent.VALUE}
	{/if}

	{$VALUE = str_replace('|',' - ',$VALUE)}
	{$VALUE = ($type=='int')?(int)$VALUE:$VALUE}

	<{$el}{if $el == 'Voltage'} unitOfMeasure="volts"{elseif $el == 'ColorTemperature'} unitOfMeasure="kelvin"{elseif $el == 'BulbLifeSpan'} unitOfMeasure="hr"{/if}><![CDATA[{$VALUE}]]></{$el}>
	{if $el == 'Color'}
		<ColorMap>{if $VALUE == 'schwarz'}black{else}white{/if}</ColorMap>
	{/if}


{/if}
{/strip}{/function}

<CE>
	<ProductType>
		<ConsumerElectronics>
			{if $D.FEED.ARTICLE.VARIANTE.D}
				<VariationData>
					<Parentage>{if !$kVAR}parent{else}child{/if}</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
			{/if}
		
			{$dATT = ['BulbLifeSpan','BulbSpecialFeatures','BulbType','CapType','Color','ColorRenderingIndex','ColorTemperature','IncludedComponent','InternationalProtectionRating','LightingMethod','LampStartupTime','LampWarmupTime','SpecialFeatures','SpecificUses','StyleName','Voltage','Wattage','FixtureType','Efficiency']}
			{foreach name=ATT key=kATT item=ATT from=$dATT}
				{attribute el=$ATT lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D[$ATT] var=$VAR.ATTRIBUTE.D[$ATT]}
			{/foreach}
			
		</ConsumerElectronics>
	</ProductType>
</CE>