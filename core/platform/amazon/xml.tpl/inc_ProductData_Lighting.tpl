{*http://g-ecx.images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/Lighting.xsd*}
{function name=attribute}{strip}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE || $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
		{$VALUE = ($var.LANGUAGE.D[$lg].VALUE)?$var.LANGUAGE.D[$lg].VALUE:$var.VALUE}
	{else}
		{$VALUE = ($parent.LANGUAGE.D[$lg].VALUE)?$parent.LANGUAGE.D[$lg].VALUE:$parent.VALUE}
	{/if}

	{if $VALUE}
		{$VALUE = str_replace('|',' - ',$VALUE)}
		{*$VALUE = ($type=='int')?(int)$VALUE:$VALUE*}

		{if $maxOccurs > 1 && $delimiter}
			{$aValue = explode($delimiter,$VALUE)}
			{foreach name=VAL key=kVAL item=VAL from=$aValue}
				{if $smarty.foreach.VAL.iteration <= $maxOccurs}
					{if $length}
						{$VAL = $VAL|truncate:$length:"":true}
					{/if}
					
					{$VAL = ($type == 'number')?(int)$VAL:$VAL}
					
					{$VAL = str_replace("\r","",$VAL)}
				{if $VAL}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VAL}]]></{$el}>{/if}
				{/if}
			{/foreach}
		{else}
			{if $length}
				{$VALUE = $VALUE|truncate:$length:"":true}
			{/if}
			{$VALUE = ($type == 'number')?(int)$VALUE:$VALUE}
			{if $VALUE}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VALUE}]]></{$el}>{/if}
			
			{if $el == 'Color'}
				<ColorMap>white</ColorMap>
			{/if}
		{/if}
	{/if}

{/if}
{/strip}{/function}
<Lighting>
	<ProductType>		
		{*if !$D.FEED.ARTICLE.VARIANTE.D}
		<LightingAccessories>
			<Color>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D[ $D.FEED.LANG ].ATTRIBUTE.D}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</Color>
			<ColorMap>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D[ $D.FEED.LANG ].ATTRIBUTE.D}{str_replace(['warmweiß','neutralweiß','kaltweiß'],['white','white','white'],$ATT.VALUE)}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</ColorMap>
		</LightingAccessories>
		{else*}
		<LightsAndFixtures>
			{if $D.FEED.ARTICLE.VARIANTE.D}
				<VariationData>
					<Parentage>{if !$kVAR}parent{else}child{/if}</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
			{/if}
				
				{attribute el='BaseDiameter' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BaseDiameter'] var=$VAR.ATTRIBUTE.D['BaseDiameter']}
				{attribute el='BulbDiameter' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BulbDiameter'] var=$VAR.ATTRIBUTE.D['BulbDiameter']}
				{attribute el='BulbLength' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BulbLength'] var=$VAR.ATTRIBUTE.D['BulbLength']}
				
				{attribute el='BulbLifeSpan' lg=$D.FEED.LANG type='number' unitOfMeasure='hr' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BulbLifeSpan'] var=$VAR.ATTRIBUTE.D['BulbLifeSpan']}
				{attribute el='BulbSpecialFeatures' lg=$D.FEED.LANG maxOccurs=5 delimiter="\n" length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BulbSpecialFeatures'] var=$VAR.ATTRIBUTE.D['BulbSpecialFeatures']}
				{attribute el='BulbType' lg=$D.FEED.LANG length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['BulbType'] var=$VAR.ATTRIBUTE.D['BulbType']}
				{attribute el='CapType' lg=$D.FEED.LANG length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['CapType'] var=$VAR.ATTRIBUTE.D['CapType']}
				
				{if $D.FEED.ARTICLE.ATTRIBUTE.D['AttributeGroup'].VALUE == 'Lighting'}{*ToDo: eine notlösung wen AttributeGroup nicht gesetzt ist.*}
				{$vgid = explode("|",$D.FEED.ARTICLE.VARIANTE_GROUP_ID)}
				{foreach name=gid key=kgid item=gid from=$vgid}
					{if $VAR.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE || $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
						{$val = ($VAR.ATTRIBUTE.D[$gid])? $VAR.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE : $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
					{else}
						{$val = ($VAR.ATTRIBUTE.D[$gid])? $VAR.ATTRIBUTE.D[$gid].VALUE : $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].VALUE}
					{/if}
					{$Color.LANGUAGE.D[ $D.FEED.LANG ].VALUE = ($Color.LANGUAGE.D[ $D.FEED.LANG ].VALUE)?"{$Color.LANGUAGE.D[ $D.FEED.LANG ].VALUE} - {$val}":$val}
				{/foreach}
					{attribute el='Color' lg=$D.FEED.LANG length=50 parent=null var=$Color}
				{else}
					{attribute el='Color' lg=$D.FEED.LANG length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Color'] var=$VAR.ATTRIBUTE.D['Color']}
				{/if}
				{*attribute el='ColorMap' lg=$D.FEED.LANG length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['ColorMap'] var=$VAR.ATTRIBUTE.D['ColorMap']*}
				
				{attribute el='ColorRenderingIndex' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['ColorRenderingIndex'] var=$VAR.ATTRIBUTE.D['ColorRenderingIndex']}
				{attribute el='ColorTemperature' lg=$D.FEED.LANG type='number' unitOfMeasure='kelvin' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['ColorTemperature'] var=$VAR.ATTRIBUTE.D['ColorTemperature']}
				
				{attribute el='IncludedComponent' lg=$D.FEED.LANG maxOccurs=5 delimiter="\n" length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'] var=$VAR.ATTRIBUTE.D['IncludedComponent']}
				
				{attribute el='InternationalProtectionRating' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['InternationalProtectionRating'] var=$VAR.ATTRIBUTE.D['InternationalProtectionRating']}
				{attribute el='LampStartupTime' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['LampStartupTime'] var=$VAR.ATTRIBUTE.D['LampStartupTime']}
				{attribute el='LampWarmupTime' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['LampWarmupTime'] var=$VAR.ATTRIBUTE.D['LampWarmupTime']}
				{attribute el='LightingMethod' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['LightingMethod'] var=$VAR.ATTRIBUTE.D['LightingMethod']}
				{attribute el='Material' lg=$D.FEED.LANG length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Material'] var=$VAR.ATTRIBUTE.D['Material']}
				{attribute el='PlugType' lg=$D.FEED.LANG length=100 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['PlugType'] var=$VAR.ATTRIBUTE.D['PlugType']}
				
				{attribute el='SpecialFeatures' lg=$D.FEED.LANG maxOccurs=5 delimiter="\n" length=50 parent=$D.FEED.ARTICLE.ATTRIBUTE.D['SpecialFeatures'] var=$VAR.ATTRIBUTE.D['SpecialFeatures']}
				{attribute el='SpecificUses' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['SpecificUses'] var=$VAR.ATTRIBUTE.D['SpecificUses']}
				{attribute el='StyleName' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['StyleName'] var=$VAR.ATTRIBUTE.D['StyleName']}
				{attribute el='Voltage' lg=$D.FEED.LANG type='number' unitOfMeasure='volts' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Voltage'] var=$VAR.ATTRIBUTE.D['Voltage']}
				{*attribute el='Wattage' lg=$D.FEED.LANG type='number' unitOfMeasure='watts' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Wattage'] var=$VAR.ATTRIBUTE.D['Wattage']*}
				{*attribute el='WeightedAnnualEnergyConsumption' lg=$D.FEED.LANG type='number' unitOfMeasure='watt_hours' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Wattage'] var=$VAR.ATTRIBUTE.D['Wattage']*}
				{attribute el='FixtureType' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['FixtureType'] var=$VAR.ATTRIBUTE.D['FixtureType']}
				{attribute el='Efficiency' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Efficiency'] var=$VAR.ATTRIBUTE.D['Efficiency']}
				{attribute el='Efficiency' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['efficiency'] var=$VAR.ATTRIBUTE.D['efficiency']}
				
				{*$D.FEED.ARTICLE.ATTRIBUTE.D['EuEnergyLabelEfficiencyClass'].LANGUAGE.D['DE']['VALUE'] = 'a_to_g'*}
				{*attribute el='EuEnergyLabelEfficiencyClass' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['EuEnergyLabelEfficiencyClass']['VALUE'] var=$D.FEED.ARTICLE.ATTRIBUTE.D['EuEnergyLabelEfficiencyClass']['VALUE']*}
				{*<EuEnergyEfficiencyRating>not_rated</EuEnergyEfficiencyRating>*}
				{*ATTRIBUTE ENDE*}

		</LightsAndFixtures>
	</ProductType>
</Lighting>