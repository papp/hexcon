<Message>
	<MessageID>{$D.FEED.INDEX}0</MessageID>
	<Price>
		<SKU>{$D.FEED.ARTICLE.NUMBER}</SKU>
		{*if !$D.FEED.ARTICLE.VARIANTE*}
		<StandardPrice currency="{if $D.FEED.LANG == 'EN'}GBP{else}EUR{/if}">{$D.FEED.ARTICLE.PRICE}</StandardPrice>
		{*/if*}
	</Price>
</Message>
{foreach name=VAR key=kVAR item=VAR from=$D.FEED.ARTICLE.VARIANTE.D}
	{if $VAR.ACTIVE > 0}
<Message>
	<MessageID>{$D.FEED.INDEX}0{$smarty.foreach.VAR.iteration}</MessageID>
	<Price>
		<SKU>{$VAR.NUMBER}</SKU>
		<StandardPrice currency="{if $D.FEED.LANG == 'EN'}GBP{else}EUR{/if}">{$VAR.PRICE}</StandardPrice>
	</Price>
</Message>
	{/if}
{/foreach}