{function name=attribute}{strip}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE || $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
		{$VALUE = ($var.LANGUAGE.D[$lg].VALUE)?$var.LANGUAGE.D[$lg].VALUE:$var.VALUE}
	{else}
		{$VALUE = ($parent.LANGUAGE.D[$lg].VALUE)?$parent.LANGUAGE.D[$lg].VALUE:$parent.VALUE}
	{/if}

	{if $VALUE}
		{$VALUE = str_replace('|',' - ',$VALUE)}
		{*$VALUE = ($type=='int')?(int)$VALUE:$VALUE*}

		{if $maxOccurs > 1 && $delimiter}
			{$aValue = explode($delimiter,$VALUE)}
			{foreach name=VAL key=kVAL item=VAL from=$aValue}
				{if $smarty.foreach.VAL.iteration <= $maxOccurs}
					{if $length}
						{$VAL = $VAL|truncate:$length:"":true}
					{/if}
					
					{$VAL = ($type == 'number')?(int)$VAL:$VAL}
					
					{$VAL = str_replace("\r","",$VAL)}
				{if $VAL}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VAL}]]></{$el}>{/if}
				{/if}
			{/foreach}
		{else}
			{if $length}
				{$VALUE = $VALUE|truncate:$length:"":true}
			{/if}
			{$VALUE = ($type == 'number')?(int)$VALUE:$VALUE}
			{if $VALUE}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VALUE}]]></{$el}>{/if}
			
			{if $el == 'Color'}
				{$ColorMap = 'white'}
				{$ColorMap = (strpos(strtolower($VALUE),'schwarz') !== false)?'black':$ColorMap}
				{$ColorMap = (strpos(strtolower($VALUE),'silber') !== false)?'white':$ColorMap}
				<ColorMap>{$ColorMap}</ColorMap>
			{/if}
		{/if}
	{/if}

{/if}
{/strip}{/function}

{if strpos(strtolower($D.FEED.ARTICLE.VARIANTE_GROUP_ID),'color') !== false && strpos(strtolower($D.FEED.ARTICLE.VARIANTE_GROUP_ID),'size') !== false}
	{$VariationTheme = 'Size-Color'}
{elseif strpos(strtolower($D.FEED.ARTICLE.VARIANTE_GROUP_ID),'color')!== false}
	{$VariationTheme = 'Color'}
{else}
	{$VariationTheme = 'Size'}
{/if}

<HomeImprovement>
	<ProductType>
		<Hardware>
			{if $D.FEED.ARTICLE.VARIANTE}
					<VariationData>
						<Parentage>{if !$kVAR}parent{else}child{/if}</Parentage>
						<VariationTheme>{$VariationTheme}</VariationTheme>
					</VariationData>
			{/if}		
			{*ToDo Color
					{if $VAR.ATTRIBUTE.D['Color'].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
						<Color>{str_replace('|',' - ',$VAR.ATTRIBUTE.D['Color'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)}</Color>
						<ColorMap>{str_replace('|',' - ',$VAR.ATTRIBUTE.D['Color'].LANGUAGE.D[ $D.FEED.LANG ].VALUE)}</ColorMap>
					{/if}
			*}
			{*$dATT = ['Color']}
			{foreach name=ATT key=kATT item=ATT from=$dATT}
				{attribute el=$ATT lg= $D.FEED.LANG  parent=$D.FEED.ARTICLE.ATTRIBUTE.D[$ATT] var=$VAR.ATTRIBUTE.D[$ATT]}
			{/foreach*}

			{$vgid = explode("|",$D.FEED.ARTICLE.VARIANTE_GROUP_ID)}
			{foreach name=gid key=kgid item=gid from=$vgid}
				{if $VAR.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE || $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
					{$val = ($VAR.ATTRIBUTE.D[$gid])? $VAR.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE : $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].LANGUAGE.D[ $D.FEED.LANG ].VALUE}
				{else}
					{$val = ($VAR.ATTRIBUTE.D[$gid])? $VAR.ATTRIBUTE.D[$gid].VALUE : $D.FEED.ARTICLE.ATTRIBUTE.D[$gid].VALUE}
				{/if}
				{$Att.LANGUAGE.D[ $D.FEED.LANG ].VALUE = ($Att.LANGUAGE.D[ $D.FEED.LANG ].VALUE)?"{$Att.LANGUAGE.D[ $D.FEED.LANG ].VALUE} - {$val}":$val}
			{/foreach}
			{if $VariationTheme == 'Color'}
				{attribute el='Color' lg=$D.FEED.LANG length=50 parent=null var=$Att}
			{else}
				{attribute el='Size' lg=$D.FEED.LANG length=50 parent=null var=$Att}
			{/if}


			
			{if $D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode'].VALUE}{*HSNTSN*}
				{attribute el='VehicleFitmentCode' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['VehicleFitmentCode']}{*max2000*}
				<VehicleFitmentStandard>KBA</VehicleFitmentStandard>
			{/if}

			
			{attribute el='Material' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Material'] var=$VAR.ATTRIBUTE.D['Material']}
			{*
			{attribute el='Voltage' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Voltage'] var=$VAR.ATTRIBUTE.D['Voltage']}
			{attribute el='Wattage' lg=$D.FEED.LANG parent=$D.FEED.ARTICLE.ATTRIBUTE.D['Wattage'] var=$VAR.ATTRIBUTE.D['Wattage']}
			*}
		</Hardware>
	</ProductType>
</HomeImprovement>