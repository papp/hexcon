{*http://g-ecx.images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/Office.xsd*}
{*ColorMap Erlaubte Farben
beige, black, blue, brass, bronze, brown, burst, chrome, clear, gold, gray, green, metallic, multi-colored, natural, off-white, orange, pink, purple, red, silver, white, yellow
*}
{function name=attribute}{strip}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE || $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
		{$VALUE = ($var.LANGUAGE.D[$lg].VALUE)?$var.LANGUAGE.D[$lg].VALUE:$var.VALUE}
	{else}
		{$VALUE = ($parent.LANGUAGE.D[$lg].VALUE)?$parent.LANGUAGE.D[$lg].VALUE:$parent.VALUE}
	{/if}

	{if $VALUE}
		{$VALUE = str_replace('|',' - ',$VALUE)}
		{*$VALUE = ($type=='int')?(int)$VALUE:$VALUE*}

		{if $maxOccurs > 1 && $delimiter}
			{$aValue = explode($delimiter,$VALUE)}
			{foreach name=VAL key=kVAL item=VAL from=$aValue}
				{if $smarty.foreach.VAL.iteration <= $maxOccurs}
					{if $length}
						{$VAL = $VAL|truncate:$length:"":true}
					{/if}
					
					{$VAL = ($type == 'number')?(int)$VAL:$VAL}
					
					{$VAL = str_replace("\r","",$VAL)}
				{if $VAL}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VAL}]]></{$el}>{/if}
				{/if}
			{/foreach}
		{else}
			{if $length}
				{$VALUE = $VALUE|truncate:$length:"":true}
			{/if}
			{$VALUE = ($type == 'number')?(int)$VALUE:$VALUE}
			{if $VALUE}<{$el}{if $unitOfMeasure} unitOfMeasure="{$unitOfMeasure}"{/if}><![CDATA[{$VALUE}]]></{$el}>{/if}
			
			{if $el == 'Color'}
				<ColorMap>white</ColorMap>
			{/if}
		{/if}
	{/if}

{/if}
{/strip}{/function}
<Office>
	<ProductType>		
		<EducationalSupplies>
			{if $D.FEED.ARTICLE.VARIANTE.D}
				<VariationData>
					<Parentage>{if !$kVAR}parent{else}child{/if}</Parentage>
					<VariationTheme>Size</VariationTheme>
				</VariationData>
			{/if}
			{*
				<ColorSpecification>
					<Color></Color>
					<ColorMap></ColorMap>
				</ColorSpecification>
			*}
				
		</EducationalSupplies>
	</ProductType>
	
	{attribute el='Size' lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D['SIZE'] var=$VAR.ATTRIBUTE.D['SIZE']}
</Office>