<AutoAccessory>
	<ProductType>
		<AutoPart>
			{if $D.FEED.ARTICLE.VARIANTE.D}
				{if !$kVAR}{*PARENT ARTIKEL*}
				<VariationData>
					<Parentage>parent</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
				{else}{*KIND ARTIKEL*}
				<VariationData>
					<Parentage>child</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
				<ColorSpecification>
					<Color>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D['DE'].ATTRIBUTE.D}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</Color>
					<ColorMap>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D['DE'].ATTRIBUTE.D}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</ColorMap>
				</ColorSpecification>
				{/if}
			{/if}
			<VehicleFitmentCode>{$D.FEED.ARTICLE.LANGUAGE.D['DE'].ATTRIBUTE.D['HSNTSN'].VALUE|replace:"|":","}</VehicleFitmentCode>{*max2000*}
			<VehicleFitmentStandard>KBA</VehicleFitmentStandard>
		</AutoPart>
	</ProductType>
</AutoAccessory>