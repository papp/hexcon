<Office>
	<ProductType>
		<OfficeProducts>
			{if $D.FEED.ARTICLE.VARIANTE.D}
				{if !$kVAR}{*PARENT ARTIKEL*}
				<VariationData>
					<Parentage>parent</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
				{else}{*KIND ARTIKEL*}
				<VariationData>
					<Parentage>child</Parentage>
					<VariationTheme>Color</VariationTheme>
				</VariationData>
				<ColorSpecification>
					<Color>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D['DE'].ATTRIBUTE.D}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</Color>
					<ColorMap>{foreach name=ATT key=kATT item=ATT from=$VAR.LANGUAGE.D['DE'].ATTRIBUTE.D}{$ATT.VALUE}{if !$smarty.foreach.ATT.last} | {/if}{/foreach}</ColorMap>
				</ColorSpecification>
				{/if}
			{/if}
		</OfficeProducts>
	</ProductType>
</Office>