{*http://g-ecx.images-amazon.com/images/G/01/rainier/help/xsd/release_4_1/Home.xsd*}
{function name=attribute}{strip}
{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE || $parent.LANGUAGE.D[$lg].VALUE || $parent.VALUE}
	{if $var.LANGUAGE.D[$lg].VALUE || $var.VALUE}
		{$VALUE = ($var.LANGUAGE.D[$lg].VALUE)?$var.LANGUAGE.D[$lg].VALUE:$var.VALUE}
	{else}
		{$VALUE = ($parent.LANGUAGE.D[$lg].VALUE)?$parent.LANGUAGE.D[$lg].VALUE:$parent.VALUE}
	{/if}

	{$VALUE = str_replace('|',' - ',$VALUE)}
	{$VALUE = ($type=='int')?(int)$VALUE:$VALUE}

	<{$el}{if $el == 'Voltage'} unitOfMeasure="volts"{elseif $el == 'ColorTemperature'} unitOfMeasure="kelvin"{elseif $el == 'BulbLifeSpan'} unitOfMeasure="hr"{/if}><![CDATA[{$VALUE}]]></{$el}>
	


{/if}
{/strip}{/function}
<Home>
	<ProductType>		
		<BedAndBath>
			{if $D.FEED.ARTICLE.VARIANTE.D}
                {if $kVAR && strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color')!== false}
                <ColorMap>white</ColorMap>
                {/if}
				<VariationData>
					<VariationTheme>{if strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color') !== false && strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Size') !== false}Size-Color{elseif strpos($D.FEED.ARTICLE.VARIANTE_GROUP_ID,'Color')!== false}Color{else}Size{/if}</VariationTheme>
                 
                    {$dATT = ['Size','Color','Shape']}
                    {foreach name=ATT key=kATT item=ATT from=$dATT}
                        {attribute el=$ATT lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D[$ATT] var=$VAR.ATTRIBUTE.D[$ATT]}
                    {/foreach}
                    
                </VariationData>
                {$dATT = ['Wattage','Efficiency']}
                {foreach name=ATT key=kATT item=ATT from=$dATT}
                    {attribute el=$ATT lg='DE' parent=$D.FEED.ARTICLE.ATTRIBUTE.D[$ATT] var=$VAR.ATTRIBUTE.D[$ATT]}
                {/foreach}
			{/if}
				
            
		</BedAndBath>
	</ProductType>
    <Parentage>{if !$kVAR}parent{else}child{/if}</Parentage>
	{if $D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D['DE'].VALUE}
	<IncludedComponents>{str_replace(["\n","\r"],['',', '],$D.FEED.ARTICLE.ATTRIBUTE.D['IncludedComponent'].LANGUAGE.D['DE'].VALUE)}</IncludedComponents>
	{/if}
</Home>