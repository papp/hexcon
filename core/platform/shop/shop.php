<?php
class shop extends platform
{
	function __construct($account_id, $platform_id)
	{
		parent::__construct($account_id, $platform_id);
		global $SQL, $CCache;
		#$this->SQL = $SQL;
		#$this->CCache = $CCache;
		$this->platform_id = $platform_id;
		$this->account_id = $account_id;
	}
	
	static function info(&$D=null)
	{
		$CLASS = get_class();
		$FUNCTION = get_class_methods($CLASS);
		$D['CLASS'][ $CLASS ] = [
			'NAME'		=> 'shop',
			'VERSION'	=> 1.00,
			'ICON'		=> 'icon.svg',
			'FUNCTION'	=> $FUNCTION,
		];
	}
	
	#ToDo: Veraltet? aktuelle Funktion in Account::set_warehouse(); 
	function set_warehouse(&$D=null)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['WAREHOUSE']['D'] AS $kWAR => $WAR)
		{
			if($WAR['ACTIVE'] != -2)
			{
				$IU_warehouse .= (($IU_warehouse)?',':'')."('{$kWAR}','{$this->platform_id}'";
				$IU_warehouse .= (isset($WAR['ACTIVE']))? ",'{$WAR['ACTIVE']}'":",NULL";
				$IU_warehouse .= (isset($WAR['TITLE']))? ",'{$WAR['TITLE']}'":",NULL";
				$IU_warehouse .= ")";
				
				foreach((array)$WAR['STORAGE']['D'] AS $kSTO => $STO)
				{
					if($STO['ACTIVE'] != -2)
					{
						$IU_storage .= (($IU_storage)?',':'')."('{$kSTO}','{$kWAR}'";
						$IU_storage .= (isset($STO['ACTIVE']))? ",'{$STO['ACTIVE']}'":",NULL";
						$IU_storage .= (isset($STO['TITLE']))? ",'{$STO['TITLE']}'":",NULL";
						$IU_storage .= (isset($STO['SORT']))? ",'{$STO['SORT']}'":",NULL";
						$IU_storage .= ")";
						
						foreach((array)$STO['ARTICLE']['D'] AS $kART => $ART)
						{
							if($ART['ACTIVE'] != -2)
							{
								$IU_article .= (($IU_article)?',':'')."('{$kART}','{$kSTO}','{kWAR}'";
								$IU_article .= (isset($ART['ACTIVE']))? ",'{$ART['ACTIVE']}'":",NULL";
								$IU_article .= (isset($ART['STOCK']))? ",'{$ART['STOCK']}'":",NULL";
								$IU_article .= ")";
							}
							else
								$D_article .= (($D_article)?',':'')."'{$kWAR}{$kSTO}{$kART}'";
						}
					}
					else
						$D_storage .= (($D_storage)?',':'')."'{$kWAR}{$kSTO}'";
				}
			}
			else
				$D_warehouse .= (($D_warehouse)?',':'')."'{$kWAR}'";
		}	
		
		if($IU_warehouse)
			$this->SQL->query("INSERT INTO wp_warehouse (id,platform_id,active,title) VALUES {$IU_warehouse} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_warehouse.active END,
							title = CASE WHEN excluded.title IS NOT NULL THEN excluded.title ELSE wp_warehouse.title END
						");
		if($IU_storage)
			$this->SQL->query("INSERT INTO wp_warehouse_storage (id,warehouse_id,active,title,sort) VALUES {$IU_storage} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_warehouse_storage.active END,
							title = CASE WHEN excluded.title IS NOT NULL THEN excluded.title ELSE wp_warehouse_storage.title END,
							sort = CASE WHEN excluded.sort IS NOT NULL THEN excluded.sort ELSE wp_warehouse_storage.sort END
						");
		if($IU_article)
			$this->SQL->query("INSERT INTO wp_article_stock (article_id,storage_id,warehouse_id,active,stock) VALUES {$IU_article} 
					ON DUPLICATE KEY UPDATE 
						active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_article_stock.active END,
						itimestamp = CASE WHEN excluded.stock) > 0 AND wp_article_stock.stock < 1 THEN now( ELSE wp_article_stock.itimestamp END,
						stock = CASE WHEN excluded.stock IS NOT NULL THEN excluded.stock ELSE wp_article_stock.stock END
					");#ToDo: Hotfix: itimestamp wird auf aktuelles Datum gesetzt sobald ein Bestand von 0 auf n erhöht wird.
		
		if($IU_warehouse)
		{
			$this->SQL->query("DELETE FROM wp_warehouse WHERE id IN ({$IU_warehouse}) ");
			$this->SQL->query("DELETE FROM wp_warehouse_storage WHERE CONCAT(warehouse_id) IN ({$IU_warehouse}) ");
			$this->SQL->query("DELETE FROM wp_article_stock WHERE CONCAT(warehouse_id) NOT IN ({$IU_warehouse}) ");
		}
		if($IU_storage)
		{
			$this->SQL->query("DELETE FROM wp_warehouse_storage WHERE CONCAT(warehouse_id,id) IN ({$IU_storage}) ");
			$this->SQL->query("DELETE FROM wp_article_stock WHERE CONCAT(warehouse_id,storage_id) NOT IN ({$IU_storage}) ");
		}

		if($D_warehouse)
		{
			$this->SQL->query("DELETE FROM wp_warehouse WHERE id IN ({$D_warehouse})  AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_warehouse_storage WHERE warehouse_id NOT IN (SELECT id FROM wp_warehouse ) ");
			$this->SQL->query("DELETE FROM wp_article_stock WHERE warehouse_id NOT IN (SELECT id FROM wp_warehouse) ");
		}

		if($D_article)
			$this->SQL->query("DELETE FROM wp_article_stock WHERE CONCAT(warehouse_id,storage_id,article_id) IN ({$D_article}) ");
		
		#Update utime beim artikel
		$this->SQL->query("UPDATE wp_article a, wp_article_stock ass SET a.utimestamp = ass.utimestamp WHERE a.id = ass.article_id AND a.utimestamp < ass.utimestamp");
	}

	function get_warehouse(&$D=null) {
		/*
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['WAREHOUSE']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['WAREHOUSE']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id ID, parent_id PARENT_ID, platform_id PLATFORM_ID, attribute_id ATTRIBUTE_ID, active ACTIVE, value VALUE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
							FROM wp_warehouse2
							WHERE platform_id = '{$this->platform_id}'
							AND id IN (SELECT id FROM wp_warehouse2 WHERE attribute_id = 'Type' AND value = 'Warehouse')
							{$W}
							ORDER BY value");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['WAREHOUSE']['D'][ $a['ID'] ]['ATTRIBUTE']['D'][ $a['ATTRIBUTE_ID'] ] = $a;
		}
		*/
	}
	
	function get_storage(&$D=null) {
		/*
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['W']['WAREHOUSE_ID'])? " AND parent_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['W']['WAREHOUSE_ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['W']['ID']}')":'';
		
		$qry = $this->SQL->query("SELECT id ID, parent_id PARENT_ID, platform_id PLATFORM_ID, attribute_id ATTRIBUTE_ID, active ACTIVE, value VALUE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
							FROM wp_warehouse2
							WHERE platform_id = '{$this->platform_id}'
							AND id IN (SELECT id FROM wp_warehouse2 WHERE attribute_id = 'Type' AND value = 'Storage')
							");

		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['D'][  $a['ID']  ]['ATTRIBUTE']['D'][ $a['ATTRIBUTE_ID'] ] = $a;
		}
		
		$qry = $this->SQL->query("SELECT id ID, parent_id PARENT_ID, platform_id PLATFORM_ID, attribute_id ATTRIBUTE_ID, active ACTIVE, value VALUE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
							FROM wp_warehouse2
							WHERE platform_id = '{$this->platform_id}'
							AND id IN (SELECT id FROM wp_warehouse2 WHERE attribute_id = 'Type' AND value = 'Article')
							
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['STORAGE']['D'][ $a['PARENT_ID'] ]['ARTICLE']['D'][ $a['ID'] ]['ATTRIBUTE']['D'][ $a['ATTRIBUTE_ID'] ] = $a;
		}
		*/
	}

	function get_shipping(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['SHIPPING']['W']['ID'])? " AND s.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SHIPPING']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_shipping s 
							WHERE platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['SHIPPING']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
		}
	}

	#ToDo: Veraltet
	/*
	function get_country(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['COUNTRY']['W']['ID'])? " AND c.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['COUNTRY']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_country c 
							WHERE 1");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['COUNTRY']['D'][ $a['id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
	}
	*/

	function get_article(&$D=null)
	{
		$ARTICLE = &$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE'];
		//ToDo: einen Finalen Interpreter erstellen, und diese untere Abfragen mit berücksichtigen!
		
		$W .= CWP::where_interpreter([
			'ID:IN'					=> "a.id IN ([ID:IN])",
			'ID:NOTIN'				=> "a.id NOT IN ([ID:NOTIN])",
			'ACTIVE'				=> "a.active = [ACTIVE]",
			'ATTRIBUTE_ID:NOTIN'	=> "a.id NOT IN (SELECT id FROM wp_article ab INNER JOIN wp_article_attribute aa ON article_id = ab.id WHERE attribute_id = '[ATTRIBUTE_ID:NOTIN]' AND ab.platform_id = aa.platform_id AND aa.platform_id = '{$this->platform_id}' )",
			'ATTRIBUTE_ID:IN'		=> "a.id IN (SELECT id FROM wp_article ab INNER JOIN wp_article_attribute aa ON article_id = ab.id WHERE attribute_id = '[ATTRIBUTE_ID:NOTIN]' AND ab.platform_id = aa.platform_id AND aa.platform_id = '{$this->platform_id}' )",
			'NUMBER:IN'				=> "a.number IN ([NUMBER:IN])",
			#'CATEGORIE_ID'			=> "EXISTS (SELECT 1 FROM wp_categorie_article WHERE categorie_id IN ('[CATEGORIE_ID]') AND platform_id = '{$this->platform_id}' AND article_id = a.id )",
			'ATTRIBUTECategorieId'	=> "EXISTS (SELECT 1 FROM wp_article_attribute WHERE attribute_id = 'CategorieId' AND value IN ('[ATTRIBUTECategorieId]') AND platform_id = '{$this->platform_id}' AND article_id = a.id )",//ToDo: Vorübergehend, muss durch Attribute Filter ersetzt werden
			'ID'					=> "a.id IN ('[ID]')",
			'STORAGE_ID:IN'			=> "a.id IN (SELECT article_id FROM wp_article_stock WHERE storage_id IN ([STORAGE_ID:IN]))",
			##'SETID:IN'				=> "a.id IN (SELECT contain_article_id FROM wp_article_set WHERE article_id IN ([SETID:IN]) ) ",
			'PARENT_ID:NOTIN'		=> "(a.parent_id NOT IN ([PARENT_ID:NOTIN]) OR a.parent_id IS NULL)",
			'PARENT_ID'				=> "a.parent_id IN ('[PARENT_ID]')",#ToDo: Überdenken
			'ID2PARENT'				=> "a.id IN (SELECT parent_id FROM wp_article WHERE id IN ('[ID2PARENT]') AND platform_id = '{$this->platform_id}')",#Ist wichtig um anhand einer Variation das Komplette Parent mit ALLEN variationen zu hollen
			'NUMBER:LIKE'			=> "a.number LIKE '[NUMBER:LIKE]'",
			'EAN:LIKE'				=> "a.ean LIKE '[EAN:LIKE]'",
			'TITLE:LIKE'			=> "a.id IN (SELECT id FROM wp_article ab INNER JOIN wp_article_attribute aa ON article_id = ab.id WHERE attribute_id = 'TITLE' AND ab.platform_id = aa.platform_id AND aa.platform_id = '{$this->platform_id}'  AND value LIKE '[TITLE:LIKE]')",
			#'TITLE:LIKE'			=> "EXISTS (SELECT 1 FROM wp_article ab INNER JOIN wp_article_attribute aa ON article_id = ab.id WHERE id = a.id AND attribute_id = 'TITLE' AND ab.platform_id = aa.platform_id AND aa.platform_id = '{$this->platform_id}'  AND value LIKE '[TITLE:LIKE]' )",
			#'TO_PLATFORM_ID'		=> "a.id IN (SELECT article_id FROM wp_categorie_article ca LEFT JOIN wp_article_reference ar ON  ca.platform_id = ar.from_platform_id AND ca.article_id = ar.from_article_id WHERE ar.to_platform_id IN ('[TO_PLATFORM_ID]') )", #Gib alle Artikel die zur TO_PLF zugeordnet sind
			'TO_PLATFORM_ID'		=> "a.id IN (SELECT aa.article_id 
			FROM wp_article_attribute aa
			LEFT JOIN wp_article_reference ar ON  aa.platform_id = ar.from_platform_id AND aa.article_id = ar.from_article_id 
			WHERE aa.attribute_id = 'CategorieId'
			AND ar.to_platform_id IN ('[TO_PLATFORM_ID]') )", #Gib alle Artikel die zur TO_PLF zugeordnet sind'TO_PLATFORM_ID_UPDATE'	=> "a.id IN (SELECT from_article_id FROM wp_article_reference WHERE active <> 0 AND (fail < 3 OR fail IS NULL) AND to_platform_id IN ('[TO_PLATFORM_ID_UPDATE]') AND (utimestamp < a.utimestamp OR utimestamp*1 < ".date('YmdHis', strtotime('-30 days'))." ) AND from_platform_id = a.platform_id)", #Gib alle Artikel die zur TO_PLF zugeordnet sind und aktueller als übertragen sind ODER wenn die referenz Datum älter als 30Tage ist, soll erneut ein Update gesendet werden.
			
			
			##'TO_PLATFORM_ID'		=> "a.id IN (SELECT article_id FROM wp_categorie_article ca LEFT JOIN wp_platform_platform pp ON ca.platform_id = pp.platform_from_id WHERE platform_to_id IN ('[TO_PLATFORM_ID]') AND pp.type='article')", #Gib alle Artikel die zur TO_PLF zugeordnet sind
			##'TO_PLATFORM_ID_UPDATE'	=> "a.id IN (SELECT from_article_id FROM wp_article_reference WHERE active <> 0 AND (fail < 3 OR fail IS NULL) AND to_platform_id IN ('[TO_PLATFORM_ID_UPDATE]') AND (utimestamp < a.utimestamp OR utimestamp*1 < ".date('YmdHis', strtotime('-30 days'))." ) AND from_platform_id = a.platform_id)", #Gib alle Artikel die zur TO_PLF zugeordnet sind und aktueller als übertragen sind ODER wenn die referenz Datum älter als 30Tage ist, soll erneut ein Update gesendet werden.
			
			#'TO_PLATFORM_ID_UPDATE'	=> "a.utimestamp > IFNULL( (SELECT MIN(utimestamp) FROM wp_article_reference WHERE to_platform_id IN ('[TO_PLATFORM_ID_UPDATE]') AND from_article_id = a.id AND from_platform_id = a.platform_id AND account_id = a.account_id), 0)", #Gib alle Artikel die zur TO_PLF zugeordnet sind und aktueller als übertragen sind und die noch keine referenz haben
			'SUPPLIER_ID:IN'		=> "a.id IN (SELECT article_id FROM wp_supplier_to_article WHERE supplier_id IN ('[SUPPLIER_ID:IN]') AND platform_id = '{$this->platform_id}')",
		],$ARTICLE['W']);
		
		foreach((array)$ARTICLE['O'] AS $kO => $O) {#ORDER
			$ORDER .= "{$kO} {$O}, ";
		}

		$L .= (isset($ARTICLE['L']['START']) && $ARTICLE['L']['STEP'])? " LIMIT {$ARTICLE['L']['START']},{$ARTICLE['L']['STEP']} ":'';
		#ARTICLE START ===========================
		#Count
		
		$qry = $this->SQL->query("SELECT count(id) COUNT
				FROM wp_article a
				WHERE parent_id IS NULL AND platform_id = '{$this->platform_id}' {$W}");
		$a = $qry->fetchArray(SQLITE3_ASSOC);
		$ARTICLE['COUNT']['TOTAL'] = $a['COUNT'];
		
		$qry = $this->SQL->query("SELECT count(id) COUNT
				FROM wp_article a
				INNER JOIN 
				(
					SELECT parent_id
					FROM wp_article a
					WHERE a.parent_id IS NOT NULL AND a.platform_id = '{$this->platform_id}' {$W} 
				) AS va ON va.parent_id = a.id");
		$a = $qry->fetchArray(SQLITE3_ASSOC);
		$ARTICLE['COUNT']['TOTAL'] += $a['COUNT'];

		#Gib mir alle Vater
		$qry = $this->SQL->query("SELECT id, a.parent_id PARENT_ID, platform_id PLATFORM_ID, active ACTIVE, number NUMBER, ean EAN, weight WEIGHT, price PRICE, rprice RPRICE, vat VAT, sort SORT, variante_group_id VARIANTE_GROUP_ID, utimestamp*1 AS UTIMESTAMP, itimestamp*1 ITIMESTAMP
				FROM wp_article a
				WHERE a.parent_id IS NULL AND platform_id = '{$this->platform_id}' {$W} ORDER BY {$ORDER} SORT, ITIMESTAMP DESC {$L}
				");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$ARTICLE['D'][ $a['id'] ] = $a;
			$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ] = &$ARTICLE['D'][ $a['id'] ];
			$ARTICLE['CHILD']['D'][ $a['id'] ] = &$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ];#ToDo: CHILD ist nicht notwendig, kann dierekt so zu gegriffen werden: $ARTICLE['D'][ $a['id'] ]
		}

		#Gib mir alle Parent wenn die gesuchte ID variante ist.
		$qry = $this->SQL->query("SELECT id, a.parent_id PARENT_ID, platform_id PLATFORM_ID, active ACTIVE, number NUMBER, ean EAN, weight WEIGHT, price PRICE, rprice RPRICE, vat VAT, sort SORT, variante_group_id VARIANTE_GROUP_ID, utimestamp*1 AS UTIMESTAMP, itimestamp*1 ITIMESTAMP
				FROM wp_article a
				INNER JOIN 
				(
					SELECT parent_id
					FROM wp_article a
					WHERE a.parent_id IS NOT NULL AND a.platform_id = '{$this->platform_id}' {$W} 
				) AS va ON va.parent_id = a.id
				
				WHERE a.parent_id IS NULL AND platform_id = '{$this->platform_id}' {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$ARTICLE['D'][ $a['id'] ] = $a;
			$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ] = &$ARTICLE['D'][ $a['id'] ];
			$ARTICLE['CHILD']['D'][ $a['id'] ] = &$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ];#ToDo: CHILD ist nicht notwendig, kann dierekt so zu gegriffen werden: $ARTICLE['D'][ $a['id'] ]
		}
		
		#Gib mir alle Variationen wenn gesuchte ID Parent ID ist
		$ID = implode("','",array_keys((array)$ARTICLE['D']));
		$qry = $this->SQL->query("SELECT b.id, parent_id PARENT_ID, platform_id PLATFORM_ID, active ACTIVE, number NUMBER, ean EAN, weight WEIGHT, price PRICE, rprice RPRICE, vat VAT, sort SORT, variante_group_id VARIANTE_GROUP_ID, utimestamp*1 AS UTIMESTAMP, itimestamp*1 ITIMESTAMP
				FROM wp_article b
				WHERE b.parent_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$ARTICLE['D'][ $a['id'] ] = $a;
			$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ] = &$ARTICLE['D'][ $a['id'] ];
			$ARTICLE['CHILD']['D'][ $a['id'] ] = &$ARTICLE['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['id'] ];#ToDo: CHILD ist nicht notwendig, kann dierekt so zu gegriffen werden: $ARTICLE['D'][ $a['PARENT_ID'] ]
		}
		#ARTICLE ENDE ===========================
		if($ARTICLE['D'] ) {
			$ID = implode("','",array_keys((array)$ARTICLE['D']));
			#FILE START ===============================
			$qry = $this->SQL->query("SELECT platform_id, article_id, file_id, active ACTIVE, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP, sort SORT
										FROM wp_article_file
										WHERE article_id IN ('{$ID}')
											AND platform_id = '{$this->platform_id}' 
											
										ORDER BY sort");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['FILE']['D'][ $a['file_id'] ] = $a;
			}
			#FILE ENDE ================================
			#ATTRIBUTE START ==========================
			$qry = $this->SQL->query("SELECT article_id, attribute_id, aa.language_id, aa.active ACTIVE, aa.value VALUE, aa.utimestamp*1 UTIMESTAMP, aa.itimestamp*1 ITIMESTAMP
										FROM wp_article_attribute aa 
										WHERE article_id IN ('{$ID}')
											
											AND	aa.platform_id = '{$this->platform_id}'");
											
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) { #Type: ATTRIBUTE,META,DESCRIPTION ;
				if( $a['language_id'] ) {
					#$ARTICLE['D'][ $a['article_id'] ]['ATTRIBUTETYPE']['D'][$a['type']]['ATTRIBUTE']['D'][ $a['attribute_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;#ToDo: Alte Struktur
					$ARTICLE['D'][ $a['article_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;#neue Struktur
				}
				else {
					#$ARTICLE['D'][ $a['article_id'] ]['ATTRIBUTETYPE']['D'][$a['type']]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;#ToDo: Alte Struktur
					$ARTICLE['D'][ $a['article_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;#neue Struktur
				}
				#ToDo: $a['type'] wird vermutlich nicht benötigt. Die Struktur kann von der Platform entnohmen werden.
			}
			#ATTRIBUTE ENDE	===========================
			#ATTRIBUTE PLATFORM START =================
			$qry = $this->SQL->query("SELECT article_id,attribute_id,language_id,  to_platform_id, active ACTIVE, value VALUE, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
										FROM wp_article_platform_attribute 
										WHERE article_id IN ('{$ID}')
											
											AND	platform_id = '{$this->platform_id}'");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				if( $a['language_id'] ) {
					$ARTICLE['D'][ $a['article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;
				}
				else {
					$ARTICLE['D'][ $a['article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;
				}
			}
			#ATTRIBUTE PLATFORM ENDE ==================
			#CATEGORIE START ==========================
			/*
			$qry = $this->SQL->query("SELECT categorie_id, article_id, active ACTIVE, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
										FROM wp_categorie_article
										WHERE article_id IN ('{$ID}')
											AND platform_id = '{$this->platform_id}'");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['CATEGORIE']['D'][ $a['categorie_id'] ] = $a;
			}
			*/
			#CATEGORIE ENDE	===========================
			#SUPPLIER START ===========================
			/*
			$qry = $this->SQL->query("SELECT supplier_id, article_id, reference_id REFERENCE_ID, active ACTIVE, number NUMBER, title TITLE, price PRICE, stock STOCK, comment COMMENT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
										FROM wp_supplier_to_article 
										WHERE article_id IN ('{$ID}')
											
										ORDER BY price");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['D'][ $a['supplier_id'] ] = $a;
				
				#$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['STOCK'] += (($a['ACTIVE'])?$a['STOCK']:0);
				$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['PRICE'] = ($ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['PRICE']+$a['PRICE'])/++$z;
			}

			#Bestellungen #Status 0=0ffen; 20=bestellt; 40=abgeschloßen
			$qry = $this->SQL->query("SELECT ii.id, ii.supplier_id, ii.active ACTIVE, ii.status STATUS, ii.date_paid DATE_PAID, ii.itimestamp*1 ITIMESTAMP, ii.utimestamp*1 UTIMESTAMP
				,iia.id article_id, iia.price PRICE, iia.stock STOCK 
				FROM wp_buying ii, wp_buying_article iia
				WHERE iia.id IN ('{$ID}')
					AND ii.group_id = 'buying'
					AND ii.status < 40
					AND ii.id = iia.buying_id
					AND ii.platform_id = iia.platform_id
					AND ii.platform_id = '{$this->platform_id}'
				");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['D'][ $a['supplier_id'] ]['ORDER']['D'][ $a['id'] ] = $a;
				$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['STOCK'] += (($a['STATUS'] == 20)?$a['STOCK']:0);
			}
			*/
			#SUPPLIER ENDE ============================
			
			#REFERENZ START ===========================
			$qry = $this->SQL->query("SELECT from_article_id, to_platform_id, to_article_id, active ACTIVE, fail FAIL, data DATA, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
							FROM wp_article_reference
							WHERE from_platform_id = '{$this->platform_id}' AND from_article_id IN ('{$ID}') 
							ORDER BY itimestamp");
			$REF_IDs = '';
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				#From->To
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ] = $a;
				if($a['DATA'])
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['DATA'] = json_decode($a['DATA'],true);

				$REF_IDs .= (($REF_IDs)?"','":'')."{$a['to_platform_id']}{$a['to_article_id']}";
				if($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['MAX_UTIMESTAMP'] < $a['UTIMESTAMP'])
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['MAX_UTIMESTAMP'] = $a['UTIMESTAMP'];
				if($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['MIN_UTIMESTAMP'] == null || $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['MIN_UTIMESTAMP'] > $a['UTIMESTAMP'])
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['MIN_UTIMESTAMP'] = $a['UTIMESTAMP'];
			}
			#REFERENZ ENDE ============================
			


			
			#SALE START ===============================
			#ToDo: Prfen
/* #ToDo: Schlechte Performance, soll nicht in Artikel drin sein
			$qry = $this->SQL->query("SELECT ia.id, i.from_platform_id, SUM(stock) stock, strftime('%Y',i.itimestamp) year, strftime('%m',i.itimestamp) month, strftime('%d',i.itimestamp) day
										FROM wp_invoice_article ia, wp_invoice i
										WHERE ia.id IN ('{$ID}')
											AND i.id = ia.invoice_id
											AND ia.platform_id = i.platform_id
											AND ia.platform_id = '{$this->platform_id}'
											
										GROUP BY i.from_platform_id, strftime('%Y',i.itimestamp),strftime('%m',i.itimestamp),strftime('%d',i.itimestamp), ia.id
									");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['STOCK'] += $a['stock'];
				
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['STOCK'] += $a['stock'];

				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['STOCK'] += $a['stock'];
			}
			$qry = $this->SQL->query("
										SELECT contain_article_id, ia.from_platform_id, SUM(ia.stock*ars.quantity) stock, strftime('%Y',i.itimestamp) year, strftime('%m',i.itimestamp) month, strftime('%d',i.itimestamp) day
										FROM wp_invoice_article ia, wp_article_set ars, wp_invoice i
										WHERE ia.id = ars.article_id
											AND i.id = ia.invoice_id
											AND ia.platform_id = ars.platform_id
											AND ars.platform_id = i.platform_id
											AND ars.contain_article_id IN ('{$ID}')
											AND ia.platform_id = '{$this->platform_id}'
											
										GROUP BY i.from_platform_id, strftime('%Y',i.itimestamp),strftime('%m',i.itimestamp),strftime('%d',i.itimestamp), contain_article_id
									");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['INVOICE']['STOCK'] += $a['stock'];
				
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['id'] ]['PLATFORM']['D'][ $a['from_platform_id'] ]['INVOICE']['STOCK'] += $a['stock'];

				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['DAY']['D'][ $a['day'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['MONTH']['D'][ $a['month'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['YEAR']['D'][ $a['year'] ]['STOCK'] += $a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['INVOICE']['STOCK'] += $a['stock'];
			}
*/
			#SALE END =================================
			
			/*#ToDo: die obere Abfrage hat konflikt mit dieser!
			#RESERVED STOCK START ====================
			#ToDo: Status muss autommatishc übergeben werden OFFEN;Klärung; Versandfreigabe
			$qry = $this->SQL->query("SELECT ia.id aid, ia.stock, i.id iid
										FROM wp_invoice_article ia, wp_invoice i
										WHERE  ia.platform_id = i.platform_id
											
											AND ia.platform_id = '{$this->platform_id}'
											AND i.status IN ('0','9','20')
											AND ia.id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$ARTICLE['D'][ $a['article_id'] ]['INVOICE']['D'][ $a['iid'] ]['ARTICLE']['D'][ $a['aid'] ]['STOCK'] +=$a['stock'];
				$ARTICLE['D'][ $a['article_id'] ]['INVOICE']['STOCK'] += $a['stock'];
				$ARTICLE['D'][ $a['article_id'] ]['INVOICE']['STOCK_RESERVED'] += $a['stock'];
			}
			#RESERVED STOCK ENDE =====================
			*/

			#SET START ================================
			$qry = $this->SQL->query("SELECT article_id, contain_article_id, active ACTIVE, quantity QUANTITY, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
										FROM wp_article_set
										WHERE platform_id = '{$this->platform_id}'
											
											AND article_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['SET']['ARTICLE']['D'][ $a['contain_article_id'] ] = $a;
				$ARTICLE['D'][ $a['article_id'] ]['WEIGHT'] += (float)$a['weight']*(int)$a['QUANTITY'];#ToDo weight steht nicht zur verfügung!
				$kSET .= (($kSET)?"','":'').$a['contain_article_id'];
				$ART_SET[ $a['article_id'] ][ $a['contain_article_id'] ]['ACTIVE'] = $a['ACTIVE'];
			}
			#SET ENDE ================================
			
			#INSET START ==================
			$qry = $this->SQL->query("SELECT article_id, contain_article_id, aset.active ACTIVE, aset.quantity QUANTITY, aset.utimestamp*1 UTIMESTAMP, aset.itimestamp*1 ITIMESTAMP,
				a.number NUMBER
				FROM wp_article_set AS aset, wp_article AS a
				WHERE aset.platform_id = '{$this->platform_id}'
					AND aset.platform_id = a.platform_id
					AND a.id = aset.article_id
					AND contain_article_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['contain_article_id'] ]['INSET']['ARTICLE']['D'][ $a['article_id'] ] = [
					'ACTIVE'	=> $a['ACTIVE'],
					'STOCK'		=> $a['QUANTITY'],
					'NUMBER'	=> $a['NUMBER'],
				];
			}
			#INSET ENDE ==================
			
			#STOCK START =============================
			$qry = $this->SQL->query("SELECT article_id, stock STOCK, storage_id, warehouse_id, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
										FROM wp_article_stock 
										WHERE article_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['WAREHOUSE']['D'][ $a['warehouse_id'] ]['STORAGE']['D'][ $a['storage_id'] ] = $a;
				$ARTICLE['D'][ $a['article_id'] ]['WAREHOUSE']['STOCK'] += $a['STOCK'];
				$ARTICLE['D'][ $a['article_id'] ]['STOCK'] += $a['STOCK'];

				#Summierung für Parent Anhängen
				$ARTICLE['PARENT']['D'][ $ARTICLE['CHILD']['D'][ $a['article_id'] ]['PARENT_ID'] ]['CHILD']['STOCK'] += $a['STOCK'];
				$ARTICLE['PARENT']['D'][ $ARTICLE['CHILD']['D'][ $a['article_id'] ]['PARENT_ID'] ]['CHILD']['WAREHOUSE']['STOCK'] += $a['STOCK'];
			}
			#STOCK ENDE ==============================
			
			#ORDER START ==================
			$qry = $this->SQL->query("SELECT oa.id,oa.platform_id,order_id,oa.active ACTIVE,title TITLE,stock STOCK,oa.price PRICE,oa.vat VAT,to_article_id,oa.itimestamp*1 ITIMESTAMP,oa.utimestamp*1 UTIMESTAMP,
			to_platform_id, from_platform_id, from_article_id
			FROM wp_order_article oa 
			INNER JOIN wp_article_reference ar ON oa.platform_id = ar.to_platform_id AND oa.id = ar.to_article_id
			WHERE  from_platform_id = '{$this->platform_id}' AND from_article_id IN ('{$ID}')
			ORDER BY oa.itimestamp");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['ORDER']['D'][ $a['order_id'] ] = $a;
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['ORDER']['STOCK'] += $a['STOCK'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['ORDER']['PRICE'] += $a['PRICE']*(($a['VAT']*0.01)+1); #Brutto Preis
				if($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['ORDER']['MAX_ITIMESTAMP'] < $a['ITIMESTAMP'])
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ]['ORDER']['MAX_ITIMESTAMP'] = $a['ITIMESTAMP'];
			}
			
			#ORDER ENDE ==================
			
			#STOCK START offene Bestellungen==========
			#ToDo: muss überprüft werden, vor allem wass pasiert wen STOCK gemindert wird ob dies beim Lager nicht zur Problemem führt zur Abbuchen
			#/*
			#1. Artikel ohne SET
			#2. Artikel Set aufgelöst
			$qry = $this->SQL->query("SELECT delivery_id, article_id, stock STOCK, d.itimestamp*1 ITIMESTAMP, d.utimestamp*1 UTIMESTAMP
										FROM wp_delivery_article da, wp_delivery d
										WHERE da.delivery_id = d.id
											AND d.status IN (20,9)
											AND da.platform_id = d.platform_id
											AND d.platform_id = '{$this->platform_id}'
											AND da.article_id IN ('{$ID}')
											AND NOT EXISTS (SELECT 1 FROM wp_article_set ars WHERE ars.platform_id = d.platform_id AND ars.article_id = article_id) 
											
									UNION ALL
										SELECT delivery_id, ars.contain_article_id, da.stock * quantity STOCK, d.itimestamp*1 ITIMESTAMP, d.utimestamp*1 UTIMESTAMP
										FROM wp_article_set ars, wp_delivery_article da, wp_delivery d
										WHERE da.delivery_id = d.id
											AND d.status IN (20,9)
											AND da.platform_id = ars.platform_id
											AND da.platform_id = d.platform_id
											AND d.platform_id = '{$this->platform_id}'
											AND da.article_id = ars.article_id
											AND ars.contain_article_id IN ('{$ID}')
									");
									#AND article_id NOT IN (SELECT article_id FROM wp_article_set ars WHERE ars.platform_id = d.platform_id AND ars.account_id = d.account_id)
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$ARTICLE['D'][ $a['article_id'] ]['STOCK'] -= $a['STOCK'];
				$ARTICLE['PARENT']['D'][ $ARTICLE['CHILD']['D'][ $a['article_id'] ]['PARENT_ID'] ]['CHILD']['STOCK'] -= $a['STOCK'];

				$ARTICLE['D'][ $a['article_id'] ]['DELIVERY']['D'][ $a['delivery_id'] ]['ARTICLE']['D'][ $a['article_id'] ] = $a;
				$ARTICLE['D'][ $a['article_id'] ]['DELIVERY']['ARTICLE']['D'][ $a['article_id'] ]['STOCK'] += $a['STOCK'];
			}
			#*/
			#STOCK ENDE offene Bestellungen ==========

			#STOCK START Supplier ====================
			/*$qry = $this->SQL->query("SELECT supplier_id, article_id, reference_id, active ACTIVE, price PRICE, stock STOCK, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_supplier_to_article sta, wp_supplier su
								WHERE sta.account_id = su.account_id
									AND sta.supplier_id = su.id
									AND su.dropshipping = 1
									AND sta.account_id = '{$this->account_id}' AND article_id IN ('{$ID}') ");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$ARTICLE['D'][ $a['article_id'] ]['SUPPLIER']['D'][ $a['supplier_id'] ]['ARTICLE']['D'][ $a['article_id'] ] = $a;
				$ARTICLE['D'][ $a['article_id'] ]['STOCK'] += $a['STOCK'];
			}*/
			#STOCK END Supplier ======================

			#Shipping Coust Start ==================
/* #ToDo: Muss für SqLite umgestellt werden
			$qry = $this->SQL->query("SELECT sc.id AS shipping_class_id, scon.group_id, scon.column, scon.operator, scon.value, scon.active AS con_active
							FROM wp_shipping_class sc LEFT JOIN wp_shipping_conditions scon ON sc.id = scon.shipping_class_id AND scon.active = 1
							WHERE sc.active = 1
							ORDER BY sc.sort, scon.shipping_class_id, scon.group_id");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$w[$a['shipping_class_id']] .= (($w[$a['shipping_class_id']])? (($a['group_id'] != $gid)? ') OR ('  : ' AND ') :" sc.id = '{$a['shipping_class_id']}' ".(($a['con_active'])?"AND (( ":'AND (( 1'))." {$a['column']} {$a['operator']} {$a['value']} ";
				$gid = $a['group_id'];
			}
			foreach((array)$w AS $k => $v)
				$w2 .= (($w2)?' OR ':' 0 OR ')."({$v})) )";
			
			$w2 = ($w2)?" AND ({$w2}) ":'';
            if($ID) {
				
				$qry = $this->SQL->query("SELECT a.id,a.price,a.categorie_id, sc.shipping_id, sc.active, sc.active, ctc.price, sc.shipping_id, ctc.id AS cost_id, c.id AS cid, c.title
											FROM (SELECT a.id, a.price, ca.categorie_id FROM wp_article a, wp_categorie_article ca WHERE a.id = ca.article_id AND a.platform_id = ca.platform_id AND a.platform_id = '{$this->platform_id}' AND a.id IN ('{$ID}')) a
											, wp_shipping_class sc, wp_shipping_class_to_cost ctc, wp_shipping_class_to_country sc2c, wp_country c  
											WHERE
											sc.id = ctc.shipping_class_id
											AND ctc.id = sc2c.shipping_cost_id
											AND sc.id = sc2c.shipping_class_id
											AND sc2c.country_id = c.id
											AND ctc.active = 1
											AND sc.active = 1
											{$w2}
											ORDER BY c.id, sc.sort");
				while($a = $qry->fetchArray(SQLITE3_ASSOC))
				{
					#lösche Land aus der vorigen Cost, weil diese mit der nächsten überschrieben wird.
					if($oldCostID = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COUNTRY']['D'][ $a['cid'] ]['COST_ID'])
					{
						unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COST']['D'][ $oldCostID ]['COUNTRY']['D'][ $a['cid'] ]);
						if(count($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COST']['D'][ $oldCostID ]['COUNTRY']['D']) == 0) #Lösche cost wenn es keine Länder beinhaltet
							unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COST']['D'][ $oldCostID ]);
					}
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['ACTIVE'] = $a['active'];
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COUNTRY']['D'][ $a['cid'] ] = [
						'TITLE' => $a['title'],
						'PRICE' => $a['price'],
						'COST_ID' => $a['cost_id'],
					];
					
					#Gruppiert nach Preis
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COST']['D'][ $a['cost_id'] ]['PRICE'] = $a['price'];
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ]['SHIPPING']['D'][ $a['shipping_id'] ]['COST']['D'][ $a['cost_id'] ]['COUNTRY']['D'][ $a['cid'] ] = [
						'TITLE' => $a['title'],
					];
				}

			
			}
*/#---------------
			#Shipping Coust END ==================
			
			#STOCK aus SET Artikel
			if($kSET)
			{
				$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['ID|PARENT_ID'] = $kSET;
				$this->get_article($d);
				
				foreach($ART_SET AS $kA => $vA) #alle Artikel die Set Artikel haben
				{
					$ARTICLE['D'][ $kA ]['STOCK'] = $ARTICLE['D'][ $kA ]['WEIGHT'] = NULL;#Lösche die voreinstellungen damit die Daten nur von SET übernohmen werden.
					/*foreach((array)$ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] AS $kAC => $vAC)#durchlaufe alle kinder
					{
						$ARTICLE['D'][ $kAC ]['STOCK'] = $ARTICLE['D'][ $kAC ]['WEIGHT'] = NULL; #Setze Bestand und Gewicht auf 0 falls dies vorher flschlicherweise ber die Datenbank noch ausgelesen wurde durch frheren Einstellungen
					}*/
					
					
					foreach($vA AS $kS => $vS) #je SET ART
					{
						#1.IST Artikel Parent und Set Artikel Parent DANN Child anhand ATT verknüpfen !Achtung: dies kann nur verknüpft werden wen beide Artikel gleiche Attribute zugewiesen sind
						#2.Ist Artikel Parent und SET Artikel allein stehend DANN beim Child die Bestnde angeben.
						#3.Ist Artikel allein stehend und SET Artikel allein stehend DANN Bestand angeben.
						#4.IST Artikel allein stehend und SET Artikel hat Kinder DANN ist nicht mglich
						if($ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] !== null && $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][ $kS ]['CHILD']['D'] !== null)
						{
							foreach((array)$ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] AS $kAC => $vAC)#durchlaufe alle kinder
							{
								foreach((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][ $kS ]['CHILD']['D'] AS $kSC => $vSC) #durchlaufe alle set Artikel Kinder
								{
									$CHECK_ALL_ATT = 0;
									#foreach((array)$ARTICLE['D'][ $kAC ]['ATTRIBUTETYPE']['D']['ATTRIBUTE'][ 'ATTRIBUTE' ]['D'] AS $kATT => $vATT)#durchlaufe alle ATTRIBUTE des Artikel Kindes
									
									foreach((array)explode('|',$ARTICLE['D'][ $kA ]['VARIANTE_GROUP_ID']) AS $kATT)#durchlaufe NUR VARIATION ATTRIBUTE des Artikel Kindes
									{
										#$vATT = $ARTICLE['D'][ $kAC ]['ATTRIBUTETYPE']['D']['ATTRIBUTE'][ 'ATTRIBUTE' ]['D'][$kATT];
										if(trim($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kSC ][ 'ATTRIBUTE' ]['D'][ $kATT ]['LANGUAGE']['D'][ 'DE' ]['VALUE']) == trim($ARTICLE['D'][ $kAC ][ 'ATTRIBUTE' ]['D'][$kATT]['LANGUAGE']['D'][ 'DE' ]['VALUE'])) #durchlaufe alle attribute kombis der SET Artikel und vergleiche mit denn
											$CHECK_ALL_ATT = 1;
										else
										{
											$CHECK_ALL_ATT = 0;
											break;
										}
									}
									if($CHECK_ALL_ATT)
									{
										if($ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY'] > 0)
										{
											$QUANTITY = floor($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kSC ]['STOCK'] / $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']);
											$ARTICLE['D'][ $kAC ]['STOCK'] = ($ARTICLE['D'][ $kAC ]['STOCK'] === null || $ARTICLE['D'][ $kAC ]['STOCK'] > $QUANTITY)?$QUANTITY:$ARTICLE['D'][ $kAC ]['STOCK'];
											$ARTICLE['D'][ $kAC ]['WEIGHT'] += $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']*$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kSC ]['WEIGHT'];
											
											$ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['WAREHOUSE']['D'] = $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kSC ]['WAREHOUSE']['D'];

											$ARTICLE['CHILD']['D'][ $kAC ]['SET']['ARTICLE']['D'][ $kSC ] = [
												'ACTIVE'	=> 1, #ToDo: Hotfix, damit es beim Artikel bearbeiten für Variation nicht gespeichert wird
												'QUANTITY'	=> $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY'],
												'WAREHOUSE' => $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kSC ]['WAREHOUSE'],
											];
										}
										break;
									}
									else
									{
										##$ARTICLE['D'][ $kAC ]['STOCK'] = $ARTICLE['D'][ $kAC ]['WEIGHT'] = 0;
										#ToDo: LOG Fehler: keine bereinstimmung der Attribute!
									}
								}
							}
						}
						elseif($ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] !== null && $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][ $kS ]['CHILD']['D'] == '')
						{
							if($ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']>0)
							{
								$QUANTITY = floor($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['STOCK'] / $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']);
								foreach((array)$ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] AS $kAC => $vAC )
								{
									$ARTICLE['D'][ $kAC ]['STOCK'] = ($ARTICLE['D'][ $kAC ]['STOCK'] === null || $ARTICLE['D'][ $kAC ]['STOCK'] > $QUANTITY)?$QUANTITY:$ARTICLE['D'][ $kAC ]['STOCK'];
									$ARTICLE['D'][ $kAC ]['WEIGHT'] += $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']*$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['WEIGHT'];
									
									$ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['WAREHOUSE']['D'] = $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['WAREHOUSE']['D'];

									$ARTICLE['CHILD']['D'][ $kAC ]['SET']['ARTICLE']['D'][ $kS ] = [
										'ACTIVE'	=> 1, #ToDo: Hofix, damit es beim Artikel ebarbeiten für Variation nicht gespeichert wird
										'QUANTITY'	=> $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY'],
										'WAREHOUSE' => $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['WAREHOUSE'],
									];
								}
							}
						}
						elseif($ARTICLE['PARENT']['D'][ $kA ]['CHILD']['D'] == '' && $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][ $kS ]['CHILD']['D'] == '')
						{
							if($ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']>0)
							{
								$QUANTITY = floor($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['STOCK'] / $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']);
								$ARTICLE['D'][ $kA ]['STOCK'] = ($ARTICLE['D'][ $kA ]['STOCK'] === null || $ARTICLE['D'][ $kA ]['STOCK'] > $QUANTITY)?$QUANTITY:$ARTICLE['D'][ $kA ]['STOCK'];
								$ARTICLE['D'][ $kA ]['WEIGHT'] += $ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['QUANTITY']*$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['WEIGHT'];
								
								$ARTICLE['D'][ $kA ]['SET']['ARTICLE']['D'][ $kS ]['WAREHOUSE']['D'] = $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kS ]['WAREHOUSE']['D'];
							}
						}
						else
						{
							#ToDo: LOG Kann nicht zuordnen, weil das Artikel keine Varianten besitz und das zu geordnete Parent Artikel hat Varianten, Es muss dann die einzelne Variation zugeordnet werden oder das Artikel muss gleiche Variationen wie das Set Artikel haben!
						}
					}
				}
			}
			
			

		}
	}

	
	function set_article($D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][NULL]['CHILD']['D'] AS $kART => $ART)
		{
			#ARTICLE START ================
			if($ART['ACTIVE'] != -2)
			{
				$d['ARTICLE']['D'][ $kART ] = [
							'Active'	=> 1,
							'ParentId'	=> '',
						];
				$d['ARTICLE']['D'][ $kART ]['CUSTOMERGROUP']['D'][0] = [
							'Price'	=> $ART['PRICE'],
						];
						
				$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Title']['LANGUAGE']['D']['DE'] = [
					'Active'	=> 1,
					'Value'		=> $ART['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ 'DE' ]['VALUE'],
				];
				$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Sku'] = [
					'Active'	=> 1,
					'Value'		=> $ART['NUMBER'],
				];
				$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Url']['LANGUAGE']['D']['DE'] = [
					'Active'	=> 1,
					'Value'		=> preg_replace("/[^a-zA-Z0-9]/", "-", $ART['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ 'DE' ]['VALUE']),
				];
				$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Description']['LANGUAGE']['D']['DE'] = [
					'Active'	=> 1,
					'Value'		=> $ART['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ 'DE' ]['VALUE'],
				];
				#VarianteGroup
				$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['VarianteGroup'] = [
					'Active'	=> 1,
					'Value'		=> $ART['VARIANTE_GROUP_ID'],
				];
				#Bilder
				
				#$iter = 0;
				$_image = null;
				foreach((array)$ART['FILE']['D'] AS $kFIL => $FIL) {
					
					#$iter = str_pad($iter, 2, 0, STR_PAD_LEFT);
					
					$bild_url = "https://{$D['ACCOUNT_ID']}.hexcon.de/file/{$D['ACCOUNT_ID']}/{$ART['PLATFORM_ID']}/{$kFIL}_1500x1500.jpg";
					
					$md5 = md5_file($bild_url);
					$d['FILE']['D'][$md5] = [
						'Active' 	=> 1,
						'Name'		=> $bild_url."_{$iter}",
						#'Size'	 	=> 1234, #byte
						#'Extension' => 'jpg',
						'Url'		=> $bild_url,
					];
				/*
					$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Pic'.$iter] = [
						'Active' => 1,
						'Value'	 => $md5,
					];
					$iter++;
					*/
					$_image[] = $md5;
				}
					$d['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']['Image'] = [
								'Active' => 1,
								'Value'	 => json_encode($_image),
							];
				
				#Variante
				$iter2 = 0;
				foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['PARENT']['D'][$kART]['CHILD']['D'] AS $kVAR => $VAR) {
					$iter2++;
					$iter2 = str_pad($iter2, 2, 0, STR_PAD_LEFT);
					if($VAR['ACTIVE'] != -2) {
						$d['ARTICLE']['D'][ $kVAR ] = [
							'Active'	=> 1,
							'ParentId'	=> $kART,
						];
						
						$d['ARTICLE']['D'][ $kVAR ]['CUSTOMERGROUP']['D'][0] = [
							'Price'	=> $VAR['PRICE'],
						];
						
						$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Title']['LANGUAGE']['D']['DE'] = [
							'Active'	=> 1,
							'Value'		=> $ART['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ 'DE' ]['VALUE'],
						];
						$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Sku'] = [
							'Active'	=> 1,
							'Value'		=> $VAR['NUMBER'],
						];
						$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Url']['LANGUAGE']['D']['DE'] = [
							'Active'	=> 1,
							'Value'		=> preg_replace("/[^a-zA-Z0-9]/", "-", $ART['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][ 'DE' ]['VALUE'])."-".$iter2,
						];
						$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Description']['LANGUAGE']['D']['DE'] = [
							'Active'	=> 1,
							'Value'		=> $ART['ATTRIBUTE']['D']['DESCRIPTION']['LANGUAGE']['D'][ 'DE' ]['VALUE'],
						];
						
						$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D'][ $ART['VARIANTE_GROUP_ID'] ]['LANGUAGE']['D']['DE'] = [
							'Active'	=> 1,
							'Value'		=> $VAR['ATTRIBUTE']['D'][ $ART['VARIANTE_GROUP_ID'] ]['LANGUAGE']['D'][ 'DE' ]['VALUE'],
						];
						
						
						#Bilder
						
						#$iter3 = 0;
						$_image=null;
						foreach((array)$VAR['FILE']['D'] AS $kFIL => $FIL) {
							
							#$iter3 = str_pad($iter3, 2, 0, STR_PAD_LEFT);
							
							$bild_url = "https://{$D['ACCOUNT_ID']}.hexcon.de/file/{$D['ACCOUNT_ID']}/{$ART['PLATFORM_ID']}/{$kFIL}_1500x1500.jpg";
							
							$md5 = md5_file($bild_url);
							$d['FILE']['D'][$md5] = [
								'Active' 	=> 1,
								'Name'		=> $bild_url."_{$iter3}",
								#'Size'	 	=> 1234, #byte
								#'Extension' => 'jpg',
								'Url'		=> $bild_url,
							];
							/*
							$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Pic'.$iter3] = [
								'Active' => 1,
								'Value'	 => $md5,
							];
							$iter3++;
							*/
							$_image[] = $md5;
						}
						
							$d['ARTICLE']['D'][ $kVAR ]['ATTRIBUTE']['D']['Image'] = [
								'Active' => 1,
								'Value'	 => json_encode($_image),
							];
					}
				}
				
			}
			else
			{
				
			}
			#ARTICLE END ==================
		}
		
		$curlHandle = curl_init('https://fuchswerk.eu/api?D[_APIKEY]=testtest');
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, ['D[_D]'		=> json_encode($d) ]);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
		echo $curlResponse = curl_exec($curlHandle);
		curl_close($curlHandle);
		echo '';
	}



	
	
	function get_attribute(&$D=null)
	{
		
	}
	
	function set_attribute(&$D)
	{
		
	}
	
	function get_payment(&$D=null)
	{
		
	}
	
	function set_payment($D)
	{
		
	}
	
	function get_categorie(&$D=null)
	{
		$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ 'main'] = [
			'ACTIVE'	=> 1,
			'PARENT_ID'	=> ''
		];
		$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][ 'main' ]['LANGUAGE']['D']['DE']['TITLE'] = 'shop';
		parent::set_categorie($D);
		
		parent::get_categorie($D);
	}
	
	function set_categorie(&$D)
	{
		parent::set_categorie($D);
		#platform::set_categorie($D);
	}
	
	function get_language(&$D=null) #ToDo: Funktion veraltet, bitte dierekt die Information über die AvailableContentLanguage-Einstellung verwenden.
	{
	}
	
	function set_language($D)
	{
		
	}
	

	function set_invoice($D)
	{

	}
	/*
	function get_order(&$D=null)
	{
		$d['ORDER']['W']['PLATFORM_ID'] = $this->platform_id;
		parent::get_order($d);
		$D['PLATFORM']['D'][ $this->platform_id ]['ORDER'] = $d['ORDER'];
	}*/

	function get_invoice(&$D=null) {

	}

	function get_delivery(&$D=null)
	{
	}

	#Versendete Artikel
	function set_delivery(&$D)
	{
		#Ermittlet die letzte Nummer
		$qry = $this->SQL->query("SELECT MAX(number) number FROM wp_delivery WHERE platform_id = '{$this->platform_id}'");
		$a = $qry->fetchArray(SQLITE3_ASSOC);
		$NEW_NUM = $a['number']+1;


		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['DELIVERY']['D'] AS $kDEL => $DEL) {
			if($DEL['ACTIVE'] != -2) {
				$IU_DEL .= (($IU_DEL)?',':'')."('{$kDEL}','{$this->platform_id}'";
				$IU_DEL .= (isset($DEL['CUSTOMER_ID']))? ",'{$DEL['CUSTOMER_ID']}'":",NULL";
				$IU_DEL .= (isset($DEL['INVOICE_ID']))? ",'{$DEL['INVOICE_ID']}'":",NULL";
				$IU_DEL .= (isset($DEL['ORDER_ID']))? ",'{$DEL['ORDER_ID']}'":",NULL";
				$IU_DEL .= (isset($DEL['WAREHOUSE_ID']))? ",'{$DEL['WAREHOUSE_ID']}'":",NULL";
				$IU_DEL .= (isset($DEL['SHIPPING_ID']))? ",'{$DEL['SHIPPING_ID']}'":",NULL";
				$IU_DEL .= (isset($DEL['TRACKING_NO']))? ",'{$DEL['TRACKING_NO']}'":",NULL";
				$IU_DEL .= (isset($DEL['SHIPPED_DATE']))? ",'{$DEL['SHIPPED_DATE']}'":",NULL";
				$IU_DEL .= (isset($DEL['ACTIVE']))? ",'{$DEL['ACTIVE']}'":",NULL";
				$IU_DEL .= (isset($DEL['STATUS']))? ",'{$DEL['STATUS']}'":",NULL";
				if(isset($DEL['NUMBER']) && $DEL['NUMBER'] != '') {
					$IU_DEL .= ",'{$DEL['NUMBER']}'";
				}
				else {
					$IU_DEL .= ",'{$NEW_NUM}'";
					$NEW_NUM++;
				}
				$IU_DEL .= (isset($DEL['RETURN_COMPANY']))? ",'".$this->SQL->escapeString($DEL['RETURN_COMPANY'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_FNAME']))? ",'".$this->SQL->escapeString($DEL['RETURN_FNAME'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_NAME']))? ",'".$this->SQL->escapeString($DEL['RETURN_NAME'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_PHONE']))? ",'".$this->SQL->escapeString($DEL['RETURN_PHONE'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_EMAIL']))? ",'".$this->SQL->escapeString($DEL['RETURN_EMAIL'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_STREET']))? ",'".$this->SQL->escapeString($DEL['RETURN_STREET'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_STREET_NO']))? ",'".$this->SQL->escapeString($DEL['RETURN_STREET_NO'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_ADDITION']))? ",'".$this->SQL->escapeString($DEL['RETURN_ADDITION'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_ZIP']))? ",'".$this->SQL->escapeString($DEL['RETURN_ZIP'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_CITY']))? ",'".$this->SQL->escapeString($DEL['RETURN_CITY'])."'":",NULL";
				$IU_DEL .= (isset($DEL['RETURN_COUNTRY_ID']))? ",'{$DEL['RETURN_COUNTRY_ID']}'":",NULL";

				$IU_DEL .= (isset($DEL['DELIVERY_COMPANY']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_COMPANY'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_FNAME']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_FNAME'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_NAME']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_NAME'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_PHONE']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_PHONE'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_EMAIL']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_EMAIL'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_STREET']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_STREET'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_STREET_NO']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_STREET_NO'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_ADDITION']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_ADDITION'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_ZIP']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_ZIP'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_CITY']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_CITY'])."'":",NULL";
				$IU_DEL .= (isset($DEL['DELIVERY_COUNTRY_ID']))? ",'".$this->SQL->escapeString($DEL['DELIVERY_COUNTRY_ID'])."'":",NULL";
				$IU_DEL .= (isset($DEL['COMMENT']))? ",'".$this->SQL->escapeString($DEL['COMMENT'])."'":",NULL";
				$IU_DEL .= ")";

				#Article
				foreach((array)$DEL['ARTICLE']['D'] AS $kART => $ART) {
					if($ART['ACTIVE'] != -2) {
						$IU_ART .= (($IU_ART)?',':'')."('{$kDEL}','{$kART}','{$this->platform_id}'";
						$IU_ART .= (isset($ART['ACTIVE']))? ",'{$ART['ACTIVE']}'":",NULL";
						$IU_ART .= (isset($ART['NUMBER']))? ",'{$ART['NUMBER']}'":",NULL";
						$IU_ART .= (isset($ART['TITLE']))? ",'".$this->SQL->escapeString($ART['TITLE'])."'":",NULL";
						$IU_ART .= (isset($ART['STOCK']))? ",'{$ART['STOCK']}'":",NULL";
						$IU_ART .= (isset($ART['WEIGHT']))? ",'{$ART['WEIGHT']}'":",NULL";
						$IU_ART .= ")";
					}
					else {
						$D_ART .= (($D_ART)?',':'')."'{$kDEL}{$kART}'";
					}
				}
			}
			else {
				$D_DEL .= (($D_DEL)?',':'')."'{$kDEL}'";
			}
		}
		
		if($IU_DEL) {
			$this->SQL->query("INSERT INTO wp_delivery (id, platform_id, customer_id, invoice_id, order_id, warehouse_id, shipping_id, tracking_no, shipped_date, active,status,number,
					return_company,return_fname,return_name,return_phone,return_email,return_street,return_street_no,return_addition,return_zip,return_city,return_country_id,
					delivery_company,delivery_fname,delivery_name,delivery_phone,delivery_email,delivery_street,delivery_street_no,delivery_addition,delivery_zip,delivery_city,delivery_country_id,comment) VALUES {$IU_DEL} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							customer_id =			CASE WHEN excluded.customer_id IS NOT NULL			AND ifnull(customer_id,'') <> excluded.customer_id					THEN excluded.customer_id ELSE customer_id END, 
							invoice_id =			CASE WHEN excluded.invoice_id IS NOT NULL			AND ifnull(invoice_id,'') <> excluded.invoice_id					THEN excluded.invoice_id ELSE invoice_id END,
							order_id =				CASE WHEN excluded.order_id IS NOT NULL				AND ifnull(order_id,'') <> excluded.order_id						THEN excluded.order_id ELSE order_id END,
							warehouse_id =			CASE WHEN excluded.warehouse_id IS NOT NULL			AND ifnull(warehouse_id,'') <> excluded.warehouse_id				THEN excluded.warehouse_id ELSE warehouse_id END,
							shipping_id =			CASE WHEN excluded.shipping_id IS NOT NULL			AND ifnull(shipping_id,'') <> excluded.shipping_id					THEN excluded.shipping_id ELSE shipping_id END,
							tracking_no =			CASE WHEN excluded.tracking_no IS NOT NULL			AND ifnull(tracking_no,'') <> excluded.tracking_no					THEN excluded.tracking_no ELSE tracking_no END,
							shipped_date =			CASE WHEN excluded.shipped_date IS NOT NULL			AND ifnull(shipped_date,'') <> excluded.shipped_date				THEN excluded.shipped_date ELSE shipped_date END,
							active =				CASE WHEN excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active							THEN excluded.active ELSE active END,
							status =				CASE WHEN excluded.status IS NOT NULL				AND ifnull(status,'') <> excluded.status							THEN excluded.status ELSE status END,
							return_company =		CASE WHEN excluded.return_company IS NOT NULL		AND ifnull(return_company,'') <> excluded.return_company			THEN excluded.return_company ELSE return_company END,
							return_fname =			CASE WHEN excluded.return_fname IS NOT NULL			AND ifnull(return_fname,'') <> excluded.return_fname				THEN excluded.return_fname ELSE return_fname END,
							return_name =			CASE WHEN excluded.return_name IS NOT NULL			AND ifnull(return_name,'') <> excluded.return_name					THEN excluded.return_name ELSE return_name END,
							return_phone =			CASE WHEN excluded.return_phone IS NOT NULL			AND ifnull(return_phone,'') <> excluded.return_phone				THEN excluded.return_phone ELSE return_phone END,
							return_email =			CASE WHEN excluded.return_email IS NOT NULL			AND ifnull(return_email,'') <> excluded.return_email				THEN excluded.return_email ELSE return_email END,
							return_street =			CASE WHEN excluded.return_street IS NOT NULL		AND ifnull(return_street,'') <> excluded.return_street				THEN excluded.return_street ELSE return_street END,
							return_street_no =		CASE WHEN excluded.return_street_no IS NOT NULL		AND ifnull(return_street_no,'') <> excluded.return_street_no		THEN excluded.return_street_no ELSE return_street_no END,
							return_addition =		CASE WHEN excluded.return_addition IS NOT NULL		AND ifnull(return_addition,'') <> excluded.return_addition			THEN excluded.return_addition ELSE return_addition END,
							return_zip =			CASE WHEN excluded.return_zip IS NOT NULL			AND ifnull(return_zip,'') <> excluded.return_zip					THEN excluded.return_zip ELSE return_zip END,
							return_city =			CASE WHEN excluded.return_city IS NOT NULL			AND ifnull(return_city,'') <> excluded.return_city					THEN excluded.return_city ELSE return_city END,
							return_country_id =		CASE WHEN excluded.return_country_id IS NOT NULL	AND ifnull(return_country_id,'') <> excluded.return_country_id		THEN excluded.return_country_id ELSE return_country_id END,
							delivery_company =		CASE WHEN excluded.delivery_company IS NOT NULL		AND ifnull(delivery_company,'') <> excluded.delivery_company		THEN excluded.delivery_company ELSE delivery_company END,
							delivery_fname =		CASE WHEN excluded.delivery_fname IS NOT NULL		AND ifnull(delivery_fname,'') <> excluded.delivery_fname			THEN excluded.delivery_fname ELSE delivery_fname END,
							delivery_name =			CASE WHEN excluded.delivery_name IS NOT NULL		AND ifnull(delivery_name,'') <> excluded.delivery_name				THEN excluded.delivery_name ELSE delivery_name END,
							delivery_phone =		CASE WHEN excluded.delivery_phone IS NOT NULL		AND ifnull(delivery_phone,'') <> excluded.delivery_phone			THEN excluded.delivery_phone ELSE delivery_phone END,
							delivery_email =		CASE WHEN excluded.delivery_email IS NOT NULL		AND ifnull(delivery_email,'') <> excluded.delivery_email			THEN excluded.delivery_email ELSE delivery_email END,
							delivery_street =		CASE WHEN excluded.delivery_street IS NOT NULL		AND ifnull(delivery_street,'') <> excluded.delivery_street			THEN excluded.delivery_street ELSE delivery_street END,
							delivery_street_no =	CASE WHEN excluded.delivery_street_no IS NOT NULL	AND ifnull(delivery_street_no,'') <> excluded.delivery_street_no	THEN excluded.delivery_street_no ELSE delivery_street_no END,
							delivery_addition =		CASE WHEN excluded.delivery_addition IS NOT NULL	AND ifnull(delivery_addition,'') <> excluded.delivery_addition		THEN excluded.delivery_addition ELSE delivery_addition END,
							delivery_zip =			CASE WHEN excluded.delivery_zip IS NOT NULL			AND ifnull(delivery_zip,'') <> excluded.delivery_zip				THEN excluded.delivery_zip ELSE delivery_zip END,
							delivery_city =			CASE WHEN excluded.delivery_city IS NOT NULL		AND ifnull(delivery_city,'') <> excluded.delivery_city				THEN excluded.delivery_city ELSE delivery_city END,
							delivery_country_id =	CASE WHEN excluded.delivery_country_id IS NOT NULL	AND ifnull(delivery_country_id,'') <> excluded.delivery_country_id	THEN excluded.delivery_country_id ELSE delivery_country_id END,
							comment =				CASE WHEN excluded.comment IS NOT NULL				AND ifnull(comment,'') <> excluded.comment							THEN excluded.comment ELSE comment END,
							
							utimestamp =			CASE WHEN 
															excluded.customer_id IS NOT NULL			AND ifnull(customer_id,'') <> excluded.customer_id
															OR excluded.invoice_id IS NOT NULL			AND ifnull(invoice_id,'') <> excluded.invoice_id
															OR excluded.order_id IS NOT NULL			AND ifnull(order_id,'') <> excluded.order_id
															OR excluded.warehouse_id IS NOT NULL		AND ifnull(warehouse_id,'') <> excluded.warehouse_id
															OR excluded.shipping_id IS NOT NULL			AND ifnull(shipping_id,'') <> excluded.shipping_id
															OR excluded.tracking_no IS NOT NULL			AND ifnull(tracking_no,'') <> excluded.tracking_no
															OR excluded.shipped_date IS NOT NULL		AND ifnull(shipped_date,'') <> excluded.shipped_date
															OR excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active
															OR excluded.status IS NOT NULL				AND ifnull(status,'') <> excluded.status
															OR excluded.return_company IS NOT NULL		AND ifnull(return_company,'') <> excluded.return_company
															OR excluded.return_fname IS NOT NULL		AND ifnull(return_fname,'') <> excluded.return_fname
															OR excluded.return_name IS NOT NULL			AND ifnull(return_name,'') <> excluded.return_name
															OR excluded.return_phone IS NOT NULL		AND ifnull(return_phone,'') <> excluded.return_phone
															OR excluded.return_email IS NOT NULL		AND ifnull(return_email,'') <> excluded.return_email
															OR excluded.return_street IS NOT NULL		AND ifnull(return_street,'') <> excluded.return_street
															OR excluded.return_street_no IS NOT NULL	AND ifnull(return_street_no,'') <> excluded.return_street_no
															OR excluded.return_addition IS NOT NULL		AND ifnull(return_addition,'') <> excluded.return_addition
															OR excluded.return_zip IS NOT NULL			AND ifnull(return_zip,'') <> excluded.return_zip
															OR excluded.return_city IS NOT NULL			AND ifnull(return_city,'') <> excluded.return_city
															OR excluded.return_country_id IS NOT NULL	AND ifnull(return_country_id,'') <> excluded.return_country_id
															OR excluded.delivery_company IS NOT NULL	AND ifnull(delivery_company,'') <> excluded.delivery_company
															OR excluded.delivery_fname IS NOT NULL		AND ifnull(delivery_fname,'') <> excluded.delivery_fname
															OR excluded.delivery_name IS NOT NULL		AND ifnull(delivery_name,'') <> excluded.delivery_name
															OR excluded.delivery_phone IS NOT NULL		AND ifnull(delivery_phone,'') <> excluded.delivery_phone
															OR excluded.delivery_email IS NOT NULL		AND ifnull(delivery_email,'') <> excluded.delivery_email
															OR excluded.delivery_street IS NOT NULL		AND ifnull(delivery_street,'') <> excluded.delivery_street
															OR excluded.delivery_street_no IS NOT NULL	AND ifnull(delivery_street_no,'') <> excluded.delivery_street_no
															OR excluded.delivery_addition IS NOT NULL	AND ifnull(delivery_addition,'') <> excluded.delivery_addition
															OR excluded.delivery_zip IS NOT NULL		AND ifnull(delivery_zip,'') <> excluded.delivery_zip
															OR excluded.delivery_city IS NOT NULL		AND ifnull(delivery_city,'') <> excluded.delivery_city
															OR excluded.delivery_country_id IS NOT NULL	AND ifnull(delivery_country_id,'') <> excluded.delivery_country_id
															OR excluded.comment IS NOT NULL				AND ifnull(comment,'') <> excluded.comment
													THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($IU_ART) {
			$this->SQL->query("INSERT INTO wp_delivery_article (delivery_id, article_id, platform_id, active, number, title, stock, weight) VALUES {$IU_ART} 
						ON CONFLICT(delivery_id, article_id, platform_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL	AND ifnull(active,'') <> excluded.active	THEN excluded.active ELSE active END,
							number =		CASE WHEN excluded.number IS NOT NULL	AND ifnull(number,'') <> excluded.number	THEN excluded.number ELSE number END,
							title =			CASE WHEN excluded.title IS NOT NULL	AND ifnull(title,'') <> excluded.title		THEN excluded.title ELSE title END,
							stock =			CASE WHEN excluded.stock IS NOT NULL	AND ifnull(stock,'') <> excluded.stock		THEN excluded.stock ELSE stock END,
							weight =		CASE WHEN excluded.weight IS NOT NULL	AND ifnull(weight,'') <> excluded.weight	THEN excluded.weight ELSE weight END,
							utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
													OR excluded.number IS NOT NULL	AND ifnull(number,'') <> excluded.number
													OR excluded.title IS NOT NULL	AND ifnull(title,'') <> excluded.title
													OR excluded.stock IS NOT NULL	AND ifnull(stock,'') <> excluded.stock
													OR excluded.weight IS NOT NULL	AND ifnull(weight,'') <> excluded.weight
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($D_DEL) {
			$this->SQL->query("DELETE FROM wp_delivery WHERE id IN ({$D_DEL}) AND platform_id = '{$this->platform_id}' ");
			$this->SQL->query("DELETE FROM wp_delivery WHERE delivery_id NOT IN (SELECT id FROM wp_delivery WHERE platform_id = '{$this->platform_id}'  ) AND platform_id = '{$this->platform_id}' ");
		}

		if($D_ART) {
			$this->SQL->query("DELETE FROM wp_delivery_article WHERE (delivery_id||article_id) IN ({$D_ART}) AND platform_id = '{$this->platform_id}' ");
		}
		#Setze Artikel auf geändert wenn Eine Änderung durchgeführt wurde: ToDo: Bei Status 20=freigabe gesetzt wurde.
		$this->SQL->query("UPDATE wp_article a, wp_delivery_article da 
		SET a.utimestamp = da.utimestamp 
		WHERE a.id = da.article_id  
		AND a.utimestamp < da.utimestamp 
		
		AND a.platform_id = da.platform_id
		AND a.platform_id = '{$this->platform_id}'");
		
	}

	/**Rckabwicklung*/	
	function set_return($D)
	{
		#Ermittlet die letzte Rechnungsnummer
		$qry = $this->SQL->query("SELECT MAX(number) number FROM wp_return WHERE platform_id = '{$this->platform_id}'");
		$a = $qry->fetchArray(SQLITE3_ASSOC);
		$NEW_NUM = $a['number']+1;

		foreach ((array) $D['PLATFORM']['D'][$this->platform_id]['RETURN']['D'] as $kRET => $RET) {
			if ($RET['ACTIVE'] != -2) {
				$IU_return .= (($IU_return) ? ',' : '') . "('{$kRET}','{$this->platform_id}'";
				$IU_return .= (isset($RET['INVOICE_ID'])) ? ",'{$RET['INVOICE_ID']}'" : ",NULL";
				$IU_return .= (isset($RET['PAYMENT_ID'])) ? ",'{$RET['PAYMENT_ID']}'" : ",NULL";
				$IU_return .= (isset($RET['ACTIVE'])) ? ",'{$RET['ACTIVE']}'" : ",NULL";
				$IU_return .= (isset($RET['STATUS'])) ? ",'{$RET['STATUS']}'" : ",NULL";

				if (isset($RET['NUMBER']) && $RET['NUMBER'] != '')
					$IU_return .= ",'{$RET['NUMBER']}'";
				else {
					$IU_return .= ",'{$NEW_NUM}'";
					$NEW_NUM++;
				}

				$IU_return .= (isset($RET['DATE'])) ? ",'{$RET['DATE']}'" : ",NULL";
				$IU_return .= (isset($RET['CUSTOMER_ID'])) ? ",'{$RET['CUSTOMER_ID']}'" : ",NULL";
				$IU_return .= (isset($RET['CUSTOMER_NICKNAME'])) ? ",'{$RET['CUSTOMER_NICKNAME']}'" : ",NULL";
				$IU_return .= (isset($RET['CUSTOMER_EMAIL'])) ? ",'{$RET['CUSTOMER_EMAIL']}'" : ",NULL";
				$IU_return .= (isset($RET['COMPANY'])) ? ",'{$RET['COMPANY']}'" : ",NULL";
				$IU_return .= (isset($RET['NAME'])) ? ",'{$RET['NAME']}'" : ",NULL";
				$IU_return .= (isset($RET['FNAME'])) ? ",'{$RET['FNAME']}'" : ",NULL";
				$IU_return .= (isset($RET['STREET'])) ? ",'{$RET['STREET']}'" : ",NULL";
				$IU_return .= (isset($RET['STREET_NO'])) ? ",'{$RET['STREET_NO']}'" : ",NULL";
				$IU_return .= (isset($RET['ZIP'])) ? ",'{$RET['ZIP']}'" : ",NULL";
				$IU_return .= (isset($RET['CITY'])) ? ",'{$RET['CITY']}'" : ",NULL";
				$IU_return .= (isset($RET['COUNTRY_ID'])) ? ",'{$RET['COUNTRY_ID']}'" : ",NULL";
				$IU_return .= (isset($RET['ADDITION'])) ? ",'{$RET['ADDITION']}'" : ",NULL";
				#$IU_return .= (isset($RET['WAREHOUSE_ID']))? ",'{$RET['WAREHOUSE_ID']}'":",NULL";
				$IU_return .= (isset($RET['COMMENT'])) ? ",'{$RET['COMMENT']}'" : ",NULL";
				$IU_return .= ")";


				foreach ((array) $RET['ARTICLE']['D'] as $kART => $ART) {
					if ($ART['ACTIVE'] != -2) {
						$IU_ARTICLE .= (($IU_ARTICLE) ? ',' : '') . "('{$kART}','{$kRET}','{$this->platform_id}'";
						$IU_ARTICLE .= (isset($ART['ACTIVE'])) ? ",'{$ART['ACTIVE']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['NUMBER'])) ? ",'{$ART['NUMBER']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['TITLE'])) ? ",'{$ART['TITLE']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['STOCK'])) ? ",'{$ART['STOCK']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['WEIGHT'])) ? ",'{$ART['WEIGHT']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['PRICE'])) ? ",'{$ART['PRICE']}'" : ",NULL";
						$IU_ARTICLE .= (isset($ART['VAT'])) ? ",'{$ART['VAT']}'" : ",NULL";
						$IU_ARTICLE .= ")";
					} else {
						$D_ARTICLE .= (($D_ARTICLE) ? ',' : '') . "'{$kART}{$kRET}'";
					}
				}
			} else {
				$D_return .= (($D_return) ? ',' : '') . "'{$kRET}'";
			}
		}
		#number = CASE WHEN excluded.number IS NOT NULL THEN excluded.number ELSE wp_invoice.number END,
		$this->SQL->query("INSERT INTO wp_return (id, platform_id,invoice_id,payment_id, active, status, number,date, customer_id, customer_nickname, customer_email
								,company,name,fname,street,street_no,zip,city,country_id,addition
								,comment) VALUES {$IU_return} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							payment_id =		CASE WHEN excluded.payment_id IS NOT NULL			AND ifnull(payment_id,'') <> excluded.payment_id				THEN excluded.payment_id ELSE payment_id END,
							active =			CASE WHEN excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active						THEN excluded.active ELSE active END,
							status =			CASE WHEN excluded.status IS NOT NULL				AND ifnull(status,'') <> excluded.status						THEN excluded.status ELSE status END,
							date =				CASE WHEN excluded.date IS NOT NULL					AND ifnull(date,'') <> excluded.date							THEN excluded.date ELSE date END,
							customer_id =		CASE WHEN excluded.customer_id IS NOT NULL			AND ifnull(customer_id,'') <> excluded.customer_id				THEN excluded.customer_id ELSE customer_id END,
							customer_nickname =	CASE WHEN excluded.customer_nickname IS NOT NULL	AND ifnull(customer_nickname,'') <> excluded.customer_nickname	THEN excluded.customer_nickname ELSE customer_nickname END,
							customer_email =	CASE WHEN excluded.customer_email IS NOT NULL		AND ifnull(customer_email,'') <> excluded.customer_email		THEN excluded.customer_email ELSE customer_email END,
							company =			CASE WHEN excluded.company IS NOT NULL				AND ifnull(company,'') <> excluded.company						THEN excluded.company ELSE company END,
							name =				CASE WHEN excluded.name IS NOT NULL					AND ifnull(name,'') <> excluded.name							THEN excluded.name ELSE name END,
							fname =				CASE WHEN excluded.fname IS NOT NULL				AND ifnull(fname,'') <> excluded.fname							THEN excluded.fname ELSE fname END,
							street =			CASE WHEN excluded.street IS NOT NULL				AND ifnull(street,'') <> excluded.street						THEN excluded.street ELSE street END,
							street_no =			CASE WHEN excluded.street_no IS NOT NULL			AND ifnull(street_no,'') <> excluded.street_no					THEN excluded.street_no ELSE street_no END,
							zip =				CASE WHEN excluded.zip IS NOT NULL					AND ifnull(zip,'') <> excluded.zip								THEN excluded.zip ELSE zip END,
							city =				CASE WHEN excluded.city IS NOT NULL					AND ifnull(city,'') <> excluded.city							THEN excluded.city ELSE city END,
							country_id =		CASE WHEN excluded.country_id IS NOT NULL			AND ifnull(country_id,'') <> excluded.country_id				THEN excluded.country_id ELSE country_id END,
							addition =			CASE WHEN excluded.addition IS NOT NULL				AND ifnull(addition,'') <> excluded.addition					THEN excluded.addition ELSE addition END,
							comment =			CASE WHEN excluded.comment IS NOT NULL				AND ifnull(comment,'') <> excluded.comment						THEN excluded.comment ELSE comment END,
							utimestamp =	CASE WHEN 
													   excluded.payment_id IS NOT NULL				AND ifnull(payment_id,'') <> excluded.payment_id				
													OR excluded.active IS NOT NULL					AND ifnull(active,'') <> excluded.active						
													OR excluded.status IS NOT NULL					AND ifnull(status,'') <> excluded.status						
													OR excluded.date IS NOT NULL					AND ifnull(date,'') <> excluded.date							
													OR excluded.customer_id IS NOT NULL				AND ifnull(customer_id,'') <> excluded.customer_id				
													OR excluded.customer_nickname IS NOT NULL		AND ifnull(customer_nickname,'') <> excluded.customer_nickname	
													OR excluded.customer_email IS NOT NULL			AND ifnull(customer_email,'') <> excluded.customer_email		
													OR excluded.company IS NOT NULL					AND ifnull(company,'') <> excluded.company						
													OR excluded.name IS NOT NULL					AND ifnull(name,'') <> excluded.name							
													OR excluded.fname IS NOT NULL					AND ifnull(fname,'') <> excluded.fname							
													OR excluded.street IS NOT NULL					AND ifnull(street,'') <> excluded.street						
													OR excluded.street_no IS NOT NULL				AND ifnull(street_no,'') <> excluded.street_no					
													OR excluded.zip IS NOT NULL						AND ifnull(zip,'') <> excluded.zip								
													OR excluded.city IS NOT NULL					AND ifnull(city,'') <> excluded.city							
													OR excluded.country_id IS NOT NULL				AND ifnull(country_id,'') <> excluded.country_id				
													OR excluded.addition IS NOT NULL				AND ifnull(addition,'') <> excluded.addition					
													OR excluded.comment IS NOT NULL					AND ifnull(comment,'') <> excluded.comment						
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		
		$this->SQL->query("INSERT INTO wp_return_article (id,return_id,platform_id,active,number,title,stock,weight,price,vat) VALUES {$IU_ARTICLE} 
						ON CONFLICT(id, return_id, platform_id) DO UPDATE SET
							platform_id =	CASE WHEN excluded.platform_id IS NOT NULL 	AND ifnull(platform_id,'') <> excluded.platform_id	THEN excluded.platform_id ELSE platform_id END,
							return_id =		CASE WHEN excluded.return_id IS NOT NULL	AND ifnull(return_id,'') <> excluded.return_id		THEN excluded.return_id ELSE return_id END,
							active =		CASE WHEN excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active			THEN excluded.active ELSE active END,
							number =		CASE WHEN excluded.number IS NOT NULL		AND ifnull(number,'') <> excluded.number			THEN excluded.number ELSE number END,
							title =			CASE WHEN excluded.title IS NOT NULL		AND ifnull(title,'') <> excluded.title				THEN excluded.title ELSE title END,
							stock =			CASE WHEN excluded.stock IS NOT NULL		AND ifnull(stock,'') <> excluded.stock				THEN excluded.stock ELSE stock END,
							weight =		CASE WHEN excluded.weight IS NOT NULL		AND ifnull(weight,'') <> excluded.weight			THEN excluded.weight ELSE weight END,
							price =			CASE WHEN excluded.price IS NOT NULL		AND ifnull(price,'') <> excluded.price				THEN excluded.price ELSE price END,
							vat =			CASE WHEN excluded.vat IS NOT NULL			AND ifnull(vat,'') <> excluded.vat					THEN excluded.vat ELSE vat END,
							utimestamp =	CASE WHEN 
													   excluded.platform_id IS NOT NULL	AND ifnull(platform_id,'') <> excluded.platform_id
													OR excluded.return_id IS NOT NULL	AND ifnull(return_id,'') <> excluded.return_id
													OR excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
													OR excluded.number IS NOT NULL		AND ifnull(number,'') <> excluded.number
													OR excluded.title IS NOT NULL		AND ifnull(title,'') <> excluded.title
													OR excluded.stock IS NOT NULL		AND ifnull(stock,'') <> excluded.stock
													OR excluded.weight IS NOT NULL		AND ifnull(weight,'') <> excluded.weight
													OR excluded.price IS NOT NULL		AND ifnull(price,'') <> excluded.price
													OR excluded.vat IS NOT NULL			AND ifnull(vat,'') <> excluded.vat
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($D_return)
		{
			$this->SQL->query("DELETE FROM wp_return WHERE id IN ({$D_return}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_return_article WHERE CONCAT(invoice_id) NOT IN (SELECT id FROM wp_invoice) AND platform_id = '{$this->platform_id}'");
		}
		if($D_ARTICLE)
			$this->SQL->query("DELETE FROM wp_return_article WHERE CONCAT(id,invoice_id) IN ({$D_ARTICLE}) AND platform_id = '{$this->platform_id}'");
	}
	
	function get_return(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['WAREHOUSE_ID'])? " AND warehouse_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['WAREHOUSE_ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['STATUS'])? " AND status IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['STATUS']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['DATE'])? " AND date LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['W']['DATE']}'":'';
		
		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['STEP'])? "LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['STEP']},{$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['START']}":(($D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['START'])?"LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['L']['START']}":"");
		
		$qry = $this->SQL->query("SELECT id,payment_id,active,status,number, date, customer_id, customer_nickname, customer_email
								,company,name,fname,street,street_no,zip,city,country_id,addition
								,comment,
								itimestamp*1 itimestamp
								,utimestamp
							FROM wp_return
							WHERE platform_id = '{$this->platform_id}' {$W} ORDER BY number DESC {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['id'] ] = [
				#'PLATFORM_ID'			=> $a['from_platform_id'], #From Platform ID
				'PAYMENT_ID'			=> $a['payment_id'],
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'NUMBER'				=> $a['number'],
				'DATE'					=> $a['date'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'COMPANY'				=> $a['company'],
				'NAME'					=> $a['name'],
				'FNAME'					=> $a['fname'],
				'STREET'				=> $a['street'],
				'STREET_NO'				=> $a['street_no'],
				'ZIP'					=> $a['zip'],
				'CITY'					=> $a['city'],
				'COUNTRY_ID'			=> $a['country_id'],
				'ADDITION'				=> $a['addition'],
				#'WAREHOUSE_ID'			=> $a['warehouse_id'],
				'COMMENT'				=> $a['comment'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
			
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['DATE']['D'][ $a['date'] ]['COUNT'] ++;
		}
		
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D']));
		$qry = $this->SQL->query("SELECT id,return_id, active,number,title,stock, CASE WHEN weight <> '' THEN weight ELSE 0 END WEIGHT,price,vat,
								itimestamp*1 itimestamp
								,utimestamp
							FROM wp_return_article oa 
							WHERE platform_id = '{$this->platform_id}' AND return_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['ARTICLE']['D'][ $a['id'] ] = array(
				'ACTIVE'				=> $a['active'],
				'NUMBER'				=> $a['number'],
				'TITLE'					=> $a['title'],
				'STOCK'					=> $a['stock'],
				'WEIGHT'				=> $a['WEIGHT'],
				'PRICE'					=> $a['price'],
				'VAT'					=> $a['vat'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
				);
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['PRICE'] += ($a['price'])*$a['stock'];#ToDo: Veraltet
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];#ToDo: Veraltet
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['ARTICLE']['WEIGHT'] += ($a['WEIGHT'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			
			
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['RETURN']['D'][ $a['return_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
		}
		
		#return $D;
	}
	
	function set_incominginvoice($D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'] AS $kI => $vI) {
			if($vI['ACTIVE'] != -2) {
				$IU_I .= (($IU_I)?',':'')."('{$kI}','{$this->platform_id}'";
				$IU_I .= (isset($vI['PAYMENT_ID']))? ",'{$vI['PAYMENT_ID']}'":",NULL";
				$IU_I .= (isset($vI['GROUP_ID']))? ",'{$vI['GROUP_ID']}'":",NULL";
				$IU_I .= (isset($vI['SUPPLIER_ID']))? ",'{$vI['SUPPLIER_ID']}'":",NULL";
				$IU_I .= (isset($vI['ACTIVE']))? ",'{$vI['ACTIVE']}'":",NULL";
				$IU_I .= (isset($vI['STATUS']))? ",'{$vI['STATUS']}'":",NULL";
				$IU_I .= (isset($vI['NUMBER']))? ",'{$vI['NUMBER']}'":",NULL";
				$IU_I .= (isset($vI['DATE']))? ",'{$vI['DATE']}'":",NULL";
				$IU_I .= (isset($vI['DATE_PAID']))? ",'{$vI['DATE_PAID']}'":",NULL";
				$IU_I .= (isset($vI['COMMENT']))? ",'{$vI['COMMENT']}'":",NULL";
				$IU_I .= ")";
				foreach((array)$vI['FILE']['D'] AS $kF => $vF) {
					if($vF['ACTIVE'] != -2) {
						$IU_FIL .= (($IU_FIL)?',':'')."('{$kI}','{$kF}','{$this->platform_id}'";
						$IU_FIL .= (isset($vF['ACTIVE']))? ",'{$vF['ACTIVE']}'":",NULL";
						$IU_FIL .= (isset($vF['TITLE']))? ",'{$vF['TITLE']}'":",NULL";
						$IU_FIL .= ")";
					}
					else {
						$D_FIL .= (($D_FIL)?',':'')."'{$kI}{$kF}'";
					}
				}
				
				foreach((array)$vI['ARTICLE']['D'] AS $kA => $vA) {
					if($vA['ACTIVE'] != -2) {
						$IU_ART .= (($IU_ART)?',':'')."('{$kA}','{$kI}','{$this->platform_id}'";
						$IU_ART .= (isset($vA['ACTIVE']))? ",'{$vA['ACTIVE']}'":",NULL";
						$IU_ART .= (isset($vA['NUMBER']))? ",'{$vA['NUMBER']}'":",NULL";
						$IU_ART .= (isset($vA['TITLE']))? ",'{$vA['TITLE']}'":",NULL";
						$IU_ART .= (isset($vA['STOCK']))? ",'{$vA['STOCK']}'":",NULL";
						$IU_ART .= (isset($vA['PRICE']))? ",'{$vA['PRICE']}'":",NULL";
						$IU_ART .= (isset($vA['VAT']))? ",'{$vA['VAT']}'":",NULL";
						$IU_ART .= ")";
					}
					else {
						$D_ART .= (($D_ART)?',':'')."'{$kI}{$kA}'";
					}
				}
			}
			else {
				$D_I .= (($D_I)?',':'')."'{$kI}'";
			}
		}

		$this->SQL->query("INSERT INTO wp_incominginvoice (id, platform_id, payment_id,group_id, supplier_id, active,status, number,date,date_paid,comment) VALUES {$IU_I} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							payment_id =	CASE WHEN excluded.payment_id IS NOT NULL		AND ifnull(payment_id,'') <> excluded.payment_id	THEN excluded.payment_id ELSE payment_id END,
							group_id =		CASE WHEN excluded.group_id IS NOT NULL			AND ifnull(group_id,'') <> excluded.group_id		THEN excluded.group_id ELSE group_id END,
							supplier_id =	CASE WHEN excluded.supplier_id IS NOT NULL		AND ifnull(supplier_id,'') <> excluded.supplier_id	THEN excluded.supplier_id ELSE supplier_id END,
							active =		CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active			THEN excluded.active ELSE active END,
							status =		CASE WHEN excluded.status IS NOT NULL			AND ifnull(status,'') <> excluded.status			THEN excluded.status ELSE status END,
							number =		CASE WHEN excluded.number IS NOT NULL			AND ifnull(number,'') <> excluded.number			THEN excluded.number ELSE number END,
							date =			CASE WHEN excluded.date IS NOT NULL				AND ifnull(date,'') <> excluded.date				THEN excluded.date ELSE date END,
							date_paid =		CASE WHEN excluded.date_paid IS NOT NULL		AND ifnull(date_paid,'') <> excluded.date_paid		THEN excluded.date_paid ELSE date_paid END,
							comment =		CASE WHEN excluded.comment IS NOT NULL			AND ifnull(comment,'') <> excluded.comment			THEN excluded.comment ELSE comment END,
							utimestamp =	CASE WHEN 
													   excluded.payment_id IS NOT NULL		AND ifnull(payment_id,'') <> excluded.payment_id
													OR excluded.group_id IS NOT NULL		AND ifnull(group_id,'') <> excluded.group_id 
													OR excluded.supplier_id IS NOT NULL		AND ifnull(supplier_id,'') <> excluded.supplier_id
													OR excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active
													OR excluded.status IS NOT NULL			AND ifnull(status,'') <> excluded.status
													OR excluded.number IS NOT NULL			AND ifnull(number,'') <> excluded.number
													OR excluded.date IS NOT NULL			AND ifnull(date,'') <> excluded.date
													OR excluded.date_paid IS NOT NULL		AND ifnull(date_paid,'') <> excluded.date_paid
													OR excluded.comment IS NOT NULL			AND ifnull(comment,'') <> excluded.comment
										THEN CURRENT_TIMESTAMP ELSE utimestamp END

						");
						
		$this->SQL->query("INSERT INTO wp_incominginvoice_article (id, incominginvoice_id,platform_id, active, number,title,stock,price,vat) VALUES {$IU_ART} 
						ON CONFLICT(id, platform_id, incominginvoice_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL	AND ifnull(active,'') <> excluded.active	THEN excluded.active ELSE active END,
							number =		CASE WHEN excluded.number IS NOT NULL	AND ifnull(number,'') <> excluded.number	THEN excluded.number ELSE number END,
							title =			CASE WHEN excluded.title IS NOT NULL	AND ifnull(title,'') <> excluded.title		THEN excluded.title ELSE title END,
							stock =			CASE WHEN excluded.stock IS NOT NULL	AND ifnull(stock,'') <> excluded.stock		THEN excluded.stock ELSE stock END,
							price =			CASE WHEN excluded.price IS NOT NULL	AND ifnull(price,'') <> excluded.price		THEN excluded.price ELSE price END,
							vat =			CASE WHEN excluded.vat IS NOT NULL		AND ifnull(vat,'') <> excluded.vat			THEN excluded.vat ELSE vat END,
							utimestamp =	CASE WHEN 
												   excluded.active IS NOT NULL	AND ifnull(active,'') <> excluded.active
												OR excluded.number IS NOT NULL	AND ifnull(number,'') <> excluded.number
												OR excluded.title IS NOT NULL	AND ifnull(title,'') <> excluded.title
												OR excluded.stock IS NOT NULL	AND ifnull(stock,'') <> excluded.stock
												OR excluded.price IS NOT NULL	AND ifnull(price,'') <> excluded.price
												OR excluded.vat IS NOT NULL		AND ifnull(vat,'') <> excluded.vat
										THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
						
		$this->SQL->query("INSERT INTO wp_incominginvoice_file (incominginvoice_id,file_id,platform_id,active,title) VALUES {$IU_FIL} 
						ON CONFLICT(incominginvoice_id, file_id, platform_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL	AND active <> excluded.active	THEN excluded.active ELSE active END,
							title =			CASE WHEN excluded.title IS NOT NULL	AND title <> excluded.title		THEN excluded.title ELSE title END,
							utimestamp =	CASE WHEN 
												   excluded.active IS NOT NULL	AND active <> excluded.active
												OR excluded.title IS NOT NULL	AND title <> excluded.title
										THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		if($D_I) {
			$this->SQL->query("DELETE FROM wp_incominginvoice WHERE id IN ({$D_I}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_incominginvoice_article WHERE CONCAT(incominginvoice_id) NOT IN (SELECT id FROM wp_incominginvoice) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_incominginvoice_file WHERE CONCAT(incominginvoice_id) NOT IN (SELECT id FROM wp_incominginvoice) AND platform_id = '{$this->platform_id}'");
		}
		if($D_FIL) {
			$this->SQL->query("DELETE FROM wp_incominginvoice_file WHERE CONCAT(incominginvoice_id,file_id) IN ({$D_FIL}) AND platform_id = '{$this->platform_id}'");
		}
		if($D_ART) {
			$this->SQL->query("DELETE FROM wp_incominginvoice_article WHERE CONCAT(incominginvoice_id,id) IN ({$D_ART}) AND platform_id = '{$this->platform_id}'");
		}
		return $D;
	}
	
	function get_incominginvoice(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID:IN'					=> "id IN ('[ID:IN]')",
			'GROUP_ID:IN'			=> "group_id IN ('[GROUP_ID:IN]')",
			'NUMBER'				=> "number LIKE '[NUMBER]'",
			'DATE'					=> "date LIKE '[DATE]'",
			'DATE_PAID:LIKE'		=> "date_paid LIKE '[DATE_PAID:LIKE]'",
			'COMMENT'				=> "comment LIKE '[COMMENT]'",
			'STATUS:IN'				=> "status IN ('[STATUS:IN]')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['W']);
		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['STEP'])? "LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['STEP']},{$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['START']}":(($D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['START'])?"LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['L']['START']}":"");
		$qry = $this->SQL->query("SELECT id,payment_id PAYMENT_ID,group_id GROUP_ID,supplier_id SUPPLIER_ID, active ACTIVE,status STATUS,number NUMBER,date DATE,date_paid DATE_PAID, comment COMMENT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_incominginvoice
							WHERE platform_id = '{$this->platform_id}' {$W} ORDER BY date_paid DESC {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['id'] ] = $a;
		}
		
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D']));
		$qry = $this->SQL->query("SELECT id,incominginvoice_id,active ACTIVE,number NUMBER,title TITLE,stock STOCK,price PRICE,vat VAT,itimestamp*1 ITIMESTAMP,utimestamp*1 UTIMESTAMP
							FROM wp_incominginvoice_article
							WHERE platform_id = '{$this->platform_id}' AND incominginvoice_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['ARTICLE']['D'][ $a['id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['ARTICLE']['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['ARTICLE']['VAT'] += ($a['PRICE']/(100+$a['VAT'])*$a['VAT'])*$a['STOCK'];

			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['VAT'] += $D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['ARTICLE']['VAT'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['DATE'] ]['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['DATE'] ]['VAT'] += ($a['PRICE']/(100+$a['VAT'])*$a['VAT'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['DATE'] ]['STOCK'] += $a['STOCK'];
		}
		
		$qry = $this->SQL->query("SELECT incominginvoice_id,file_id,iif.active,iif.title,f.name,f.size, iif.itimestamp,iif.utimestamp
							FROM wp_incominginvoice_file iif, wp_file f
							WHERE iif.file_id = f.id
								AND iif.platform_id = f.platform_id
								
								
								AND iif.platform_id = '{$this->platform_id}' AND incominginvoice_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$ext = pathinfo($a['name'], PATHINFO_EXTENSION);
			$D['PLATFORM']['D'][ $this->platform_id ]['INCOMINGINVOICE']['D'][ $a['incominginvoice_id'] ]['FILE']['D'][ $a['file_id'] ] = [
				'ACTIVE'				=> $a['active'],
				'NAME'					=> $a['name'],
				'TITLE'					=> $a['title'],
				'EXTENDSION'			=> $ext,
				'SIZE'					=> $a['size'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
		}
	}

	function set_buying($D)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'] AS $kI => $vI)
		{
			if($vI['ACTIVE'] != -2)
			{
				$IU_I .= (($IU_I)?',':'')."('{$kI}','{$this->platform_id}'";
				$IU_I .= (isset($vI['PAYMENT_ID']))? ",'{$vI['PAYMENT_ID']}'":",NULL";
				$IU_I .= (isset($vI['GROUP_ID']))? ",'{$vI['GROUP_ID']}'":",NULL";
				$IU_I .= (isset($vI['SUPPLIER_ID']))? ",'{$vI['SUPPLIER_ID']}'":",NULL";
				$IU_I .= (isset($vI['ACTIVE']))? ",'{$vI['ACTIVE']}'":",NULL";
				$IU_I .= (isset($vI['STATUS']))? ",'{$vI['STATUS']}'":",NULL";
				$IU_I .= (isset($vI['NUMBER']))? ",'{$vI['NUMBER']}'":",NULL";
				$IU_I .= (isset($vI['DATE']))? ",'{$vI['DATE']}'":",NULL";
				$IU_I .= (isset($vI['DATE_PAID']))? ",'{$vI['DATE_PAID']}'":",NULL";
				$IU_I .= (isset($vI['COMMENT']))? ",'{$vI['COMMENT']}'":",NULL";
				$IU_I .= ")";
				
				
				foreach((array)$vI['ARTICLE']['D'] AS $kA => $vA)
				{
					if($vA['ACTIVE'] != -2)
					{
						$IU_ART .= (($IU_ART)?',':'')."('{$kA}','{$kI}','{$this->platform_id}'";
						$IU_ART .= (isset($vA['ACTIVE']))? ",'{$vA['ACTIVE']}'":",NULL";
						$IU_ART .= (isset($vA['NUMBER']))? ",'{$vA['NUMBER']}'":",NULL";
						$IU_ART .= (isset($vA['TITLE']))? ",'{$vA['TITLE']}'":",NULL";
						$IU_ART .= (isset($vA['STOCK']))? ",'{$vA['STOCK']}'":",NULL";
						$IU_ART .= (isset($vA['PRICE']))? ",'{$vA['PRICE']}'":",NULL";
						$IU_ART .= (isset($vA['VAT']))? ",'{$vA['VAT']}'":",NULL";
						$IU_ART .= ")";
					}
					else
					{
						$D_ART .= (($D_ART)?',':'')."'{$kI}{$kA}'";
					}
				}
			}
			else
			{
				$D_I .= (($D_I)?',':'')."'{$kI}'";
			}
		}

		$this->SQL->query("INSERT INTO wp_buying (id, platform_id, payment_id,group_id, supplier_id, active,status, number,date,date_paid,comment) VALUES {$IU_I} 
						ON DUPLICATE KEY UPDATE
							payment_id = CASE WHEN excluded.payment_id IS NOT NULL THEN excluded.payment_id ELSE wp_buying.payment_id END,
							group_id = CASE WHEN excluded.group_id IS NOT NULL THEN excluded.group_id ELSE wp_buying.group_id END,
							supplier_id = CASE WHEN excluded.supplier_id IS NOT NULL THEN excluded.supplier_id ELSE wp_buying.supplier_id END,
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_buying.active END,
							status = CASE WHEN excluded.status IS NOT NULL THEN excluded.status ELSE wp_buying.status END,
							number = CASE WHEN excluded.number IS NOT NULL THEN excluded.number ELSE wp_buying.number END,
							date = CASE WHEN excluded.date IS NOT NULL THEN excluded.date ELSE wp_buying.date END,
							date_paid = CASE WHEN excluded.date_paid IS NOT NULL THEN excluded.date_paid ELSE wp_buying.date_paid END,
							comment = CASE WHEN excluded.comment IS NOT NULL THEN excluded.comment ELSE wp_buying.comment END
						");
						
		$this->SQL->query("INSERT INTO wp_buying_article (id, buying_id,platform_id, active, number,title,stock,price,vat) VALUES {$IU_ART} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_buying_article.active END,
							number = CASE WHEN excluded.number IS NOT NULL THEN excluded.number ELSE wp_buying_article.number END,
							title = CASE WHEN excluded.title IS NOT NULL THEN excluded.title ELSE wp_buying_article.title END,
							stock = CASE WHEN excluded.stock IS NOT NULL THEN excluded.stock ELSE wp_buying_article.stock END,
							price = CASE WHEN excluded.price IS NOT NULL THEN excluded.price ELSE wp_buying_article.price END,
							vat = CASE WHEN excluded.vat IS NOT NULL THEN excluded.vat ELSE wp_buying_article.vat END
						");
						
		if($D_I)
		{
			$this->SQL->query("DELETE FROM wp_buying WHERE id IN ({$D_I}) AND platform_id = '{$this->platform_id}'");
			$this->SQL->query("DELETE FROM wp_buying_article WHERE CONCAT(buying_id) NOT IN (SELECT id FROM wp_buying) AND platform_id = '{$this->platform_id}'");
		}
		
		if($D_ART)
			$this->SQL->query("DELETE FROM wp_buying_article WHERE CONCAT(buying_id,id) IN ({$D_ART}) AND platform_id = '{$this->platform_id}'");
		
	}
	#Waren Einkauf
	function get_buying(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID:IN'					=> "id IN ('[ID:IN]')",
			'GROUP_ID:IN'			=> "group_id IN ('[GROUP_ID:IN]')",
			'NUMBER'				=> "number LIKE '[NUMBER]'",
			'DATE'					=> "date LIKE '[DATE]'",
			'DATE_PAID:LIKE'		=> "date_paid LIKE '[DATE_PAID:LIKE]'",
			'COMMENT'				=> "comment LIKE '[COMMENT]'",
			'STATUS:IN'				=> "status IN ('[STATUS:IN]')",
		],$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['W']);
		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['STEP'])? "LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['STEP']},{$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['START']}":(($D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['START'])?"LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['L']['START']}":"");
		$qry = $this->SQL->query("SELECT id,payment_id PAYMENT_ID,group_id GROUP_ID,supplier_id SUPPLIER_ID, active ACTIVE,status STATUS,number NUMBER,date DATE,date_paid DATE_PAID, comment COMMENT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_buying
								WHERE platform_id = '{$this->platform_id}' {$W} {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['id'] ] = $a;
		}
		
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D']));
		$qry = $this->SQL->query("SELECT id,buying_id,active ACTIVE,number NUMBER,title TITLE,stock STOCK,price PRICE,vat VAT,itimestamp*1 ITIMESTAMP,utimestamp*1 UTIMESTAMP
								FROM wp_buying_article
								WHERE platform_id = '{$this->platform_id}' AND buying_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['ARTICLE']['D'][ $a['id'] ] = $a;
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['ARTICLE']['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['ARTICLE']['VAT'] += ($a['PRICE']/(100+$a['VAT'])*$a['VAT'])*$a['STOCK'];

			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['VAT'] += $D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['ARTICLE']['VAT'];
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['DATE'] ]['PRICE'] += ($a['PRICE'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['DATE'] ]['VAT'] += ($a['PRICE']/(100+$a['VAT'])*$a['VAT'])*$a['STOCK'];
			$D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['BUYING']['D'][ $a['buying_id'] ]['DATE'] ]['STOCK'] += $a['STOCK'];
		}
	}
	/*
	function get_supplier2(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ACTIVE'])? " AND id IN (SELECT id FROM wp_supplier2 WHERE type_id = 'SUPPLIER' AND attribute_id = 'ACTIVE' AND value = '{$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ACTIVE']}' )":'';
		
		$qry = $this->SQL->query("SELECT id, parent_id, type_id, platform_id, attribute_id, value VALUE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_supplier2
								WHERE platform_id = '{$this->platform_id}' AND type_id = 'SUPPLIER' {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ][ $a['type_id'] ]['D'][ $a['id'] ][ $a['attribute_id'] ] = $a;
		}
		
	}

	function set_supplier2(&$D=null)
	{
			foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D'] AS $kSup => $Sup) {
				foreach((array)$Sup AS $kATT => $ATT) {
					if($ATT['ACTIVE'] != -2 && isset($ATT['VALUE']))
					{
						$IU_Sup .= (($IU_Sup)?',':'')."('{$kSup}','{$this->platform_id}','SUPPLIER','{$kATT}','{$ATT['PARENT_ID']}'";
						#$IU_Sup .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
						$IU_Sup .= (isset($ATT['LANGUAGE_ID']))? ",'{$ATT['LANGUAGE_ID']}'":",''";
						$IU_Sup .= (isset($ATT['VALUE']))? ",'{$ATT['VALUE']}'":",NULL";
						$IU_Sup .= ")";
					}
					else {
						$D_SUP .= (($D_SUP)?',':'')."'{$kSup}{$ATT['PARENT_ID']}{$this->platform_id}SUPPLIER{$kATT}'";
					}
				}
			}
		
		if($IU_Sup) {
			$this->SQL->query("INSERT INTO wp_supplier2 (id, platform_id, type_id, attribute_id, parent_id, language_id, value) VALUES {$IU_Sup} 
						ON CONFLICT(id, parent_id, platform_id, type_id, attribute_id,language_id) DO UPDATE SET
							value =			CASE WHEN excluded.value IS NOT NULL	AND ifnull(value,'') <> excluded.value		THEN excluded.value ELSE value END,
							type_id =		CASE WHEN excluded.type_id IS NOT NULL	AND ifnull(type_id,'') <> excluded.type_id	THEN excluded.type_id ELSE type_id END,
							utimestamp =	CASE WHEN 
													excluded.value IS NOT NULL	AND ifnull(value,'') <> excluded.value
													OR excluded.type_id IS NOT NULL	AND ifnull(type_id,'') <> excluded.type_id
											THEN CURRENT_TIMESTAMP ELSE utimestamp END

						");
		}

		if($D_SUP) {
			$this->SQL->query("DELETE FROM wp_supplier2 WHERE id || parent_id || platform_id || type_id || attribute_id IN ({$D_SUP})");
		}
	}
*/
	#Alt
	function get_supplier(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, active ACTIVE, title TITLE, comment COMMENT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_supplier
								WHERE platform_id = '{$this->platform_id}' {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D'][ $a['id'] ] = $a;
		}

		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D']));

		#ATTRIBUTE START ===========================
		$qry = $this->SQL->query("SELECT supplier_id, attribute_id, parent_id PARENT_ID, active ACTIVE, value VALUE, utimestamp*1 UTIMESTAMP, itimestamp*1 ITIMESTAMP
										FROM wp_supplier_attribute
										WHERE supplier_id IN ('{$ID}')
											 AND platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D'][ $a['supplier_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = $a;
		}
			/*
		'ATTRIBUTE' => ['D' => [
					'Company'		=> ['TITLE' => '', 'VALUE' => ''],
					'CountryID'		=> ['TITLE' => '', 'VALUE' => 'DE'],
					'VatID'			=> ['TITLE' => '', 'VALUE' => ''],
					'CurrencyID'	=> ['TITLE' => '', 'VALUE' => 'EUR'],
					'Comment'		=> ['TITLE' => '', 'VALUE' => ''],
				]],
*/
		#ATTRIBUTE ENDE ============================


		#ARTICLE START =======================
		
		$qry = $this->SQL->query("SELECT supplier_id, article_id, reference_id REFERENCE_ID, active ACTIVE, price PRICE, stock STOCK, comment COMMENT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
								FROM wp_supplier_to_article
								WHERE platform_id = '{$this->platform_id}' AND supplier_id IN ('{$ID}') ");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D'][ $a['supplier_id'] ]['ARTICLE']['D'][ $a['article_id'] ] = $a;
		}
		#ARTICLE ENDE ========================
	}
	

	#Alt
	function set_supplier($D=null)
	{
		foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['SUPPLIER']['D'] AS $kSUP => $SUP)
		{
			if($SUP['ACTIVE'] != -2)
			{
				$IU_SUP .= (($IU_SUP)?',':'')."('{$kSUP}','{$this->platform_id}'";
				$IU_SUP .= (isset($SUP['ACTIVE']))? ",'{$SUP['ACTIVE']}'":",NULL";
				$IU_SUP .= (isset($SUP['TITLE']))? ",'{$SUP['TITLE']}'":",NULL";
				$IU_SUP .= (isset($SUP['COMMENT']))? ",'{$SUP['COMMENT']}'":",NULL";
				$IU_SUP .= ")";
				
				#ATTRIBUTE START ===============
				foreach((array)$SUP['ATTRIBUTE']['D'] AS $kATT => $ATT)
				{
					if($ATT['ACTIVE'] != -2)
					{
						$IU_ATT .= (($IU_ATT)?',':'')."('{$kSUP}','{$this->platform_id}','{$kATT}'";
						$IU_ATT .= (isset($ATT['PARENT_ID']))? ",'{$ATT['PARENT_ID']}'":",NULL";
						$IU_ATT .= (isset($ATT['ACTIVE']))? ",'{$ATT['ACTIVE']}'":",NULL";
						$IU_ATT .= (isset($ATT['VALUE']))? ",'{$ATT['VALUE']}'":",NULL";
						$IU_ATT .= ")";
					}
					else
					{
						$D_ART .= (($D_ART)?',':'')."'{$kSUP}{$kART}'";
					}
				}
				#ATTRIBUTE ENDE ================

				#ARTICLE START =================
				foreach((array)$SUP['ARTICLE']['D'] AS $kART => $ART)
				{
					if($ART['ACTIVE'] != -2)
					{
						$IU_ART .= (($IU_ART)?',':'')."('{$kSUP}','{$this->platform_id}','{$kART}'";
						$IU_ART .= (isset($ART['REFERENCE_ID']))? ",'{$ART['REFERENCE_ID']}'":",NULL";
						$IU_ART .= (isset($ART['ACTIVE']))? ",'{$ART['ACTIVE']}'":",NULL";
						$IU_ART .= (isset($ART['PRICE']))? ",'{$ART['PRICE']}'":",NULL";
						$IU_ART .= (isset($ART['STOCK']))? ",'{$ART['STOCK']}'":",NULL";
						$IU_ART .= (isset($ART['COMMENT']))? ",'{$ART['COMMENT']}'":",NULL";
						$IU_ART .= ")";
					}
					else
					{
						$D_ART .= (($D_ART)?',':'')."'{$kSUP}{$kART}'";
					}
				}
				#ARTICLE END ===================
			}
			else
			{
				$D_SUP .= (($D_SUP)?',':'')."'{$kSUP}'";
			}
		}
		
		if($IU_SUP)
			$this->SQL->query("INSERT INTO wp_supplier (id, platform_id, active, title, comment) VALUES {$IU_SUP} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_supplier.active END,
							title = CASE WHEN excluded.title IS NOT NULL THEN excluded.title ELSE wp_supplier.title END,
							comment = CASE WHEN excluded.comment IS NOT NULL THEN excluded.comment ELSE wp_supplier.comment END
						");
		if($IU_ART)
			$this->SQL->query("INSERT INTO wp_supplier_to_article (supplier_id,platform_id,article_id,reference_id,active,price,stock,comment) VALUES {$IU_ART} 
						ON DUPLICATE KEY UPDATE 
							reference_id = CASE WHEN excluded.reference_id IS NOT NULL THEN excluded.reference_id ELSE wp_supplier_to_article.reference_id END,
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_supplier_to_article.active END,
							price = CASE WHEN excluded.price IS NOT NULL THEN excluded.price ELSE wp_supplier_to_article.price END,
							stock = CASE WHEN excluded.stock IS NOT NULL THEN excluded.stock ELSE wp_supplier_to_article.stock END,
							comment = CASE WHEN excluded.comment IS NOT NULL THEN excluded.comment ELSE wp_supplier_to_article.comment END
						");
		
		if($IU_ATT)
			$this->SQL->query("INSERT INTO wp_supplier_attribute (supplier_id,platform_id,attribute_id,parent_id,active,value) VALUES {$IU_ATT} 
						ON DUPLICATE KEY UPDATE 
							parent_id = CASE WHEN excluded.parent_id IS NOT NULL THEN excluded.parent_id ELSE wp_supplier_attribute.parent_id END,
							active = CASE WHEN excluded.active IS NOT NULL THEN excluded.active ELSE wp_supplier_attribute.active END,
							value = CASE WHEN excluded.value IS NOT NULL THEN excluded.value ELSE wp_supplier_attribute.value END
						");
		#if($D_SUP)
		#$this->SQL->query("DELETE FROM wp_attribute WHERE id IN ({$D_SUP})  AND platform_id IS NULL");

		if($D_ART)
			$this->SQL->query("DELETE FROM wp_supplier_to_article WHERE CONCAT(supplier_id,article_id) IN ({$D_ART})  AND platform_id = '{$this->platform_id}'");
	}
}