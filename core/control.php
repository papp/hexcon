<?php
/*Strukturerklärung:
cache			=> Speichert in der Datenbank die Daten. Platform unabhängig
	platform	=> stellt platform unabhängige funktions erweiterungen zur verfügung und standarisiert die Platformen mit standarf funktionen.
		-xx0	=> Einzelne Platformen die individuelle Verarbeitung der Daten an den Standard zur verfügung stellt
		-xx1
		-xx2
		
control			=> Stellt eine Brücke zwischen zwei Platformen da. Bereitet Daten as einer PL für die andere PF vor. (z.B. Auflösen der Attribute-, Kategoriezuordnungen)
*/
#Managet zwischen den Platformen
class control
{
	function __construct($account_id, $platform_id)
	{
		global $SQL;
		$this->SQL = $SQL;
		$this->platform_id = $platform_id;
		$this->account_id = $account_id;
	}

	static function ini($account_id,$platform_id) #Wird benötigt um in Exprten andere Platform übergeben zu können
	{
		return new self($account_id,$platform_id);
	}
	
	function set_message($D)
	{
		$k = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['MESSAGE']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$MESSAGE = $D['PLATFORM']['D'][ $this->platform_id ]['MESSAGE']['D'][$k[$i]];
			if($MESSAGE['ACTIVE'] != -2)
			{
				$IU_MESSAGE .= (($IU_MESSAGE)?',':'')."('{$k[$i]}','{$MESSAGE['GROUP_ID']}','{$this->platform_id}'";
				$IU_MESSAGE .= (isset($MESSAGE['USER_ID']))? ",'{$MESSAGE['USER_ID']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['ACTIVE']))? ",'{$MESSAGE['ACTIVE']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['INTERNALLY']))? ",'{$MESSAGE['INTERNALLY']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['TITLE']))? ",'".$this->SQL->escapeString($MESSAGE['TITLE'])."'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['TEXT']))? ",'".$this->SQL->escapeString($MESSAGE['TEXT'])."'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['DATETIME']))? ",'{$MESSAGE['DATETIME']}'":",NULL";
				$IU_MESSAGE .= ")";
			}
			else
			{
				$D_MESSAGE .= (($D_MESSAGE)?',':'')."'{$k[$i]}'";
			}
		}
		
		$this->SQL->query("INSERT INTO wp_message (id,group_id, platform_id, user_id, active, internally, title, text, datetime) VALUES {$IU_MESSAGE} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_message.active END,
							internally = CASE WHEN VALUES(internally) IS NOT NULL THEN VALUES(internally) ELSE wp_message.internally END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_message.title END,
							text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE wp_message.text END,
							datetime = CASE WHEN VALUES(datetime) IS NOT NULL THEN VALUES(datetime) ELSE wp_message.datetime END
						");

		if($D_MESSAGE)
			$this->SQL->query("DELETE FROM wp_message WHERE id IN ({$D_MESSAGE}) AND platform_id = '{$this->platform_id}' ");
	
	}
	/*
	function set_categorie_to_categorie($D)
	{
		$k = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['D']['FROM']['PLATFORM_ID']);
		for($i=0; $i< count($k); $i++)
		{
			$MESSAGE = $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['D']['FROM']['PLATFORM_ID'][$k[$i]];
			if($CATEGORIE_TO_CATEGORIE['ACTIVE'] != -2)
			{
				$IU_MESSAGE .= (($IU_MESSAGE)?',':'')."('{$k[$i]}','{$MESSAGE['GROUP_ID']}','{$this->platform_id}'";
				$IU_MESSAGE .= (isset($MESSAGE['USER_ID']))? ",'{$MESSAGE['USER_ID']}'":",NULL";
				$IU_MESSAGE .= ")";
			}
			else
			{
				$D_MESSAGE .= (($D_MESSAGE)?',':'')."'{$k[$i]}'";
			}
		}
		
		$this->SQL->query("INSERT INTO wp_message (id,group_id, platform_id, user_id, active, internally, title, text, datetime) VALUES {$IU_MESSAGE} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_message.active END,
							internally = CASE WHEN VALUES(internally) IS NOT NULL THEN VALUES(internally) ELSE wp_message.internally END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_message.title END,
							text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE wp_message.text END,
							datetime = CASE WHEN VALUES(datetime) IS NOT NULL THEN VALUES(datetime) ELSE wp_message.datetime END
						");

		if($D_MESSAGE)
		$this->SQL->query("DELETE FROM wp_message WHERE id IN ({$D_MESSAGE}) AND platform_id = '{$this->platform_id}' ");
	}
	
	function get_categorie_to_categorie($D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['FROM_PLATFORM_ID'])? " AND from_platform_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['FROM_PLATFORM_ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['TO_PLATFORM_ID'])? " AND to_platform_id LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['TO_PLATFORM_ID']}'":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['FROM_CATEGORIE_ID'])? " AND from_categorie_id LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['FROM_CATEGORIE_ID']}'":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['TO_CATEGORIE_ID'])? " AND to_categorie_id LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['W']['TO_CATEGORIE_ID']}'":'';
		
		$L .= ($D['CATEGORIE_TO_CATEGORIE']['L']['START'] && $D['CATEGORIE_TO_CATEGORIE']['L']['STEP'])? "LIMIT {$D['CATEGORIE_TO_CATEGORIE']['L']['STEP']},{$D['CATEGORIE_TO_CATEGORIE']['L']['START']}":(($D['CATEGORIE_TO_CATEGORIE']['L']['START'])?"LIMIT {$D['CATEGORIE_TO_CATEGORIE']['L']['START']}":"");
		
		$qry = $this->SQL->query("SELECT from_platform_id,to_platform_id,from_categorie_id,to_categorie_id,active,itimestamp,utimestamp
								FROM wp_categorie_to_categorie
								WHERE to_platform_id = '{$this->platform_id}'
								{$W} ORDER BY {$O} 1 {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{ #$this->platform_id <-- TO Platform
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['FROM']['PLATFORM_ID'][ $a['from_platform_id'] ]['CATEGORIE_ID'][ $a['from_categorie_id'] ] = array(
				#'ACCOUNT_ID'	
				'CATEGORIE_ID'	=> $a['to_categorie_id'], #TO_CAT
				'ACTIVE'		=> $a['active'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				);
			
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['D'][ $a['to_categorie_id'] ] = array(
				#'ACCOUNT_ID'
				'PLATFORM_ID'	=> $a['from_platform_id'],
				'CATEGORIE_ID'	=> $a['from_categorie_id'],
				'ACTIVE'		=> $a['active'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				);
			
			$D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE_TO_CATEGORIE']['FROM']['PLATFORM_ID'][ $a['from_platform_id'] ]['CATEGORIE_ID'][ $a['from_categorie_id'] ]['TO']['CATEGORIE_ID'][ $a['to_categorie_id'] ] = array(
				#'ACCOUNT_ID'	
				'ACTIVE'		=> $a['active'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			);
		}
		return $D;
	}
	*/
	/*
	function set_order($D)
	{
		$k = (array)array_keys($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$ORDER = $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$k[$i]];
			if($ORDER['ACTIVE'] != -2)
			{
				#SET_CUSTOMER START ========================
				if($ORDER['CUSTOMER_ID'])
					$D['PLATFORM']['D'][ $this->platform_id ]['CUSTOMER']['D'][$ORDER['CUSTOMER_ID']] = array(
						'MESSAGE_GROUP_ID'=> $ORDER['MESSAGE_GROUP_ID'],
						'NICKNAME'		=> $ORDER['CUSTOMER_NICKNAME'],
						'EMAIL'			=> $ORDER['CUSTOMER_EMAIL'],
						'COMPANY'		=> $ORDER['BILLING']['COMPANY'],
						'NAME'			=> $ORDER['BILLING']['NAME'],
						'FNAME'			=> $ORDER['BILLING']['FNAME'],
						'STREET'		=> $ORDER['BILLING']['STREET'],
						'STREET_NO'		=> $ORDER['BILLING']['STREET_NO'],
						'ZIP'			=> $ORDER['BILLING']['ZIP'],
						'CITY'			=> $ORDER['BILLING']['CITY'],
						'COUNTRY_ID'	=> $ORDER['BILLING']['COUNTRY_ID'],
						'ADDITION'		=> $ORDER['BILLING']['ADDITION'],
					);
				#SET_CUSTOMER END   ========================
				
				$IU_ORDER .= (($IU_ORDER)?',':'')."('{$k[$i]}','{$this->platform_id}'";
				$IU_ORDER .= (isset($ORDER['PAYMENT_ID']))? ",'{$ORDER['PAYMENT_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['ACTIVE']))? ",'{$ORDER['ACTIVE']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_ID']))? ",'{$ORDER['CUSTOMER_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_NICKNAME']))? ",'{$ORDER['CUSTOMER_NICKNAME']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['CUSTOMER_EMAIL']))? ",'{$ORDER['CUSTOMER_EMAIL']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COMPANY']))? ",'{$ORDER['BILLING']['COMPANY']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['NAME']))? ",'".$this->SQL->escapeString($ORDER['BILLING']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['FNAME']))? ",'".$this->SQL->escapeString($ORDER['BILLING']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET']))? ",'".$this->SQL->escapeString($ORDER['BILLING']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['STREET_NO']))? ",'{$ORDER['BILLING']['STREET_NO']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ZIP']))? ",'{$ORDER['BILLING']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['CITY']))? ",'".$this->SQL->escapeString($ORDER['BILLING']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['COUNTRY_ID']))? ",'{$ORDER['BILLING']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['BILLING']['ADDITION']))? ",'{$ORDER['BILLING']['ADDITION']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COMPANY']))? ",'{$ORDER['DELIVERY']['COMPANY']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['NAME']))? ",'".$this->SQL->escapeString($ORDER['DELIVERY']['NAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['FNAME']))? ",'".$this->SQL->escapeString($ORDER['DELIVERY']['FNAME'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET']))? ",'".$this->SQL->escapeString($ORDER['DELIVERY']['STREET'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['STREET_NO']))? ",'{$ORDER['DELIVERY']['STREET_NO']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ZIP']))? ",'{$ORDER['DELIVERY']['ZIP']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['CITY']))? ",'".$this->SQL->escapeString($ORDER['DELIVERY']['CITY'])."'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['COUNTRY_ID']))? ",'{$ORDER['DELIVERY']['COUNTRY_ID']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['DELIVERY']['ADDITION']))? ",'{$ORDER['DELIVERY']['ADDITION']}'":",NULL";
				$IU_ORDER .= (isset($ORDER['COMMENT']))? ",'{$ORDER['COMMENT']}'":",NULL";
				$IU_ORDER .= ")";
				
				$kA = array_keys((array)$ORDER['ARTICLE']['D']);
				for($a=0;$a < count($kA); $a++)
				{
					$ARTICLE = $ORDER['ARTICLE']['D'][$kA[$a]];
					if($ARTICLE['ACTIVE'] != -2)
					{
						$IU_ARTICLE .= (($IU_ARTICLE)?',':'')."('{$kA[$a]}','{$k[$i]}','{$this->platform_id}'";
						$IU_ARTICLE .= (isset($ARTICLE['ACTIVE']))? ",'{$ARTICLE['ACTIVE']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['NUMBER']))? ",'{$ARTICLE['NUMBER']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['TITLE']))? ",'{$ARTICLE['TITLE']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['STOCK']))? ",'{$ARTICLE['STOCK']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['WEIGHT']))? ",'{$ARTICLE['WEIGHT']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['PRICE']))? ",'{$ARTICLE['PRICE']}'":",NULL";
						$IU_ARTICLE .= (isset($ARTICLE['VAT']))? ",'{$ARTICLE['VAT']}'":",NULL";
						$IU_ARTICLE .= ")";
					}
					else
					{
						$D_ARTICLE .= (($D_ARTICLE)?',':'')."'{$kA[$a]}{$k[$i]}'";
					}
				}
			}
			else
			{
				$D_ORDER .= (($D_ORDER)?',':'')."'{$k[$i]}'";
			}
		}
		
		$this->set_customer($D);#Kunden Speichern
		
		$this->SQL->query("INSERT INTO wp_order (id,platform_id, payment_id, active, customer_id, customer_nickname, customer_email
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition
								,comment) VALUES {$IU_ORDER} 
						ON DUPLICATE KEY UPDATE 
							payment_id = CASE WHEN VALUES(payment_id) IS NOT NULL THEN VALUES(payment_id) ELSE wp_order.payment_id END,
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_order.active END,
							customer_id = CASE WHEN VALUES(customer_id) IS NOT NULL THEN VALUES(customer_id) ELSE wp_order.customer_id END,
							customer_nickname = CASE WHEN VALUES(customer_nickname) IS NOT NULL THEN VALUES(customer_nickname) ELSE wp_order.customer_nickname END,
							customer_email = CASE WHEN VALUES(customer_email) IS NOT NULL THEN VALUES(customer_email) ELSE wp_order.customer_email END,
							billing_company = CASE WHEN VALUES(billing_company) IS NOT NULL THEN VALUES(billing_company) ELSE wp_order.billing_company END,
							billing_name = CASE WHEN VALUES(billing_name) IS NOT NULL THEN VALUES(billing_name) ELSE wp_order.billing_name END,
							billing_fname = CASE WHEN VALUES(billing_fname) IS NOT NULL THEN VALUES(billing_fname) ELSE wp_order.billing_fname END,
							billing_street = CASE WHEN VALUES(billing_street) IS NOT NULL THEN VALUES(billing_street) ELSE wp_order.billing_street END,
							billing_street_no = CASE WHEN VALUES(billing_street_no) IS NOT NULL THEN VALUES(billing_street_no) ELSE wp_order.billing_street_no END,
							billing_zip = CASE WHEN VALUES(billing_zip) IS NOT NULL THEN VALUES(billing_zip) ELSE wp_order.billing_zip END,
							billing_city = CASE WHEN VALUES(billing_city) IS NOT NULL THEN VALUES(billing_city) ELSE wp_order.billing_city END,
							billing_country_id = CASE WHEN VALUES(billing_country_id) IS NOT NULL THEN VALUES(billing_country_id) ELSE wp_order.billing_country_id END,
							billing_addition = CASE WHEN VALUES(billing_addition) IS NOT NULL THEN VALUES(billing_addition) ELSE wp_order.billing_addition END,
							delivery_company = CASE WHEN VALUES(delivery_company) IS NOT NULL THEN VALUES(delivery_company) ELSE wp_order.delivery_company END,
							delivery_name = CASE WHEN VALUES(delivery_name) IS NOT NULL THEN VALUES(delivery_name) ELSE wp_order.delivery_name END,
							delivery_fname = CASE WHEN VALUES(delivery_fname) IS NOT NULL THEN VALUES(delivery_fname) ELSE wp_order.delivery_fname END,
							delivery_street = CASE WHEN VALUES(delivery_street) IS NOT NULL THEN VALUES(delivery_street) ELSE wp_order.delivery_street END,
							delivery_street_no = CASE WHEN VALUES(delivery_street_no) IS NOT NULL THEN VALUES(delivery_street_no) ELSE wp_order.delivery_street_no END,
							delivery_zip = CASE WHEN VALUES(delivery_zip) IS NOT NULL THEN VALUES(delivery_zip) ELSE wp_order.delivery_zip END,
							delivery_city = CASE WHEN VALUES(delivery_city) IS NOT NULL THEN VALUES(delivery_city) ELSE wp_order.delivery_city END,
							delivery_country_id = CASE WHEN VALUES(delivery_country_id) IS NOT NULL THEN VALUES(delivery_country_id) ELSE wp_order.delivery_country_id END,
							delivery_addition = CASE WHEN VALUES(delivery_addition) IS NOT NULL THEN VALUES(delivery_addition) ELSE wp_order.delivery_addition END,
							comment = CASE WHEN VALUES(comment) IS NOT NULL THEN VALUES(comment) ELSE wp_order.comment END
						");
		
		$this->SQL->query("INSERT INTO wp_order_article (id,order_id,platform_id,active,number,title,stock,weight,price,vat) VALUES {$IU_ARTICLE} 
						ON DUPLICATE KEY UPDATE 
							order_id = CASE WHEN VALUES(order_id) IS NOT NULL THEN VALUES(order_id) ELSE wp_order_article.order_id END,
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_order_article.active END,
							number = CASE WHEN VALUES(number) IS NOT NULL THEN VALUES(number) ELSE wp_order_article.number END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_order_article.title END,
							stock = CASE WHEN VALUES(stock) IS NOT NULL THEN VALUES(stock) ELSE wp_order_article.stock END,
							weight = CASE WHEN VALUES(weight) IS NOT NULL THEN VALUES(weight) ELSE wp_order_article.weight END,
							price = CASE WHEN VALUES(price) IS NOT NULL THEN VALUES(price) ELSE wp_order_article.price END,
							vat = CASE WHEN VALUES(vat) IS NOT NULL THEN VALUES(vat) ELSE wp_order_article.vat END
						");
		if($D_ORDER)
		$this->SQL->query("DELETE FROM wp_order WHERE id IN ({$D_ORDER})");
		if($D_ARTICLE)
		$this->SQL->query("DELETE FROM wp_order_article WHERE CONCAT(id,order_id) IN ({$D_ARTICLE})");
	}
	*/
	
	#Gibt alle Order aller Platformen die an die Platform zu geordnet sind
	function get_order(&$D=null)
	{
		$W .= ($D['ORDER']['W']['ID'])? " AND o.id IN ('{$D['ORDER']['W']['ID']}')":'';
		$W .= ($D['ORDER']['W']['ID:NOT_IN'])? " AND o.id NOT IN ({$D['ORDER']['W']['ID:NOT_IN']})":'';#SQL Anweisung
		$W .= ($D['ORDER']['W']['STATUS'])? " AND o.status IN ('{$D['ORDER']['W']['STATUS']}')":'';
		$W .= ($D['ORDER']['W']['STATUS:IN'])? " AND o.status IN ({$D['ORDER']['W']['STATUS:IN']})":'';
		$W .= (isset($D['ORDER']['W']['STATUS:ISNULL']))? " AND o.status IS NULL":'';
		$W .= ($D['ORDER']['W']['ITIMESTAMP'])? " AND itimestamp LIKE '{$D['ORDER']['W']['ITIMESTAMP']}'":'';
		$W .= ($D['ORDER']['W']['DATE'])? " AND itimestamp LIKE '{$D['ORDER']['W']['DATE']}'":'';
		$W .= ($D['ORDER']['W']['DATE:>'])? " AND itimestamp > '{$D['ORDER']['W']['DATE:>']}'":'';
		
		$O .= ($D['ORDER']['O']['DATE'])?" itimestamp {$D['ORDER']['O']['DATE']},":" itimestamp DESC,";
		$L .= (isset($D['ORDER']['L']['START']) && isset($D['ORDER']['L']['STEP']))? "LIMIT {$D['ORDER']['L']['START']},{$D['ORDER']['L']['STEP']}":((isset($D['ORDER']['L']['STEP']))?"LIMIT {$D['ORDER']['L']['STEP']}":"");

		#Orders nur aus den Kind Platformen herforheben zur Parent Platform
		$W .= " AND platform_id IN (SELECT id FROM wp_platform WHERE parent_id = '{$this->platform_id}')";
		

		$qry = $this->SQL->query("SELECT id,platform_id,payment_id, active,status, customer_id, customer_nickname, customer_email, paid, currencycode, conversionrate
									,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition,billing_phone
									,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition,delivery_phone
									,warehouse_id,shipping_id,comment,itimestamp,utimestamp,
									DATE(itimestamp) date
								FROM wp_order o 
								WHERE 1
							{$W} ORDER BY {$O} 1 {$L}");
		/*
		$qry = $this->SQL->query("SELECT platform_id,active,platform_from_id,to_id,from_id,type,itimestamp,utimestamp
							FROM wp_relation
							WHERE platform_id = '{$this->platform_id}' AND type = 'PAYMENT'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$PAYMENT['from_id'] = $a['to_id'];
		}
		*/
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['platform_id'], #Platform_from_id
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'PAYMENT_ID'			=> $a['payment_id'],#$this->_get_payment($a['payment_id']),#$PAYMENT[ $a['payment_id'] ],#
				'DATE'					=> $a['date'],
				'PAID'					=> $a['paid'],
				'CURRENCYCODE'			=> $a['currencycode'],
				'CONVERSIONRATE'		=> $a['conversionrate'],
				'BILLING'				=> [
					'COMPANY'		=> $a['billing_company'],
					'NAME'			=> $a['billing_name'],
					'FNAME'			=> $a['billing_fname'],
					'STREET'		=> $a['billing_street'],
					'STREET_NO'		=> $a['billing_street_no'],
					'ZIP'			=> $a['billing_zip'],
					'CITY'			=> $a['billing_city'],
					'COUNTRY_ID'	=> $a['billing_country_id'],
					'ADDITION'		=> $a['billing_addition'],
					'PHONE'			=> $a['billing_phone'],
				],
				'DELIVERY'			=> [
					'COMPANY'		=> $a['delivery_company'],	
					'NAME'			=> $a['delivery_name'],
					'FNAME'			=> $a['delivery_fname'],
					'STREET'		=> $a['delivery_street'],
					'STREET_NO'		=> $a['delivery_street_no'],
					'ZIP'			=> $a['delivery_zip'],
					'CITY'			=> $a['delivery_city'],
					'COUNTRY_ID'	=> $a['delivery_country_id'],
					'ADDITION'		=> $a['delivery_addition'],
					'PHONE'			=> $a['delivery_phone'],
				],
				'WAREHOUSE_ID'			=> $a['warehouse_id'],
				'SHIPPING_ID'			=> $a['shipping_id'],
				'COMMENT'				=> $a['comment'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
		}
		
		
		if($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']) {
			$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']));
			/*
			$qry = $this->SQL->query("SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,oa.price,oa.vat,oa.itimestamp,oa.utimestamp,
								a.number,a.weight,a.parent_id,
								from_platform_id, from_article_id
								FROM wp_order_article oa 
									LEFT JOIN wp_article_reference ar ON oa.platform_id = ar.to_platform_id AND oa.id = ar.to_article_id
									INNER JOIN wp_article a ON ar.from_platform_id = a.platform_id AND ar.from_article_id = a.id
								WHERE ar.from_platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}')
								"); 
				*/				

			#Steuer Tabelle Start====================
			/*
			$Standard = "Normal"; #ToDo: Aus Einstellungen auslesen
			
			$qry = $this->SQL->query("SELECT tax_class_id, country_id,active ACTIVE, tax TAX FROM tax_class_to_country WHERE active = 1 AND tax_class_id IN ('{$Standard}') LIMIT 1");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				$D['TAX']['D'][ $a['tax_class_id'] ]['COUNTRY']['D'][ $a['country_id'] ] = $a;
			}
			*/
			#Neue Lösung MwSt Auslesen aus den Einstellungen
			$Standard = $D['SETTING']['D']['StandardTaxClass']['VALUE']; 
			$_tax = json_decode($D['SETTING']['D']['TaxClassToCountry']['VALUE'],1);
			
			#$_tax[ $Standard ][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['COUNTRY_ID'] ];
			#Steuer Tabelle Ende ====================

			#ToDo: Währungskurs automatisch ermitteln => Rechnungen werden mit Original Währung erstellt mit diesen auch das Order rein geht.
			#$_CurrencyCode = CWP::CurrencyCode('EUR');
			#$_CurrencyCode['EUR'] = 1;#Euro Kurs 1:1
			#$_CurrencyCode[NULL] = 1;#Work Around für alte Daten
			
			#1. Gib Artikel 1:1 aus.
			$qry = $this->SQL->query("SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,oa.price,oa.vat,oa.itimestamp,oa.utimestamp,
								number,weight
								FROM wp_order_article oa 
								WHERE order_id IN ('{$ID}')
								"); 
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				#PReis zurück rechnen in Brutto
				#$preis = $a['price'] /100 * (100+$a['vat']); #Steuer neutraller Preis
				##$preis = $preis /100 * (100+$D['TAX']['D'][ $Standard ]['COUNTRY']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['COUNTRY_ID'] ]['TAX']);#Errechnen neuen  Neto Preis an hand den Standard Steuersatz oder ToDo: der beim Artikel hinterlegten
				##$preis = $a['price'] / ( (100+$_tax[ $Standard ][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['COUNTRY_ID'] ])/100);
				$preis = $a['price'];#/ $_CurrencyCode[ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['CURRENCYCODE'] ];
				$vat = (float)$_tax[ $Standard ][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['COUNTRY_ID'] ]; 
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['id'] ] = [
					'PLATFORM_ID'			=> $this->platform_id,
					'PARENT_ID'				=> $a['parent_id'],
					#'ARTICLE_ID'			=> $a['from_article_id'],
					#'PARENT_ID'				=> $a['from_parent_id'],
					'ACTIVE'				=> $a['active'],
					'NUMBER'				=> $a['number'],
					'TITLE'					=> $a['title'],
					'STOCK'					=> $a['stock'],
					'WEIGHT'				=> $a['weight'],
					'PRICE'					=> $preis,#$a['price'],
					'VAT'					=> $vat,
					##'VAT'					=> (float)$D['TAX']['D'][ $Standard ]['COUNTRY']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DELIVERY']['COUNTRY_ID'] ]['TAX'],#$a['vat'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
				];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($preis)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($preis/100*$vat)*$a['stock'];
			

				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['PRICE'] += ($preis)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['VAT'] += ($preis/100*$vat)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($preis)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($preis/100*$vat)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
				
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($preis)*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($preis/100*$vat)*$a['stock'];

				#$A['ID'] .=($A['ID']?',':'')."'{$a['from_article_id']}'";
				
				$_A['ID'] .=($_A['ID']?',':'')."'{$a['number']}'";
			}
			
			#2. Versuche Artikel anhand Artikelnummer zu mappen.
			$qry = $this->SQL->query("SELECT id, number
								FROM wp_article 
								WHERE number IN ({$_A['ID']}) AND platform_id = '{$this->platform_id}'
								");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				foreach((array) $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'] AS $kO => $O) {
					if($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ]) {
						$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['id'] ] = $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ];
						unset($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ]);
					}
				}
			}
			
			
			#ToDo Prüfen!
			#3. Versuche Artikel anhand referenz_id zu mappen
			$qry = $this->SQL->query("SELECT to_article_id AS id, from_article_id AS number
								FROM wp_article_reference 
								WHERE from_article_id IN ({$_A['ID']}) AND to_platform_id = '{$this->platform_id}'
								");
			while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
				foreach((array) $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'] AS $kO => $O) {
					if($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ]) {
						$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['id'] ] = $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ];
						unset($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $kO ]['ARTICLE']['D'][ $a['number'] ]);
					}
				}
			}
			
			
			return 0;
			#ToDo: da Ebay nur referenz von Parent hat, muss wie folgt vorgegangen werden. Dieses Fall back gilt nur, wenn eine richtige Artikelnummer übergeben wurde!
			# Wenn Referenz Zuordnung einen Parent ausgibt dann
			# Vergleiche mit Artikelnummer bei der Variationen vom Parent der Referenz um die Variation raus zu picken
			# ToDo:INFO: (oa.id = ar.to_article_id OR oa.id LIKE CONCAT(ar.to_article_id,'\__') ) # Muss überprüft werden, weil der falsche wird immer noch zu geordnet
			#1. Prüfe ob ein Artikel mit der Referenz aber kein Vater artikel vorkommt
			#2. Prüfe ob anhand der Referenz ein Kindartikel gefunden werden kann
			#3. Prüfe Anhand der Artikelnummer. Vater Artikel werden ausgeschloßen.
			#4. Wenn obere drei Punkte nichts ergeben, dann gib Daten von der Platform aus
			$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D']));
			
			$qry = $this->SQL->query("SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,oa.price,oa.vat,oa.itimestamp,oa.utimestamp,
								a.number,a.weight,a.parent_id,
								from_platform_id, from_article_id
								FROM wp_order_article oa 
								LEFT JOIN wp_article_reference ar ON oa.platform_id = ar.to_platform_id AND oa.id = ar.to_article_id
								INNER JOIN wp_article a ON ar.from_platform_id = a.platform_id AND ar.from_article_id = a.id
								WHERE ar.from_platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}')
								
								AND (
									(a.variante_group_id IS NULL OR a.variante_group_id = '') ||
									 NOT EXISTS (SELECT 1 FROM wp_article v WHERE v.number = oa.number AND v.platform_id = a.platform_id AND (v.variante_group_id IS NULL OR v.variante_group_id = ''))
								)
								"); 
			
			#�bergibt auch Artikel die keine Referenz mehr haben.
			#1. ToDo: Zusätzlich sollen Artikel.Nummer überprüft werden. wenn Nummer von Child und Parent ID zur einander passen, dann soll diese statt Parent beforzugt werden!
			#2. ToDo: Gewicht angabe soll ebenfalls hier ausgegeben werden
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['from_article_id'] ] = [
					'PLATFORM_ID'			=> $this->platform_id,
					'PARENT_ID'				=> $a['parent_id'],
					#'ARTICLE_ID'			=> $a['from_article_id'],
					#'PARENT_ID'				=> $a['from_parent_id'],
					'ACTIVE'				=> $a['active'],
					'NUMBER'				=> $a['number'],
					'TITLE'					=> $a['title'],
					'STOCK'					=> $a['stock'],
					#'WEIGHT'				=> $a['weight'],
					'PRICE'					=> $a['price'],
					'VAT'					=> $a['vat'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
				];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			

				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
				
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];

				$A['ID'] .=($A['ID']?',':'')."'{$a['from_article_id']}'"; 
			}
			
			
			
			$qry = $this->SQL->query("
								SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,oa.price,oa.vat,oa.itimestamp,oa.utimestamp,
								a.number,a.weight,a.parent_id,
								from_platform_id, a.id
								FROM wp_order_article oa 
								LEFT JOIN wp_article_reference ar ON oa.platform_id = ar.to_platform_id AND (oa.id = ar.to_article_id OR oa.id LIKE (ar.to_article_id || '__') )
								INNER JOIN wp_article a ON ar.from_platform_id = a.platform_id AND ar.from_article_id = a.parent_id AND oa.number = a.number
								WHERE ar.from_platform_id = '{$this->platform_id}' AND order_id IN ('{$ID}')
								
"); 			#�bergibt auch Artikel die keine Referenz mehr haben.
			#1. ToDo: Zusätzlich sollen Artikel.Nummer überprüft werden. wenn Nummer von Child und Parent ID zur einander passen, dann soll diese statt Parent beforzugt werden!
			#2. ToDo: Gewicht angabe soll ebenfalls hier ausgegeben werden
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['from_article_id'] ] = [
					'PLATFORM_ID'			=> $this->platform_id,
					'PARENT_ID'				=> $a['parent_id'],
					#'ARTICLE_ID'			=> $a['from_article_id'],
					#'PARENT_ID'				=> $a['from_parent_id'],
					'ACTIVE'				=> $a['active'],
					'NUMBER'				=> $a['number'],
					'TITLE'					=> $a['title'],
					'STOCK'					=> $a['stock'],
					#'WEIGHT'				=> $a['weight'],
					'PRICE'					=> $a['price'],
					'VAT'					=> $a['vat'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
				];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			

				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
				
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];

				$A['ID'] .=($A['ID']?',':'')."'{$a['from_article_id']}'"; 
			}
			#ToDo: wp_platform_platform Veraltet
			$qry = $this->SQL->query("
				SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,IFNULL(oa.price,(a.price-a.price/(100+a.vat)*a.vat)),IFNULL(oa.vat,a.vat),oa.itimestamp,oa.utimestamp,
				a.number,a.weight,a.parent_id,
				a.platform_id, a.id
				FROM wp_order_article oa 
				INNER JOIN wp_article a ON oa.number = a.number
				LEFT JOIN wp_platform_platform pp ON pp.platform_from_id = oa.platform_id 
				WHERE pp.platform_to_id = '{$this->platform_id}' AND order_id IN ('{$ID}')
				AND (a.variante_group_id IS NULL OR a.variante_group_id = '')
				AND NOT EXISTS (SELECT 1 FROM wp_article_reference ar WHERE oa.platform_id = ar.to_platform_id AND (oa.id = ar.to_article_id OR oa.id LIKE (ar.to_article_id || '__') ) )
			"); 								
				#�bergibt auch Artikel die keine Referenz mehr haben.
			#1. ToDo: Zusätzlich sollen Artikel.Nummer überprüft werden. wenn Nummer von Child und Parent ID zur einander passen, dann soll diese statt Parent beforzugt werden!
			#2. ToDo: Gewicht angabe soll ebenfalls hier ausgegeben werden
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['from_article_id'] ] = [
					'PLATFORM_ID'			=> $this->platform_id,
					'PARENT_ID'				=> $a['parent_id'],
					#'ARTICLE_ID'			=> $a['from_article_id'],
					#'PARENT_ID'				=> $a['from_parent_id'],
					'ACTIVE'				=> $a['active'],
					'NUMBER'				=> $a['number'],
					'TITLE'					=> $a['title'],
					'STOCK'					=> $a['stock'],
					#'WEIGHT'				=> $a['weight'],
					'PRICE'					=> $a['price'],
					'VAT'					=> $a['vat'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
				];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			

				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
				
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];

				$A['ID'] .=($A['ID']?',':'')."'{$a['from_article_id']}'"; 
			}		
			$qry = $this->SQL->query("
								SELECT oa.id,oa.platform_id,order_id,oa.active,title,stock,oa.price,oa.vat,oa.itimestamp,oa.utimestamp,
								oa.number,NULL AS weight,NULL AS parent_id,
								pp.platform_to_id AS from_platform_id, oa.number AS from_article_id
								FROM wp_order_article oa 
								LEFT JOIN wp_platform_platform pp ON pp.platform_from_id = oa.platform_id
								WHERE pp.platform_to_id = '{$this->platform_id}' AND order_id IN ('{$ID}') 
								AND oa.id NOT IN (SELECT to_article_id FROM wp_article_reference ar WHERE oa.platform_id = ar.to_platform_id)
								AND SUBSTRING_INDEX(oa.id,'_',1) NOT IN (SELECT to_article_id FROM wp_article_reference ar WHERE oa.platform_id = ar.to_platform_id)
								
AND ( NOT EXISTS (SELECT v.number FROM wp_article v WHERE v.number = oa.number AND (v.variante_group_id IS NULL OR v.variante_group_id = '') AND v.platform_id = pp.platform_to_id ) )
		
								ORDER BY itimestamp"); 

			#�bergibt auch Artikel die keine Referenz mehr haben.
			#1. ToDo: Zusätzlich sollen Artikel.Nummer überprüft werden. wenn Nummer von Child und Parent ID zur einander passen, dann soll diese statt Parent beforzugt werden!
			#2. ToDo: Gewicht angabe soll ebenfalls hier ausgegeben werden
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['D'][ $a['from_article_id'] ] = [
					'PLATFORM_ID'			=> $this->platform_id,
					'PARENT_ID'				=> $a['parent_id'],
					#'ARTICLE_ID'			=> $a['from_article_id'],
					#'PARENT_ID'				=> $a['from_parent_id'],
					'ACTIVE'				=> $a['active'],
					'NUMBER'				=> $a['number'],
					'TITLE'					=> $a['title'],
					'STOCK'					=> $a['stock'],
					#'WEIGHT'				=> $a['weight'],
					'PRICE'					=> $a['price'],
					'VAT'					=> $a['vat'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
				];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			

				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
				
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
				$D['PLATFORM']['D'][ $this->platform_id ]['FROM_PLATFORM']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][  $a['order_id'] ]['PLATFORM_ID'] ]['ORDER']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][ $a['order_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];

				$A['ID'] .=($A['ID']?',':'')."'{$a['from_article_id']}'"; 
			}

			#Gewicht, Lager, Versand Ermitteln # Es wird Artikel mit allen Inforation geladen
			if($A['ID'])
			{
				$CLASS = $D['PLATFORM']['D'][$this->platform_id]['CLASS_ID'];
				$CLASS = 'this';
				$iPLF = new $CLASS($this->account_id,$this->platform_id);
				$d['PLATFORM']['D'][$this->platform_id]['ARTICLE']['W']['ID:IN'] = $A['ID']; #Suche Artikel anhand der Artikelnummer, grundsätzlich kann aber auch nach ID nachgeschla $a['from_article_id'] 
				
				$iPLF->get_article($d);
				if($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'])
				{
					foreach((array) $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'] AS $kORD => $ORD)
					{
						$WAREHOUSE_ID = '';
						foreach((array)$ORD['ARTICLE']['D'] AS $kART => $ART)
						{
							$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['ARTICLE']['D'][$kART]['WEIGHT'] = $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['WEIGHT'];
							
							if(!in_array($ART['NUMBER'],['0000','0010','0100'])) #Versand-,Rabatt und Artikel-Artikel ausschließen
							{
								#Wenn jedes Artikel zu geordnet werden konnte, so wird ein Lager bestimmt, sonnst = NULL
								
								#if( !array_key_exists('WAREHOUSE_ID',(array)array_keys($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD])) || $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['WAREHOUSE_ID'] !== NULL)
								if($WAREHOUSE_ID !== NULL)
								{
									if($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['SET'])
									{
										#ToDo: Die SET Artikel wurden nicht geladen dadurch steht die Information nicht zur verfügung!!!
										#$d['PLATFORM']['D'][$this->platform_id]['ARTICLE']['W']['ID:IN'] = "'".implode("','",array_keys((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['SET']))."'";
										#$iPLF->get_article($d);
										foreach((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['SET']['ARTICLE']['D'] AS $kSET => $SET)
										{
											$WIDs = array_keys((array)$SET['WAREHOUSE']['D']);
											#$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['WAREHOUSE_ID'] = ($WIDs[0])?$WIDs[0]:NULL;
											$WAREHOUSE_ID = ($WIDs[0] && $WAREHOUSE_ID !== NULL)?$WIDs[0]:NULL;
										}
									}
									else
									{
										$WIDs = array_keys((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['WAREHOUSE']['D']);
										#$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['WAREHOUSE_ID'] = ($WIDs[0])?$WIDs[0]:NULL;
										$WAREHOUSE_ID = ($WIDs[0])?$WIDs[0]:NULL;
									}
									
								}
							
								#ToDo: Muss eine gemeinsame  Versandart von allen Artikel gefunden werden. Lieferland und Kosten müssen berücksichtig werden.
								#ToDo: Die Platform selbst kann ebenfalls bestimmen über welche Versandart versendert werden soll. Soll diese auch beforzugen sonst NULL
								if(!array_key_exists('SHIPPING_ID',(array)$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]) || $D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['SHIPPING_ID'] === NULL)
								{
									if($d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['PARENT_ID'])#PARENT
										$WIDs = array_keys((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['PARENT_ID'] ]['SHIPPING']['D']);
									else #kein Parent
										$WIDs = array_keys((array)$d['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['SHIPPING']['D']);
									$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['SHIPPING_ID'] = ($WIDs[0])?$WIDs[0]:NULL;
								}
							}
						}
						if($D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['WAREHOUSE_ID'] === NULL)
							$D['PLATFORM']['D'][ $this->platform_id ]['ORDER']['D'][$kORD]['WAREHOUSE_ID'] = ($WAREHOUSE_ID != '')?$WAREHOUSE_ID:NULL;
					}
				}
			}

		}
		
		#SET START ======================
		/*
		$qry = $this->SQL->query("SELECT article_id, contain_article_id, aset.active, aset.quantity, aset.utimestamp, aset.itimestamp,
										a.weight, from_platform_id
										FROM wp_article_reference ar, wp_article_set aset, wp_article a
										WHERE 
												aset.platform_id = ar.from_platform_id 
											AND aset.article_id = ar.from_article_id
											AND aset.contain_article_id = a.id
											AND aset.platform_id = a.platform_id
											AND ar.from_platform_id = '{$this->platform_id}'
											");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['SET']['ARTICLE']['D'][ $a['contain_article_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'QUANTITY'		=> $a['quantity'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
			$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['WEIGHT'] += $a['weight']*$a['quantity'];
		}
		*/
		#SET ENDE =======================
		
		#return $D;
	}
	
	function get_invoice(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['ID'])? " AND id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['ID']}')":'';
		#$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['WAREHOUSE_ID'])? " AND warehouse_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['WAREHOUSE_ID']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['STATUS'])? " AND status IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['STATUS']}')":'';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['DATE'])? " AND date LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['W']['DATE']}'":'';
		
		$O .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['O']['NUMBER'])?" number {$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['O']['NUMBER']},":" number DESC,";
		
		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['STEP'])? "LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['STEP']},{$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['START']}":(($D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['START'])?"LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['L']['START']}":"");
		$qry = $this->SQL->query("SELECT id,from_platform_id,payment_id,active,status,number, date, customer_id, customer_nickname, customer_email
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition
								,traking_no,shipped_date,shipping_id,warehouse_id,comment,
								DATE_FORMAT(itimestamp,'%Y%m%d%h%i%s') itimestamp
								,utimestamp
							FROM wp_invoice
							WHERE platform_id IN (SELECT platform_from_id FROM wp_platform_platform WHERE platform_to_id = '{$this->platform_id}' AND type = 'invoice' ) {$W} 
							ORDER BY {$O} 1 {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['id'] ] = [
				'PLATFORM_ID'			=> $a['platform_id'], #From Platform ID
				'PAYMENT_ID'			=> $a['payment_id'],
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'NUMBER'				=> $a['number'],
				'DATE'					=> $a['date'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'BILLING'				=> array(
						'COMPANY'		=> $a['billing_company'],
						'NAME'			=> $a['billing_name'],
						'FNAME'			=> $a['billing_fname'],
						'STREET'		=> $a['billing_street'],
						'STREET_NO'		=> $a['billing_street_no'],
						'ZIP'			=> $a['billing_zip'],
						'CITY'			=> $a['billing_city'],
						'COUNTRY_ID'	=> $a['billing_country_id'],
						'ADDITION'		=> $a['billing_addition'],
						),
				'DELIVERY'				=> array(
						'COMPANY'		=> $a['delivery_company'],	
						'NAME'			=> $a['delivery_name'],
						'FNAME'			=> $a['delivery_fname'],
						'STREET'		=> $a['delivery_street'],
						'STREET_NO'		=> $a['delivery_street_no'],
						'ZIP'			=> $a['delivery_zip'],
						'CITY'			=> $a['delivery_city'],
						'COUNTRY_ID'	=> $a['delivery_country_id'],
						'ADDITION'		=> $a['delivery_addition'],
						),
				'TRACKING_NO'			=> $a['traking_no'],
				'SHIPPED_DATE'			=> $a['shipped_date'],
				'SHIPPING_ID'			=> $a['shipping_id'],
				'WAREHOUSE_ID'			=> $a['warehouse_id'],
				'COMMENT'				=> $a['comment'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
			
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['DATE']['D'][ $a['date'] ]['COUNT'] ++;
			
		}
		
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D']));
		$qry = $this->SQL->query("SELECT id,invoice_id,active,number,title,stock,weight,price,vat,
			DATE_FORMAT(itimestamp,'%Y%m%d%h%i%s') itimestamp
			,utimestamp
							FROM wp_invoice_article oa 
							WHERE platform_id = '{$this->platform_id}' AND invoice_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['D'][ $a['id'] ] = [
				'ACTIVE'				=> $a['active'],
				'NUMBER'				=> $a['number'],
				'TITLE'					=> $a['title'],
				'STOCK'					=> $a['stock'],
				'WEIGHT'				=> $a['weight'],
				'PRICE'					=> $a['price'],
				'VAT'					=> $a['vat'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['PRICE'] += ($a['price'])*$a['stock'];#ToDo: Veraltet
			$D['PLATF<ORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];#ToDo: Veraltet
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['WEIGHT'] += ($a['weight'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			
			
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['DATE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['INVOICE']['D'][ $a['invoice_id'] ]['DATE'] ]['STOCK'] += $a['stock'];
		}
		#return $D;
	}
	
	/*Gibt alle Artikel die an die Platform zu gewiesen worden sind*/
	#ToDo: da bis auf Categorie die Daten mit den aus z.B. this->get_article identisch sind, soll �berlegt werden dass die Daten dierekt aus den Quellclassen includet werden!
	function get_article(&$D=null)
	{
			$kFromPLA = $D['PLATFORM']['D'][$this->platform_id]['PARENT_ID'];
			##$FromPLA = $D['PLATFORM']['D'][$kFromPLA];
		###foreach((array)$D['PLATFORM']['D'][$this->platform_id]['TO']['PLATFORM']['D'] AS $kFromPLA => $FromPLA)
		###{
			###if($FromPLA['TYPE'] == 'order')
			if($kFromPLA) {
				
				$CLASS = $D['PLATFORM']['D'][$kFromPLA]['CLASS_ID'];
				$iPLF = new $CLASS($this->account_id,$kFromPLA);
				unset($D['PLATFORM']['D'][$kFromPLA]['ARTICLE']); #HotFix: Lösche vorher falls es bereits daten dran hängen z.B. beim cronjob2 
				$D['PLATFORM']['D'][$kFromPLA]['ARTICLE']['W'] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W'];#Setze Filter auf die From Platform
				$D['PLATFORM']['D'][$kFromPLA]['ARTICLE']['L'] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['L'];#Setze Filter auf die From Platform
				$D['PLATFORM']['D'][$kFromPLA]['ARTICLE']['O'] = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['O'];
				$D['PLATFORM']['D'][$kFromPLA]['ARTICLE']['W']['TO_PLATFORM_ID'] = $this->platform_id; #Gib nur Artikel die zur TO_PLF referenziert sind
				
				$iPLF->get_article($D);
				
				$iPLF->get_attribute($D);
				$iPLF->get_categorie($D);#ToDo: mit Where Arbeiten und nur die betroffenen Kategorien filtern.
				$D['PLATFORM']['D'][ $this->platform_id ] = $D['PLATFORM']['D'][$kFromPLA];
				
				$IDs = implode("','",(array)array_keys((array) $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'] ) );
				#CATEGORIE START =========================
				foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'] AS $kART => $ART)
				{
					#Workaround um das Problem zu beheben dass statt Copy eine Referenz übergeben wird.
					unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]);
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART] = $D['PLATFORM']['D'][ $kFromPLA ]['ARTICLE']['D'][$kART];
					
					#ToDo: Bestand minderung Hotfix
						$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['STOCK'] = ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['STOCK'] > 3)?$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['STOCK'] - 3 : 0;

					#Replace Platzhalter START =======================
					#foreach((array)$ART['ATTRIBUTETYPE']['D'] AS $kATTT => $ATTT)
					#{
						foreach((array)$ART['ATTRIBUTE']['D'] AS $kATT => $ATT)
						{
							#$Replace[$kART]['DE']['TITLE'][] = "([{$D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D']['DE']['TITLE']}])";
						/*	$_val = (($D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['I18N'])?$ATT['LANGUAGE']['D']['DE']['VALUE']:$ATT['VALUE']);
							$_val_result = CWP::rand_choice_str($_val,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ]);#Spinning anwenden
							
							
							if($kATT == 'TITLE' && count((array)$ART['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D']) > 0 && !$D['UPDATE'])#Suche nach möglichen freien Title
							{
								for($i = 0; $i < 3; $i++)#3 Chancen um einen Title zu finden
								{
									$_find = 0;
									$_val_result = CWP::rand_choice_str($_val,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ]);#Spinning neu anwenden
									foreach((array)$ART['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'] AS $kREF => $REF)
									{
										if($REF['DATA'])
										{
											$_val2[$kATT] = $REF['DATA']['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D']['DE']['VALUE'];
											if($_val2[$kATT] == $_val_result)
											{
												$_find = 1;
												break;
											}
										}
									}
									if(!$_find)
									{
										break;
									}
								}
								if($_find == 1) # Wenn kein alternative Title gefunden wurde, wird dieses Artikel enfernt um zu vermeiden dass dieses eingestellt wird
								{
									unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]);
								}
							}
							*/
							#erstelle Tabelle für Replace
							foreach((array) $D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'] AS $kLAN => $LAN)
							{
								$Replace[$kART][ $kLAN ]['TITLE'][] = "([{$D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['TITLE']}])";
								$Replace[$kART][ $kLAN ]['VALUE'][] = (($D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['I18N'])?$ATT['LANGUAGE']['D'][ $kLAN ]['VALUE']:$ATT['VALUE']);
							}
						}
					#}
					#Replace Platzhaler Ende =========================
					
					$FromCatID = key((array)$ART['CATEGORIE']['D']);
					
					#ATTRIBUTE MAPPING START ===================
					#foreach((array)$ART['ATTRIBUTETYPE']['D'] AS $kATTT => $ATTT)
					#{
						foreach((array)$ART['ATTRIBUTE']['D'] AS $kATT => $ATT)
						{
							if($D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['I18N'])
							{
								foreach((array)$ATT['LANGUAGE']['D'] AS $kLAN => $LAN)
								{
									#Replace =============
									$ATT['LANGUAGE']['D'][ $kLAN ]['VALUE'] = str_replace($Replace[$kART][ $kLAN ]['TITLE'],$Replace[$kART][ $kLAN ]['VALUE'],$ATT['LANGUAGE']['D'][ $kLAN ]['VALUE']);
									#Replace end =============

									#Spining =============
									$_val = $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['ATTRIBUTE']['D'][$kATT]['LANGUAGE']['D'][ $kLAN ]['VALUE'];
									$_val_result = CWP::rand_choice_str($_val,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ]);#Spinning anwenden
									
									if($kATT == 'TITLE' && count((array)$ART['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D']) > 0 && !$D['UPDATE'])#Suche nach möglichen freien Title
									{
										for($i = 0; $i < 3; $i++)#3 Chancen um einen Title zu finden
										{
											$_find = 0;
											$_val_result = CWP::rand_choice_str($_val,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ]);#Spinning neu anwenden
											foreach((array)$ART['PLATFORM']['D'][ $this->platform_id ]['REFERENCE']['D'] AS $kREF => $REF)
											{
												if($REF['DATA'])
												{
													$_val2[$kATT] = $REF['DATA']['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'];
													if($_val2[$kATT] == $_val_result)
													{
														$_find = 1;
														break;
													}
												}
											}
											if(!$_find)
											{
												break;
											}
										}
										if($_find == 1) # Wenn kein alternative Title gefunden wurde, wird dieses Artikel enfernt um zu vermeiden dass dieses eingestellt wird
										{
											unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]);
										}
									}
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][$kART]['ATTRIBUTE']['D'][$kATT]['LANGUAGE']['D'][ $kLAN ]['VALUE'] = $_val_result;
									#Spining end=============
								}

							}
							else
							{	
								#Replace =============
								$ATT['VALUE'] = str_replace($Replace[$kART]['DE']['TITLE'],$Replace[$kART]['DE']['VALUE'],$ATT['VALUE']);
								#Replace end =============
								#Spining =============
								$ATT['VALUE'] = CWP::rand_choice_str($ATT['VALUE'],['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ]);
								#Spining end =============
							}
							#=====================
							
							$neuATTID = $D['PLATFORM']['D'][ $this->platform_id ]['CATEGORIE']['D'][$FromCatID]['ATTRIBUTE']['D'][ $kATT]['TO']['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE_ID'];
							$neuATTID = (!$neuATTID)?$D['PLATFORM']['D'][ $kFromPLA ]['ATTRIBUTE']['D'][ $kATT ]['TO']['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE_ID']:$neuATTID;
							
							if($neuATTID)#Wenn für den Attribut eine Referenz ID gesetztist, so tausche die ID aus.
							{
								#ToDo: Achtung: Muss überprüft werden. Weil nun werden auch attribute mit umgemäppten Attributen an die Platform gesendet.
								#unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]);#vorhe altes Attribut Löschen
									
								if($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $neuATTID ]['VALUE']
									&& $neuATTID != $kATT) #stellt sicher damit namensgleiche Attribute nicht mehrfach zusammen gemäppt werden.
								{#Doppel Referenz gesetzt, daher kombiniere
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $neuATTID ]['VALUE'] .= '|'.$ATT['VALUE'];
								}
								elseif($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $neuATTID ]['LANGUAGE']['D']['DE']['VALUE']
									&& $neuATTID != $kATT) #stellt sicher damit namensgleiche Attribute nicht mehrfach zusammen gemäppt werden.
								{#Doppel Referenz gesetzt bei Lang Variable, daher kombiniere
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $neuATTID ]['LANGUAGE']['D']['DE']['VALUE'] .= '|'.$ATT['LANGUAGE']['D']['DE']['VALUE'];
								}
								else
								{
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $neuATTID ] = $ATT;
								}
								
								#Erstelle Variation_Group_id
								if(strpos($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $ART['PARENT_ID'] ]['VARIANTE_GROUP_ID'],(string)$kATT) !== false)
								{
									if(strpos($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $ART['PARENT_ID'] ]['VARIANTE_GROUP_ID'],(string)$neuATTID) !== false)
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $ART['PARENT_ID'] ]['VARIANTE_GROUP_ID'] = str_replace([$kATT.'|','|'.$kATT,$kATT],'',$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['PARENT_ID'] ]['VARIANTE_GROUP_ID']);
									else
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $ART['PARENT_ID'] ]['VARIANTE_GROUP_ID'] = str_replace([$kATT.'|','|'.$kATT,$kATT],[$neuATTID.'|','|'.$neuATTID,$neuATTID],$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['PARENT_ID'] ]['VARIANTE_GROUP_ID']);
								}
							}

						}
					#}
					

					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'] = array_replace_recursive((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'],(array)$ART['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']['D']); #Platform Abhängige Attribute werden überschrieben
					
					#Replace Platzhalter für Platform abhängige Attribute START =======================
					if($ART['PARENT_ID'])
					{
						$mergeATT = array_replace_recursive((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $ART['PARENT_ID'] ]['ATTRIBUTE']['D'], (array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D']);
						
						#Überprüft ob Attribute Replace im Title vorhanden sind und gibt weiter an Amazon Platform weiter damit ATT anhängen an Title deaktiviert wird.
						foreach((array)$mergeATT['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'] AS $kLG => $LG)
							if(strpos($mergeATT['ARTICLE']['ATTRIBUTE']['D']['TITLE']['LANGUAGE']['D'][$kLG]['VALUE'], '([') !== false )
							{
								$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['IS_TITLE_REPLACE_ATTRIBUTE'] = true;
							}
						
						foreach( $mergeATT AS $kATTT => $ATTT)
						{
							foreach((array)$ATTT['ATTRIBUTE']['D'] AS $kATT => $ATT)
							{
								if($ATT['LANGUAGE']['D'])
								{
									foreach((array)$ATT['LANGUAGE']['D'] AS $kLAN => $LAN)
									{
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ][ $kATTT ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] = str_replace(
											array_merge( (array)$Replace[$kART][ $kLAN ]['TITLE'], (array)$Replace[$ART['PARENT_ID']][ $kLAN ]['TITLE'] ),
											array_merge( (array)$Replace[$kART][ $kLAN ]['VALUE'], (array)$Replace[$ART['PARENT_ID']][ $kLAN ]['VALUE'] ),
											$ATT['LANGUAGE']['D'][ $kLAN ]['VALUE'] );
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ][ $kATTT ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] = preg_replace(['#\(\[.*?\]\) #','#\(\[.*?\]\)#'], '', $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] );#Lösche Platzhalter die nicht gibt
									}
								}
								else
								{
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] = str_replace(
										array_merge( (array)$Replace[$kART]['DE']['TITLE'], (array)$Replace[$ART['PARENT_ID']]['DE']['TITLE'] ),
										array_merge( (array)$Replace[$kART]['DE']['VALUE'], (array)$Replace[$ART['PARENT_ID']]['DE']['VALUE'] ),
										$ATT['VALUE'] );
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] = preg_replace(['#\(\[.*?\]\) #','#\(\[.*?\]\)#'], '', $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] );
								}
							}
						}
					}
					#Replace Platzhaler Ende =========================
					
					unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['PLATFORM']['D'][ $this->platform_id ]['ATTRIBUTE']);#Lösche den Zweig, weil oben bereits die ATT ersetzt wurden
					#ATTRIBUTE MAPPING ENDE ===================

					unset($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['CATEGORIE']);#Lösche Falsche Categorien

					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['PLATFORM_ID'] = $kFromPLA;
					#print_R($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]);exit;
				}
				
				#Replace Platzhalter für Platform abhängige Attribute START =======================
				#durchlaufe alle Attribute und lösche ([...]) das nicht ersetzt werden konnte. Das muss nochmal durchlaufen, damit beim Parent als letztes ersetzt wird.
				
				foreach((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'] AS $kART => $ART)
				{
					if(!$ART['PARENT_ID'])#Nur Vater aktuallisie
					{
						#foreach( $ART['ATTRIBUTETYPE']['D'] AS $kATTT => $ATTT)
						#{
							foreach((array)$ATTT['ATTRIBUTE']['D'] AS $kATT => $ATT)
							{
								if($ATT['LANGUAGE']['D']['DE']['VALUE'])
								{
									foreach((array)$ATT['LANGUAGE']['D'] AS $kLAN => $LAN)
									{
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] = str_replace(
											array_merge( (array)$Replace[$kART][ $kLAN ]['TITLE'], (array)$Replace[$ART['PARENT_ID']]['DE']['TITLE'] ),
											array_merge( (array)$Replace[$kART][ $kLAN ]['VALUE'], (array)$Replace[$ART['PARENT_ID']]['DE']['VALUE'] ),
											$ATT['LANGUAGE']['D'][ $kLAN ]['VALUE'] );
										$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] = preg_replace(['#\(\[.*?\]\) #','#\(\[.*?\]\)#'], '', $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['LANGUAGE']['D'][ $kLAN ]['VALUE'] );#Lösche Platzhalter die nicht gibt
									}
								}
								else
								{
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] = str_replace(
										array_merge( (array)$Replace[$kART]['DE']['TITLE'], (array)$Replace[$ART['PARENT_ID']]['DE']['TITLE'] ),
										array_merge( (array)$Replace[$kART]['DE']['VALUE'], (array)$Replace[$ART['PARENT_ID']]['DE']['VALUE'] ),
										$ATT['VALUE'] );
									$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] = preg_replace(['#\(\[.*?\]\) #','#\(\[.*?\]\)#'], '', $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $kART ]['ATTRIBUTE']['D'][ $kATT ]['VALUE'] );
								}
							}
						#}
					}
				}
				#Replace Platzhaler Ende =========================
				
				#print_r($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D']);exit;
				#ToDo: Durch platform2 Classe wird bereits bei get_categorie die Kategorie zur Platform ausgegeben, ggf. diese Funktion nutzen.
				$qry = $this->SQL->query("SELECT ctc.to_categorie_id, from_platform_id, article_id, ca.active, ctc.utimestamp, ctc.itimestamp, c.breadcrumb_id
									FROM wp_categorie_to_categorie ctc, wp_categorie c, wp_article_attribute ca, wp_article a
									WHERE
												ctc.from_categorie_id = ca.value
											AND ca.attribute_id = 'CategorieId'
											AND ctc.from_platform_id = ca.platform_id 
											AND ctc.to_platform_id = c.platform_id
											AND ctc.to_categorie_id = c.id
											AND ca.platform_id = a.platform_id
											AND ca.article_id = a.id
											AND ctc.to_platform_id = '{$this->platform_id}'
											AND a.id IN ('{$IDs}')
											ORDER BY ctc.itimestamp
										");
				while($a = $qry->fetchArray(SQLITE3_ASSOC))
				{
					$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['CATEGORIE']['D'][$a['to_categorie_id']] = [
						'ACTIVE'		=> $a['active'],
						'BREADCRUMB_ID' => $a['breadcrumb_id'],
						'UTIMESTAMP'	=> $a['utimestamp'],
						'ITIMESTAMP'	=> $a['itimestamp'],
						];
				}
				#CATEGORIE ENDE	==========================
			}
		####}
		unset($D['PLATFORM']['D'][ $kFromPLA ]['ARTICLE']['D']);#ToDo: Prüfen: Hotfix für Amazon Abgleich, damit Variaation_group_ID nicht gelert gespeichert wird
		#print_r($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D']['bjml2wj0a']);exit;
	}
	
	function set_customer($D)
	{
		$k = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['CUSTOMER']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$customer = $D['PLATFORM']['D'][ $this->platform_id ]['CUSTOMER']['D'][$k[$i]];
			if($customer['ACTIVE'] != -2)
			{
				$IU_customer .= (($IU_customer)?',':'')."('{$k[$i]}','{$this->platform_id}'";
				$IU_customer .= (isset($customer['MESSAGE_GROUP_ID']))? ",'{$customer['MESSAGE_GROUP_ID']}'":",NULL";
				$IU_customer .= (isset($customer['ACTIVE']))? ",'{$customer['ACTIVE']}'":",NULL";
				$IU_customer .= (isset($customer['NICKNAME']))? ",'{$customer['NICKNAME']}'":",NULL";
				$IU_customer .= (isset($customer['EMAIL']))? ",'{$customer['EMAIL']}'":",NULL";
				$IU_customer .= (isset($customer['COMPANY']))? ",'{$customer['COMPANY']}'":",NULL";
				$IU_customer .= (isset($customer['NAME']))? ",'{$customer['NAME']}'":",NULL";
				$IU_customer .= (isset($customer['FNAME']))? ",'{$customer['FNAME']}'":",NULL";
				$IU_customer .= (isset($customer['STREET']))? ",'{$customer['STREET']}'":",NULL";
				$IU_customer .= (isset($customer['STREET_NO']))? ",'{$customer['STREET_NO']}'":",NULL";
				$IU_customer .= (isset($customer['ZIP']))? ",'{$customer['ZIP']}'":",NULL";
				$IU_customer .= (isset($customer['CITY']))? ",'{$customer['CITY']}'":",NULL";
				$IU_customer .= (isset($customer['COUNTRY_ID']))? ",'{$customer['COUNTRY_ID']}'":",NULL";
				$IU_customer .= (isset($customer['ADDITION']))? ",'{$customer['ADDITION']}'":",NULL";
				$IU_customer .= ")";
			}
			else
			{
				$D_customer .= (($D_customer)?',':'')."'{$k[$i]}'";
			}
		}
			if($IU_customer)
				$this->SQL->query("INSERT INTO wp_customer (id,platform_id,message_group_id,active,nickname,email,company,name,fname,street,street_no,zip,city,country_id,addition) VALUES {$IU_customer} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_customer.active END,
							message_group_id = CASE WHEN VALUES(message_group_id) IS NOT NULL THEN VALUES(message_group_id) ELSE wp_customer.message_group_id END,
							nickname = CASE WHEN VALUES(nickname) IS NOT NULL THEN VALUES(nickname) ELSE wp_customer.nickname END,
							email = CASE WHEN VALUES(email) IS NOT NULL THEN VALUES(email) ELSE wp_customer.email END,
							company = CASE WHEN VALUES(company) IS NOT NULL THEN VALUES(company) ELSE wp_customer.company END,
							name = CASE WHEN VALUES(name) IS NOT NULL THEN VALUES(name) ELSE wp_customer.name END,
							fname = CASE WHEN VALUES(fname) IS NOT NULL THEN VALUES(fname) ELSE wp_customer.fname END,
							street = CASE WHEN VALUES(street) IS NOT NULL THEN VALUES(street) ELSE wp_customer.street END,
							street_no = CASE WHEN VALUES(street_no) IS NOT NULL THEN VALUES(street_no) ELSE wp_customer.street_no END,
							zip = CASE WHEN VALUES(zip) IS NOT NULL THEN VALUES(zip) ELSE wp_customer.zip END,
							city = CASE WHEN VALUES(city) IS NOT NULL THEN VALUES(city) ELSE wp_customer.city END,
							country_id = CASE WHEN VALUES(country_id) IS NOT NULL THEN VALUES(country_id) ELSE wp_customer.country_id END,
							addition = CASE WHEN VALUES(addition) IS NOT NULL THEN VALUES(addition) ELSE wp_customer.addition END
						");
		
	}
	
	//Verbindung FROM - Categorie/Attribute/PayMent/Shipping
	//type = CATEGORIE|ATTRIBUTE|PAYMENT|SHIPPING
	function get_relation($D=null)
	{
		$W .= CWP::sql_get_where2(array(
			'TYPE|IN'				=> "type [1] ('[V]')",
		),$D['PLATFORM']['D'][ $this->platform_id ]['RELATION']['W']);
			
		$qry = $this->SQL->query("SELECT platform_id,active,platform_from_id,to_id,from_id,type,itimestamp,utimestamp
							FROM wp_relation
							WHERE platform_id = '{$this->platform_id}' {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ][ $a['type'] ]['D'][ $a['to_id'] ]['RELATION']['PLATFORM']['D'][  $a['platform_from_id']  ][ $a['type'] ]['D'][ $a['from_id'] ] = array(
				'ACTIVE'				=> $a['active'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
			);
		}
		return $D;
	}
	
	function set_relation($D=null)
	{
		$tK = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]);
		for($t=0; $t< count($tK); $t++)#TYPE
		{
			$TYPE = $tK[$t];
			$toK = array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ][ $tK[$t] ]['D']);
			for($to=0; $to< count($toK); $to++)#TO
			{
				$PLATFORM_FROM = $D['PLATFORM']['D'][ $this->platform_id ]['type_IDxxx']['D'][ $toK[$to] ]['RELATION']['PLATFORM']['D'];
				$pK = array_keys((array)$PLATFORM_FROM);
				for($p=0; $p< count($pK); $p++)#PLATFORM_FROM
				{
					$PLATFORM = $PLATFORM_FROM[ $pK[$p] ];
					$fK = array_keys((array)$PLATFORM[ $TYPE ]['D']);
					for($f=0; $f< count($fK); $f++)#FROM
					{
						$FROM = $PLATFORM[ $TYPE ]['D'][ $fK[$f] ];
						if($customer['ACTIVE'] != -2)
						{
							$IU_relation .= (($IU_relation)?',':'')."('{$this->platform_id}','{$pK[$p]}','{$toK[$to]}','{$fK[$f]}','{$TYPE}'";
							$IU_relation .= (isset($FROM['ACTIVE']))? ",'{$FROM['ACTIVE']}'":",NULL";
							$IU_relation .= ")";
						}
						else
							$D_relation .= (($D_relation)?',':'')."'{$this->platform_id}{$pK[$p]}{$toK[$to]}{$fK[$f]}{$TYPE}'";
					}
					
				}
			}
			if($IU_relation)
				$this->SQL->query("INSERT INTO wp_relation (platform_id,platform_from_id,to_id,from_id,type,active) VALUES {$IU_relation} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_relation.active END
						");
			if($D_relation)
				$this->SQL->query("DELETE FROM wp_relation WHERE CONCAT(platform_id,platform_from_id,to_id,from_id,type) IN ({$D_relation})");
		
			return $D;
		}
	}
	
	#-----------------------------------
	/* DIE Platformen liefenr nun anhand der Standard Payments die richtige ID.
	function _get_payment($p)#ToDo: Notl�sung, muss payment zu Payment erstellt werden
	{
		switch($p)
		{
			case 'PayPal':#PayPal,
			case 'oxidpaypal':
				return 'PP';
				break;
			case 'None':
			case 'MoneyXferAcceptedInCheckout': #�berweisung
			case 'oxidpayadvance':
				return 'TP';
			case 'Other':#Amazon
				return 'AMAZON';
				break;
		}
	}*/
}