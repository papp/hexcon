<?php
class account
{
	function __construct($account_id)
	{
		global $SYSTEM;
		$this->SYSTEM = $SYSTEM;
		global  $MAIN;
		$this->MAIN = $MAIN;
		
		$this->account_id = $account_id;
		
		if($account_id) {
			$SQL = new SQLite3("data/ACCOUNT/{$account_id}/data.db");
			$SQL->exec('
				PRAGMA busy_timeout = 5000;
				PRAGMA cache_size = -2000;
				PRAGMA synchronous = 1;
				PRAGMA foreign_keys = ON;
				PRAGMA temp_store = MEMORY;
				PRAGMA default_temp_store = MEMORY;
				PRAGMA read_uncommitted = true;
				PRAGMA journal_mode = wal;
				PRAGMA wal_autocheckpoint=1000;
			');
		}
		$this->SQL = $SQL;
	}
	
	
	function set_data($TYPE_ID,&$D=null,$_to_id=null,$_to_type_id=null) #$_to_id bei Funktion aufrunff nicht nutzen!
	{
		#ToDo: Durch reqursivität wird SQL Einträge mehrfach durchgeführt. Ggf. sammeln und einmal ausführen.
		$TYPE_ID = (is_array($TYPE_ID))?$TYPE_ID:[$TYPE_ID];
		foreach((array)$TYPE_ID AS $kType => $Type) {
			foreach((array)$D[$Type]['D'] AS $kSup => $Sup) {
				if($Sup['Active']['Value'] != -2 && $D['PATTERN'][$Type]) { #Pattern: Überprüft ob erlaubtes Type übergeben wurde

					$IU_DATA .= (($IU_DATA)?',':'')."('{$kSup}','{$this->platform_id}','{$Type}','{$_to_id}','{$_to_type_id}')";

					foreach((array)$Sup AS $kATT => $ATT) {
						#echo $Type.'-'.$kATT.'|';
						if(is_array($ATT) && $D['PATTERN'][$Type]) {
							#print_R($kATT.'-'.$ATT); echo "|";
							if($ATT['D']) { #Verschachtelung
								
								$d[$kATT]['D'] = &$ATT['D'];
								$d['PATTERN'] = &$D['PATTERN'][$Type]['D'];
								$this->set_data($kATT, $d,$kSup,$Type);
							}
							elseif( $D['PATTERN'][$Type][$kATT] ) { #Pattern: Überprüfen ob erlaubtes Attribut des Types übergeben wurde
								#echo $Type.'-'.$kATT.'|';
								if($ATT['Value'] != '' && isset($ATT['Value'])) {
									$IU_DATA_ATT .= (($IU_DATA_ATT)?',':'')."('{$kSup}','{$this->platform_id}','{$Type}','{$kATT}'";
									$IU_DATA_ATT .= (isset($ATT['LANGUAGE_ID']))? ",'{$ATT['LANGUAGE_ID']}'":",''";#ToDo: Language ID übergeben!
									$IU_DATA_ATT .= (isset($ATT['Value']))? ",'{$ATT['Value']}'":",NULL";
									$IU_DATA_ATT .= ")";
								}
								else {
									$D_DATA_ATT .= (($D_DATA_ATT)?',':'')."'{$kSup}{$this->platform_id}{$Type}{$kATT}'";
								}
							}
						}
					}
				}
				else {
					#ToDo: Wenn Patern nicht vorhandeen ist, wird alles fälschlicherweise gelöscht. Muss korigieren!!!
					$D_ALLDELETE .= (($D_ALLDELETE)?',':'')."'{$kSup}{$this->platform_id}'";
				}
			}
		}

		if($IU_DATA) {
			$this->SQL->query("INSERT INTO wp_data (id, platform_id, type_id, to_data_id,to_type_id) VALUES {$IU_DATA} 
						ON CONFLICT(id, platform_id, type_id, to_data_id,to_type_id) DO UPDATE SET
							to_data_id =			CASE WHEN excluded.to_data_id IS NOT NULL	AND ifnull(to_data_id,'') <> excluded.to_data_id		THEN excluded.to_data_id ELSE to_data_id END,
							to_type_id =			CASE WHEN excluded.to_type_id IS NOT NULL	AND ifnull(to_type_id,'') <> excluded.to_type_id		THEN excluded.to_type_id ELSE to_type_id END,
							utimestamp =	CASE WHEN 
												excluded.to_type_id IS NOT NULL	AND ifnull(to_type_id,'') <> excluded.to_type_id
											THEN cast(strftime('%s', 'now') as int) ELSE utimestamp END
						");#CURRENT_TIMESTAMP 1665989041
						#itimestamp =	CASE WHEN excluded.itimestamp IS NULL THEN excluded.cast(strftime('%s', 'now') as int) ELSE itimestamp END
		}

		if($IU_DATA_ATT) {
			$this->SQL->query("INSERT INTO wp_data_att (id, platform_id, type_id, attribute_id, language_id, value) VALUES {$IU_DATA_ATT} 
						ON CONFLICT(id, platform_id, type_id, attribute_id,language_id) DO UPDATE SET
							value =			CASE WHEN excluded.value IS NOT NULL	AND ifnull(value,'') <> excluded.value		THEN excluded.value ELSE value END,
							utimestamp =	CASE WHEN 
												excluded.value IS NOT NULL	AND ifnull(value,'') <> excluded.value
											THEN cast(strftime('%s', 'now') as int) ELSE utimestamp END
						");#CURRENT_TIMESTAMP 1665989041
						#itimestamp =	CASE WHEN excluded.itimestamp IS NULL THEN excluded.cast(strftime('%s', 'now') as int) ELSE itimestamp END
		}

		if($D_ALLDELETE) {
			#echo "DELETE FROM wp_data WHERE (id || platform_id) IN ({$D_ALLDELETE})";
			$this->SQL->query("DELETE FROM wp_data WHERE (id || platform_id) IN ({$D_ALLDELETE})"); #Lösche Vater
			$this->SQL->query("DELETE FROM wp_data WHERE (to_data_id || platform_id) NOT IN (SELECT (id || platform_id) FROM wp_data WHERE platform_id = '{$this->platform_id}' AND to_data_id  = '') AND platform_id = '{$this->platform_id}' AND to_data_id  <> ''");#Lösche RefIds
			#Lösche verwaiste Kinder Zweige aus wp_data nur für jeweilige Platform wegen Performance
			$this->SQL->query("DELETE FROM wp_data_att WHERE (id || platform_id) NOT IN (SELECT (id || platform_id) FROM wp_data WHERE  platform_id = '{$this->platform_id}' ) AND platform_id = '{$this->platform_id}'");
		}
		if($D_DATA_ATT) { #Lösche leeres Attribut
			$this->SQL->query("DELETE FROM wp_data_att WHERE id || platform_id || type_id || attribute_id IN ({$D_DATA_ATT})");
		}

			#Cache Refresh start =================
			if($_to_id === NULL) {#Soll nur einmal ausführen, nicht reqursive
				#ToDo: Performance Problem, wenn über die gesamte Tabelle gesucht wird. Es muss ids im Cache hinzugefügt werden diese gerade geändert wurden. Cache Aufwärmen Funktion separieren!
				
				#Lösche alte temp Einträge
				$this->SQL->query("DELETE FROM wp_data_tmp AS dtmp WHERE EXISTS (SELECT 1 FROM wp_data_att WHERE id = dtmp.id AND utimestamp > dtmp.utimestamp)");
				#$this->SQL->query("DELETE FROM wp_data_tmp AS dtmp WHERE EXISTS (SELECT 1 FROM wp_data WHERE id = dtmp.id AND utimestamp > dtmp.utimestamp)");
				#wenn nichtg im Cache dann
				$qry = $this->SQL->query("SELECT id, platform_id, type_id, attribute_id, language_id, value
										FROM wp_data_att dt
										WHERE NOT EXISTS (SELECT 1 FROM wp_data_tmp WHERE dt.id = id AND dt.platform_id = platform_id AND dt.type_id = type_id )
											AND platform_id = '{$this->platform_id}'
										");
				
				while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
					$set_d[ $a['type_id'] ]['D'][ $a['id'] ][ $a['attribute_id'] ] = 
					($a['language_id'] == '')? ['Value' => $a['value']] : [ 'LANGUAGE' => [ 'D' => [ $a['language_id'] => [ 'Value' => $a['value'] ]] ] ] ;
				}

				##print_r($set_d);
				#Speichere neue Daten im Cache
				#Erstelle Cache
				$IU_DATA_ATT = '';
				foreach((array)$set_d AS $kType => $Type) {
						foreach((array)$Type['D'] AS $kSup => $Sup) {
							$IU_DATA_ATT .= (($IU_DATA_ATT)?',':'')."('{$kSup}','{$this->platform_id}','{$kType}'";
							$IU_DATA_ATT .= ",'".$this->SQL->escapeString(json_encode($Sup))."'";
							$IU_DATA_ATT .= ")";
						}
				}

				if($IU_DATA_ATT) {
					$this->SQL->query("REPLACE INTO wp_data_tmp (id, platform_id, type_id, data) VALUES {$IU_DATA_ATT} 
								ON CONFLICT(id, platform_id, type_id) DO UPDATE SET
									data =			CASE WHEN excluded.data IS NOT NULL	AND ifnull(data,'') <> excluded.data		THEN excluded.data ELSE data END,
									utimestamp =	CASE WHEN excluded.data IS NOT NULL	AND ifnull(data,'') <> excluded.data
													THEN cast(strftime('%s', 'now') as int) ELSE utimestamp END

								");
				}
				#Lösche verwaiste Cache Daten
				$this->SQL->query("DELETE FROM wp_data_tmp AS dtmp WHERE NOT EXISTS (SELECT 1 FROM wp_data WHERE id = dtmp.id AND dtmp.platform_id = platform_id AND dtmp.type_id = type_id)");
			}
			#Cache Refresh end================

	}

	function get_data($TYPE_ID, &$D=null,$_to_id=null)
	{
		#Info: Sonder Filterung: ...[W][ID] | [W][toID] |
		foreach((array)$D[$TYPE_ID]['W'] AS $kR => $R) {
			if($kR != 'ID' && $kR != 'toID') {
				$W .= (($W)?' OR ':' AND ').' ( ';
				$W2 = '';
				foreach((array)$R AS $kWW => $WW) {
					$W2 .= (($W2)?' AND ':'')." attribute_id IN ('{$kWW}') AND value IN ('{$WW}') ";
				}
				$W .= "{$W2} ) ";
			}
			else {
				$W1 .= ($kR == 'ID')?" AND dtmp.id IN ('{$R}') ":'';
				$W1 .= ($kR == 'toID')? " AND id IN (SELECT id FROM wp_data WHERE to_data_id IN ('{$R}') ) AND platform_id = '{$this->platform_id}' ":'';
			}
		}
		

		$W1 .= ($W)? " AND EXISTS (SELECT dtmp.id FROM wp_data_att WHERE dtmp.id = id AND dtmp.platform_id = platform_id AND dtmp.type_id = type_id {$W})" : '';
		
		$W1 .= ($_to_id)? " AND id IN (SELECT id FROM wp_data WHERE to_data_id IN ('{$_to_id}') ) AND platform_id = '{$this->platform_id}' ":'';
		$W1 .= ($TYPE_ID)? " AND dtmp.type_id = '{$TYPE_ID}' ":'';

		$L .= (isset($D[$TYPE_ID]['L']['START']) && $D[$TYPE_ID]['L']['STEPP'])? "LIMIT {$D[$TYPE_ID]['L']['START']},{$D[$TYPE_ID]['L']['STEPP']}":'';
		
		if($D[$TYPE_ID]['O']) {
			$O = "ORDER BY ";
			foreach ((array)$D[$TYPE_ID]['O'] as $key => $value) {
				$O .= ((strlen($O) > 10)?',':'')." (SELECT id FROM wp_data WHERE dtmp.id = to_data_id AND dtmp.platform_id = platform_id AND dtmp.type_id = type_id AND to_data_id = '{$key}' ) {$value}";
			}
		}
		
		#$time = microtime();
		
		$qry = $this->SQL->query("SELECT dtmp.id, dtmp.type_id, data 
									FROM wp_data_tmp dtmp
									WHERE 1 {$W1} AND dtmp.platform_id = '{$this->platform_id}'
									{$L} {$O}");
		#Info: strftime('%Y%m%d%H%M%S', datetime(itimestamp,'unixepoch'))
		#echo '[time(db)'.(microtime() - $time).']|';$time = microtime();
		while($a = $qry->fetchArray(SQLITE3_NUM)) {
			$D[ $a[1] ]['D'][$a[0]] = array_merge_recursive((array)$D[ $a[1] ]['D'][$a[0]],json_decode($a[2],1));
		}
		#echo '[time(feach)'.(microtime() - $time).']|';

		#Prüfe weitere Ebenen
		#ToDo: *bearbeiten, besser machen: Achtung ist nicht reqursive, kann nur eine Ebenne mehr auslesen!
		if($D[ $TYPE_ID ]['D']) {
			$ID = implode("','",array_keys((array)$D[ $TYPE_ID ]['D']));
			#echo $ID.'|';
			$_type_id = NULL;
			$this->get_data($_type_id, $d,$ID);
			
			foreach ((array)$d as $key => $value) {
			
				$id = implode("','",array_keys((array)$d[ $key ]['D']));
				#echo $TYPE_ID.'-'.$key.'-'.$id.'|';
				$qry = $this->SQL->query("SELECT id, type_id, to_data_id, to_type_id
									FROM wp_data
									WHERE id IN ('{$id}') AND platform_id = '{$this->platform_id}' AND type_id = '{$key}' AND to_type_id = '{$TYPE_ID}'");
				while($a = $qry->fetchArray(SQLITE3_NUM)) {
						$D[ $a[3] ]['D'][ $a[2] ][ $a[1] ]['D'][ $a[0] ] = $value['D'][ $a[0] ];
				}
			}

		}

	}


	function get_task(&$D=null)
	{
		$W .= CWP::where_interpreter([
			'ID'				=> " id IN ('[ID]')",
			'PLATFORM_ID:IN'	=> " platfomr_id IN ('[PLATFORM_ID:IN]')",
			'FEED_ID:IN'		=> " feed_id IN ('[FEED_ID:IN]')",
			'ACTIVE'			=> " active IN ('[ACTIVE]')",
			'FAIL:IN'			=> " fail IN ('[FAIL:IN]')",
			#'FAIL:<'			=> " IIF(fail,fail,0) < [FAIL:<]", #compatible mit sqlite 3.32
			'FAIL:<'			=> " CASE WHEN fail THEN fail ELSE 0 END < [FAIL:<]", #compatible mit sqlite 3.27
			'START_TIME:<='		=> " start_time <= '[START_TIME:<=]'",
			'START_TIME:>'		=> " start_time > '[START_TIME:>]'",
			'UTIMESTAMP:<'		=> " DATETIME(utimestamp, 'localtime') < '[UTIMESTAMP:<]'",
			'START_TIME'		=> " start_time IN ('[START_TIME]')",
		],$D['TASK']['W']);
		$L .= (isset($D['TASK']['L']['START']) && $D['TASK']['L']['STEP'])? " LIMIT {$D['TASK']['L']['START']},{$D['TASK']['L']['STEP']} ":'';
		$O .= (isset($D['TASK']['O']['UTIMESTAMP']))?" utimestamp {$D['TASK']['O']['UTIMESTAMP']}, ":'';
		$qry = $this->SQL->query("SELECT id, platform_id PLATFORM_ID, feed_id FEED_ID, active ACTIVE, fail FAIL, start_time START_TIME, itimestamp ITIMESTAMP, utimestamp UTIMESTAMP
							FROM  wp_task
							WHERE 1 {$W}
							ORDER BY {$O} 1 {$L}");

		while($a = $qry->fetchArray(SQLITE3_ASSOC) ) {
			$D['TASK']['D'][ $a['id'] ] = $a;
		}
	}
	
	function set_task(&$D=null)
	{
		foreach((array)$D['TASK']['D'] AS $kC => $C) {
			if($C['ACTIVE'] != -2) {
				
				$IU_CRN .= (($IU_CRN)?',':'')."('{$kC}','{$C['PLATFORM_ID']}'";
				$IU_CRN .= (isset($C['FEED_ID']))? ",'{$C['FEED_ID']}'":",NULL";
				$IU_CRN .= (isset($C['ACTIVE']))? ",'{$C['ACTIVE']}'":",NULL";
				$IU_CRN .= (isset($C['FAIL']))? ",'{$C['FAIL']}'":",NULL";
				$IU_CRN .= (isset($C['START_TIME']))? ",'{$C['START_TIME']}'":",NULL";
				$IU_CRN .= ")";
			}
			else {
				$D_CRN .= (($D_CRN)?',':'')."'{$kC}'";
			}
		}
		if($IU_CRN) {
			$this->SQL->query("INSERT INTO wp_task (id, platform_id, feed_id, active, fail, start_time) VALUES {$IU_CRN}
							ON CONFLICT(id,platform_id) DO UPDATE SET
								active =		CASE WHEN excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active			THEN excluded.active ELSE active END,
								platform_id =	CASE WHEN excluded.platform_id IS NOT NULL	AND ifnull(platform_id,'') <> excluded.platform_id	THEN excluded.platform_id ELSE platform_id END,
								feed_id =		CASE WHEN excluded.feed_id IS NOT NULL		AND ifnull(feed_id,'') <> excluded.feed_id			THEN excluded.feed_id ELSE feed_id END,
								fail =			CASE WHEN excluded.fail IS NOT NULL			AND ifnull(fail,'') <> excluded.fail				THEN excluded.fail ELSE fail END,
								start_time =	CASE WHEN excluded.start_time IS NOT NULL	AND ifnull(start_time,'') <> excluded.start_time	THEN excluded.start_time ELSE start_time END,

								utimestamp =	CASE WHEN 
															   excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
															OR excluded.platform_id IS NOT NULL	AND ifnull(platform_id,'') <> excluded.platform_id
															OR excluded.feed_id IS NOT NULL		AND ifnull(feed_id,'') <> excluded.feed_id
															OR excluded.fail IS NOT NULL		AND ifnull(fail,'') <> excluded.fail
															OR excluded.start_time IS NOT NULL	AND ifnull(start_time,'') <> excluded.start_time
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
		}
		if($D_CRN) {
			$this->SQL->query("DELETE FROM wp_task WHERE id IN ({$D_CRN})");
		}
	}


	function get_platform(&$D=null)
	{/*
		$W .= CWP::sql_get_where2(array(
			'ID'		=> "id IN ('[V]')",
			'ACTIVE'	=> "active [1] '[V]'",
			),$D['PLATFORM']['W']);*/
		$W = (isset($D['PLATFORM']['W']['ID']))?"AND id IN ('{$D['PLATFORM']['W']['ID']}')":'';
		$W .= (isset($D['PLATFORM']['W']['ACTIVE']))?"active = '{$D['PLATFORM']['W']['ACTIVE']}'":'';
		$L = '';
		$qry = $this->SQL->query("SELECT p.id, parent_id, class_id, p.active, p.itimestamp, p.utimestamp, p.title, sort
							FROM  wp_platform p
							WHERE 1 {$W}
							ORDER BY sort,title {$L}");
							
		
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$Z['PLATFORM']['D'][ $a['id'] ] = [
				'CLASS_ID'		=> $a['class_id'],
				'PARENT_ID'		=> $a['parent_id'],
				'ACTIVE'		=> $a['active'],
				'TITLE'			=> $a['title'],
				'SORT'			=> $a['sort'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			];
			$D['PLATFORM']['PARENT']['D'][ $a['parent_id'] ]['CHILD']['D'][ $a['id'] ] =& $D['PLATFORM']['D'][ $a['id'] ];
		
			$D['PLATFORM']['D'][ $a['id'] ] = array_replace_recursive((array)((isset($D['PLATFORM']['D'][ $a['id'] ]))?$D['PLATFORM']['D'][ $a['id'] ]:null),(array)$Z['PLATFORM']['D'][ $a['id'] ]);
		}
		
		$ID = implode("','",array_keys((array)$D['PLATFORM']['D']));
		/*
		$qry = $this->SQL->query("SELECT platform_id, language_id, active ACTIVE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_platform_language
								WHERE account_id = '{$this->account_id}' AND platform_id IN ('{$ID}')");
	
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['PLATFORM']['D'][ $a['platform_id'] ]['LANGUAGE']['D'][ $a['language_id'] ] = $a;
		}
		*/
		#ToDo: Veraltet? soll aus wp_platform => parent_id info ziehen!
		/*
		$qry = $this->SQL->query("SELECT platform_from_id, platform_to_id, type, active, itimestamp, utimestamp
							FROM  wp_platform_platform
							WHERE  platform_to_id IN ('{$ID}')");
						
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['PLATFORM']['D'][ $a['platform_to_id'] ]['FROM']['PLATFORM']['D'][ $a['platform_from_id'] ] =[
				'ACTIVE'		=> $a['active'],
				'TYPE'			=> $a['type'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			];
		}
		
		#ToDo: Veraltet? soll aus wp_platform => parent_id info ziehen!
		$qry = $this->SQL->query("SELECT platform_from_id, platform_to_id, type, active, itimestamp, utimestamp
							FROM  wp_platform_platform
							WHERE platform_from_id IN ('{$ID}')");
						
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['PLATFORM']['D'][ $a['platform_from_id'] ]['TO']['PLATFORM']['D'][ $a['platform_to_id'] ] = [
				'ACTIVE'		=> $a['active'],
				'TYPE'			=> $a['type'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			];
		}
		*/
	}
	
	function set_platform($D)
	{
		foreach((array)$D['PLATFORM']['D'] AS $kPL => $PL) {
			if($PL['ACTIVE'] != -2) {
				$IU_platform .= (($IU_platform)?',':'')."('{$kPL}'";
				$IU_platform .= (isset($PL['PARENT_ID']))? ",'{$PL['PARENT_ID']}'":",NULL";
				$IU_platform .= (isset($PL['ACTIVE']))? ",'{$PL['ACTIVE']}'":",NULL";
				$IU_platform .= (isset($PL['CLASS_ID']))? ",'{$PL['CLASS_ID']}'":",NULL";
				$IU_platform .= (isset($PL['TITLE']))? ",'{$PL['TITLE']}'":",NULL";
				$IU_platform .= (isset($PL['SORT']))? ",'{$PL['SORT']}'":",NULL";
				$IU_platform .= ")";
			}
			else {
				$D_platform .= (($D_platform)?',':'')."'{$kPL}'";
			}
		}
		
		if($IU_platform) {
			$this->SQL->query("INSERT INTO wp_platform (id,parent_id,active,class_id,title,sort) VALUES {$IU_platform} 
						ON CONFLICT(id) DO UPDATE SET
							active =			CASE WHEN excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active				THEN excluded.active ELSE active END,
							parent_id =			CASE WHEN excluded.parent_id IS NOT NULL			AND ifnull(parent_id,'') <> excluded.parent_id			THEN excluded.parent_id ELSE parent_id END,
							class_id =			CASE WHEN excluded.class_id IS NOT NULL				AND ifnull(class_id,'') <> excluded.class_id			THEN excluded.class_id ELSE class_id END,
							title =				CASE WHEN excluded.title IS NOT NULL				AND ifnull(title,'') <> excluded.title					THEN excluded.title ELSE title END,
							sort =				CASE WHEN excluded.sort IS NOT NULL					AND ifnull(sort,'') <> excluded.sort					THEN excluded.sort ELSE sort END,
							utimestamp =		CASE WHEN 
													   excluded.active IS NOT NULL					AND active <> excluded.active
													OR excluded.parent_id IS NOT NULL				AND parent_id <> excluded.parent_id
													OR excluded.class_id IS NOT NULL				AND class_id <> excluded.class_id
													OR excluded.title IS NOT NULL					AND title <> excluded.title
													OR excluded.sort IS NOT NULL					AND sort <> excluded.sort
												THEN CURRENT_TIMESTAMP ELSE utimestamp END

						");
		}
		
	}
	
	

	
	function get_customer(&$D=null)
	{
		$W .= CWP::where_interpreter(array(
			'ID'		=> " AND id IN ('ID') ",
			'ACTIVE'	=> " AND active IN ('ID') ",
			'EMAIL'		=> " AND email IN ('EMAIL') ",
			),$D['CUSTOMER']['W']);
		
		$D['CUSTOMER']['L']['START'] = ($D['CUSTOMER']['L']['START'])?$D['CUSTOMER']['L']['START']:0;
		$L .= ($D['CUSTOMER']['L']['STEP'])? " LIMIT {$D['CUSTOMER']['L']['START']},{$D['CUSTOMER']['L']['STEP']} ":'';
		$qry = $this->SQL->query("SELECT id, platform_id, active,nickname,email,company,vat_id,name,fname,street,street_no,zip,city,country_id,addition,message_group_id,
									(SELECT COUNT(group_id) FROM wp_message m WHERE m.group_id = c.message_group_id AND m.platform_id = platform_id) count_message,
									(SELECT COUNT(group_id) FROM wp_message m WHERE m.group_id = c.message_group_id AND m.platform_id = platform_id AND m.active = 0) count_message_read,
									(SELECT COUNT(customer_id) FROM wp_invoice i WHERE i.customer_id = c.id AND i.platform_id = platform_id) count_invoice
									FROM  wp_customer c
									WHERE 1 {$W}
									ORDER BY nickname, name {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['CUSTOMER']['D'][ $a['id'] ] = array(
				'PLATFORM_ID'	=> $a['platform_id'],
				'MESSAGE_GROUP_ID'	=> $a['message_group_id'],
				'ACTIVE'		=> $a['active'],
				'NICKNAME'		=> $a['nickname'],
				'EMAIL'			=> $a['email'],
				'COMPANY'		=> $a['company'],
				'VAT_ID'		=> $a['vat_id'],
				'NAME'			=> $a['name'],
				'FNAME'			=> $a['fname'],
				'STREET'		=> $a['street'],
				'STREET_NO'		=> $a['street_no'],
				'ZIP'			=> $a['zip'],
				'CITY'			=> $a['city'],
				'COUNTRY_ID'	=> $a['country_id'],
				'ADDITION'		=> $a['addition'],
				'COUNT_MESSAGE'	=> $a['count_message'], #ToDo: Veraltet
				'COUNT_INVOICE'	=> $a['count_invoice'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				);
			
			$D['CUSTOMER']['D'][ $a['id'] ]['MESSAGE'] = array(
				'READ'			=> $a['count_message_read'],
				'UNREAD'		=> $a['count_message']-$a['count_message_read'],
				'COUNT'			=> $a['count_message'],
				);
		}
		
		
		//COUNT
		$qry = $this->SQL->query("SELECT count(*) AS C FROM wp_customer");
		$a = $qry->fetchArray(SQLITE3_ASSOC);
		$D['CUSTOMER']['COUNT'] = $a['C'];
		#return $D;
	}
	
	function set_customer($D)
	{
		$k = array_keys((array)$D['CUSTOMER']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$customer = $D['CUSTOMER']['D'][$k[$i]];
			if($customer['ACTIVE'] != -2)
			{
				$IU_customer .= (($IU_customer)?',':'')."('{$k[$i]}','{$this->account_id}'";
				$IU_customer .= (isset($customer['ACTIVE']))? ",'{$customer['ACTIVE']}'":",NULL";
				$IU_customer .= (isset($customer['MESSAGE_GROUP_ID']))? ",'{$customer['MESSAGE_GROUP_ID']}'":",NULL";
				$IU_customer .= (isset($customer['NICKNAME']))? ",'{$customer['NICKNAME']}'":",NULL";
				$IU_customer .= (isset($customer['EMAIL']))? ",'{$customer['EMAIL']}'":",NULL";
				$IU_customer .= (isset($customer['COMPANY']))? ",'{$customer['COMPANY']}'":",NULL";
				$IU_customer .= (isset($customer['VAT_ID']))? ",'{$customer['VAT_ID']}'":",NULL";
				$IU_customer .= (isset($customer['NAME']))? ",'{$customer['NAME']}'":",NULL";
				$IU_customer .= (isset($customer['FNAME']))? ",'{$customer['FNAME']}'":",NULL";
				$IU_customer .= (isset($customer['STREET']))? ",'{$customer['STREET']}'":",NULL";
				$IU_customer .= (isset($customer['STREET_NO']))? ",'{$customer['STREET_NO']}'":",NULL";
				$IU_customer .= (isset($customer['ZIP']))? ",'{$customer['ZIP']}'":",NULL";
				$IU_customer .= (isset($customer['CITY']))? ",'{$customer['CITY']}'":",NULL";
				$IU_customer .= (isset($customer['COUNTRY_ID']))? ",'{$customer['COUNTRY_ID']}'":",NULL";
				$IU_customer .= (isset($customer['ADDITION']))? ",'{$customer['ADDITION']}'":",NULL";
				$IU_customer .= ")";
			}
			else
			{
				$D_customer .= (($D_customer)?',':'')."'{$k[$i]}'";
			}
			if($IU_customer)
			$this->SQL->query("INSERT INTO wp_customer (id,active,message_group_id,nickname,email,company,vat_id,name,fname,street,street_no,zip,city,country_id,addition) VALUES {$IU_customer} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_customer.active END,
							message_group_id = CASE WHEN VALUES(message_group_id) IS NOT NULL THEN VALUES(message_group_id) ELSE wp_customer.message_group_id END,
							nickname = CASE WHEN VALUES(nickname) IS NOT NULL THEN VALUES(nickname) ELSE wp_customer.nickname END,
							email = CASE WHEN VALUES(email) IS NOT NULL THEN VALUES(email) ELSE wp_customer.email END,
							company = CASE WHEN VALUES(company) IS NOT NULL THEN VALUES(company) ELSE wp_customer.company END,
							vat_id = CASE WHEN VALUES(vat_id) IS NOT NULL THEN VALUES(vat_id) ELSE wp_customer.vat_id END,
							name = CASE WHEN VALUES(name) IS NOT NULL THEN VALUES(name) ELSE wp_customer.name END,
							fname = CASE WHEN VALUES(fname) IS NOT NULL THEN VALUES(fname) ELSE wp_customer.fname END,
							street = CASE WHEN VALUES(street) IS NOT NULL THEN VALUES(street) ELSE wp_customer.street END,
							street_no = CASE WHEN VALUES(street_no) IS NOT NULL THEN VALUES(street_no) ELSE wp_customer.street_no END,
							zip = CASE WHEN VALUES(zip) IS NOT NULL THEN VALUES(zip) ELSE wp_customer.zip END,
							city = CASE WHEN VALUES(city) IS NOT NULL THEN VALUES(city) ELSE wp_customer.city END,
							country_id = CASE WHEN VALUES(country_id) IS NOT NULL THEN VALUES(country_id) ELSE wp_customer.country_id END,
							addition = CASE WHEN VALUES(addition) IS NOT NULL THEN VALUES(addition) ELSE wp_customer.addition END
						");
		}
	}
	
	
	function set_message($D)
	{
		$k = array_keys((array)$D['MESSAGE']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$MESSAGE = $D['MESSAGE']['D'][$k[$i]];
			if($ORDER['ACTIVE'] != -2)
			{
				$IU_MESSAGE .= (($IU_MESSAGE)?',':'')."('{$k[$i]}','{$MESSAGE['GROUP_ID']}','{$MESSAGE['PLATFORM_ID']}'";
				$IU_MESSAGE .= (isset($MESSAGE['USER_ID']))? ",'{$MESSAGE['USER_ID']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['ACTIVE']))? ",'{$MESSAGE['ACTIVE']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['INTERNALLY']))? ",'{$MESSAGE['INTERNALLY']}'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['TITLE']))? ",'".$this->SQL->escapeString($MESSAGE['TITLE'])."'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['TEXT']))? ",'".$this->SQL->escapeString($MESSAGE['TEXT'])."'":",NULL";
				$IU_MESSAGE .= (isset($MESSAGE['DATETIME']))? ",'{$MESSAGE['DATETIME']}'":",NULL";
				$IU_MESSAGE .= ")";
			}
			else
			{
				$D_MESSAGE .= (($D_MESSAGE)?',':'')."'{$k[$i]}'";
			}
		}
		
		$this->SQL->query("INSERT INTO wp_message (id,group_id, platform_id, user_id, active, internally, title, text, datetime) VALUES {$IU_MESSAGE} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_message.active END,
							internally = CASE WHEN VALUES(internally) IS NOT NULL THEN VALUES(internally) ELSE wp_message.internally END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_message.title END,
							text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE wp_message.text END,
							datetime = CASE WHEN VALUES(datetime) IS NOT NULL THEN VALUES(datetime) ELSE wp_message.datetime END
						");

		if($D_MESSAGE)
		$this->SQL->query("DELETE FROM wp_message WHERE id IN ({$D_MESSAGE}) ");
		
	}
	
	function get_message(&$D=null)
	{
		$W .= ($D['MESSAGE']['W']['INTERNALLY'])?" AND internally IN ('{$D['MESSAGE']['W']['INTERNALLY']}')":'';
		$W .= (isset($D['MESSAGE']['W']['GROUP_ID']))?" AND group_id IN ('{$D['MESSAGE']['W']['GROUP_ID']}')":'';
		$W .= (isset($D['MESSAGE']['W']['ITIMESTAMP:<']))?" AND '{$D['MESSAGE']['W']['ITIMESTAMP:<']}' < itimestamp ":'';
		#$W .= ($D['MESSAGE']['W']['CUSTOMER_ID'])?" AND customer_id IN ('{$D['MESSAGE']['W']['CUSTOMER_ID']}')":'';
		#$W .= ($D['MESSAGE']['W']['PASSWORD'])?" AND pass IN ('{$D['USER']['W']['PASSWORD']}')":'';
		#$W .= ($D['MESSAGE']['W']['ID'])?" AND id IN ('{$D['USER']['W']['ID']}')":'';
		#$L .= ($D['MESSAGE']['L']['START'] && $D['USER']['L']['STEPP'])? " LIMIT {$D['USER']['L']['START']},{$D['USER']['L']['STEPP']} ":'';
		
		$O .= ($D['MESSAGE']['O']['DATETIME'])?" datetime {$D['MESSAGE']['O']['DATETIME']}, ":'';
		
		$qry = $this->SQL->query("SELECT id,group_id,user_id, platform_id, active, internally, title, text, datetime, itimestamp, utimestamp
							FROM wp_message
							WHERE 1 {$W}
							ORDER BY {$O} datetime {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['MESSAGE']['D'][ $a['id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'GROUP_ID'					=> $a['group_id'],
				'USER_ID'					=> $a['user_id'],
				#'CUSTOMER_ID'				=> $a['customer_id'],
				'PLATFORM_ID'				=> $a['platform_id'],
				'INTERNALLY'				=> $a['internally'],
				'TITLE'						=> $a['title'],
				'TEXT'						=> $a['text'],
				'DATETIME'					=> $a['datetime'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		#return $D;
	}
	
	

	
	#veraltet??
	function get_invoice(&$D=null)
	{
		$W .= ($D['INVOICE']['W']['ID'])? " AND id IN ('{$D['INVOICE']['W']['ID']}')":'';
		$W .= ($D['INVOICE']['W']['CUSTOMER_ID'])? " AND customer_id IN ('{$D['INVOICE']['W']['CUSTOMER_ID']}')":'';
		$W .= ($D['INVOICE']['W']['WAREHOUSE_ID'])? " AND warehouse_id IN ('{$D['INVOICE']['W']['WAREHOUSE_ID']}')":'';
		$W .= ($D['INVOICE']['W']['STATUS'])? " AND status IN ('{$D['INVOICE']['W']['STATUS']}')":'';
		$W .= ($D['INVOICE']['W']['DATE'])? " AND date LIKE '{$D['INVOICE']['W']['DATE']}'":'';
		
		$O .= ($D['INVOICE']['O']['NUMBER'])?" number {$D['INVOICE']['O']['NUMBER']},":" number DESC,";
		
		$qry = $this->SQL->query("SELECT id,from_platform_id,payment_id,active,status,number, date, customer_id, customer_nickname, customer_email
								,billing_company,billing_name,billing_fname,billing_street,billing_street_no,billing_zip,billing_city,billing_country_id,billing_addition
								,delivery_company,delivery_name,delivery_fname,delivery_street,delivery_street_no,delivery_zip,delivery_city,delivery_country_id,delivery_addition
								,traking_no,shipped_date,shipping_id,warehouse_id,comment,
								DATE_FORMAT(itimestamp,'%Y%m%d%h%i%s') itimestamp
								,utimestamp
							FROM wp_invoice
							WHERE 1 {$W} ORDER BY {$O} 1 ");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['INVOICE']['D'][ $a['id'] ] = array(
				#'PLATFORM_ID'			=> $a['from_platform_id'], #From Platform ID
				'PAYMENT_ID'			=> $a['payment_id'],
				'ACTIVE'				=> $a['active'],
				'STATUS'				=> $a['status'],
				'NUMBER'				=> $a['number'],
				'DATE'					=> $a['date'],
				'CUSTOMER_ID'			=> $a['customer_id'],
				'CUSTOMER_NICKNAME'		=> $a['customer_nickname'],
				'CUSTOMER_EMAIL'		=> $a['customer_email'],
				'BILLING'				=> array(
						'COMPANY'		=> $a['billing_company'],
						'NAME'			=> $a['billing_name'],
						'FNAME'			=> $a['billing_fname'],
						'STREET'		=> $a['billing_street'],
						'STREET_NO'		=> $a['billing_street_no'],
						'ZIP'			=> $a['billing_zip'],
						'CITY'			=> $a['billing_city'],
						'COUNTRY_ID'	=> $a['billing_country_id'],
						'ADDITION'		=> $a['billing_addition'],
						),
					'DELIVERY'				=> array(
						'COMPANY'		=> $a['delivery_company'],	
						'NAME'			=> $a['delivery_name'],
						'FNAME'			=> $a['delivery_fname'],
						'STREET'		=> $a['delivery_street'],
						'STREET_NO'		=> $a['delivery_street_no'],
						'ZIP'			=> $a['delivery_zip'],
						'CITY'			=> $a['delivery_city'],
						'COUNTRY_ID'	=> $a['delivery_country_id'],
						'ADDITION'		=> $a['delivery_addition'],
						),
					'TRACKING_NO'			=> $a['traking_no'],
					'SHIPPED_DATE'			=> $a['shipped_date'],
					'SHIPPING_ID'			=> $a['shipping_id'],
					'WAREHOUSE_ID'			=> $a['warehouse_id'],
					'COMMENT'				=> $a['comment'],
					'ITIMESTAMP'			=> $a['itimestamp'],
					'UTIMESTAMP'			=> $a['utimestamp'],
					);
			
			$D['INVOICE']['DATE']['D'][ $a['date'] ]['COUNT'] ++;
			
		}
		
		$ID = implode("','",array_keys((array)$D['INVOICE']['D']));
		$qry = $this->SQL->query("SELECT id,invoice_id,active,number,title,stock,weight,price,vat,
			DATE_FORMAT(itimestamp,'%Y%m%d%h%i%s') itimestamp
			,utimestamp
							FROM wp_invoice_article oa 
							WHERE invoice_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['D'][ $a['id'] ] = array(
				'ACTIVE'				=> $a['active'],
				'NUMBER'				=> $a['number'],
				'TITLE'					=> $a['title'],
				'STOCK'					=> $a['stock'],
				'WEIGHT'				=> $a['weight'],
				'PRICE'					=> $a['price'],
				'VAT'					=> $a['vat'],
				'ITIMESTAMP'			=> $a['itimestamp'],
				'UTIMESTAMP'			=> $a['utimestamp'],
				);
			$D['INVOICE']['D'][ $a['invoice_id'] ]['PRICE'] += ($a['price'])*$a['stock'];#ToDo: Veraltet
			$D['INVOICE']['D'][ $a['invoice_id'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];#ToDo: Veraltet
			$D['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['WEIGHT'] += ($a['weight'])*$a['stock'];
			$D['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['INVOICE']['D'][ $a['invoice_id'] ]['ARTICLE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			
			
			$D['INVOICE']['PRICE'] += ($a['price'])*$a['stock'];
			$D['INVOICE']['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
			$D['INVOICE']['DATE']['D'][ $D['INVOICE']['D'][ $a['invoice_id'] ]['DATE'] ]['PRICE'] += ($a['price'])*$a['stock'];
			$D['INVOICE']['DATE']['D'][ $D['INVOICE']['D'][ $a['invoice_id'] ]['DATE'] ]['VAT'] += ($a['price']/100*$a['vat'])*$a['stock'];
		}
		#return $D;
	}
	
	function set_warehouse(&$D=null)
	{
		$k = array_keys((array)$D['WAREHOUSE']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$warehouse = $D['WAREHOUSE']['D'][$k[$i]];
			if($warehouse['ACTIVE'] != -2)
			{
				$IU_warehouse .= (($IU_warehouse)?',':'')."('{$k[$i]}''";
				$IU_warehouse .= (isset($warehouse['ACTIVE']))? ",'{$warehouse['ACTIVE']}'":",NULL";
				$IU_warehouse .= (isset($warehouse['TITLE']))? ",'{$warehouse['TITLE']}'":",NULL";
				$IU_warehouse .= ")";
				
				#$sK = array_keys((array)$warehouse['STORAGE']['D']);
				#for($s=0;$s < count($sK);$s++)
				foreach((array)$warehouse['STORAGE']['D'] AS $kSTO => $STO)
				{
					#$storage = $warehouse['STORAGE']['D'][$sK[$s]];
					if($storage['ACTIVE'] != -2)
					{
						$IU_storage .= (($IU_storage)?',':'')."('{$kSTO}','{$k[$i]}'";
						$IU_storage .= (isset($STO['ACTIVE']))? ",'{$STO['ACTIVE']}'":",NULL";
						$IU_storage .= (isset($STO['TITLE']))? ",'{$STO['TITLE']}'":",NULL";
						$IU_storage .= (isset($STO['SORT']))? ",'{$STO['SORT']}'":",NULL";
						$IU_storage .= (isset($STO['REFID']))? ",'{$STO['REFID']}'":",NULL";
						
						$IU_storage .= ")";
						
						#$aK = array_keys((array)$storage['ARTICLE']['D']);
						#for($a=0;$a < count($aK);$a++)
						foreach((array)$STO['ARTICLE']['D'] AS $kART => $ART)
						{
							#$article = $storage['ARTICLE']['D'][$kART];
							if($ART['ACTIVE'] != -2)
							{
								$IU_article .= (($IU_article)?',':'')."('{$kART}','{$kSTO}','{$k[$i]}'";
								$IU_article .= (isset($ART['ACTIVE']))? ",'{$ART['ACTIVE']}'":",NULL";
								$IU_article .= (isset($ART['STOCK']))? ",'{$ART['STOCK']}'":",NULL";
								$IU_article .= ")";
							}
							else
								$D_article .= (($D_article)?',':'')."'{$k[$i]}{$kSTO}{$kART}'";
						}
					}
					else
						$D_storage .= (($D_storage)?',':'')."'{$k[$i]}{$kSTO}'";
				}
			}
			else
				$D_warehouse .= (($D_warehouse)?',':'')."'{$k[$i]}'";
		}	
		
		if($IU_warehouse) {
			$this->SQL->query("INSERT INTO wp_warehouse (id,active,title) VALUES {$IU_warehouse} 
						ON CONFLICT(id, platform_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active	THEN excluded.active ELSE active END,
							title =			CASE WHEN excluded.title IS NOT NULL			AND title <> excluded.title		THEN excluded.title ELSE title END,
							utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL			AND active <> excluded.active
													OR excluded.title IS NOT NULL		AND title <> excluded.title
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($IU_storage) {
			$this->SQL->query("INSERT INTO wp_warehouse_storage (id,warehouse_id,active,title,sort,refid) VALUES {$IU_storage} 
						ON CONFLICT(id, warehouse_id) DO UPDATE SET
							active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active	THEN excluded.active ELSE active END,
							title =			CASE WHEN excluded.title IS NOT NULL			AND title <> excluded.title		THEN excluded.title ELSE title END,
							sort =			CASE WHEN excluded.sort IS NOT NULL			AND title <> excluded.sort		THEN excluded.title ELSE sort END,
							refid =			CASE WHEN excluded.refid IS NOT NULL			AND refid <> excluded.refid		THEN excluded.title ELSE refid END,
							utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL			AND active <> excluded.active
													OR excluded.title IS NOT NULL		AND title <> excluded.title
													OR excluded.sort IS NOT NULL		AND sort <> excluded.sort
													OR excluded.refid IS NOT NULL		AND refid <> excluded.refid
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
						");
		}
		if($IU_article) {
			$this->SQL->query("INSERT INTO wp_article_stock (article_id,storage_id,warehouse_id,active,stock) VALUES {$IU_article} 
					ON CONFLICT(article_id, warehouse_id, storage_id) DO UPDATE SET
						active =		CASE WHEN excluded.active IS NOT NULL		AND active <> excluded.active	THEN excluded.active ELSE active END,
						stock =			CASE WHEN excluded.stock IS NOT NULL			AND stock <> excluded.stock		THEN excluded.stock ELSE stock END,
						utimestamp =	CASE WHEN 
													excluded.active IS NOT NULL			AND active <> excluded.active
													OR excluded.stock IS NOT NULL		AND stock <> excluded.stock
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
					");#ToDo: Hotfix: itimestamp wird auf aktuelles Datum gesetzt sobald ein Bestand von 0 auf n erhöht wird.
		}
		if($IU_warehouse) {
			$this->SQL->query("DELETE FROM wp_warehouse WHERE id IN ({$IU_warehouse})");
			$this->SQL->query("DELETE FROM wp_warehouse_storage WHERE (warehouse_id) IN ({$IU_warehouse})");
			$this->SQL->query("DELETE FROM wp_article_stock WHERE (warehouse_id) NOT IN ({$IU_warehouse})");
		}
		if($IU_storage) {
			$this->SQL->query("DELETE FROM wp_warehouse_storage WHERE (warehouse_id || id) IN ({$IU_storage})");
			$this->SQL->query("DELETE FROM wp_article_stock WHERE (warehouse_id || storage_id) NOT IN ({$IU_storage})");
		}
		if($D_article) {
			$this->SQL->query("DELETE FROM wp_article_stock WHERE (warehouse_id || storage_id || article_id) IN ({$D_article}) ");
		}
		#Update utime beim artikel
		$this->SQL->query("UPDATE wp_article SET utimestamp = (SELECT utimestamp FROM wp_article_stock WHERE wp_article.id = wp_article_stock.article_id AND wp_article.utimestamp < wp_article_stock.utimestamp)");
	}
	
	
	function get_supplier(&$D=null)
	{
		$W .= ($D['SUPPLIER']['W']['ID'])? " AND s.id IN ('{$D['SUPPLIER']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_supplier  
							WHERE 1 {$W}");
							
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SUPPLIER']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				];
		}
		
		#ARTICLE START =======================
		$wID = implode("','",array_keys((array)$D['SUPPLIER']['D']));
		$qry = $this->SQL->query("SELECT supplier_id, article_id, reference_id, active, price, stock, itimestamp, utimestamp 
								FROM wp_supplier_to_article
								WHERE supplier_id IN ('{$wID}') ");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SUPPLIER']['D'][ $a['supplier_id'] ]['ARTICLE']['D'][ $a['article_id'] ] = array(
				'REFERENCE_ID'				=> $a['reference_id'],
				'ACTIVE'					=> $a['active'],
				'PRICE'						=> $a['price'],
				'STOCK'						=> $a['stock'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		#ARTICLE ENDE ========================
		#return $D;
	}
	
	function set_supplier($D=null)
	{
		$kSUP = array_keys((array)$D['SUPPLIER']['D']);
		for($a=0; $a< count($kSUP); $a++)
		{
			$SUPPLIER = $D['SUPPLIER']['D'][ $kSUP[$a] ];
			if($SUPPLIER['ACTIVE'] != -2)
			{
				$IU_SUP .= (($IU_SUP)?',':'')."('{$kSUP[$a]}'";
				$IU_SUP .= (isset($SUPPLIER['ACTIVE']))? ",'{$SUPPLIER['ACTIVE']}'":",NULL";
				$IU_SUP .= (isset($SUPPLIER['TITLE']))? ",'{$SUPPLIER['TITLE']}'":",NULL";
				$IU_SUP .= ")";
				
				#ARTIKEL START =================
				$kART = array_keys((array)$SUPPLIER['ARTICLE']['D']);
				for($v=0; $v< count($kART); $v++)
				{
					$ARTICLE = $SUPPLIER['ARTICLE']['D'][$kART[$v]];
					if($ARTIKEL['ACTIVE'] != -2)
					{
						$IU_ART .= (($IU_ART)?',':'')."('{$kSUP[$a]}','{$kART[$v]}'";
						$IU_ART .= (isset($ARTICLE['REFERENCE_ID']))? ",'{$ARTICLE['REFERENCE_ID']}'":",NULL";
						$IU_ART .= (isset($ARTICLE['ACTIVE']))? ",'{$ARTICLE['ACTIVE']}'":",NULL";
						$IU_ART .= (isset($ARTICLE['PRICE']))? ",'{$ARTICLE['PRICE']}'":",NULL";
						$IU_ART .= (isset($ARTICLE['STOCK']))? ",'{$ARTICLE['STOCK']}'":",NULL";
						$IU_ART .= ")";
					}
				}
				#ARTIKEL END ===================
			}
			else
			{
				$D_SUP .= (($D_SUP)?',':'')."'{$kSUP[$a]}'";
			}
		}
		
		if($IU_SUP)
			$this->SQL->query("INSERT INTO wp_supplier (id, active, title) VALUES {$IU_SUP} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_supplier.active END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_supplier.title END
						");
		if($IU_ART)
			$this->SQL->query("INSERT INTO wp_supplier_to_article (supplier_id,article_id,reference_id,active,price,stock) VALUES {$IU_ART} 
						ON DUPLICATE KEY UPDATE 
							reference_id = CASE WHEN VALUES(reference_id) IS NOT NULL THEN VALUES(reference_id) ELSE wp_supplier_to_article.reference_id END,
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_supplier_to_article.active END,
							price = CASE WHEN VALUES(price) IS NOT NULL THEN VALUES(price) ELSE wp_supplier_to_article.price END,
							stock = CASE WHEN VALUES(stock) IS NOT NULL THEN VALUES(stock) ELSE wp_supplier_to_article.stock END
						");
		#if($D_SUP)
		#$this->SQL->query("DELETE FROM wp_attribute WHERE id IN ({$D_SET}) AND platform_id IS NULL");
		return $D;
	}
	
	function get_shipping(&$D=null)
	{
		/*
		$W .= ($D['SHIPPING']['W']['ID'])? " AND s.id IN ('{$D['SHIPPING']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_shipping s 
							WHERE 1 {$W}"); #WHERE account_id = '{$this->account_id}'
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SHIPPING']['D'][ $a['id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		*/
		$qry = $this->SQL->query("SELECT id, shipping_id, active, title, sort, itimestamp, utimestamp 
							FROM wp_shipping_class 
							WHERE 1 {$W}
							ORDER BY sort,title");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SHIPPING']['D'][ $a['shipping_id'] ]['CLASS']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'SORT'						=> $a['sort'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
		}
		
		#Länder=======
		$qry = $this->SQL->query("SELECT id, shipping_class_id, shipping_id, active, price, itimestamp, utimestamp 
									FROM wp_shipping_class_to_cost 
									LEFT JOIN (SELECT id AS id2, shipping_id FROM wp_shipping_class WHERE 1 {$W}) scl ON scl.id2 = shipping_class_id
									");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SHIPPING']['D'][ $a['shipping_id'] ]['CLASS']['D'][ $a['shipping_class_id'] ]['COST']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'PRICE'						=> $a['price'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
		}
		$qry = $this->SQL->query("SELECT country_id, shipping_class_id, shipping_cost_id, shipping_id, active, itimestamp, utimestamp 
									FROM wp_shipping_class_to_country
									LEFT JOIN (SELECT id, shipping_id FROM wp_shipping_class WHERE 1 {$W}) scl ON scl.id = shipping_class_id
									");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SHIPPING']['D'][ $a['shipping_id'] ]['CLASS']['D'][ $a['shipping_class_id'] ]['COST']['D'][ $a['shipping_cost_id'] ]['COUNTRY']['D'][ $a['country_id'] ] = [
				'ACTIVE'					=> $a['active'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
		}

		#=================
		
		#Regeln
		$qry = $this->SQL->query("SELECT sc.id, shipping_class_id, shipping_id, group_id, active, `column`, operator, value, itimestamp, utimestamp 
									FROM wp_shipping_conditions sc
									LEFT JOIN (SELECT id, shipping_id FROM wp_shipping_class WHERE 1 {$W}) scl ON scl.id = shipping_class_id
									");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SHIPPING']['D'][ $a['shipping_id'] ]['CLASS']['D'][ $a['shipping_class_id'] ]['GROUP']['D'][ $a['group_id'] ]['CONDITIONS']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'COLUMN'					=> $a['column'],
				'OPERATOR'					=> $a['operator'],
				'VALUE'						=> $a['value'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				];
		}
	}
	
	function set_shipping(&$D=NULL)
	{
		foreach((array)$D['SHIPPING']['D'] AS $kS => $vS)
		{
			foreach((array)$vS['CLASS']['D'] AS $kC => $vC)
			{
				if($vC['ACTIVE'] != -2)
				{
					$IU_CLS .= (($IU_CLS)?',':'')."('{$kC}','{$kS}'";
					$IU_CLS .= (isset($vC['ACTIVE']))? ",'{$vC['ACTIVE']}'":",NULL";
					$IU_CLS .= (isset($vC['TITLE']))? ",'".$this->SQL->escapeString($vC['TITLE'])."'":",NULL";
					$IU_CLS .= (isset($vC['SORT']))? ",'{$vC['SORT']}'":",NULL";
					$IU_CLS .= ")";
					
					foreach((array)$vC['COST']['D'] AS $kCOST => $vCOST)
					{
						if($vCOST['ACTIVE'] != -2)
						{
							$IU_COST .= (($IU_COST)?',':'')."('{$kCOST}','{$kC}'";
							$IU_COST .= (isset($vCOST['ACTIVE']))? ",'{$vCOST['ACTIVE']}'":",NULL";
							$IU_COST .= (isset($vCOST['PRICE']))? ",'{$vCOST['PRICE']}'":",NULL";
							$IU_COST .= ")";
						
							foreach((array)$vCOST['COUNTRY']['D'] AS $kCY => $vCY)
							{
								if($vCY['ACTIVE'] != -2)
								{
									$IU_COU .= (($IU_COU)?',':'')."('{$kCY}','{$kC}','{$kCOST}'";
									$IU_COU .= (isset($vCY['ACTIVE']))? ",'{$vCY['ACTIVE']}'":",NULL";
									$IU_COU .= ")";
								}
								else
								{
									$D_COU .= (($D_COU)?',':'')."'{$kCY}{$kC}{$kCOST}'";
								}
							}
						}
						else
						{
							$D_COST .= (($D_COST)?',':'')."'{$kCOST}'";
						}
					}
					foreach((array)$vC['GROUP']['D'] AS $kG => $vG)
						foreach((array)$vG['CONDITIONS']['D'] AS $kCS => $vCS)
						{
							if($vCS['ACTIVE'] != -2)
							{
								$IU_CON .= (($IU_CON)?',':'')."('{$kCS}','{$kC}','{$kG}'";
								$IU_CON .= (isset($vCS['ACTIVE']))? ",'{$vCS['ACTIVE']}'":",NULL";
								$IU_CON .= (isset($vCS['COLUMN']))? ",'".$this->SQL->escapeString($vCS['COLUMN'])."'":",NULL";
								$IU_CON .= (isset($vCS['OPERATOR']))? ",'".$this->SQL->escapeString($vCS['OPERATOR'])."'":",NULL";
								$IU_CON .= (isset($vCS['VALUE']))? ",'".$this->SQL->escapeString($vCS['VALUE'])."'":",NULL";
								$IU_CON .= ")";
							}
							else
							{
								$D_CON .= (($D_CON)?',':'')."'{$kCS}{$kC}'";
							}
						}
				}
				else
				{
					$D_CLS .= (($D_CLS)?',':'')."'{$kC}{$kS}'";
				}
			}
		}
		
		if($IU_CLS)
			$this->SQL->query("INSERT INTO wp_shipping_class (id, shipping_id, active, title, sort) VALUES {$IU_CLS} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_shipping_class.active END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_shipping_class.title END,
							sort = CASE WHEN VALUES(sort) IS NOT NULL THEN VALUES(sort) ELSE wp_shipping_class.sort END
						");
		if($IU_COU)
			$this->SQL->query("INSERT INTO wp_shipping_class_to_country (country_id, shipping_class_id, shipping_cost_id, active) VALUES {$IU_COU} 
						ON DUPLICATE KEY UPDATE
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_shipping_class_to_country.active END
						");
		if($IU_COST)
			$this->SQL->query("INSERT INTO wp_shipping_class_to_cost (id, shipping_class_id, active, price) VALUES {$IU_COST} 
						ON DUPLICATE KEY UPDATE
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_shipping_class_to_cost.active END,
							price = CASE WHEN VALUES(price) IS NOT NULL THEN VALUES(price) ELSE wp_shipping_class_to_cost.price END
						");
		
		if($IU_CON)
			$this->SQL->query("INSERT INTO wp_shipping_conditions (id, shipping_class_id, group_id, active, `column`, operator, value) VALUES {$IU_CON} 
						ON DUPLICATE KEY UPDATE 
							group_id = CASE WHEN VALUES(group_id) IS NOT NULL THEN VALUES(group_id) ELSE wp_shipping_conditions.group_id END,
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_shipping_conditions.active END,
							`column` = CASE WHEN VALUES(`column`) IS NOT NULL THEN VALUES(`column`) ELSE wp_shipping_conditions.`column` END,
							operator = CASE WHEN VALUES(operator) IS NOT NULL THEN VALUES(operator) ELSE wp_shipping_conditions.operator END,
							value = CASE WHEN VALUES(value) IS NOT NULL THEN VALUES(value) ELSE wp_shipping_conditions.value END
						");
			
		if($D_CLS)
		{
			$this->SQL->query("DELETE FROM wp_shipping_class WHERE (id || shipping_id) IN ({$D_CLS})");
			$this->SQL->query("DELETE FROM wp_shipping_class_to_country WHERE shipping_class_id NOT IN (SELECT id FROM wp_shipping_class )");
			$this->SQL->query("DELETE FROM wp_shipping_conditions WHERE shipping_class_id NOT IN (SELECT id FROM wp_shipping_class )");
			$this->SQL->query("DELETE FROM wp_shipping_class_to_cost WHERE shipping_class_id NOT IN (SELECT id FROM wp_shipping_class )");
		}
		if($D_COU)
			$this->SQL->query("DELETE FROM wp_shipping_class_to_country WHERE (country_id || shipping_class_id || shipping_cost_id) IN ({$D_COU})");
		if($D_CON)
			$this->SQL->query("DELETE FROM wp_shipping_conditions WHERE (id || shipping_class_id) IN ({$D_CON})");
	}

	
	
	function get_warehouse(&$D=null)
	{/*
		$W .= ($D['WAREHOUSE']['W']['ID'])? " AND w.id IN ('{$D['WAREHOUSE']['W']['ID']}')":'';
		$W .= ($D['WAREHOUSE']['W']['ACTIVE'])? " AND w.active IN ('{$D['WAREHOUSE']['W']['ACTIVE']}')":'';
		
		
		
		$qry = $this->SQL->query("SELECT id ID, active ACTIVE, title TITLE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_warehouse w 
							WHERE 1 {$W}
							ORDER BY title");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['WAREHOUSE']['D'][ $a['ID'] ] = $a;
		}
		
		#STORE START =======================
		$wID = implode("','",array_keys((array)$D['WAREHOUSE']['D']));
		$qry = $this->SQL->query("SELECT id ID, warehouse_id WAREHOUSE_ID, refid REFID, active ACTIVE, title TITLE, pos POS, sort SORT, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
								FROM wp_warehouse_storage
								WHERE warehouse_id IN ('{$wID}')
								ORDER BY sort, title, pos");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['WAREHOUSE']['D'][ $a['WAREHOUSE_ID'] ]['STORAGE']['D'][ $a['ID'] ] = $a;
			$ID .= (($ID)?',':'')."'{$a['ID']}{$a['WAREHOUSE_ID']}'";
		}
		#STORE ENDE ========================
		
		#ARTICLE STOCK START ===============
		if($ID) {
		$qry = $this->SQL->query("SELECT article_id ARTICLE_ID, warehouse_id WAREHOUSE_ID, storage_id STORAGE_ID, active ACTIVE, stock STOCK, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP 
								FROM wp_article_stock
								WHERE (storage_id || warehouse_id) IN ({$ID})
								ORDER BY itimestamp");
		}
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['WAREHOUSE']['D'][ $a['WAREHOUSE_ID'] ]['STORAGE']['D'][ $a['STORAGE_ID'] ]['ARTICLE']['D'][ $a['ARTICLE_ID'] ] = $a;
			$D['WAREHOUSE']['D'][ $a['WAREHOUSE_ID'] ]['ARTICLE']['D'][ $a['ARTICLE_ID'] ]['STOCK'] += $a['stock'];
		}
		#ARTICLE STOCK ENDE ================
		
		$D['WAREHOUSE']['D']['AMAZON_FBA'] = ['ACTIVE' => 1, 'TITLE' => 'Amazon FBA']; #ToDo: Solche Lager sollen einzelne Platformen ggf. mit übergeben
		#return $D;
		*/
	}
	
	#veraltet??
	function get_article(&$D=null)
	{
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['ID'])? " 
		AND (
				a.id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['ID']}')
				OR a.id IN (SELECT parent_id FROM wp_article WHERE id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['ID']}') )
			)
		":'';
		
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['NUMBER'])? "AND a.number LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['NUMBER']}'" : '';
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['TITLE'])? "AND a.id IN (SELECT article_id FROM wp_article_description ad WHERE title LIKE '{$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['TITLE']}' AND ad.platform_id = a.platform_id)" : '';
		
		$W .= ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['CATEGORIE_ID'])? " AND a.id IN (SELECT article_id FROM wp_categorie_article ca WHERE ca.categorie_id IN ('{$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['W']['CATEGORIE_ID']}') AND ca.platform_id = a.platform_id )":'';
		$L .= ($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['L']['START'] && $D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['L']['STEPP'])? " LIMIT {$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['L']['START']},{$D['ARTICLE']['L']['STEPP']} ":'';
		
		#Description start --------------------
		$qry = $this->SQL->query("SELECT article_id, language_id, title, short_text, long_text, utimestamp, itimestamp
							FROM wp_article_description ad
							WHERE platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$LANG[ $a['article_id'] ]['D'][ $a['language_id'] ]['DESCRIPTION'] = array(
				'TITLE'			=> $a['title'],
				'SHORT_TEXT'	=> $a['short_text'],
				'LONG_TEXT'		=> $a['long_text'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
		}	
		#Description ende --------------------
		
		#ATTRIBUTE START =========================
		$qry = $this->SQL->query("SELECT article_id, attribute_id, language_id, active, value, utimestamp, itimestamp
							FROM wp_article_attribute
							WHERE platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$LANG[ $a['article_id'] ]['D'][ $a['language_id'] ]['ATTRIBUTE']['D'][ $a['attribute_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'VALUE'			=> $a['value'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
		}
		#ATTRIBUTE ENDE	==========================
		
		#CATEGORIE START =========================
		$qry = $this->SQL->query("SELECT categorie_id, article_id, active, utimestamp, itimestamp
							FROM wp_categorie_article
							WHERE platform_id = '{$this->platform_id}'");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$CAT[ $a['article_id'] ]['D'][ $a['categorie_id'] ] = array(
				'ACTIVE'		=> $a['active'],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				);
		}
		#CATEGORIE ENDE	==========================
		
		#STOCK START =============================
		$qry = $this->SQL->query("SELECT article_id, stock
							FROM wp_article_stock");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$STOCK[ $a['article_id'] ]['STOCK'] = $a['stock'];
		}
		#STOCK ENDE ==============================
		
		#ARTICLE START ===========================
		$qry = $this->SQL->query("SELECT id, active, number, ean, weight, price, vat, attribute_group_id, variante_group_id, utimestamp, itimestamp
							FROM wp_article a
							WHERE platform_id = '{$this->platform_id}' AND parent_id IS NULL {$W} 
							ORDER BY number DESC {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['id'] ] = array(
				'ATTRIBUTE_GROUP_ID'	=> $a['attribute_group_id'],
				'VARIANTE_GROUP_ID'		=> $a['variante_group_id'],
				'ACTIVE'		=> $a['active'],
				'NUMBER'		=> $a['number'],
				'EAN'			=> $a['ean'],
				'WEIGHT'		=> $a['weight'],
				'PRICE'			=> $a['price'],
				'VAT'			=> $a['vat'],
				'STOCK'			=> $STOCK[ $a['id'] ]['STOCK'],
				'LANGUAGE'		=> $LANG[ $a['id'] ],
				'CATEGORIE'		=> $CAT[ $a['id'] ],
				'UTIMESTAMP'	=> $a['utimestamp'],
				'ITIMESTAMP'	=> $a['itimestamp'],
			);
		}
		#ARTICLE ENDE ===========================
		
		if($D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'])
		{
			$ID = implode("','",array_keys((array)$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D']));
			#REFERENZ START =================
			$qry = $this->SQL->query("SELECT from_article_id, to_platform_id, to_article_id, active, utimestamp, itimestamp
							FROM wp_article_reference
							WHERE from_platform_id = '{$this->platform_id}' AND from_article_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['from_article_id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
			}
			#REFERENZ ENDE ==================
			
			#SET START ======================
			$qry = $this->SQL->query("SELECT article_id, contain_article_id, aset.active, aset.quantity, aset.utimestamp, aset.itimestamp,
										a.weight
										FROM wp_article_set aset, wp_article a
										WHERE aset.contain_article_id = a.id
											AND aset.platform_id = a.platform_id
											AND a.platform_id = '{$this->platform_id}'
											AND article_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['SET']['ARTICLE']['D'][ $a['contain_article_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'QUANTITY'		=> $a['quantity'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['WEIGHT'] += $a['weight']*$a['quantity'];
			}
			#SET ENDE =======================
			
			#FILE START ===========================
			$qry = $this->SQL->query("SELECT platform_id, article_id, file_id, active, utimestamp, itimestamp, sort
							FROM wp_article_file
							WHERE platform_id = '{$this->platform_id}' AND article_id IN ('{$ID}')
							ORDER BY sort");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['article_id'] ]['FILE']['D'][ $a['file_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'SORT'			=> $a['sort'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
			}
			#FILE ENDE ============================
			
			#VARIANTE START ========================================
			$qry = $this->SQL->query("SELECT id, parent_id, active, number, weight, price, vat, utimestamp, itimestamp
							FROM wp_article
							WHERE platform_id = '{$this->platform_id}' AND parent_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'NUMBER'		=> $a['number'],
					'WEIGHT'		=> $a['weight'],
					'PRICE'			=> $a['price'],
					'VAT'			=> $a['vat'],
					'STOCK'			=> $STOCK[ $a['id'] ]['STOCK'],
					'LANGUAGE'		=> $LANG[ $a['id'] ],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
			}
			
			#REFERENZ START =================
			$qry = $this->SQL->query("SELECT from_article_id, to_platform_id, to_article_id, ar.active, ar.utimestamp, ar.itimestamp
							,a.id,a.parent_id
							FROM wp_article_reference ar, wp_article a
							WHERE ar.from_article_id = a.id
							AND ar.from_platform_id = a.platform_id
							AND a.platform_id = '{$this->platform_id}'
							AND a.parent_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['id'] ]['PLATFORM']['D'][ $a['to_platform_id'] ]['REFERENCE']['D'][ $a['to_article_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
			}
			#REFERENZ ENDE ==================
			
			#SET START ======================
			$qry = $this->SQL->query("SELECT article_id, contain_article_id, ase.active, ase.quantity, ase.utimestamp, ase.itimestamp
							,a.id,b.parent_id,a.weight
							FROM wp_article_set ase, wp_article a, wp_article b
							WHERE ase.contain_article_id = a.id
							AND ase.platform_id = a.platform_id
							AND ase.article_id = b.id
							AND a.platform_id = b.platform_id
							AND b.platform_id = '{$this->platform_id}'
							AND b.parent_id IN ('{$ID}')");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['article_id'] ]['SET']['ARTICLE']['D'][ $a['contain_article_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'QUANTITY'		=> $a['quantity'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['article_id'] ]['WEIGHT'] += $a['weight']*$a['quantity'];
			}
			#SET ENDE =======================
			
			#FILE START ===========================
			$qry = $this->SQL->query("SELECT af.platform_id, af.article_id, af.file_id, af.active, af.utimestamp, af.itimestamp, af.sort
								,a.id,a.parent_id
							FROM wp_article_file af, wp_article a
							WHERE af.article_id = a.id
								AND af.platform_id = a.platform_id
								AND a.platform_id = '{$this->platform_id}'
								AND a.parent_id IN ('{$ID}')
							ORDER BY af.sort");
			while($a = $qry->fetchArray(SQLITE3_ASSOC))
			{
				$D['PLATFORM']['D'][ $this->platform_id ]['ARTICLE']['D'][ $a['parent_id'] ]['VARIANTE']['D'][ $a['id'] ]['FILE']['D'][ $a['file_id'] ] = array(
					'ACTIVE'		=> $a['active'],
					'SORT'			=> $a['sort'],
					'UTIMESTAMP'	=> $a['utimestamp'],
					'ITIMESTAMP'	=> $a['itimestamp'],
					);
			}
			#FILE ENDE ============================
			
			#VARIANTE ENDE =========================================
		}
	}
	
	

	function get_setting(&$D=null)
	{
		$this->MAIN->get_setting($D);

		$W .= (isset($D['SETTING']['W']['ID']))? " AND id IN ('{$D['SETTING']['W']['ID']}')":'';
		$W .= (isset($D['SETTING']['W']['READWRITE']))? " AND readwrite IN ('{$D['SETTING']['W']['READWRITE']}')":'';
		$qry = $this->SQL->query("SELECT id AS ID, parent_id AS PARENT_ID, active AS ACTIVE, value AS VALUE, platform_id AS PLATFORM_ID, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
			FROM wp_setting
			WHERE platform_id = '' {$W}
		");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			if($D['SETTING']['D'][ $a['ID'] ]['READWRITE']>21)
				$D['SETTING']['D'][ $a['ID'] ] = array_replace_recursive((array)$D['SETTING']['D'][ $a['ID'] ],$a);
		}
	}
	
	function set_setting(&$D)
	{
		foreach((array)$D['SETTING']['D'] AS $kSET => $SET) {
			if($SET['ACTIVE'] != -2) {
					$IU_SET .= (($IU_SET)?',':'')."('{$kSET}',''";
					$IU_SET .= (isset($SET['PARENT_ID']))? ",'{$SET['PARENT_ID']}'":",NULL";
					$IU_SET .= (isset($SET['ACTIVE']))? ",'{$SET['ACTIVE']}'":",NULL";
					$IU_SET .= (isset($SET['VALUE']))? ",'{$SET['VALUE']}'":",NULL";
					$IU_SET .= ")";
			}
			else {
				$D_SET .= (($D_SET)?',':'')."'{$kSET}'";
			}
			
			if($IU_SET) {
				$this->SQL->query("INSERT INTO wp_setting (id, platform_id, parent_id, active, value) VALUES {$IU_SET} 
							ON CONFLICT(id, platform_id) DO UPDATE SET
								parent_id =	CASE WHEN excluded.parent_id IS NOT NULL			AND ifnull(parent_id,'') <> excluded.parent_id			THEN excluded.parent_id ELSE parent_id END,
								active =		CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'')  <> excluded.active				THEN excluded.active ELSE active END,
								value =			CASE WHEN excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value					THEN excluded.value ELSE value END,
								utimestamp =	CASE WHEN 
														excluded.parent_id IS NOT NULL			AND ifnull(parent_id ,'')<> excluded.parent_id
														OR excluded.active IS NOT NULL			AND ifnull(active,'') <> excluded.active
														OR excluded.value IS NOT NULL			AND ifnull(value,'') <> excluded.value
												THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
			}
			if($D_SET) {
				$this->SQL->query("DELETE FROM wp_setting WHERE id IN ({$D_SET}) AND platform_id = ''");
			}
			
		}
	}
	
	
	function get_user(&$D=null)
	{
		$W .= ($D['USER']['W']['NICKNAME'])?" AND nickname IN ('{$D['USER']['W']['NICKNAME']}')":'';
		$W .= ($D['USER']['W']['PASSWORD'])?" AND pass IN ('{$D['USER']['W']['PASSWORD']}')":'';
		$W .= ($D['USER']['W']['ID'])?" AND id IN ('{$D['USER']['W']['ID']}')":'';
		#$W .= ($D['USER']['W']['ACCOUNT_ID'])?" AND id IN (SELECT user_id FROM wp_user_account WHERE account_id = '{$D['USER']['W']['ACCOUNT_ID']}')":'';
		$L .= ($D['USER']['L']['START'] && $D['USER']['L']['STEPP'])? " LIMIT {$D['USER']['L']['START']},{$D['USER']['L']['STEPP']} ":'';
		
		$qry = $this->SQL->query("SELECT id, active AS ACTIVE, nickname AS NICKNAME, name AS NAME, fname AS FNAME, pass AS PASSWORD, email AS EMAIL, login_fail AS LOGIN_FAIL, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_user
							WHERE 1 {$W} {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['USER']['D'][ $a['id'] ] = $a;
		}
		
		/*
		$ID = implode("','",array_keys((array)$D['USER']['D']));
		$qry = $this->SQL->query("SELECT user_id, right_id,platform_id, active, itimestamp, utimestamp 
							FROM wp_user_right
							WHERE 1 AND user_id IN ('{$ID}')");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['USER']['D'][ $a['user_id'] ]['PLATFORM']['D'][ $a['platform_id'] ]['RIGHT']['D'][ $a['right_id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		*/
	}
	
	function set_user(&$D)
	{
		foreach((array)$D['USER']['D'] AS $kUSR => $USR) {
			if($USR['ACTIVE'] != -2) {
					$IU_USR .= (($IU_USR)?',':'')."('{$kUSR}'";
					$IU_USR .= (isset($USR['ACTIVE']))? ",'{$USR['ACTIVE']}'":",NULL";
					$IU_USR .= (isset($USR['NICKNAME']))? ",'{$USR['NICKNAME']}'":",NULL";
					$IU_USR .= (isset($USR['NAME']))? ",'{$USR['NAME']}'":",NULL";
					$IU_USR .= (isset($USR['FNAME']))? ",'{$USR['FNAME']}'":",NULL";
					$IU_USR .= (isset($USR['PASSWORD']) && $USR['PASSWORD'] != '')? ",'{$USR['PASSWORD']}'":",NULL";
					$IU_USR .= (isset($USR['EMAIL']) && $USR['EMAIL'] != '')? ",'{$USR['EMAIL']}'":",NULL";
					$IU_USR .= (isset($USR['LOGIN_FAIL']) && $USR['LOGIN_FAIL'] != '')? ",'{$USR['LOGIN_FAIL']}'":",NULL";
					$IU_USR .= ")";
			}
			else {
				$D_USR .= (($D_USR)?',':'')."'{$kUSR}'";
			}
			
			if($IU_USR) {
				$this->SQL->query("INSERT INTO wp_user (id,active,nickname,name,fname,pass,email,login_fail) VALUES {$IU_USR} 
							ON CONFLICT(id) DO UPDATE SET
								active =		CASE WHEN excluded.active IS NOT NULL			AND ifnull(active,'')  <> excluded.active				THEN excluded.active ELSE active END,
								nickname =		CASE WHEN excluded.nickname IS NOT NULL			AND ifnull(nickname,'') <> excluded.nickname			THEN excluded.nickname ELSE nickname END,
								name =			CASE WHEN excluded.name IS NOT NULL				AND ifnull(name,'') <> excluded.name					THEN excluded.name ELSE name END,
								fname =			CASE WHEN excluded.fname IS NOT NULL			AND ifnull(fname,'') <> excluded.fname					THEN excluded.fname ELSE fname END,
								pass =			CASE WHEN excluded.pass IS NOT NULL				AND ifnull(pass,'') <> excluded.pass					THEN excluded.pass ELSE pass END,
								email =			CASE WHEN excluded.email IS NOT NULL			AND ifnull(email,'') <> excluded.email					THEN excluded.email ELSE email END,
								login_fail =	CASE WHEN excluded.login_fail IS NOT NULL		AND ifnull(login_fail,'') <> excluded.login_fail		THEN excluded.login_fail ELSE login_fail END,
								
								utimestamp =	CASE WHEN 
														excluded.active IS NOT NULL				AND ifnull(active,'') <> excluded.active
														OR excluded.nickname IS NOT NULL		AND ifnull(nickname,'') <> excluded.nickname
														OR excluded.name IS NOT NULL			AND ifnull(name,'') <> excluded.name
														OR excluded.fname IS NOT NULL			AND ifnull(fname,'') <> excluded.fname
														OR excluded.pass IS NOT NULL			AND ifnull(pass,'') <> excluded.pass
														OR excluded.email IS NOT NULL			AND ifnull(email,'') <> excluded.email
														OR excluded.login_fail IS NOT NULL		AND ifnull(login_fail,'') <> excluded.login_fail
												THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
			}
			if($D_USR) {
				$this->SQL->query("DELETE FROM wp_user WHERE id IN ({$D_USR})");
			}
			
		}
		
	}
	/*
	function get_storage($D=null)
	{
		$W .= ($D['STORAGE']['W']['ID'])? " AND id IN ('{$D['STORAGE']['W']['ID']}')":'';
		$W .= ($D['STORAGE']['W']['WAREHOUSE_ID'])? " AND warehouse_id IN ('{$D['STORAGE']['W']['WAREHOUSE_ID']}')":'';
		$W .= ($D['STORAGE']['W']['ACTIVE'])? " AND active = '{$D['STORAGE']['W']['ACTIVE']}'":'';
		$qry = $this->SQL->query("SELECT id, warehouse_id, active, title, itimestamp, utimestamp 
								FROM wp_warehouse_storage
								WHERE account_id = '{$this->account_id}'
								ORDER BY title");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['STORAGE']['D'][ $a['id'] ] = array(
				'WAREHOUSE_ID'				=> $a['warehouse_id'],
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
			$ID = (($ID)?',':'')."'{$a['id']}{$a['warehouse_id']}'";
		}
		
		
		$qry = $this->SQL->query("SELECT article_id, storage_id, active, stock, itimestamp, utimestamp 
								FROM wp_article_stock
								WHERE account_id = '{$this->account_id}' AND CONCAT(storage_id,warehouse_id) IN ({$ID})
								ORDER BY itimestamp");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['STORAGE']['D'][ $a['storage_id'] ]['ARTICLE']['D'][ $a['article_id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'STOCK'						=> $a['stock'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
			$D['STORAGE']['ARTICLE']['D'][ $a['article_id'] ]['STOCK'] += $a['stock'];
		}
		
		return $D;
	}
	*/
}