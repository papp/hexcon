<?php
#read Only
class system #extends CData
{
	function __construct()
	{
		#Read only
		$SQL_System = new SQLite3("system.db", SQLITE3_OPEN_READWRITE);
		$SQL_System->exec('
			PRAGMA busy_timeout = 5000;
			PRAGMA cache_size = -2000;
			PRAGMA synchronous = OFF;
			PRAGMA foreign_keys = ON;
			PRAGMA temp_store = MEMORY;
			PRAGMA default_temp_store = MEMORY;
			PRAGMA read_uncommitted = true;
			PRAGMA journal_mode = off;
			PRAGMA query_only = true;
		');
		$this->SQL = $SQL_System;
		
		$D['PATTERN']['LANGUAGE'] = [
			'Active'		=> ['Type' => 'checkbox'],
			'Title'			=> ['Type' => 'text', 'Length' => 200],
		];
		$D['PATTERN']['LANGUAGE']['I18N'] = [
			'LANGUAGE'		=> ['Type' => 'id'],
			'Active'		=> ['Type' => 'checkbox'],
			'Value'			=> ['Type' => 'text', 'Length' => 200],

		];
		##parent::__construct($SQL_System, ['PATTERN' => &$D['PATTERN']]);
		
	}
	
	function get_i18n(&$D=null) {
		$W = (isset($D['I18N']['W']['LANGUAGE_ID']))? " AND language_id IN ('{$D['I18N']['W']['LANGUAGE_ID']}')":'';
		$qry = $this->SQL->query("SELECT id, language_id, value VALUE 
							FROM wp_i18n
							WHERE 1 {$W}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC)) {
			$D['LANGUAGE']['D'][ $a['language_id'] ]['I18N']['D'][$a['id']] = $a;
		}
	}

	function get_setting(&$D=null)
	{
		/*
		readwrite:
		read|write
		0 = system
		1 = main
		2 = account
		3 = platform
		*/
		$W = (isset($D['SETTING']['W']['ID']))? " AND id IN ('{$D['SETTING']['W']['ID']}')":'';
		$W .= (isset($D['SETTING']['W']['READWRITE']))? " AND readwrite IN ('{$D['SETTING']['W']['READWRITE']}')":'';
		#$W .= (isset($D['SETTING']['W']['READWRITE:>=']))? " AND '{$D['SETTING']['W']['READWRITE:>=']}' >= readwrite":'';
		#$W .= (isset($D['SETTING']['W']['READWRITE:<=']))? " AND '{$D['SETTING']['W']['READWRITE:<=']}' <= readwrite":'';
		#$W .= (isset($D['SETTING']['W']['READWRITE:LIKE']))? " AND readwrite LIKE '{$D['SETTING']['W']['READWRITE:<=']}' ":'';
		$qry = $this->SQL->query("SELECT id AS ID, parent_id AS PARENT_ID, active AS ACTIVE, readwrite AS READWRITE, type AS TYPE, value AS VALUE, value_option AS VALUE_OPTION, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
			FROM wp_setting
			WHERE 1 {$W}
		");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['SETTING']['D'][ $a['ID'] ] = $a;
			$D['SETTING']['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['ID'] ] = &$D['SETTING']['D'][ $a['ID'] ];
			$D['SETTING']['CHILD']['D'][ $a['ID'] ] = &$D['SETTING']['PARENT']['D'][ $a['PARENT_ID'] ]['CHILD']['D'][ $a['ID'] ];
		}
	}


	function get_status(&$D=null)
	{
		$D['INVOICE']['STATUS']['D'] = [
			'-20'	=> ['TITLE' => 'storno', 'COLOR' => '#fbbac5'],
			'0'		=> ['TITLE' => 'offen', 'COLOR' => '#fff'],
			'9'		=> ['TITLE' => 'Klärung', 'COLOR' => '#bafbfa'],
			'20'	=> ['TITLE' => 'Versandfreigabe', 'COLOR' => '#fafbba'],
			'40'	=> ['TITLE' => 'Fertig', 'COLOR' => '#c3fbba'],
		];
	}


	function get_update(&$D=null) {
		$W .= ($D['UPDATE']['W']['ID'])? " AND id IN ('{$D['UPDATE']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id ID, changelog CHANGELOG, itimestamp*1 ITIMESTAMP 
							FROM wp_update
							WHERE 1 {$W} {$L}
							ORDER BY itimestamp DESC");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['UPDATE']['D'][ $a['ID'] ] = $a;
		}
	}
	
}