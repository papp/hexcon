<?php
class main
{
	function __construct()
	{
		global $SYSTEM;
		$this->SYSTEM = $SYSTEM;
		

		$SQL_Main = new SQLite3("data/main.db", SQLITE3_OPEN_READWRITE);
		$SQL_Main->exec('
				PRAGMA busy_timeout = 5000;
				PRAGMA cache_size = -2000;
				PRAGMA synchronous = 1;
				PRAGMA foreign_keys = ON;
				PRAGMA temp_store = MEMORY;
				PRAGMA default_temp_store = MEMORY;
				PRAGMA read_uncommitted = true;
				PRAGMA journal_mode = wal;
				PRAGMA wal_autocheckpoint=1000;
			');
		$this->SQL = $SQL_Main;
	}
	
	function get_setting(&$D=null)
	{
		$this->SYSTEM->get_setting($D);

		$W .= (isset($D['SETTING']['W']['ID']))? " AND id IN ('{$D['SETTING']['W']['ID']}')":'';
		$W .= (isset($D['SETTING']['W']['READWRITE']))? " AND readwrite IN ('{$D['SETTING']['W']['READWRITE']}')":'';
		$qry = $this->SQL->query("SELECT id AS ID, parent_id AS PARENT_ID, active AS ACTIVE, value AS VALUE, platform_id AS PLATFORM_ID, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
			FROM wp_setting
			WHERE platform_id = '' {$W}
		");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			if($D['SETTING']['D'][ $a['ID'] ]['READWRITE']>10)
				$D['SETTING']['D'][ $a['ID'] ] = array_replace_recursive((array)$D['SETTING']['D'][ $a['ID'] ],$a);
		}
		
	}



	#gibt alle mögliche Platformen aus die Zur verfügung stehen
	function get_platform(&$D = null)
	{
		$f = CFile::dir(['PATH' => 'core/platform']);
		foreach($f['DIR'] AS $kF => $F)
			$D['PLATFORM']['D'][$F['NAME']]['TITLE'] = $F['NAME'];
			#print_r($D['PLATFORM']['D']);
		#return $D;
	}
	
	function get_account(&$D=null) {
		$W = (isset($D['ACCOUNT']['W']['ID']))?" AND id IN ('{$D['ACCOUNT']['W']['ID']}')":'';
		$W .= (isset($D['ACCOUNT']['W']['ACTIVE']))?" AND active IN ('{$D['ACCOUNT']['W']['ACTIVE']}')":'';
		$W .= CWP::sql_get_where2(array(
			'ID:IN'				=> " AND id [1] ('[V]')",
			'ACTIVE'			=> " AND active [1] ('[V]')",
			),$D['ACCOUNT']['W']);
		
		$L = (isset($D['ACCOUNT']['L']['START']) && isset($D['ACCOUNT']['L']['STEP']))? " LIMIT {$D['ACCOUNT']['L']['START']},{$D['ACCOUNT']['L']['STEP']} ":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_account 
							WHERE 1 {$W}
							ORDER BY sort {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['ACCOUNT']['D'][ $a['id'] ]['ACTIVE'] = $a['active'];
			$D['ACCOUNT']['D'][ $a['id'] ]['TITLE'] = $a['title'];
			$D['ACCOUNT']['D'][ $a['id'] ]['ITIMESTAMP'] = $a['itimestamp'];
			$D['ACCOUNT']['D'][ $a['id'] ]['UTIMESTAMP'] = $a['utimestamp'];
		}
		/*
		$ID = implode("','",array_keys((array)$D['ACCOUNT']['D']));
		$qry = $this->SQL->query("SELECT  p.id, class_id, p.active, p.itimestamp, p.utimestamp, p.title
							FROM  wp_platform p
							
							ORDER BY sort {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['ACCOUNT']['D'][ $a['account_id'] ]['PLATFORM']['D'][ $a['id'] ] = array(
				'CLASS_ID'		=> $a['class_id'],
				'ACTIVE'		=> $a['active'],
				'TITLE'			=> $a['title'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			);
		}
		
		
		$qry = $this->SQL->query("SELECT platform_from_id, platform_to_id, type, active, itimestamp, utimestamp
							FROM  wp_platform_platform 
							
							");
		while($a = $qry->fetchArray(SQLITE3_ASSOC) )
		{
			$D['ACCOUNT']['D'][ $a['account_id'] ]['PLATFORM']['D'][ $a['platform_from_id'] ]['TO']['PLATFORM']['D'][ $a['platform_to_id']] = [
				'ACTIVE'		=> $a['active'],
				'TYPE'			=> $a['type'],
				'ITIMESTAMP'	=> $a['itimestamp'],
				'UTIMESTAMP'	=> $a['utimestamp'],
			];
			$D['ACCOUNT']['D'][ $a['account_id'] ]['PLATFORM']['D'][ $a['platform_to_id'] ]['FROM']['PLATFORM']['D'][ $a['platform_from_id']] = &$D['ACCOUNT']['D'][ $a['account_id'] ]['PLATFORM']['D'][ $a['platform_from_id'] ]['TO']['PLATFORM']['D'][ $a['platform_to_id']];
		}
		*/
	}
	
	function set_account($D) {
		foreach((array)$D['ACCOUNT']['D'] AS $kP => $P) {
			if($P['ACTIVE'] != -2) {
				$IU_ACO .= (($IU_ACO)?',':'')."('{$kP}'";
				$IU_ACO .= (isset($P['ACTIVE']))? ",'{$P['ACTIVE']}'":",NULL";
				$IU_ACO .= (isset($P['TITLE']))? ",'{$P['TITLE']}'":",NULL";
				$IU_ACO .= (isset($P['TITLE']))? ",'{$P['SORT']}'":",NULL";
				$IU_ACO .= ")";
			}
			else {
				$D_ACO .= (($D_ACO)?',':'')."'{$kP}'";
			}
		}
		if($IU_ACO) {
			$this->SQL->query("INSERT INTO wp_account (id, active, title, sort) VALUES {$IU_ACO}
							ON CONFLICT(id) DO UPDATE SET
								active =		CASE WHEN excluded.active IS NOT NULL	AND ifnull(active,'') <> excluded.active	THEN excluded.active ELSE active END,
								title =			CASE WHEN excluded.title IS NOT NULL	AND ifnull(title,'') <> excluded.title		THEN excluded.title ELSE title END,
								sort =			CASE WHEN excluded.sort IS NOT NULL		AND ifnull(sort,'') <> excluded.sort		THEN excluded.sort ELSE sort END,

								utimestamp =	CASE WHEN 
															   excluded.active IS NOT NULL		AND ifnull(active,'') <> excluded.active
															OR excluded.title IS NOT NULL		AND ifnull(title,'') <> excluded.title
															OR excluded.sort IS NOT NULL		AND ifnull(sort,'') <> excluded.sort
											THEN CURRENT_TIMESTAMP ELSE utimestamp END
							");
		}
	}
	
	function get_right(&$D=null)
	{
		$W .= ($D['RIGHT']['W']['ID'])?" AND id IN ('{$D['RIGHT']['W']['ID']}')":'';
		$L .= ($D['RIGHT']['L']['START'] && $D['RIGHT']['L']['STEPP'])? " LIMIT {$D['RIGHT']['L']['START']},{$D['RIGHT']['L']['STEPP']} ":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
								FROM wp_right 
								WHERE 1 {$W}
								ORDER BY title {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['RIGHT']['D'][ $a['id'] ] = array(
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
				);
		}
		
	}
	
	function get_user(&$D=null)
	{
		$W .= ($D['USER']['W']['NICKNAME'])?" AND nickname IN ('{$D['USER']['W']['NICKNAME']}')":'';
		$W .= ($D['USER']['W']['PASSWORD'])?" AND pass IN ('{$D['USER']['W']['PASSWORD']}')":'';
		$W .= ($D['USER']['W']['ID'])?" AND id IN ('{$D['USER']['W']['ID']}')":'';
		#$W .= ($D['USER']['W']['ACCOUNT_ID'])?" AND id IN (SELECT user_id FROM wp_user_account WHERE account_id = '{$D['USER']['W']['ACCOUNT_ID']}')":'';
		$L .= ($D['USER']['L']['START'] && $D['USER']['L']['STEPP'])? " LIMIT {$D['USER']['L']['START']},{$D['USER']['L']['STEPP']} ":'';
		
		$qry = $this->SQL->query("SELECT id, active AS ACTIVE, nickname AS NICKNAME, pass AS PASSWORD, email AS EMAIL, login_fail AS LOGIN_FAIL, itimestamp*1 AS ITIMESTAMP, utimestamp*1 AS UTIMESTAMP
							FROM wp_user
							WHERE 1 {$W} {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['USER']['D'][ $a['id'] ] = $a;
		}
	}
	/*
	function set_user($D)
	{
		$k = array_keys((array)$D['USER']['D']);
		for($i=0; $i< count($k); $i++)
		{
			$user = $D['USER']['D'][$k[$i]];
			if($user['ACTIVE'] != -2)
			{
				$IU_user .= (($IU_user)?',':'')."('{$k[$i]}'";
				$IU_user .= (isset($user['ACTIVE']))? ",'{$user['ACTIVE']}'":",NULL";
				$IU_user .= (isset($user['NICKNAME']))? ",'{$user['NICKNAME']}'":",NULL";
				$IU_user .= (isset($user['NAME']))? ",'{$user['NAME']}'":",NULL";
				$IU_user .= (isset($user['FNAME']))? ",'{$user['FNAME']}'":",NULL";
				$IU_user .= (isset($user['PASSWORD']) && $user['PASSWORD'] != '')? ",'{$user['PASSWORD']}'":",NULL";
				$IU_user .= ")";
				
				$aK = array_keys((array)$user['ACCOUNT']['D']);
				for($a=0; $a<count($aK);$a++)#ACCOUNT
				{
					$account = $user['ACCOUNT']['D'][$aK[$a]];
					$pK = array_keys((array)$account['PLATFORM']['D']);
					for($p=0; $p<count($pK);$p++)#PLATFORM
					{
						$platform = $account['PLATFORM']['D'][$pK[$p]];
						$rK = array_keys((array)$platform['RIGHT']['D']);
						for($r=0; $r<count($rK);$r++)#PLATFORM
						{
							$right = $platform['RIGHT']['D'][$rK[$r]];
							if($right['ACTIVE'] != -2)
							{
								$IU_right .= (($IU_right)?',':'')."('{$k[$i]}','{$aK[$a]}','{$pK[$p]}','{$rK[$r]}'";
								$IU_right .= (isset($right['ACTIVE']))? ",'{$right['ACTIVE']}'":",NULL";
								$IU_right .= ")";
							}
							else
								$D_right .= (($D_right)?',':'')."'{$k[$i]}{$aK[$a]}{$pK[$p]}{$rK[$r]}'";
						}
					}
				}
			}
			else
				$D_user .= (($D_user)?',':'')."'{$k[$i]}'";
			
			
			if($IU_user)
				$this->SQL->query("INSERT INTO wp_user (id,active,nickname,name,fname,pass) VALUES {$IU_user} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_user.active END,
							nickname = CASE WHEN VALUES(nickname) IS NOT NULL THEN VALUES(nickname) ELSE wp_user.nickname END,
							name = CASE WHEN VALUES(name) IS NOT NULL THEN VALUES(name) ELSE wp_user.name END,
							fname = CASE WHEN VALUES(fname) IS NOT NULL THEN VALUES(fname) ELSE wp_user.fname END,
							pass = CASE WHEN VALUES(pass) IS NOT NULL THEN VALUES(pass) ELSE wp_user.pass END
						");
			if($IU_right)
			$this->SQL->query("INSERT INTO wp_user_right (user_id,account_id,platform_id,right_id,active) VALUES {$IU_right} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_user_right.active END
						");
			if($D_right)
			$this->SQL->query("DELETE FROM wp_user_right WHERE concat(user_id,account_id,platform_id,right_id) IN ({$D_right})");
		}
		return $D;
	}
	*/
	#veraltet
	function get_language(&$D=null)
	{
		$W .= ($D['LANGUAGE']['W']['ID'])?" AND id IN ('{$D['LANGUAGE']['W']['ID']}')":'';
		$L .= ($D['LANGUAGE']['L']['START'] && $D['LANGUAGE']['L']['STEPP'])? " LIMIT {$D['LANGUAGE']['L']['START']},{$D['LANGUAGE']['L']['STEPP']} ":'';
		$qry = $this->SQL->query("SELECT id, active, title, itimestamp, utimestamp 
							FROM wp_language
							WHERE 1 {$W} {$L}");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['LANGUAGE']['D'][ $a['id'] ] = [
				'ACTIVE'					=> $a['active'],
				'TITLE'						=> $a['title'],
				'ITIMESTAMP'				=> $a['itimestamp'],
				'UTIMESTAMP'				=> $a['utimestamp'],
			];
		}
	}


	
	
	
	#veraltet
	function get_status(&$D=null)
	{
		$D['INVOICE']['STATUS']['D'] = [
			'-20'	=> ['TITLE' => 'storno', 'COLOR' => '#fbbac5'],
			'0'		=> ['TITLE' => 'offen', 'COLOR' => '#fff'],
			'9'		=> ['TITLE' => 'Klärung', 'COLOR' => '#bafbfa'],
			'20'	=> ['TITLE' => 'Versandfreigabe', 'COLOR' => '#fafbba'],
			'40'	=> ['TITLE' => 'Fertig', 'COLOR' => '#c3fbba'],
		];
	}
	#veraltet
	#ToDo: Diese werte sollten je Account einstellbar sein, so dass nicht jeder wert zur auswahl je Account zur verfügung steht.
/*
	function get_payment(&$D=null)
	{
		$D['PAYMENT']['D'] = [
			'PP'		=> ['ACTIVE' => 1, 'TITLE' => 'PayPal'],
			#'CC'		=> ['ACTIVE' => 1, 'TITLE' => 'Kreditkarte'],
			'AMAZON'	=> ['ACTIVE' => 1, 'TITLE' => 'Amazon'],
			'TP'		=> ['ACTIVE' => 1, 'TITLE' => 'Vorkasse'],
			'CP'		=> ['ACTIVE' => 1, 'TITLE' => 'Barzahlung'],
			'BP'		=> ['ACTIVE' => 1, 'TITLE' => 'Auf Rechnung'],
		];
	}
	*/
	
	#veraltet
	/*
	function get_shipping(&$D=null)
	{
		$D['SHIPPING']['D'] = [
			'DHL'		=> ['ACTIVE' => 1, 'TITLE' => 'DHL'],
			#'DHL_EX'	=> ['ACTIVE' => 1, 'TITLE' => 'DHL Express'],
			'DP'		=> ['ACTIVE' => 1, 'TITLE' => 'Deutsche Post'],
			#'DPD'		=> ['ACTIVE' => 1, 'TITLE' => 'DPD'],
			'HS'		=> ['ACTIVE' => 1, 'TITLE' => 'Hermes'],
			#'GLS'		=> ['ACTIVE' => 1, 'TITLE' => 'GLS'],
			#'UPS'		=> ['ACTIVE' => 1, 'TITLE' => 'UPS'],
			'DPD'		=> ['ACTIVE' => 1, 'TITLE' => 'DPD'],
		];
	}
	*/
/*
	function get_currency(&$D=null) #ToDo: Veraltet, soll aus account get_currency verwendet werden, weil die ExchageRate sich nach standard Währung richten muss.
	{
		$D['CURRENCY']['D'] = [
			'EUR'	=> ['SIGN' => '€',	'ExchangeRate' => 1],
			'USD'	=> ['SIGN' => '$',	'ExchangeRate' => 1.1],
			'PLN'	=> ['SIGN' => 'zł',	'ExchangeRate' => 4.35],
		];
	}*/
/*
	function get_warehouse(&$D=null)
	{
		$D['WAREHOUSE']['D']['AMAZON_FBA'] = ['ACTIVE' => 1, 'TITLE' => 'Amazon FBA']; #ToDo: Solche Lager sollen einzelne Platformen ggf. mit übergeben
	}
*/
	#ToDo: History zuende programmieren
	function set_history($D)
	{ #Type = [ARTICLE, CATEGORIE, ....] | type_id = article_id, categorie_id
		$d['HISTORY']['D']['W']['TYPE'] = 'ARTICLE';
		$d['HISTORY']['D']['W']['TYPE_ID'] = 'xyz';
		$d['HISTORY']['D']['L']['START'] = 0;
		$d['HISTORY']['D']['L']['STEP'] = 1;
		$this->get_history($d);
		$diff = CWP::array_diff_assoc_recursive($D['ARTICLE']['D']['xyz'],$d['ARTICLE']['D']['xyz']);
		$jd = json_encode($diff);
		if(strlen($diff) > 2)
		{
			$IU_his .= (($IU_his)?',':'')."(time(),'{$D['SESSION']['USER_ID']}','{$D['PLATFORM_ID']}','{$D['ACCOUNT_ID']}',1,'ARTICLE','xyz','{$jd}'";
			$IU_his .= ")";
		}

		$this->SQL->query("INSERT INTO wp_history (id, user_id, platform_id, account_id, active, type, type_id, change) VALUES {$IU_his}");
	}

	function get_history(&$D=null)
	{
		$W .= ($D['HISTORY']['W']['ID'])? " AND id IN ('{$D['HISTORY']['W']['ID']}')":'';
		$W .= ($D['HISTORY']['W']['TYPE'])? " AND type IN ('{$D['HISTORY']['W']['TYPE']}')":'';
		$W .= ($D['HISTORY']['W']['TYPE_ID'])? " AND type_id IN ('{$D['HISTORY']['W']['TYPE_ID']}')":'';
		$L .= ($D['HISTORY']['L']['START'] && $D['HISTORY']['L']['STEP'])? "LIMIT {$D['HISTORY']['L']['START']},{$D['HISTORY']['L']['STEP']}" : '';
		$qry = $this->SQL->query("SELECT id, user_id, platform_id, account_id, active ACTIVE, type, type_id, change CHANGE, itimestamp*1 ITIMESTAMP 
							FROM wp_history
							WHERE 1 {$W} {$L}
							ORDER BY itimestamp DESC");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D[ $a['type'] ]['D'][ $a['type_id'] ]['HISTORY']['D'][ $a['id'] ] = $a;
		}
	}


	function get_update(&$D=null) {
		$W .= ($D['UPDATE']['W']['ID'])? " AND id IN ('{$D['UPDATE']['W']['ID']}')":'';
		$qry = $this->SQL->query("SELECT id ID, changelog CHANGELOG, itimestamp*1 ITIMESTAMP 
							FROM wp_update
							WHERE 1 {$W} {$L}
							ORDER BY itimestamp DESC");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['UPDATE']['D'][ $a['ID'] ] = $a;
		}
	}
	
	#veraltet
	function get_currency(&$D=null)
	{
		$qry = $this->SQL->query("SELECT id, active ACTIVE, title TITLE, symbol SYMBOL, rate RATE, itimestamp*1 ITIMESTAMP, utimestamp*1 UTIMESTAMP
							FROM wp_currency
							ORDER BY id");
		while($a = $qry->fetchArray(SQLITE3_ASSOC))
		{
			$D['CURRENCY']['D'][ $a['id'] ] = $a;
		}
	}
	
	#veraltet
	function set_currency($D)
	{
		foreach((array)$D['CURRENCY']['D'] AS $kCUR => $CUR)
		{
			if($CUR['ACTIVE'] != -2)
			{
				$IU_CUR .= (($IU_CUR)?',':'')."('{$kCUR}'";
				$IU_CUR .= (isset($CUR['ACTIVE']))? ",'{$CUR['ACTIVE']}'":",NULL";
				$IU_CUR .= (isset($CUR['TITLE']))? ",'".$this->SQL->escapeString($CUR['TITLE'])."'":",NULL";
				$IU_CUR .= (isset($CUR['SYMBOL']))? ",'".$this->SQL->escapeString($CUR['SYMBOL'])."'":",NULL";
				$IU_CUR .= (isset($CUR['RATE']))? ",'{$CUR['RATE']}'":",NULL";
				$IU_CUR .= ")";
			}
			else
			{
				$D_CUR .= (($D_CUR)?',':'')."'{$kCUR}'";
			}
		}
		if($IU_CUR)
			$this->SQL->query("INSERT INTO wp_currency (id, active, title, symbol, rate) VALUES {$IU_CUR} 
						ON DUPLICATE KEY UPDATE 
							active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE wp_currency.active END,
							title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE wp_currency.title END,
							symbol = CASE WHEN VALUES(symbol) IS NOT NULL THEN VALUES(symbol) ELSE wp_currency.symbol END,
							rate = CASE WHEN VALUES(rate) IS NOT NULL THEN VALUES(rate) ELSE wp_currency.rate END");
		if($D_CUR)
			$this->SQL->query("DELETE FROM wp_currency WHERE id IN ({$D_CUR})");
	}

}