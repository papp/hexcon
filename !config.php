<?php
#use Mpdf\Mpdf;

error_reporting(E_ALL ^ E_NOTICE ^E_WARNING ^E_DEPRECATED);
ini_set('display_errors', 1);
#error_reporting(E_ALL ^ E_NOTICE  ^E_DEPRECATED);

#use wp\CData;
session_start();
$D = array_merge((array)$_SESSION['D'],(array)$_REQUEST['D']);
date_default_timezone_set('Europe/Berlin');#ToDo: Zeitzone einstellbar
#Include Framework
#include('core/framework/dompdf/dompdf_config.inc.php');
require_once __DIR__ . '/core/vendor/autoload.php';


include('core/framework/smarty/Smarty.class.php');
include('core/framework/wp/CFile.php');
include('core/framework/wp/wp.php');
include('core/framework/wp/CSExt.php');
include('core/framework/wp/CMail.php');
#include('core/framework/wp/CCache.php');
#include('core/framework/wp/CData.php'); #Datenbank Erweiterung wird vom composer geladen!

##include("core/platform/cache.php"); ##??? Veraltet
include("core/account.php");
include("core/platform/platform.php");



$smarty = new Smarty();

$mpdf = new Mpdf\Mpdf(); #Neue PDF
$CFile = new CFile();
$CMail = new CMail();
#$CCache = new CCache(['DB'=>$WP['ACCOUNT']['ID']]);#deprecated
$CWP = new CWP();
$CSExt = new CSExt(); #Sichere Funktionserweiterungen für das Smarty Feed Template


$ROOTDOMAIN = explode('.', $_SERVER['SERVER_NAME']);
$_SUB = (count($ROOTDOMAIN) > 2)?$ROOTDOMAIN[0]:'';
$D['ACCOUNT_ID'] =  (in_array($ROOTDOMAIN[0], ['localhost']))?'test':$_SUB;
$D['SESSION']['ACCOUNT_ID'] = $_SESSION['D']['SESSION']['ACCOUNT_ID'] = $D['ACCOUNT_ID'];
#$lang = strtoupper(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)); #Browser Language
#ToDo: Standard Sprache Landen, wenn nicht vorhanden. Achtung dies gilt auch für PDF Exporte, ggf. mus bei exporten Sprache definiert werden in welche diese exportiert werdne sollen
$D['SYSTEM']['LANGUAGE_ID'] = $_SESSION['D']['SESSION']['LANGUAGE_ID'] = (!empty($D['SYSTEM']['LANGUAGE_ID']))? $D['SYSTEM']['LANGUAGE_ID'] : ( (!empty($_SESSION['D']['SESSION']['LANGUAGE_ID']))? $_SESSION['D']['SESSION']['LANGUAGE_ID'] : ((!empty($CWP::getBrowserLang()))?$CWP::getBrowserLang(): 'DE'));

#sqlite SYSTEM-------------------
include('core/system.php');
$SYSTEM = new system();
#sqlite-------------------

#sqlite MAIN-------------------
include('core/main.php');
$MAIN = new main();
#sqlite-------------------

#Account laden //ToDo: Nicht bei Subdomain admin!
if($D['SESSION']['ACCOUNT_ID'] && in_array($D['ACCOUNT_ID'], ['admin', 'www', ''] ) === false) {
	$D['ACCOUNT']['W']['ID'] = $D['SESSION']['ACCOUNT_ID'];
	$MAIN->get_account($D);
	if($D['ACCOUNT']['D'][ $D['SESSION']['ACCOUNT_ID'] ]['ACTIVE']) { #Ist Account registriert und auch aktive?
		#sqlite ACCOUNT-------------------
		#ToDo: DB Instanz vereerben!
		$SQL = new SQLite3("data/ACCOUNT/{$D['SESSION']['ACCOUNT_ID']}/data.db", SQLITE3_OPEN_READWRITE);
		$SQL->exec('
			PRAGMA busy_timeout = 5000;
			PRAGMA cache_size = -2000;
			PRAGMA synchronous = 1;
			PRAGMA foreign_keys = ON;
			PRAGMA temp_store = MEMORY;
			PRAGMA default_temp_store = MEMORY;
			PRAGMA read_uncommitted = true;
			PRAGMA journal_mode = wal;
			PRAGMA wal_autocheckpoint=1000;
		');
		#sqlite-----------------
		
		#Data SQL --------------
		$SQL_Data = new wp\CData(['PATTERN' => '', 
			'DB' => ['FILENAME' => "data/ACCOUNT/{$D['SESSION']['ACCOUNT_ID']}/data.db"], 
			'BACKUP' => ['DestinationPath' => "data/ACCOUNT/{$D['SESSION']['ACCOUNT_ID']}/backup/", 'BackupPassword' => null ],
		]);

		#include("core/account.php");
		$ACCOUNT = new account($D['SESSION']['ACCOUNT_ID']);
		$ACCOUNT->get_setting($D); #Einstellungen
		#Lade aktive Platformen Start ================
		#Include Platform Manager
		include("core/control.php");

		$ACCOUNT->get_platform($D);
		foreach((array)$D['PLATFORM']['D'] AS $kP => $P) {
			if($P['ACTIVE']) {
				include_once("core/platform/{$P['CLASS_ID']}/{$P['CLASS_ID']}.php");
				$PLATFORM[ $kP ] = new $P['CLASS_ID']( $D['SESSION']['ACCOUNT_ID'], $kP );
				$PLATFORM[ $kP ]->info($D);
				$CONTROL[ $kP ] = new control($D['SESSION']['ACCOUNT_ID'],$kP); #Platform Manager
			}
		}
		#Lade aktive Platformen Ende ================

		
	}
/*	else {
		#echo "{$D['SESSION']['ACCOUNT_ID']} Account ist nicht aktive.";
		header("Location: index.php?D[PAGE]=login&D[ACTION]=logout");
	}
*/
}
else {
	$MAIN->get_setting($D); #Einstellungen
}


//$smarty->force_compile = true;
#$smarty->compile_check = true;
$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 120;
$smarty->template_dir = $D['ACCOUNT_ID']=='admin'?'view/admin/template/':($D['ACCOUNT_ID']=='www' || $D['ACCOUNT_ID']==''?'view/main/template/':'view/account/template/');
$smarty->compile_dir = "data_tmp/template_c/";

$smarty->loadPlugin('smarty_compiler_switch');
$smarty->registerFilter('post', 'smarty_postfilter_switch');


$my_security_policy = new Smarty_Security($smarty);

#ToDo: file_get_contents ist keien Sichere Funktion da es Zugrif auf lokale Dateien erlaubt. Es muss eine abgespeckte Version der CFILE Klasse zur verfügung gestelt wertden.
#file_get_contents nur auf URLs erlauben nicht auf URIs

$my_security_policy->php_functions = ['print_r','serialize','COUNT','file_get_contents','substr','json_decode','array_key_exists','array_merge_recursive','array_diff_key','str_pad','strtotime','date','ceil','array_keys','current','count','strlen','strtolower','number_format','md5','in_array','is_array','time','nl2br','print_r','array_key_first','strpos','strrpos','str_replace','isset','empty','sizeof','trim','explode','implode'];

$my_security_policy->php_modifiers = ['in_array','strlen','strstr','array_keys','count','COUNT','number_format'];#Smarty PHP Erweiteurngen
$my_security_policy->streams = null;
$my_security_policy->secure_dir = ['core/'];
#$smarty->enableSecurity($my_security_policy); #Securite ist in feed.php aktiviert!


$smarty->assign('CWP', $CWP );
$smarty->assign('CFILE', $CFile );

#==========DB==================
#ToDo: Prüfen, die SQL Abfrage finden weiter oben Statt. Und ist session Account Id immer verfügbar, auch bei Cronjob?
$CData = new wp\CData( [	'DB'		=> ['FILENAME' => "data/ACCOUNT/{$D['SESSION']['ACCOUNT_ID']}/data.db" ],
						'PATTERN'	=> $D['PATTERN'],
						'BACKUP'	=> ["DestinationPath" => "data/ACCOUNT/{$D['SESSION']['ACCOUNT_ID']}/backup/", 'BackupPassword' => null],
					] );

		