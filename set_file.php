<?php
include('!config.php');

if(!$D['SESSION']['ACCOUNT_ID']) exit('Kein Zugriff!'); #Hotfix Schutz vor fremd zugriff.

$CFile->copy($_FILES['files'],"data_tmp/ACCOUNT/{$D['ACCOUNT_ID']}/data/");
$ID = md5_file("data_tmp/ACCOUNT/{$D['ACCOUNT_ID']}/data/{$_FILES['files']['name']}");
$CFile->move("data_tmp/ACCOUNT/{$D['ACCOUNT_ID']}/data/{$_FILES['files']['name']}","data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM']['W']['ID']}/FILE/".date("Ym")."/{$ID}.jpg");


$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['FILE']['D'][ $ID ] = [
	'ACTIVE'	=> 1,
	'NAME'		=> "{$ID}.jpg",
	'SIZE'		=> filesize("data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM']['W']['ID']}/FILE/".date("Ym")."/{$ID}.jpg"), #Byte
];
$PLATFORM[ $D['PLATFORM']['W']['ID'] ]->set_file($D);

#$s = $_SESSION['upload_progress_'.intval($_GET['PHP_SESSION_UPLOAD_PROGRESS'])];
$progress = [
	'file_id' => $ID,
	#'lengthComputable' => true,
	#'loaded' => $s['bytes_processed'],
	#'total' => $s['content_length'],
];

exit(json_encode($progress));