<?php
ini_set('max_execution_time', 100);
#$D = array_merge((array)$_SESSION['D'],(array)$_REQUEST['D']);
include('!config.php');
$SYSTEM->get_i18n($D); #i18n Texte

#FILE Export
$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FILE']['W']['ID'] = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FEED']['W']['ID:IN'];

$PLATFORM[ $D['PLATFORM_ID'] ]->get_file($D);
foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FILE']['D'] AS $kFILE => $FILE) {
	$D['SOURCE']['FILE'] = "data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM_ID']}/FILE/{$FILE['URL']}";
	$D['RETURN']['FILE'] = "{$FILE['NAME']}";
	$D['RETURN']['ATTACHMENT'] = 1;
	$CFile->stream($D);
}

#OR Feed Export / Import

#Exportiere Feed wenn kein File mit Hash gefunden wurde

#Smarty Security
$smarty->enableSecurity($my_security_policy);

$PLATFORM[ $D['PLATFORM_ID'] ]->get_feed($D);
foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FEED']['D'] AS $kFEED => $FEED) {

	if($FEED['KEY'] == $D['KEY'] && $FEED['ACTIVE']) {

		$smarty->assign('D',$D);
		#Classes
		##$C['SYSTEM'] = $SYSTEM;
		##$C['MAIN'] = $MAIN; #Darf nicht zur verfügung gestellt werden! Aufgrund des Zugriffs auf andere Accounts
		$C['ACCOUNT'] = $ACCOUNT;
		$C['PLATFORM'] = $PLATFORM;
		$C['CONTROL'] = $CONTROL;#new control($WP['ACCOUNT']['ID'],$D['PLATFORM_ID']);
		#$C['CFile'] = $CFile; #ToDo: Funktionen Einschränken. Kein Zugriff auf Dateien System!!
		$C['CMail'] = $CMail;
		#$C['WP'] = $CWP; #Aus sicherheitsgründen entfernt
		$C['CSExt'] = $CSExt; #Stellt Zusätzliche Funktionen für das Smarty Template bereit
		$smarty->assign('C',$C);
		
		$smarty->fetch('extends:include/input.tpl');

		if(strpos($FEED['TEXT'],"<!--SET-XML-PDF>") !== false ) {
			preg_match_all("=<!--SET-XML-PDF>[^>](.*)</SET-XML-PDF-->=siU", $FEED['TEXT'], $_SubCode);
			preg_replace("=<!--SET-XML-PDF>[^>](.*)</SET-XML-PDF-->=siU", "", $FEED['TEXT']); #Lösche raus
			$D['SOURCE']['CONTANT-SUB'] = $smarty->fetch('string:'.$_SubCode[1][0]);
		}

		$D['SOURCE']['CONTANT'] = $smarty->fetch('string:'.$FEED['TEXT']);
		$D['RETURN']['FILE'] = $D['FILENAME'];

		switch($D['FILETYPE']) {
			case 'import':
			case 'io':
				break;
			case 'csv':
			case 'txt':
				$D['RETURN']['ATTACHMENT'] = 1;
				$CFile->stream($D);
				break;
			case 'pdf':
				if($D['SOURCE']['CONTANT']) {
					#$dompdf = new Dompdf\DOMPDF(); #wird bereits in !config deklariert
					#$dompdf->set_paper(array(0,0,169,95));
					/*
					$dompdf->loadHtml($D['SOURCE']['CONTANT']);
					$dompdf->set_option('isRemoteEnabled', TRUE);
					$dompdf->getOptions()->setIsFontSubsettingEnabled(true);
					$dompdf->render();
					$dompdf->stream("{$D['RETURN']['FILE']}.pdf",['Attachment'=>0]);
*/
					#https://mpdf.github.io/configuration/configuration-v7-x.html
				
					if($D['SOURCE']['CONTANT-SUB']) {
						$mpdf = new \Mpdf\Mpdf([
							'PDFA' => true,
							'PDFAauto' => true,
						]);
						$mpdf->SetAssociatedFiles([[
							'name' => "zugferd-invoice.xml", #Das ist erlaubter Name, darf nicht geändert werdenfür Rechnungen!
							'mime' => 'text/xml',
							#'description' => 'some description',
							'AFRelationship' => 'Alternative',
							#'path' => __DIR__ . '/../data/xml/test.xml',
							'content' => $D['SOURCE']['CONTANT-SUB'],
						]]);
						#RDF File: ToDo: Separat als content angeben
						$rdf  = '<rdf:Description rdf:about="" xmlns:zf="urn:ferd:pdfa:CrossIndustryDocument:invoice:1p0#">'."\n";
						$rdf .= '  <zf:DocumentType>INVOICE</zf:DocumentType>'."\n";
						$rdf .= '  <zf:DocumentFileName>ZUGFeRD-invoice.xml</zf:DocumentFileName>'."\n";
						$rdf .= '  <zf:Version>1.0</zf:Version>'."\n";
						$rdf .= '  <zf:ConformanceLevel>BASIC</zf:ConformanceLevel>'."\n";
						$rdf .= '</rdf:Description>'."\n";
						
						$mpdf->SetAdditionalXmpRdf($rdf);
						
					}

					$mpdf->WriteHTML($D['SOURCE']['CONTANT']);
					$mpdf->Output();

				}
				else {
					$D['RETURN']['ATTACHMENT'] = 1;
					$CFile->stream($D);
				}
				break;
			default: #xml, html, jpg
				$CFile->stream($D);
				break;
		}
	}
	else {
		echo "Wrong key or Feed is disabled.";
	}
}