{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'get_article'}
		<ul id="myTab" role="tablist" class="nav nav-pills" style="position:absolute;background:#fff;">
			<li class="nav-item">
				<a class="nav-link active" href="#tab0" data-toggle="tab">Artikel</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#tab1" data-toggle="tab">SET</a>
			</li>
		</ul>
		<div style="height:20px;"></div>
<div class="tab-content" id="myTabContent">
	<div id="tab0" class="tab-pane fade show active" >
		<table class="table">
			<tr>
				<th></th>
				<th style="width:50px;">ID</th>
				<th style="width:50px;">Bild</th>
				<th style="width:100px">Nummer</th>
				<th>Title</th>
				<th style="width:50px;">Bestand</th>
			</tr>
		{foreach from=$PLA.ARTICLE.PARENT.D[{NULL}].CHILD.D key="kART" item="ART"}
			{$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)}
			{if !$PLA.ARTICLE.D[$kART].SET && !$first_VAR.SET}
				{$TITLE = $CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,$RCS)}
				<tr style="cursor:pointer;{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}background:#ffdede;color:red;{/if}{if $PLA.ARTICLE.D[$kART].SET}background:#d4eaff;color:blue;{/if}">
					<td><button class="btn" type="button" onclick="{str_replace('ID',"'{$kART}'",$D.DONE)}"><+</button>
						<input name="artcheckbox" type="checkbox" value="{$kART}">
					</td>
					<td>{$kART}</td>
					<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg?{$ART.UTIMESTAMP}'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg?{$ART.UTIMESTAMP}') center no-repeat;"></div></td>
					<td>{$ART.NUMBER}</td>
					<td>{$TITLE}</td>
					<td title="<table class='table'>
							{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
								{if $ART.WAREHOUSE.D[$kWAR]}
									<tr>
										<td colspan=2>{$WAR.TITLE}</td>
									</tr>
									{foreach from=$WAR.STORAGE.D key="kSTO" item="STO"}
										<tr>
											<td>{$STO.TITLE}</td>
											<td style='text-align:right;'>{$ART.WAREHOUSE.D[$kWAR].STORAGE.D[ $kSTO ].STOCK*1}</td>
										</tr>
									{/foreach}
								{/if}
							{/foreach}
							</table>">{$ART.STOCK}</td>
				</tr>
				{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
					<tr style="cursor:pointer;{if $PLA.ARTICLE.D[ $PLA.ARTICLE.D[$kVAR].PARENT_ID ].SET}background:#eff6fd;color:blue;{/if}">
						<td><button class="btn" type="button" onclick="{str_replace('ID',"'{$kVAR}'",$D.DONE)}"><+</button>
							<input name="artcheckbox" type="checkbox" value="{$kVAR}">
						</td>
						<td>{$kVAR}</td>
						<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_200x200.jpg?{$VAR.UTIMESTAMP}'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg?{$VAR.UTIMESTAMP}') center no-repeat;"></div></td>
						<td>{$VAR.NUMBER}</td>
						<td>
							{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
							{for $i=0 to count($aVAR)-1}
								{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
							{/for}
						</td>
						<td title="<table class='table'>
							{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
								{if $VAR.WAREHOUSE.D[$kWAR]}
									<tr>
										<td colspan=2>{$WAR.TITLE}</td>
									</tr>
									{foreach from=$WAR.STORAGE.D key="kSTO" item="STO"}
										<tr>
											<td>{$STO.TITLE}</td>
											<td style='text-align:right;'>{$VAR.WAREHOUSE.D[$kWAR].STORAGE.D[ $kSTO ].STOCK*1}</td>
										</tr>
									{/foreach}
								{/if}
							{/foreach}
							</table>">{$VAR.STOCK}</td>
					</tr>
				{/foreach}
			{/if}
		{/foreach}
		</table>
	</div>
	{*set*}
	<div id="tab1" class="tab-pane fade">
			<table class="table">
				<tr>
					<th></th>
					<th style="width:50px;">ID</th>
					<th style="width:50px;">Bild</th>
					<th style="width:100px">Nummer</th>
					<th>Title</th>
					<th style="width:50px;">Bestand</th>
				</tr>
			{foreach from=$PLA.ARTICLE.PARENT.D[{NULL}].CHILD.D key="kART" item="ART"}
				{$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)}
				{if $PLA.ARTICLE.D[$kART].SET || $first_VAR.SET}
					{$TITLE = $CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,$RCS)}
					<tr style="cursor:pointer;{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}background:#ffdede;color:red;{/if}background:#d4eaff;color:blue;">
						<td><button class="btn" type="button" onclick="{str_replace('ID',"'{$kART}'",$D.DONE)}"><+</button></td>
						<td>{$kART}</td>
						<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg?{$ART.UTIMESTAMP}'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg?{$ART.UTIMESTAMP}') center no-repeat;"></div></td>
						<td>{$ART.NUMBER}</td>
						<td>{$TITLE}</td>
						<td>{$ART.STOCK}</td>
					</tr>
					{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
						<tr style="cursor:pointer;{if $PLA.ARTICLE.D[ $PLA.ARTICLE.D[$kVAR].PARENT_ID ].SET}background:#eff6fd;color:blue;{/if}">
							<td><button class="btn" type="button" onclick="{str_replace('ID',"'{$kVAR}'",$D.DONE)}"><+</button></td>
							<td>{$kVAR}</td>
							<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_200x200.jpg?{$VAR.UTIMESTAMP}'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg?{$VAR.UTIMESTAMP}') center no-repeat;"></div></td>
							<td>{$VAR.NUMBER}</td>
							<td>
							
								{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
								{for $i=0 to count($aVAR)-1}
									{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
								{/for}
							</td>
							<td>{$VAR.STOCK}</td>
						</tr>
					{/foreach}
				{/if}
			{/foreach}
			</table>
	</div>
</div>
	{/case}
	{default}
	<form>
		<div class="input-group mb-1">
			<div class="input-group-prepend">
				<input id="search" class="form-control" title="Suche in:<br>Nummer<br>EAN<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben">
				<button class="btn btn-outline-secondary" onclick="wp.ajax({
														'url'	:	'?D[PAGE]=popup.search&D[ACTION]=get_article',
														'data'	:	{
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}'
																		,'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|EAN:LIKE|TITLE:LIKE]' : '%'+$('#search').val()+'%'
																		{if $PLA.ARTICLE.W['SUPPLIER_ID:IN']},'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][SUPPLIER_ID:IN]' : '{$PLA.ARTICLE.W['SUPPLIER_ID:IN']}'{/if}
																		,'D[DONE]' : '{str_replace("'","\\'",$D.DONE)}'
																	},
														'div'	:	'artSearchResult',
														'ASYNC'	: 1
														})" type="button">Suche</button>
			</div>
		</div>
		<div id="artSearchResult" style="overflow-y:scroll;height:300px;"></div>
		<div style="color:red;">* Rot Markierte Artikel sind Vater Artikel und haben keinen Bestand!</div>
		<div style="color:blue;">* Blau sind SET Artikel und bekommen Bestand von Grund Artikel verärbt</br>
	</form>
	<script>
			SAVEARTSEARCH = function()
			{
				$('#tab0 input[type=checkbox]').each(function(){
					if($(this).is(':checked'))
					{
						aid = $(this).val();
						{str_replace('ID',"aid",$D.DONE)};
					}
				});
			}
		</script>
{/switch}
