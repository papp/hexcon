<?php
switch($D['ACTION'])
{
	case 'download':
		$CFile->remove("tmp/{$D['PLATFORM_ID']}_download");
		$CFile->remove("tmp/{$D['PLATFORM_ID']}_download.zip");
		foreach((array)$D['ZIP'] AS $k => $v)
		{
			$pathinfo = pathinfo("{$v['URL']}");
			$v['URL'] = str_replace(' ','%20',$v['URL']);
			$CFile->copy($v['URL'], "tmp/{$D['PLATFORM_ID']}_download/INCOMINGINVOICE/{$pathinfo['basename']}");
		}
		$CFile->gezip("tmp/{$D['PLATFORM_ID']}_download/","tmp/{$D['PLATFORM_ID']}_download.zip");
		$CFile->remove("tmp/{$D['PLATFORM_ID']}_download");
		#$CFile->stream([ 'SOURCE' => [ 'FILE' => "tmp/{$D['PLATFORM_ID']}_download.zip" ], 'RETURN' => [ 'FILE' => "{$D['PLATFORM_ID']}_download.zip" ] ]);
		#$CFile->remove("tmp/{$D['PLATFORM_ID']}_download.zip");
		exit;
		break;
	case 'set_incominginvoice':
		$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_incominginvoice($D);
		exit;
		break;
	default:
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_incominginvoice($D);
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['W']['TYPE'] = 'INCOMINGINVOICE';
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_group($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_payment($D);

		#Fix: Gruppe
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['D'] = [
			'office'		=> [ 'TITLE' => "Büro"],
			'import_tax'	=> [ 'TITLE' => "Einfuhrsteuer"],
			'buying'		=> [ 'TITLE' => "Einkauf"],
			'salary'		=> [ 'TITLE' => "Gehalt"],
			'others'		=> [ 'TITLE' => "Sonstiges"],
			'tax'			=> [ 'TITLE' => "Steuer"],
			'shipping'		=> [ 'TITLE' => "Versand"],
		];
		break;
}
$smarty->assign('D',$D);
$smarty->display('platform.incominginvoice_list.tpl');
