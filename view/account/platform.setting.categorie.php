<?php
switch ($D['ACTION'])
{
	case "get_categorie":
		if($D['PLATFORM_ID'])
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);
		break;
	case "save":
	case "set_categorie":
		$PLATFORM[ $D['PLATFORM_ID'] ]->set_categorie($D);
		exit();
		break;
	case "add_attribute":
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_language($D);
		break;
	default:
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_language($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);

		#Gib alle Parent Kategorien aus
		$_CAT_ID = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'];#aktuelle ID
		$_PARENT_IDs = str_replace('|',"','",$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['D'][ $_CAT_ID]['BREADCRUMB_ID']);
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = $_PARENT_IDs;
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);
		##$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ATTRIBUTE']['W']['CATEGORIE_ID'] = "'{$_PARENT_IDs}','{$_CAT_ID}'";
		##$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);

		#Gib alle Verknüpfte Kategorien von Fremdplatformen aus
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['D'] AS $kCAT => $CAT) {
			#To Platform
			foreach((array)$CAT['PLATFORM']['D'] AS $kToPLA => $ToPLA) {
				#$_PLATFORM['D'][ $kToPLA ]['CATEGORIE_IDS'] .= (($_PLATFORM['D'][ $kToPLA ]['CATEGORIE_IDS'])?"','":'').implode("','",array_keys((array)$ToPLA['CATEGORIE']['D']));#TO Categorie ID
				$_PLATFORM['D'][ $kToPLA ]['CATEGORIE_IDS'] = array_merge((array)$_PLATFORM['D'][ $kToPLA ]['CATEGORIE_IDS'],array_keys((array)$ToPLA['CATEGORIE']['D']));#TO Categorie ID
			}
		}

		#Durchlaufe alle relevanten Platformen und hole Categorien raus
		foreach((array)$_PLATFORM['D'] AS $kPLA => $vPLA) {
			$_IDs = implode("','",$vPLA['CATEGORIE_IDS']);
			
			$D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['CACHE'] = 1;
			$D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['W']['ID'] = $_IDs;
			
			$PLATFORM[ $kPLA ]->get_categorie($D);
			
			
			foreach($vPLA['CATEGORIE_IDS'] AS $k => $v) {#Gib alle Parent IDs
				$_PIDs .= (($_PIDs)?"','":'').str_replace('|',"','", $D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['D'][ $v ]['BREADCRUMB_ID']);#Gib alle Parent IDs
			}
			$D['PLATFORM']['D'][ $kPLA ]['ATTRIBUTE']['W']['CATEGORIE_ID'] = "'{$_PIDs}','{$_IDs}'";
			$PLATFORM[ $kPLA ]->get_attribute($D);
		}
		
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = $_CAT_ID;

		break;
}

$smarty->assign('D',$D);
$smarty->display('extends:platform.setting.categorie.tpl|include/input.tpl');