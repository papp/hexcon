<?php
switch ($D['ACTION'])
{
	/*
	case 'upload':
		$CFile->copy($_FILES['files'],'tmp');
		$ID = (str_replace('.','',microtime(true)));
		#$ID = uniqid();
		$ID = md5_file("tmp/{$_FILES['files']['name'][0]}");
		$CFile->move("tmp/{$_FILES['files']['name'][0]}","data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM_ID']}/FILE/".date("Y/m")."/{$ID}.jpg");
		
		
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FILE']['D'][ $ID ] = array(
		'ACTIVE'	=> 1,
		'NAME'		=> "{$ID}.jpg",
		'SIZE'		=> 10,
		);
		$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_file($D);
		
		#$s = $_SESSION['upload_progress_'.intval($_GET['PHP_SESSION_UPLOAD_PROGRESS'])];
		$progress = array(
			'file_id' => $ID,
			#'lengthComputable' => true,
			#'loaded' => $s['bytes_processed'],
			#'total' => $s['content_length'],
			);
		
		exit(json_encode($progress));
		break;
	*/
	case 'load_set_article':
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
		break;
	case 'load_article_var':
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);
		break;
	case '_set_attribute':
	case 'attribute_ddl':
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);
		break;
	case "save":
	case "set_article":
		#SET automatisch zur Variation Start ============
		if($D['AUTOSET_GENERATE']) #ToDo: Diese Überprüfung ist überflüssig, wird nur zur Betha Phase eingeführt
		{
			foreach((array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'] AS $kA => $A)
			{
				if($A['VARIANTE_GROUP_ID'] == '' && !$A['PARENT_ID']) #Es darf nur ein Artikel sein der noch keine Variationen beinhaltet
				{
					#Gib die zugeordnete SET Artikel 
					$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['W']['ID:IN'] =  "'".implode("','",array_keys((array) $A['SET']['ARTICLE']['D']))."'";
					$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($d);
					
					#Variation Namen beim Parent hinterlegen
					#dreht das Array um, damit die reihenfolge der Bilder und Atribute umgedreht werden bzw. vom ersten Artikel genohmen wird.
					$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']['']['CHILD']['D'] = array_reverse($d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']['']['CHILD']['D'],true);

					foreach((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']['']['CHILD']['D'] AS $kSA => $vSA)
					{
						if($vSA['VARIANTE_GROUP_ID'] && in_array($kSA,(array)array_keys((array)$A['SET']['ARTICLE']['D'])) )
						{
							$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['VARIANTE_GROUP_ID'] = ($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['VARIANTE_GROUP_ID']? $vSA['VARIANTE_GROUP_ID'].'|'.$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['VARIANTE_GROUP_ID'] :$vSA['VARIANTE_GROUP_ID']);
						
							if($D['AUTOSET_GENERATE_PIC'])
							{
								$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D'] = array_replace_recursive(
									(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D'],
									(array)$vSA['FILE']['D']
								);
							}

							$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['ATTRIBUTE']['D'] = array_replace_recursive(
								(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['ATTRIBUTE']['D'],
								(array)$vSA['ATTRIBUTE']['D']
							);
						}
					}
					
					if($D['AUTOSET_GENERATE_PIC'])
					{
						#Setzt Sort nachträglich für File
						$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D'] = array_reverse($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D']);
						foreach($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D'] AS $k => $v)
						$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['FILE']['D'][$k]['SORT'] = $sort++;
					}

					if($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['VARIANTE_GROUP_ID'])
					{
						#Lösche Attribute aus dem Artikel die beim anderen Artikel Als VAR-ATT hinterlegt ist
						foreach((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']['']['CHILD']['D'] AS $kSA => $vSA)
						{
							foreach((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$kSA]['CHILD']['D'] AS $kSV => $vSV)
							{
								foreach((array)$vSV['ATTRIBUTE']['D'] AS $kSVATT => $vSVATT)
								{
									if(strpos($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $kA ]['VARIANTE_GROUP_ID'],$kSVATT) !== false && 
										strpos($vSA['VARIANTE_GROUP_ID'],$kSVATT) === false)
										unset($d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$kSA]['CHILD']['D'][$kSV]['ATTRIBUTE']['D'][$kSVATT]);
								}
								
							}
						}
						
						unset($d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']['']);
						#dreht das Array um, damit die reihenfolge der Bilder und Atribute umgedreht werden bzw. vom ersten Artikel genohmen wird.
						$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'] = array_reverse($d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'],true);

						$D['ID'] = $kA;
						rec($D, $d);
					}
					#print_r($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D']);
	#exit;
				
				}
			}
			#exit;
		}
		#SET automatisch zur Variation Ende =============

		$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_article($D);
		#Leert Cashe des Articles nur bilder.
		#echo "tmp/file.{$D['ACCOUNT_ID']}.{$D['PLATFORM_ID']}.*_*.jpg";
		$kART = array_keys((array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D']);
		for($i=0;$i<count($kART);$i++)
			$CFile->remove("tmp/file.{$D['SESSION']['ACCOUNT_ID']}.{$D['PLATFORM_ID']}.{$kART[$i]}_*.jpg");
		/*HOT FIX, da symlinc nicht erkannt wurden muss es per reddir durchlaufen werden
		ToDo: Cache Bereiniger in cronjob übertragen aus Performance gründen. Es kann anhand File Timestamp erkannt werden welche älter als Artikel selbst ist und diese löschen
		*/
		
		if($dh = opendir('tmp/'))
		{
			while (($file = readdir($dh)) !== false)
			{
				if($file != '.' && $file != '..')
				{
					foreach((array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'] AS $kART => $ART)
					{
						if(strpos($file,$kART) !== false)#Lösche alle dateien die Artikel ID beinhaltet
						{
							unlink("tmp/{$file}");
						}
						
						foreach((array)$ART['FILE']['D'] AS $kFIL => $FIL)
						{
							if(strpos($file,$kFIL) !== false && is_link($file)) #Lösche nur symlink der Files aber nicht das Bild selbst, weil sonnst gild die referenz nicht mehr aus anderen Artikel
							{
								unlink("tmp/{$file}");
							}
						}
					}
				}
			}
		}
		#======================

		
		#Hotfix:
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'] AS $kART => $ART) {
			if(isset($ART['ACTIVE'])) {
				$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['Active'] = $ART['ACTIVE'];
			}
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['ParentId'] = $ART['PARENT_ID'];
		}
		##$CData->set_object_reqursive($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->set_object($D);
		#$ACCOUNT->set_data('WAREHOUSE',$D);

		#$D = $ACCOUNT->set_warehouse($D);
		#$D = $account->set_supplier($D);
		
		exit();
		break;
	default:
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);

		$d['F']['PLATFORM']['W'][0]['ID'] = $D['PLATFORM_ID'];
		$d['F']['PLATFORM']['WAREHOUSE']['W'][0]['Active'] = '1';
		$d['F']['PLATFORM']['WAREHOUSE']['STORAGE']['W'][0]['Active'] = 1;
		$d['F']['PLATFORM']['SUPPLIER']['W'][0]['Active'] = '1';
		$d['F']['PLATFORM']['ARTICLE']['W'][0]['ID'] = array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ][ 'ARTICLE' ]['D']);
		$d['F']['PLATFORM']['SUPPLIER']['ARTICLE'] = null;
		$d['F']['PLATFORM']['WAREHOUSE']['STORAGE']['ARTICLE'] = null;
		#$CData->get_object_reqursive($d);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_object($d,$d['F']);
		
		#ToDo: alte get_artikle Methode ist nicht mit get_object_reqursive kompatible, daher ein WorkArround!
		if(isset($d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['WAREHOUSE']))
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['WAREHOUSE'] = $d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['WAREHOUSE'];
		if(isset($d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['SUPPLIER']))
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['SUPPLIER'] = $d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['SUPPLIER'];
		
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'] AS $kART => $ART) {
			if(isset($d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['SUPPLIER']))
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['SUPPLIER'] = $d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['SUPPLIER'];
			##$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['STORAGE'] = $d['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$kART]['STORAGE'];
		}

		
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_language($D);
		####$system->get_language($D);
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_group($D);
		
		###$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = implode("','",array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] ]['CATEGORIE']['D']));
		###$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] ]['ATTRIBUTE']['D']['CategorieId']['VALUE'];
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);

		#Gibt Attribute von Ober Kategorien aus
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['D'] AS $kCAT => $CAT)
			$IDs .= str_replace('|',"','",$CAT['BREADCRUMB_ID']);
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = $IDs;
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);
		#ToDo: doppleter aufruff von get_categotrie. Der Filter kann zusammen geführt werden!

		#Hollt alle IDS von Parent Categorien um die über liegende Attribute mit zu erfassen
		###foreach($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] ]['CATEGORIE']['D'] AS $kCAT => $CAT)
		###	$CAT_IDs .= (($CAT_IDs)?",":'' )."'{$kCAT}','".str_replace('|',"','",$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['D'][ $kCAT ]['BREADCRUMB_ID'])."'";
		###$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ATTRIBUTE']['W']['ID'] = "SELECT attribute_id FROM wp_attribute_to_categorie WHERE categorie_id IN ({$CAT_IDs})  ";
		#ToDo: Hotfix: Der Filter von Attributen soll überarbeitet werden
		###$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ATTRIBUTE']['W']['ID'] .= ") OR id IN ('".implode("','",array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] ]['LANGUAGE']['D']['DE']['ATTRIBUTE']['D']))."'";
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);
		
		#Gib alle Verknüpfte Kategorien von Fremdplatformen aus
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['D'] AS $kCAT => $CAT)
		{
			#To Platform
			foreach((array)$CAT['PLATFORM']['D'] AS $kToPLA => $ToPLA)
			{
				$_PLATFORM['D'][ $kToPLA ]['CATEGORIE_IDS'] = implode("','",array_keys((array)$ToPLA['CATEGORIE']['D']));#TO Categorie ID
			}
		}
		
		#gib alle SET Artikel=============
		#$_W = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']; 
		#$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W'] = NULL;
		foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'] AS $kART => $ART)
		{
			foreach((array)$ART['SET']['ARTICLE']['D'] AS $kSET_ART => $SET_ART)
			{
				$D['SET']['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID:IN'] .= ( $D['SET']['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID:IN'] ?",":'')."'{$kSET_ART}'";
			}
		}
		if($D['SET']['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W'])
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D['SET']); #ToDo: Hotfix das $D['SET'] statt nur $D misbraucht wird. 
		#$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W'] = $_W;
		#==================================
		
		#Durchlaufe alle relevanten Platformen und hole Categorien raus
		foreach((array)$_PLATFORM['D'] AS $kPLA => $vPLA)
		{
			if($vPLA['ACTIVE']) {
				#$_IDs = implode("','",(array)$vPLA['CATEGORIE_IDS']);
				$_IDs = $vPLA['CATEGORIE_IDS'];
				#echo $kPLA.print_r($_IDs,1).'<br>';
				$D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['CACHE'] = 1;
				$D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['W']['ID'] = $_IDs;
				
				$PLATFORM[ $kPLA ]->get_categorie($D);
				$PLATFORM[ $kPLA ]->get_language($D);
				$_aIDs = explode("','",$vPLA['CATEGORIE_IDS']);
				foreach((array)$_aIDs AS $k => $v) #Gib alle Parent IDs
				{
					$_PIDs .= (($_PIDs)?"','":'').str_replace('|',"','", $D['PLATFORM']['D'][ $kPLA ]['CATEGORIE']['D'][ $v ]['BREADCRUMB_ID']);#Gib alle Parent IDs
				}
				$D['PLATFORM']['D'][ $kPLA ]['ATTRIBUTE']['W']['CATEGORIE_ID'] = "'{$_PIDs}','{$_IDs}'";
				$PLATFORM[ $kPLA ]->get_attribute($D);
			}
		}
		

		break;
}

$smarty->assign('D',$D);
$smarty->display('extends:platform.article.tpl|include/input.tpl');


#Reqursive erstellung von SETs
function rec(&$D,&$d,$next = 0, $ID = null)
{
	$_E = array_keys((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D']);

	foreach((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$next]]['CHILD']['D'] AS $kV => $vV)
	{
		$id = ((isset($ID))?$ID.'|':'').$kV;#$v['NAME'];
		if(count($_E) > $next+1)
		{
			rec($D,$d,$next+1, $id);
		}
		else
		{
			usleep(10000);
			$VID =  base_convert( microtime(true) ,10,36);
			#Variation erstellen
			$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ] = [
				'PARENT_ID' => $D['ID'],
				'VAT' => $vV['VAT'],
			];
			$IDs = explode('|',$id);
			for($i=0; $i <= $next; $i++) #Holt von x Artikel je Variation die Attribute
			{
				if($D['AUTOSET_GENERATE_PIC'])
				{
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'] = array_replace_recursive(
						(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'],
						(array)array_reverse($d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$i]]['CHILD']['D'][ $IDs[$i] ]['FILE']['D'])
					);
				}
				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['ATTRIBUTE']['D'] = array_replace_recursive(
					(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['ATTRIBUTE']['D'],
					(array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$i]]['CHILD']['D'][ $IDs[$i] ]['ATTRIBUTE']['D']
				);

				#Preis errechnung
				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['PRICE'] +=
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $D['ID'] ]['SET']['ARTICLE']['D'][ $_E[$i] ]['QUANTITY'] * 
					$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$i]]['CHILD']['D'][ $IDs[$i] ]['PRICE'];
				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['RPRICE'] +=
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $D['ID'] ]['SET']['ARTICLE']['D'][ $_E[$i] ]['QUANTITY'] * 
					$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$i]]['CHILD']['D'][ $IDs[$i] ]['RPRICE'];
				
				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['SORT'] += ($next+1)*($i+1)*$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['PARENT']['D'][$_E[$i]]['CHILD']['D'][ $IDs[$i] ]['SORT'];

				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['SET']['ARTICLE']['D'][ $IDs[$i] ] = [
					'ACTIVE' => 1,
					'QUANTITY' => $D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $D['ID'] ]['SET']['ARTICLE']['D'][ $_E[$i] ]['QUANTITY']
				];
			}
			
			#Fix Artikel ohne Variation, Bilder übernehmen
			foreach((array)$d['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'] AS $kVP => $vVP)
			{
				if($vVP['PARENT_ID'] == '' && $vVP['VARIANTE_GROUP_ID'] == '')
				{
					if($D['AUTOSET_GENERATE_PIC'])
					{
						$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'] = array_replace_recursive(
							(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'],
							(array)array_reverse($vVP['FILE']['D'])
						);
					}

					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['ATTRIBUTE']['D'] = array_replace_recursive(
						(array)$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['ATTRIBUTE']['D'],
						(array)$vVP['ATTRIBUTE']['D']
					);
					#Preis
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['PRICE'] +=
						$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $D['ID'] ]['SET']['ARTICLE']['D'][ $kVP ]['QUANTITY'] * 
						$vVP['PRICE'];
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['RPRICE'] +=
						$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $D['ID'] ]['SET']['ARTICLE']['D'][ $kVP ]['QUANTITY'] * 
						$vVP['RPRICE'];
				}
			}

			

			if($D['AUTOSET_GENERATE_PIC'])
			{
				#Setzt Sort nachträglich für File
				$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'] = array_reverse($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D']);
				foreach($D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'] AS $k => $v)
					$D['PLATFORM']['D'][$D['PLATFORM_ID']]['ARTICLE']['D'][ $VID ]['FILE']['D'][$k]['SORT'] = $sort++;
			}
		}
	}
}