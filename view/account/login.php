<?php
switch($D['ACTION'])
{
	case 'login':#1. User Login
		if($D['ACCOUNT']['D'][ $D['SESSION']['ACCOUNT_ID'] ]['ACTIVE'] && $D['USER']['W']['NICKNAME'] && $D['PASSWORD']) {
			$d['USER']['W']['NICKNAME'] = $D['USER']['W']['NICKNAME'];
			$ACCOUNT->get_user($d);
			$User = array_keys((array)$d['USER']['D']);

			$USER_ID = $User[0];

			if($USER_ID) {
				if($d['USER']['D'][$USER_ID]['ACTIVE'] && password_verify($D['PASSWORD'], $d['USER']['D'][$USER_ID]['PASSWORD']) ) {
					$_SESSION['D']['SESSION']['USER'] = $d['USER']['D'][$USER_ID];
					$_SESSION['D']['SESSION']['USER']['ID'] = $USER_ID;
					$D['USER']['D'][$USER_ID]['LOGIN_FAIL'] = 0;
					$ACCOUNT->set_user($D);
					header("Location: index.php");
				}
				else {
					$D['USER']['D'][$USER_ID]['LOGIN_FAIL'] ++;
					$ACCOUNT->set_user($D);
				}
			}
		}
		break;
	case 'logout':
		$_SESSION['D'] = null;
		break;
	case 'password_forgotten':
		#1. ToDo: user nick und sicherheitscode prüfen
		#2. email mit zurück setzen Link versenden
		break;
	case 'password_reset':
		#3. Passwort zurück setzen auf vor generierte Passwort.
		break;
}
#echo password_hash('test',PASSWORD_DEFAULT);
#if( password_verify('test', '$2y$10$BPQIf5Rg5jrOLARUHbxmteu3yZlvxoEgCzJWYmzp7hafS5ESFnUrW') )echo 'OK';
$smarty->assign('D',$D);
$smarty->display('extends:login.tpl|include/input.tpl');