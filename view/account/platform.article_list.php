<?php
	switch ($D['ACTION'])
	{
		case 'copy_article':
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] = $D['ART_ID'];
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);

			#$new_id = 'c'.uniqid();
			$new_id = $CWP::uuid();
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id] = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$D['ART_ID']];
			foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['PARENT']['D'][ $D['ART_ID'] ]['CHILD']['D'] as $k => $v ) {
				#$new_id2 = 'c'.uniqid();
				$new_id2 = $CWP::uuid();
				$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id2] = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$k];
				$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id2]['ACTIVE'] = 0;
				$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id2]['NUMBER'] = null;
				$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id2]['PARENT_ID'] = $new_id;
				unset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id2]['SUPPLIER']);
			}
			
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['ACTIVE'] = 0;
			unset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['NUMBER']);
			unset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['SET']);
			unset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['SUPPLIER']);
			foreach((array)$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['PLATFORM']['D'] AS $kP => $P) {
				unset($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$new_id]['PLATFORM']['D'][$kP]['REFERENCE']);
			}
			$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_article($D);
			exit;
			break;
		case 'del_article':
			$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][$D['ART_ID']]['ACTIVE'] = '-2';
			$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_article($D);
			exit;
			break;
		case 'get_categorie':
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D);
			break;
		case 'set_categorie':
			$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_categorie($D);
			exit;
			break;
		case 'get_article':
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_setting($D);
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
			##$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['CATEGORIE']['W']['ID'] = $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['CATEGORIE_ID'];
			##$PLATFORM[ $D['PLATFORM_ID'] ]->get_categorie($D); 
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_attribute($D);
			$PLATFORM[ $D['PLATFORM_ID'] ]->get_feed($D);
			break;
		default:
			break;
	}
	
#$CWP = new CWP();
#$x['SCALE'] = $CWP::scale(array('START'=>time()-2505600, 'END' => time(), 'UNIT' => 'DAY'));
#echo "<pre>";
#print_r($D['SCALE']);
#$smarty->assign('X',$x);

$smarty->assign('D',$D);
$smarty->display('extends:platform.article_list.tpl|include/input.tpl');