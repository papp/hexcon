<?
switch($D['ACTION'])
{
	case 'load_article':
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['W']['ID'] = $D['ARTICLE_ID'];
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
		#Wen Variation gewählt wurde und es hat keinen eigenen Title, so wird an Parent Title Variattion_ATT angehängt
		$aVAR = explode('|',$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['PARENT_ID'] ]['VARIANTE_GROUP_ID']);
		
		$TITLE = '';
		for($i=0; $i < count($aVAR); $i++)
			$TITLE .= $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['ATTRIBUTETYPE']['D']['ATTRIBUTE']['ATTRIBUTE']['D'][$aVAR[$i]]['LANGUAGE']['D']['DE']['VALUE'] .', ';
		


		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['BUYING']['D'][ $D['BUYING_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['NUMBER'] = 
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['NUMBER'];
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['BUYING']['D'][ $D['BUYING_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['TITLE'] =
		(($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['TITLE'])?$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['TITLE'] : $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['PARENT_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['TITLE']." - {$TITLE}")." | RefNr: {$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['NUMBER']}";
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['BUYING']['D'][ $D['BUYING_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['PRICE'] =
		($D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['PRICE'])?$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['PRICE'] : $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['PARENT_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['PRICE'];
		
		break;
	case 'search_article':
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
		break;
	case "save":
	case 'set_buying':
		$PLATFORM[ $D['PLATFORM_ID'] ]->set_buying($D);
		exit();
		break;
	default:
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_buying($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_payment($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_supplier($D);
		#$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['W']['TYPE'] = 'BUYING';
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_group($D);

		#Fix: Gruppe
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['D'] = [
		#	'office'		=> [ 'TITLE' => "Büro"],
			#'import_tax'	=> [ 'TITLE' => "Einfuhrsteuer"],
			'buying'		=> [ 'TITLE' => "Einkauf"],
			#'salary'		=> [ 'TITLE' => "Gehalt"],
			#'others'		=> [ 'TITLE' => "Sonstiges"],
			#'tax'			=> [ 'TITLE' => "Steuer"],
			#'shipping'		=> [ 'TITLE' => "Versand"],
		];
		break;
}
$smarty->assign('D',$D);
$smarty->display('platform.buying.tpl');