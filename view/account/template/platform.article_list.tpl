{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
	{switch $D.ACTION}
		{case 'get_categorie'}
			{include file="include/tree.tpl" TREE=$PLA.CATEGORIE.D ID='CAT'}
			{*foreach from=$PLA.CATEGORIE.D key="kCAT" item="CAT"}
				<li>
				<label style="cursor:pointer;" onclick="{if $CAT.COUNT}get_categorie('{$kCAT}');{else}get_article('{$kCAT}');{/if}">{if $CAT.COUNT}[+]{else}&nbsp;&nbsp; &nbsp;{/if} {$CAT.LANGUAGE.D.DE.TITLE}</label>
					<ul id="fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kCAT}" style="display:none;"></ul>
				</li>
			{/foreach*}
		{/case}
		{case 'get_article'}
			
{*$y_s = ($smarty.now-2419200)|date_format:"%Y"}
{$y_e = $smarty.now|date_format:"%Y"}
{$m_s = ($smarty.now-2419200)|date_format:"%m"*}
{*$m_e = $smarty.now|date_format:"%m"}
{*$d_s = ($smarty.now-2419200)|date_format:"%d"}
{$d_e = $smarty.now|date_format:"%d"}

{*for $m=($m_s*1) to $m_e}
|{$m}
	{for $d=($d_s*1) to 31}
	:{$d}
	{/for}
{/for*}

{*for $y=($y_s*1) to $y_e}
{for $m=($m_s*1) to $m_e}
{for $d=($d_s*1) to 31}
{if $m < (($smarty.now|date_format:"%m")*1) || ( $m == (($smarty.now|date_format:"%m")*1) && $d <= (($smarty.now|date_format:"%d")*1) ) }
"{if $PLA.ARTICLE.INVOICE.YEAR.D['2015'].MONTH.D['6'].DAY.D[$smarty.section.d.index+1].STOCK > 0}{$PLA.ARTICLE.INVOICE.YEAR.D[$y].MONTH.D[$m].DAY.D[$d].STOCK}{else}0{/if}",
{/if}
{/for}{/for}{/for}
<br>
{*for $y=($y_s*1) to $y_e}{for $m=($m_s*1) to $m_e}{for $d=1 to 31}{if ( $m < (($smarty.now|date_format:"%m")*1) && $d_s*1 <= $d ) || ( $m == (($smarty.now|date_format:"%m")*1) && $d_e*1 > $d-1 ) }"{$d}.{$m}.{$y}",{/if}{/for}{/for}{/for}

{for $y=($y_s*1) to $y_e}{for $m=($m_s*1) to $m_e}{for $d=1 to 31}{if ( $m < (($smarty.now|date_format:"%m")*1) && $d_s*1 <= $d ) || ( $m == (($smarty.now|date_format:"%m")*1) && $d_e*1 > $d-1 ) }"{if $PLA.ARTICLE.INVOICE.YEAR.D[$y].MONTH.D[$m].DAY.D[$d].STOCK > 0}{$PLA.ARTICLE.INVOICE.YEAR.D['2015'].MONTH.D['6'].DAY.D[$smarty.section.d.index+1].STOCK}{else}0{/if}",{/if}{/for}{/for}{/for}

{*section name=d loop=31}"{if $PLA.ARTICLE.INVOICE.YEAR.D['2015'].MONTH.D['6'].DAY.D[$smarty.section.d.index+1].STOCK > 0}{$PLA.ARTICLE.INVOICE.YEAR.D['2015'].MONTH.D['6'].DAY.D[$smarty.section.d.index+1].STOCK}{else}0{/if}",{/section*}

		<nav>
			<ul class="pagination pagination-sm justify-content-end">
				{$STEP = 30}
				{$START = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.L.START}
				{section name=i loop=ceil($PLA.ARTICLE.COUNT.TOTAL/$STEP)}
					<li onclick="wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][CATEGORIE_ID]' : '{$PLA.ARTICLE.W.CATEGORIE_ID}',
											'D[ACTION]' : 'get_article',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][L][START]' : '{$smarty.section.i.index*$STEP}',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][L][STEP]' : '{$STEP}'
										},
							'div'	:	'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'
							});"
					class="page-item {if $smarty.section.i.index == (($START>0)?$START/$STEP:0)}active{/if}"><div class="btn page-link">{$smarty.section.i.index+1}</div></li>
				{/section}
					<li title="Achtung: Es werden alle Artikel dieser Kategorie geladen!" onclick="wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][CATEGORIE_ID]' : '{$PLA.ARTICLE.W.CATEGORIE_ID}',
											'D[ACTION]' : 'get_article',
										},
							'div'	:	'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'
							});"
					class="page-item {if $smarty.section.i.index == (($START>0)?$START/$STEP:0)}disabled{/if}"><div class="btn page-link">ALL</div></li>
			</ul>
		</nav>

			<table id="tbList" class="table table-sm table-hover" style="border-collapse: separate;border-spacing: 0;">
				<thead class="thead-dark">
					<tr>
						<th style="width:50px;"><button class="btn btn-light" type="button" onclick="id = wp.get_genID(); wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}'+id, 'TITLE' : 'Neuer Artikel' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+id+'][ATTRIBUTE][D][CategorieId][VALUE]={$PLA.ARTICLE.W.ATTRIBUTECategorieId}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">+</button></th>
						<th style="width:50px;"><input type="checkbox" onclick="$('input[name=\'cbARTICLE\']').prop('checked', $(this).prop('checked') );"></th>
						<th style="width:50px;"><button class="btn btn-warning" type="button" onclick="btn_edit();">Edit</button></th>
						<th style="width:10px;">ID</th>
						<th style="width:100px;">Nummer</th>
						<th style="width:5px;" title="aktive">A</th>
						<th style="width:500px;">Titel</th>
						<th style="width:50px;text-align:right;">InSET</th>
						<th style="width:50px;text-align:right;">Gewicht</th>
						<th style="width:50px;text-align:right;">Bestand</th>
						<th style="width:50px;text-align:right;">Ø EK</th>
						<th style="width:50px;text-align:right;">VK</th>
						<th>
							<div class="dropdown">
							  <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenu1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fa fa-download"></i>
								<span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								{foreach from=$PLA.FEED.D key="kFEE" item="FEE" name="FEE"}
									<li onclick="window.open('{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEE}/{$FEE.KEY}/{$ART.NUMBER}_{$FEE.FILENAME}.{$FEE.FILETYPE}')">{$FEE.NAME}</li>
								{/foreach}
							  </ul>
							</div>
						</th>
						<th>Update</th>
						<th>Sale</th>
						{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $PLA.ARTICLE.W.CATEGORIE_ID ].TO.PLATFORM.D key="kPL" item="PL" name="PL"}
							{if $D.PLATFORM.D[$kPL].ACTIVE}
							<th style="text-align:right;width:20px;"><img style="width:20px;" src="core/platform/{$D.PLATFORM.D[$kPL].CLASS_ID}/{$D.CLASS[$D.PLATFORM.D[$kPL].CLASS_ID].ICON}" title="{$D.PLATFORM.D[$kPL].TITLE}"></th>
							{/if}
						{/foreach}
					</tr>
				</thead>
				<tbody>
					{foreach from=$PLA.ARTICLE.PARENT.D[{NULL}].CHILD.D key="kART" item="ART"}
						{$ART = $PLA.ARTICLE.D[$kART]}
						{if $ART.FILE.D}
							{$FILE = $ART.FILE.D|@array_keys}
						{else}
							{$FILE = null}
						{/if}
							{*$X['D'] = $D}
							{$POST['POST'] = $CWP->http_build_query($X)*}
							{$RCS['DELIMITER']['LEFT'] = '[('}
							{$RCS['DELIMITER']['RIGHT'] = ')]'}
							{$TITLE = $CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,$RCS)}
							{$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)}
						<tr class="{if $ART.SET.ARTICLE.D || ($PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D && $first_VAR.SET.ARTICLE.D ) }table-primary{/if}" oncontextmenu="wp.contextmenu.open({ ID:'{$kART}', MENU: [ { TITLE: '<b>Öffnen</b>', ONCLICK : 'article_open{$kART}();' }, { CLASS: 'copy', TITLE: 'Copy', ONCLICK : 'copy_article(\'{$kART}\');' }, { CLASS: 'del', TITLE: 'Löschen', ONCLICK : 'del_article(\'{$kART}\');' } ] });">
							<script>
							article_open{$kART} = function()
							{
								wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$kART}', 'TITLE' : '{$ART.NUMBER} | {$TITLE|escape:"html"|truncate:80:"...":false}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kART}'});
							}

							copy_article = function(id)
							{
								if (confirm('Soll dieses Artikel dupliziert werden?'))
								{
									wp.ajax({
										'url'	:	'?D[PAGE]=platform.article_list',
										'data'	:	{
														'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[ART_ID]' : id,
														'D[ACTION]' : 'copy_article',
													},
										'done'	:	function(){  }
										});
								}
							}
							del_article = function(id)
							{
								if (confirm('Dieses Artikel wird komplet mit allen Varianten, Sets, ... gelöscht?'))
								{
									wp.ajax({
										'url'	:	'?D[PAGE]=platform.article_list',
										'data'	:	{
														'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[ART_ID]' : id,
														'D[ACTION]' : 'del_article',
													},
										'done'	:	function(){  }
										});
								}
							}
							</script>
							<td onclick="$('#v{$kART}').toggle();" style="cursor:pointer;"><nobr>{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}▼({$PLA.ARTICLE.PARENT.D[$kART].CHILD.D|@count}){/if} {if $ART.SET.ARTICLE.D || $first_VAR.SET}SET{/if}<nobr></td>
							<td><input type="checkbox" name="cbARTICLE" value="{$kART}">
							{*WARNING START*}
							{$INFO = null}
							{if !$PLA.ARTICLE.PARENT.D[$kART].CHILD.D}{*NUR Artikel ohne VAR*}
								{if !$ART.PRICE}{$INFO[] = "<li>VK-Preis ist nicht hinterlegt</li>"}{/if}
								{if $ART.SUPPLIER.PRICE > $ART.PRICE}{$INFO[] = "<li>EK-Preis ist höher als der VK-Preis</li>"}{/if}
								{if !$ART.VAT}{$INFO[] = "<li>Steuer ist nicht hinterlegt</li>"}{/if}
								{if !$ART.WEIGHT}{$INFO[] = "<li>Gewicht ist nicht hinterlegt</li>"}{/if}
								{if !isset($ART.STOCK) && !$ART.SET}{$INFO[] = "<li>Bestand ist nicht hinterlegt</li>"}{/if}
								{if !count((array)$ART.FILE.D)}{$INFO[] = "<li>Bilder Fehlt.</li>"}{/if}
								{*if !isset($ART.SUPPLIER)}{$INFO[] = "<li>Lieferant ist nicht hinterlegt</li>"}{/if*}
							{else}{*VARIANTE*}
								{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D  key="kVAR" item="VAR" name="VAR"}
									{if !$VAR.PRICE}{$INFO[] = "<li>VARIANTE: VK-Preis ist nicht hinterlegt</li>"}{/if}
									{if $ART.SUPPLIER.PRICE > $VAR.PRICE || $VAR.SUPPLIER.PRICE > $VAR.PRICE}{$INFO[] = "<li>VARIANTE: EK-Preis ist höher als der VK-Preis</li>"}{/if}
									{if !$VAR.VAT}{$INFO[] = "<li>VARIANTE: Steuer ist nicht hinterlegt</li>"}{/if}
									{if !$ART.WEIGHT && !$VAR.WEIGHT}{$INFO[] = "<li>VARIANTE: Gewicht ist nicht hinterlegt</li>"}{/if}
									{if !isset($VAR.STOCK) && !$VAR.SET}{$INFO[] = "<li>VARIANTE: Bestand ist nicht hinterlegt</li>"}{/if}
									{*if !isset($ART.SUPPLIER) && !isset($VAR.SUPPLIER)}{$INFO[] = "<li>VARIANTE: Lieferant ist nicht hinterlegt</li>"}{/if*}
									{if !count((array)$VAR.FILE.D)}{$INFO[] = "<li>VARIANTE: Bilder Fehlt.</li>"}{/if}
								{/foreach}
							{/if}
							{*Prüffe Pflicht Attribute beim Parent*}
							{foreach from=$ART.ATTRIBUTE.D key="kATT" item="ATT"}
							{*foreach from=$PLA.CATEGORIE.ATTRIBUTE.D key="kATT" item="ATT"*}{*ToDo: Att müssen von Categorie übergeben werden, sonnst wird auf überflüssige ATT geprüft*}
								{if $PLA.ATTRIBUTE.D[$kATT].REQUIRED && ( (!$PLA.ATTRIBUTE.D[$kATT].I18N && $ATT.VALUE == '') || ($PLA.ATTRIBUTE.D[$kATT].I18N == 1 && $ATT.LANGUAGE.D['DE'].VALUE == ''))}
									{$INFO[] = "<li>Fehlender Wert beim: {$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE}</li>"}
								{/if}
							{/foreach}

							{*foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D.ATTRIBUTE.D key="kATT" item="ATT"}
								{if $PLA.ATTRIBUTE.D[$kATT].REQUIRED && ( (!$PLA.ATTRIBUTE.D[$kATT].I18N && $ATT.VALUE == '') || ($PLA.ATTRIBUTE.D[$kATT].I18N == 1 && $ATT.LANGUAGE.D['DE'].VALUE == ''))}
									{$INFO[] = "<li>Variante {$VAR.NUMBER}: Fehlender Wert beim: {$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE}</li>"}
								{/if}
							{/foreach*}
							{if !$ART.FILE}{$INFO[] = "<li>Bild ist nicht hinterlegt</li>"}{/if}
							
							{foreach name="CAT" from=$PLA.CATEGORIE.D key="kCAT" item="CAT" name="CAT"}
								{if strpos($PLA.CATEGORIE.D[ $PLA.ARTICLE.W['CATEGORIE_ID'] ].BREADCRUMB_ID,$kCAT) !== false}
									{foreach name="ATT" from=$CAT.ATTRIBUTE.D key="kATT" item="ATT" name="ATT"}
										{if !$ART.ATTRIBUTE.D[$kATT] && $ATT.REQUIRED}
										
											{if $PLA.ATTRIBUTE.D[$kATT]['TOPIN'] != 'child'}
												{$INFO[] = "<li style='background:#ffd;'>{$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE} ist nicht hinterlegt</li>"}
											{else}

												{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D  key="kVAR" item="VAR" name="VAR"}
													{if !$VAR.ATTRIBUTE.D[$kATT]}
														{$INFO[] = "<li style='background:#ffd;'>VARIANTE: {$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE} ist nicht hinterlegt</li>"}
													{/if}
												{/foreach}

											{/if}
										{/if}
									{/foreach}
								{/if}
							{/foreach}
							
							{if $INFO}
								<i style="color:red;" class="fa fa-exclamation-triangle blink" title="<ul>{foreach name="VAR" from=$INFO item="INF" name="INF"}{$INF}{/foreach}</ul>"></i>
							{/if}
							{*WARNING Ende*}
							</td>
							<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_200x200.jpg'>">
								<div style='width:25px;height:100%;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_25x25.jpg") center no-repeat;text-align:right;'><label style="text-shadow: 1px  1px 1px #fff, 1px -1px 1px #fff, -1px 1px 1px #fff, -1px -1px 1px #fff;">{count((array)$ART.FILE.D)}</label></div>
								<!--<img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg?{$FILE[0]}">-->
							</td>
							<td style="cursor:pointer;" title="{$kART}" onclick="navigator.clipboard.writeText('{$kART}');">&#xf0c5;</td>
							<td>
								<div class="btn-group">
							<button class="btn btn-primary btn-sm mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$kART}', 'TITLE' : '{$ART.NUMBER} | {$TITLE|replace:'\'':'\\\''|replace:'"':''|truncate:80:"...":false}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kART}'});"><nobr>{$ART.NUMBER}</nobr></button>
							<button class="btn btn-info btn-sm btn-block mb-0" type="button" onclick="var v{$D.PLATFORM_ID}{$kART} = window.open('?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kART}&D[popup]=1', '{$ART.NUMBER} | {$TITLE|replace:'\'':'\\\''|replace:'"':''|truncate:80:"...":false}' , 'WIDTH=1000px,HEIGHT=620px');">🗔</button>
								</div>
							</td>
							<td style="background:{if $ART.ACTIVE > 0}green{else}red{/if}">{$ART.ACTIVE}</td>
							
							<td>{$TITLE|truncate:80:"":false} | {$TITLE|truncate:80:"...":false|count_characters:true}</td>
							<td {if $ART.INSET.ARTICLE.D}title="<table>
											<tr>
												<td>Nummer</td>
												<td>Summe</td>
											</tr>
										{foreach from=$ART.INSET.ARTICLE.D key="kINSET" item="INSET" name="INSET"}
											<tr>
												<td>{$INSET.NUMBER}</td>
												<td style='text-align:right;'>{$INSET.STOCK}</td>
											</tr>
										{/foreach}
										</table>"{/if} style="text-align:right;">{if $ART.VARIANTE_GROUP_ID}
								{$INSET = 0}
								{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="VAR"}
									{$INSET = $INSET + count((array)$VAR.INSET.ARTICLE.D)}
								{/foreach}
								{$INSET}
							{else}{count((array)$ART.INSET.ARTICLE.D)}{/if}</td>
							<td style="text-align:right;">{$ART.WEIGHT} g</td>
							<td style="text-align:right;" {if count((array)$PLA.ARTICLE.PARENT.D[$kART].CHILD.D) > 0}title="
									<table class='list'>
										<tr>
											<td></td>
											<td>Nummer</td>
											<td>Bestand</td>
											<td>Bestellt</td>
										</tr>
									
									{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="VAR"}
										{$VAR = $PLA.ARTICLE.D[$kVAR]}
										<tr>
											<td>{$smarty.foreach.VAR.iteration}</td>
											<td style='text-align:right;'>{$VAR.NUMBER}</td>
											<td style='text-align:right;'>{if $VAR.STOCK>0}{$VAR.STOCK}{else}<label style='color:red;'>0</label> {/if}</td>
											<td style='text-align:right;'>{if $VAR.SUPPLIER.STOCK>0}{$VAR.SUPPLIER.STOCK}{else}<label style='color:red;'>0</label> {/if}
												{if $VAR.ATTRIBUTE.D['discontinued'].VALUE}<div style='color:red;'>runout</div>{/if}
											</td>
										</tr>
										{$_art_stock[$kART] = $_art_stock[$kART] + $VAR.STOCK}
										{$_art_supp_stock[$kART] = $_art_supp_stock[$kART] + $VAR.SUPPLIER.STOCK}
									{/foreach}
									</table>
									{if $ART.ATTRIBUTE.D['discontinued'].VALUE}<div style='color:red;'>runout</div>{/if}
								"{/if}>
								{if $ART.STOCK > 0 AND !$PLA.ARTICLE.PARENT.D[$kART].CHILD.D}{$ART.STOCK}{elseif $_art_stock[$kART] > 0}∑ {$_art_stock[$kART]}{else}<label style="color:red;">0</label> {/if} 
								{if $ART.SUPPLIER.STOCK}({$ART.SUPPLIER.STOCK}){elseif $_art_supp_stock[$kART]>0}({$_art_supp_stock[$kART]}){/if}
								{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.WAREHOUSE.STOCK > $PLA.ARTICLE.PARENT.D[$kART].CHILD.STOCK}<label title="Reserviert" style="color:orange;">+{$PLA.ARTICLE.PARENT.D[$kART].CHILD.WAREHOUSE.STOCK-$PLA.ARTICLE.PARENT.D[$kART].CHILD.STOCK}</label>{/if}
							</td>
							<td style="text-align:right;" {if count((array)$ART.SUPPLIER.D) > 0}title="
									<table class='list'>
										<tr>
											<td></td>
											<td>Preis</td>
											<td>Bestand</td>
										</tr>
									
									{foreach from=$ART.SUPPLIER.D key="kSUP" item="SUP" name="SUP"}
										<tr>
											<td>{$smarty.foreach.SUP.iteration}</td>
											<td style='text-align:right;'>{$SUP.PRICE|number_format:2:".":""}</td>
											<td style='text-align:right;'>{$SUP.STOCK}</td>
										</tr>
									{/foreach}
									</table>
								"{/if}>{if $ART.SUPPLIER.PRICE}{$ART.SUPPLIER.PRICE|number_format:2:".":""}{else}-{/if}</td>
							<td style="text-align:right;{if $ART.PRICE <= 0 || $ART.SUPPLIER.PRICE >= $ART.PRICE}color:red{/if}">{$ART.PRICE|number_format:2:".":""}</td>
							<td>
								<div class="dropup">
									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-download"></i>
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									{foreach from=$PLA.FEED.D key="kFEE" item="FEE" name="FEE"}
										<li onclick="window.open('{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEE}/{$FEE.KEY}/{$ART.NUMBER}_{$FEE.FILENAME}.{$FEE.FILETYPE}?D[ID]={$kART}')">{$FEE.NAME}</li>
									{/foreach}
									</ul>
								</div>
							</td>
							<td><nobr>{$ART.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
							<td style="text-align:right;" 
							{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="VAR"}
								{$ART.INVOICE.STOCK = $ART.INVOICE.STOCK + $VAR.INVOICE.STOCK}
								{foreach from=$VAR.INVOICE.YEAR.D key="kJ" item="J"}
									{$ART.INVOICE.YEAR.D[ $kJ ].STOCK = $ART.INVOICE.YEAR.D[ $kJ ].STOCK  + $J.STOCK}
									{foreach from=$J.MONTH.D key="kM" item="M"}
										{$ART.INVOICE.YEAR.D[ $kJ ].MONTH.D[ $kM ].STOCK = $ART.INVOICE.YEAR.D[ $kJ ].MONTH.D[ $kM ].STOCK  + $M.STOCK}
									{/foreach}
								{/foreach}

								{foreach from=$VAR.PLATFORM.D key="kPL" item="PL" name="PL"}
									{foreach from=$VAR.PLATFORM.D[$kPL].INVOICE.YEAR.D key="kJ" item="J"}
										{$ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[ $kJ ].STOCK = $ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[ $kJ ].STOCK + $J.STOCK}
										{foreach from=$J.MONTH.D key="kM" item="M"}
											{$ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[ $kJ ].MONTH.D[ $kM ].STOCK = $ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[ $kJ ].MONTH.D[ $kM ].STOCK + $M.STOCK}
										{/foreach}
									{/foreach}
								{/foreach}
							{/foreach}
							{if $ART.INVOICE.YEAR.D}
								title="
									<table class='table table-bordered'>
										<thead>
										<tr>
											<th style='text-align:center;'>Zeit</th>
											{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].FROM.PLATFORM.D key="kPL" item="PL" name="PL"}
												{if $D.PLATFORM.D[$kPL].ACTIVE == 1}
											<th><div style='transform:rotate(180deg);writing-mode:vertical-rl;'><nobr>{$D.PLATFORM.D[$kPL].TITLE}</nobr></div></th>
												{/if}
											{/foreach}
											<th style='text-align:center;'><div style='transform:rotate(180deg);writing-mode:vertical-rl;'>Summe</div></th>
											<th></th>
										</tr>
										</thead>
								{for $y=date('Y') to date('Y')}
									{for $m=1 to date('m')}
									<tr>
										<td>{$y}.{$m}</td>
										{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].FROM.PLATFORM.D key="kPL" item="PL" name="PL"}
											{if $D.PLATFORM.D[$kPL].ACTIVE == 1}
											<td style='text-align:right;'>{if $ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK > 0}<b>{$ART.PLATFORM.D[$kPL].INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK*1}</b>{/if}</td>
											{/if}
										{/foreach}
										<td style='text-align:right;'>{$ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK*1}</td>
										<td>
										{if $m < date('m')}
											{if $m > 1 && $ART.INVOICE.YEAR.D[$y].MONTH.D[{$m-1}].STOCK > $ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}
												{$top=-1}
											{elseif $m > 1 && $ART.INVOICE.YEAR.D[$y].MONTH.D[{$m-1}].STOCK < $ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}
												{$top=1}
											{else}
												{$top = 0}
											{/if}
											{if $top == -1}<b style='color:red;'>▼</b>{elseif $top == 1}<b style='color:green;'>▲</b>{else}={/if}
										{/if}
										</td>
									</tr>
									{/for}
								{/for}
									</table>"
							{/if}
							>{$ART.INVOICE.STOCK}
							{if $top == -1}<b style='color:red;'>▼</b>{elseif $top == 1}<b style='color:green;'>▲</b>{else}={/if}</td>

							{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $PLA.ARTICLE.W.CATEGORIE_ID ].TO.PLATFORM.D key="kPL" item="PL" name="PL"}
								{if $D.PLATFORM.D[$kPL].ACTIVE}
									{$ACTIVE_PL = $ACTIVE_PL +1}
								<td {if 1 || $ART.PLATFORM.D[$kPL].REFERENCE.D|count > 0}title="
									<table class='list'>
										<thead>
											<tr>
												<td></td>
												<td>Referenz</td>
												<td>Fail</td>
												<td>uDatum</td>
												<td>iDatum</td>
												<td>Sale</td>
												<td>AVG</td>
											</tr>
										</thead>
									{$REF_ACTIVE = 1}
									{$REF_ACTIVE1 = 1}
									{$FAIL = 0}
									{foreach from=$ART.PLATFORM.D[$kPL].REFERENCE.D key="kREF" item="REF" name="REF"}
										<tr>
											<td>{$smarty.foreach.REF.iteration}</td>
											<td>{$kREF}</td>
											<td style='{if $REF.FAIL}color:red{/if}'>{$REF.FAIL}</td>
											<td style='{if $REF.ACTIVE == -1}text-decoration:line-through;{$REF_ACTIVE1 = -1}{/if}color:{if $REF.ACTIVE < 1}red{$REF_ACTIVE = 0}{elseif $ART.UTIMESTAMP > $REF.UTIMESTAMP}#db5{else}green{/if}'><nobr>{$REF.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
											<td><nobr>{$REF.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
											<td style='text-align:right;'>{if $REF.ORDER.STOCK}{$REF.ORDER.STOCK}{else}0{/if}</td>
											<td style='text-align:right;'><nobr>{if $REF.ORDER.PRICE}{($REF.ORDER.PRICE/count((array)$REF.ORDER.D))|number_format:2:".":""}{else}0{/if} €</nobr></td>
										</tr>
										{$FAIL = $REF.FAIL+$FAIL}
									{/foreach}
									{$VAR_REFF_COUNT = 0}
									{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="VAR"}
										{$VAR = $PLA.ARTICLE.D[$kVAR]}
										{$VAR_REFF_COUNT = $VAR_REFF_COUNT + count((array)$VAR.PLATFORM.D[$kPL].REFERENCE.D)}
										{if count((array)$VAR.PLATFORM.D[$kPL].REFERENCE.D) > 0}
										<thead>
											<tr>
												<td colspan=5>
												{foreach name="VAR" from=$VAR.LANGUAGE.D['DE'].ATTRIBUTE.D key="kATT" item="ATT" name="ATT"}
													{$ATT.VALUE}
												{/foreach}
												</td>
											</tr>
										</thead>
										{foreach from=$VAR.PLATFORM.D[$kPL].REFERENCE.D key="kREF" item="REF" name="REF"}
										<tr>
											<td>{$smarty.foreach.REF.iteration}</td>
											<td>{$kREF}</td>
											<td style='{if $REF.FAIL}color:red{/if}'>{$REF.FAIL}</td>
											<td style='{if $REF.ACTIVE == -1}text-decoration:line-through;{$REF_ACTIVE1 = -1}{/if}color:{if $REF.ACTIVE < 1}red{$REF_ACTIVE = 0}{elseif $ART.UTIMESTAMP > $REF.UTIMESTAMP}#db5{else}green{/if}'><nobr>{$REF.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
											<td><nobr>{$REF.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
											<td style='text-align:right;'>{if $REF.ORDER.STOCK}{$REF.ORDER.STOCK}{else}0{/if}</td>
											<td style='text-align:right;'><nobr>{if $REF.ORDER.PRICE}{($REF.ORDER.PRICE/count((array)$REF.ORDER.D))|number_format:2:".":""}{else}0{/if} €</nobr></td>
										</tr>
										{$FAIL = $REF.FAIL+$FAIL}
										{/foreach}
										{/if}
									{/foreach}
									</table>
								"{/if}
								 style="text-align:right;{if $REF_ACTIVE1 == -1}text-decoration:line-through;{/if}color:{if $VAR_REFF_COUNT+(count((array)$ART.PLATFORM.D[$kPL].REFERENCE.D)) == 0}#000{elseif !$REF_ACTIVE || $FAIL > 0}red{elseif count((array)$ART.PLATFORM.D[$kPL].REFERENCE) > 0 && $ART.UTIMESTAMP > $ART.PLATFORM.D[$kPL].REFERENCE.MIN_UTIMESTAMP}#db5{else}green{/if}"
								>{if $VAR_REFF_COUNT+count((array)$ART.PLATFORM.D[$kPL].REFERENCE.D) > 0}{$VAR_REFF_COUNT+count((array)$ART.PLATFORM.D[$kPL].REFERENCE.D)}{else}-{/if}</td>
								{/if}
							{/foreach}
						</tr>

						{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
							<tbody id="v{$kART}" style="display:none;border-left:5px solid red;">
							{foreach name="VAR" from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="VAR"}
								{$VAR = $PLA.ARTICLE.D[$kVAR]}
								{if $VAR.FILE.D}
									{$FILE = $VAR.FILE.D|@array_keys}
								{else}
									{$FILE = null}
								{/if}
								<tr oncontextmenu="wp.contextmenu.open({ ID:'{$kART}', MENU: [ { TITLE: '<b>Öffnen</b>', ONCLICK : 'article_open{$kART}();' }, { CLASS: 'copy', TITLE: 'Copy', ONCLICK : 'copy_article(\'{$kVAR}\');' }, { CLASS: 'del', TITLE: 'Löschen', ONCLICK : 'del_article(\'{$kVAR}\');' } ] });" style="background:#fffece;">
									<td style="text-align:right;">{$smarty.foreach.VAR.iteration}</td>
									<td></td>
									<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_200x200.jpg'>">
										<div style="width:25px;height:25px;text-align:right;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_25x25.jpg') center no-repeat;"><label style="text-shadow: 1px  1px 1px #fff, 1px -1px 1px #fff, -1px 1px 1px #fff, -1px -1px 1px #fff;">{count((array)$VAR.FILE.D)}</label></div>
										{*<!--<div style='width:25px;height:25px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_25x25.jpg") center no-repeat;'></div>
										<img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$FILE[0]}_25x25.jpg">-->*}
									</td>
									<td style="cursor:pointer;" title="{$kVAR}" onclick="navigator.clipboard.writeText('{$kVAR}');">&#xf0c5;</td>
									<td style="cursor:pointer;" title="copy" onclick="navigator.clipboard.writeText('{$VAR.NUMBER}');">{$VAR.NUMBER}</td>
									<td style="background:{if $VAR.ACTIVE < 1 OR $ART.ACTIVE < 1}red{else}green{/if}">{$VAR.ACTIVE}</td>
									<td>
									{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
									{for $i=0 to count((array)$aVAR)-1}
										{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
									{/for}
									</td>
									<td {if $VAR.INSET.ARTICLE.D}title="<table>
											<tr>
												<td>Nummer</td>
												<td>Summe</td>
											</tr>
										{foreach from=$VAR.INSET.ARTICLE.D key="kINSET" item="INSET" name="INSET"}
											<tr>
												<td>{$INSET.NUMBER}</td>
												<td style='text-align:right;'>{$INSET.STOCK}</td>
											</tr>
										{/foreach}
										</table>"{/if}
									style="text-align:right;">{count((array)$VAR.INSET.ARTICLE.D)}</td>
									<td style="text-align:right;">{$VAR.WEIGHT} g</td>
									<td style="text-align:right;">
										{if $VAR.STOCK > 0 }{$VAR.STOCK}{else}<label style="color:red;">0</label>{/if}
										{if $VAR.SUPPLIER.STOCK}({$VAR.SUPPLIER.STOCK}){/if}
										{if $VAR.WAREHOUSE.STOCK > $VAR.STOCK}<label title="Reserviert" style="color:orange;">+{$VAR.WAREHOUSE.STOCK-$VAR.STOCK}</label>{/if}
									</td>
									<td style="text-align:right;">{if $VAR.SUPPLIER}{$VAR.SUPPLIER.PRICE|number_format:2:".":""}{elseif $ART.SUPPLIER}{$ART.SUPPLIER.PRICE|number_format:2:".":""}{else}-{/if}</td>
									<td style="text-align:right;">{if $VAR.PRICE > 0 }{$VAR.PRICE|number_format:2:".":""}{/if}</td>
									<td>
										<div class="dropup">
											<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-download"></i>
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
											{foreach from=$PLA.FEED.D key="kFEE" item="FEE" name="FEE"}
												<li onclick="window.open('{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEE}/{$FEE.KEY}/{$ART.NUMBER}_{$FEE.FILENAME}.{$FEE.FILETYPE}?D[ID]={$kVAR}')">{$FEE.NAME}</li>
											{/foreach}
											</ul>
										</div>
									</td>
									<td><nobr>{$VAR.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
									<td style="text-align:right;">{$VAR.INVOICE.STOCK}</td>
									{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $PLA.ARTICLE.W.CATEGORIE_ID ].TO.PLATFORM.D key="kPL" item="PL" name="PL"}
										{if $D.PLATFORM.D[$kPL].ACTIVE}
											<td style="text-align:right;">{if count((array)$VAR.PLATFORM.D[$kPL].REFERENCE.D) > 0}{count((array)$VAR.PLATFORM.D[$kPL].REFERENCE.D)}{else}-{/if}</td>
										{/if}
									{/foreach}
								</tr>
							{/foreach}
							</tbody>
						{/if}
					{/foreach}
				</tbody>
				<tfoot style="position:sticky; bottom:0;z-index:10;">
					<tr>
						<td></td>
						<td></td>
						<script>
						btn_edit = function(){
							var ids = '';
							$('#tbList').find('input[name="cbARTICLE"]:checked').each(function(){
							
								ids += ((ids == '')?'':"','")+$(this).attr('value');
							});
							if(ids != '')
							{
								{if $PLA.ARTICLE.W['CATEGORIE_ID']}
									wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}all', 'TITLE' : 'Alle Artikel' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.articles&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][CATEGORIE_ID]={$PLA.ARTICLE.W['CATEGORIE_ID']}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID:IN]=\''+ids+'\''});
								{else}
									wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}all', 'TITLE' : 'Alle Artikel' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.articles&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID:IN]=\''+ids+'\''});
								{/if}
							}
						}
						</script>
						<td colspan="13" >&nbsp;</td>
						{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $PLA.ARTICLE.W.CATEGORIE_ID ].TO.PLATFORM.D key="kPL" item="PL" name="PL"}
							{if $D.PLATFORM.D[$kPL].ACTIVE}
							<td></td>
							{/if}
						{/foreach}
					</tr>
				</tfoot>
			</table>
			
		{/case}
		{default}
			
			<script>
{*
			/*
				get_categorie = function(parent_id)
				{
					if($('#fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'+parent_id).html() == "")
					{
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][W][PARENT_ID]' : parent_id,
											'D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][CACHE]' : 1,
											'D[ACTION]' : 'get_categorie'
										},
							'div'	:	'fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'+parent_id,
							'done'	:	function(){ $('#fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'+parent_id).slideDown(); }
							});
					}
					else
						$('#fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'+parent_id).slideToggle();
				}

				get_article = function(cat_id)
				{
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][CATEGORIE_ID]' : cat_id,
											'D[ACTION]' : 'get_article'
										},
							'div'	:	'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'
							});
				}
				*/
*}

				new_tree_object = function(key, parent_id, count)
				{
					title = prompt("Ordner Namen eingeben","");
					if(title != "" && title != null)
					{
						id = wp.get_genID();
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D]['+id+'][ACTIVE]=1&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D]['+id+'][PARENT_ID]='+parent_id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D]['+id+'][LANGUAGE][D][DE][TITLE]='+title,
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[ACTION]' : 'set_categorie'
										},
							'done'	:	function(){ /*get_tree_object(key,parent_id, count);*/ }
							});
					}
				}

				remove_tree_object = function(key, parent_id, count)
				{
					if(confirm("Ordner löschen?"))
					{
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D]['+parent_id+'][ACTIVE]=-2',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[ACTION]' : 'set_categorie'
										},
							'done'	:	function(){ /*get_tree_object(key,parent_id, count);*/ }
							});
					}
				}

				rename_tree_object = function(key, parent_id, count)
				{
					title = prompt("Ordner Namen eingeben","");
					if(title != "" && title != null)
					{
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D]['+parent_id+'][LANGUAGE][D][DE][TITLE]='+title,
							'data'	:	{
											//'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[ACTION]' : 'set_categorie'
										},
							'done'	:	function(){ /*get_tree_object(key,parent_id, count);*/ }
							});
					}
				}

				get_tree_object = function(key, parent_id, count)
				{
					if(typeof parent_id === 'undefined')
						parent_id = '';

					if(count > 0 || parent_id == '') //Hat ordner
					{
						if($('#'+key+parent_id).html() == "")
						{
							wp.ajax({
								'url'	:	'?D[PAGE]=platform.article_list',
								'data'	:	{
												//'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
												'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
												'D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][W][PARENT_ID]' : parent_id,
												'D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][CACHE]' : 1,
												'D[ACTION]' : 'get_categorie'
											},
								'div'	:	key+parent_id,
								'done'	:	function(){ $('#'+key+parent_id).slideDown(); }
								});
						}
						else
							$('#'+key+parent_id).slideToggle();

						if(isNaN(document.getElementById('ins'+parent_id+key)))
						{
							document.getElementById('ins'+parent_id+key).className = (document.getElementById('ins'+parent_id+key).className == 'plus')?'minus':'plus';
							document.getElementById('obj'+parent_id+key).className = (document.getElementById('obj'+parent_id+key).className == 'open')?'close':'open';
						}
					}
					else //letzter Ordner
					{
						wp.ajax({
							'url'	:	'?D[PAGE]=platform.article_list',
							'data'	:	{
											//'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											//'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][CATEGORIE_ID]' : parent_id,
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ATTRIBUTECategorieId]' : parent_id, //ToDo: Vorübergehend, muss durch Attribute Filter ersetzt werden
											'D[ACTION]' : 'get_article',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][L][START]' : '0',
											'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][L][STEP]' : '30'
										},
							'div'	:	'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'
							});
					}
				}
			</script>

			
			<table style="width:100%;height:100%;">
				<tr>
					<td style="width:200px;border-right:solid 1px #000;" valign="top">
					{*
						<div id="html" class="demo">
							<ul>
								<li data-jstree='{ "opened" : true }'>Root node
									<ul>
										<li data-jstree='{ "selected" : true }'>Child node 1</li>
										<li>Child node 2</li>
									</ul>
								</li>
							</ul>
						</div>
			
					<script>
					

					$('#html').jstree({
					'core' : { 
						"check_callback" : true,
						'data' : [{ "id":1,"text":"Root node","children":[{ "id":2,"text":"Child node 1"},{ "id":3,"text":"Child node 2"}]}]
						
				
								},

						"plugins" : [ "contextmenu", "dnd" ]
					});
					</script>
					*}

						<div id="fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}"></div>
						<!--<ul id="fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" style="display:none;"></ul>-->
						<script>/*get_categorie('');*/
						get_tree_object('fCAT{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}');</script>



					</td>
					<td valign="top">
						
						<nav class="navbar navbar-light bg-light p-1">
							<form class="form-inline" method="post" name="fsearch" id="fsearch">
								<input name="D[ACTION]" type="hidden" value="get_article">
								<input class="form-control mr-sm-0" onkeydown="$( this ).keypress( function(e){ if(e.which == 13){ SEARCHART();return false;}} );" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|EAN:LIKE|TITLE:LIKE]" value="{$D['PLATFORM']['D'][$D.PLATFORM_ID]['ARTICLE']['W']['NUMBER:LIKE|EAN:LIKE|TITLE:LIKE']}" title="Suche in:<br>Nummer<br>EAN<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben">
								<button class="btn btn-outline-secondary  my-2 my-sm-0" type="button" onclick="SEARCHART();">Suchen</button>
							
							<script>
								SEARCHART = function()
								{
									wp.ajax({
									'url' : '?D[PAGE]=platform.article_list&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
									'div' : 'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
									'data': $('#fsearch').serialize()
									});return false;
								}
							</script>
							</form>
						</nav>
{*
						<form method="post" name="fsearch" id="fsearch">
							<input name="D[ACTION]" type="hidden" value="get_article">
							<div class="container">
								<div class="row justify-content-end">
									<div class="col-md-auto">
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<span class="input-group-text form-control ">Artikel Suche:</span>
											</div>
											<input class="form-control" onkeydown="$( this ).keypress( function(e){ if(e.which == 13){ SEARCHART();return false;}} );" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|EAN:LIKE|TITLE:LIKE]" value="{$D['PLATFORM']['D'][$D.PLATFORM_ID]['ARTICLE']['W']['NUMBER:LIKE|EAN:LIKE|TITLE:LIKE']}" title="Suche in:<br>Nummer<br>EAN<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben">
											<button class="btn btn-outline-secondary" type="button" onclick="SEARCHART();">Suchen</button>
										</div>
									</div>
								</div>
							</div>
							<script>
								SEARCHART = function()
								{
									wp.ajax({
									'url' : '?D[PAGE]=platform.article_list&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
									'div' : 'fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
									'data': $('#fsearch').serialize()
									});return false;
								}
							</script>
						</form>
						*}
							<div id="fART{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}"></div>
						
					</td>
				</tr>
			</table>
		
	{/switch}