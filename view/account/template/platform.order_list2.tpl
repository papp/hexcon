{*foreach from=$D.PLATFORM.D key="kPLA" item="PLA"*}
{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'get_order'}


<button type="button" class="btn" style="background:#fbbac5">Storniert</button>
<button type="button" class="btn" style="background:#fff;">Offen</button>
<button type="button" class="btn" style="background:green">Bezahlt</button>
<button type="button" class="btn" style="background:#bafbfa;">Klärung</button>
<button type="button" class="btn" style="background:#fafbba;">Versandfreigabe</button>
<button type="button" class="btn" style="background:#c3fbba;">Versendet</button>
		-1 Storniert | 0 Offen | 20 Bezahlt | Klärung | Versandfreigabe | 40 Versendet<br>
		{*foreach from=$D.PLATFORM.D key="kPL" item="PL"*}
		
		<table class="table">
			<thead>
				<tr>
					<td>ID</td>
					<td>Platform</td>
					<td>Kunde</td>
					<td>Rechnung</td>
					<td>Status</td>
					<td style="width:60px;">Datum</td>
					<td>Bezahlt</td>
					<td>Versand</td>
					<td>Name</td>
					<td>Nickname</td>
					<td style="width:60px;">Netto</td>
					<td style="width:60px;">MwSt</td>
					<td style="width:60px;">Brutto</td>
				</tr>
			</thead>

			<tbody>
				
					{foreach from=$PLA.ORDER.D key="kORD" item="ORD"}
					<tr>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$kORD}', 'TITLE' : 'Order {$kORD}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.order&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ORDER][W][ID]={$kORD}&D[PLATFORM_TO_ID]={$D.PLATFORM_ID}'});">{$kORD}</button></td>
						<td>{$D.PLATFORM.D[$ORD.PLATFORM_ID].TITLE}</td>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$ORD.CUSTOMER_ID}', 'TITLE' : 'Kunde {$ORD.CUSTOMER_ID}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.customer&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">{$ORD.CUSTOMER_ID}</button></td>
						<td><button class="btn btn-primary btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$ORD.INVOICE_ID}', 'TITLE' : 'Rechnung {$ORD.INVOICE_ID}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&amp;D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$ORD.INVOICE_ID}&&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][ACTIVE]'});"><i class="fa fa-money"></i>{$ORD.INVOICE_ID}</button></td>
						<td>{$ORD.STATUS}</td>
						<td>{$ORD.ITIMESTAMP|date_format:"%d.%m.%Y"}</td>
						<td>{if $ORD.PAID}<div style="color:green">ja{else}<div style="color:red">nein{/if}</div></td>
						<td>{count((array)$ORD.DELIVERY.D)}</td>
						<td>{$ORD.BILLING.FAME} {$ORD.BILLING.NAME}</td>
						<td>{$ORD.CUSTOMER_NICKNAME}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.PRICE|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{($ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)|number_format:2:",":"."}</td>
						{$INVOICE_PRICE = $INVOICE_PRICE + $ORD.ARTICLE.PRICE}
						{$INVOICE_VAT = $INVOICE_VAT + $ORD.ARTICLE.VAT}
						{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + $ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT}
					</tr>
					{/foreach}
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
				</tr>
			</tfoot>
		</table>


	{/case}
	{case 'get_invoice'}

		<table class="table">
			<thead>
				<tr>
					<td>ID</td>
					<td>Kunde</td>
					<td>Status</td>
					<td style="width:60px;">Datum</td>
					<td>Bezahlt</td>
					<td>Versand</td>
					<td>Name</td>
					<td>Nickname</td>
					<td style="width:60px;">Netto</td>
					<td style="width:60px;">MwSt</td>
					<td style="width:60px;">Brutto</td>
				</tr>
			</thead>

			<tbody>
				
					{foreach from=$PLA.INVOICE.D key="kORD" item="ORD"}
					<tr>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : 'gjxuexf9q', 'TITLE' : '12' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.invoice&D[PLATFORM_ID]=platform&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kORD}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][ACTIVE]'});">{$kORD}</button></td>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$ORD.CUSTOMER_ID}', 'TITLE' : 'Kunde {$ORD.CUSTOMER_ID}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.customer&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">{$ORD.CUSTOMER_ID}</button></td>
						<td>{$ORD.STATUS}</td>
						<td>{$ORD.ITIMESTAMP|date_format:"%d.%m.%Y"}</td>
						<td>{if $ORD.PAID}<div style="color:green">ja{else}<div style="color:red">nein{/if}</div></td>
						<td>{count((array)$ORD.DELIVERY.D)}</td>
						<td>{$ORD.BILLING.FAME} {$ORD.BILLING.NAME}</td>
						<td>{$ORD.CUSTOMER_NICKNAME}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.PRICE|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{($ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)|number_format:2:",":"."}</td>
						{$INVOICE_PRICE = $INVOICE_PRICE + $ORD.ARTICLE.PRICE}
						{$INVOICE_VAT = $INVOICE_VAT + $ORD.ARTICLE.VAT}
						{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + $ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT}
					</tr>
					{/foreach}
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
				</tr>
			</tfoot>
		</table>

{function name=pagination p=null}
{$baseURL = basename($smarty.server.REQUEST_URI)}
<nav>
  <ul class="pagination pagination-sm">
	{if ($p.START) > 0}{$START=($p.START-$p.STEP)}{else}{$START=0}{/if}
    <li class="page-item">
      <a class="page-link" href="#" onclick="wp.ajax({
		  										'url'	: '{preg_replace(["~\[STEP\]=([^&]*)~si","~\[START\]=([^&]*)~si"],["[STEP]={$p.STEP}","[START]={$START}"],$baseURL)}',
												'div'	:	'{$p.CONTAINER}','ASYNC': 1
												});return false;">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>


{$MAX = ceil($p.COUNT / $p.STEP)}
{for $i=0 to $MAX-1}
	{$START = $p.STEP*$i}
    <li class="page-item {if $p.START == $p.STEP*$i}active{/if}"><a class="page-link" href="#" onclick="wp.ajax({
												'url'	: '{preg_replace(["~\[STEP\]=([^&]*)~si","~\[START\]=([^&]*)~si"],["[STEP]={$p.STEP}","[START]={$START}"],$baseURL)}',
												'div'	:	'{$p.CONTAINER}','ASYNC': 1
												});return false;">{$i+1}</a></li>
{/for}

{if $p.START+$p.STEP <= $p.COUNT}{$START =($p.START+$p.STEP)}{else}{$START =($p.STEP*$MAX)-$p.STEP}{/if}

    <li class="page-item">
      <a class="page-link" href="#" onclick="wp.ajax({
		  										'url'	: '{preg_replace(["~\[STEP\]=([^&]*)~si","~\[START\]=([^&]*)~si"],["[STEP]={$p.STEP}","[START]={$START}"],$baseURL)}',
												'div'	:	'{$p.CONTAINER}','ASYNC': 1
												});return false;">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>

{/function}

{pagination p=['CONTAINER' => "sale_list{$D.PLATFORM_ID}", 'START'=> $D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.L.START, 'STEP' => $D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.L.STEP, 'COUNT' => $D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.COUNT ]}


	{/case}
	{case 'get_return'}

		<table class="table">
			<thead>
				<tr>
					<td>ID</td>
					<td>Kunde</td>
					<td>Status</td>
					<td style="width:60px;">Datum</td>
					<td>Bezahlt</td>
					<td>Versand</td>
					<td>Name</td>
					<td>Nickname</td>
					<td style="width:60px;">Netto</td>
					<td style="width:60px;">MwSt</td>
					<td style="width:60px;">Brutto</td>
				</tr>
			</thead>

			<tbody>
				
					{foreach from=$PLA.RETURN.D key="kORD" item="ORD"}
					<tr>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : 'Rgjxuexf9q', 'TITLE' : '' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kORD}'});">{$kORD}</button></td>
						<td><button class="btn btn-light btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$ORD.CUSTOMER_ID}', 'TITLE' : 'Kunde {$ORD.CUSTOMER_ID}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.customer&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">{$ORD.CUSTOMER_ID}</button></td>
						<td>{$ORD.STATUS}</td>
						<td>{$ORD.ITIMESTAMP|date_format:"%d.%m.%Y"}</td>
						<td>{if $ORD.PAID}<div style="color:green">ja{else}<div style="color:red">nein{/if}</div></td>
						<td>{count((array)$ORD.DELIVERY.D)}</td>
						<td>{$ORD.BILLING.FAME} {$ORD.BILLING.NAME}</td>
						<td>{$ORD.CUSTOMER_NICKNAME}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.PRICE|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$ORD.ARTICLE.VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{($ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)|number_format:2:",":"."}</td>
						{$INVOICE_PRICE = $INVOICE_PRICE + $ORD.ARTICLE.PRICE}
						{$INVOICE_VAT = $INVOICE_VAT + $ORD.ARTICLE.VAT}
						{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + $ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT}
					</tr>
					{/foreach}
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
				</tr>
			</tfoot>
		</table>

	{/case}
	{default}
	<div class="container-fluid">
	<div class="row">
		<div class="col-1">
			
			<div class="nav flex-column nav-pills" style="position:fixed;">
				{*<a class="nav-link active"  data-toggle="pill" href="#v-pills-home" aria-selected="true">Angebote</a>*}
				<a class="nav-link active" data-toggle="pill" href="#v-pills-profile" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.order_list2&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=get_order&D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][L][STEP]=1000',
												'div'	:	'sale_list{$D.PLATFORM_ID}','ASYNC':1
												});return false;">Aufträge</a>
				<a class="nav-link" data-toggle="pill" href="#v-pills-messages" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.order_list2&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=get_invoice&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][L][STEP]=10&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][L][START]=0',
												'div'	:	'sale_list{$D.PLATFORM_ID}','ASYNC':1
												});return false;">Rechnungen</a>
				<a class="nav-link" data-toggle="pill" href="#v-pills-settings" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.order_list2&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=get_return&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][L][STEP]=1000',
												'div'	:	'sale_list{$D.PLATFORM_ID}','ASYNC':1
												});return false;">Stornorechnungen</a>
			</div>

		</div>
		<div class="col" id="sale_list{$D.PLATFORM_ID}">

		</div>
	</div>
</div>
{/switch}
