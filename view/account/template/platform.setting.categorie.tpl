{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

{function name="cat_att_list" D=null CAT_ID=null}
	{if !$readonly}
		<table class="table">
			<thead>
				<tr>
					<th title="aktive">A</th>
					<th>ID</th>
					<th>Pflicht</th>
					<th>Type</th>
					<th>Attribute</th>
					<th>Option</th>
					<th>►</th>
					{foreach from=$PLA.CATEGORIE.D[ $D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID ].PLATFORM.D key="kToPL" item="ToPL"}
					<th>{$D.PLATFORM.D[$kToPL].TITLE}</th>
					{/foreach}
				</tr>
			</thead>
	{/if}
	{if $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $CAT_ID ].PARENT_ID}
			{cat_att_list D=$D readonly=1 CAT_ID=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $CAT_ID ].PARENT_ID}
	{/if}
		<thead>
			<tr>
				<th colspan={count((array)$PLA.CATEGORIE.D[ $D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID ].PLATFORM.D)+7}><i class="fa fa-folder" style=""></i> {$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $CAT_ID ].LANGUAGE.D['DE'].TITLE}
					<button type="button" onclick="wp.window.open({ 'ID' : '{md5("CAT{$D.PLATFORM_ID}{$CAT_ID}")}', 'TITLE' : 'Kategorie {$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $CAT_ID ].LANGUAGE.D['DE'].TITLE} Einstellungen' , 'WIDTH' : '920px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.setting.categorie&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][W][ID]={$CAT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});" class="btn" title="Einstellungen"></button>
				</th>
			</tr>
		</thead>
			<tbody>
			{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D key="kATT" item="ATT"}
					{if $readonly}
					<tr>
						<td></td>
						<td>{$kATT}</td>
						<td>{if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ]['REQUIRED']}ja{/if}</td>
						
						<td>{$PLA.ATTRIBUTE.D[$kATT].TYPE}</td>
						<td>{$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE}</td>
						<td>
							{if $PLA.ATTRIBUTE.D[$kATT].TYPE == 'select' || $PLA.ATTRIBUTE.D[$kATT].TYPE == 'multiselect'}
								{if $PLA.ATTRIBUTE.D[$kATT].I18N}
									<textarea class="form-control" readonly>{$PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ].LANGUAGE.D['DE'].VALUE_OPTION}</textarea>
								{else}
									<textarea class="form-control" readonly>{$PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ].VALUE_OPTION}</textarea>
								{/if}
							{/if}
						</td>
						<td>►</td>
						{foreach from=$PLA.CATEGORIE.D[ $CAT_ID ].PLATFORM.D key="kToPL" item="ToPL"}
						<td></td>
						{/foreach}
					</tr>
					{else}
					<tr>
						<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" value="{if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ]}1{else}-2{/if}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]').value = this.checked?1:-2" {if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ]}checked{/if}></td>
						<td>{$kATT}</td>
						<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]" value="{$PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ]['REQUIRED']}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]').value = this.checked?1:0" {if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ]['REQUIRED']}checked{/if}></td>
						
						<td>{$PLA.ATTRIBUTE.D[$kATT].TYPE}</td>
						<td>{$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].TITLE}</td>
						<td>
							{if $PLA.ATTRIBUTE.D[$kATT].TYPE == 'select' || $PLA.ATTRIBUTE.D[$kATT].TYPE == 'multiselect'}
								{if $PLA.ATTRIBUTE.D[$kATT].I18N}
									{input p=['type' => 'textarea', 'value' => $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ].LANGUAGE.D['DE'].VALUE_OPTION, 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][DE][VALUE_OPTION]"]}
								{else}
									{input p=['type' => 'textarea', 'value' => $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[ $kATT ].VALUE_OPTION, 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][VALUE_OPTION]"]}
								{/if}
							{/if}
						</td>

						<td>►</td>
						{foreach from=$PLA.CATEGORIE.D[ $CAT_ID ].TO.PLATFORM.D key="kToPL" item="ToPL"}
						<td>
							
							<input id="att2att{$D.PLATFORM_ID}{$kATT}{$kToPL}" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][TO][PLATFORM][D][{$kToPL}][ACTIVE]" value="{if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[$kATT].TO.PLATFORM.D[$kToPL].ACTIVE}1{else}-2{/if}">
							<select class="form-control" onchange="$('#att2att{$D.PLATFORM_ID}{$kATT}{$kToPL}').val(this.value?1:-2)" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$CAT_ID}][ATTRIBUTE][D][{$kATT}][TO][PLATFORM][D][{$kToPL}][ATTRIBUTE_ID]">
								<option value=''></option>
									{foreach from=$D.PLATFORM.D[$kToPL].ATTRIBUTE.D key="kATT2" item="ATT2" name="ATT2"}
										<option {if $PLA.CATEGORIE.D[ $CAT_ID ].ATTRIBUTE.D[$kATT].TO.PLATFORM.D[$kToPL].ATTRIBUTE_ID == $kATT2}selected{/if} value="{$kATT2}" style="{if $D.PLATFORM.D[$kToPL].CATEGORIE.D[$kCAT].ATTRIBUTE.D[$kATT2].REQUIRED}color:red{/if}">{$D.PLATFORM.D[$kToPL].ATTRIBUTE.D[$kATT2].LANGUAGE.D['DE'].TITLE}</option>
									{/foreach}
							</select>
						</td>
						{/foreach}
					</tr>
					{/if}
			{/foreach}
			</tbody>
			
	{if !$readonly}
		</table>
	{/if}


{/function}

{switch $D.ACTION}
	{case 'get_categorie'}
		{if $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.PARENT.D[{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}].CHILD.D}
		►<select onchange="get_categorie('newCAT{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}','{$D.PLATFORM_ID}',this.value)">
			<option></option>
				{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.PARENT.D[{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}].CHILD.D key="kCAT" item="CAT"}
			<option value="{$kCAT}">{$CAT.LANGUAGE.D['DE'].TITLE}</option>
				{/foreach}
		</select>
		<div style="display:inline" id="newCAT{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}">
			{if $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}
				<input type="hidden" name="D[PLATFORM][D][{$D.FROM_PLATFORM_ID}][CATEGORIE][D][{$D.FROM_CATEGORIE_ID}][TO][PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}][ACTIVE]" value="1">
			{/if}
		</div>
		{else}
			<input type="hidden" name="D[PLATFORM][D][{$D.FROM_PLATFORM_ID}][CATEGORIE][D][{$D.FROM_CATEGORIE_ID}][TO][PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.PARENT_ID}][ACTIVE]" value="1">
		{/if}
	{/case}
	{case 'add_attribute'}
	{/case}
	{default}
<form id="form{$D.PLATFORM_ID}CAT{$D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}" method="post">
	<b>Platform Zuordnung:</b><br>
	<ul>
	{*
		{foreach from=$PLA.CATEGORIE.D key="kToCat" item="ToCat"}
			{if $kToCat != $D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}
				<li>
				<b>{$D.PLATFORM.D[$D.PLATFORM_ID].TITLE}</b> ► {str_replace('|',' ► ',$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[$kToCat].LANGUAGE.D['DE'].BREADCRUMB_TITLE)} ► {$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[$kToCat].LANGUAGE.D['DE'].TITLE} | <label class="text-info">{$kToCat}</label>
				{foreach from=$ToCat.PLATFORM.D key="kToPL" item="ToPL"}
						<div class="{if array_key_exists($kToPL,$PLA.CATEGORIE.D[ $D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID ].PLATFORM.D)}text-muted{/if}">
					{foreach from=$ToPL.CATEGORIE.D key="kCAT" item="CAT"}
						<b>{$D.PLATFORM.D[$kToPL].TITLE}</b> ► {str_replace('|',' ► ',$D.PLATFORM.D[$kToPL].CATEGORIE.D[$kCAT].LANGUAGE.D['DE'].BREADCRUMB_TITLE)} ► {$D.PLATFORM.D[$kToPL].CATEGORIE.D[$kCAT].LANGUAGE.D['DE'].TITLE} | <label class="text-info">{$kCAT}</label>
					{/foreach}
						</div>
				{/foreach}
				</li>
			{/if}
			
		{/foreach}
		*}
		<br>
		{foreach from=$PLA.CATEGORIE.D[ $D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID ].PLATFORM.D key="kToPL" item="ToPL"}

			{foreach from=$ToPL.CATEGORIE.D key="kCAT" item="CAT"}
				<li>
					<input id="{$D.PLATFORM_ID}{$D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}{$kToPL}{$kCAT}_ACTIVE" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}][TO][PLATFORM][D][{$kToPL}][CATEGORIE][D][{$kCAT}][ACTIVE]" value="1">
					<button class="btn btn-danger" onclick="$( this ).parent().css('display','none'); $('#{$D.PLATFORM_ID}{$D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}{$kToPL}{$kCAT}_ACTIVE').val('-2');" type="button">x</button> 
					<b>{$D.PLATFORM.D[$kToPL].TITLE}</b> ► {str_replace('|',' ► ',$D.PLATFORM.D[$kToPL].CATEGORIE.D[$kCAT].LANGUAGE.D['DE'].BREADCRUMB_TITLE)} ► {$D.PLATFORM.D[$kToPL].CATEGORIE.D[$kCAT].LANGUAGE.D['DE'].TITLE} | <label class="text-info">{$kCAT}</label>
				</li>
			{/foreach}

		{/foreach}
		<li>
			<select onchange="get_categorie('newCAT_START',this.value,'')">
				<option></option>
				{foreach from=$D.PLATFORM.D key="kPL" item="PL"}
					{if $PL.ACTIVE && $kPL != $D.PLATFORM_ID}
					<option value="{$kPL}">{$PL.TITLE}</option>
					{/if}
				{/foreach}
			</select>
			<div style="display:inline" id="newCAT_START"></div>
		</li>
	</ul>
	<script>
		{$D.FROM_PLATFORM_ID = $D.PLATFORM_ID}
		{$D.FROM_CATEGORIE_ID = $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}
		get_categorie = function(div, kPL, kCAT)
		{
			wp.ajax({
				'url' : '?D[PAGE]=platform.setting.categorie&D[ACTION]=get_categorie&D[PLATFORM_ID]='+kPL+'&D[PLATFORM][D]['+kPL+'][CATEGORIE][W][PARENT_ID]='+kCAT+'&D[FROM_PLATFORM_ID]={$D.FROM_PLATFORM_ID}&D[FROM_CATEGORIE_ID]={$D.FROM_CATEGORIE_ID}',
				'div' : div
			});
		}
	</script>
<div class="alert alert-warning">
Es können Attribute in der Überliegenden Kategorien zugeordnet werden die gelten dann automatisch für alle unterliegenden Kategorien. Z.B. Attribute "Hersteller" kann auf der obersten Kategorie Ebene zugewiesen werden, weil dieses Attribute für alle Artikel dann gelten soll.
</div>



	{*ToDo Funktion um Vater attribute zu öffnen, reqursive bis an die Spitze*}
	
	{cat_att_list D=$D CAT_ID=$D.PLATFORM.D[{$D.PLATFORM_ID}].CATEGORIE.W.ID}

	<b>Verfügbare Attribute</b><br>
	<div style="overflow-y:scroll;height:100px;"">
	<table class="table">
		<thead>
			<tr>
				<th title="aktive">A</th>
				<th>ID</th>
				<th>Pflicht</th>
				<th>Type</th>
				<th>Attribute</th>
				<th>Option</th>
			</tr>
		</thead>
		{foreach from=$PLA.ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATT0" item="ATT0"}
		<tr>
			<td>{$kATT0}</td>
		</tr>
		
			{foreach from=$PLA.ATTRIBUTE.PARENT.D[$kATT0].CHILD.D key="kATT" item="ATT"}
				{if !$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[$kATT]}
					<tr>
						<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" value="{if $PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ]}1{else}-2{/if}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]').value = this.checked?1:-2" {if $PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ]}checked{/if}></td>
						<td>{$kATT}</td>
						<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]" value="{$PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ]['REQUIRED']}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][REQUIRED]').value = this.checked?1:0" {if $PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ]['REQUIRED']}checked{/if}></td>
						<td>{$ATT.TYPE}</td>
						<td>{$ATT.LANGUAGE.D['DE'].TITLE}</td>
						<td>
							{if $ATT.TYPE == 'select' || $ATT.TYPE == 'multiselect'}
								{if $ATT.I18N}
									{input p=['type' => 'textarea', 'value' => $PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ].LANGUAGE.D['DE'].VALUE_OPTION, 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][DE][VALUE_OPTION]"]}
								{else}
									{input p=['type' => 'textarea', 'value' => $PLA.CATEGORIE.D[ $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID ].ATTRIBUTE.D[ $kATT ].VALUE_OPTION, 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][D][{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}][ATTRIBUTE][D][{$kATT}][VALUE_OPTION]"]}
								{/if}
							{/if}
						</td>
					</tr>
				{/if}
			{/foreach}
		{/foreach}
	</table>
	</div>
</form>

<script>
wp.window.bar('{md5("CAT{$D.PLATFORM_ID}{$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID}")}',{ 'Breadcrumb' : '{str_replace('|','/', $D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID].LANGUAGE.D['DE'].BREADCRUMB_TITLE)} / {$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.D[$D.PLATFORM.D[$D.PLATFORM_ID].CATEGORIE.W.ID].LANGUAGE.D['DE'].TITLE}'});
</script>

{/switch}