{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'search_article'}
		<div onclick="document.getElementById('search{$D.return_ID|replace:'-':''}').style.display = 'none';" style="border-bottom:solid 2px #ddd;text-align:center;cursor:pointer;">Schließen</div>
		<table class='list' style='width:100%;'>
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			<tr style="cursor:pointer;" onclick="wp.ajax({ 'div' : 'art_list{$D.return_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.return&D[ACTION]=load_article&D[ARTICLE_ID]={$kART}&D[return_ID]={$D.return_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
			'data' : {
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][NUMBER]' : '{$ART.NUMBER}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][PRICE]' : '-{($ART.PRICE - ($ART.PRICE/(100+$ART.VAT)*$ART.VAT))}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][STOCK]' : '0',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][VAT]' : '{$ART.VAT}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][WEIGHT]' : '{$ART.WEIGHT}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$kART}][TITLE]' : '{$ART.LANGUAGE.D.DE.DESCRIPTION.TITLE}'
					}});
					document.getElementById('search{$D.return_ID|replace:'-':''}').style.display = 'none';">
				<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg"></td>
				<td>{$ART.NUMBER}</td>
				<td>{$ART.LANGUAGE.D.DE.DESCRIPTION.TITLE}</td>
				<td style="text-align:right;">{$ART.PRICE|number_format:2:".":""}€</td>
			</tr>
		{/foreach}
		</table>
		
	{/case}
	{case 'load_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.return_ID}{$D.ARTICLE_ID}">
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button type="button" class="del" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.return_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input style="width:100%;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'>g</td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input style="width:60px;text-align:right;" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input style="width:60px;text-align:right;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
		</tr>
	{/case}
	{case 'load_return_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.return_ID}{$D.ARTICLE_ID}">
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button type="button" class="del" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.return_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input style="width:100%;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'>g</td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input style="width:60px;text-align:right;" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input style="width:60px;text-align:right;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$D.return_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].RETURN.D[{$D.return_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
		</tr>
	{/case}
	{default}
		{foreach from=$PLA.RETURN.D key="kRET" item="RET"}
		
				{*
				<!--CUSTOMER START-->
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][ACTIVE]" value='1'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][NICKNAME]" value='{$RET.CUSTOMER_NICKNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][EMAIL]" value='{$RET.CUSTOMER_EMAIL}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][NAME]" value='{$RET.BILLING.NAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][FNAME]" value='{$RET.BILLING.FNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][COMPANY]" value='{$RET.BILLING.COMPANY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][STREET]" value='{$RET.BILLING.STREET}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][STREET_NO]" value='{$RET.BILLING.STREET_NO}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][ZIP]" value='{$RET.BILLING.ZIP}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][CITY]" value='{$RET.BILLING.CITY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][COUNTRY_ID]" value='{$RET.BILLING.COUNTRY_ID}'>
				<input type='hidden' name="D[CUSTOMER][D][{$RET.CUSTOMER_ID}][ADDITION]" value='{$RET.BILLING.ADDITION}'>
				<!--CUSTOMER END-->
				*}

				<table style="width:100%;">
					<tr>
						<td valign="top">
							<form id="form_R{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kRET}" method="post">
								<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][INVOICE_ID]" value="{$RET.INVOICE_ID}">
								<input type='hidden' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][CUSTOMER_ID]" value='{$RET.CUSTOMER_ID}'>
								<table>
									<tr>
										<td>Storno</td>
										<td>Rechnungsadresse</td>
										<td>Kommentar</td>
									</tr>
									<tr>
										<td style='width:150px;'>
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td>NR:</td>
													<td>{if $RET.NUMBER}{$RET.NUMBER}{else}-neu-{/if}
														<!--<input style='width:50px;text-align:right;' type='hidden' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][NUMBER]" value='{$RET.NUMBER}'>-->
													</td>
												</tr>
												<tr>
													<td>Datum:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][DATE]" value='{if $RET.DATE}{$RET.DATE}{else}{$smarty.now|date_format: "%Y-%m-%d"}{/if}'></td>
												</tr>
												<tr>
													<td>Status:</td>
													<td>
														<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][STATUS]">
															<option value='0' {if $RET.STATUS ==0}selected{/if}>offen</option>
															<option value='20' {if $RET.STATUS ==20}selected{/if}>Versandfreigabe</option>
															<option value='40' {if $RET.STATUS ==40}selected{/if}>Fertig</option>
														</select>
													</td>
												</tr>
												<tr>
													<td>E-Mail:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][CUSTOMER_EMAIL]" value='{$RET.CUSTOMER_EMAIL}'></td>
												</tr>
												<tr>
													<td>UserNick:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][CUSTOMER_NICKNAME]" value='{$RET.CUSTOMER_NICKNAME}'></td>
												</tr>
												<tr>
													<td>Zahlart:</td>
													<td>
														<select title='Zahlungsart' style='width:100px;' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][PAYMENT_ID]">
																<option value=""></option>
															{*foreach from=$PLA.PAYMENT.D key="kPAY" item="PAY"}
																<option value='{$kPAY}' {if $RET.PAYMENT_ID == $kPAY}selected{/if}>{$PAY.TITLE}</option>
															{/foreach*}
															{foreach from=explode("|",$D.PLATFORM.D[$D.PLATFORM_ID].SETTING.D['AvailablePaymentMethods'].VALUE) key="kPAY" item="PAY"}
																<option value='{$PAY}' {if $INV.PAYMENT_ID == $PAY}{$_isPayment=1}selected{/if}>{i18n id="payment_{$PAY}"}</option>
															{/foreach}
															{if !$_isPayment}
																<option value='{$INV.PAYMENT_ID}' selected>{i18n id="payment_{$INV.PAYMENT_ID}"}</option>
															{/if}
														</select>
													</td>
												</tr>
											</table>

										</td>
										<td style='background:#eee;width:150px;'>
											<input style='width:140px;' placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][COMPANY]" value='{$RET.COMPANY}'><br>
											<input style='width:70px;' placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][FNAME]" value='{$RET.FNAME}'><input style='width:70px;' placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][NAME]" value='{$RET.NAME}'><br>
											<input style='width:100px;' placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][STREET]" value='{$RET.STREET}'><input style='width:40px;' placeholder="Straßen Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][STREET_NO]" value='{$RET.STREET_NO}'><br>
											<input style='width:140px;' placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][ADDITION]" value='{$RET.ADDITION}'><br>
											<input style='width:40px;' placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][ZIP]" value='{$RET.ZIP}'><input style='width:100px;' placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][CITY]" value='{$RET.CITY}'><br>
					
											<select style='width:140px;' name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][COUNTRY_ID]">
												<option value=""></option>
												ption value=""></option>
												{foreach from=explode("|",$D.SETTING.D['country_bill'].VALUE) key="kCOU" item="COU"}
													<option value='{$COU}' {if $RET.COUNTRY_ID == $COU}selected{/if}>{i18n id="country_{$COU}"}</option>
												{/foreach}
											</select>
										</td>
										
										
										<td valign="top"><textarea name='D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][COMMENT]' style='width:200px;height:100px;'>{$RET.COMMENT}</textarea>
						
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kRET}][PLATFORM_ID]" value="{$RET.PLATFORM_ID}">
										</td>
									</tr>
								</table>
								<div style="background:#eee;height:20px;padding:2px;width:240px;"><input title="Artikel Title eingeben + '%' für wildcard und '_' für Platzhalter 1 Zeichen" id="tbSearch{$kRET|replace:'-':''}"><button onclick="wp.ajax({ 'div' : 'search{$kRET|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.return&D[ACTION]=search_article&D[return_ID]={$kRET}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][TITLE]='+$('#tbSearch{$kRET|replace:'-':''}').val()+'&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});document.getElementById('search{$kRET|replace:'-':''}').style.display = '';" type="button">Suchen</button></div>
								<div id='search{$kRET|replace:'-':''}' style="position:absolute;min-height:100px;background:#fff; max-height:300px;overflow-y:scroll;border:solid 1px #ddd;display:none;"></div>
								<table class='list' style='width:100%;'>
									<thead>
										<tr>
											<td style="width:10px;"><!--<button type="button" onclick="new_article('{$kRET}');">+</button>--></td>
											<td></td>
											<td style="width:100px;">Nummer</td>
											<td>Title</td>
											<td style="width:60px;">Gewicht</td>
											<td style="width:60px;">Netto</td>
											<td style="width:60px;">MwSt</td>
											<td style="width:60px;">Brutto</td>
											<td style="width:60px;">Stück</td>
										</tr>
									</thead>
									<tbody id="art_list{$kRET|replace:'-':''}">
									{foreach from=$RET.ARTICLE.D key="kART" item="ART"}
										{$D['ACTION'] = 'load_article'}
										{$D['return_ID'] = $kRET}
										{$D['ARTICLE_ID'] = $kART}
										
										{include file="platform.return.tpl" D=$D}

										{$return_PRICE = $return_PRICE + $ART.PRICE*$ART.STOCK}
										{$return_VAT = $return_VAT + $ART.PRICE/100*$ART.VAT*$ART.STOCK}
										{$return_PRICE_BRUTTO = $return_PRICE_BRUTTO + ($ART.PRICE + $ART.PRICE/100*$ART.VAT)*$ART.STOCK}
									{/foreach}
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td style="text-align:right;">{$return_PRICE|number_format:2:".":""}</td>
											<td style="text-align:right;">{$return_VAT|number_format:2:".":""}</td>
											<td style="text-align:right;">{$return_PRICE_BRUTTO|number_format:2:".":""}</td>
											<td></td>
										</tr>
									</tfoot>
								</table>

		


							</form>
						</td>
					<td valign="top">
					
							<script>
							//id = wp.get_genID(); wp.ajax({ 'div' : 'art_list{$kRET}', INSERT : 'append', 'url' : '?D[PAGE]=platform.return&D[ACTION]=load_article&D[ARTICLE_ID]='+id+'&D[return_ID]={$kRET}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});
								new_article = function(IID)
								{
									id = wp.get_genID();
									$('#art_list'+IID).append("<td id='new{$kRET|replace:'-':''}' colspan='9'><input onclick=\"" 
									+"wp.ajax({ 'div' : 'search{$kRET|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.return&D[ACTION]=search_article&D[return_ID]={$kRET}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][TITLE]='+this.value+'&D[return_ID]={$kRET|replace:'-':''}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});"
									+"\" ><div id='search"+IID+"'></div></td>");
								}

								
							</script>
							{*MESSAGE START ================*}
							{if $RET.CUSTOMER_ID}
							<div style="height:300px;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kRET}"></div>
						
								<script>
									wp.ajax({
														'url'	:	'?D[PAGE]=message',
														'data'	:	{
																		'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		'D[MESSAGE][W][GROUP_ID]' : '{$RET.CUSTOMER_ID}', //group_id,
																		'D[MESSAGE][O][DATETIME]' : 'DESC'
																	},
														'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kRET}'
														});
								</script>
							{/if}
						{*MESSAGE END ================*}
						
					</td>
				</tr>
			</table>

				
				<script>

				SAVER{$kRET|replace:"-":"_"} = function()
				{
					wp.ajax({
					'url' : '?D[PAGE]=platform.return&D[ACTION]=set_return&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
					'div' : 'ajax',
					'data': $('#form_R{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kRET}').serialize()
					});
			
				}
				</script>
			{/foreach}
		
{/switch}

