
{*assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]*}
{$LimitStep = 100}
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
		

{function name=BreadCrumb D=null LEVEL = null}
	
	{foreach from=$D.F key="kDAT" item="DAT"}
		
	
		{if $D.PATTERN[$kDAT]}
			<li class="breadcrumb-item"><a href="#" onclick="wp.ajax({
				'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
				'data'	:	{
								{$PFAD = ''}
								{foreach $LEVEL key='kLVL' item='LVL'}
									{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
									'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
								{/foreach}

								'D[F]{$PFAD}[{$kDAT}]' : [null],
								'D[F]{$PFAD}[{$kDAT}][L][START]' : 0,
								'D[F]{$PFAD}[{$kDAT}][L][STEP]' : {$LimitStep},
								{foreach $D.PATTERN[$kDAT].D key='kPAT' item='PAT'}
								'D[F]{$PFAD}[{$kDAT}][{$kPAT}]' : [null],
								'D[F]{$PFAD}[{$kDAT}][{$kPAT}][L][STEP]' : 0,
								{/foreach}
							},
				'div'	:	'fPLA{$D.PLATFORM_ID}',
				'ASYNC'	:	1
				});return false;">{$kDAT}</a>
			</li>
			{if $D.F[$kDAT].W.0.ID}<li class="breadcrumb-item" title="{$D.F[$kDAT].W.0.ID}" onclick="navigator.clipboard.writeText('{$D.F[$kDAT].W.0.ID}');" style="cursor:pointer;">
				{if isset($D[$kDAT].D[ $D.F[$kDAT].W.0.ID ].Title)}{$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ].Title}{else}<i>{$D.F[$kDAT].W.0.ID}</i>{/if}</li>
			{/if}

			{if $D.F[$kDAT] != [null] && $D.F[$kDAT].W.0.ID}
				{*$d = $D[$kDAT]*}
				{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
					{if $D.PATTERN[$kDAT].D[$kNext]}
						{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
						{$d[ $kNext ] = (array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ]}
						{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ]}
					{/if}
				{/foreach}
				{$d.PLATFORM_ID = $D.PLATFORM_ID}{*ToDo: Ggf. kann dies entfernt werden*}
				{$d.PATTERN = $D.PATTERN[$kDAT].D}
				{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
				{BreadCrumb D=$d LEVEL=$LEVEL}
			{/if}
		{/if}
	{/foreach}
{/function}


		<ol class="breadcrumb">
		{BreadCrumb D=$D}
		</ol>


		{function name="Search" D=null LEVEL= null}
			{foreach from=$D key="kDAT" item="DAT"}
				
				{if $D.PATTERN[$kDAT]}
					{if $D.F[$kDAT].W.0.ID}
						{*$d = $D[$kDAT].D[ $D.F[$kDAT].W.0.ID ]*}
						{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
							{if $D.PATTERN[$kDAT].D[$kNext]}
								{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
								{$d[ $kNext ] = (array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ]}
								{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ] }
							{/if}
						{/foreach}
						{$d.PLATFORM_ID = $D.PLATFORM_ID}{*ToDo: Ggf. kann dies entfernt werden*}
						{$d.PATTERN = $D.PATTERN[$kDAT].D}

						{$d.SearchKey = $D.SearchKey}
						{$d.SearchOption = $D.SearchOption}
						{$d.SearchValue = $D.SearchValue}
							
						{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
						{Search D=$d LEVEL=$LEVEL}
					{else}
						{$LEVEL[] = ['TYPE' => $kDAT]} 
						{$PFAD = ''}
						{$PFAD2 = ''}
						{foreach $LEVEL key='kLVL' item='LVL'}
							{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
							{if $LVL.ID}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}][D][{$LVL.ID}]"}{else}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}]"}{/if}
						{/foreach}
						{$LastLevel = end($LEVEL)}
		
						<div class="col-3">
						<div class="input-group mb-1">
						
							
							<select id="SearchKey" class="form-control">
							{foreach from=$D.PATTERN[$LastLevel.TYPE] key="K" item="I"}
								{if $I.Type}
								<option value="{$K}" {if $D.SearchKey == $K}selected{/if} style="{if $I.Type== 'id'}color:red{/if}">{$K}</option>
								{/if}
							{/foreach}
							</select>
							<select id="SearchOption" class="form-control">
							<option value='LIKE%%' {if $D.SearchOption == 'LIKE%%'}selected{/if}>LIKE %%</option>
							<option value='LIKE%-' {if $D.SearchOption == 'LIKE%-'}selected{/if}>LIKE %*</option>
							<option value='LIKE-%' {if $D.SearchOption == 'LIKE-%'}selected{/if}>LIKE *%</option>
							<option value='NOTLIKE' {if $D.SearchOption == 'NOTLIKE'}selected{/if}>NOT LIKE</option>
							<option value='>' {if $D.SearchOption == '>'}selected{/if}>></option>
							<option value='<' {if $D.SearchOption == '<'}selected{/if}><</option>
							<option value='<>' {if $D.SearchOption == '<>'}selected{/if}>!=</option>
							<option value='>%3D' {if $D.SearchOption == '>='}selected{/if}>>=</option>
							<option value='<%3D' {if $D.SearchOption == '<='}selected{/if}><=</option>
							<option value='%3D' {if $D.SearchOption == '='}selected{/if}>=</option>
							</select>
							<input id="SearchValue" type="text" class="form-control" value="{$D.SearchValue}">
							<button type="button" onclick="wp.ajax({
										'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]=this&D[SearchKey]='+$('#SearchKey').val()+'&D[SearchOption]='+$('#SearchOption').val()+'&D[SearchValue]='+$('#SearchValue').val()+'&D[F]{$PFAD}[W][0][' + $('#SearchKey :selected').val() +'][' +$('#SearchOption :selected').val()+ ']='+ $('#SearchValue').val(),
										'data'	:	{
											
													{$PFAD = ''}
													{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
														{if $smarty.foreach.LVL.iteration < count($LEVEL)}
														{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
														'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
														{/if}
													{/foreach}

													'D[F]{$PFAD}' : [null],
													'D[F]{$PFAD}[L][START]' : 0,
													'D[F]{$PFAD}[L][STEP]' : {$LimitStep},
													{foreach $D.PATTERN[$kDAT].D  key='kPAT' item='PAT'}
													'D[F]{$PFAD}[{$kPAT}]' : [null],
													'D[F]{$PFAD}[{$kPAT}][L][STEP]' : 0,
													{/foreach}
														
													},
										'div'	:	'fPLAthis',
										'ASYNC'	:	1
										});return false;
							">S</button>
						</div>
					</div>



					{/if}
				{/if}	
			{/foreach}
		{/function}

		<div class="row">
			{Search D=$D}
		</div>


{function name="thead" D=null LEVEL = null}
	{foreach from=$D key="kDAT" item="DAT"}
		
		{if $D.PATTERN[$kDAT]}
			{if $D.F[$kDAT].W.0.ID}
				{*$d = $D[$kDAT].D[ $D.F[$kDAT].W.0.ID ]*}
				{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
					{if $D.PATTERN[$kDAT].D[$kNext]}
						{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
						{$d[ $kNext ] = (array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ]}
						{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ] }
					{/if}
				{/foreach}
				{$d.PLATFORM_ID = $D.PLATFORM_ID}{*ToDo: Ggf. kann dies entfernt werden*}
				{$d.PATTERN = $D.PATTERN[$kDAT].D}
					
				{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
				{thead D=$d LEVEL=$LEVEL}
			{else}
				{$LEVEL[] = ['TYPE' => $kDAT]} 
				{$PFAD = ''}
				{$PFAD2 = ''}
				{foreach $LEVEL key='kLVL' item='LVL'}
					{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
					{if $LVL.ID}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}][D][{$LVL.ID}]"}{else}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}]"}{/if}
				{/foreach}
				{$LastLevel = end($LEVEL)}

				<th style="width:50px;"><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="id = wp.get_genID();wp.window.open({ 'ID' : '{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'+id, 'TITLE' : id+'' , 'WIDTH' : '500px', 'HEIGHT' : '500px',
				'DATA' : { 
						{$_PFAD=''}
						{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
							{if $smarty.foreach.LVL.iteration < count($LEVEL)}
							{$_PFAD = "{$_PFAD}[{$LVL.TYPE}]"}
							'D[F]{$_PFAD}[W][0][ID]' : '{$LVL.ID}',
							{/if}
						{/foreach}
						'D[F]{$PFAD}[W][0][ID]' : id,
					},
				'URL' : '?D[PAGE]=data&D[PLATFORM_ID]={$D.PLATFORM_ID}&D{$PFAD2}[D]['+id+'][{$LEVEL[count($LEVEL)-2].TYPE}]={$LEVEL[count($LEVEL)-2].ID}'});">+</button></th>
				<th style="width:20px;">ID</th>
				<th style="width:50px;"></th>
				
				{foreach from=$D.PATTERN[ $LastLevel['TYPE'] ] key="kPAT" item="PAT" name="PAT"}
					{if $kPAT != 'D'}
				
					<th style="cursor:pointer;{if $D.F[$kDAT].O[$kPAT]}color:yellow;{/if}"
					onclick="wp.ajax({
						'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
						'data'	:	{
										{$PFAD = ''}
										{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
											{if $smarty.foreach.LVL.iteration < count($LEVEL)}
											{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
											'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
											{/if}
										{/foreach}

										'D[F]{$PFAD}[{$kDAT}]' : [null],
										'D[F]{$PFAD}[{$kDAT}][L][START]' : {if $D.F[$kDAT].L.START}{$D[$kDAT].L.START}{else}0{/if},
										'D[F]{$PFAD}[{$kDAT}][L][STEP]' : {$LimitStep},
										'D[F]{$PFAD}[{$kDAT}][O][{$kPAT}]' : {if $D.F[$kDAT].O[$kPAT] == 'ASC'}'DESC'{else}'ASC'{/if},
										{foreach $D.PATTERN[$kDAT].D  key='kPAT' item='PAT'}
										'D[F]{$PFAD}[{$kDAT}][{$kPAT}]' : [null],
										'D[F]{$PFAD}[{$kDAT}][{$kPAT}][L][STEP]' : 0,
										{/foreach}
										
									},
						'div'	:	'fPLA{$D.PLATFORM_ID}',
						'ASYNC'	:	1
						});return false;"><nobr>{$kPAT} {if $D.F[$kDAT].O[$kPAT] == 'ASC'}&#xf077;{elseif $D.F[$kDAT].O[$kPAT] == 'DESC'}&#xf078;{/if}</nobr></th>
					{/if}
				{/foreach}
				{if $D.PATTERN[ $LastLevel['TYPE'] ].D}
				<th>Child</th>
				{/if}
			{/if}
		{/if}	
	{/foreach}
{/function}

{function name="tfoot" D=null LEVEL = null}
	{foreach from=$D key="kDAT" item="DAT"}
		
		{if $D.PATTERN[$kDAT]}
			{if $D.F[$kDAT].W.0.ID}
				{*$d = $D[$kDAT].D[ $D.F[$kDAT].W.0.ID ]*}
				{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
					{if $D.PATTERN[$kDAT].D[$kNext]}
						{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
						{$d[ $kNext ] = (array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ]}
						{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ] }
					{/if}
				{/foreach}
				{$d.PLATFORM_ID = $D.PLATFORM_ID}{*ToDo: Ggf. kann dies entfernt werden*}
				{$d.PATTERN = $D.PATTERN[$kDAT].D}
					
				{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
				{tfoot D=$d LEVEL=$LEVEL}
			{else}
				{$LEVEL[] = ['TYPE' => $kDAT]} 
				{$PFAD = ''}
				{$PFAD2 = ''}
				{foreach $LEVEL key='kLVL' item='LVL'}
					{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
					{if $LVL.ID}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}][D][{$LVL.ID}]"}{else}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}]"}{/if}
				{/foreach}
				{$LastLevel = end($LEVEL)}

				<td></td>
				<td></td>
				<td> <button class="btn btn-primary m-1" type="button" onclick="SAVE();">Speichern</button> </td>
				<td colspan="{count((array)$D.PATTERN[ $LastLevel['TYPE'] ])}">
				
					
					<nav>
						{$_ActivePage = ($D.F[$kDAT].L.STEP)? ceil($D.F[$kDAT].L.START / $D.F[$kDAT].L.STEP)+1 : 1}
						{$_Pages = ($D.F[$kDAT].L.STEP && isset($D[$kDAT].COUNT))?ceil($D[$kDAT].COUNT/$D.F[$kDAT].L.STEP) : 1}
						<ul class="pagination pagination-sm justify-content-end m-1">
							{if $_Pages > 1}
							<li class="page-item {if ($D.F[$kDAT].L.STEP*($_ActivePage-2)) < 0}disabled{/if}">
								<span class="page-link" style="cursor:pointer;" onclick="wp.ajax({
									'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
									'data'	:	{
													{$PFAD = ''}
													{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
														{if $smarty.foreach.LVL.iteration < count($LEVEL)}
														{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
														'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
														{/if}
													{/foreach}

													'D[F]{$PFAD}[{$kDAT}]' : [null],
													'D[F]{$PFAD}[{$kDAT}][L][START]' : {$D.F[$kDAT].L.STEP*($_ActivePage-2)},
													'D[F]{$PFAD}[{$kDAT}][L][STEP]' : {$D.F[$kDAT].L.STEP},

													{foreach $D.F[$kDAT].O  key='kO' item='O'}
													'D[F]{$PFAD}[{$kDAT}][O][{$kO}]' : '{$O}',
													{/foreach}

													{foreach $D.PATTERN[$kDAT].D  key='kPAT' item='PAT'}
													'D[F]{$PFAD}[{$kDAT}][{$kPAT}]' : [null],
													'D[F]{$PFAD}[{$kDAT}][{$kPAT}][L][STEP]' : 0,
													{/foreach}
												},
									'div'	:	'fPLA{$D.PLATFORM_ID}',
									'ASYNC'	:	1
									});return false;"> &#xf0d9; </span>
							</li>
							{for $page=1 to $_Pages}
								{if $_ActivePage == $page}
									<li class="page-item active"><span class="page-link" style="cursor:pointer;">{$page}</span></li>
								{else}
								<li class="page-item"><span class="page-link" style="cursor:pointer;"
									onclick="wp.ajax({
										'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
										'data'	:	{
														{$PFAD = ''}
														{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
															{if $smarty.foreach.LVL.iteration < count($LEVEL)}
															{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
															'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
															{/if}
														{/foreach}

														'D[F]{$PFAD}[{$kDAT}]' : [null],
														'D[F]{$PFAD}[{$kDAT}][L][START]' : {$D.F[$kDAT].L.STEP*($page-1)},
														'D[F]{$PFAD}[{$kDAT}][L][STEP]' : {$D.F[$kDAT].L.STEP},

														{foreach $D.F[$kDAT].O  key='kO' item='O'}
														'D[F]{$PFAD}[{$kDAT}][O][{$kO}]' : '{$O}',
														{/foreach}

														{foreach $D.PATTERN[$kDAT].D  key='kPAT' item='PAT'}
														'D[F]{$PFAD}[{$kDAT}][{$kPAT}]' : [null],
														'D[F]{$PFAD}[{$kDAT}][{$kPAT}][L][STEP]' : 0,
														{/foreach}
														
													},
										'div'	:	'fPLA{$D.PLATFORM_ID}',
										'ASYNC'	:	1
										});return false;">{$page}</span></li>
								{/if}
							{/for}
							<li class="page-item {if isset($D[$kDAT].COUNT) && ($D.F[$kDAT].L.STEP*($_ActivePage)) >= $D[$kDAT].COUNT}disabled{/if}">
								<span class="page-link" style="cursor:pointer;" onclick="wp.ajax({
									'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
									'data'	:	{
													{$PFAD = ''}
													{foreach $LEVEL key='kLVL' item='LVL' name='LVL'}
														{if $smarty.foreach.LVL.iteration < count($LEVEL)}
														{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
														'D[F]{$PFAD}[W][0][ID]' : '{$LVL.ID}',
														{/if}
													{/foreach}

													'D[F]{$PFAD}[{$kDAT}]' : [null],
													'D[F]{$PFAD}[{$kDAT}][L][START]' : {$D.F[$kDAT].L.STEP*($_ActivePage)},
													'D[F]{$PFAD}[{$kDAT}][L][STEP]' : {$D.F[$kDAT].L.STEP},

													{foreach $D.F[$kDAT].O  key='kO' item='O'}
													'D[F]{$PFAD}[{$kDAT}][O][{$kO}]' : '{$O}',
													{/foreach}

													{foreach $D.PATTERN[$kDAT].D  key='kPAT' item='PAT'}
													'D[F]{$PFAD}[{$kDAT}][{$kPAT}]' : [null],
													'D[F]{$PFAD}[{$kDAT}][{$kPAT}][L][STEP]' : 0,
													{/foreach}
												},
									'div'	:	'fPLA{$D.PLATFORM_ID}',
									'ASYNC'	:	1
									});return false;"> &#xf0da; </span>
							</li>
							{/if}
							<li class="page-item disabled">
								<span class="page-link" style="cursor:pointer;">&#xf3c5; {$_ActivePage} von {$_Pages} &#xf1c0; {if isset($D[$kDAT].COUNT)}{if $LimitStep*$_ActivePage < $D[$kDAT].COUNT}{$LimitStep*$_ActivePage}{else}{$D[$kDAT].COUNT}{/if} / {$D[$kDAT].COUNT}{/if}</span>
							</li>
						</ul>
					</nav>
					
				</td>
				{*foreach from=$D.PATTERN[ $LastLevel['TYPE'] ] key="kPAT" item="PAT" name="PAT"}
					{if $kPAT != 'D'}
					<td>{$kPAT}</td>
					{/if}
				{/foreach}
				{if $D.PATTERN[ $LastLevel['TYPE'] ].D}
				<td>Child</td>
				{/if*}
			{/if}
		{/if}	
	{/foreach}
{/function}

{function name="tbody" D=null LEVEL = null}
	
	{foreach from=$D key="kDAT" item="DAT"}
		{if $D.PATTERN[$kDAT]}
			
			{if $D.F[$kDAT].W.0.ID}
				{*$d = $D[$kDAT].D[$D.F[$kDAT].W.0.ID]*}

				{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
					{if $D.PATTERN[$kDAT].D[$kNext]}
						{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
						{$d[ $kNext ] = $D[$kDAT].D[ $D.F[$kDAT].W.0.ID ][ $kNext ]}
						{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ] }
					{/if}
				{/foreach}
				
				{$d.PLATFORM_ID = $D.PLATFORM_ID}{*ToDo: Ggf. kann dies entfernt werden*}
				{$d.DATASETTING = $D.DATASETTING}
				{$d.PATTERN = $D.PATTERN[$kDAT].D}
				{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
				{tbody D=$d LEVEL=$LEVEL}
			{else}
				{$LEVEL[] = ['TYPE' => $kDAT]}
				
				{foreach $LEVEL key='kLVL' item='LVL'}
					{$PFAD = "{$PFAD}[{$LVL.TYPE}]"}
					{if $LVL.ID}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}][D][{$LVL.ID}]"}{else}{$PFAD2 = "{$PFAD2}[{$LVL.TYPE}]"}{/if}
				{/foreach}
				{$LastLevel = end($LEVEL)}
				
				{if isset($D[$LastLevel.TYPE])}
					{foreach $D[$LastLevel.TYPE].D key='kDATA' item='DATA'}
						{$LEVEL[count((array)$LEVEL)-1 ].ID = $kDATA}

						<tr id="d{$kDATA}">
						<td><input id="dactive{$kDATA}" type="hidden" value="{$DATA.Active}" name="D{$PFAD2}[D][{$kDATA}][Active]">
								<button class="btn btn-danger btn-sm btn-block mb-0" type="button" onclick="$('#d{$kDATA}').hide();$('#dactive{$kDATA}').val('-2');">-</button>
						</td>
								
						<td style="cursor:pointer;" title="{$kDATA}" onclick="navigator.clipboard.writeText('{$kDATA}');"></td>
						<td>
							<button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 
															'ID' : '{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kDATA}', 'TITLE' : '{$kDATA} | {str_replace(["'","\""],["\\'","\'\'"],$DATA.Title)}',
															'WIDTH' : '500px',
															'HEIGHT' : '500px',
															'DATA' : { 

																{$_PFAD=''}
																{foreach $LEVEL key='kLVL' item='LVL'}
																	{$_PFAD = "{$_PFAD}[{$LVL.TYPE}]"}
																	'D[F]{$_PFAD}[W][0][ID]' : '{$LVL.ID}',
																{/foreach}

																{*'D{$PFAD}[W][0][ID]' : '{$kDATA}',*}
																},
															'URL' : '?D[PAGE]={if $D.DATASETTING[$LastLevel.TYPE].TEMPLATE.TPL}{$D.DATASETTING[$LastLevel.TYPE].TEMPLATE.TPL}{else}data{/if}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">{$kDATA}</button>
							
						</td>
					
						{foreach from=$D.PATTERN[ $LastLevel.TYPE ] key="kPAT" item="PAT" name="PAT"}
							{if $kPAT != 'D'}
							<td>{$DATA[$kPAT]}</td>
							{/if}
						{/foreach}
					
						{if isset($D.PATTERN[ $LastLevel.TYPE ].D)}
						<td>
							{foreach from=$D.PATTERN[$LastLevel.TYPE].D key="kPAT" item="PAT" name="PAT"}

								<button type="button" class="btn btn-light" onclick="wp.ajax({
									'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
									'data'	:	{
									
									{$_PFAD=''}
									{foreach $LEVEL key='kLVL' item='LVL'}
										{$_PFAD = "{$_PFAD}[{$LVL.TYPE}]"}
										'D[F]{$_PFAD}[W][0][ID]' : '{$LVL.ID}',
									{/foreach}
									
									'D[F]{$PFAD}[{$kPAT}]' : [null],
									'D[F]{$PFAD}[{$kPAT}][L][START]' : 0,
									'D[F]{$PFAD}[{$kPAT}][L][STEP]' : {$LimitStep},
									
									{foreach from=$PAT.D key="kPAT1" item="PAT1" name="PAT1"}
										'D[F]{$PFAD}[{$kPAT}][{$kPAT1}]' : [null],
										'D[F]{$PFAD}[{$kPAT}][{$kPAT1}][L][STEP]' : 0,
									{/foreach}
									},
										'div'	:	'fPLA{$D.PLATFORM_ID}',
										'ASYNC'	:	1
								});return false;">
								{$kPAT} <span class="badge badge-{if $DATA[$kPAT].COUNT > 0 }dark{else}light{/if}">{$DATA[$kPAT].COUNT}</span>
								</button>

							{/foreach}
						</td>
						{/if}
						</tr>
					{/foreach}
				{/if}
			{/if}
		{/if}	
	{/foreach}
{/function}

	<table class='table table-sm table-hover' style="border-collapse: separate;border-spacing: 0;">
		<thead class="thead-dark" style="position:sticky; top:75px; z-index:10;">
			<tr>
				{thead D=$D}
			</tr>
		</thead>
		<tbody>
			{tbody D=$D}
		</tbody>
		<tfoot style="position:sticky; bottom:0;z-index:10;">
			{tfoot D=$D}
		</tfoot>
	</table>

		
	</form>

		<script>
			SAVE = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=data_list&D[ACTION]=save&D[PLATFORM_ID]={$D.PLATFORM_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize()
				});
			}
		</script>