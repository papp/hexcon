	{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
	{function name="load_article"}
		<div id="tr{$D.STORAGE_ID}{$D.ARTICLE_ID}" class="card m-1" style="width: 20rem;float:left;{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SET}background:#d4eaff{elseif $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].VARIANTE_GROUP_ID}background:red{/if}">
			{*<img class="card-img-top" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg?{$FILE[0]}">*}
			<div class="card-header p-1" style="height:20px;overflow:hidden;">
				<div title="{$CWP->rand_choice_str($D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ]])}">{$CWP->rand_choice_str($D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ]])}</div>
			</div>
			<div class="card-body p-0">

					<div class="row p-0 m-0">
						<div class="col-4 p-0">
							<div style="height:100px;width:100%;background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_160x160.jpg?{$FILE[0]}) center center no-repeat;"></div>
						</div>
						<div class="col-6 p-1">
							<div title="ID: {$D.ARTICLE_ID}">{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].NUMBER}</div>

							<div>
								{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SUPPLIER.D}
									{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SUPPLIER.D  name="SUP" key="kSUP" item="SUP"}
										<span title="Herstellernummer:{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE} {$SUP.NUMBER}" style="font-size:10px;" class="badge badge-warning">{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE}: {$SUP.NUMBER}</span>
									{/foreach}
								{/if}
							</div>
							
							<div style="color:red;">
							{$PART = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$D.ARTICLE_ID].PARENT_ID}
							{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID}
								{$aVAR = explode('|',$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID)}
								{for $i=0 to count($aVAR)-1}
									{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
								{/for}
							{/if}
							</div>
						</div>
						<div class="col-2 p-1">
							{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['discontinued'].VALUE}
							<span style="font-size:10px;" class="badge badge-warning" title="Dieser Artikel wird nicht mehr nachbestellt!">Auslauf</span>
							{/if}
							{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D  name="STO" key="kSTO" item="STO"}
								<span style="font-size:10px;" class="badge {if $D.STORAGE_ID == $kSTO}badge-primary{else}badge-secondary{/if}">{$D.WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D[$kSTO].TITLE} [{$STO.STOCK}]</span><br>
							{/foreach}
						</div>
					</div>
				
			</div>
			<div class="card-footer p-1" style="height:30px;">
				<div style="float:left;">
					<input type="hidden" id="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]" name="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]" value="{$ART.ACTIVE}">
					<button class="btn btn-danger" type="button" title="Löschen" onclick="document.getElementById('D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2'; document.getElementById('tr{$D.STORAGE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
				</div>
				{if !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SET && !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].VARIANTE_GROUP_ID}
					{*<input class="form-control" style="text-align:right;width:50px;display:unset;" name="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]" value="{if $D.STOCK_TRANSFER>0}{$D.STOCK_TRANSFER}{else}{if $ART.STOCK}{$ART.STOCK}{else}0{/if}{/if}">*}
					
					<div style="float:left;">
						{if $D.STOCK_TRANSFER>0}{$_STOCK = $D.STOCK_TRANSFER}{elseif $ART.STOCK>0}{$_STOCK = $ART.STOCK}{else}{$_STOCK = 0}{/if}
						{input p=['name'=>"D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]", 'value'=>$_STOCK, 'style'=>'text-align:right;width:50px;display:unset;']}
					</div>
					<div style="float:right;">
						<button class="btn" type="button" title="Umbuchen" onclick="new_article('{$D.WAREHOUSE_ID}',$('#WAREHOUSE_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}').val(),'{$D.ARTICLE_ID}',$('#STOCK_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}').val() )">-></button>
						<select class="form-control" id="WAREHOUSE_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}" style="width:50px;display:unset;">
							{foreach from=$D.WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D  name="STO" key="kSTO" item="STO"}
							<option value="{$kSTO}">{$STO.TITLE}</option>
							{/foreach}
						</select>
						<input class="form-control" id="STOCK_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}" style="text-align:right;width:50px;display:unset;" value="0">
					</div>
				{else}
					Dieses Artikel kann nicht gelagert werden!
				{/if}
			</div>
		</div>
	{/function}
	
	{switch $D.ACTION}
		{case 'load_article'}
			{load_article D=$D}
		{/case}
		{default}
			
			<form id="form{$D.SESSION.ACCOUNT_ID}STO" method="post">
				<div style="background:#ddd;position:fixed;width:100%;z-index:1">
					<button class="btn btn-primary" type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}();">Speichern</button>

					{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
						{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
						<button class="btn btn-primary" type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W.ID}','_blank');" title="Lager">
						{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
						</button>
						{/if}
					{/foreach}
				</div>
				<div style="height:30px;"></div>
				{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
					{if $WAR.ACTIVE}
						<ul class="nav nav-tabs" id="myTab">
						{foreach from=$WAR.STORAGE.MAP  name="MAP" key="kMAP" item="MAP"}
							<li class="nav-item">
								<a class="nav-link {if $smarty.foreach.MAP.iteration == 1}active{/if}" data-toggle="tab" href="#{$kMAP}">{$kMAP}</a>
							</li>
						{/foreach}
						</ul>
					<div class="tab-content p-1" id="myTabContent">
					{foreach from=$WAR.STORAGE.MAP name="MAP" key="kMAP" item="MAP"}
						<div class="tab-pane fade show {if $smarty.foreach.MAP.iteration == 1}active{/if}" id="{$kMAP}">
							<table class="table table-bordered">
								{*TITLE*}
								<thead>
								<tr>
									<th style="width:10px;text-align:center;background:#ff9;"><b>{$kMAP}</b></th>
									{foreach from=current($MAP) key="kMAP2" item="MAP2"}
										<th style="width:{round(100/count(current($MAP)))}%;text-align:center;background:#ff9;"><b>{$kMAP2}</b></th>
									{/foreach}
								</tr>
								</thead>
								{foreach from=$MAP key="kMAP1" item="MAP1"}
									<tr>
										<td style='width:20px; text-align:center; vertical-align:middle;background:#ff9;'><b>{$kMAP1}</b></td>
										{foreach from=$MAP1 key="kMAP2" item="MAP2"}
											<td>
												<button class="btn" type="button" onclick="wp.window.open({ 'ID' : 'ARTSEARCH', 'TITLE' : 'Artikel Suche' , 'WIDTH' : '600px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=popup.search&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[DONE]=new_article(\'{$kWAR}\',\'{$MAP2.ID}\',ID)'});">+</button>
												
												<input class="form-control" name="D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$MAP2.ID}][TITLE]" style="width:50px;displaY:unset;" value="{$MAP2.TITLE}">

												<div id="list_{$kWAR}{$MAP2.ID}">
												{foreach from=$MAP2.ARTICLE.D key="kART" item="ART"}
													{$D['ACTION'] = 'load_article'}
													{$D['PLATFORM_ID'] = $D.PLATFORM_ID}
													{$D['WAREHOUSE_ID'] = $kWAR}
													{$D['STORAGE_ID'] = $MAP2.ID}
													{$D['ARTICLE_ID'] = $kART}
													{load_article D=$D}
												{/foreach}
												</div>
											</td>
										{/foreach}
									</tr>
								{/foreach}
							</table>
						</div>
					{/foreach}
					</div>
					{/if}
				{/foreach}
				
				<script>
					new_article = function(WID,SID,AID,STOCK)
					{
						if(!AID)
							AID = window.prompt("Bitte hier Artikel ID eingeben","");
					
						if(AID)
						{
							wp.ajax({ 
							'div' : 'list_'+WID+SID, 
							INSERT : 'append', 
							'url' : '?D[PAGE]=warehouse.stocktaking&D[ACTION]=load_article&D[WAREHOUSE_ID]='+WID+'&D[STORAGE_ID]='+SID+'&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]='+AID+'&D[ARTICLE_ID]='+AID+'&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[STOCK_TRANSFER]='+STOCK,
							
							});
						}
					}
					SAVE{$D.SESSION.ACCOUNT_ID} = function()
					{
						wp.ajax({
						'url' : '?D[PAGE]=warehouse.stocktaking&D[ACTION]=set_warehouse&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
						'div' : 'ajax',
						'data': $('#form{$D.SESSION.ACCOUNT_ID}STO').serialize()
						});
					}
				</script>
				
				
			</form>
{/switch}