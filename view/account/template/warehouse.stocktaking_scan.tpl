{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

{switch $D.ACTION}
	{case 'get_article'}
		{foreach from=$PLA.ARTICLE.D name="ART" key="kART" item="ART"}
			{if $ART.NUMBER == $PLA.ARTICLE.W['NUMBER:LIKE']}
				{$kFileFirstID = current(array_keys((array)$ART.FILE.D))}
				<div>{$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}</div>
				<table style="width:100%">
					<tr>
						<td><div style="width:100px;height:100px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFileFirstID}_100x100.jpg') center no-repeat;"></div></td>
						<td valign=top>
							{foreach from=$ART.SUPPLIER.D name="SUP" key="kSUP" item="SUP"}
								<div>{$PLA.SUPPLIER.D[ $kSUP ].TITLE} : {$SUP.NUMBER}</div>
							{/foreach}

							<table class="table" style="text-align:right;">
							{foreach from=$ART.WAREHOUSE.D[$D.WAREHOUSE.W['ID']].STORAGE.D name="STO" key="kSTO" item="STO"}
								{if $D.STORAGE_ID == $kSTO}{$issetSto = 1}{/if}
								<tr id="id_{$D.WAREHOUSE.W['ID']}{$kSTO}{$kART}">
									<td style="font-size:20px;"><button type="button" class="btn btn-danger" onclick="$('#id{$D.WAREHOUSE.W['ID']}{$kSTO}{$kART}_ACTIVE').val(-2); $('#id_{$D.WAREHOUSE.W['ID']}{$kSTO}{$kART}').hide();">-</button> {$D.WAREHOUSE.D[$D.WAREHOUSE.W['ID']].STORAGE.D[$kSTO].TITLE}</td>
									<td>
										<input type="hidden" id="id{$D.WAREHOUSE.W['ID']}{$kSTO}{$kART}_ACTIVE" name="D[WAREHOUSE][D][{$D.WAREHOUSE.W['ID']}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][ACTIVE]" value="{if $STO.STOCK > 0}1{else}-2{/if}">
										<input style="width:60px;text-align:right;font-size:20px;" onkeyup="$('#id{$D.WAREHOUSE.W['ID']}{$kSTO}{$kART}_ACTIVE').val(( $(this).val() != '' )?1:-2)" name="D[WAREHOUSE][D][{$D.WAREHOUSE.W['ID']}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" value="{$STO.STOCK}">
									</td>
								</tr>
							{/foreach}
							{if $D.STORAGE_ID && !$issetSto}
								<tr style="background:yellow">
									<td style="font-size:20px;">{$D.WAREHOUSE.D[$D.WAREHOUSE.W['ID']].STORAGE.D[ $D.STORAGE_ID ].TITLE}</td>
									<td>
									<input type="hidden" id="id{$D.WAREHOUSE.W['ID']}{$D.STORAGE_ID}{$kART}_ACTIVE" name="D[WAREHOUSE][D][{$D.WAREHOUSE.W['ID']}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$kART}][ACTIVE]" value="-2">
									<input style="width:60px;text-align:right;font-size:20px;" onkeyup="$('#id{$D.WAREHOUSE.W['ID']}{$D.STORAGE_ID}{$kART}_ACTIVE').val(( $(this).val() > 0 )?1:-2)" name="D[WAREHOUSE][D][{$D.WAREHOUSE.W['ID']}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$kART}][STOCK]" value="">
									{*input p=['style' => 'width:60px;font-size:20px;', 'name' => "D[WAREHOUSE][D][{$D.WAREHOUSE.W['ID']}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$kART}][STOCK]", 'value'=>""]*}
									</td>
								</tr>
							{/if}
							</table>
						</td>
					</tr>
				</table>
			{/if}
		{/foreach}
		<div style="text-align:right;"><button class="btn btn-primary" style="font-size:20px;width:100px;" id="btnSave" type="button" onclick="saveStorage()">save</button></div>
	{/case}
	{default}
	<style>.bg-light { display:none}</style>
	<form id="form{$D.PLATFORM_ID}">
		<table>
			<tr>
				<td>
					<input style="font-size:20px;" class="form-control p-1 form-control-lg" name="scan" id="scan" placeholder="Barcode" onkeyup="scanBarcode(this.value)">

					<input style="font-size:20px;" class="form-control p-1 form-control-lg" placeholder="LagerortNr" id="storageId" name="D[STORAGE_ID]">
					
					
					

					<div class="input-group">
						<input style="font-size:20px;" class="form-control p-1 form-control-lg" placeholder="ArtikelNr" id="articleNr" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE]">
						<div class="input-group-append">
							<button type="button" class="btn btn-outline-secondary" style="font-size:20px;width:50px;" onclick="loadStorage()">Go</button>
						</div>
					</div>


					<div id="Storage"></div>
				</td>
			</tr>
		</table>
		<script>
			scanBarcode = function(scan)
			{
				if(scan[scan.length-1] == '*') // Ende
				{
					ret = scan.substr(0, scan.length - 1);
					ret = ret.substr(1);
					if(scan[0] == 's') // Regal
					{
						$('#storageId').val(ret);
						$('#scan').val('');
					}
					else if(scan[0] == 'a') // Artikel
					{
						$('#articleNr').val(ret);
						$('#scan').val('');
					}
					
					if( $('#articleNr').val() )
						loadStorage();
					
				}
					
			}

			clearBarcode = function()
			{
				$('#storageId').val('');
				$('#articleNr').val('');
			}

			loadStorage = function()
			{
				if( $('#storageId').val())
				{
					wp.ajax({
						'url' : '?D[PAGE]=warehouse.stocktaking_scan&D[ACTION]=get_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W['ID']}&D[STORAGE_ID]='+$('#storageId').val(),
						'div' : 'Storage',
						'data': $('#form{$D.PLATFORM_ID}').serialize()
					});
				}
				else
				{
					wp.ajax({
						'url' : '?D[PAGE]=warehouse.stocktaking_scan&D[ACTION]=get_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W['ID']}',
						'div' : 'Storage',
						'data': $('#form{$D.PLATFORM_ID}').serialize()
					});
				}
			}

			saveStorage = function()
			{
				btnBg = $('#btnSave').css('background-color');
				$('#btnSave').css('background-color','red');
				wp.ajax({
					'url' : '?D[PAGE]=warehouse.stocktaking_scan&D[ACTION]=set_warehouse&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W['ID']}',
					'div' : 'ajax',
					'done': function(){ $('#btnSave').css('background-color',btnBg); },
					'data': $('#form{$D.PLATFORM_ID}').serialize()
				});
				scan.focus();
				clearBarcode();
			}
		</script>

		
	</form>
{/switch}