{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

{function name="get_delivery" D=null}
{$kINV = $D.INVOICE_ID}
{$kDEL = $D.DELIVERY_ID}
{$readonly = ($DEL.STATUS >= 40)?1:0}
		<tr id="del{$D.PLATFORM_ID}{$kDEL}_main" style="background:{if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].STATUS >=40}green{else if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].STATUS >=20}yellow{else}red{/if}">
			<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][INVOICE_ID]" value="{$kINV}">
			<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ORDER_ID]" value="{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].ORDER_ID}">
		
			<td>
				<button class="btn" onclick="$('#del{$D.PLATFORM_ID}{$kDEL}').toggle();"type="button">▼</button>
				{if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].STATUS < 20}
				<input id='del{$D.PLATFORM_ID}{$kDEL}active' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ACTIVE]'>
				<button class="btn" type="button" onclick="document.getElementById('del{$D.PLATFORM_ID}{$kDEL}active').value = '-2';document.getElementById('del{$D.PLATFORM_ID}{$kDEL}_main').style.display = 'none';document.getElementById('del{$D.PLATFORM_ID}{$kDEL}').style.display = 'none';">-</button>
				{/if}
			</td>
			<td>{$DEL.NUMBER}</td>
			<td>
				<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][STATUS]">
					{foreach from=$D.INVOICE.STATUS.D key="kSTA" item="STA"}
					<option style="background:{$STA.COLOR}" value='{$kSTA}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].STATUS ==$kSTA}selected{/if}>{$STA.TITLE}</option>
					{/foreach}
				</select>
			</td>
			<td>
				{*input p=['type'=>'select', 'option' => [1 => 'a',2 => 'b' ] ]*}
				<select class="form-control" title="Lager" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][WAREHOUSE_ID]">
				{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].WAREHOUSE.D key="kWAR" item="WAR"}
					{if $WAR.Active == 1}
					<option value='{$kWAR}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].WAREHOUSE_ID == $kWAR}selected{/if}>{$WAR.Title}</option>
					{/if}
				{/foreach}
				</select>
			</td>
			<td>{input p=['readonly' => $readonly, 'placeholder'=>'Tracking Nummer', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][TRACKING_NO]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].TRACKING_NO]}</td>
			<td>
				<select class="form-control" placeholder="Versandfirma" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][SHIPPING_ID]">
					<option value=""></option>
					{*foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
						<option value='{$kSHI}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
					{/foreach*}
					{foreach from=explode("|",$D.PLATFORM.D[$D.PLATFORM_ID].SETTING.D['AvailableShippingMethods'].VALUE) key="kSHI" item="SHI"}
						<option value='{$SHI}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPING_ID == $SHI}{$_isShipp=1}selected{/if}>{i18n id="shipping_{$SHI}"}</option>
					{/foreach}
					{if !$_isShipp}
						<option value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPING_ID}' selected>{i18n id="shipping_{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPING_ID}"}</option>
					{/if}
					
				</select>
			</td>
			<td>{input p=['readonly' => $readonly, 'placeholder'=>'Datum', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][SHIPPED_DATE]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPED_DATE]}</td>
		</tr>
		<tr id="del{$D.PLATFORM_ID}{$kDEL}" style="{if $DEL.STATUS >= 40}display:none{/if}">
			<td colspan=7>
				<table class="table">
					<tr>
						<td>Absender</td>
						<td>Empfänger</td>
						<td style="width:30%">Kommentar</td>
					</tr>
					<tr>
						<td>
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Firma", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_COMPANY]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_COMPANY]}
							</div>
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Vorname", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_FNAME]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_FNAME]}
								{input p=['ischanged' => 1, 'placeholder'=>"Name", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_NAME]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_NAME]}
							</div>
								{input p=['ischanged' => 1, 'placeholder'=>"Telefon", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_PHONE]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_PHONE]}
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Straße", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_STREET]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_STREET]}
								{input p=['ischanged' => 1, 'placeholder'=>"Nummer", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_STREET_NO]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_STREET_NO]}
							</div>
							{input p=['ischanged' => 1, 'placeholder'=>"Zusatz", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_ADDITION]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_ADDITION]}
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"PLZ", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_ZIP]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_ZIP]}
								{input p=['ischanged' => 1, 'placeholder'=>"Ort", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_CITY]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_CITY]}
							</div>
							<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_COUNTRY_ID]">
								<option value=""></option>
							{foreach from=explode("|",$D.SETTING.D['country_shipp'].VALUE) key="kCOU" item="COU"}
								<option value='{$COU}' {if $INV.BILLING.COUNTRY_ID == $COU}selected{/if}>{i18n id="country_{$COU}"}</option>
							{/foreach}
							</select>
						</td>
						<td>
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Firma", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_COMPANY]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_COMPANY]}
							</div>
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Vorname", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_FNAME]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_FNAME]}
								{input p=['ischanged' => 1, 'placeholder'=>"Name", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_NAME]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_NAME]}
							</div>
							{input p=['ischanged' => 1, 'placeholder'=>"Telefon", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_PHONE]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_PHONE]}
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"Straße", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_STREET]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_STREET]}
								{input p=['ischanged' => 1, 'placeholder'=>"Nummer", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_STREET_NO]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_STREET_NO]}
							</div>
							{input p=['ischanged' => 1, 'placeholder'=>'Zusatz', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_ADDITION]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_ADDITION]}
							<div class="input-group">
								{input p=['ischanged' => 1, 'placeholder'=>"PLZ", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_ZIP]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_ZIP]}
								{input p=['ischanged' => 1, 'placeholder'=>"Ort", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_CITY]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_CITY]}
							</div>
							{input p=['ischanged' => 1, 'placeholder'=>"E-Mail", 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_EMAIL]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_EMAIL]}
							
							<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][DELIVERY_COUNTRY_ID]">
								<option value=""></option>
								{foreach from=explode("|",$D.SETTING.D['country_shipp'].VALUE) key="kCOU" item="COU"}
									<option value='{$COU}' {if $INV.BILLING.COUNTRY_ID == $COU}selected{/if}>{i18n id="country_{$COU}"}</option>
								{/foreach}
							</select>
						</td>
						<td>
							{input p=['ischanged' => 1, 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][COMMENT]", 'type'=>"textarea", 'style'=>'height:100px;','value'=>$DEL.COMMENT ]}
						</td>
					</tr>
				</table>


				<table class="table">
					<thead>
						<tr>
							<td  style="width:20px">
								<button class="btn" title="Artikel suchen" type="button" onclick="wp.window.open({ 'ID' : 'ARTSEARCH', 'TITLE' : 'Artikel Suche' , 'WIDTH' : '600px', 'HEIGHT' : '400px', 
								'URL' : '?D[PAGE]=popup.search&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}',
								'DATA' : {
									'D[DONE]' : 'new_article{$D.PLATFORM_ID}{md5($kDEL)}(ID);'
								}
								});">+</button>
							<script>
							new_article{$D.PLATFORM_ID}{md5($kDEL)} = function(ArtID)
							{
								wp.ajax({ 'div' : 'load_article2{$D.PLATFORM_ID}{$kDEL}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article2&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]='+ArtID+'&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[DELIVERY_ID]={$kDEL}'});
								
							}</script>
							</td>
							<td style="width:20px;"></td>
							<td style="width:60px">Nummer</td>
							<td>Title</td>
							<td style="width:50px">Gewicht</td>
							<td style="width:50px">Stück</td>
							<td style="width:60px">auf Lager</td>
						</tr>
					</thead>
					<tbody  id="load_article2{$D.PLATFORM_ID}{$kDEL}">
					{*<script>
					{foreach from=$DEL.ARTICLE.D key="kART" item="ART"}
						new_article{$D.PLATFORM_ID}{md5($kDEL)}('{$kART}');
					{/foreach}
					</script>*}
					{foreach from=$DEL.ARTICLE.D key="kART" item="ART"}
					
					<tr id="del{$D.PLATFORM_ID}{$kDEL}{$kART}">
						<td>
							<input id='del{$D.PLATFORM_ID}{$kDEL}{$kART}active' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ARTICLE][D][{$kART}][ACTIVE]'>
							<button class="btn" type="button" onclick="document.getElementById('del{$D.PLATFORM_ID}{$kDEL}{$kART}active').value = '-2';document.getElementById('del{$D.PLATFORM_ID}{$kDEL}{$kART}').style.display = 'none';">-</button>
						</td>
						<td>
							<div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg') center no-repeat;"></div>
						</td>
						<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ARTICLE][D][{$kART}][NUMBER]", 'value'=>"{$ART.NUMBER}"]}</td>
						<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ARTICLE][D][{$kART}][TITLE]", 'value'=>"{$ART.TITLE}"]}</td>
						<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ARTICLE][D][{$kART}][WEIGHT]", 'value'=>"{$ART.WEIGHT}"]}</td>
						<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][ARTICLE][D][{$kART}][STOCK]", 'value'=>"{$ART.STOCK}"]}</td>
						<td> ({$D.PLANTFORM.D[ $D.PLATFORM_ID ]['ARTICLE']['D'][$kART]['STOCK']})</td>
					</tr>
					{/foreach}
					</tbody>
				</table>
			</td>
		</tr>
	

{/function}

{switch $D.ACTION}
	{case 'get_delivery'}
		{get_delivery D=$D}
	{/case}
	{case 'search_article'}
		<table class='table' style='width:100%;'>
		{$RCS['DELIMITER']['LEFT'] = '[('}
		{$RCS['DELIMITER']['RIGHT'] = ')]'}
		{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}
			{$ART = $PLA.ARTICLE.D[$kART]}
				<thead>
				<tr>
					<th style="cursor:pointer;"  colspan=5 onclick="wp.ajax({ 'div' : 'art_list{$D.INVOICE_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article&D[ARTICLE_ID]={$kART}&D[INVOICE_ID]={$D.INVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'data' : {
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][NUMBER]' : '{$ART.NUMBER}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][PRICE]' : '{($ART.PRICE - ($ART.PRICE/(100+$ART.VAT)*$ART.VAT))}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][STOCK]' : '0',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][VAT]' : '{$ART.VAT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][WEIGHT]' : '{$ART.WEIGHT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][TITLE]' : '{$CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,$RCS)}'
						}});
						document.getElementById('search{$D.INVOICE_ID|replace:'-':''}box').style.display = 'none';">{$ART.NUMBER} - {$CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}</th>
				</tr>
				</thead>
			{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
				{$VAR = $PLA.ARTICLE.D[$kVAR]}
				{$TITLE = ''}
				{if $ART.VARIANTE_GROUP_ID}
					{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
					{for $i=0 to count($aVAR)-1}
						{$TITLE = "{$TITLE} {$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE} |"}
					{/for}
				{/if}
				{$TITLE2 = ($VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)?"{$TITLE} {$VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}":"{$TITLE} {$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}" }
				
				<tr style="cursor:pointer;" onclick="wp.ajax({ 'div' : 'art_list{$D.INVOICE_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article&D[ARTICLE_ID]={$kVAR}&D[INVOICE_ID]={$D.INVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'data' : {
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][NUMBER]' : '{$VAR.NUMBER}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][PRICE]' : '{($VAR.PRICE - ($VAR.PRICE/(100+$VAR.VAT)*$VAR.VAT))}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][STOCK]' : '0',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][VAT]' : '{$VAR.VAT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][WEIGHT]' : '{if $VAR.WEIGHT}{$VAR.WEIGHT}{else}{$ART.WEIGHT}{/if}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][TITLE]' : '{$CWP->rand_choice_str($TITLE2,$RCS)}'
						}});
						document.getElementById('search{$D.INVOICE_ID|replace:'-':''}box').style.display = 'none';">
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg"></td>
					<td>{$VAR.NUMBER}</td>
					<td>{$CWP->rand_choice_str($TITLE,$RCS)}</td>
					<td style="text-align:right;">{$VAR.PRICE|number_format:2:".":""}€</td>
					<td title="Bestand" style="text-align:right;{if !$VAR.STOCK>0}color:red;{/if}">{$VAR.STOCK*1}</td>
				</tr>
			{/foreach}
		{/foreach}
		</table>
		
	{/case}
	{case 'load_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}" {if $PLA.ARTICLE.D[ $D.ARTICLE_ID ].VARIANTE_GROUP_ID || !$PLA.ARTICLE.D[ $D.ARTICLE_ID ]}style="background:#fcc"{/if}>
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button class="btn" type="button" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;cursor:pointer;" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$D.ARTICLE_ID}', 'TITLE' : '{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER} | {$TITLE|replace:'"':''|truncate:80:"...":true}' , 'WIDTH' : '920px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]={$D.ARTICLE_ID}'});" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input class="form-control" style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value.replace(/,/, '.')/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input class="form-control" style="width:60px;text-align:right; {if $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK>$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}color:red;{/if}" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
			<td style="text-align:right;">{$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}</td>
		</tr>
	{/case}
	{case 'load_article2'}{*Delivery*}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			{if !$ART.VARIANTE_GROUP_ID}
			<tr>
				<td></td>
				<td>
					<div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$a = array_keys((array)$ART.FILE.D)}{$a[0]}_200x200.jpg'>" style="width:25px;height:25px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$a = array_keys((array)$ART.FILE.D)}{$a[0]}_25x25.jpg') center no-repeat;"></div>
				</td>
				<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$D.DELIVERY_ID}][ARTICLE][D][{$kART}][NUMBER]", 'value'=>"{$ART.NUMBER}"]}</td>
				<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$D.DELIVERY_ID}][ARTICLE][D][{$kART}][TITLE]", 'value'=>($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)?$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE:$PLA.ARTICLE.D[ $ART.PARENT_ID ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE]}</td>
				<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$D.DELIVERY_ID}][ARTICLE][D][{$kART}][WEIGHT]", 'value'=>"{$ART.WEIGHT}"]}</td>
				<td>{input p=['ischanged' => 1, 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$D.DELIVERY_ID}][ARTICLE][D][{$kART}][STOCK]", 'value'=>"0"]}</td>
				<td> ({$ART.STOCK})</td>
			</tr>
			{/if}
		{/foreach}
	{/case}
	{case 'load_return_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}">
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button type="button" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input style="width:100%;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'>g</td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input style="width:60px;text-align:right;" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input style="width:60px;text-align:right;{if $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK>$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}color:red;{/if}" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
			<td style="text-align:right;">{$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}</td>
		</tr>
	{/case}
	{default}
		{foreach from=$PLA.INVOICE.D key="kINV" item="INV"}
		
				{*
				<!--CUSTOMER START-->
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ACTIVE]" value='1'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][NICKNAME]" value='{$INV.CUSTOMER_NICKNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][EMAIL]" value='{$INV.CUSTOMER_EMAIL}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][NAME]" value='{$INV.BILLING.NAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][FNAME]" value='{$INV.BILLING.FNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][COMPANY]" value='{$INV.BILLING.COMPANY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][STREET]" value='{$INV.BILLING.STREET}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][STREET_NO]" value='{$INV.BILLING.STREET_NO}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ZIP]" value='{$INV.BILLING.ZIP}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][CITY]" value='{$INV.BILLING.CITY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][COUNTRY_ID]" value='{$INV.BILLING.COUNTRY_ID}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ADDITION]" value='{$INV.BILLING.ADDITION}'>
				<!--CUSTOMER END-->
				*}

<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}" method="post">
		
	<input type='hidden' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_ID]" value='{$INV.CUSTOMER_ID}'>

<div class="container p-1">
	<div class="form-row  mr-0">
		<div class="col-4">

			<div class="form-row">
				<div class="col-6">
					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">RNr</span></div>
						<div class="form-control">{if $INV.NUMBER}{$INV.NUMBER}{else}-neu-{/if}</div>
						<input class="form-control" type='text' placeholder="Datum" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DATE]" value='{if $INV.DATE}{$INV.DATE}{else}{$smarty.now|date_format: "%Y-%m-%d"}{/if}'>
					</div>

					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">Status</span></div>
						<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">
							{foreach from=$D.INVOICE.STATUS.D key="kSTA" item="STA"}
							<option style="background:{$STA.COLOR}" value='{$kSTA}' {if $INV.STATUS ==$kSTA}selected{/if}>{$STA.TITLE}</option>
							{/foreach}
						</select>
					</div>

					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">Zahlungsart</span></div>
						<select title='Zahlungsart' class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][PAYMENT_ID]">
								<option value=""></option>
							{*
							{foreach from=$D.PAYMENT.D key="kPAY" item="PAY"}
								<option value='{$kPAY}' {if $INV.PAYMENT_ID == $kPAY}selected{/if}>{$PAY.TITLE}</option>
							{/foreach}
							*}
							{foreach from=explode("|",$D.PLATFORM.D[$D.PLATFORM_ID].SETTING.D['AvailablePaymentMethods'].VALUE) key="kPAY" item="PAY"}
								<option value='{$PAY}' {if $INV.PAYMENT_ID == $PAY}{$_isPayment=1}selected{/if}>{i18n id="payment_{$PAY}"}</option>
							{/foreach}
							{if !$_isPayment}
								<option value='{$INV.PAYMENT_ID}' selected>{i18n id="payment_{$INV.PAYMENT_ID}"}</option>
							{/if}
						</select>
					</div>

					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">UserNick</span></div>
						<input class="form-control" type='text' placeholder="Nickname" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_NICKNAME]" value='{$INV.CUSTOMER_NICKNAME}'>
					</div>

					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">E-Mail</span></div>
						<input class="form-control" type='text' placeholder="E-Mail" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_EMAIL]" value='{$INV.CUSTOMER_EMAIL}'>
					</div>
					Platform: {$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}<br>

<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][PLATFORM_ID]" value="{$INV.PLATFORM_ID}">
<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][ORDER_ID]" value="{$INV.ORDER_ID}">
										
								<button class="btn btn-light" type="button" onclick="wp.window.open({ 'ID' : 'R{$kINV}', 'TITLE' : '{$ORD.NUMBER}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[PLATFORM_ID]={$D.PLATFORM_ID}&&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kINV}', 'DATA' : {
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][PLATFORM_ID]' : '{$INV.PLATFORM_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ORDER_ID]' : '{$INV.ORDER_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][PAYMENT_ID]': '{$INV.PAYMENT_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_ID]': '{$INV.CUSTOMER_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_NICKNAME]': '{$INV.CUSTOMER_NICKNAME}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_EMAIL]': '{$INV.CUSTOMER_EMAIL}',
								//'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][STATUS]': '{$INV.STATUS}',

								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][NAME]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$INV.BILLING.NAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][FNAME]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$INV.BILLING.FNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][STREET]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$INV.BILLING.STREET)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][STREET_NO]': '{$INV.BILLING.STREET_NO}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ZIP]': '{$INV.BILLING.ZIP}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CITY]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$INV.BILLING.CITY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][COUNTRY_ID]': '{$INV.BILLING.COUNTRY_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ADDITION]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$INV.BILLING.ADDITION)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][PHONE]': '{$INV.BILLING.PHONE}',
								
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][INVOICE_ID]' : '{$kINV}',
								
								'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][COMMENT]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´',''],$INV.COMMENT)}',
								{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][NUMBER]': '{if $ART.PARENT_ID}{$PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].NUMBER}{else}{$PLA.ARTICLE.D[$kART].NUMBER}{/if}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][TITLE]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$ART.TITLE)}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][PRICE]': '-{$ART.PRICE}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][VAT]': '{$ART.VAT}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][STOCK]': '{$ART.STOCK}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][WEIGHT]': '{if $ART.PARENT_ID}{$PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].WEIGHT}{else}{$PLA.ARTICLE.D[$kART].WEIGHT}{/if}',
								{/foreach}
							}});"><nobr>Stornorechnung</nobr></button> 
										<button class="btn btn-light" type="button" onclick="if(confirm('Rechnung an die E-Mail senden? Ist die Rechnung richtig?') == true) wp.ajax({
											'url' : '?D[PAGE]=platform.invoice&D[ACTION]=set_email_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}',
											'div' : 'ajax',
											'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}').serialize()
											});">R. senden</button>


				</div>
				<div class="col-6">
				
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control" placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][COMPANY]" value='{$INV.BILLING.COMPANY}'>
						<input class="form-control" placeholder="UstID" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][VATID]" value='{$INV.BILLING.VATID}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control" placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][FNAME]" value='{$INV.BILLING.FNAME}'>
						<input class="form-control" placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][NAME]" value='{$INV.BILLING.NAME}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control" placeholder="Telefon" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][PHONE]" value='{$INV.BILLING.PHONE}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control w-75" placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][STREET]" value='{$INV.BILLING.STREET}'>
						<input class="form-control w-25" placeholder="Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][STREET_NO]" value='{$INV.BILLING.STREET_NO}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control" placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][ADDITION]" value='{$INV.BILLING.ADDITION}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
						<input class="form-control w-25" placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][ZIP]" value='{$INV.BILLING.ZIP}'>
						<input class="form-control w-75" placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][CITY]" value='{$INV.BILLING.CITY}'>
					</div>
					<div class="input-group input-group-xxs mb-05">
			
						<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][COUNTRY_ID]">
							<option value=""></option>
						{foreach from=explode("|",$D.SETTING.D['country_bill'].VALUE) key="kCOU" item="COU"}
							<option value='{$COU}' {if $INV.BILLING.COUNTRY_ID == $COU}selected{/if}>{i18n id="country_{$COU}"}</option>
						{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="col-12">{input p=['type'=> 'textarea', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]", 'value'=>$INV.COMMENT, 'style'=>'width:100%;height:60px;']}</div>
			</div>
			<div class="form-row">
				<div class="col-12">
					<div class="input-group input-group-xxs mb-05">
						<div class="input-group-prepend"><span class="input-group-text">Währung</span></div>
						<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CURRENCYCODE]">
							{foreach item='CCode' from=['EUR','USD','JPY','BGN','CZK','DKK','GBP','HUF','PLN','RON','SEK','CHF','ISK','NOK','HRK','TRY','AUD','BRL','CAD','CNY','HKD','IDR','ILS','INR','KRW','MXN','MYR','NZD','PHP','SGD','THB','ZAR']}
							<option value="{$CCode}" {if $INV.CURRENCYCODE == $CCode}selected{/if}>{$CCode}</option>
							{/foreach}
						</select>
						<span class="input-group-text">=</span>
						<input class="form-control" placeholder="Umrechnungskurs" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CONVERSIONRATE]" value='{$INV.CONVERSIONRATE}'>
						<div class="input-group-prepend"><span class="input-group-text">EUR</span></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-6">

			{*DELIVERY START ========================*}
			<div style="overflow-y:scroll;height:300px;">
				
				<table class="table">
					<thead>
						<tr>
							<td style="width:60px;"><button class="btn" type="button" onclick="id = wp.get_genID();wp.ajax({ 'div' : 'delivery_{$D.PLATFORM_ID}{$kINV}', 'INSERT' : 'prepend', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=get_delivery&D[INVOICE_ID]={$D.INVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_COMPANY]={$INV.DELIVERY.COMPANY}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_FNAME]={$INV.DELIVERY.FNAME}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_NAME]={$INV.DELIVERY.NAME}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_PHONE]={$INV.DELIVERY.PHONE}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_STREET]={$INV.DELIVERY.STREET}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_STREET_NO]={$INV.DELIVERY.STREET_NO}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_ADDITION]={$INV.DELIVERY.ADDITION}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_ZIP]={$INV.DELIVERY.ZIP}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_CITY]={$INV.DELIVERY.CITY}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][DELIVERY_COUNTRY_ID]={$INV.DELIVERY.COUNTRY_ID}&&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D]['+id+'][ORDER_ID]={$INV.ORDER_ID}',
							'data' : {
								//'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][1][ACTIVE]' : 1,
								'D[DELIVERY_ID]' : id,
								'D[INVOICE_ID]' : '{$kINV}'
							} });">+</button></td>
							<td>LNr</td>
							<td>Status</td>
							<td>Lager</td>
							<td>Trecking</td>
							<td>Firma</td>
							<td>Versand Datum</td>
						</tr>
					</thead>
					<tbody id="delivery_{$D.PLATFORM_ID}{$kINV}">
						{foreach from=$PLA.DELIVERY.D key="kDEL" item="DEL"}
							{$D.DELIVERY_ID = $kDEL}
							{$D.INVOICE_ID = $kINV}
							{get_delivery D=$D}
						{/foreach}
					</tbody>
				</table>
				

			</div>
			{*DELIVERY END ==========================*}
		</div>
		
		{*MESSAGE START ================*}
		{*
		<div class="col-2">
			{if $INV.CUSTOMER_ID || $kINV}
			<div style="height:300px;overflow: hidden;overflow: auto;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}"></div>
		
				<script>
					wp.ajax({
										'url'	:	'?D[PAGE]=message',
										'data'	:	{
														'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[MESSAGE][W][GROUP_ID]' : '{if $INV.CUSTOMER_ID}{$INV.CUSTOMER_ID}{else}{$kINV|replace:"-":"_"}{/if}', //group_id,
														'D[MESSAGE][O][DATETIME]' : 'DESC'
													},
										'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}'
										});
				</script>
			{/if}
		</div>
		*}
		{*MESSAGE END ================*}
		
 	</div>
</div>


				<table style="width:100%;">
					<tr>
						<td valign="top">
							
								
								<div style="background:#eee;padding:2px;width:240px;">
									<div class="input-group">
										<input class="form-control" title="Suche in:<br>Nummer<br>EAN<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben" id="tbSearch{$kINV|replace:'-':''}">
										<span class="input-group-btn">
											<button class="btn btn-default" onclick="wp.ajax({ 'div' : 'search{$kINV|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=search_article&D[INVOICE_ID]={$kINV}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|EAN:LIKE|TITLE:LIKE]='+$('#tbSearch{$kINV|replace:'-':''}').val()+'&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});$('#search{$kINV|replace:'-':''}box').show();" type="button">Suchen</button>
										</span>
									</div>
								</div>
								<div id='search{$kINV|replace:'-':''}box' style="position:absolute;border:solid 1px #ddd;display:none;padding:2px;background:#eee;">
									<div onclick="$('#search{$kINV|replace:'-':''}box').hide();" style="border-bottom:solid 2px #ddd;text-align:center;cursor:pointer;">Schließen</div>
									<div id='search{$kINV|replace:'-':''}' style="min-height:100px;background:#fff; max-height:300px;overflow-y:scroll;"></div>
								</div>
								<table class='table'>
									<thead>
										<tr>
											<td style="width:10px;"><!--<button type="button" onclick="new_article('{$kINV}');">+</button>--></td>
											<td></td>
											<td style="width:100px;">Nummer</td>
											<td>Title</td>
											<td style="width:60px;">Gewicht</td>
											<td style="width:60px;">Netto</td>
											<td style="width:60px;">MwSt</td>
											<td style="width:60px;">Brutto</td>
											<td style="width:60px;">Stück</td>
											<td style="width:60px;">auf Lager</td>
										</tr>
									</thead>
									<tbody id="art_list{$kINV|replace:'-':''}">
									{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
										{$D['ACTION'] = 'load_article'}
										{$D['INVOICE_ID'] = $kINV}
										{$D['ARTICLE_ID'] = $kART}
										
										{include file="platform.invoice.tpl" D=$D}

										{$INVOICE_PRICE = $INVOICE_PRICE + $ART.PRICE*$ART.STOCK}
										{$INVOICE_VAT = $INVOICE_VAT + $ART.PRICE/100*$ART.VAT*$ART.STOCK}
										{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + ($ART.PRICE + $ART.PRICE/100*$ART.VAT)*$ART.STOCK}
									{/foreach}
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INVOICE_VAT|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:".":""}</td>
											<td></td>
											<td></td>
										</tr>
									</tfoot>
								</table>

		



							
						</td>
					<td valign="top">
					
							<script>
							//id = wp.get_genID(); wp.ajax({ 'div' : 'art_list{$kINV}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article&D[ARTICLE_ID]='+id+'&D[INVOICE_ID]={$kINV}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});
								new_article = function(IID)
								{
									id = wp.get_genID();
									$('#art_list'+IID).append("<td id='new{$kINV|replace:'-':''}' colspan='9'><input onclick=\"" 
									+"wp.ajax({ 'div' : 'search{$kINV|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=search_article&D[INVOICE_ID]={$kINV}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][TITLE]='+this.value+'&D[INVOICE_ID]={$kINV|replace:'-':''}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});"
									+"\" ><div id='search"+IID+"'></div></td>");
								}

								
							</script>
							
						
					</td>
				</tr>
			</table>
</form>
				





				<script>
				SAVE{$kINV|replace:"-":"_"} = function()
				{
					$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}').find(':input').each(function(){
						if( $(this).attr( "ischanged" ) == 0 )
							$(this).attr( "disabled", true );
					});
					wp.ajax({
					'url' : '?D[PAGE]=platform.invoice&D[ACTION]=set_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
					'div' : 'ajax',
					'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}').serialize()
					});

					$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}').find(':input').each(function(){
						$(this).attr( "disabled", false );
					});
				}
				</script>
			{/foreach}
		
{/switch}

