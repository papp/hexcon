	<table style='height:100%;width:100%;' cellpadding="0" cellspacing="0">
		<tr>
			<td valign=top class="bg-light" style="width:150px;padding:2px;font-size:13px;">
				<div style="text-align:center;">Einkauf</div>
				<nav class="navbar navbar-light" style="position:fixed;">
					<ul class="nav nav-pills flex-column">
								<li class="nav-link"  onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.buying.article_list',
												'data'	:	{
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
															},
												'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
												'ASYNC'	: 1
												})"
								><i class="fa fa fa-pencil"></i> Bestellvorschläge</li>
								{*<li class="nav-link" onclick="alert('noch keine Funktion!');"><i class="fa fa fa-pencil"></i> Bestelliste</li>*}
								<li class="nav-link"  onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.buying_list',
												'data'	:	{
																
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][GROUP_ID:IN]' : 'buying',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][STATUS:IN]' : '0',
															},
												'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
												'ASYNC'	: 1
												})"
								><i class="fa fa fa-pencil"></i> Offen</li>
								<li class="nav-link"  onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.buying_list',
												'data'	:	{
																
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][GROUP_ID:IN]' : 'buying',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][STATUS:IN]' : '20',
															},
												'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
												'ASYNC'	: 1
												})"
								><i class="fa fa-clock-o"></i> Bestellt</li>
								<li class="nav-link"  onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.buying_list',
												'data'	:	{
																
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][GROUP_ID:IN]' : 'buying',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][STATUS:IN]' : '40',
															},
												'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
												'ASYNC'	: 1
												})"
								><i class="fa fa-check"></i> Fertig</li>
						
						<li class="nav-link"  onclick="wp.ajax({
								'url'	:	'?D[PAGE]=platform.supplier_list',
								'data'	:	{
												'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											},
								'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
								'ASYNC'	: 1
								})"><i class="fa fa-check-square-o"></i> Lieferant</li>
					</ul>
				</nav>
			</td>
			<td valign=top id='fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'></td>
		</tr>
	</table>