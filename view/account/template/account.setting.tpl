{function name="get_setting" ALLSETTINGS=null D=null PARENT_ID=null}

	{foreach from=$ALLSETTINGS.PARENT.D[$PARENT_ID].CHILD.D key="kSET" item="SET"}
		<tr>
			<td style="{if $ALLSETTINGS.PARENT.D[$kSET].CHILD.D}position:sticky;top:20px;z-index:10;background:#ff9;{/if}cursor:pointer;" title="{$kSET}" onclick="navigator.clipboard.writeText('{$kSET}');">&#xf0c5;</td>
			<td style="position:sticky; left:0px;background:#fafafa;z-index:1;{if $ALLSETTINGS.PARENT.D[$kSET].CHILD.D}top:20px;z-index:20;font-weight:bold;background:#ff9;{/if}">{i18n id=$kSET}</td>
			
			
			<td style="{if $ALLSETTINGS.PARENT.D[$kSET].CHILD.D}position:sticky;top:20px;z-index:10;background:#ff9;{/if}">
			{if !$ALLSETTINGS.PARENT.D[$kSET].CHILD.D && $D.SETTING.D[$kSET]}
				{*$D.SETTING.D[$kSET].READWRITE*} 
				{if $D.SETTING.D[$kSET].READWRITE >= 22}
					{input p=[ id=> "set_{$kSET}_active", name=>"D[PRESET][SETTING][D][{$kSET}][ACTIVE]", type=>'hidden', ischanged => true, value=> (($D.SETTING.D[$kSET].VALUE == '')?'-2':'1')  ]}
					{input p=[ 'type'=>$D.SETTING.D[$kSET].TYPE, onchange => "$('#set_{$kSET}_active').val(( $(this).val() != '' ?'1':'-2') );", 'option' => explode("\n",$D.SETTING.D[$kSET].VALUE_OPTION), 'name'=>"D[PRESET][SETTING][D][{$kSET}][VALUE]", 'value'=>$D.SETTING.D[$kSET].VALUE]}
				{elseif $D.SETTING.D[$kSET].READWRITE >= 20}
					{input p=[ 'type'=>$D.SETTING.D[$kSET].TYPE, 'option' => explode("\n",$D.SETTING.D[$kSET].VALUE_OPTION), readonly=> true,  'value'=>$D.SETTING.D[$kSET].VALUE]}
				{/if}
			{/if}
			</td>
			{foreach from=$D.PLATFORM.D key=kPL item=PL}
				{if $PL.ACTIVE}
					<td style="{if $ALLSETTINGS.PARENT.D[$kSET].CHILD.D}position:sticky;top:20px;z-index:10;background:#ff9;{/if}">
						
						{if !$ALLSETTINGS.PARENT.D[$kSET].CHILD.D}
							
							{if $PL.SETTING.D[$kSET].READWRITE >= 33}
								<div style="text-align:center;">
								{*$PL.SETTING.D[$kSET].READWRITE*} {if $PL.SETTING.D[$kSET].HELP}<label title="{$PL.SETTING.D[$kSET].HELP}"></label>{/if}

								{input p=[ id=> "set_{$kSET}_{$kPL}_active", name=>"D[PRESET][PLATFORM][D][{$kPL}][SETTING][D][{$kSET}][ACTIVE]", type=>'hidden', ischanged => true, value=> (($D.SETTING.D[$kSET].VALUE == $PL.SETTING.D[$kSET].VALUE || $PL.SETTING.D[$kSET].VALUE == '')?'-2':'1')  ]}
								{if ($D.SETTING.D[$kSET] || $D.SETTING.D[$kSET].VALUE || $D.SETTING.D[$kSET].VALUE_OPTION) && $PL.SETTING.D[$kSET].TYPE != 'hidden' && $PL.SETTING.D[$kSET].TYPE != 'label'}
									
									<button class="btn" onclick="$('#set_{$kSET}_{$kPL}_input').toggle();$('#set_{$kSET}_{$kPL}_active').val( ($('#set_{$kSET}_{$kPL}_input').is(':hidden')?'-2':'1') );($('#set_{$kSET}_{$kPL}_input').is(':hidden')?$(this).html(''): $(this).html(''));" type="button" title="Diese Einstellung erbt von Globalen Einstellung, individuelle Einstellung wird entfernt."></button>
								{/if}

								</div>
								<div id="set_{$kSET}_{$kPL}_input" style="{if $D.SETTING.D[$kSET] && $D.SETTING.D[$kSET].VALUE == $PL.SETTING.D[$kSET].VALUE}display:none;{/if}">
									{input p=[ 'type'=>$PL.SETTING.D[$kSET].TYPE, onchange => "$('#set_{$kSET}_{$kPL}_active').val(( $(this).val() != '' ?'1':'-2') );", 'option' => explode("\n",$PL.SETTING.D[$kSET].VALUE_OPTION), 'name'=>"D[PRESET][PLATFORM][D][{$kPL}][SETTING][D][{$kSET}][VALUE]", 'value'=>$PL.SETTING.D[$kSET].VALUE]}
								</div>
							{elseif $PL.SETTING.D[$kSET].READWRITE >= 30 && (!$D.SETTING.D[$kSET] || $D.SETTING.D[$kSET].VALUE != $PL.SETTING.D[$kSET].VALUE)}
								{input p=[ 'type'=>$PL.SETTING.D[$kSET].TYPE, 'option' => explode("\n",$PL.SETTING.D[$kSET].VALUE_OPTION), readonly=> true,  'value'=>$PL.SETTING.D[$kSET].VALUE]}
							{/if}
						{/if}
					</td>
				{/if}
			{/foreach}
		</tr>
		
		{if $ALLSETTINGS.PARENT.D[$kSET].CHILD.D}
			{get_setting ALLSETTINGS=$ALLSETTINGS D=$D PARENT_ID=$kSET}
		{/if}
	{/foreach}
{/function}

{switch $D.ACTION}
	{case 'add_platform'}
	{/case}

	{default}

	<form id="form{$D.ACCOUNT_ID}SET" name="form{$D.ACCOUNT_ID}SET" method="post">
{*$x.SETTING.D = array_diff_key((array)$D.SETTING.D,(array)$D.PLATFORM.D['amazon_de'].SETTING.D)*}

		<table class="table" style="border-collapse: separate;border-spacing: 0;">
			<thead style="position:sticky; top:0px;z-index:10;">		
				<tr>
					<th style="text-align:center;">ID</th>
					<th style="min-width:100px;text-align:center;position:sticky; left:0px;font-weight:bold;background:#f5f5f5;">Title</th>
					<th style="min-width:150px;text-align:center;">Account</th>
					{foreach from=$D.PLATFORM.D key=kPL item=PL}
						{if $PL.ACTIVE}
					<th style="min-width:150px;text-align:center;">{$PL.TITLE}</th>
						{/if}
					{/foreach}
				</tr>
				
			</thead>
			<tbody>
				{$AllSet = array_merge_recursive($D.SETTING)}
				{foreach from=$D.PLATFORM.D key=kPL item=PL}
					{if $PL.ACTIVE}
					{$AllSet = array_merge_recursive($AllSet, $D.PLATFORM.D[$kPL].SETTING)}
					{/if}
				{/foreach}
				{get_setting ALLSETTINGS=$AllSet D=$D}
				
			</tbody>
		</table>


	</form>
{/switch}