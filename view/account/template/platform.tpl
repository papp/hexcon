{*foreach from=$D.PLATFORM.D key="kPLA" item="PLA"*}
{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}


	<div class="fixed-top">
		<div class="border border-top-0 border-right-0 border-left-0 border-secondary">
			
			
			<div class="bg-secondary">
				<div class="row">
					<div class="col">

						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img style="height:16px;" src="core/platform/{$PLA.CLASS_ID}/{$D.CLASS[$PLA.CLASS_ID].ICON}">  {$D.PLATFORM.D[$D.PLATFORM_ID].TITLE} 
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								{foreach from=$D.PLATFORM.D key="kPL" item="PL"}
									{if $PL.ACTIVE}
										<a class="dropdown-item" href="#" onclick="wp.ajax({ 'url' : '?D[PAGE]=platform&D[PLATFORM_ID]={$kPL}','div' : 'frame_index'});"><img style="height:16px;" src="core/platform/{$PL.CLASS_ID}/{$D.CLASS[$PL.CLASS_ID].ICON}"> {$PL.TITLE}</a>
									{/if}
								{/foreach}
							</div>
						</div>

					</div>
					<div class="col align-self-end">
					
						<div class="btn-group float-end">
						
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									&#xf1de; Einstellungen
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'PLSET', 'TITLE' : 'Platform Einstellungen' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=account.platform'});">Plattform</a>
									<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'SET', 'TITLE' : 'Einstellungen' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=account.setting'});">Einstellungen</a>
									<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'USER', 'TITLE' : 'Benutzer' , 'WIDTH' : '800px', 'HEIGHT' : '500px', 'URL' : '?D[PAGE]=account.user'});">Benutzer</a>

									<a class="dropdown-item" href="#"onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'data'	:	{
																					'D[F][PLATFORM]' : [null],
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;">Data Einstellungen</a>
									{*<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'WAREHOUSE', 'TITLE' : 'Benutzer' , 'WIDTH' : '800px', 'HEIGHT' : '500px', 'URL' : '?D[PAGE]=account.data_list&D[TYPE]=WAREHOUSE'});"> Lagerort Einstellungen</a>*}
									
									{*<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'PLATFORM{$D.SESSION.ACCOUNT_ID}SHIPPING', 'TITLE' : 'Versandregeln' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=account.shipping'});">Versandregeln</a>*}
								
									<a class="dropdown-item" href="#" title="Feed Einstellungen für {$PLA.TITLE}" onclick="wp.window.open({ 'ID' : 'PLATFORM{$D.SESSION.ACCOUNT_ID}FeedSet_{$D.PLATFORM_ID}', 'TITLE' : '{$PLA.TITLE} - Feed Einstellungen' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.setting.feed&D[PLATFORM_ID]={$D['PLATFORM_ID']}'});"><img style="height:16px;" src="core/platform/{$PLA.CLASS_ID}/{$D.CLASS[$PLA.CLASS_ID].ICON}"> Feed Einstellungen</a>
									<a class="dropdown-item" href="#" title="Attribute Einstellungen für {$PLA.TITLE}" onclick="wp.window.open({ 'ID' : 'PLATFORM{$D.SESSION.ACCOUNT_ID}ATT_{$D.PLATFORM_ID}', 'TITLE' : '{$PLA.TITLE} - Attribute' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.attribute&D[PLATFORM_ID]={$D['PLATFORM_ID']}'});"><img style="height:16px;" src="core/platform/{$PLA.CLASS_ID}/{$D.CLASS[$PLA.CLASS_ID].ICON}"> Attribute</a>
									<a class="dropdown-item" href="#" title="Attribute Einstellungen für {$PLA.TITLE}" onclick="wp.window.open({ 'ID' : 'PLATFORM{$D.SESSION.ACCOUNT_ID}ATT_{$D.PLATFORM_ID}', 'TITLE' : '{$PLA.TITLE} - Attribute' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.attribute2&D[PLATFORM_ID]={$D['PLATFORM_ID']}'});"><img style="height:16px;" src="core/platform/{$PLA.CLASS_ID}/{$D.CLASS[$PLA.CLASS_ID].ICON}"> Attribute [neu]</a>
								</div>
							</div>

							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									&#xf007; User
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#" onclick="window.open('?D[PAGE]=login&D[ACTION]=logout','_self');set_active();"><i class="fa fa-sign-out" ></i> Logout</a>
								</div>
							</div>
						
						</div>

					</div>
				</div>
			
			</div>


			<div class="bg-light">
				<div class="row XXoverflow-auto">
					<div class="col-auto mr-auto  m-auto">

						<div class="btn-group btn-group-toggle">
							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({ 'url':'?D[PAGE]=platform.dashboard&D[PLATFORM_ID]={$D.PLATFORM_ID}','div':'fPLA{$D.PLATFORM_ID}'});return false;"><div style="font-size:15pt">&#xf0e4;</div>Dashboard</button>

							<div class="dropdown">
								<button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<div style="font-size:15pt">&#xf218; </div>Einkauf
								</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										{*
										<a class="dropdown-item" href="#" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.supplier_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;">&#xf046; Lieferant</a>
										*}
										<a class="dropdown-item" href="#" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'data'	:	{
																					'D[F][PLATFORM][W][0][ID]' : '{$D.PLATFORM_ID}',
																					'D[F][PLATFORM][SUPPLIER]' : [null],
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;">&#xf046; Lieferant</a>
										<a class="dropdown-item" href="#" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.buying.start&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;">&#xf218; Einkauf</a>
										
										
								</div>
							</div>

{*						
							<button type="button" class="btn btn-light btn-sm"onclick="wp.ajax({
															'url'	:	'?D[PAGE]=platform.customer_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
															'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
															});return false;"><div style="font-size:15pt">&#xf2ba;</div>Kunden</button>
*}					
							
							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.article_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"><div style="font-size:15pt">&#xf02c;</div>Artikel</button>
						
{*
							{if $PLA.CLASS_ID && 'get_order'|in_array:$D.CLASS[$PLA.CLASS_ID].FUNCTION && $D.SESSION.USER.PLATFORM.D[$D['PLATFORM_ID']].RIGHT.D['WP00001'].ACTIVE}
							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.order_list2',
																	'data'	:	{
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					'D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][CACHE]' : 1
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"><div style="font-size:15pt">&#xf135;</div>Verkauf</button>
							{/if}
*}
							
							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.invoice_list',
																	'data'	:	{
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][DATE]' : '{$smarty.now|date_format:"%Y-%m"}%'
																			
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"><div style='font-size:15pt'>&#xf0d6;</div>Rechnung</button>
{*
							<li><a onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.return_list',
																	'data'	:	{
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																					'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][DATE]' : '{$smarty.now|date_format:"%Y-%m"}%'
																			
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}',ASYNC:1
																	});return false;" href="#"><i class="fa fa-level-down" style="font-size:22pt"></i><br>Reture</a></li>
*}
							

							
							<div class="dropdown">
								<button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<div style="font-size:15pt">&#xf494;</div> Lager
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									{foreach from=$D.PLATFORM.D[ $D.PLATFORM_ID ].WAREHOUSE.D key="kWAR" item="WAR"}
									<a class="dropdown-item" href="#" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=warehouse.index',
																	'data'	:	{
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					//'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$kWAR}',
																					'D[WAREHOUSE][W][ID]' : '{$kWAR}'
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC': 1
																	});return false;">&#xf494; {$WAR.Title}</a>
									{/foreach}
									<a class="dropdown-item" href="#" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=data_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	'data'	:	{
																					//'D[PLATFORM]' : [null],
																					'D[F][PLATFORM][W][0][ID]' : '{$D.PLATFORM_ID}',
																					'D[F][PLATFORM][WAREHOUSE]' : [null],
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;">&#xf494; Lagerort Einstellungen</a>
								</div>
							</div>

							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.order_list',
																	'data'	:	{
																					//'D[PLATFORM][D][{$D.PLATFORM_ID}][ACTIVE]' : '1',
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																					'D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][CACHE]' : 1
																					//,'D[INVOICE][W][WAREHOUSE_ID]' : '{$kWAR}'
																					//,'D[INVOICE][W][STATUS]' : 20
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"><div style="font-size:15pt">&#xf0c9;</div>Order</button>

							<button type="button" class="btn btn-light btn-sm" onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.incominginvoice_list',
																	'data'	:	{
																					'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][W][DATE_PAID:LIKE]' : '{$smarty.now|date_format:"%Y-%m"}%'
																				},
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"><div style='font-size:15pt'>&#xf571;</div>Ausgaben</button>
							
			{*
							<div class="dropdown">
								<button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="wp.tab.open({ 'ID': 'setting', 'TITLE' : 'Einstellungen', 
								'URL': '?D[PAGE]=platform.setting&D[PLATFORM_ID]={$D.PLATFORM_ID}'});return false;">
									<div style="font-size:15pt">&#xf1de;</div>Einstellungen
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

									{if $PLA.CLASS_ID && 'get_feed'|in_array:$D.CLASS[$PLA.CLASS_ID].FUNCTION}
									<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'FEED{$D.PLATFORM_ID}', 'TITLE' : 'Feed Einstellungen' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.setting.feed&D[PLATFORM_ID]={$D.PLATFORM_ID}'});return false;">Feed Einstellungen</a>
									{/if}
					{-*
									{if $PLA.CLASS_ID && 'get_group'|in_array:$D.CLASS[$PLA.CLASS_ID].FUNCTION}
										<a href="#" onclick="wp.window.open({ 'ID' : 'GRO{$D.PLATFORM_ID}', 'TITLE' : 'Gruppe' , 'WIDTH' : '800px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.group&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">Gruppe</a>
									{/if}
					*-}
									{if $PLA.CLASS_ID && 'get_attribute'|in_array:$D.CLASS[$PLA.CLASS_ID].FUNCTION}
									<li>
										<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'ATT{$D.PLATFORM_ID}', 'TITLE' : 'Attribute' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.attribute&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">Attribute</a>
									</li>
									{/if}
									{-*
									<a class="dropdown-item" href="#" onclick="wp.window.open({ 'ID' : 'REL{$D.PLATFORM_ID}', 'TITLE' : 'Zuordnung' , 'WIDTH' : '800px', 'HEIGHT' : '600px', 'URL' : '?D[PAGE]=platform.relation&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">PL. Zuordnung</a>
									*-}
									<a class="dropdown-item" href="#" 
													onclick="wp.ajax({
																	'url'	:	'?D[PAGE]=platform.log&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}',
																	
																	'div'	:	'fPLA{$D.PLATFORM_ID}','ASYNC':1
																	});return false;"
									>Log</a>

									
								</div>
			*}
							</div>
						</div>
					</div>
					
				</div>
			</div>



		</div>
	</div>
		
			

	<!--</div>-->

	{*
	<div class="bg-white p-1">
		<ul class="nav nav-tabs" id="nav_page">
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="#home" data-bs-toggle="tab">Dashboard</a>
			</li>
		</ul>
	</div>
	*}	
	
<!--</div>	-->

	

<div class="container-fluid " style="/*min-height: 75rem;*/ padding-top:6.5rem;">
{*tab Content
	<div class="tab-content" id="nav_pageContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"></div>
	</div>
	*}

	
	<div id='fPLA{$D.PLATFORM_ID}' class="XXoverflow-auto" style="display: ; clear:both;position:absolute;top:0;left:0;right:0;bottom:0;padding-top:75px;"></div>
</div>



	

	

	
<div id="layer_footer" style="border-top:solid 1px #ddd;padding:2px;bottom:0;position:fixed;width:100%;background:#f5f5f5;z-index:10000;"></div>