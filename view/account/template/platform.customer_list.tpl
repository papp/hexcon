{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}supplier" method="post">
		<div style="">
			<table class='table display' id="example" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width:50px;"></th>
						<th style="width:100px;">ID</th>
						<th style="width:100px;">Status</th>
						<th style="width:150px;">Firma</th>
						<th style="width:100px;">Ust-ID</th>
						<th style="width:150px;">Nachname</th>
						<th style="width:150px;">Vorname</th>
						<th style="width:100px;">E-Mail</th>
						<th style="width:50px;">Straße</th>
						<th style="width:50px;">Str.Nr.</th>
						<th style="width:50px;">PLZ</th>
						<th style="width:200px;">Ort</th>
						<th style="width:100px;">Land</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{foreach from=$D.CUSTOMER.D key="kCUS" item="CUS" name="CUS"}
						<tr>
							<td>{$CUS.ACTIVE}</td>
							<td>{$kCUS}</td>
							<!--<td><input name="D[CUSTOMER][D][{$kCUS}][TITLE]" value="{$CUS.TITLE}"></td>-->
							<td></td>
							<td>{$CUS.company}</td>
							<td>{$CUS.vat_id}</td>
							<td>{$CUS.NAME}</td>
							<td>{$CUS.FNAME}</td>
							<td>{$CUS.EMAIL}</td>
							<td>{$CUS.STREET}</td>
							<td>{$CUS.STREET_NO}</td>
							<td>{$CUS.ZIP}</td>
							<td>{$CUS.CITY}</td>
							<td>{$CUS.COUNTRY_ID}</td>
							<td></td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
			<!--<button class="btn" type="button" onclick="SAVEsupplier();">Speichern</button>-->
			{*
			<nav>
			  <ul class="pagination">
			  {section name=run start=0 loop=$D.CUSTOMER.COUNT step=100}
				<li><a href="#" onclick="NEXTsupplier({$smarty.section.run.index});">{$smarty.section.run.index/100}</a></li>
				{/section}
			  </ul>
			</nav>*}
		</form>
		
		<script>
		$(document).ready(function() {
    $('#example').DataTable({
		scrollY:        '50vh',
        scrollCollapse: true,
        paging:         false,
		responsive: true,
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return data +' ('+ row[3]+')';
                },
                "targets": 0
            },
            { "visible": false,  "targets": [ 3 ] }
        ]
    } );
} );
			NEXTsupplier = function(start)
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.customer_list&D[CUSTOMER][L][START]='+start+'&D[PLATFORM_ID]={$D.PLATFORM_ID}',
				'div' : 'fPLApl_{$D.SESSION.ACCOUNT_ID}'
				});
				return false;
			}
			SAVEsupplier = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.customer_list&D[ACTION]=set_supplier&D[PLATFORM_ID]={$D.PLATFORM_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}supplier').serialize()
				});
			}
		</script>