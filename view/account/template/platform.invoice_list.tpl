{*foreach from=$D.PLATFORM.D key="kPLA" item="PLA"*}
{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<div style="max-height:300px;overflow-y:scroll;">
	<!--Order-->
	<table class='table'>
			<thead>
				<tr>
					<th style="width:30px;"></th>
					<th style="width:100px;">ID</th>
					<th style="width:60px;">Platform</th>
					<th style="width:60px;">Datum</th>
					<th>Name</th>
					<th>Status</th>
					<th style="width:10%">Versand</th>
					<th style="width:10%">AutoCheck</th>
					<th style="width:100px;">Nickname</th>
					<th style="width:60px;">Gewicht</th>
					<th style="width:60px;">Netto</th>
					<th style="width:60px;">MwSt</th>
					<th style="width:60px;">Brutto</th>
				</tr>
			</thead>
			<tbody>
				
					{foreach from=$PLA.ORDER.D key="kORD" item="ORD"}
						{$Q = 1}
						{if !$ORD.SHIPPING_ID}{$Q = 0}{/if}
						{if !$ORD.WAREHOUSE_ID}{$Q = 0}{/if}
						{if !$ORD.PAID}{$Q = 0}{/if}
						{if !(($ORD.DELIVERY.COMPANY || $ORD.DELIVERY.NAME) && $ORD.DELIVERY.STREET && strlen($ORD.DELIVERY.STREET) > 3 && $ORD.DELIVERY.STREET_NO && $ORD.DELIVERY.CITY && $ORD.DELIVERY.ZIP && $ORD.DELIVERY.COUNTRY_ID)}{$Q = 0}{/if}
						{if !($ORD.DELIVERY.PHONE || $ORD.BILLING.PHONE || $ORD.CUSTOMER_EMAIL)}{$Q = 0}{/if}
						{if strpos(strtolower($ORD.DELIVERY.STREET),'packstation') !== false && !(strlen($ORD.DELIVERY.STREET_NO) == 3 && strlen($ORD.DELIVERY.ADDITION) >=8 && strlen($ORD.DELIVERY.ADDITION) < 10)}{$Q=0}{/if}

						<tr style="{if $PLA.INVOICE.D[$kORD]}background:#f5f5f5;color:#777;{/if}">
							<td>
									{foreach from=$ORD.ARTICLE.D key="kART" item="ART"}
										<img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg">
										{if $ART.PARENT_ID}
											{$ORDER_WEIGHT[$kORD] = $ORDER_WEIGHT[$kORD] + $PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].WEIGHT}
										{else}
											{$ORDER_WEIGHT[$kORD] = $ORDER_WEIGHT[$kORD] + $PLA.ARTICLE.D[$kART].WEIGHT}
										{/if}
									
									{/foreach}
							</td>
							<td>{*<button type="button" onclick="wp.window.open({ 'ID' : '{$kORD}', 'TITLE' : '{$ORD.NUMBER}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.invoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kORD}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][FROM_PLATFORM_ID]={$ORD.PLATFORM_ID}', 'DATA' : $('#frm{$D.PLATFORM_ID}{$kORD}').serialize()});"><nobr>{$kORD}</nobr></button>
							*}
							
							{if $D.PLATFORM.D[$ORD.PLATFORM_ID].TITLE != 'dropShipper'}
								{$newID = $CWP->uuid()}
							<button class="btn btn-primary btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$newID}', 'TITLE' : '{$ORD.NUMBER}' , 'WIDTH' : '1000px', 'HEIGHT' : '500px', 'URL' : '?D[PAGE]=platform.invoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$newID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][FROM_PLATFORM_ID]={$ORD.PLATFORM_ID}', 'DATA' : {
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][PLATFORM_ID]' : '{$ORD.PLATFORM_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ORDER_ID]' : '{$kORD}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][PAYMENT_ID]': '{$ORD.PAYMENT_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][CUSTOMER_ID]': '{$ORD.CUSTOMER_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][CUSTOMER_NICKNAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.CUSTOMER_NICKNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][CUSTOMER_EMAIL]': '{$ORD.CUSTOMER_EMAIL}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][CURRENCYCODE]': '{$ORD.CURRENCYCODE}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][CONVERSIONRATE]': '{$ORD.CONVERSIONRATE}',
								{*'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][PAID]': '{$ORD.PAID}',*}
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][STATUS]': '{if $ORD.STATUS}{$ORD.STATUS}{elseif $Q}20{else}{/if}',

								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][COMPANY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.COMPANY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][NAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.NAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][FNAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.FNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][STREET]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.STREET)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][STREET_NO]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.STREET_NO)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][ZIP]': '{$ORD.BILLING.ZIP}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][CITY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.CITY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][COUNTRY_ID]': '{$ORD.BILLING.COUNTRY_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][ADDITION]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.ADDITION)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][BILLING][PHONE]': '{str_replace(["\n","\r","'",'"',"\\"],['\\n','\\r','´','´','\\\\'],$ORD.BILLING.PHONE)}',
								
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][COMPANY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.COMPANY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][NAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.NAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][FNAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.FNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][STREET]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.STREET)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][STREET_NO]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.STREET_NO)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][ZIP]': '{$ORD.DELIVERY.ZIP}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][CITY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.CITY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][COUNTRY_ID]': '{$ORD.DELIVERY.COUNTRY_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][ADDITION]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.ADDITION)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][DELIVERY][PHONE]': '{str_replace(["\n","\r","'",'"',"\\"],['\\n','\\r','´','´','\\\\'],$ORD.DELIVERY.PHONE)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][SHIPPING_ID]': '{$ORD.SHIPPING_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][WAREHOUSE_ID]': '{$ORD.WAREHOUSE_ID}',
									
								'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][COMMENT]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.COMMENT)}',

								{*Delivery Start*}
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][INVOICE_ID]': '{$kORD}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ORDER_ID]': '{$kORD}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][CUSTOMER_ID]': '{$ORD.CUSTOMER_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][STATUS]': '{if $ORD.STATUS}{$ORD.STATUS}{elseif $Q}20{else}{/if}',
								
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_EMAIL]': '{$ORD.CUSTOMER_EMAIL}',
								
								{if $ORD.DELIVERY.COMPANY || $ORD.DELIVERY.NAME}
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_COMPANY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.COMPANY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_NAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.NAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_FNAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.FNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_STREET]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.STREET)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_STREET_NO]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.STREET_NO)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_ZIP]': '{$ORD.DELIVERY.ZIP}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_CITY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.CITY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_COUNTRY_ID]': '{$ORD.DELIVERY.COUNTRY_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_ADDITION]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.DELIVERY.ADDITION)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_PHONE]': '{str_replace(["\n","\r","'",'"',"\\"],['\\n','\\r','´','´','\\\\'],$ORD.DELIVERY.PHONE)}',
								{else}
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_COMPANY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.COMPANY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_NAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.NAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_FNAME]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.FNAME)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_STREET]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.STREET)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_STREET_NO]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.STREET_NO)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_ZIP]': '{$ORD.BILLING.ZIP}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_CITY]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.CITY)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_COUNTRY_ID]': '{$ORD.BILLING.COUNTRY_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_ADDITION]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.BILLING.ADDITION)}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][DELIVERY_PHONE]': '{str_replace(["\n","\r","'",'"',"\\"],['\\n','\\r','´','´','\\\\'],$ORD.BILLING.PHONE)}',
								{/if}
								
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][SHIPPING_ID]': '{$ORD.SHIPPING_ID}',
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][WAREHOUSE_ID]': '{$ORD.WAREHOUSE_ID}',
									
								'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][COMMENT]': '{str_replace(["\n","\r","'",'"'],['\\n','\\r','´','´'],$ORD.COMMENT)}',
								{*Delivery Ende*}


								{foreach from=$ORD.ARTICLE.D key="kART" item="ART"}
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][NUMBER]': '{$ART.NUMBER}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][TITLE]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$ART.TITLE)}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][PRICE]': '{$ART.PRICE}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][VAT]': '{$ART.VAT}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][STOCK]': '{$ART.STOCK}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$newID}][ARTICLE][D][{$kART}][WEIGHT]': '{$ART.WEIGHT}{*if $ART.CHILD.D[ $kART ].PARENT_ID && $PLA.ARTICLE.D[$ART.CHILD.D[ $kART ].PARENT_ID].WEIGHT}{$PLA.ARTICLE.D[$ART.CHILD.D[ $kART ].PARENT_ID].WEIGHT}{else}{$PLA.ARTICLE.D[$kART].WEIGHT}{/if*}',
								
									{*Delivery Start*}
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][NUMBER]': '{$ART.NUMBER}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][TITLE]': '{str_replace(["\n","\r","'"],['\\n','\\r','´'],$ART.TITLE)}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][PRICE]': '{$ART.PRICE}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][VAT]': '{$ART.VAT}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][STOCK]': '{$ART.STOCK}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$newID}][ARTICLE][D][{$kART}][WEIGHT]': '{$ART.WEIGHT}{*if $ART.CHILD.D[ $kART ].PARENT_ID && $PLA.ARTICLE.D[$ART.CHILD.D[ $kART ].PARENT_ID].WEIGHT}{$PLA.ARTICLE.D[$ART.CHILD.D[ $kART ].PARENT_ID].WEIGHT}{else}{$PLA.ARTICLE.D[$kART].WEIGHT}{/if*}',
									{*Delivery Ende*}
								{/foreach}
							}});"><nobr>{$kORD}</nobr></button>
							{else}
								<nobr>{$kORD}</nobr>
							{/if}
							</td>
							<td style="text-align:center;"><nobr>{$D.PLATFORM.D[$ORD.PLATFORM_ID].TITLE}</nobr></td>
							<td style="text-align:center;"><nobr>{$ORD.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</nobr></td>
							<td>{$ORD.BILLING.FNAME}, {$ORD.BILLING.NAME}</td>
							<td>{if $ORD.STATUS == NULL}◕{elseif $ORD.STATUS == 0}offen{elseif $ORD.STATUS == 9}<label style="color:red;">Klärung</label>{elseif $ORD.STATUS == 20}<label style="color:yellow;">Versandfreigabe</label>{elseif $ORD.STATUS == 40}fertig{else}{$ORD.STATUS}{/if}</td>
							<td>{$D['SHIPPING']['D'][$ORD.SHIPPING_ID].TITLE}</td>
							<td>
								<div title="<ul>
									<li>Versandzuordnung: {if $ORD.SHIPPING_ID}OK{else}Fail{/if}</li>
									<li>Lagerzuordnung: {if $ORD.WAREHOUSE_ID}OK{else}Fail{/if}</li>
									<li>Bezahlt: {if $ORD.PAID}OK{else}Fail{/if}</li>
									<li>Anschrift(hinterlegt): {if ($ORD.DELIVERY.COMPANY || $ORD.DELIVERY.NAME) && $ORD.DELIVERY.STREET && $ORD.DELIVERY.STREET_NO && $ORD.DELIVERY.CITY && $ORD.DELIVERY.ZIP && $ORD.DELIVERY.COUNTRY_ID}OK{else}Fail{/if}</li>
									<li>Anschrift Check: n/a</li>
									{if strpos(strtolower($ORD.DELIVERY.STREET),'packstation') !== false}
									<li>Paketstation Check: {if strlen($ORD.DELIVERY.STREET_NO) == 3 && strlen($ORD.DELIVERY.ADDITION) >=8 && strlen($ORD.DELIVERY.ADDITION) < 10}OK{else}Fail{/if}</li>
									{/if}
									<li>Telefon/E-Mail: {if $ORD.DELIVERY.PHONE || $ORD.BILLING.PHONE || $ORD.CUSTOMER_EMAIL}OK{else}Fail{/if}</li>
									<li>Artikel Referenz: n/a</li>
									<li>Storno: n/a</li>
								</ul>" style="padding:2px;background-color:{if $Q}green{else}red{/if}">{if $Q}OK{else}Fail{/if}</div>
							</td>
							<td>{$ORD.CUSTOMER_NICKNAME}</td>
							<td style="text-align:right;">{$ORDER_WEIGHT[$kORD]|number_format:2:",":"."}</td>
							<td style="text-align:right;">{($ORD.ARTICLE.PRICE*$ORD.CONVERSIONRATE)|number_format:2:",":"."}</td>
							<td style="text-align:right;">{($ORD.ARTICLE.VAT*$ORD.CONVERSIONRATE)|number_format:2:",":"."}</td>
							<td style="text-align:right;">{(($ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)*$ORD.CONVERSIONRATE)|number_format:2:",":"."}</td>
					
							{$ORDER_WEIGHT_SUM = $ORDER_WEIGHT_SUM + $ORDER_WEIGHT[$kORD]}
							{$ORDER_PRICE = ($ORDER_PRICE + $INV.ARTICLE.PRICE)*$ORD.CONVERSIONRATE}
							{$ORDER_VAT = ($ORDER_VAT + $ORD.ARTICLE.VAT)*$ORD.CONVERSIONRATE}
							{$ORDER_PRICE_BRUTTO = ($ORDEER_PRICE_BRUTTO +$ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)*$ORD.CONVERSIONRATE}
						</tr>
					{/foreach}
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right;">{$ORDER_WEIGHT_SUM|number_format:2:",":"."}g</td>
					<td style="text-align:right;">{$ORDER_PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$ORDER_VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$ORDER_PRICE_BRUTTO|number_format:2:",":"."}</td>
				</tr>
			</tfoot>
		</table>
</div>

	<!--Inoice-->
<div class="container">
	<form method="post" name="fsearch" id="fsearch">
		<div class="row justify-content-end">
			<div class="col-md-auto">
				<div class="input-group mb-1">
					{*<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]">
						<option value="">alle</option>
						<option value='9' {if $D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['STATUS'] == 9}selected{/if}>Klärung</option>
					</select>*}
					<input type="text" class="form-control" placeholder="Search for..." onkeydown="$( this ).keypress( function(e){ if(e.which == 13){ /*SEARCH();*/return false;}} );" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][EMAIL:LIKE|NICKNAME:LIKE|NUMBER:LIKE|BILLING_NAME:LIKE|BILLING_FNAME:LIKE|BILLING_STREET:LIKE|BILLING_STREET_NO:LIKE|BILLING_ZIP:LIKE|BILLING_CITY:LIKE|BILLING_ADDITION:LIKE|DELIVERY_COMPANY:LIKE|DELIVERY_NAME:LIKE|DELIVERY_FNAME:LIKE|DELIVERY_STREET:LIKE|DELIVERY_STREET_NO:LIKE|DELIVERY_ZIP:LIKE|DELIVERY_CITY:LIKE|DELIVERY_ADDITION:LIKE|TRACKING_NO:LIKE]" value="{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['EMAIL:LIKE|NICKNAME:LIKE|NUMBER:LIKE|BILLING_NAME:LIKE|BILLING_FNAME:LIKE|BILLING_STREET:LIKE|BILLING_STREET_NO:LIKE|BILLING_ZIP:LIKE|BILLING_CITY:LIKE|BILLING_ADDITION:LIKE|DELIVERY_COMPANY:LIKE|DELIVERY_NAME:LIKE|DELIVERY_FNAME:LIKE|DELIVERY_STREET:LIKE|DELIVERY_STREET_NO:LIKE|DELIVERY_ZIP:LIKE|DELIVERY_CITY:LIKE|DELIVERY_ADDITION:LIKE|TRACKING_NO:LIKE']}" title="Suche in:<br>Nummer<br>Name<br>VorName<br>Straße<br>Straße Nr<br>Zusatz<br>PLZ<br>Ort<br>EMail<br>UserNickname<br>Trackingnummer<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben">
					<button class="btn btn-outline-secondary" type="button" onclick="SEARCH();">Suchen</button>
				</div>
			</div>
		</div>
	</form>
	<script>
		SEARCH = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.invoice_list&D[PLATFORM_ID]={$D.PLATFORM_ID}',
				'div' : 'fPLA{$D.PLATFORM_ID}',
				'data': $('#fsearch').serialize()
				});
		
			}
	</script>
</div>
	<div id="tab{$D.PLATFORM_ID}TimeLine">
			<ul class="nav nav-tabs">
				{$now_y = $smarty.now|date_format:"%Y"}
				{for $y=$now_y-10 to $now_y}
				<li class="nav-item" onclick="wp.ajax({
											'url'	:	'?D[PAGE]=platform.invoice_list',
											'data'	:	{
															'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
															'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][DATE]' : '{$y}-01%'
														},
											'div'	:	'fPLA{$D.PLATFORM_ID}'
											})"><a class="nav-link {if ($PLA.INVOICE.W.DATE|truncate:4:'') == $y}active{/if}" href="#tab{$D.PLATFORM_ID}TimeLine-{$y}">{$y}</a></li>
				{/for}
			</ul>
			<div class="btn-group btn-group-sm">
			{$y = $PLA.INVOICE.W.DATE|truncate:4:''}
				<button class="btn {if ($PLA.INVOICE.W.DATE|truncate:4:'') == $y && ($PLA.INVOICE.W.DATE|replace:"%":'') == $y}btn-primary{else}btn-light{/if}" onclick="wp.ajax({
					'url'	:	'?D[PAGE]=platform.invoice_list',
					'data'	:	{
									'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
									'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
									'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][DATE]' : '{$y}%'
								},
					'div'	:	'fPLA{$D.PLATFORM_ID}'
					})">Alle</button>
				{for $i=1 to 12}
					<button class="btn {if ($PLA.INVOICE.W.DATE|truncate:4:'') == $y && ($PLA.INVOICE.W.DATE|replace:"%":''|date_format: "%m") == $i && ($PLA.INVOICE.W.DATE|replace:"%":'') != $y}btn-primary{else}btn-light{/if}" onclick="wp.ajax({
									'url'	:	'?D[PAGE]=platform.invoice_list',
									'data'	:	{
													'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
													'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
													'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][DATE]' : '{$y}-{if $i < 10}0{/if}{$i}%'
												},
									'div'	:	'fPLA{$D.PLATFORM_ID}'
									})">{$i}</button>
				{/for}
			</div>

		<form method="post">
			<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}]['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
			<!--<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}]['PLATFORM_ID']" value="{$D.PLATFORM_ID}">-->
			
			{*$DATA['HEAD']['D']['0']['D']['0']['VALUE'] = 'Bilanz'}
			{$DATA['HEAD']['D']['0']['D']['1']['VALUE'] = ''}
			{$DATA['HEAD']['D']['0']['D']['2']['VALUE'] = 'Nummer'}
			{$DATA['HEAD']['D']['0']['D']['3']['VALUE'] = 'Platform'}
			{$DATA['HEAD']['D']['0']['D']['4']['VALUE'] = 'Datum'}
			{$DATA['HEAD']['D']['0']['D']['5']['VALUE'] = 'Status'}
			{$DATA['HEAD']['D']['0']['D']['6']['VALUE'] = 'Lager'}
			{$DATA['HEAD']['D']['0']['D']['7']['VALUE'] = 'Name'}
			{$DATA['HEAD']['D']['0']['D']['8']['VALUE'] = 'Nickname'}
			{$DATA['HEAD']['D']['0']['D']['9']['VALUE'] = 'Gewicht'}
			{$DATA['HEAD']['D']['0']['D']['10']['VALUE'] = 'Netto'}
			{$DATA['HEAD']['D']['0']['D']['11']['VALUE'] = 'MwSt'}
			{$DATA['HEAD']['D']['0']['D']['12']['VALUE'] = 'Brutto'}
			{$DATA['HEAD']['D']['0']['D']['13']['VALUE'] = ''}

			{foreach from=$PLA.INVOICE.D key="kINV" item="INV" name="INV"}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['0']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['1']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['2']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['3']['VALUE'] = $kINV}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['3']['VALUE'] = "AAA"|cat:" BBB"}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['4']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['5']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['6']['VALUE'] = 'Bilanz'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['7']['VALUE'] = $INV.BILLING.FNAME}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['8']['VALUE'] = $INV.CUSTOMER_NICKNAME}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['9']['VALUE'] = $INV.ARTICLE.WEIGHT|number_format:0:",":"."|cat:'g'}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['10']['VALUE'] = $INV.ARTICLE.PRICE|number_format:2:",":"."}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['11']['VALUE'] = $INV.ARTICLE.VAT|number_format:2:",":"."}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['12']['VALUE'] = ($INV.ARTICLE.PRICE + $INV.ARTICLE.VAT)|number_format:2:",":"."}
				{$DATA['BODY']['D'][$smarty.foreach.INV.index]['D']['13']['VALUE'] = 'Bilanz'}
			{/foreach}
			
			{include file="include/table.tpl" DATA=$DATA*}
			
			<table class='table' style="border-collapse: separate;border-spacing: 0;">
				<thead style="position:sticky; top:75px; z-index:10;">
					<tr>
						<th colspan="8"></th>
						<th colspan="6" style="text-align:center;background:#ddd">Lieferschein</th>
						<th colspan="2" style="text-align:center;border-right:solid 1px #ddd;">Kunde</th>
						<th colspan="6"></th>
					</tr>
					<tr>
						<th style="width:50px;">Bilanz</th>
						<th style="width:30px;"><button type="button" class="btn btn-success" onclick="id = wp.get_genID(); wp.window.open({ 'ID' : id, 'TITLE' : 'Neue Rechnung' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.invoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D]['+id+'][ACTIVE]=1&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D]['+id+'][CURRENCYCODE]=EUR&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D]['+id+'][CONVERSIONRATE]=1'});">+</button></th>
						<th style="width:30px;"><input type="checkbox" onclick="$('input[name=\'cbINVOICE\']').prop('checked', $(this).prop('checked') );"></th>
						<th style="width:100px;">ID</th>
						<th style="width:100px;text-align:center;">R.Nummer</th>
						<th style="width:100px;text-align:center;">Platform</th>
						<th style="width:160px;text-align:center;">OrderID</th>
						<th style="width:60px;">Datum</th>
						<th style="width:60px;">LNr</th>
						<th style="width:100px;">Status</th>
						<th style="width:100px;">Lager</th>
						<th style="width:60px;">Spedition</th>
						<th style="width:60px;">Ver.Datum</th>
						<th style="width:200px;">Name</th>
						<th style="width:200px;">Name</th>
						<th style="width:100px;">Nickname</th>
						<th style="width:60px;">Gewicht</th>
						<th style="width:60px;">Netto</th>
						<th style="width:60px;">MwSt</th>
						<th style="width:60px;">Brutto</th>
						<th style="width:60px;"><i title="Storno" class="fa fa-retweet"></i></th>
						<th>
							<div class="dropdown">
							  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fa fa-download"></i>
								<span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							  	<li onclick="alert(getCBChecked());">test</li>
								{foreach from=$PLA.FEED.D key="kFEE" item="FEE" name="FEE"}
									<li onclick="window.open('{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEE}/{$FEE.KEY}/{$PLA.INVOICE.W.DATE|date_format:"%Y.%m"}_{$FEE.FILENAME}.{$FEE.FILETYPE}?D[ID]='+getCBChecked())">{$FEE.NAME}</li>
								{/foreach}
							  </ul>
							</div>
						</th>
					</tr>
				</thead>
				<tbody id="tbList">
				
						{foreach from=$PLA.INVOICE.D key="kINV" item="INV" name="INV"}
							<tr>
							{if $LAST_DAY == NULL && $INV.NUMBER > 0}
								{$LAST_DAY = $INV.DATE|date_format:"%d"}
							{/if}
							{if $group_D != $INV.DATE}
								<td style="text-align:center;height:100px;width:50px;position:sticky;top:126px;padding-top:10px;background:#fff;" rowspan="{$PLA.INVOICE.DATE.D[ $INV.DATE ].COUNT}">
								<div style="transform:rotate(270deg);">{$PLA.INVOICE.DATE.D[$INV.DATE].COUNT}St. <br> {($PLA.INVOICE.DATE.D[$INV.DATE].PRICE+$PLA.INVOICE.DATE.D[$INV.DATE].VAT)|number_format:2:",":"."}€</div></td>
							{/if}
							{$group_D = $INV.DATE}

								<td style="text-align:right;">{$smarty.foreach.INV.iteration}</td>
								<td><input type="checkbox" name="cbINVOICE" value="{$kINV}"></td>
								<td>{$kINV}</td>
								<td style="text-align:right;">
									<button class="btn btn-primary btn-block" type="button" onclick="wp.window.open({ 'ID' : '{$kINV}', 'TITLE' : '{$INV.NUMBER}' , 'WIDTH' : '1000px', 'HEIGHT' : '500px', 'URL' : '?D[PAGE]=platform.invoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kINV}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][ACTIVE]'});"><i class="fa fa-money"></i> {$INV.NUMBER}</button>
								</td>
								<td style="text-align:center;">{$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}</td>
								<td style="text-align:center;">{$INV.ORDER_ID}</td>
								<td style="text-align:center;">{$INV.DATE|date_format:"%d.%m.%Y"}</td>
								<td colspan=6>
									
								{if $PLA.DELIVERY.INVOICE.D[$kINV].DELIVERY.D}
									<div style="max-height:100px;overflow-y:auto;">
									<table class="table">
									{foreach from=$PLA.DELIVERY.INVOICE.D[$kINV].DELIVERY.D key="kDEL" item="DEL" name="DEL"}
										<tr>
											<td style="text-align:center;width:60px;">{$PLA.DELIVERY.D[$kDEL].NUMBER}</td>
											<td style="text-align:ceter;width:100px;background:{$D.INVOICE.STATUS.D[$PLA.DELIVERY.D[$kDEL].STATUS].COLOR}">{$D.INVOICE.STATUS.D[$PLA.DELIVERY.D[$kDEL].STATUS].TITLE}</td>
											<td style="text-align:center;width:100px;"><nobr>{if $PLA.WAREHOUSE.D[$PLA.DELIVERY.D[$kDEL].WAREHOUSE_ID].Title}{$PLA.WAREHOUSE.D[$PLA.DELIVERY.D[$kDEL].WAREHOUSE_ID].Title}{else}{$PLA.WAREHOUSE.D[$PLA.DELIVERY.D[$kDEL].WAREHOUSE_ID].Title}{/if}</nobr></td>
											<td style="text-align:center;width:60px;">{i18n id="shipping_{$PLA.DELIVERY.D[$kDEL].SHIPPING_ID}"}</td>
											<td style="text-align:center;width:60px;"><nobr>{$PLA.DELIVERY.D[$kDEL].SHIPPED_DATE}</nobr></td>
											<td style="text-align:center;width:200px;"><nobr>{$PLA.DELIVERY.D[$kDEL].DELIVERY_FNAME}, {$PLA.DELIVERY.D[$kDEL].DELIVERY_NAME}</nobr></td>
										</tr>
									{/foreach}
									</table>
									</div>
								{/if}
								</td>
								<td>{$INV.BILLING.FNAME}, {$INV.BILLING.NAME}</td>
								<td>{$INV.CUSTOMER_NICKNAME}</td>
								<td style="text-align:right;">{$INV.ARTICLE.WEIGHT|number_format:0:",":"."}g</td>
								<td style="text-align:right;{if $PLA.RETURN.D[$kINV]}color:red{/if}">{*$INV.ARTICLE.PRICE|number_format:2:",":"."*}{(($INV.ARTICLE.PRICE + $PLA.RETURN.D[$kINV].PRICE))|number_format:2:",":"."}</td>
								<td style="text-align:right;{if $PLA.RETURN.D[$kINV]}color:red{/if}">{*$INV.ARTICLE.VAT|number_format:2:",":"."*}{(($INV.ARTICLE.VAT + $PLA.RETURN.D[$kINV].VAT))|number_format:2:",":"."}</td>
								<td style="text-align:right;{if $PLA.RETURN.D[$kINV]}color:red{/if}">{*($INV.ARTICLE.PRICE + $INV.ARTICLE.VAT)|number_format:2:",":"."*}{((($INV.ARTICLE.PRICE + $INV.ARTICLE.VAT) + ($PLA.RETURN.D[$kINV].PRICE + $PLA.RETURN.D[$kINV].VAT)))|number_format:2:",":"."}</td>
								<td>
									{if $PLA.RETURN.D[$kINV]}
										<button class="btn btn-danger btn-block" title="Storno" type="button" onclick="wp.window.open({ 'ID' : 'return{$kINV}', 'TITLE' : 'Storno: {$PLA.RETURN.D[$kINV].NUMBER}' , 'WIDTH' : '800px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[ACCOUNT_ID]={$D.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kINV}'});">{$PLA.RETURN.D[$kINV].NUMBER}</button>
									{/if}
								</td>
								<td>
									<div class="dropdown">
									  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										<i class="fa fa-download"></i>
										<span class="caret"></span>
									  </button>
									  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										{foreach from=$PLA.FEED.D key="kFEE" item="FEE" name="FEE"}
											<li onclick="window.open('{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEE}/{$FEE.KEY}/{$INV.DATE|date_format:"%Y.%m.%d"}_{$FEE.FILENAME}_{$INV.NUMBER}.{$FEE.FILETYPE}?D[ID]={$kINV}')">{$FEE.NAME}</li>
										{/foreach}
									  </ul>
									</div>
								</td>

								{*$INV_PLATFORM[$INV.PLATFORM_ID] = 1*}
								
								{if $INV.NUMBER > 0}
									{$INV_PRICE = ($INV.ARTICLE.PRICE + $PLA.RETURN.D[$kINV].PRICE)}{**$INV.CONVERSIONRATE}*}
									{$INV_VAT = ($INV.ARTICLE.VAT + $PLA.RETURN.D[$kINV].VAT)}{**$INV.CONVERSIONRATE}*}
									
									{$INVOICE_WEIGHT = $INVOICE_WEIGHT + $INV.ARTICLE.WEIGHT}
									{$INVOICE_PRICE = $INVOICE_PRICE + $INV_PRICE}
									{$INVOICE_VAT = $INVOICE_VAT + $INV_VAT}
									{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + $INV_PRICE + $INV_VAT}
								
									{*PRO LAGER*}
									{$INVOICE_COUNT[$INV.WAREHOUSE_ID] = $INVOICE_COUNT[$INV.WAREHOUSE_ID] + 1}
									{$INVOICE_PRICE2[$INV.WAREHOUSE_ID] = $INVOICE_PRICE2[$INV.WAREHOUSE_ID] + $INV_PRICE}
									{$INVOICE_VAT2[$INV.WAREHOUSE_ID] = $INVOICE_VAT2[$INV.WAREHOUSE_ID] + $INV_VAT}

									{*PRO Platform*}
									{$INVOICE_COUNT_PL[$INV.PLATFORM_ID] = $INVOICE_COUNT_PL[$INV.PLATFORM_ID] + 1}
									{$INVOICE_PRICE_PL[$INV.PLATFORM_ID] = $INVOICE_PRICE_PL[$INV.PLATFORM_ID] + $INV_PRICE}
									{$INVOICE_VAT_PL[$INV.PLATFORM_ID] = $INVOICE_VAT_PL[$INV.PLATFORM_ID] + $INV_VAT}

									{*PRO Versandart*}
									{foreach from=$PLA.DELIVERY.INVOICE.D[$kINV].DELIVERY.D key="kDEL" item="DEL" name="DEL"}
										{$INVOICE_COUNT_SHIPPING[$PLA.DELIVERY.D[$kDEL].SHIPPING_ID] = $INVOICE_COUNT_SHIPPING[$PLA.DELIVERY.D[$kDEL].SHIPPING_ID] + 1}
									{/foreach}
									{$INVOICE_PRICE_SHIPPING[$INV.SHIPPING_ID] = $INVOICE_PRICE_SHIPPING[$INV.SHIPPING_ID] + $INV_PRICE}
									{$INVOICE_VAT_SHIPPING[$INV.SHIPPING_ID] = $INVOICE_VAT_SHIPPING[$INV.SHIPPING_ID] + $INV_VAT}
								{/if}
							</tr>
						{/foreach}
				
				</tbody>
				<tfoot style="position:sticky; bottom:0;z-index:10;">
					<tr>
						<td style="text-align:left;" colspan="10" title="
							
							<table class='table'>
								<thead>
								<tr>
									<th></th>
									<th>St./Tag</th>
									<th>€/Tag</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td colspan=3><b>Pro Lager:</b></td>
								</tr>
								{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
									{if $LAST_DAY > 0 && $WAR.ACTIVE == 1}
										<tr>
											<td>{$WAR.TITLE}</td>
											<td style='text-align:right;'>{($INVOICE_COUNT[$kWAR]/$LAST_DAY)|number_format:0:",":"."}</td>
											<td style='text-align:right;'>{(($INVOICE_PRICE2[$kWAR]+$INVOICE_VAT2[$kWAR])/$LAST_DAY)|number_format:2:",":"."}</td>
										</tr>
									{/if}
								{/foreach}
								{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
									{if $LAST_DAY > 0 && $WAR.Active == 1}
										<tr>
											<td>{$WAR.Title}</td>
											<td style='text-align:right;'>{($INVOICE_COUNT[$kWAR]/$LAST_DAY)|number_format:0:",":"."}</td>
											<td style='text-align:right;'>{(($INVOICE_PRICE2[$kWAR]+$INVOICE_VAT2[$kWAR])/$LAST_DAY)|number_format:2:",":"."}</td>
										</tr>
									{/if}
								{/foreach}
								<tr>
									<td colspan=3><b>Pro Platform:</b></td>
								</tr>
								{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].FROM.PLATFORM.D key="kPL" item="PL" name="PL"}
									{if $D.PLATFORM.D[$kPL].ACTIVE == 1 && $LAST_DAY > 0}
										<tr>
											<td>{$D.PLATFORM.D[$kPL].TITLE}</td>
											<td style='text-align:right;'>{($INVOICE_COUNT_PL[$kPL]/$LAST_DAY)|number_format:0:",":"."}</td>
											<td style='text-align:right;'>{(($INVOICE_PRICE_PL[$kPL]+$INVOICE_VAT_PL[$kPL])/$LAST_DAY)|number_format:2:",":"."}</td>
										</tr>
									{/if}
								{/foreach}
								<tr>
									<td colspan=3><b>Pro Versandart:</b></td>
								</tr>
								{foreach from=$D.SHIPPING.D key="kSHI" item="SHI" name="SHI"}
									{if $SHI.ACTIVE == 1 && $LAST_DAY > 0}
										<tr>
											<td>{$SHI.TITLE}</td>
											<td style='text-align:right;'>{($INVOICE_COUNT_SHIPPING[$kSHI]/$LAST_DAY)|number_format:0:",":"."}</td>
											<td style='text-align:right;'>{(($INVOICE_PRICE_SHIPPING[$kSHI]+$INVOICE_VAT_SHIPPING[$kSHI])/$LAST_DAY)|number_format:2:",":"."}</td>
										</tr>
									{/if}
								{/foreach}
									</tbody>
								</table>
						">
							{if $LAST_DAY > 0}Ø {($PLA.INVOICE.D|COUNT/$LAST_DAY)|number_format:0:",":"."}St./Tag | {($INVOICE_PRICE_BRUTTO/$LAST_DAY)|number_format:0:",":"."}€/Tag | ({$LAST_DAY} Tage){/if}</td>
						
						
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align:right;">{$INVOICE_WEIGHT|number_format:0:",":"."}g</td>
						<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
						<td></td>
						<td></td>
					</tr>
				</tfoot>
			</table>

		</form>
				<script>
				getCBChecked = function(){
								var ids = '';
								$('#tbList').find('input[name="cbINVOICE"]:checked').each(function(){
								
									ids += ((ids == '')?'':"','")+$(this).attr('value');
								});
								return ids;
				}


				$(function() {
					var index = $('#tab{$D.PLATFORM_ID}TimeLine a[href="#tab{$D.PLATFORM_ID}TimeLine-{$PLA.INVOICE.W.DATE|truncate:4:''}"]').parent().index();
					//var index = $("#tab{$D.PLATFORM_ID}TimeLine>ul").index( $("#tab{$D.PLATFORM_ID}TimeLine-2014") );
					//alert(index);
					//////$( "#tab{$D.PLATFORM_ID}TimeLine" ).tabs({ active: index });
				});
				</script>
	</div>
{*/foreach*}