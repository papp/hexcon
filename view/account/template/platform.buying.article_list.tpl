{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

	<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][SUPPLIER_ID:IN]"
			onchange="wp.ajax({ 'url' : '?D[PAGE]=platform.buying.article_list','data' :{ 'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}','D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][SUPPLIER_ID:IN]' : this.value},'div' : 'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}','ASYNC' : 1})">
		<option value=''>Hersteller</option>
		{foreach from=$PLA.SUPPLIER.D item=SUP key=kSUP name=SUP}
			{if $SUP.ACTIVE}
				<option {if $PLA.ARTICLE.W['SUPPLIER_ID:IN'] == $kSUP}selected{/if} value='{$kSUP}'>{$SUP.TITLE}</option>
			{/if}
		{/foreach}
	</select>

<table class="table">
	<thead>
		<tr>
			<th>bild</th>
			<th>Art.Nr.</th>
			<th>Name</th>
			<th>Bestand</th>
			<th>Bestellt</th>
			<th style="text-align:right;width:100px;">Sale</th>
			<th style="text-align:right;">VK</th>
			<th>Lieferant</th>
		</tr>
	</thead>
{foreach from=$PLA.ARTICLE.D item=ART key=kART name=ART}
	{if !$PLA.ARTICLE.PARENT.D[$kART].CHILD.D && $ART.SUPPLIER}
		{$TITLE = ($ART.PARENT_ID)?$PLA.ARTICLE.D[$ART.PARENT_ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE:$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}
	<tr>
		<td>
		{foreach from=$ART.FILE.D item=FILE key=kFILE name=FILE}
			{if $smarty.foreach.FILE.iteration == 1}
			<div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFILE}_300x300.jpg'>" style="height:40px;width:40px;background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFILE}_40x40.jpg) center no-repeat"></div>
			{/if}
		{/foreach}
		</td>
		<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{if $ART.PARENT_ID}{$ART.PARENT_ID}{else}{$kART}{/if}', 'TITLE' : '{$ART.NUMBER} | {$TITLE|replace:'\'':'\\\''|replace:'"':''|truncate:80:"...":false}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={if $ART.PARENT_ID}{$ART.PARENT_ID}{else}{$kART}{/if}'});">{$ART.NUMBER}</button></td>
		<td>{$CWP->rand_choice_str($TITLE,['DELIMITER'=>['LEFT'=> '[(', 'RIGHT'=>')]' ]])}
			{if $PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE_GROUP_ID}
				<div style="color:red;">
				{$aVAR = explode('|',$PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE_GROUP_ID)}
				{for $i=0 to count($aVAR)-1}
					{$ART.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
				{/for}
				</div>
			{/if}
		</td>
		<td>{$ART.STOCK}</td>
		<td>{$ART.SUPPLIER.STOCK}</td>
		
		<td style="text-align:right;" {if $ART.INVOICE.YEAR.D}
				title="
					{$maxY = 2}
					<table class='table'>
						<thead>
						<tr>
							<td style='text-align:right;'>Monat</td>
							{for $y=date('Y')-$maxY to date('Y')}
								<td style='text-align:right;'>{$y}</td>
							{/for}
						</tr>
						</thead>
						{for $m=1 to 12}
							{$mm = str_pad($m, 2 ,'0', STR_PAD_LEFT)}
						<tr>
							<td style='text-align:right;'>{$mm}</td>
							{for $y=date('Y')-$maxY to date('Y')}
								<td style='text-align:right;'>
									{if $ART.ITIMESTAMP < ((int)"{$y}{$mm}00000000") && date('Ymd') > (int)"{$y}{$mm}00"}
										{$_AVG_SUM[$kART] = $_AVG_SUM[$kART]+$ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}
										{$_AVG_COUNT[$kART] = $_AVG_COUNT[$kART]+1}
										{$_AVG_SUM[$y][$kART] = $_AVG_SUM[$y][$kART]+$ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}
										{$_AVG_COUNT[$y][$kART] = $_AVG_COUNT[$y][$kART]+1}
										{if !$ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}0{/if}
									{/if}
									{$ART.INVOICE.YEAR.D[$y].MONTH.D[$m].STOCK}
								</td>
							{/for}
						</tr>
						{/for}
						<tr>
							<td style='text-align:right;'><b>Σ</b></td>
							{for $y=date('Y')-$maxY to date('Y')}
								<td style='text-align:right;'><b>{$ART.INVOICE.YEAR.D[$y].STOCK}</b></td>
							{/for}
						</tr>
						<tr>
							<td style='text-align:right;'><b>Ø</b></td>
							{for $y=date('Y')-$maxY to date('Y')}
								<td style='text-align:right;'>{if $_AVG_SUM[$y][$kART]> 0}<b>{($_AVG_SUM[$y][$kART] / $_AVG_COUNT[$y][$kART])|number_format:1:".":""}</b>{else}0{/if}</td>
							{/for}
						</tr>
							
					</table>"
			{/if}>{$ART.INVOICE.STOCK} {if $_AVG_SUM[$kART]> 0}<br>[Ø {($_AVG_SUM[$kART]/$_AVG_COUNT[$kART])|number_format:1:".":""}/mon]{/if}
		</td>
		<td style="text-align:right;">{$ART.PRICE|number_format:2:".":""} €</td>
		<td>
		{if $ART.SUPPLIER.D}
			<table class="table">
				{*<thead>
					<tr>
						<th>Lieferant</th>
						<th style="width:150px;">Nummer</th>
						<th style="width:50px;text-align:right;">EK</th>
					</tr>
				</thead>*}
			{foreach from=$ART.SUPPLIER.D item=SUP key=kSUP name=SUP}
				<tr>
					<td style="width:150px;">{$PLA.SUPPLIER.D[$kSUP].TITLE}</td>
					<td><b>{$SUP.NUMBER}</b> - {$SUP.TITLE}<br><i>{$SUP.COMMENT}</i></td>
					<td style="width:50px;text-align:right;">{$SUP.PRICE|number_format:2:".":""}</td>
				</tr>
			{/foreach}
			</table>
		{/if}
		</td>
	</tr>
	{/if}
{/foreach}
</table>