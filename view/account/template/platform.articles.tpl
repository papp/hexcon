{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

<div style="border:2px red dotted;margin:5px;padding:5px;">
Achtung:<br>
Jedes hinzugefügte Attribut wird dem Artikel mit dem Wert hinzugefügt oder falls vorhanden überschrieben.<br>
Die Änderung erfolgt erst nach OK oder Übernehmen.<br>
1. Attribut aus der DDL selektieren.<br>
2. Wert eintragen.<br>
3. Mit [+] allen Artikel Hinzufügen.<br>
Es können auch vor dem speichern die Werte der Attribute je Artikel auch individuell angepasst werden.
</div>
<form id="form{$D.PLATFORM_ID}all">
	Attribute an <select id="addAtt">
		<option value="p" selected>Vater</option>
		<option value="c">Kind</option>
	</select> anhängen.
	<table>
		<tr><td>
	<div class="p-1 bg-warning text-dark">
		Artikel Attribute:<br>
		<div class="input-group">
			<div class="input-group-prepend">
				<button class="btn" type="button" onclick="add_attribut();">+</button>
			</div>
			<select id="ddl_Attribut" class="form-control">
				{foreach from=$PLA.ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATTT" item="ATTT"}
					<optgroup label="{$kATTT}">
					{foreach from=$PLA.ATTRIBUTE.PARENT.D[$kATTT].CHILD.D key="kATT" item="ATT"}
						{if $ATT.ACTIVE}
					<option value="{$kATTT}|{$kATT}|{$ATT.I18N}">{if $ATT.I18N}&#xf1ab;{/if} {$ATT.LANGUAGE.D['DE'].TITLE}</option>
						{/if}
					{/foreach}
					</optgroup>
				{/foreach}
			</select>
			<input id="tbValue1" class="form-control">
		</div>
	</div>
	</td><td>
	<div class="p-1 bg-info text-dark">
		Platform Attribute:<br>
		<div class="input-group">
			<div class="input-group-prepend">
				<button class="btn" type="button" onclick="add_pl_attribut();">+</button>
			</div>
			<select id="ddl_Platform" class="form-control">
			
				{foreach from=$D.PLATFORM.D key="kPL" item="PL"}
					{if $D.PLATFORM.D[$kPL].ACTIVE > 0 && $kPL != $D.PLATFORM_ID}
					<optgroup label="{$D.PLATFORM.D[$kPL].TITLE}">
						{foreach from=$D.PLATFORM.D[$kPL].ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATTT" item="ATTT"}
							<optgroup label="{$kATTT}">
							{foreach from=$D.PLATFORM.D[$kPL].ATTRIBUTE.PARENT.D[$kATTT].CHILD.D key="kATT" item="ATT"}
								{if $ATT.ACTIVE}
							<option value="{$kPL}|{$kATTT}|{$kATT}|{$ATT.I18N}">{if $ATT.I18N}&#xf1ab;{/if} {$ATT.LANGUAGE.D['DE'].TITLE}</option>
								{/if}
							{/foreach}
							</optgroup>
						{/foreach}
					</optgroup>
					{/if}
				{/foreach}
			</select>
			<input id="tbValue2" class="form-control">
		</div>
	</div>
	</td><td>
	<div class="p-1 bg-danger text-dark">
		<table>
		{foreach from=$D.PLATFORM.D key="kPL" item="PL"}
			{if $D.PLATFORM.D[$kPL].ACTIVE > 0 && $kPL != $D.PLATFORM_ID }
				<tr>
					<td>{$D.PLATFORM.D[$kPL].TITLE}</td>
					<td>
						<button class="btn" type="button" onclick="if (confirm('Dieses Artikel soll neu angelegt werden?'))window.open('cronjob_test.php?D[PLATFORM][D][{$kPL}][ARTICLE][W][ID]={foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}{$kART}\',\'{/foreach}&D[PLATFORM][W][ID]={$kPL}&D[SESSION][ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}','_blank');">ADD</button>
						<button class="btn" type="button" onclick="if (confirm('Dieses Artikel soll bei aktuallisiert werden?'))window.open('cronjob_test.php?D[PLATFORM][D][{$kPL}][ARTICLE][W][ID]={foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}{$kART}\',\'{/foreach}&D[PLATFORM][W][ID]={$kPL}&D[SESSION][ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[UPDATE]=1','_blank');">UPDATE</button>
					</td>
				</tr>
			{/if}
		{/foreach}
		</table>
	</div>
	</td>
	<td>
		Translate in:<br>
		{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
			{if $LAN.ACTIVE && $kLAN != 'DE'}
				<a href="cronjob__deepl.php?D[LANGUAGE_ID]={$kLAN}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID:IN]='{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}{$kART}','{/foreach}'" target="_blank">{i18n id="language_{$kLAN}"}</a><br>
			{/if}
		{/foreach}
		
	</td></tr>
	</table>
	<table class="table" id="tb{$D.PLATFORM_ID}_art" class="list" style="width:100%;" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th></th>
				<th style="min-width:100px">ArtNr.</th>
				<th style="min-width:150px">Title</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}
			<tr aid="{$kART}" data-type="p">
				<td><div style="width:25px;height:25px;background: url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg?{$ART.UTIMESTAMP}') center center no-repeat"></div></td>
				<td>{$ART.NUMBER}</td>
				<td>{$CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,[ 'DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ])}</td>
			</tr>
				{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
				<tr aid="{$kVAR}" data-type="c" style="background:#fffece;">
					<td><div style="width:25px;height:25px;background: url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg?{$VAR.UTIMESTAMP}') center center no-repeat"></div></td>
					<td>{$VAR.NUMBER}</td>
					<td>{$CWP->rand_choice_str($VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,[ 'DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ] ])}</td>
				</tr>
				{/foreach}
			{/foreach}
		</tbody>
	</table>
</form>

<script>
add_attribut = function()
{
	$('#tb{$D.PLATFORM_ID}_art thead tr').append('<td><b>'+$('#ddl_Attribut option:selected').text()+'</b></td>');
	$('#tb{$D.PLATFORM_ID}_art').find('tbody tr').each(function(){
		if($('#addAtt option:selected').val() == $( this ).data( 'type' ) )
		{
			ATTTIDuATTID = $('#ddl_Attribut').val().split('|');
			if(ATTTIDuATTID[2] == 1)//ist i18n
				$(this).append('<td style="min-width:150px"><div class="input-group" id="att'+$(this).attr('aid')+ATTTIDuATTID[1]+'">'
					+'<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][ATTRIBUTE][D]['+ATTTIDuATTID[1]+'][LANGUAGE][D][DE][ACTIVE]" value="1">'
					+'<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][ATTRIBUTE][D]['+ATTTIDuATTID[1]+'][LANGUAGE][D][DE][VALUE]" value="'+$('#tbValue1').val()+'">'
					+'<div class="input-group-prepend"><button class="btn" onclick="if(confirm(\'Dieser Wert bleibt unverändert?\')){ $(\'#att'+$(this).attr('aid')+ATTTIDuATTID[1]+'\').empty();}" type="button"><i onclick="wp.window.close(\'ART{$D.PLATFORM_ID}all\');" style="font-size:13pt;color:red;" class="fa fa-minus-circle"></i></button></div>'
					+'</div></td>');
			else
				$(this).append('<td style="min-width:150px"><div class="input-group" id="att'+$(this).attr('aid')+ATTTIDuATTID[1]+'">'
					+'<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][ATTRIBUTE][D]['+ATTTIDuATTID[1]+'][ACTIVE]" value="1">'
					+'<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][ATTRIBUTE][D]['+ATTTIDuATTID[1]+'][VALUE]" value="'+$('#tbValue1').val()+'">'
					+'<div class="input-group-prepend"><button class="btn" onclick="if(confirm(\'Dieser Wert bleibt unverändert?\')){ $(\'#att'+$(this).attr('aid')+ATTTIDuATTID[1]+'\').empty();}" type="button"><i onclick="wp.window.close(\'ART{$D.PLATFORM_ID}all\');" style="font-size:13pt;color:red;" class="fa fa-minus-circle"></i></button></div>'
					+'</div></td>');
		}
		else
			$(this).append('<td></td>');
	});
	$("#ddl_Attribut option[value='"+$('#ddl_Attribut').val()+"']").remove();//Eintrag aus der DDL löschen, weil bereits verwendert
}

add_pl_attribut = function()
{
	$('#tb{$D.PLATFORM_ID}_art thead tr').append('<td><b>'+$('#ddl_Platform option:selected').text()+'</b></td>');
	$('#tb{$D.PLATFORM_ID}_art').find('tbody tr').each(function(){
		if($('#addAtt option:selected').val() == $( this ).data( 'type' ) )
		{
			arVal = $('#ddl_Platform').val().split('|');
			if(arVal[3] == 1)//ist i18n
				$(this).append('<td style="min-width:150px"><div class="input-group" id="att'+$(this).attr('aid')+arVal[0]+arVal[1]+arVal[2]+'">'
					+'<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][PLATFORM][D]['+arVal[0]+'][ATTRIBUTE][D]['+arVal[2]+'][LANGUAGE][D][DE][ACTIVE]" value="1">'
					+'<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][PLATFORM][D]['+arVal[0]+'][ATTRIBUTE][D]['+arVal[2]+'][LANGUAGE][D][DE][VALUE]" value="'+$('#tbValue2').val()+'">'
					+'<div class="input-group-prepend"><button class="btn" onclick="if(confirm(\'Dieser Wert bleibt unverändert?\')){ $(\'#att'+$(this).attr('aid')+arVal[0]+arVal[1]+arVal[2]+'\').empty();}" type="button"><i onclick="wp.window.close(\'ART{$D.PLATFORM_ID}all\');" style="font-size:13pt;color:red;" class="fa fa-minus-circle"></i></button></div>'
					+'</div></td>');
			else
				$(this).append('<td style="min-width:150px"><div class="input-group" id="att'+$(this).attr('aid')+arVal[0]+arVal[1]+arVal[2]+'">'
					+'<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][PLATFORM][D]['+arVal[0]+'][ATTRIBUTE][D]['+arVal[2]+'][ACTIVE]" value="1">'
					+'<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]['+$(this).attr('aid')+'][PLATFORM][D]['+arVal[0]+'][ATTRIBUTE][D]['+arVal[2]+'][VALUE]" value="'+$('#tbValue2').val()+'">'
					+'<div class="input-group-prepend"><button class="btn" onclick="if(confirm(\'Dieser Wert bleibt unverändert?\')){ $(\'#att'+$(this).attr('aid')+arVal[0]+arVal[1]+arVal[2]+'\').empty();}" type="button"><i onclick="wp.window.close(\'ART{$D.PLATFORM_ID}all\');" style="font-size:13pt;color:red;" class="fa fa-minus-circle"></i></button></div>'
					+'</div></td>');
		}
		else
			$(this).append('<td></td>');
			
	});
	$("#ddl_Platform option[value='"+$('#ddl_Platform').val()+"']").remove();//Eintrag aus der DDL löschen, weil bereits verwendert
}
</script>
{*
<script>
SAVEART{$D.PLATFORM_ID}all = function()
{				
	wp.ajax({
	'url' : '?D[PAGE]=platform.articles&D[ACTION]=set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
	'div' : 'ajax',
	'data': $('#form{$D.PLATFORM_ID}all').serialize()
	});
			
}
</script>
*}