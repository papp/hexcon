{switch $D.ACTION}
	{case 'get_message'}
		{foreach from=$D.MESSAGE.D key="kMES" item="MES"}
			{if $DATE != $MES.DATETIME|date_format:"%d.%m.%Y"}
				<div style="text-align:center;;margin:1px;clear:both;"><lable style='background:#eee;padding:0px 20px; border:solid 1px #ddd;border-radius:10px'>{$MES.DATETIME|date_format:"%d.%m.%Y"}</lable></div>
				{$USER = 'xxx'}
			{/if}
			{$DATE = $MES.DATETIME|date_format:"%d.%m.%Y"}

			{if $MES.USER_ID}
				{if $MES.INTERNALLY}
					

					<div style="clear:both;float:right;border:dotted 2px #cfc;margin:2px 2px 2px 10px;border-radius:5px;color:#555;background:#f5fff5;padding:4px;">
						<div style="border-bottom:solid 1px #cfc;height:14px;">
							<div style="float:left;">{$D.USER.D[$MES.USER_ID].FNAME}, {$D.USER.D[$MES.USER_ID].NAME}</div>
							<div style="float:right;">{$MES.DATETIME|date_format:"%H:%M"}</div>
						</div>
						{$MES.TEXT|nl2br}
					</div>

				{else}
					<div style="clear:both;float:right;border:solid 1px #9f9;margin:2px 2px 2px 10px;border-radius:5px;background:#efe;padding:4px;">
						<div style="border-bottom:solid 1px #9f9;height:14px;">
							<div style="float:left;">{$D.USER.D[$MES.USER_ID].FNAME}, {$D.USER.D[$MES.USER_ID].NAME}</div>
							<div style="float:right;">{$MES.DATETIME|date_format:"%H:%M"}</div>
						</div>
						{$MES.TEXT|nl2br}
					</div>
				{/if}
			{else}
				<div style="clear:both;float:left;border:solid 1px #ddd;margin:2px 10px 2px 2px;border-radius:5px;background:#eee;padding:4px;">
					<div style="border-bottom:solid 1px #ddd;height:14px;">
						<div style="float:left;">{$D.CUSTOMER.D[$MES.GROUP_ID].NICKNAME}</div>
						<div style="float:right;">{$MES.DATETIME|date_format:"%H:%M"}</div>
					</div>
					{$MES.TEXT|nl2br}
				</div>
			{/if}
		{/foreach}
	{/case}
	{default}
{*MESSAGE START ================*}
							<form id="form_message{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}{$D.MESSAGE.W.GROUP_ID}" method="post">
								<div style="height:100%;overflow:auto;overflow-y:auto;">
									<div style="border:solid 1px #9f9;margin:2px;border-radius:5px;background:#efe;padding:2px;">
										<table style="width:100%;background:transparent;" cellspacing="0">
											<tr>
												{$new_ID = ($smarty.now|date_format:"%Y%m%d%H%M%S")}
												<input name="D[MESSAGE][D][wp{$new_ID}][ACTIVE]" type="hidden" value="1">
												<input name="D[MESSAGE][D][wp{$new_ID}][GROUP_ID]" type="hidden" value="{$D.MESSAGE.W.GROUP_ID}">
												<input name="D[MESSAGE][D][wp{$new_ID}][PLATFORM_ID]" type="hidden" value="{$D.PLATFORM_ID}">
												<input name="D[MESSAGE][D][wp{$new_ID}][USER_ID]" type="hidden" value="{$D.SESSION.USER.ID}">
												<input name="D[MESSAGE][D][wp{$new_ID}][DATETIME]" type="hidden" value="{$smarty.now|date_format:"%Y%m%d%H%M%S"}">
												<td>
													{if $D.MESSAGE.W.INTERNALLY}
														<input name="D[MESSAGE][D][wp{$new_ID}][INTERNALLY]" value="{$D.MESSAGE.W.INTERNALLY}" type="hidden" >
													{else}
														<input name="D[MESSAGE][D][wp{$new_ID}][INTERNALLY]" value="1" type="checkbox" title="Intern, Kunde erhält keine Nachricht darüber!" checked>
													{/if}
												</td>
												<td><textarea name="D[MESSAGE][D][wp{$new_ID}][TEXT]" style="width:100%;" class="message"></textarea></td>
												<td valign="bottom"><button type="button" onclick="send_message{$kINV|replace:"-":"_"}{$D.MESSAGE.W.GROUP_ID}();">S</button></td>
											</tr>
										</table>
										<script>
											$('textarea.message').elasticArea();
										</script>
									</div>
									<div id="fmessage_box{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}{$D.MESSAGE.W.GROUP_ID}"></div>
									
								</div>
							</form>
								<script>
									//Lade MESSAGE
									get_message{$D.MESSAGE.W.GROUP_ID} = function()
									{
									wp.ajax({
														'url'	:	'?D[PAGE]=message',
														'data'	:	{
																		'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																		//'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		//'D[MESSAGE][W][CUSTOMER_ID]' : '{$INV.CUSTOMER_ID}',
																		'D[MESSAGE][W][GROUP_ID]' : '{$D.MESSAGE.W.GROUP_ID}', //group_id,
																		'D[MESSAGE][O][DATETIME]' : 'DESC',
																		{if $D.MESSAGE.W.INTERNALLY}'D[MESSAGE][W][INTERNALLY]' : '{$D.MESSAGE.W.INTERNALLY}',{/if}
																		'D[ACTION]' : 'get_message'
																	},
														'div'	:	'fmessage_box{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}{$D.MESSAGE.W.GROUP_ID}',
														'done'	:	function(){  }
														});
									}
									get_message{$D.MESSAGE.W.GROUP_ID}();
								</script>
					
							
								<script>
									send_message{$kINV|replace:"-":"_"}{$D.MESSAGE.W.GROUP_ID} = function()
									{
										wp.ajax({
										'url' : '?D[PAGE]=message&D[ACTION]=set_message&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
										'div' : 'ajax',
										'data': $('#form_message{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}{$D.MESSAGE.W.GROUP_ID}').serialize(),
										'done' : function(){ get_message{$D.MESSAGE.W.GROUP_ID}(); }
										});
			
									}
								</script>
						{*MESSAGE END ================*}
{/switch}