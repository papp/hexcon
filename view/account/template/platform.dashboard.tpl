		<div class="row m-2 mt-4">
			<div class="col mb-2">
				<div class="card" style="height:100%;">
					<div class="card-header">Umsatz</div>
     				<div class="card-body p-1">
						<canvas id="SALES" role="img"></canvas>
						<script>
						//labels = Utils.months({ count: 7});
						color = ['grey', 'red','lime','yellow','blue','fuchsia','aqua','black', 'maroon', 'green', 'olive' ,'navy', 'purple', 'teal','silver' ];
						data = { 
							//labels: labels,
							labels: [{for $i=1 to 12}'{str_pad($i, 2, 0, STR_PAD_LEFT)}',{/for}],
							datasets: [
							
							{for $y=date(Y)-5 to date(Y)}
								{if $D.PLATFORM.D[ $D.PLATFORM_ID ].STATISTIC.D['INVOICE'].DATE.D["{$y}"]}
								{ 
									label: '{$y}',
									data: [{for $i=1 to 12}{$i = str_pad($i, 2, 0, STR_PAD_LEFT)}'{$D.PLATFORM.D[ $D.PLATFORM_ID ].STATISTIC.D['INVOICE'].DATE.D["{$y}{$i}"].SALES}',{/for}],
									fill: false,
									borderColor: color[{$y-(date(Y)-5)}], //'rgb({70+(($y-date(Y))*30)}, {100+(($y-date(Y))*30)}, 200)',
									backgroundColor: color[{$y-(date(Y)-5)}],
									tension: 0.1
								},
								{/if}
							{/for}
						]
						};
						
						myChart = new Chart(
							document.getElementById('SALES'),
							{ 
								type: 'line',//'bar',
								data: data,
							}
						);
						</script>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
			<div class="col mb-2">
				<div class="card" style="height:100%;">
					<div class="card-header">{date(Y)} Umsatz je Land</div>
     				 <div class="card-body">
					  	<canvas id="SALES1" role="img"></canvas>
						<script>
						//labels = Utils.months({ count: 7});
						color = ['grey', 'red','lime','yellow','blue','fuchsia','aqua','black', 'maroon', 'green', 'olive' ,'navy', 'purple', 'teal','silver' ];
						color_counter = 0;
						data = { 
							//labels: labels,
							labels: [{for $i=1 to 12}'{str_pad($i, 2, 0, STR_PAD_LEFT)}',{/for}],
							datasets: [

							
								{foreach from=$D.PLATFORM.D[ $D.PLATFORM_ID ].STATISTIC.D['INVOICE'].DATE.D[date(Y)].COUNTRY.D key="kCOU" item="COU"}
										{ 
											label: '{$kCOU}',
											data: [{for $i=1 to 12}{$i = str_pad($i, 2, 0, STR_PAD_LEFT)}'{$D.PLATFORM.D[ $D.PLATFORM_ID ].STATISTIC.D['INVOICE'].DATE.D["{date(Y)}{$i}"].COUNTRY.D[{$kCOU}].SALES}',{/for}],
											fill: false,
											borderColor: color[color_counter++], 
											backgroundColor: color[color_counter],
											tension: 0.1
										},
								{/foreach}
						]
						};
						
						myChart = new Chart(
							document.getElementById('SALES1'),
							{ 
								type: 'bar',//'line',//'bar',
								data: data,
							}
						);
						</script>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
			

		</div>
		
	
	
