	<table style='height:100%;width:100%;' cellpadding="0" cellspacing="0">
		<tr>
			<td valign=top class="bg-light" style="width:150px;padding:2px;font-size:13px;">
				<div style="position:fixed;">
					<div style="text-align:center;">{$D.PLATFORM.D[$D.PLATFORM_ID].WAREHOUSE.D[$D.WAREHOUSE.W.ID].TITLE}</div>
					<nav class="navbar navbar-light">
						<ul class="nav nav-pills flex-column">

							{*<li style="text-align:center;font-size:20px;cursor:pointer;" onclick=""
							><a href="#"><i class="fa fa-share-square"></i> Eingang</a></li>*}
							{*
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf474; Ausgang</li>*}
							{*
							<li class="nav-link" onclick="alert('ACHTUNG: Diese Maske NICHT nutzen!');wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list3',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf474; Ausgang (alt)</li>*}
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list4',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf474; Ausgang</li>
							{foreach from=explode("|",$D.PLATFORM.D[$D.PLATFORM_ID].SETTING.D['AvailableShippingMethods'].VALUE) key="kSHI" item="SHI"}
								<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list4'+(($('#filter_shipping').val() != '')?'&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][SHIPPING_ID:IN]=\'{$SHI}\'' : ''),
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})">{i18n id="shipping_{$SHI}"}</li>
								
							{/foreach}
							{*foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
								<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list4'+(($('#filter_shipping').val() != '')?'&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][SHIPPING_ID:IN]=\'{$kSHI}\'' : ''),
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})">{$SHI.TITLE}</li>
							{/foreach*}
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list2',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf474; Ausgang v2 (nicht nutzen)</li>
							{*
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=warehouse.stocktaking',
													'data'	:	{
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[WAREHOUSE][W][ACTIVE]' : 1
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"><i class="fa fa-check-square-o"></i> Inventur alt</li>
							*}
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=warehouse.stocktaking2',
													'data'	:	{
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[WAREHOUSE][W][ACTIVE]' : 1
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"><i class="fa fa-check-square-o"></i> Inventur</li>
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=warehouse.article',
													'data'	:	{
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}'
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf02b; Artikel</li>
							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.buying_list',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][GROUP_ID:IN]' : 'buying',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][STATUS:IN]' : '20',
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							>&#xf48b; Eingang</li>

							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=warehouse.stocktaking_scan',
													'data'	:	{
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}',
																	'D[WAREHOUSE][W][ACTIVE]' : 1
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"><i class="fa fa-check-square-o"></i> Inventur Scan</li>

							<li class="nav-link" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=warehouse.setting',
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}',
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
							> Einstellungen</li>
						</ul>
					</nav>
				</div>
			</td>
			<td valign=top id='fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}'></td>
		</tr>
	</table>