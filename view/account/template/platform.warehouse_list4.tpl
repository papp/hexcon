{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'load_set_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			
			{if $kART == $PLA.ARTICLE.W.ID}
				{$SOLL = $D.BAY_STOCK * $D.SET.QUANTITY}
				<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
					<td style="text-align:center;font-size:30px;{if $D.SET.QUANTITY > 1}animation: blink 1s steps(2, start) infinite;{/if}">{$D.SET.QUANTITY}</td>
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_100x100.jpg"></td>
					<td>{$ART.NUMBER}<br><br>
						{if $PLA.ARTICLE.D[$kART].SUPPLIER.D}
							<b>Lieferantennummer:</b><br>
							{foreach from=$PLA.ARTICLE.D[$kART].SUPPLIER.D key="kSUP" item="SUP"}
								{$PLA.SUPPLIER.D[$kSUP].TITLE}: <b>{$SUP.NUMBER}</b><br>
							{/foreach}
						{/if}
					</td>
					<td>
						{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
							<div class="alert alert-warning p-1" style="color:red;">
								{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
								<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
								{for $i=0 to count((array)$aVAR)-1}
									{$PLA.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].TITLE}: {$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} <br> {/if}
								{/for}</b>
								{/if}
							</div>
							{$RCS['DELIMITER']['LEFT'] = '[('}
							{$RCS['DELIMITER']['RIGHT'] = ')]'}
							{$CWP->rand_choice_str($PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}
						{else}
							{$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}<br>
						{/if}
						
						{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
							<div class="alert alert-danger p-1">FEHLER: keine Zuordnung! Bitte Information im Kommentar beachten oder nachfragen!</div>
						{/if}
					</td>
					<td><label style="{if !$ART.WEIGHT}color:red{/if}">{$ART.WEIGHT*$SOLL} g (e.{$ART.WEIGHT} g)</label></td>
					<td style="text-align:right;">
						{foreach from=$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
							{if isset($D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK)}
								<div><b>{$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</b> 
								(<label id="#IST{$D.WAREHOUSE_ID}{$kSTO}{$kART}">{$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}</label>{if $D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK != $D.IST[$kSTO]} => <label style="color:red;">{$D.IST[$kSTO]}</label>{/if}) 
								<input style="width:50px;text-align:right;" group="STOCK" data-kSTO="{$kSTO}" data-kART="{$kART}" onkeyup="set_warehouse_stock('{$D.WAREHOUSE_ID}');" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$D.DELIVERY_ID}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" 
								value="{if $SOLL >= $D.IST[$kSTO]}{$SOLL = $SOLL-$D.IST[$kSTO]}{$D.IST[$kSTO]}{else}{$SOLL}{$SOLL = 0}{/if}"></div>
							{/if}
						{/foreach}
						{if $SOLL}
							<div style="animation: blink 1s steps(2, start) infinite;">Überbuchung: {$SOLL}St. fehlen</div>
						{/if}
							
					</td>
					<td>
						<div style="font-size:30px;text-align:center;{if $SOLL}color:red{else}color:green;{/if}"><b>{$D.BAY_STOCK * $D.SET.QUANTITY}</b></div>
					</td>
				</tr>
			{/if}
		{/foreach}
	{/case}
	{default}
	{*
	<button type="button" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.warehouse_list2',
												'data'	:	{
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['STATUS']}',
															},
												'div'	:	'fPLA{$D.PLATFORM_ID}',ASYNC:1
												})">NEUE LAGER</button>
												*}
	<script>
	SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID} = function()
	{
		$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').find(':input').each(function(){
			if( $(this).attr( "ischanged" ) == 0 )
				$(this).attr( "disabled", true );
		});
		wp.ajax({
		'url' : '?D[PAGE]=platform.warehouse_list4&D[ACTION]=set_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
		'div' : 'ajax',
		'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize(),
		'done': function(){ set_soll_stock_zero_after_save('{$PLA.DELIVERY.W.WAREHOUSE_ID}');}
		});
		//set_soll_stock_zero_after_save('{$PLA.DELIVERY.W.WAREHOUSE_ID}');
		$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').find(':input').each(function(){
			$(this).attr( "disabled", false );
		});
	}
	</script>
	
	<style>@keyframes blink { to { color: red; }}</style>
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
		
		<div style="background:#ddd;position:fixed;width:100%;padding-top:5px;padding-bottom:5px;z-index:1;">
			<input type="hidden" name="D['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
			<input type="hidden" name="D['PLATFORM_ID']" value="{$D.PLATFORM_ID}">
			<button class="btn btn-primary" type="button" id="btnSave" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>

			Bestellungen: {count((array)$PLA.DELIVERY.D)}
			<button class="btn btn-primary" onclick="$('#funcDDL').toggle();" type="button">Funktionen</button>
			<div id="funcDDL" style="display:none;width:100%;position:absolute;background:#fff;padding:5px;border:solid 1px #ddd;overflow-y:scroll;">
			{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
				{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
				<button class="btn btn-primary" type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]=20&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][O][NUMBER]=ASC&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]={$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.W.WAREHOUSE_ID}{if $D.PLATFORM.D[{$D.PLATFORM_ID}].DELIVERY.W['SHIPPING_ID:IN']}&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][SHIPPING_ID:IN]={str_replace("'","\\'",$D.PLATFORM.D[{$D.PLATFORM_ID}].DELIVERY.W['SHIPPING_ID:IN'])}{/if}','_blank');" title="Export Inv:{$INV.NUMBER}">
				{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
				</button>
				{/if}
			{/foreach}
			</div>

			<select id="filter_shipping" onchange="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list4'+((this.value != '')?'&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][SHIPPING_ID:IN]=\''+this.value+'\'' : ''),
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.W.WAREHOUSE_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})"
													{*$('.list').each(function(){ if($(this).attr('data-shipping')){ $(this).hide(); } });$('.list[data-shipping*=\'|'+this.value+'|\']').show();*}>
				<option value=''>alle</option>
				{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
					<option value='{$kSHI}' {if $D.PLATFORM.D[{$D.PLATFORM_ID}].DELIVERY.W['SHIPPING_ID:IN'] == "'{$kSHI}'"}selected{/if}>{$SHI.TITLE}</option>
				{/foreach}
			</select><button class="btn" title="Seite neu laden" type="button" onclick="wp.ajax({
													'url'	:	'?D[PAGE]=platform.warehouse_list4'+(($('#filter_shipping').val() != '')?'&D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][SHIPPING_ID:IN]=\''+$('#filter_shipping').val()+'\'' : ''),
													'data'	:	{
																	
																	'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][WAREHOUSE_ID]' : '{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.W.WAREHOUSE_ID}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][STATUS]' : 20
																},
													'div'	:	'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
													'ASYNC'	: 1
													})">aktuallisieren</button>
			<button class="btn btn-primary" type="button" id="btnSave" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>
		</div>
		
		<div style="height:30px;"></div>

		<div>
			{foreach from=$D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
				{foreach from=$STO.ARTICLE.D key="kART" item="ART"}
				{if isset($ART.STOCK) && $ART.STOCK > 0}
					<div style="float:left;display:none;border:#999 solid 1px;margin:1px; padding:1px;" group="STOCK_SOLL" id="WSA{$PLA.DELIVERY.W.WAREHOUSE_ID}{$kSTO}{$kART}">
						{$PLA.ARTICLE.D[$kART].NUMBER} | {$STO.TITLE}<br>
						<input id="WSA{$PLA.DELIVERY.W.WAREHOUSE_ID}{$kSTO}{$kART}STOCK_IST" style="width:40px;background:#85ffac;text-align:right;" value="{$ART.STOCK}">
						<input id="WSA{$PLA.DELIVERY.W.WAREHOUSE_ID}{$kSTO}{$kART}STOCK_SOLL" group="STOCK_SOLL" style="width:40px;background:#ffaa8f;text-align:right;" name="D[WAREHOUSE][D][{$PLA.DELIVERY.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][GET_STOCK]" value="0">
					</div>
				{/if}
				{/foreach}
			{/foreach}
			<script>
				set_warehouse_stock = function(kwar)
				{
					$("input[group='STOCK_SOLL']").val(0);//Setze auf 0 vor neu Berechnung
					$("div[group='STOCK_SOLL']").hide();
					$("table[group='invoice']").each(function(){
						if($(this).find("input[group='status']:checked").val() == 40 ) //Wurde auf Fertig gesetzt
						{
							$(this).find("input[group='STOCK']").each(function(){
								kart = $(this).data('kart');
								ksto = $(this).data('ksto');
								
								if(parseInt( $(this).val() ) > 0)
								{
									$('#WSA'+kwar+ksto+kart+'STOCK_SOLL').val(
										parseInt($('#WSA'+kwar+ksto+kart+'STOCK_SOLL').val()) + parseInt( $(this).val() )
									);
									$('#WSA'+kwar+ksto+kart).show();

								}
							});
						}
						
					});
				}

				set_soll_stock_zero_after_save = function(kwar)
				{
					$("table[group='invoice']").each(function(){
						if($(this).find("input[group='status']:checked").val() == 40 ) //Wurde auf Fertig gesetzt
						{
							$(this).find("input[group='STOCK']").val('0');
							$(this).find("input[group='STOCK']").css('background','green');
						}
					});
					set_warehouse_stock(kwar);//berechne erneut
					$( "#btnSave" ).prop( "disabled", false );
				}
			</script>
		</div>



		{foreach from=$PLA.DELIVERY.D key="kINV" item="INV"}
			<table class='list' group="invoice" data-shipping="|all| |{$INV.SHIPPING_ID}|" style='width:100%;{IF $INV.CUSTOMER_ID == 'pxx'}background:pink{/if}'>
				<thead>
					<tr>
						<td style="width:30px;"></td>
						<td style="width:150px;">ID</td>
						<td style="width:60px;">Status</td>
						<td>Absendeadresse</td>
						<td>Lieferadresse</td>
						<td style="width:150px;">Versand</td>
						<td style="width:150px;">Kommentar</td>
						<td style="width:200px;"></td>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td><input type="hidden" value='{$INV.PLATFORM_ID}' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][PLATFORM_ID]"></td>
							<td><b>Lieferschein</b>
								N<div style="font-size:20px;border:dotted 2px #999;text-align:center;">{substr($INV.NUMBER,0,strlen($INV.NUMBER)-3)}<b style="color:red;">{substr($INV.NUMBER,-3)}</b></div>
								Erstelt am: {$INV.ITIMESTAMP|date_format:"%d.%m.%Y"}<br>
								ID: {$kINV}<br>
								<br>
								<b>Kunde</b><br>
								E-Mail: {$INV.DELIVERY_EMAIL}<br>
								{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
									{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'order' && $SET.VARIANTE.D['ACTIVE'].VALUE}
									<button class="btn btn-primary" type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][ID]={$kINV}','_blank');" title="Export Lieferschein:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
									{/if}
								{/foreach}
							</td>
							<td onmouseover="if(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][TRACKING_NO]').value == '') $('#tr{$D.PLATFORM_ID}{$kINV}').show(); else $('#tr{$D.PLATFORM_ID}{$kINV}').hide();" style="text-align:ceter;width:100px;background:{if $INV.STATUS >=40}green{else if $INV.STATUS >=20}yellow{else}red{/if}">

<!-- Default unchecked -->
{input p=['type' => 'radio', 'option' => [ '0' => 'offen', '9' => 'Klärung', '20' => 'Versandfreigabe', '40' => 'Fertig' ], 'onclick' => "set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');", 'value' => $INV.STATUS, 'group' => 'status', 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]" ]}
<div style="height:20px;">{if $INV.SHIPPING_ID != 'DP'}<div id="tr{$D.PLATFORM_ID}{$kINV}" style="color:red;text-align:center;display:none;font-size:15px;"><b>Tracking?</b></div>{/if}</div>

{*
								<input type="radio" value='0' onclick="set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');" group="status" {if $INV.STATUS ==0}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]">offen<br>
								<input type="radio" value='9' onclick="set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');" group="status" {if $INV.STATUS ==9}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]">Klärung<br>
								<input type="radio" value='20' onclick="set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');" group="status" {if $INV.STATUS ==20}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]">Versandfreigabe<br>
								<input type="radio" value='40' onclick="set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');" group="status" {if $INV.STATUS ==40}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]">Fertig<br>
								*}
								{*
								<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][STATUS]">
									<option value='0' {if $INV.STATUS ==0}selected{/if}>offen</option>
									<option value='20' {if $INV.STATUS ==20}selected{/if}>Versandfreigabe</option>
									<option value='40' {if $INV.STATUS ==40}selected{/if}>Fertig</option>
									</select>*}
							</td>
							<td style="background:#fff;font-size:13px;">
								{$INV.RETURN_COMPANY}<br>
								{$INV.RETURN_FNAME} {$INV.RETURN_NAME}<br>
								{$INV.RETURN_STREET} {$INV.RETURN_STREET_NO}<br>
								{if $INV.RETURN_ADDITION}{$INV.RETURN_ADDITION}<br>{/if}
								{$INV.RETURN_ZIP} {$INV.RETURN_CITY}<br>
								{if $INV.RETURN_COUNTRY_ID}{i18n id="country_{$INV.RETURN_COUNTRY_ID}"}{/if}
							</td>
							<td style="background:#fff;font-size:13px;">
								{if strpos(strtolower($INV.DELIVERY_STREET), "packstation") !== false || strpos(strtolower($INV.DELIVERY_ADDITION), "packstation") !== false}
								<style>
								@keyframes blinker {  
									50% { opacity: 0; }
									}
								</style>
								<div style="animation: blinker 1s linear infinite;color:Red;"><b>PACKSTATION</b></div>{/if}
									{$INV.DELIVERY_COMPANY}<br>
									{$INV.DELIVERY_FNAME} {$INV.DELIVERY_NAME}<br>
									{$INV.DELIVERY_STREET} {$INV.DELIVERY_STREET_NO}<br>
									{if $INV.DELIVERY_PHONE}Tel:{$INV.DELIVERY_PHONE}<br>{/if}
									{if $INV.DELIVERY_ADDITION}{$INV.DELIVERY_ADDITION}<br>{/if}
									{$INV.DELIVERY_ZIP} {$INV.DELIVERY_CITY}<br>
									{i18n id="country_{$INV.DELIVERY_COUNTRY_ID}"}
							</td>
							<td style="text-align:right;">
								</select>
								<div style="{if $INV.SHIPPING_ID != 'DP'}background-color:yellow;{/if}">Traking: {input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][TRACKING_NO]", 'value'=>$INV.TRACKING_NO]}</div>
								
								Datum: {*input p=['type'=>'text', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][SHIPPED_DATE]", 'value'=>$smarty.now|date_format:"%Y-%m-%d" ]*}
								{*ToDo: Info: wird noch alte input verwendet, weil der Value sich nciht aktuallisiert und so auch ischaged=0 ist*}
								<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][SHIPPED_DATE]" value="{$smarty.now|date_format:"%Y-%m-%d"}">
								Versandfirma:<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][SHIPPING_ID]">
									<option value=""></option>
									{*foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
										<option value='{$kSHI}' {if $INV.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
									{/foreach*}
									{foreach from=explode("|",$D.PLATFORM.D[$D.PLATFORM_ID].SETTING.D['AvailableShippingMethods'].VALUE) key="kSHI" item="SHI"}
										<option value='{$SHI}' {if $INV.SHIPPING_ID == $SHI}selected{/if}>{i18n id="shipping_{$SHI}"}</option>
									{/foreach}
									
								</select>
								{*input p=['type'=>'select', 'option' => ['1' => 'value','2' => 'value'] ]*}
								{*
								<button type="button" class="btn" onclick="$(this).prop( 'disabled', true );
										wp.ajax({
															'url'	:	'?D[PAGE]=platform.warehouse_list4',
															'data'	:	{
																			'D[ACTION]' : 'create_shipping',
																			'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																			'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																			'D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][W][ID]' : '{$kINV}',
																		}
															});
										">Label</button>*}
								<div style="{if !$INV.ARTICLE.WEIGHT}color:red{/if}">Gewicht: {round($INV.ARTICLE.WEIGHT/1000,2)} kg</div>
							</td>
							<td style="text-align:right;">	
								{*<textarea class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][COMMENT]' style='width:200px;height:100px;'>{$INV.COMMENT}</textarea>*}
								{input p=['type'=> 'textarea', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][COMMENT]", 'value'=>$INV.COMMENT, 'style'=>'width:200px;height:100px;']}
											
							</td>
							<td rowspan="2" valign="top">
							{*MESSAGE START ================*}
								{if $INV.CUSTOMER_ID}
								<div style="height:180px;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}"></div>
									<script>
										wp.ajax({
															'url'	:	'?D[PAGE]=message',
															'data'	:	{
																			'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																			'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																			'D[MESSAGE][W][GROUP_ID]' : '{$INV.CUSTOMER_ID}', //group_id,
																			'D[MESSAGE][O][DATETIME]' : 'DESC',
																			'D[MESSAGE][W][INTERNALLY]' : '1'
																		},
															'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}'
															});
									</script>
								{/if}
							{*MESSAGE END ================*}
							</td>
						</tr>
						{*
						<tr>
							<td colspan=9>
								<table class="table">
									<tr>
										<td>Versandfirma</td>
										<td>Nummer</td>
										<td>Status</td>
										<td>Date</td>
										<td></td>
									</tr>
								{foreach from=$INV.SHIPPING.D key="kSHI" item="SHI"}
									{foreach from=$SHI.SHIPMENT.D key="kSHIPMENT" item="SHIPMENT"}
										<tr>
											<td>{$kSHI}</td>
											<td>{$kSHIPMENT}</td>
											<td>{$SHIPMENT.STATUS}</td>
											<td>{$SHIPMENT.ITIMESTAMP|date_format:"%d.%m.%Y"}</td>
											<td>{if $SHIPMENT.STATUS == 0}<button  type="button" class="btn">Label</button> <button type="button" class="btn">x</button>{/if}</td>
										</tr>
									{/foreach}
								{/foreach}
								</table>
							</td>
						</tr>
*}
						<tr style="border-bottom:5px red solid;">
							<td colspan="9">
								<table  style="width:100%;">
									<thead>
										<tr>
											<td style="width:15%"></td>
											<td style="width:10%">Anz</td>
											<td style="width:10%">Bild</td>
											<td style="width:10%">Num</td>
											<td>Title</td>
											<td style="width:10%">Gewicht</td>
											<td style="width:15%">Lager</td>
											<td style="width:10%">Soll Summe</td>
										</tr>
									</thead>
									
								{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
									
												<tbody id="SET{$D.PLATFORM_ID}{$kINV}{$kART}" style="border-bottom:solid 2px red;">
													<tr style="height:1px;">
														{if $PLA.ARTICLE.D[$kART].SET}
															<td style="text-align:center;vertical-align:middle;width:150px;border-right:dotted 2px blue;" rowspan={count($PLA.ARTICLE.D[$kART].SET.ARTICLE.D)+1}>
																<table style="width:100%;"><tr><td style="border:none;">
																<h2>SET</h2>
																<div title="<nobr>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
																</nobr>">
																	<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_50x50.jpg">
																</div>
																{$ART.NUMBER}<br>
																{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																	<div style="color:red;">
																	{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																	<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
																	{for $i=0 to count($aVAR)-1}
																		{$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
																	{/for}</b></div>
																	{/if}
																{else}
																	{$ART.TITLE}<br>
																{/if}
																</td>
																<td style="border:none;text-align:center;font-size:30px;vertical-align:middle;{if $ART.STOCK > 1}animation: blink 1s steps(2, start) infinite;{/if}">{if $PLA.ARTICLE.D[$kART].SET}{$ART.STOCK}x{/if}
																</td></tr></table>
															</td>
															{foreach from=$PLA.ARTICLE.D[$kART].SET.ARTICLE.D key="kSET" item="SET"}
																<script>
																wp.ajax({
																'url' : '?D[PAGE]=platform.warehouse_list4&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[INVOICE_ID]={$kINV}&D[ARTICLE_ID]={$kART}',
																'div' : 'SET{$D.PLATFORM_ID}{$kINV}{$kART}',
																
																'INSERT' : 'append',
																'data'	: {
																	//'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]' : '{$kART}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
																	'D[SET][QUANTITY]' : '{$SET.QUANTITY}',
																	'D[BAY_STOCK]' : '{$ART.STOCK}',
																	'D[WAREHOUSE_ID]'	: '{$PLA.DELIVERY.W.WAREHOUSE_ID}',
																	'D[INVOICE_ID]' : '{$kINV}',
																	
																	//kommentar
																	{$SOLL = $SET.QUANTITY * $ART.STOCK}
																	{foreach from=$D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
																		{if $STO.ARTICLE.D[$kSET].STOCK > 0}
																			{if !isset($IST[{$kSTO}][$kSET])}
																				{$IST[{$kSTO}][$kSET] = $STO.ARTICLE.D[$kSET].STOCK}
																			{/if}
																				'D[IST][{$kSTO}]' : {$IST[$kSTO][$kSET]},
																				{if $SOLL >= $IST[$kSTO][$kSET]}
																					{$SOLL = $SOLL-$IST[$kSTO][$kSET]}
																					{$IST[$kSTO][$kSET] = 0}
																				{else}
																					{$IST[$kSTO][$kSET] = $IST[$kSTO][$kSET]-$SOLL}
																					{$SOLL = 0}
																				{/if}
																		{/if}
																	{/foreach}
																		}
																});
																</script>
															{/foreach}
														{else}
																<td></td>
																<td style="text-align:center;font-size:30px;vertical-align:middle;{if $ART.STOCK > 1}animation: blink 1s steps(2, start) infinite;{/if}">{if !$PLA.ARTICLE.D[$kART].SET}{$ART.STOCK}{/if}</td>
																<td title="<nobr>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
																</nobr>">
																	<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_100x100.jpg">
																</td>
																<td>{$ART.NUMBER}<br><br>
																{if $PLA.ARTICLE.D[$kART].SUPPLIER.D}
																	<b>Lieferantennummer:</b><br>
																	{foreach from=$PLA.ARTICLE.D[$kART].SUPPLIER.D key="kSUP" item="SUP"}
																	<div class="alert-warning p-1 m-1">
																		{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE}: <b>{$SUP.NUMBER}</b>
																	</div>
																	{/foreach}
																{/if}
																</td>
																<td>
																	{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																		<div class="alert alert-warning p-1" style="color:red;">
																			<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
																			{for $i=0 to count($aVAR)-1}
																				{$PLA.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].TITLE}: {$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} <br> {/if}
																			{/for}</b>
																		</div>
																		{if $ART.TITLE}
																			{$ART.TITLE}
																		{else}
																			{$RCS['DELIMITER']['LEFT'] = '[('}
																			{$RCS['DELIMITER']['RIGHT'] = ')]'}
																			{$CWP->rand_choice_str($PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}
																		{/if}
																	{else}
																		{$ART.TITLE}<br>
																	{/if}
																	{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
																		<div class="alert alert-danger p-1">FEHLER: keine Zuordnung! Bitte Information im Kommentar beachten oder nachfragen!</div>
																	{/if}
																</td>
																<td><label style="{if !$ART.WEIGHT}color:red{/if}">{$ART.WEIGHT*$ART.STOCK} g (e.{$ART.WEIGHT} g)</label></td>
																<td style="text-align:right;">
																	{if !$PLA.ARTICLE.D[$kART].SET}
																		{$SOLL = $ART.STOCK}
																		{foreach from=$D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
																			{if !isset($IST[$kSTO][$kART])}
																				{$IST[$kSTO][$kART] = $D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
																			{/if}
																			{if $D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK > 0}
																				<div><b>{$D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</b> 
																				(<label id="#IST{$PLA.DELIVERY.W.WAREHOUSE_ID}{$kSTO}{$kART}">{$D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}</label>{if $D.WAREHOUSE.D[$PLA.DELIVERY.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK != $IST[$kSTO][$kART]} => <label style="color:red;">{$IST[$kSTO][$kART]}</label>{/if}) 
																				<input style="width:50px;text-align:right;" group="STOCK" data-kSTO="{$kSTO}" data-kART="{$kART}" onkeyup="set_warehouse_stock('{$PLA.DELIVERY.W.WAREHOUSE_ID}');" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kINV}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$PLA.DELIVERY.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" 
																				value="{if $SOLL >= $IST[$kSTO][$kART]}{$SOLL = $SOLL-$IST[$kSTO][$kART]}{$IST[$kSTO][$kART]}{$IST[$kSTO][$kART] = 0}{else}{$IST[$kSTO][$kART] = $IST[$kSTO][$kART]-$SOLL}{$SOLL}{$SOLL = 0}{/if}"><div>
																			{/if}
																		{/foreach}
																		{if $SOLL}
																			<div style="animation: blink 1s steps(2, start) infinite;">Überbuchung: {$SOLL}St. fehlen</div>
																		{/if}
																		
																	{/if}
																</td>
																<td>
																	<div style="font-size:30px;text-align:center;{if $SOLL}color:red{else}color:green;{/if}"><b> {$ART.STOCK}</b></div>
																</td>
															
															{/if}
													</tr>
									</tbody>
								{/foreach}
									
								</table>
							</td>
							

						</tr>
					
						
					</tbody>
				</table>
			{/foreach}
				
			</form>
		{/switch}
