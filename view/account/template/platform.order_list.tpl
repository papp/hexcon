<!--Order-->
<div>
	<table class="table">
		<thead>
			<tr>
				<td>Nr</td>
				<td>ID</td>
				<td style="width:60px;">Datum</td>
				<td>Name</td>
				<td>Nickname</td>
				<td style="width:60px;">Netto</td>
				<td style="width:60px;">MwSt</td>
				<td style="width:60px;">Brutto</td>
			</tr>
		</thead>

		<tbody>
			{foreach from=$D.PLATFORM.D key="kPLA" item="PLA"}
				{foreach from=$PLA.ORDER.D key="kORD" item="ORD" name="ORD"}
				<tr>
					<td>{$smarty.foreach.ORD.iteration}</td>
					<td>{$kORD}</td>
					<td>{$ORD.ITIMESTAMP|date_format:"%d.%m.%Y"}</td>
					<td>{$ORD.BILLING.FAME} {$ORD.BILLING.NAME}</td>
					<td>{$ORD.CUSTOMER_NICKNAME}</td>
					<td style="text-align:right;">{$ORD.ARTICLE.PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$ORD.ARTICLE.VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{($ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT)|number_format:2:",":"."}</td>
					{$INVOICE_PRICE = $INVOICE_PRICE + $ORD.ARTICLE.PRICE}
					{$INVOICE_VAT = $INVOICE_VAT + $ORD.ARTICLE.VAT}
					{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + $ORD.ARTICLE.PRICE + $ORD.ARTICLE.VAT}
				</tr>
				{/foreach}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
			<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
				<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
				<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
			</tr>
		</tfoot>
	</table>
</div>
