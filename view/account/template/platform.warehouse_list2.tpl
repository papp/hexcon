{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}



{function name=fcn_art}
	<tr>
		<td style="width:50px;text-align:center;font-size:25px;">
			<div style="{if $ART.STOCK > 1}animation: blinker 1s linear infinite;color:Red;{/if}">
				{if $ART.QUANTITY}{$ART.STOCK * $ART.QUANTITY}{else}{$ART.STOCK}{/if}
			</div>
			<div style="font-size:12px;">{if $ART.QUANTITY}({$ART.STOCK} * {$ART.QUANTITY}){/if}</div>
		</td>
		<td style="width:100px;" title="<nobr>
			<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
			<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
			<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
		</nobr>">
			<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_100x100.jpg">
		</td>
		<td style="font-size:15px;">{$ART.NUMBER}<br>{$ART.TITLE}</td>
		<td style="width:200px;">
			{if $PLA.ARTICLE.D[$kART].SET.ARTICLE.D}
				{*foreach from=$PLA.ARTICLE.D[$kART].SET.ARTICLE.D key="kSET" item="SET"}
					{$ART2['STOCK'] = $ART.STOCK}
					{$ART2['QUANTITY'] = $SET.QUANTITY}
					{$kART = $kSET}
					<table class="table" >
					{fcn_art ART=$ART2 D=$D}
					</table>
				{/foreach*}
			{else}
				{**KEIN SET***********************}
				
				{if $ART.QUANTITY}
					{$_SOL = $ART.STOCK * $ART.QUANTITY}
				{else}
					{$_SOL = $ART.STOCK}
				{/if}
				{$_SOL = $_SOL - $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$kINV}].TRANSACTION.ARTICLE.D[{$kART}].STOCK}
				<input type="hidden" value="{$_SOL}" id="SOL_{$kART}">
				<table class="table">
					<thead>
						<tr>
							<td style="text-align:center;">LAGER</td>
							<td style="text-align:right;">SOLL</td>
							<td style="text-align:right;">IST</td>
						</tr>
					</thead>
					{foreach from=$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
						{if $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
							{$_IST = $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
							<input type="hidden" value="{$_IST}" id="IST_{$kART}_{$kSTO}">
							
							{$SUM_IST = $SUM_IST + $_IST}
							{$SUM_SOL = $SUM_SOL + $_SOL}
						
							{if $_IST >= $_SOL}
								{$_IST = $_IST - $_SOL}
								{$SOL = $_SOL}
								{$_SOL = 0}
							{else}
								{$SOL = $_IST}
								{$_SOL = $_SOL - $_IST}
								{$_IST=0}
							{/if}
						
							
							{$SOL = $SOL - $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$kINV}].TRANSACTION.WAREHOUSE.D[{$PLA.INVOICE.W.WAREHOUSE_ID}].STORAGE.D[{$kSTO}].ARTICLE.D[{$kART}].STOCK}
							{if $SOL <0}{$SOL=0}{/if}
							<tr>
								<td style="text-align:center;">{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</td>
								<td style="text-align:right;">
									{*<input name="D[WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" style="width:30px;text-align:right;" value="{$SOL}">*}
									<input type="hidden" class="SOLL_ACTIVE" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][ACTIVE]" value="-2">
									<input onkeyup="//$('#IST{$PLA.INVOICE.W.WAREHOUSE_ID}{$kSTO}{$kART}').val( (this.value > 0)? {$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}-this.value:'{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}');" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" style="width:30px;text-align:right;" value="{$SOL}">
									{*$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$kINV}].TRANSACTION.ARTICLE.D[{$kART}].STOCK*}
								</td>
								<td style="text-align:right;">{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
									<input type="hidden" id="IST{$PLA.INVOICE.W.WAREHOUSE_ID}{$kSTO}{$kART}" name="D[WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" value="{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}">
								</td>
							</tr>
						{/if}
					{/foreach}
						{if $SUM_SOL > $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$kINV}].TRANSACTION.ARTICLE.D[{$kART}].STOCK}
							{$SUM_SOL = $SUM_SOL - $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$kINV}].TRANSACTION.ARTICLE.D[{$kART}].STOCK}
						{else}
							{$SUM_SOL = 0}
						{/if}
					<tr>
						<td></td>
						<td style="text-align:right;">{$SUM_SOL}</td>
						<td style="text-align:right;">{$SUM_IST}</td>
					</tr>
				</table>
			{/if}
		</td>
	</tr>
	
	
	{if $PLA.ARTICLE.D[$kART].SET.ARTICLE.D}
	<tr>
		<td></td>
		<td colspan=3>
		{foreach from=$PLA.ARTICLE.D[$kART].SET.ARTICLE.D key="kSET" item="SET"}
			{$ART2['STOCK'] = $ART.STOCK}
			{$ART2['QUANTITY'] = $SET.QUANTITY}
			{$kART = $kSET}
			<table class="table">
			{fcn_art ART=$ART2 D=$D}
			</table>
		{/foreach}
	</td>
	{/if}
		
{/function}


{switch $D.ACTION}
	{case 'load_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			{if $kART == $PLA.ARTICLE.W['ID|ID2PARENT']}
				{if $ART.PARENT_ID}
					{$ART_ID = $ART.PARENT_ID}
				{else}
					{$ART_ID = $kART}
				{/if}
				{if $PLA.ARTICLE.D[$ART_ID].SET|count > 0}
					<table style="font-size:15px;">
					<tr>
						<td>Anzahl</td>
						<td>Bild</td>
						<td>Num</td>
						<td>Title</td>
					</tr>
					<tbody id="SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}"></tbody>
				</table>
				{foreach from=$PLA.ARTICLE.D[$ART_ID].SET.ARTICLE.D key="kSET" item="SET"}
			<script>
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
				'div' : 'SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}',
				'INSERT' : 'append',
				'data'	: {
					'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
					'D[SET][QUANTITY]' : '{$SET.QUANTITY}'
				}
				});
			</script>
				{/foreach}
				{/if}
			{/if}
		
		
			{*if $kART == $PLA.ARTICLE.W.ID}{ *PARENT* }
				<table style="font-size:15px;">
					<tr>
						<td>Anzahl</td>
						<td>Bild</td>
						<td>Num</td>
						<td>Title</td>
					</tr>
					<tbody id="SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}"></tbody>
				</table>
				{foreach from=$ART.SET.ARTICLE.D key="kSET" item="SET"}
			<script>
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
				'div' : 'SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}',
				'INSERT' : 'append',
				'data'	: {
					'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
					'D[SET][QUANTITY]' : '{$SET.QUANTITY}'
				}
				});
			</script>
				{/foreach}
			{else}{ *VARIANTE* }
				{foreach from=$ART.VARIANTE.D key="kVAR" item="VAR"}
					{if $kVAR == $PLA.ARTICLE.W.ID}
						<table style="font-size:15px;">
							<tr>
								<td>Anzahl</td>
								<td>Bild</td>
								<td>Num</td>
								<td>Title</td>
							</tr>
							<tbody id="SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}"></tbody>
						</table>
						{foreach from=$VAR.SET.ARTICLE.D key="kSET" item="SET"}
							<script>
								wp.ajax({
								'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
								'div' : 'SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}',
								'INSERT' : 'append',
								'data'	: {
									'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
									'D[SET][QUANTITY]' : '{$SET.QUANTITY}'
								}
								});
							</script>
						{/foreach}
					{/if}
				{/foreach}
			{/if*}
		{/foreach}
	{/case}
	{case 'load_set_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			{*if $kART == $PLA.ARTICLE.W.ID}{ *PARENT*}
				<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
					<td style="font-size:20px;text-align:right;">{$D.SET.QUANTITY}</td>
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_50x50.jpg"></td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].LANGUAGE.D.DE.DESCRIPTION.TITLE}</td>
				</tr>
				{*else}{ *VARIANTE* }
					<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
						<td style="font-size:20px;text-align:right;">{$D.SET.QUANTITY}</td>
						<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_50x50.jpg"></td>
						<td>{$PLA.ARTICLE.D[$kART].VARIANTE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
						<td>{$PLA.ARTICLE.D[$kART].LANGUAGE.D.DE.DESCRIPTION.TITLE}</td>
					</tr>
				{/if*}
		{/foreach}
	{/case}
	{case 'get_invoice'}
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th style="text-align:right;">R.Nr.</th>
					<th>Name</th>
					<th style="text-align:right;">Tag</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$PLA.INVOICE.D key="kINV" item="INV" name="INV"}
				<tr style="cursor:pointer;" onclick="wp.ajax({
											'url'	:	'?D[PAGE]=platform.warehouse_list2&D[ACTION]=get_warenouse',
											'data'	:	{
															'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]' : '{$kINV}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['STATUS']}',
														},
											'div'	:	'fINV{$D.PLATFORM_ID}',ASYNC:1
											})">
					<td style="text-align:right;">{$smarty.foreach.INV.iteration}</td>
					<td style="text-align:right;">{$INV.NUMBER}</td>
					<td>{$INV.BILLING.FNAME} {$INV.BILLING.NAME}</td>
					<th style="text-align:right;">{floor(abs($smarty.now - strtotime($CWP->concat($INV.DATE,' 00:00:00')))/(24*60*60))}</th>
				</tr>
				{/foreach}
			</tbody>
		</table>
	{/case}
	{case 'get_warenouse'}
	<style>
	@keyframes blinker {  
		  50% { opacity: 0; }
		}
	</style>
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
		<input type="hidden" name="D['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
		<input type="hidden" name="D['PLATFORM_ID']" value="{$D.PLATFORM_ID}">
		{foreach from=$PLA.INVOICE.D key="kINV" item="INV"}
		
		<div class="panel panel-{if $INV.STATUS ==0}danger{elseif $INV.STATUS ==20}warning{elseif $INV.STATUS ==40}success{/if}">
			<div class="panel-heading">{if $INV.STATUS ==0}Offen{elseif $INV.STATUS ==20}Versandfreigabe{elseif $INV.STATUS ==40}Fertig{/if}
			| ID: {$kINV} | Nummer: {$INV.NUMBER} | Nickname: {$INV.CUSTOMER_NICKNAME} | Platform: {$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}</div>
			<div class="panel-body">
			
				<div class="alert alert-warning">{$INV.NUMBER} | 
				{*
					<span class="label label-danger"><input onchange="stock_transaktion(0)" type="radio" value='0' {if $INV.STATUS ==0}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> offen</span>
					<span class="label label-warning"><input onchange="stock_transaktion(0)" type="radio" value='20' {if $INV.STATUS ==20}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> Versandfreigabe</span>
					<span class="label label-success"><input onchange="stock_transaktion(1)" type="radio" value='40' {if $INV.STATUS ==40}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> Fertig</span>
			*}
					<div class="btn-group">
					{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
						{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'order' && $SET.VARIANTE.D['ACTIVE'].VALUE}
						<button type="button" class="btn btn-default" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kINV}','_blank');" title="Export Inv:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
						{/if}
					{/foreach}
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6">

		<input type="hidden" value='{$INV.PLATFORM_ID}' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][PLATFORM_ID]">
				<table class='table'>
					<thead>
						<tr>
							<th>Rechnungsadresse</th>
							<th>Lieferadresse</th>
							<th style="width:150px;">Versand</th>
						</tr>
					</thead>
					<tbody>
						
							
							<tr >
								<td style="background:#fff;font-size:13px;">
									{$INV.BILLING.COMPANY}<br>
									{$INV.BILLING.FNAME} {$INV.BILLING.NAME}<br>
									{$INV.BILLING.STREET} {$INV.BILLING.STREET_NO}<br>
									{if $INV.BILLING.ADDITION}{$INV.BILLING.ADDITION}<br>{/if}
									{$INV.BILLING.ZIP} {$INV.BILLING.CITY}<br>
									{$D.COUNTRY.D[$INV.BILLING.COUNTRY_ID].TITLE}
								</td>
								<td style="background:#fff;font-size:13px;">
								{if strpos(strtolower($INV.DELIVERY.STREET), "packstation") !== false || strpos(strtolower($INV.DELIVERY.ADDITION), "packstation") !== false}
								
								<div style="animation: blinker 1s linear infinite;color:Red;"><b>PACKSTATION</b></div>{/if}
									{$INV.DELIVERY.COMPANY}<br>
									{$INV.DELIVERY.FNAME} {$INV.DELIVERY.NAME}<br>
									{$INV.DELIVERY.STREET} {$INV.DELIVERY.STREET_NO}<br>
									{if $INV.DELIVERY.PHONE}Tel:{$INV.DELIVERY.PHONE}<br>{/if}
									{if $INV.DELIVERY.ADDITION}{$INV.DELIVERY.ADDITION}<br>{/if}
									{$INV.DELIVERY.ZIP} {$INV.DELIVERY.CITY}<br>
									{$D.COUNTRY.D[$INV.DELIVERY.COUNTRY_ID].TITLE}
									</td>
								<td style="text-align:left;">
									
									Traking:<input class="form-control" type='text' style="{if $INV.ARTICLE.PRICE > 39.99 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}background:yellow;{/if}" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRACKING_NO]" value='{$INV.TRACKING_NO}'">
									Datum:<input class="form-control" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPED_DATE]" value='{$smarty.now|date_format:"%Y-%m-%d"}'>
									Versandfirma:<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPING_ID]">
										<option value=""></option>
										{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
											<option value='{$kSHI}' {if $INV.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
										{/foreach}
									</select>
								</td>
							</tr>
						
							
					</tbody>
				</table>
				
					</div>
					<div class="col-xs-3">
						<textarea class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]' style='height:200px;'>{$INV.COMMENT}</textarea>
					</div>
					<div class="col-xs-3">
						{*MESSAGE START ================*}
							{if $INV.CUSTOMER_ID}
							<div style="height:180px;"  id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}"></div>
								<script>
									wp.ajax({
														'url'	:	'?D[PAGE]=message',
														'data'	:	{
																		'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		'D[MESSAGE][W][GROUP_ID]' : '{$INV.CUSTOMER_ID}', //group_id,
																		'D[MESSAGE][O][DATETIME]' : 'DESC',
																		'D[MESSAGE][W][INTERNALLY]' : '1'
																	},
														'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}'
														});
								</script>
							{/if}
						{*MESSAGE END ================*}
					</div>
				</div>
				
					<table class="table">
						<thead>
							<tr>
								<th style="width:50px;">ANZ</th>
								<th style="width:50px;">Bild</th>
								<th>Title</th>
								<th>Lagerort</th>
							</tr>
						</thead>
						{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
							{fcn_art $ART}
						{/foreach}
					</table>
					
				
			</div>
			<div class="panel-footer">
			
				<span class="label label-danger"><input onchange="stock_transaktion(0)" type="radio" value='0' {if $INV.STATUS ==0}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> offen</span>
					<span class="label label-warning"><input onchange="stock_transaktion(0)" type="radio" value='20' {if $INV.STATUS ==20}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> Versandfreigabe</span>
					<span class="label label-success"><input onchange="stock_transaktion(1)" type="radio" value='40' {if $INV.STATUS ==40}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]"> Fertig</span>
			
				<button class="btn btn-default" type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>
			</div>
		</div>
		{/foreach}
	</form>
	{/case}
	{default}
	<button type="button" title="aktuallisieren" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.warehouse_list2',
												'data'	:	{
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['STATUS']}',
															},
												'div'	:	'fPLA{$D.PLATFORM_ID}',ASYNC:1
												})">@</button>
	<script>
	SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID} = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list2&D[ACTION]=set_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize()
				});
			
			}
	</script>
	{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
			{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
			<button type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]=20&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][O][NUMBER]=ASC&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]={$D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.W.WAREHOUSE_ID}','_blank');" title="Export Inv:{$INV.NUMBER}">
			{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
			</button>
			{/if}
		{/foreach}
	<div class="row">
		<div class="col-xs-2">
			<div class="panel panel-warning">
				<div class="panel-heading">Versandfreigabe</div>
				<div id="invoice_20"></div>
			</div>
			
			<div class="panel panel-success">
				<div class="panel-heading">Fertig</div>
				<div id="invoice_40"></div>
			</div>
			<script>
				stock_transaktion = function(set)
				{
					if(set == 1)
					{
						$('.SOLL_ACTIVE').val(1);
					}
					else
					{
						$('.SOLL_ACTIVE').val(-2);
					}
				}
				
				wp.ajax({
						'url'	:	'?D[PAGE]=platform.warehouse_list2&D[ACTION]=get_invoice',
						'data'	:	{
										'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][O][NUMBER]' : 'ASC',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : '20',
									},
						'div'	:	'invoice_20',ASYNC:1
						});
				wp.ajax({
						'url'	:	'?D[PAGE]=platform.warehouse_list2&D[ACTION]=get_invoice',
						'data'	:	{
										'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][O][NUMBER]' : 'DESC',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : '40',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][L][START]' : '10',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][L][STEP]' : '0'
									},
						'div'	:	'invoice_40',ASYNC:1
						});
			</script>
		</div>
		<div class="col-xs-10">
			<div id="fINV{$D.PLATFORM_ID}"></div>
		</div>
	</div>

{/switch}
