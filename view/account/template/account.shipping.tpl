<form id="formr{$D.SESSION.ACCOUNT_ID}" method="post">
	<div class="alert alert-warning">
		<ul>
			<li>1.	Es könne pro Versandart beliebig viele Versandklassen erstellt werden.</li>
			<li>2.	Jede Versandklasse kann beliebig viele Regeln und Preis-Staffelung je Land enthalten. Ein Land kann nur einmal pro Versandklasse an irgendeine Preis-Staffel vergeben werden.</li>
			<li>3.	Alle Regeln in einer Box sind mit und Verknüpft und alle Regel-Boxen mit einander mit oder.
			<li>4.	Versandklassen können innerhalb Versandart sortiert werden. Die untere überschreibt den Preis aus der oberen insofern in beiden gleiches Land mit Preis definiert wurde. Es kann somit eine Globale Versandklasse die für alle Artikel für die keine individuelle Versandklasse über die Regel definiert worden ist.</li>
		</ul>
	</div>
	{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
	<div class="panel panel-default" id="dv{$kSHI}">
		<div class="panel-heading">
			{*
			<input type="hidden" id="D[SHIPPING][D][{$kSHI}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][ACTIVE]" value="{$SHI.ACTIVE}">
			<button onclick="if(confirm('Wirklich löschen?')){ document.getElementById('D[SHIPPING][D][{$kSHI}][ACTIVE]').value = '-2';document.getElementById('dv{$kSHI}').style.display ='none';}" type="button"><i class="fa fa-minus-circle"></i></button>
			*}
			{$SHI.TITLE}
		</div>
		<div class="panel-body">
		<ul id="sc_{$D.SESSION.ACCOUNT_ID}_{$kSHI}" class="list-group">
			
			
				{foreach from=$SHI.CLASS.D key="kCLA" item="CLA"}
				<li id="tr{$kCLA}" class="list-group-item">
							<div style="float:left;">
								
								<input group="SHIPPING_CLASS_SORT{$kSHI}" type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][SORT]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][SORT]" value="{$CLA.SORT}">
								<input type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][ACTIVE]" value="{$CLA.ACTIVE}">
								<button class="btn btn-danger btn-xs" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][ACTIVE]').value = '-2';document.getElementById('tr{$kCLA}').style.display ='none';}" type="button"><i class="fa fa-minus-circle"></i></button>
								<input type="checkbox" {if $CLA.ACTIVE}checked{/if} onclick="document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][ACTIVE]').value = (this.checked)?1:0">
								<input id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][TITLE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][TITLE]" value="{$CLA.TITLE}">
							</div>
							<div style="float:right;"><i id="sort_{$D.SESSION.ACCOUNT_ID}_{$kSHI}" class="fa fa-bars" title="sortieren"></i></div>
							<div style="clear:both;" class="row">
								<div class="col-md-6">
									
									<ul class="list-group">
										{foreach from=$CLA.GROUP.D key="kGRO" item="GRO"}
											<li class="list-group-item">
												<table class="table">
													<thead>
														<tr>
															<th></th>
															<th>A</th>
															<th>Feld</th>
															<th>Operator</th>
															<th>Wert</th>
														</tr>
													</thead>
													{foreach from=$GRO.CONDITIONS.D key="kCON" item="CON"}
													<tr id="tr{$kSHI}{$kCLA}{$kCON}">
														<td>
															<input type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][ACTIVE]" value="{$CON.ACTIVE}">
															<button  class="btn btn-danger btn-xs" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][ACTIVE]').value = '-2';document.getElementById('tr{$kSHI}{$kCLA}{$kCON}').style.display ='none';}" type="button"><i class="fa fa-minus-circle"></i></button>
														</td>
														<td><input type="checkbox" {if $CON.ACTIVE}checked{/if} onclick="document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][ACTIVE]').value = (this.checked)?1:0"></td>
														<td>
															<select class="form-control" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][COLUMN]">
																<option {if $CON.COLUMN == 'a.id'}selected{/if} value="a.id">Artikel ID</option>
																<option {if $CON.COLUMN == 'categorie_id'}selected{/if} value="categorie_id">Kategorie ID</option>
																<option {if $CON.COLUMN == 'a.price'}selected{/if} value="a.price">Preis</option>
															</select>
														</td>
														<td>
															<select class="form-control" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][OPERATOR]">
																<option {if $CON.OPERATOR == '='}selected{/if} value='='>=</option>
																<option {if $CON.OPERATOR == '<>'}selected{/if} value='<>'>!=</option>
																<option {if $CON.OPERATOR == '>'}selected{/if} value='>'>></option>
																<option {if $CON.OPERATOR == '<'}selected{/if} value='<'><</option>
																<option {if $CON.OPERATOR == 'LIKE'}selected{/if} value='LIKE'>LIKE</option>
																<option {if $CON.OPERATOR == 'IN'}selected{/if} value='IN'>IN</option>
																<option {if $CON.OPERATOR == 'NOT IN'}selected{/if} value='NOT IN'>NOT IN</option>
															</select>
														</td>
														<td><input class="form-control" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][GROUP][D][{$kGRO}][CONDITIONS][D][{$kCON}][VALUE]" value="{$CON.VALUE}"></td>
													</tr>
													{/foreach}
												</table>
											</li>
										{/foreach}
									</ul>
									
								</div>
							
								<div class="col-md-6">
									<ul class="list-group">
										<li class="list-group-item">
										<table class="table">
											<thead>
												<tr>
													<th>Preis</th>
													<th>Land</th>
												</tr>
											</thead>
											
											{foreach from=$CLA.COST.D key="kCOST" name="vCOST" item="COST"}
											<tr>
												<td>
													<input type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][ACTIVE]" value="{if $CLA.COST.D[$kCOST].PRICE > -1}1{else}-2{/if}">
													<input class="form-control" onkeyup="document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][ACTIVE]').value = ((this.value.length > 0)?1:-2);" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][PRICE]" style="text-align:right;" value="{$COST.PRICE}">
												</td>
												<td>
													<div class="dropdown">
														<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Länder<span class="caret"></span></button>
														<ul id="cou_{$kCOST}" class="dropdown-menu" aria-labelledby="dropdownMenu1" style="max-height:200px;overflow-y:auto;">
														{foreach from=$D.CONTINENT.D key="kCON" item="CON"}
															<li style="background:#ff9;"><input type="checkbox" onclick="var status = this.checked; $('input[group=\'concheck_{$kCOST}{$kCON}\']').each(function(){ this.checked = (status?1:0)}); $('input[group=\'conactive_{$kCOST}{$kCON}\']').each(function(){ this.value = ((status?1:-2));});"> {$kCON}</li>
															{foreach from=$CON.COUNTRY.D key="kCOU" item="COU"}
																<li><input group="conactive_{$kCOST}{$COU.CONTINENT_ID}" type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]" value="{if $COST.COUNTRY.D[ $kCOU ].ACTIVE}1{else}-2{/if}">
																	<input group="concheck_{$kCOST}{$COU.CONTINENT_ID}" id="{$kCOU}" type="checkbox" onclick="document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]').value = ((this.checked)?1:-2);" {if $COST.COUNTRY.D[ $kCOU ].ACTIVE}checked{/if}> {$kCOU} | {$COU.TITLE}</li>
															{/foreach}
															<li role="separator" class="divider"></li>
														{/foreach}
														
													
														</ul>
													</div>
												{*
													<div style="height:150px;overflow-y:scroll;">
													{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
														<div style="width:50px;height:30px;border:solid #ddd 1px;float:left;overflow:hidden;padding:1px;margin:1px;" title="{$COU.TITLE}">
															<input type="hidden" id="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]" name="D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]" value="{if $COST.COUNTRY.D[ $kCOU ].ACTIVE}1{else}-2{/if}">
															<input type="checkbox" onclick="document.getElementById('D[SHIPPING][D][{$kSHI}][CLASS][D][{$kCLA}][COST][D][{$kCOST}][COUNTRY][D][{$kCOU}][ACTIVE]').value = ((this.checked)?1:-2);" {if $COST.COUNTRY.D[ $kCOU ].ACTIVE}checked{/if}>{$kCOU}
														</div>
													{/foreach}
													</div>*}
												</td>
											</tr>
											{/foreach}
										</table>
										</li>
									</ul>
								</div>
							</div>
						
				
				{/foreach}
			
			</li>
			</ul>
		</div>
	</div>
	<script>
	$(function() {
		$( "#sc_{$D.SESSION.ACCOUNT_ID}_{$kSHI}" ).sortable({
			connectWith: "#sc_{$D.SESSION.ACCOUNT_ID}_{$kSHI}",
			handle: "#sort_{$D.SESSION.ACCOUNT_ID}_{$kSHI}",
			axis : "y",
			stop: function(event, ui) {
				$('#sc_{$D.SESSION.ACCOUNT_ID}_{$kSHI} > li').each(function(index, element){
				$(this).find("input[group='SHIPPING_CLASS_SORT{$kSHI}']").val(index);
				});
			}
		});
		$( "#sc_{$D.PLATFORM_ID}_{$kSHI}" ).disableSelection();
	  });
	</script>
	{/foreach}

</form>

<script>
SAVEPLATFORM{$D.SESSION.ACCOUNT_ID}SHIPPING = function()
{
	wp.ajax({
	'url' : '?D[PAGE]=account.shipping&D[ACTION]=set_shipping',
	'div' : 'ajax',
	'data': $('#formr{$D.SESSION.ACCOUNT_ID}').serialize()
	});
}
</script>