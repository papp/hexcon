{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{function name=attribute_ddl ID=null AID=null ATTRIBUTE=null}
	<select class="form-control" onchange="_set_attribute('{$ID}','{$AID}',this.value)">
		<option>---</option>
		{foreach $ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATT" item="ATT"}
			<optgroup label="{$ATT.LANGUAGE.D['DE'].TITLE}">
			{foreach $ATTRIBUTE.PARENT.D[$kATT].CHILD.D key="kATT2" item="ATT2"}
			<option value="{$kATT2}" {if $kATTV == $kATT2}selected{/if}>{$ATT2.LANGUAGE.D['DE'].TITLE}</option>
			{/foreach}
			</optgroup>
		{/foreach}
	</select>
{/function}

{function name=_set_attribute AID=null kVAR=null ATTRIBUTE=null kATT=null SELECTED=null}
{*ToDo: hotfix, besser lösen*}
{if $kVAR}{$AID=$kVAR}{/if}

	<input id="ACTIVE{$AID}{$kATT}" class="ATTRIBUTE_ACTIVE" type="hidden" value="1" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][DE][ACTIVE]">
	{*<input id="ACTIVE{$AID}{$kATT}" type="hidden" value="{$AID}" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PARENT_ID]">*}
	
	{if $ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].VALUE}
		<select id="ATTRIBUTE{$AID}{$kATT}" class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][DE][VALUE]">
			<option value="">-</option>
			{$FIND=0}
			{foreach from=explode("\n",$ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].VALUE) key="kATT" item="ATT"}
			<option value="{trim($ATT)}" {if trim($ATT) == trim($SELECTED)}{$FIND=1}selected{/if}>{trim($ATT)}</option>
			{/foreach}
			{if $FIND == 0}
				<option style="color:red;background:#fcc;" value="{trim($SELECTED)}" selected>{trim($SELECTED)}</option>
			{/if}
		</select>
	{else}
		<input id="ATTRIBUTE{$AID}{$kATT}" class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][DE][VALUE]" value="{trim($SELECTED)}">
	{/if}
{/function}

{switch $D.ACTION}
	{case 'attribute_ddl'}
		{attribute_ddl ID=$D.ID AID=$D.AID ATTRIBUTE=$PLA.ATTRIBUTE}
	{/case}
	{case '_set_attribute'}
		{_set_attribute AID=$D.AID kVAR=$D.kVAR ATTRIBUTE=$PLA.ATTRIBUTE kATT=$D.kATT}
	{/case}
	{case 'load_set_article'}
		{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}
			{$ART = $PLA.ARTICLE.D[$kART]}
			{if $kART == $PLA.ARTICLE.W.ID}{*PARENT*}
				<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
					<td style="text-align:center;">{$smarty.foreach.SET.iteration}</td>
					<td><button class="btn" type="button" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]').value = -2;}">-</button></td>
						<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]" value="{$D.SET.ACTIVE}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]').value = (this.checked)?1:0;" {if $D.SET.ACTIVE}checked{/if}>
						</td>
					<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$PLA.ARTICLE.W.ID}', 'TITLE' : '{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER} | {$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$PLA.ARTICLE.W.ID}'});">{$PLA.ARTICLE.W.ID}</button></td>
					<td><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_50x50.jpg"></td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}</td>
					<td style="text-align:right;">{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].PRICE|number_format:2:".":""}</td>
					<td style="text-align:right;">{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].STOCK}</td>
					<td><input name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][QUANTITY]" style="text-align:right;width:50px;" value="{$D.SET.QUANTITY}"></td>
				</tr>
				{else}{*VARIANTE*}
					<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
						<td style="text-align:center;">{$smarty.foreach.SET.iteration}</td>
						<td><button class="btn" type="button" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]').value = -2;}">-</button></td>
							<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]" value="{$D.SET.ACTIVE}">
								<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][ACTIVE]').value = (this.checked)?1:0;" {if $D.SET.ACTIVE}checked{/if}>
							</td>
						<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}gk37ezz6m', 'TITLE' : '{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER} | ' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$PLA.ARTICLE.W.ID}'});">{$PLA.ARTICLE.W.ID}</button></td>
						<td><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_50x50.jpg"></td>
						<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
						<td>
						{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[ $PLA.ARTICLE.W.ID ].PARENT_ID ].VARIANTE_GROUP_ID}
							<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[ $PLA.ARTICLE.W.ID ].PARENT_ID ].VARIANTE_GROUP_ID)}
							{for $i=0 to count((array)$aVAR)-1}
								{$PLA.ARTICLE.D[ $PLA.ARTICLE.W.ID ].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
							{/for}</b>
						{/if}
						</td>
						<td style="text-align:right;">{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].PRICE|number_format:2:".":""}</td>
						<td style="text-align:right;">{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].STOCK}</td>
						<td><input name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$D.ARTICLE_ID}][SET][ARTICLE][D][{$PLA.ARTICLE.W.ID}][QUANTITY]" style="text-align:right;width:50px;" value="{$D.SET.QUANTITY}"></td>
					</tr>
				{/if}
		{/foreach}
	{/case}
	{case 'load_article_var'}
			{assign var="kART" value=$D.ARTICLE_ID}
			{assign var="kVAR" value=$D.VARIANTE_ID}
			{assign var="ART" value=$PLA.ARTICLE.D[$kART]}
			{assign var="VAR" value=$ART.VARIANTE.D[$kVAR]}
			
			<tr class="variante" style="background:#fffece" id="list{$D.PLATFORM_ID}{$kART}{$kVAR}">
				<td><i class="fa fa-sort handle" style="cursor:pointer;"></i></td>
				<td><button class="btn btn-danger" type="button" onclick="if(confirm('wirklich löschen?') ){ document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]').value = '-2';$('#list{$D.PLATFORM_ID}{$kART}{$kVAR}').hide();}"><i class='fa fa-minus-circle'></i></button></td>
				<td>
					<input id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]" value="{$D.ACTIVE}">
					<input id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PARENT_ID]" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PARENT_ID]" value="{$kART}">
					<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]').value = (this.checked)?1:0;" {if $D.ACTIVE}checked{/if}>
					<input type="hidden" group='SORT' name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SORT]" value="{$D.SORT}"">
				</td>
				<td class="varID">{$kVAR}</td>
				<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_200x200.jpg'>"><img style="width:25px;" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg"></td>
				<td><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][NUMBER]" value=""></td>
				<td><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][EAN]" value=""></td>
				<td style="text-align:right;">
					{if $VAR.SET.ARTICLE.D}
						{$VAR.WEIGHT}g
					{else}
						<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][WEIGHT]" style="width:50px;text-align:right;" value="{$D.WEIGHT}">
					{/if}
				</td>
			
				{*NEUE VERSION*}
				{if $ART.VARIANTE_GROUP_ID}
				{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
				{for $i=0 to count((array)$aVAR)-1}
					<td id="id{$kART}{$i}">
						{_set_attribute AID=$kART kVAR=$kVAR ATTRIBUTE=$PLA.ATTRIBUTE kATT=$aVAR[$i] SELECTED=$VAR.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].VALUE}
					</td>
				{/for}
				{/if}
				<td class="add_var_variante"></td>

				<td style="text-align:right;"><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][VAT]" style="width:30px;text-align:right;" value="{$D.VAT}"></td>
				<td style="text-align:right;"><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="width:50px;text-align:right;" value="{$D.PRICE}"></td>
				<td style="text-align:right;"><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="width:50px;text-align:right;" value="{$D.RPRICE}"></td>
				<td style="text-align:right;"></td>
			</tr>
			<script>
				sortVar{$D.PLATFORM_ID}{$kART}();
			</script>
	{/case}
	{default}
	{if !$PLA.ARTICLE.PARENT.D[NULL].CHILD.D}{*Hotfix zur Artikel können nicht neu angelegt werden*}
		{$PLA.ARTICLE.PARENT.D[NULL].CHILD.D[$PLA.ARTICLE.W.ID] = $PLA.ARTICLE.D[$PLA.ARTICLE.W.ID]}
	{/if}
		{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}
			{$ART = $PLA.ARTICLE.D[$kART]}
			{$kFileFirstID = current(array_keys((array)$ART.FILE.D))}
			
			{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
				{$ART['VARIANTE']['D'][$kVAR] = $PLA.ARTICLE.D[$kVAR]}
			{/foreach}
		<form id="form{$D.PLATFORM_ID}{$kART}" name="form{$D.PLATFORM_ID}{$kART}" method="post" style="height:100%;">
{*$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)*}
<table class="a{$D.PLATFORM_ID}{$kART}" style="width:100%;height:100%;">
	<tr style="position: sticky;top: 0;z-index: 10000;">
		<td valign=top style="max-width:100px;height:10px;background:#333;color:#fff;">&nbsp;</td>
		<td valign=top style="background:#333;height:10px;color:#fff;position:sticky;top:0;z-index:1;">
			<ul style="float:left;" class="art-nav-bar nav nav-pills">
				<li class="bar0 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',0);">&#xf02c; Artikel</li>
{*if !$ART.SUPPLIER.D && !$first_VAR.SUPPLIER.D && !$ART.WAREHOUSE.D && !$first_VAR.WAREHOUSE.D*}
				<li class="bar1 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',1);">&#xf12e; SET</li>
				{if $ART.INSET.ARTICLE.D || $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
				<li class="bar7 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',7);">&#xf12e; INSET</li>
				{/if}
{*/if*}
				<li class="bar2 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',2);">&#xf14c; Referenz</li>
{*if !$ART.SET && !$first_VAR.SET*}
				{$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)}
				{if !$first_VAR.SET}
				<li class="bar3 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',3);">&#xf015; Lager</li>
				{*if $D.SESSION.USER.ACCOUNT.D[$D.SESSION.ACCOUNT_ID].PLATFORM.D[$D['PLATFORM_ID']].RIGHT.D['WP00006'].ACTIVE*}
				<li class="bar4 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',4);">&#xf275; Lieferant</li>
				{/if}
{*/if*}				
				<li class="bar5 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',5);">&#xf0e8; Platform</li>
{*				
				<li class="bar6 nav-item nav-link" onclick="tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',6);">&#xf0d1; Shipping</li>
*}				
			</ul>
			<ul style="float:right;" class="art-nav-bar nav nav-pills">
				<li><select onchange="showLang(this.value,'{$D.PLATFORM_ID}', '{$kART}')">
					
					{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
						{if $LAN.ACTIVE}
							<option value="{$kLAN}">{i18n id="language_{$kLAN}"}</option>
						{/if}
					{/foreach}
				</select>
				<script>
					showLang = function(LG,kPLA, kART)
					{
						$('.a'+kPLA+kART+' .lang').hide();
						if(LG)//shop lg
						{
							$('.a'+kPLA+kART+' .lang-'+LG).show();
						}
					}
				</script>
				</li>
			</ul>
		</td>
	</tr>
	{*Seiten Navigation*}
	<tr><td valign=top style="width:120px;overflow:hidden;background:#333;color:#fff;position:sticky;left:0;z-index:10;">
		<div style="height:100%;overflow-y:auto;overflow-x:hidden;">
			{if $ART.FILE.D}<div style="height:40px;background:url('file/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$ART.FILE.D)}_50x50.jpg') center no-repeat;"></div>{/if}
			<ul class="art-nav-lefta{$D.PLATFORM_ID}{$kART}bar nav flex-column nav-pills">
				<li class="lefta{$D.PLATFORM_ID}{$kART}bar_{$kART} nav-link" onclick="select_article('a{$D.PLATFORM_ID}{$kART}','{$kART}');">Parent</li>
				{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="nVAR"}
					{$VAR = $PLA.ARTICLE.D[$kVAR]}
				<li class="lefta{$D.PLATFORM_ID}{$kART}bar_{$kVAR} nav-link" style="{if !$VAR.ACTIVE}color:yellow{/if}" onclick="select_article('a{$D.PLATFORM_ID}{$kART}','{$kVAR}');"><nobr>
				{if $ART.VARIANTE_GROUP_ID}
					{$smarty.foreach.nVAR.iteration}
					{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
					{for $i=0 to count((array)$aVAR)-1}
						{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
						{$_VARATT[$aVAR[$i]][$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE] = 1}
					{/for}
				{else}{$VAR.NUMBER}{/if}
				</nobr></li>
				{/foreach}
			</ul>
		</div>
	</td>
	<td valign=top>
			<div style="height:100%">
			  <div class="art-nav nav0" style="height:100%">
			  <!--CATEGORIE IDS-->
				

				<table style="width:100%;height:100%;resize" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top" style="width:100px">
							<div class="img_list" style="width:195px;height:100%;resize:both;">
								<div ondrop="$(this).css('background','#ddd')" ondragleave="$(this).css('background','#ddd')" ondragover="$(this).css('background','#999')" class="fileupload_{$D.PLATFORM_ID}_{$kART} list">
									
									<div style="overflow:hidden;">
										<div style="float:left;"><label>PARENT</label></div>
										<div style="float:right;">
										<div id="fileupload2_{$D.PLATFORM_ID}_{$kART}"></div>
										{*
										<span class="fileinput-button">
											<label id="progress_{$D.PLATFORM_ID}_{$kART}" style="display:none;">100%</label><button type="button" class="btn btn-success"><i class='fa fa-plus-circle'></i></button>
											<input id="fileupload_{$D.PLATFORM_ID}_{$kART}" style="width:100%" type="file" name="files[]" multiple>
										</span>*}
										{*<div id="files" class="files"></div>*}
										</div>
									</div>
									<div style="clear:both;">
										<ul id="file_{$D.PLATFORM_ID}_{$kART}">
											{foreach from=$ART.FILE.D key="kFIL" item="FIL" name="FIL"}
												<li id="img_li{$D.PLATFORM_ID}{$kART}{$kFIL}" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}_200x200.jpg{*$ART.UTIMESTAMP*}'>">
													<!--<img style="width:40px;" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}_50x50.jpg{$ART.UTIMESTAMP}">-->
													<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}_40x40.jpg{*$ART.UTIMESTAMP*}") center no-repeat;'></div>
													<div class="navi">
														<button class="btn btn-danger" type="button" onclick="if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}{$kART}{$kFIL}').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][FILE][D][{$kFIL}][ACTIVE]').value='-2';}"><i class='fa fa-minus-circle'></i></button>
													</div>
													<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][FILE][D][{$kFIL}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][FILE][D][{$kFIL}][ACTIVE]" value="{$FIL.ACTIVE}">
													<input group="FILE_SORT" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][FILE][D][{$kFIL}][SORT]" value="{$FIL.SORT}">
												</li>
											{/foreach}
										</ul>
									</div>

									<div style="clear:both;">
										{*<span class="fileinput-button">
											<button type="button" class="btn btn-success"><i class='fa fa-plus-circle'></i></button>
											<input id="fileupload" type="file" name="files[]" multiple>
										</span>
										<div id="progress"></div>
										<div id="files" class="files"></div>*}
									</div>
									<script>
										function get_pic_html(file_id, article_id)
										{
											if(article_id)
												return ""
														+"<li data-file='"+file_id+"' id='img_li{$D.PLATFORM_ID}"+article_id+file_id+"' title=\"<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/"+file_id+"_200x200.jpg'>\">"
														+"	<div style='width:40px;height:40px;background:url(\"file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/"+file_id+"_40x40.jpg\") center no-repeat;'></div>"
														+"	<div class='navi'>"
														+"		<button class='btn btn-danger' type='button' onclick=\"if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}"+article_id+""+file_id+"').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][ACTIVE]').value='-2';}\"><i class='fa fa-minus-circle'></i></button>"
														+"	</div>"
														+"	<input type='hidden' id='D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][ACTIVE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][ACTIVE]' value='1'>"
														+"	<input group='FILE_SORT' type='hidden' name='D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][SORT]' value='{$FIL.SORT}'>"
														+"</li>";

											return ""
													+"<li data-file='"+file_id+"' id='img_li{$D.PLATFORM_ID}"+article_id+file_id+"' title=\"<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/"+file_id+"_200x200.jpg'>\">"
													+"	<div style='width:40px;height:40px;background:url(\"file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/"+file_id+"_40x40.jpg\") center no-repeat;'></div>"
													+"	<div class='navi'>"
													+"		<button class='btn btn-danger' type='button' onclick=\"if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}"+article_id+""+file_id+"').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][ACTIVE]').value='-2';}\"><i class='fa fa-minus-circle'></i></button>"
													+"	</div>"
													+"	<input type='hidden' id='D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D]["+article_id+"][FILE][D]["+file_id+"][ACTIVE]'  value='1'>"
													+"	<input group='FILE_SORT' type='hidden' value='{$FIL.SORT}'>"
													+"</li>";
										}

										$(function() {
											$( "#file_{$D.PLATFORM_ID}_{$kART}" ).sortable({
												connectWith: "#file_{$D.PLATFORM_ID}_{$kART}",
												stop: function(event, ui) {
														$('ul#file_{$D.PLATFORM_ID}_{$kART} > li').each(function(index, element){
														$(this).find("input[group='FILE_SORT']").val(index);
														});
												}
											});
											$( "#file_{$D.PLATFORM_ID}_{$kART}" ).disableSelection();
										  });

										  /*
										$(function () {
											$('#fileupload_{$D.PLATFORM_ID}_{$kART}').fileupload({
												dropZone: $('.fileupload_{$D.PLATFORM_ID}_{$kART}'),
												maxFileSize: 5000000,
												url: 'file/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
												//url: '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=upload',
												dataType: 'json',
												done: function (e, data) {
													//console.log(data);
													html = get_pic_html(data.result.file_id,'{$kART}');
													$('#file_{$D.PLATFORM_ID}_{$kART}').append(html);

													$('#progress_{$D.PLATFORM_ID}_{$kART}').hide();
												},
												progressall: function (e, data) {
													var progress = parseInt(data.loaded / data.total * 100, 10);
													$('#progress_{$D.PLATFORM_ID}_{$kART}').show();
													$('#progress_{$D.PLATFORM_ID}_{$kART}').text(progress+'%');
												}
											}).prop('disabled', !$.support.fileInput)
												.parent().addClass($.support.fileInput ? undefined : 'disabled');
										});*/
									</script>
									{*Uploader V2*}
									<style>
									.ajax-file-upload-container,
									.ajax-file-upload-statusbar,
									.ajax-upload-dragdrop { display:none !important;}
									
									
									</style>
									<script>
									$(document).ready(function()
									{
										$("#fileupload2_{$D.PLATFORM_ID}_{$kART}").uploadFile({
										url:'setfile/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
										fileName:"files",
										multiple:true,
										dragDrop:false,
										dropZone: $('.fileupload_{$D.PLATFORM_ID}_{$kART}'),
										returnType:"json",
										onSuccess:function(files,data,xhr,pd)
										{
											html = get_pic_html(data.file_id,'{$kART}');
											$('#file_{$D.PLATFORM_ID}_{$kART}').append(html);
											
										}
										}); 
									});
									</script>
								</div>
							
								{*BILDER GRUPPIEREN START*}
								{if $ART.VARIANTE_GROUP_ID}
								<div class="list">
									
									<div ondrop="$(this).css('background','#ddd')" ondragleave="$(this).css('background','#ddd')" ondragover="$(this).css('background','#999')" class="fileupload_{$D.PLATFORM_ID}_group">
										<div style="overflow:hidden;position:relative;">
											<div style="float:left;width:150px">
											
												<div class="varfile-filter" style="overflow-x:auto;"><nobr>
													{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
													{for $i=0 to count((array)$aVAR)-1}
													<select id="{$aVAR[$i]}">
														<option value="">{$PLA.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].TITLE}</option>
														{foreach from=$_VARATT[$aVAR[$i]] key="kATTVAL" item="ATTVAL"}
														<option value="{$kATTVAL}">{$kATTVAL}</option>
														{/foreach}
													</select>
													{/for}
													</nobr>
												</div>

											</div>
											<div style="float:right;">
												<div id="fileupload2_{$D.PLATFORM_ID}_group"></div>
												{*
												<span class="fileinput-button">
													<label id="progress_{$D.PLATFORM_ID}_group" style="display:none;background:#fff;">100%</label><button type="button" class="btn btn-success"><i class='fa fa-plus-circle'></i></button>
													<input id="fileupload_{$D.PLATFORM_ID}_group" style="width:100%" type="file" name="files[]" multiple>
												</span>
												*}
											</div>
										</div>
										<div style="clear:both;">
											<ul id="file_{$D.PLATFORM_ID}_group"></ul>
										</div>
									
										<script>
											$(function() {
												$( "#file_{$D.PLATFORM_ID}_group" ).sortable({
													connectWith: "#file_{$D.PLATFORM_ID}_group",
													stop: function(event, ui) {
														$('ul#file_{$D.PLATFORM_ID}_group > li').each(function(index, element){
															$(this).find("input[group='FILE_SORT']").val(index);
														});
													}
												});
												$( "#file_{$D.PLATFORM_ID}_group" ).disableSelection();
											});
										{*
											$(function () {
												$('.fileupload_{$D.PLATFORM_ID}_group').fileupload({
													dropZone: $('.fileupload_{$D.PLATFORM_ID}_group'),
													maxFileSize: 5000000,
													url: 'file/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
													//url: '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=upload',
													dataType: 'json',
													done: function (e, data) {
														//console.log(data);
														
														//html = get_pic_html(data.result.file_id);
														//$('#file_{$D.PLATFORM_ID}_group').append(html);

														$(".data-file_var").each(function(i, e){
															id = $(this).data("file_var");
															comby = $(this).data("varcomby");
															//ToDo....
															bCheck = true;
															$(".varfile-filter").find("select").each(function(i2,e2){ //gehe alle DDL Filter durch
																
																if($(this).val() != '')
																{
																	if(comby.indexOf('|'+$(this).val()+'|') == -1) //prüfe ob der gewählte Filter übereinstimmt
																	{
																		bCheck = false;
																	}
																}
																	
															});
															if(bCheck)
															{
																html = get_pic_html(data.result.file_id,id);
																//$('#file_{$D.PLATFORM_ID}_'+id).append(html);
																$('#file_{$D.PLATFORM_ID}_'+id).prepend(html);

																//Sortierungsnummer neu setzen
																$('ul#file_{$D.PLATFORM_ID}_'+id+' > li').each(function(index, element){
																	$(this).find("input[group='FILE_SORT']").val(index);
																});
															}
															
														});
														

														$('#progress_{$D.PLATFORM_ID}_group').hide();
													},
													progressall: function (e, data) {
														var progress = parseInt(data.loaded / data.total * 100, 10);
														$('#progress_{$D.PLATFORM_ID}_group').show();
														$('#progress_{$D.PLATFORM_ID}_group').text(progress+'%');
													}
												}).prop('disabled', !$.support.fileInput)
													.parent().addClass($.support.fileInput ? undefined : 'disabled');
											});
										*}
										</script>
										
										<script>
										$(document).ready(function()
										{
											$("#fileupload2_{$D.PLATFORM_ID}_group").uploadFile({
											url:'file/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
											fileName:"files",
											multiple:true,
											dragDrop:false,
											dropZone: $('.fileupload_{$D.PLATFORM_ID}_group'),
											returnType:"json",
											onSuccess:function(files,data,xhr,pd)
											{
											
												$(".data-file_var").each(function(i, e){
													id = $(this).data("file_var");
													comby = $(this).data("varcomby");
													//ToDo....
													bCheck = true;
													$(".varfile-filter").find("select").each(function(i2,e2){ //gehe alle DDL Filter durch
														
														if($(this).val() != '')
														{
															if(comby.indexOf('|'+$(this).val()+'|') == -1) //prüfe ob der gewählte Filter übereinstimmt
															{
																bCheck = false;
															}
														}
															
													});
													if(bCheck)
													{
														html = get_pic_html(data.file_id,id);
														//$('#file_{$D.PLATFORM_ID}_'+id).append(html);
														$('#file_{$D.PLATFORM_ID}_'+id).prepend(html);

														//Sortierungsnummer neu setzen
														$('ul#file_{$D.PLATFORM_ID}_'+id+' > li').each(function(index, element){
															$(this).find("input[group='FILE_SORT']").val(index);
														});
													}
													
												});
														
												$('#progress_{$D.PLATFORM_ID}_group').hide();
											
											}
											}); 
										});
										</script>
									</div>
									<div style="clear:both;"></div>
								</div>
								
								{/if}
								{*BILDER GRUPPIEREN END*}

								{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="nVAR"}
									{$VAR = $PLA.ARTICLE.D[$kVAR]}
								<div ondrop="$(this).css('background','#ddd')" ondragleave="$(this).css('background','#ddd')" ondragover="$(this).css('background','#999')" class="fileupload_{$D.PLATFORM_ID}_{$kVAR} list">

									<div style="overflow:hidden;position:relative;">
										<div style="float:left;overflow:hidden;position:absolute;">
											<label style="float:left;" title="{$VAR.NUMBER}"><nobr>
											{$smarty.foreach.nVAR.iteration} 
											{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
											{for $i=0 to count((array)$aVAR)-1}
												{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
												{$_VAR_COMBY[$kVAR] = "|{$_VAR_COMBY[$kVAR]}{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}|"}
											{/for}
											</nobr></label>
										</div>
										<div style="float:right;">
										<div id="fileupload2_{$D.PLATFORM_ID}_{$kVAR}"></div>
										{*
										<span class="fileinput-button">
											<label id="progress_{$D.PLATFORM_ID}_{$kVAR}" style="display:none;background:#fff;">100%</label><button type="button" class="btn btn-success"><i class='fa fa-plus-circle'></i></button>
											<div id="fileupload2_{$D.PLATFORM_ID}_{$kVAR}"></div>
											<input id="fileupload_{$D.PLATFORM_ID}_{$kVAR}" style="width:100%" type="file" name="files[]" multiple>
										</span>
										*}
										{*<div id="files_{$D.PLATFORM_ID}_{$kVAR}" class="files"></div>*}
										</div>
									</div>
									<div style="clear:both;">
										<ul class="data-file_var" data-file_var="{$kVAR}" data-varcomby="{$_VAR_COMBY[$kVAR]}" id="file_{$D.PLATFORM_ID}_{$kVAR}">
											{foreach from=$VAR.FILE.D key="kFIL" item="FIL" name="FIL"}
												<li id="img_li{$D.PLATFORM_ID}{$kVAR}{$kFIL}" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}_200x200.jpg'>">
													<!--<img style="width:40px;" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_{$smarty.foreach.FIL.index}_50x50.jpg?{$ART.UTIMESTAMP}">-->
													<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}_40x40.jpg") center no-repeat;'></div>
													<div class="navi">
														<button class="btn btn-danger" type="button" onclick="if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}{$kVAR}{$kFIL}').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][FILE][D][{$kFIL}][ACTIVE]').value='-2';}"><i class='fa fa-minus-circle'></i></button>
													</div>
													<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][FILE][D][{$kFIL}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][FILE][D][{$kFIL}][ACTIVE]" value="{$FIL.ACTIVE}">
													<input group="FILE_SORT" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][FILE][D][{$kFIL}][SORT]" value="{$FIL.SORT}">
												</li>
											{/foreach}
										</ul>
									</div>
									<div style="clear:both;"></div>
									<script>
										$(function() {
											$( "#file_{$D.PLATFORM_ID}_{$kVAR}" ).sortable({
												connectWith: "#file_{$D.PLATFORM_ID}_{$kVAR}",
												stop: function(event, ui) {
														$('ul#file_{$D.PLATFORM_ID}_{$kVAR} > li').each(function(index, element){
														$(this).find("input[group='FILE_SORT']").val(index);
														});
												}
											});
											$( "#file_{$D.PLATFORM_ID}_{$kVAR}" ).disableSelection();
										  });

									{* 
										$(function () {
											$('.fileupload_{$D.PLATFORM_ID}_{$kVAR}').fileupload({
												dropZone: $('.fileupload_{$D.PLATFORM_ID}_{$kVAR}'),
												maxFileSize: 5000000,
												url: 'setfile/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
												//url: '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=upload',
												dataType: 'json',
												done: function (e, data) {
													//console.log(data);
													
													html = get_pic_html(data.result.file_id,'{$kVAR}');
													$('#file_{$D.PLATFORM_ID}_{$kVAR}').append(html);

													$('#progress_{$D.PLATFORM_ID}_{$kVAR}').hide();
												},
												progressall: function (e, data) {
													var progress = parseInt(data.loaded / data.total * 100, 10);
													$('#progress_{$D.PLATFORM_ID}_{$kVAR}').show();
													$('#progress_{$D.PLATFORM_ID}_{$kVAR}').text(progress+'%');
												}
											}).prop('disabled', !$.support.fileInput)
												.parent().addClass($.support.fileInput ? undefined : 'disabled');
										});
									*}
									</script>
									{*fileupload2*}
									<script>
										$(document).ready(function()
										{
											$("#fileupload2_{$D.PLATFORM_ID}_{$kVAR}").uploadFile({
											url:'setfile/{$D.ACCOUNT_ID}/{$D.PLATFORM_ID}',
											fileName:"files",
											multiple:true,
											dragDrop:false,
											dropZone: $('.fileupload_{$D.PLATFORM_ID}_{$kVAR}'),
											returnType:"json",
											onSuccess:function(files,data,xhr,pd)
											{
												html = get_pic_html(data.file_id,'{$kVAR}');
												$('#file_{$D.PLATFORM_ID}_{$kVAR}').append(html);
											}
											}); 
										});
									</script>
									
								</div>
								{/foreach}
							</div>
						</td>
						<td valign=top style="overflow:hidden;">



							
{*ATTRIBUTE START ======================================*}
	{function name="_html_attribute" ATT=null}
		{if $PLA.ATTRIBUTE.PARENT.D[$kATT].CHILD.D}
		<thead style="position:sticky;top:0;z-index:10;">
			<tr>
				<th style="position:sticky;left:0;z-index:10;">ID</th>
				<th style="position:sticky;left:0;z-index:10;">{$ATT.LANGUAGE.D['DE'].TITLE}</th>
				{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
					{if $LAN.ACTIVE}<th class="lang lang-{$kLAN}">{i18n id="language_$kLAN"}</th>{/if}
				{/foreach}
			</tr>
		</thead>
		{else}
			{*$ART.CATEGORIE.D*}
			{$_isAttInKategorie = array_key_exists($kATT,(array)$PLA.CATEGORIE.ATTRIBUTE.D)}
			{if $_isAttInKategorie || $ART.ATTRIBUTE.D[$kATT].VALUE || $ART.ATTRIBUTE.D[$kATT].LANGUAGE.D['DE'].VALUE}
			<tr>
				<td class='varID' style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
				<td style="{if !$_isAttInKategorie}text-decoration:line-through;{/if}{if $ATT.REQUIRED}color:red;font-weight:bold;{/if}">{if $ATT.I18N}&#xf1ab; {/if}{if $ATT.LANGUAGE.D['DE'].TITLE}{$ATT.LANGUAGE.D['DE'].TITLE}{else}{$kATT}{/if}</td>
				{*if $ART.ATTRIBUTE.D[$kATT].LANGUAGE.D*}
				{if $ATT.I18N}
					{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
						{if $LAN.ACTIVE}
							<td class="lang lang-{$kLAN}">{input_att p=['type' => $ATT.TYPE, 'help' => $ATT.LANGUAGE.D['DE'].HELP, 'value' => $ART.ATTRIBUTE.D[$kATT].LANGUAGE.D[$kLAN].VALUE, 'option' => explode("\n",$PLA.ATTRIBUTE.D[$kATT].LANGUAGE.D[$kLAN].VALUE), 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][{$kLAN}]"]}</td>
						{/if}
					{/foreach}
				{else}
					<td colspan="{count($PLA.LANGUAGE.D)}">{input_att p=['type' => $ATT.TYPE, 'help' => $ATT.LANGUAGE.D['DE'].HELP, 'value' => $ART.ATTRIBUTE.D[$kATT].VALUE, 'option' => explode("\n",$PLA.ATTRIBUTE.D[$kATT].VALUE), 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ATTRIBUTE][D][{$kATT}]"]}</td>
				{/if}
			</tr>
			{/if}
		{/if}
	{/function}

								<div style="min-height:350px;height:100%;overflow-y:scroll;overflow-x:hidden;">

									<table class="table a{$D.PLATFORM_ID}{$kART} art_all art_{$kART}">
										{foreach from=$PLA.ATTRIBUTE.PARENT.D[null].CHILD.D key="kATT" item="ATT"}
											<thead style="position:sticky;top:0;z-index:10;">
												{_html_attribute D=$D kART=$kART ART=$ART kATT=$kATT ATT=$ATT}
											</thead>
											<tbody id="tATT{$D.PLATFORM_ID}{$kART}{$kATT}">
												{foreach from=$PLA.ATTRIBUTE.PARENT.D[$kATT].CHILD.D key="kATT2" item="ATT2"}
													{if $kATT2 != $ART.VARIANTE_GROUP_ID && !strpos($kATT2, "{$ART.VARIANTE_GROUP_ID}|") && !strpos($kATT2, "|{$ART.VARIANTE_GROUP_ID}") && !strpos($kATT2, "|{$ART.VARIANTE_GROUP_ID}|")}
													{_html_attribute D=$D kART=$kART ART=$ART kATT=$kATT2 ATT=$ATT2}
													{/if}
												{/foreach}
											</tbody>
										{/foreach}
										{*Verwaiste Attribute*}
										{if array_diff_key((array)$ART.ATTRIBUTE.D, (array)$PLA.ATTRIBUTE.D)}
											<thead style="position:sticky;top:0;z-index:10;">
												<tr>
													<th>ID</th>
													<th style="position:sticky;left:0;z-index:10;">Verwaist &#xf5b4;</th>
													{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
														{if $LAN.ACTIVE}<th class="lang lang-{$kLAN}">{i18n id="language_$kLAN"}</th>{/if}
													{/foreach}
												</tr>
											</thead>
											<tbody style="background:#ffffd0;" id="tATT{$D.PLATFORM_ID}{$kART}{$kATT}x">
												{foreach from=$ART.ATTRIBUTE.D key="kATT" item="ATT"}
													{if !array_key_exists($kATT,(array)$PLA.ATTRIBUTE.D)}
														{_html_attribute D=$D kART=$kART ART=$ART kATT=$kATT ATT=$ATT}
													{/if}
												{/foreach}
											</tbody>
										{/if}
									</table>

{*Variante*}
									{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
										{*$VAR = $PLA.ARTICLE.D[$kVAR]*}
									<table class="table a{$D.PLATFORM_ID}{$kART} art_all art_{$kVAR}">
										{foreach from=$PLA.ATTRIBUTE.PARENT.D[null].CHILD.D key="kATT" item="ATT"}
											<thead style="position:sticky;top:0;z-index:10;">
												{_html_attribute D=$D kART=$kVAR ART=$VAR kATT=$kATT ATT=$ATT}
											</thead>
											<tbody id="tATT{$D.PLATFORM_ID}{$kVAR}{$kATT}">
												{foreach from=$PLA.ATTRIBUTE.PARENT.D[$kATT].CHILD.D key="kATT2" item="ATT2"}
													{_html_attribute D=$D kART=$kVAR ART=$VAR kATT=$kATT2 ATT=$ATT2}
												{/foreach}
											</tbody>
										{/foreach}
										{*Verwaiste Attribute*}
										{if array_diff_key((array)$VAR.ATTRIBUTE.D, (array)$PLA.ATTRIBUTE.D)}
											<thead style="position:sticky;top:0;z-index:10;">
												<tr>
													<th>ID</th>
													<th style="position:sticky;left:0;z-index:10;">Verwaist &#xf5b4;</th>
													{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
														{if $LAN.ACTIVE}<th class="lang lang-{$kLAN}">{i18n id="language_$kLAN"}</th>{/if}
													{/foreach}
												</tr>
											</thead>
											<tbody style="background:#ffffd0;" id="tATT{$D.PLATFORM_ID}{$kVAR}{$kATT}x">
												{foreach from=$VAR.ATTRIBUTE.D key="kATT" item="ATT"}
													{if !array_key_exists($kATT,(array)$PLA.ATTRIBUTE.D)}
														{_html_attribute D=$D kART=$kVAR ART=$VAR kATT=$kATT ATT=$ATT}
													{/if}
												{/foreach}
											</tbody>
										{/if}
									</table>
									{/foreach}

{*KATEGORIE START*}
{*
<table style="display:none;" class="table">
	<tr>
		<td>Kategorien</td>
		<td>
		{foreach from=$PLA.CATEGORIE.D key="kC" item="C"}
			{if !$C.COUNT}
			<li class="dropdown-item" href="#">
				<input type="hidden" value="{$ART.CATEGORIE.D[$kC].ACTIVE}" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][CATEGORIE][D][{$kC}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][CATEGORIE][D][{$kC}][ACTIVE]">
				<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][CATEGORIE][D][{$kC}][ACTIVE]').value = (this.checked)?1:-2" {if $ART.CATEGORIE.D[$kC].ACTIVE}checked{/if}> {str_replace('|',' > ',$C.LANGUAGE.D['DE'].BREADCRUMB_TITLE)} > {$C.LANGUAGE.D['DE'].TITLE}
			</li>
			{/if}
		{/foreach}
		</td>
	</tr>
</table>
*}
{*Kategorie Ende*}


								</div>
{*ATTRIBUTE END ========================================*}
							

							<script>
							//lade Attribut DDL Auswahl
							_add_var_attribute = function(aid)
							{
								var ID = wp.get_genID();
								$.ajax({
									type: 'POST',
									url: '?D[PAGE]=platform.article&D[ACTION]=attribute_ddl&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[AID]='+aid+'&D[ID]='+ID
								})
								.done(function (html) {
									$('#list_'+aid+' tr .add_var_title').before("<th id='id"+aid+ID+"' class='text-center'><button type='button' class='btn btn-danger' onclick=\"_del_var_attribute($('#list_{$kART}'), $(this).parent().index() );\"><i class='fa fa-minus-circle'></i></th>");
									$('#list_'+aid+' tr .add_var_parent').before("<td id='id"+aid+ID+"'>"+html+"</td>");
									$('#list_'+aid+' tr .add_var_variante').each(function()
										{
											$(this).before("<td id='id"+aid+ID+"'></td>");
										}
									);
								});
							}
							
							//Lade für gewählten Attribut je Variation DDL,TextBox,...
							_set_attribute = function(ID,aid,attid)
							{
								$.ajax({
										type: 'POST',
										url: '?D[PAGE]=platform.article&D[ACTION]=_set_attribute&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[kATT]='+attid+'&D[AID]='+aid
									})
									.done(function (html) {
										$('#list_'+aid+' .variante').each(
											function()
											{
												var varID = $(this).find('.varID').html();
												html2 = html.replace(/\[\]/g,'\['+varID+'\]');
												//$(this).append(html);
												$(this).find('#id'+aid+ID).append(html2);
											}
										);
										setVariation_group_id( $('#list_'+aid) );
									});
							}

							//kombiniert die Variation_group_id neu
							setVariation_group_id = function(table) 
							{
								$('#D{$D.PLATFORM_ID}{$kART}VARIANTE_GROUP_ID').val('');//lösche alte konstelation
								table.find('tbody tr').first().find('td').each(function() {
									if($(this).is(":visible"))
										if($(this).find('select').val())
											$('#D{$D.PLATFORM_ID}{$kART}VARIANTE_GROUP_ID').val( ( ($('#D{$D.PLATFORM_ID}{$kART}VARIANTE_GROUP_ID').val() != '')? $('#D{$D.PLATFORM_ID}{$kART}VARIANTE_GROUP_ID').val()+'|'+$(this).find('select').val() : $(this).find('select').val()  )  );
								});
							}

							_del_var_attribute = function(table, celNr)
							{
								if(confirm('Wirklich löschen?'))
								{
									table.find('tr').each(
										function()
										{
											$(this).children('th, td').eq(celNr).hide();
											$(this).children('th, td').eq(celNr).find('.ATTRIBUTE_ACTIVE').val(-2);
											
										}
									);
									
									setVariation_group_id(table); //group_id neu kombinieren
								}
							}
							//-----
							
							_select_attribute_group = function(pid, aid, selectedIndex)
							{
								anz = $("select[group='ATTG_VALUE"+pid+aid+"']").size();
								for(i=0; i < anz; i++)
								{
									$("select[group='ATTG_VALUE"+pid+aid+"']")[i].selectedIndex = selectedIndex;
								}
								att_group = $("select[group='ATTG_VALUE"+pid+aid+"'] option:selected").data('att');
								
								$('#tATT'+pid+aid+' tr').hide();
								if(att_group != '')
									$(att_group).show();
								else
									$('#tATT'+pid+aid+' tr').show();
							}

							//ToDo: Veraltet
							_select_attribute_var_group = function(pid, aid, gid, selectedIndex, type)
							{
								//Setzt alle ddl auf den gleichen stand
								$("select[group_type='"+type+pid+aid+"']").selectedIndex = selectedIndex;
								
								attid = $("select[group='ATTVG_VALUE"+pid+aid+"'] option:selected").data('att').split('|');

								//Setzt alle ATT auf display=none
								$("[group_type='"+type+pid+aid+"']").hide();

								for(i=0;i < attid.length; i++)
									if(attid[i] != '')
										$("[group='ATTV"+pid+aid+attid[i]+"'").show();//[group='ATTV{$D.PLATFORM_ID}{$kART}{$kATT}'],

								//Setze Active nur bei Aktiven auf = 1 sonnst auf -2
								if(type == 'ATTRIBUTE_VARIANTE')
								{ 
									$("[group_type='ATT_GROUP_ACTIVE"+pid+aid+"']").val('-2');
									for(i=0;i < attid.length; i++)
										$("[group='ATT_GROUP_ACTIVE"+pid+aid+attid[i]+"']").val('1');
								}
								
							}

							_select_language = function(pid,aid,lid)
							{
								$("[group_type='LANGUAGE_"+pid+aid+"']").hide();
								$("[group='LANGUAGE_"+lid+pid+aid+"']").show();
							}

							_select_attribute = function(pid, aid, lid, att_id, selectedIndex)
							{
								anz = $("select[group='ATT_VALUE"+pid+aid+att_id+"']").size();
								for(i=0; i < anz; i++)
								{
									$("select[group='ATT_VALUE"+pid+aid+att_id+"']")[i].selectedIndex = selectedIndex;
									$("input[group='ATT_ACTIVE"+pid+aid+att_id+"']")[i].value = ( $("select[group='ATT_VALUE"+pid+aid+att_id+"']")[i].value == '' )? '-2' : '1';
								}
							}
							</script>

						</td>
					</tr>
				<!--</table>-->

				<!--Variante-->
					<tr>
						<td colspan=2 valign=top>
<script>
$.moveColumn = function (table, from, to) {
    var rows = $('tr', table);
    var cols;
    rows.each(function() {
        //cols = $(this).children('th, td');
		cols = $(this).children('td');
        cols.eq(from).detach().insertBefore(cols.eq(to));
    });

	setVariation_group_id(table);
}


</script>
					<div style="height:100%;min-height:200px;overflow-y:auto;">
						<table class="table" id="list_{$kART}">
							<thead style="position:sticky;top:0;z-index:10;">
								<tr>
									<th></th>
									<th><button type="button" class="btn btn-success" onclick="load_article_var();"><i class='fa fa-plus-circle'></i></button></th>
									<th><input type="checkbox" onclick="active_all(this.checked)" {if $ART.ACTIVE}checked{/if}></th>
									<th>ID</th>
									<th></th>
									<th>Nummer</th>
									<th>EAN</th>
									<th>Gewicht</th>
									
									
									{*NEUE VERSION*}
									{if $ART.VARIANTE_GROUP_ID}
									{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
									{for $i=0 to count((array)$aVAR)-1}
										<th class="text-center" id="id{$kART}{$i}">
											<button type="button" class="btn btn-danger" onclick="_del_var_attribute($('#list_{$kART}'), $(this).parent().index() );"><i class='fa fa-minus-circle'></i></button>
											{*<button type="button" class="btn btn-default" onclick="jQuery.moveColumn($('#list_{$kART}'),8,9 );">&#xf053;</button>*}
											{if $i < count((array)$aVAR)-1}
											<button type="button" class="btn btn-default" style="float:right;margin-right:-15px;" onclick="jQuery.moveColumn($('#list_{$kART}'),$(this).parent().index()+1,$(this).parent().index() );">&#xf053;&#xf054;</button>
											{/if}
										</th>
									{/for}
									{/if}
									<th class="add_var_title">
										<input id="D{$D.PLATFORM_ID}{$kART}VARIANTE_GROUP_ID" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][VARIANTE_GROUP_ID]" value="{$ART.VARIANTE_GROUP_ID}">
										<button type="button" class="btn btn-success" onclick="_add_var_attribute('{$kART}');"><i class='fa fa-plus-circle'></i></button>
									</th>
									
									
									<th>MwSt</th>
									<th>Preis</th>
									<th>UVP</th>
									<th>Bestand</th>
								</tr>
							</thead>
							<script>
							load_article_var = function()
							{
								id = wp.get_genID();
								//$('#list_{$kART} > tbody:last').append("<tr id='list{$D.PLATFORM_ID}{$kART}"+id+"'></tr>");
								wp.ajax({
								'url' : '?D[PAGE]=platform.article&D[ACTION]=load_article_var&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}&D[VARIANTE_ID]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kART}',
								'div' : 'listVar{$D.PLATFORM_ID}{$kART}',//'list_{$kART}',
								'INSERT' : 'prepend',
								'data': {
									'D[ACTIVE]' : '1',
									'D[VAT]'	: '{$ART.VAT}',
									'D[PRICE]'	: '{$ART.PRICE}',
									'D[RPRICE]'	: '{$ART.RPRICE}',
									'D[WEIGHT]'	: '{$ART.WEIGHT}'
									}
								});
							}
							</script>
							<tbody>
							<tr>
								<td></td>
								<td></td>
								<td>
									<input id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ACTIVE]" group="ACTIVE" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ACTIVE]" value="{$ART.ACTIVE}">
									<input type="checkbox" group="ACTIVE" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ACTIVE]').value = (this.checked)?1:0;" {if $ART.ACTIVE}checked{/if}></td>
								<td style="cursor:pointer;" title="{$kART}" onclick="navigator.clipboard.writeText('{$kART}');">&#xf0c5;</td>
								<td {if $ART.FILE.D}title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$ART.FILE.D)}_200x200.jpg'>"{/if}>{if $ART.FILE.D}<img style="height:25px;" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$ART.FILE.D)}_50x50.jpg">{/if}</td>
								<td>
									{if $ART.NUMBER == ''}
										<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][NUMBER]" value="{$ART.NUMBER}">
									{else}
										{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][NUMBER]", 'value'=>$ART.NUMBER ]}
									{/if}
								</td>
								<td>{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][EAN]" value="{$ART.EAN}">*}
								{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][EAN]", 'value'=>$ART.EAN ]}
								</td>
								<td style="text-align:right;">
									{if $ART.SET.ARTICLE.D}
										{$ART.WEIGHT}g
									{else}
										{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][WEIGHT]" style="max-width:50px;text-align:right;" value="{$ART.WEIGHT}">*}
										{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][WEIGHT]", "type"=>"number", 'min'=>'0', 'step'=>'1', 'style'=>'max-width:50px;text-align:right;', 'value'=>$ART.WEIGHT ]}
									{/if}
								</td>
								
								{*NEUE VERSION*}
								{if $ART.VARIANTE_GROUP_ID}
								{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
								{for $i=0 to count((array)$aVAR)-1}
									<td id="id{$kART}{$i}">
									{attribute_ddl ID=$i AID=$kART kATTV=$aVAR[$i] ATTRIBUTE=$PLA.ATTRIBUTE}
									</td>
								{/for}
								{/if}
								<td class="add_var_parent" {*title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$ART.ATTRIBUTE.D['swatch']['VALUE']}_100x100.jpg?{$ART.UTIMESTAMP}'>" style="background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$ART.ATTRIBUTE.D['swatch']['VALUE']}_25x25.jpg?{$ART.UTIMESTAMP}) center center no-repeat;"*}></td>
								
								<td style="text-align:right;">{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][VAT]" style="max-width:30px;text-align:right;" value="{$ART.VAT}">*}
								{$_ustClass = ((isset($ART.ATTRIBUTE.D['TaxClass'].VALUE))?$ART.ATTRIBUTE.D['TaxClass'].VALUE:$PLA.SETTING.D['StandardTaxClass'].VALUE)}
								{$_ust = json_decode($PLA.SETTING.D['TaxClassToCountry'].VALUE,1)}
								{$_comCountry = $PLA.SETTING.D['CompanyAddressCountry'].VALUE}
								{$_ust[$_ustClass][$_comCountry]}%
								{*input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][VAT]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>'max-width:30px;text-align:right;', 'value'=>$ART.VAT ]*}
								</td>
								<td style="text-align:right;">{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="max-width:60px;text-align:right;" value="{$ART.PRICE}">*}
								

								{$_PR = 0}
								{foreach from=$ART.SET.ARTICLE.D key="kSET" item="SET"}
									{$_PR = $_PR + $D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].PRICE * (int)$SET.QUANTITY}
								{/foreach}
								{$_warn = (($_PR-$_PR*0.9) >= $ART.PRICE && $ART.VARIANTE_GROUP_ID == '') ?'border-color:red;':''}

								{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PRICE]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>"max-width:60px;text-align:right;{$_warn}", 'value'=>$ART.PRICE ]} {if $_warn}({$_PR}){/if}
								</td>
								<td style="text-align:right;">{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][RPRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="max-width:60px;text-align:right;" value="{$ART.RPRICE}">*}
								{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][RPRICE]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>'max-width:60px;text-align:right;', 'value'=>$ART.RPRICE ]}
								</td>
								<td style="text-align:right;">{if $ART.STOCK > 0}{$ART.STOCK}{else}0{/if}</td>
							</tr>
							
							</tbody>
							<tbody id="listVar{$D.PLATFORM_ID}{$kART}">
							{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR" name="nVAR"}
								{$VAR = $PLA.ARTICLE.D[$kVAR]}
								{*include file="platform.article.tpl" assign=$D*}
								<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PARENT_ID]" value="{$VAR.PARENT_ID}">
								<tr class="variante" style="background:#fffece;" id="list{$D.PLATFORM_ID}{$kART}{$kVAR}">
									<td><i class="fa fa-sort handle" style="cursor:pointer;"></i></td>
									<td>{$smarty.foreach.nVAR.iteration} <button type="button" class="btn btn-danger btn-xs" onclick="if(confirm('wirklich löschen?') ){ document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]').value = '-2';$('#list{$D.PLATFORM_ID}{$kART}{$kVAR}').hide();}"><i class='fa fa-minus-circle'></i></button></td>
									<td>
										<input id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]" group="ACTIVE" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]" value="{$VAR.ACTIVE}">
										<input type="checkbox" group="ACTIVE" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ACTIVE]').value = (this.checked)?1:0;" {if $VAR.ACTIVE}checked{/if}>
										<input type="hidden" group='SORT' name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SORT]" value="{$smarty.foreach.nVAR.iteration-1}">
									</td>
									<td class='varID' style="cursor:pointer;" title="{$kVAR}" onclick="navigator.clipboard.writeText('{$kVAR}');">&#xf0c5;</td>
									<td {if $VAR.FILE.D}title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$VAR.FILE.D)}_200x200.jpg'>"{/if}>{if $VAR.FILE.D}<img style="width:25px;" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$VAR.FILE.D)}_50x50.jpg">{/if}</td>
									<td>
										{if $VAR.NUMBER == ''}
											<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][NUMBER]" value="{$VAR.NUMBER}">
										{else}
											{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][NUMBER]", 'value'=>$VAR.NUMBER ]}
										{/if}
									</td>
									<td>{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][EAN]" value="{$VAR.EAN}">*}
									{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][EAN]", 'value'=>$VAR.EAN ]}
									</td>
									<td style="text-align:right;">
										{if $VAR.SET.ARTICLE.D}
											{$VAR.WEIGHT}g
										{else}
											{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][WEIGHT]" style="max-width:50px;text-align:right;" value="{$VAR.WEIGHT}">*}
											{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][WEIGHT]", "type"=>"number", 'min'=>'0', 'step'=>'1', 'style'=>'max-width:50px;text-align:right;', 'value'=>$VAR.WEIGHT ]}
										{/if}
									</td>
									
									{*NEUE VERSION*}
									{if $ART.VARIANTE_GROUP_ID}
									{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
									{for $i=0 to count((array)$aVAR)-1}
										<td id="id{$kART}{$i}">
											{_set_attribute AID=$kART kVAR=$kVAR ATTRIBUTE=$PLA.ATTRIBUTE kATT=$aVAR[$i] SELECTED=$VAR.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].VALUE}
										</td>
									{/for}
									{/if}
									<td class="add_var_variante" {*title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{if $VAR.ATTRIBUTE.D['swatch']['VALUE']}{$VAR.ATTRIBUTE.D['swatch']['VALUE']}{else}{$ART.ATTRIBUTE.D['swatch']['VALUE']}{/if}_100x100.jpg?{$ART.UTIMESTAMP}'>" style="background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{if $VAR.ATTRIBUTE.D['swatch']['VALUE']}{$VAR.ATTRIBUTE.D['swatch']['VALUE']}{else}{$ART.ATTRIBUTE.D['swatch']['VALUE']}{/if}_25x25.jpg?{$ART.UTIMESTAMP}) center center no-repeat;"*}></td>

									<td style="text-align:right;">
									{$_ustClass = ((isset($VAR.ATTRIBUTE.D['TaxClass'].VALUE))?$VAR.ATTRIBUTE.D['TaxClass'].VALUE:((isset($ART.ATTRIBUTE.D['TaxClass'].VALUE))?$ART.ATTRIBUTE.D['TaxClass'].VALUE:$PLA.SETTING.D['StandardTaxClass'].VALUE))}
									{$_ust[$_ustClass][$_comCountry]}%

									{*input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][VAT]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>'max-width:30px;text-align:right;', 'value'=>$VAR.VAT ]*}</td>
									<td style="text-align:right;">{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="max-width:60px;text-align:right;" value="{$VAR.PRICE}">*}
									
									{$_PR = 0}
									{foreach from=$VAR.SET.ARTICLE.D key="kSET" item="SET"}
										{$_PR = $_PR + $D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].PRICE * $SET.QUANTITY}
									{/foreach}
									{$_warn = (($_PR-$_PR*0.9) >= $VAR.PRICE)?'border-color:red;':''}
									
									{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PRICE]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>"max-width:60px;text-align:right;{$_warn}", 'value'=>$VAR.PRICE ]} {if $_warn}({$_PR}){/if}</td>
									<td style="text-align:right;">{*<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][RPRICE]" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" style="max-width:60px;text-align:right;" value="{$VAR.RPRICE}">*}
									{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][RPRICE]", "type"=>"number", 'min'=>'0', 'step'=>'0.01', 'style'=>'max-width:60px;text-align:right;', 'value'=>$VAR.RPRICE ]}</td>
									<td style="text-align:right;">{$VAR.STOCK}</td>
								</tr>

							{/foreach}
							
							</tbody>
						</table>
						<script>
							sortVar{$D.PLATFORM_ID}{$kART} = function()
							{
								$( "#listVar{$D.PLATFORM_ID}{$kART}" ).sortable({
									//connectWith: "#listVar{$D.PLATFORM_ID}{$kART}",
									handle : '.handle',
									axis : "y",
									stop: function(event, ui) {
										$('#listVar{$D.PLATFORM_ID}{$kART} > tr').each(function(index, element){
											$(this).find("input[group='SORT']").val(index);
										});
									}
								});
								$( "#listVar{$D.PLATFORM_ID}{$kART}" ).disableSelection();
							}
							sortVar{$D.PLATFORM_ID}{$kART}();

							active_all = function(bValue)
							{
								$('#list_{$kART}').each(function(index, element){
									
									$(this).find("input:checkbox[group='ACTIVE']").prop( "checked", bValue ); //ändert Checkbox
									$(this).find("input:hidden[group='ACTIVE']").val( (bValue)?1:0 ); // setzt Status in hidden Textbox
								
								});
							}
						</script>
					</div>
					</td>
					</tr>
					</table>
				</div>
				{*SET ARTICLE START =======*}
{*if !$ART.SUPPLIER.D && !$first_VAR.SUPPLIER.D && !$ART.WAREHOUSE.D && !$first_VAR.WAREHOUSE.D*}
				<div class="art-nav nav1" style="display:none;">
					<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kART}">
						<input name="D[AUTOSET_GENERATE]" type="checkbox" value="1"> Auto-Set generieren!
						<input name="D[AUTOSET_GENERATE_PIC]" type="checkbox" value="1"> Set mit Bilder
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th><button class="btn btn-default" type="button" onclick="add_article_set('{$kART}');">+</button></th>
									<th>Aktive</th>
									<th>ID</th>
									<th>Bild</th>
									<th>NUM</th>
									<th>Title</th>
									<th title="Einzelpreis">Preis</th>
									<th style="text-align:right;">Bestand</th>
									<th>Anzhl</th>
								</tr>
							</thead>
							{*<tbody id="SET{$D.PLATFORM_ID}{$kART}"></tbody>*}
							<tbody id="SET{$D.PLATFORM_ID}{$kART}">
								{foreach from=$ART.SET.ARTICLE.D key="kSET" item="SET"}
									
									<tr id="SET{$D.PLATFORM_ID}{$kART}{$kSET}">
										<td style="text-align:center;">{$smarty.foreach.SET.iteration}</td>
										<td><button class="btn" type="button" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('SET{$D.PLATFORM_ID}{$kART}{$kSET}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][SET][ARTICLE][D][{$kSET}][ACTIVE]').value = -2;}">-</button></td>
											<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][SET][ARTICLE][D][{$kSET}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][SET][ARTICLE][D][{$kSET}][ACTIVE]" value="{$SET.ACTIVE}">
												<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][SET][ARTICLE][D][{$kSET}][ACTIVE]').value = (this.checked)?1:0;" {if $SET.ACTIVE}checked{/if}>
											</td>
										<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$kSET}', 'TITLE' : '{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].NUMBER} | {$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kSET}'});">{$kSET}</button></td>
										<td><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].FILE.D)}_50x50.jpg"></td>
										<td>{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].NUMBER}</td>
										<td>{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}</td>
										<td style="text-align:right;">{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].PRICE|number_format:2:".":""}</td>
										<td style="text-align:right;">{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].STOCK}</td>
										<td><input name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][SET][ARTICLE][D][{$kSET}][QUANTITY]" style="text-align:right;width:50px;" value="{$SET.QUANTITY}"></td>
									</tr>

								{/foreach}
							</tbody>
						</table>
					{*
						<script>
								{foreach from=$ART.SET.ARTICLE.D key="kSET" item="SET" name="SET"}
									wp.ajax({
									'url' : '?D[PAGE]=platform.article&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
									'div' : 'SET{$D.PLATFORM_ID}{$kART}',
									'INSERT' : 'append',
									'data': {
										'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
										'D[SET][QUANTITY]' : '{$SET.QUANTITY}',
										'D[SET][ACTIVE]' : '{$SET.ACTIVE}'
										}
									});
								{/foreach}
						</script>
					*}
						
					</div>
						{*VARIANTEN*}
						
						{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
							{$VAR = $PLA.ARTICLE.D[$kVAR]}
							<table class="table a{$D.PLATFORM_ID}{$kART} art_all art_{$kVAR}">
							<thead>
								<tr>
									<th style="width:150px;"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$VAR.FILE.D)}_25x25.jpg"> {strip}{$ART.NUMBER} - 
										{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
										{for $i=0 to count((array)$aVAR)-1}
											{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
										{/for}{/strip}
									</th>
									<th><button class="btn btn-default" type="button" onclick="add_article_set('{$kVAR}');">+</button></th>
									<th>Aktive</th>
									<th>ID</th>
									<th>Bild</th>
									<th>NUM</th>
									<th>Title</th>
									<th title="Einzelpreis">Preis</th>
									<th style="text-align:right;">Bestand</th>
									<th>Anzhl</th>
								</tr>
							</thead>
							{*<tbody id="SET{$D.PLATFORM_ID}{$kVAR}"></tbody>*}
							<tbody id="SET{$D.PLATFORM_ID}{$kVAR}">
								{foreach from=$VAR.SET.ARTICLE.D key="kSET" item="SET"}
									
									<tr id="SET{$D.PLATFORM_ID}{$kVAR}{$kSET}">
										<td style="text-align:center;">{$smarty.foreach.SET.iteration}</td>
										<td><button class="btn" type="button" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('SET{$D.PLATFORM_ID}{$kVAR}{$kSET}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SET][ARTICLE][D][{$kSET}][ACTIVE]').value = -2;}">-</button></td>
											<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SET][ARTICLE][D][{$kSET}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SET][ARTICLE][D][{$kSET}][ACTIVE]" value="{$SET.ACTIVE}">
												<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SET][ARTICLE][D][{$kSET}][ACTIVE]').value = (this.checked)?1:0;" {if $SET.ACTIVE}checked{/if}>
											</td>
										<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$kSET}', 'TITLE' : '{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].NUMBER} | {$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kSET}'});">{$kSET}</button></td>
										<td><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{array_key_first((array)$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].FILE.D)}_50x50.jpg"></td>
										<td>{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].NUMBER}</td>
										<td>{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}</td>
										<td style="text-align:right;">{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].PRICE|number_format:2:".":""}</td>
										<td style="text-align:right;">{$D.SET.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kSET].STOCK}</td>
										<td><input name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][SET][ARTICLE][D][{$kSET}][QUANTITY]" style="text-align:right;width:50px;" value="{$SET.QUANTITY}"></td>
									</tr>

								{/foreach}
							</tbody>
							
						</table>
						{*
						<script>
								{foreach from=$VAR.SET.ARTICLE.D key="kSET" item="SET" name="SET"}
									wp.ajax({
									'url' : '?D[PAGE]=platform.article&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kVAR}',
									'div' : 'SET{$D.PLATFORM_ID}{$kVAR}',
									'INSERT' : 'append',
									'ASYNC' : true,
									'data': {
										'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
										'D[SET][QUANTITY]' : '{$SET.QUANTITY}',
										'D[SET][ACTIVE]' : '{$SET.ACTIVE}'
										}
									});
								{/foreach}
						</script>
						*}
						{/foreach}
						
				</div>
{*/if*}
				{*SET ARTICLE ENDE =======*}
				{*INSET ARTICLE START ========*}
				<div class="art-nav nav7" style="display:none;">

					{if $ART.INSET.ARTICLE.D}
						<div>Vater Artikel</div>
					<table class="table">
						<tr>
							<td>ID</td>
							<td>Nummer</td>
							<td>Summe</td>
						</tr>
					{foreach from=$ART.INSET.ARTICLE.D key="kINSET" item="INSET" name="INSET"}
						<tr>
							<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}gk37ezz6m', 'TITLE' : '{$INSET.NUMBER} |' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]={$kINSET}'});">{$kINSET}</button></td>
							<td>{$INSET.NUMBER}</td>
							<td style='text-align:right;'>{$INSET.STOCK}</td>
						</tr>
					{/foreach}
					</table>
					{/if}
					
					{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
						{$VAR = $PLA.ARTICLE.D[$kVAR]}
						<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kVAR}">
							<div>Vartiation: {$VAR.NUMBER}</div>
							<table class="table">
								<tr>
									<td>ID</td>
									<td>Nummer</td>
									<td>Summe</td>
								</tr>
							{foreach from=$VAR.INSET.ARTICLE.D key="kINSET" item="INSET" name="INSET"}
								<tr>
									<td><button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}gk37ezz6m', 'TITLE' : '{$INSET.NUMBER} |' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]={$kINSET}'});">{$kINSET}</button></td>
									<td>{$INSET.NUMBER}</td>
									<td style='text-align:right;'>{$INSET.STOCK}</td>
								</tr>
							{/foreach}
							</table>
						</div>
					{/foreach}
				</div>
				{*INSET ARTICLE ENDE =========*}

				{*REFERENCE START =========*}
				{*$X['D'] = $D}
				{$POST['POST'] = $CWP->http_build_query($X)*}
				{$RCS['DELIMITER']['LEFT'] = '[('}
				{$RCS['DELIMITER']['RIGHT'] = ')]'}
				{$TITLE = $CWP->rand_choice_str($ART.LANGUAGE.D.DE.DESCRIPTION.TITLE,$RCS)}
				<div class="art-nav nav2" style="display:none">
					<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kART}">
					{* ToDo: Problem mit get_object_reqursive Weil Artikel.Platform Knotten für Referenz hat!*}
						{foreach from=$D.PLATFORM.D key="kPLA2" item="PLA2"}
							
							{if ($D.PLATFORM.D[$kPLA2].PARENT_ID == $D.PLATFORM_ID && $D.PLATFORM.D[$kPLA2].ACTIVE == 1 && $D.PLATFORM.D[$kPLA2].CATEGORIE.D) || $ART.PLATFORM.D[$kPLA2].REFERENCE.D}
							<div style="border:solid 2px #999;">
							<img style="width:20px;" src="core/platform/{$D.PLATFORM.D[$kPLA2].CLASS_ID}/{$D.CLASS[$D.PLATFORM.D[$kPLA2].CLASS_ID].ICON}"> <b>{$D.PLATFORM.D[ $kPLA2 ].TITLE}</b>
							<table class="list" style="width:100%;" cellpadding="0" cellspacing="0">
								<thead>
									<tr>
										<td></td>
										<td><button type="button" class="btn btn-success" onclick="add_reference('{$D.PLATFORM_ID}','{$kART}','{$kPLA2}');">+</button></td>
										<td>Aktive(-1|0|1)</td>
										<td>Fail</td>
										<td>Referenz</td>
										<td>Link</td>
										<td>Update Date</td>
										<td>iDate
											<button type="button" class="btn btn-secondary" onclick="if (confirm('Dieses Artikel soll bei ebay neu angelegt werden?'))window.open('cronjob_test.php?D[PLATFORM][D][{$kPLA2}][ARTICLE][W][ID]={$kART}&D[PLATFORM][W][ID]={$kPLA2}&D[SESSION][ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}','_blank');">ADD</button>
											{if $ART.PLATFORM.D[$kPLA2].REFERENCE.D}<button type="button" class="btn btn-secondary" onclick="if (confirm('Dieses Artikel soll bei ebay neu angelegt werden?'))window.open('cronjob_test.php?D[PLATFORM][D][{$kPLA2}][ARTICLE][W][ID]={$kART}&D[PLATFORM][W][ID]={$kPLA2}&D[SESSION][ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[UPDATE]=1','_blank');">UPDATE</button>{/if}
										</td>
										<td style='text-align:right;'>Sale</td>
										<td style='text-align:right;'>AVG Sale</td>
									</tr>
								</thead>
								<tbody id="REF{$D.PLATFORM_ID}{$kART}{$kPLA2}">
									{foreach from=$ART.PLATFORM.D[$kPLA2].REFERENCE.D key="kREF" item="REF" name="REF"}
									<tr id="REF{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kREF}" style="{if $REF.ACTIVE <0}text-decoration:line-through;color:red;{elseif $REF.ACTIVE <1}color:red;{/if}">
										<td style="text-align:center;">{$smarty.foreach.REF.iteration}</td>
										<td><button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('REF{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kREF}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = -2;}">-</button></td>
										<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]" value="{$REF.ACTIVE}">
											<input type="radio" name="act{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kREF}" title="zum löschen vorgemerkt" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = -1;" {if $REF.ACTIVE == -1}checked{/if}>
											<input type="radio" name="act{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kREF}" title="inaktive wird nicht aktuallisiert" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = 0;" {if $REF.ACTIVE == 0}checked{/if}>
											<input type="radio" name="act{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kREF}" title="aktive wird aktuallisiert" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = 1;" {if $REF.ACTIVE == 1}checked{/if}>
										</td>
										<td style="{if $REF.FAIL > 0}color:red{/if}">
											<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][FAIL]" value="0">
										{$REF.FAIL}</td>
										<td>{$kREF}</td>
										<td><a href="http://www.ebay.de/itm/{$kREF}" target="_blank">{$kREF}</a></td>
										<td>{$REF.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</td>
										<td>{$REF.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</td>
										<td style='text-align:right;'>{if $REF.ORDER.STOCK}{$REF.ORDER.STOCK}{else}0{/if}</td>
										<td style='text-align:right;'>{if $REF.ORDER.PRICE}{($REF.ORDER.PRICE/count((array)$REF.ORDER.D))|number_format:2:".":""}{else}0{/if} €</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
							</div>
							{/if}
						{/foreach}
						
					</div>	

					{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
						{$VAR = $PLA.ARTICLE.D[$kVAR]}
					<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kVAR}">
						
						<div>
						
						{foreach from=$D.PLATFORM.D key="kPLA2" item="PLA2"}
							{if ($D.PLATFORM.D[$kPLA2].PARENT_ID == $D.PLATFORM_ID && $D.PLATFORM.D[$kPLA2].ACTIVE == 1 && $D.PLATFORM.D[$kPLA2].CATEGORIE.D) || $VAR.PLATFORM.D[$kPLA2].REFERENCE.D}
							<img style="width:20px;" src="core/platform/{$D.PLATFORM.D[$kPLA2].CLASS_ID}/{$D.CLASS[$D.PLATFORM.D[$kPLA2].CLASS_ID].ICON}"> <b>{$D.PLATFORM.D[ $kPLA2 ].TITLE}</b>
							<table class="list" style="width:100%;" cellpadding="0" cellspacing="0">
								<thead>
									<tr>
										<td></td>
										<td><button type="button" class="btn btn-success" onclick="add_reference('{$D.PLATFORM_ID}','{$kVAR}','{$kPLA2}');">+</button></td>
										<td>Aktive (-1|0|1)</td>
										<td>Fail</td>
										<td>Referenz</td>
										<td>Link</td>
										<td>Update Date</td>
										<td>iDate</td>
										<td style='text-align:right;'>Sale</td>
										<td style='text-align:right;'>AVG Sale</td>
									</tr>
								</thead>
								<tbody id="REF{$D.PLATFORM_ID}{$kVAR}{$kPLA2}">
									{foreach from=$VAR.PLATFORM.D[$kPLA2].REFERENCE.D key="kREF" item="REF" name="REF"}
									<tr id="REF{$D.PLATFORM_ID}{$kVAR}{$kPLA2}{$kREF}" style="{if $REF.ACTIVE <0}text-decoration:line-through;color:red;{elseif $REF.ACTIVE <1}color:red;{/if}">
										<td style="text-align:center;">{$smarty.foreach.REF.iteration}</td>
										<td><button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('REF{$D.PLATFORM_ID}{$kVAR}{$kPLA2}{$kREF}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = -2;}">-</button></td>
										<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]" value="{$REF.ACTIVE}">
											<input type="radio" name="act{$D.PLATFORM_ID}{$kVAR}{$kPLA2}{$kREF}" title="zum löschen vorgemerkt" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = -1;" {if $REF.ACTIVE == -1}checked{/if}>
											<input type="radio" name="act{$D.PLATFORM_ID}{$kVAR}{$kPLA2}{$kREF}" title="inaktive wird nicht aktuallisiert" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = 0;" {if $REF.ACTIVE == 0}checked{/if}>
											<input type="radio" name="act{$D.PLATFORM_ID}{$kVAR}{$kPLA2}{$kREF}" title="aktive wird aktuallisiert" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][ACTIVE]').value = 1;" {if $REF.ACTIVE == 1}checked{/if}>
										</td>
										<td style="{if $REF.FAIL > 0}color:red{/if}">
											<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][REFERENCE][D][{$kREF}][FAIL]" value="0">
											{$REF.FAIL}</td>
										<td>{$kREF}</td>
										<td><a href="http://www.ebay.de/itm/{$kREF}" target="_blank">{$kREF}</a></td>
										<td>{$REF.UTIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</td>
										<td>{$REF.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</td>
										<td style='text-align:right;'>{if $REF.ORDER.STOCK}{$REF.ORDER.STOCK}{else}0{/if}</td>
										<td style='text-align:right;'>{if $REF.ORDER.PRICE}{($REF.ORDER.PRICE/count((array)$REF.ORDER.D))|number_format:2:".":""}{else}0{/if} €</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
							{/if}
						{/foreach}
						</div>
					</div>
					{/foreach}

				</div>
				{*REFERENCE ENDE =========*}
{*if !$ART.SET && !$first_VAR.SET*}
				{*LAGER START ============*}
				{if !$first_VAR.SET} {*wenn SET dann ausblenden*}
				<div class="art-nav nav3" style="display:none;">
					{if !$ART.VARIANTE.D}
						<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kART}">
						<table class="table">
							<thead>
							<tr>
								<th></th>
								{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
									{*if $WAR.Active.VALUE == 1*}
									<th style="ackground:#eee;">
										{$WAR.Title} |
										{if isset($WAR.STORAGE.D)}
											<select id="sel{$kART}{$kWAR}">
											{foreach from=$WAR.STORAGE.D key="kSTO" item="STO" name="STO"}
												<option value="{$kSTO}">{$STO.Title}</option>
											{/foreach}
											</select><button class="btn btn-success" type="button" onclick="id = wp.get_genID();$('#ul{$kART}{$kWAR}').append('<li>'+$('#sel{$kART}{$kWAR} :selected').text()+'<input type=\'hidden\' name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D]['+id+'][STORAGE]\' value=\''+$('#sel{$kART}{$kWAR}').val()+'\'><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D]['+id+'][Stock]\' value=\'\' style=\'width:50px;\'></li>');">+</button>
										{/if}
									</th>
									{*/if*}
								{/foreach}
							</tr>
							</thead>
							<tbody>
							<tr>
								<td align="center">
									<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFileFirstID}_40x40.jpg") center no-repeat;'></div> {$ART.NUMBER}
								</td>
								{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
								
								<td>
									<ul id="ul{$kART}{$kWAR}" style="list-style:none;padding:0;">
										{foreach from=$ART.ARTICLE_STORAGE.D key="kSTO" item="STO" name="STO"}
											{if $WAR.STORAGE.D[$STO.STORAGE]}
											<li id="stock_pl_{$D.PLATFORM_ID}{$kWAR}{$kSTO}{$kART}">
												<div style="float:left;width:50px;">
												<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D][{$kSTO}][Active]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D][{$kSTO}][Active]" value="{$STO.Active}">
												<button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ $('#stock_pl_{$D.PLATFORM_ID}{$kWAR}{$kSTO}{$kART}').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D][{$kSTO}][Active]').value='-2';}">-</button>
												{$WAR.STORAGE.D[$STO.STORAGE].Title}: 
												</div>
												<div style="float:left;">{input p=[ 'type' => 'number', 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_STORAGE][D][{$kSTO}][Stock]", 'style'=>'width:50px;text-align:right;', 'value'=>$STO.Stock ]}</div>
												
												<div style="clear:both"></div></li>	
											{/if}
										{/foreach}
									</ul>
								</td>
								
								{/foreach}
							</tr>
							</tbody>
						</table>
						</div>
					{else}
						{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
							{if 1 || $VAR.ACTIVE || $VAR.STOCK > 0}
								{$VARkFileFirstID = current(array_keys((array)$VAR.FILE.D))}
						<div class="{*a{$D.PLATFORM_ID}{$kART} art_all*} art_{$kVAR}">
							{*VARIANTE*}
							
										<table class="table">
											<thead>
												<tr>
												<th></th>
													
												{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
														<th>
															{$WAR.Title} | 
															{if isset($WAR.STORAGE.D)}
																<select id="sel{$kVAR}{$kWAR}">
																	{foreach from=$WAR.STORAGE.D key="kSTO" item="STO" name="STO"}
																		{*if $STO.Active*}
																		<option value="{$kSTO}">{$STO.Title}</option>
																		{*/if*}
																	{/foreach}
																</select>
																<button class="btn btn-success" type="button" onclick="id = wp.get_genID();$('#ul{$kVAR}{$kWAR}').append('<li>'+$('#sel{$kVAR}{$kWAR} :selected').text()+'<input type=\'hidden\' name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D]['+id+'][STORAGE]\' value=\''+$('#sel{$kVAR}{$kWAR}').val()+'\'><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D]['+id+'][Stock]\' value=\'\' style=\'width:50px;\'></li>');">+</button>
																
															{/if}
														</th>
												{/foreach}
												</tr>
											</thead>
											<tbody>
											
											<tr>
												<td align="center">
													<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$VARkFileFirstID}_40x40.jpg") center no-repeat;'></div> {$VAR.NUMBER}
												</td>
											{foreach from=$PLA.WAREHOUSE.D key="kWAR" item="WAR" name="WAR"}
												
												<td>	
													<ul id="ul{$kVAR}{$kWAR}" style="list-style:none;padding:0;">

													{foreach from=$VAR.ARTICLE_STORAGE.D key="kSTO" item="STO" name="STO"}
														{if $WAR.STORAGE.D[$STO.STORAGE]}
														<li id="stock_pl_{$D.PLATFORM_ID}{$kWAR}{$kSTO}{$kVAR}">
															<div style="float:left;width:50px;">
															<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D][{$kSTO}][Active]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D][{$kSTO}][Active]" value="{$STO.Active}">
															<button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ $('#stock_pl_{$D.PLATFORM_ID}{$kWAR}{$kSTO}{$kVAR}').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D][{$kSTO}][Active]').value='-2';}">-</button>
															{$WAR.STORAGE.D[$STO.STORAGE].Title}: 
															</div>
															<div style="float:left;">{input p=[ 'type' => 'number', 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_STORAGE][D][{$kSTO}][Stock]", 'style'=>'width:50px;text-align:right;', 'value'=>$STO.Stock ]}</div>
															
															<div style="clear:both"></div></li>	
														{/if}
													{/foreach}
													</ul>
												</td>
												
												{/foreach}
											</tr>
											</tbody>
										</table>

						</div>
							{/if}
						{/foreach}
					{/if}
					

				</div>
				{/if}
				{*LAGER ENDE =============*}
				{*Lieferant START ============*}
				{if !$first_VAR.SET} {*wenn SET dann ausblenden*}
				<div class="art-nav nav4" style="display:none;">
{*if $D.SESSION.USER.ACCOUNT.D[$D.SESSION.ACCOUNT_ID].PLATFORM.D[$D['PLATFORM_ID']].RIGHT.D['WP00006'].ACTIVE*}
			{*$first_VAR = current((array)$PLA.ARTICLE.PARENT.D[ $kART ].CHILD.D)*}
			
				{if !$ART.VARIANTE.D} {*Einblenden wenn keine Variationen vorhaden sind*}
						<table class="table">
							<thead>
							<tr>
								<th style="width:100px;">Parent</th>
								<th>
									{if isset($PLA.SUPPLIER.D)}
										<select id="sel{$kART}_sup">
										{foreach from=$PLA.SUPPLIER.D key="kSUP" item="SUP" name="SUP"}
											<option value="{$kSUP}">{$SUP['Title']}</option>
										{/foreach}
										</select>
										{*
										<button class="btn btn-success" type="button" onclick="id = wp.get_genID();$('#ul{$kART}_sup').append('<tr><td></td><td>'+$('#sel{$kART}_sup :selected').text()+'</td><td><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][SUPPLIER][D]['+$('#sel{$kART}_sup :selected').attr('value')+'][ARTICLE_SUPPLIER][D]['+id+'][ARTICLE]\' value=\'{$kART}\'><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D]['+id+'][SUPPLIER]\' value=\''+$('#sel{$kART}_sup :selected').attr('value')+'\'><textarea style=\'width:100%;\' name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D]['+id+'][Comment]\'></textarea></td></tr>');">&#xf055;</button>
									*}
									<button class="btn btn-success" type="button" onclick="id = wp.get_genID();$('#ul{$kART}_sup').append('<tr><td></td><td>'+$('#sel{$kART}_sup :selected').text()+'</td><td><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D]['+id+'][ARTICLE]\' value=\'{$kART}\'><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D]['+id+'][SUPPLIER]\' value=\''+$('#sel{$kART}_sup :selected').attr('value')+'\'><textarea style=\'width:100%;\' name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D]['+id+'][Comment]\'></textarea></td></tr>');">&#xf055;</button>
									{/if}
								</th>
							</tr>
							</thead>
							<tr>
								<td align="center" style="width:100px;">
									<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFileFirstID}_40x40.jpg") center no-repeat;'></div> {$ART.NUMBER}
								</td>
								<td>
									<table class="table" id="ul{$kART}_sup" style="list-style:none;padding:0;">
										<thead>
											<tr>
												<th></th>
												<th>Hersteller</th>
												<th style="width:10%;">ANr</th>
												<th style="width:20%;">Titel</th>
												<th style="width:10%;">EK-Preis</th>
												<th style="width:5%;"></th>
												<th>Bestellt</th>
												<th>Kommentar</th>
											</tr>
										</thead>
										{*foreach from=$PLA.ARTICLE_TO_SUPPLIER['ArticleId'].D[$kART].D key="kA2S" item="A2S" name="A2S"*}
										
										{foreach from=$ART['ARTICLE_SUPPLIER'].D key="kA2S" item="A2S" name="A2S"}
											
											<tr id="id{$D.PLATFORM_ID}{$kART}{$kA2S}">
												<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]" value="{$SUP.Active}">
													<button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('id{$D.PLATFORM_ID}{$kART}{$kA2S}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]').value = -2;}">&#xf056;</button></td>
												</td>
												<td>{$PLA.SUPPLIER.D[ $A2S.SUPPLIER ].Title}</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Number]", 'value'=>$A2S.Number]}
												</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Title]", 'value'=>$A2S.Title]}</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Price]", 'type' => 'number', 'onchange' => "this.value = parseFloat(this.value.replace(/,/, '.'))", 'value'=>$A2S.Price]}</td>
												<td>{$PLA.SUPPLIER.D[ $A2S.SUPPLIER ].CurrencyId}</td>
												<td>-</td>
												<td>{input p=['type'=>'textarea','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][ARTICLE_SUPPLIER][D][{$kA2S}][Comment]", 'value'=>$A2S.Comment]}</td>
											</tr>
										{/foreach}
										
									</table>
									
								</td>
							</tr>
						</table>
				{/if}
						{*VARIANTE*}
						{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
							{$VARkFileFirstID = current(array_keys((array)$VAR.FILE.D))}
							<div class="{*a{$D.PLATFORM_ID}{$kART} art_all*} art_{$kVAR}">
							<table class="table">
								<thead>
								<tr>
									<th style="width:100px;">child</th>
									<th>
										{if isset($PLA.SUPPLIER.D)}
											<select id="sel{$kART}{$kVAR}_sup">
												{foreach from=$PLA.SUPPLIER.D key="kSUP" item="SUP" name="SUP"}
													<option value="{$kSUP}">{$SUP['Title']}</option>
												{/foreach}
											</select><button class="btn btn-success" type="button" onclick="id = wp.get_genID();$('#ul{$kART}{$kVAR}_sup').append('<tr><td></td><td>'+$('#sel{$kART}{$kVAR}_sup :selected').text()+'</td><td><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D]['+id+'][ARTICLE]\' value=\'{$kVAR}\'><input name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D]['+id+'][SUPPLIER]\' value=\''+$('#sel{$kART}{$kVAR}_sup :selected').attr('value')+'\' style=\'width:50px;\'></td><td><textarea style=\'width:100%;\' name=\'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D]['+id+'][Comment]\'></textarea></td></tr>');">&#xf055;</button>
										{/if}
									</th>
								</tr>
								</thead>
								<tr>
									<td align="center">
										<div style='width:40px;height:40px;background:url("file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$VARkFileFirstID}_40x40.jpg") center no-repeat;'></div> 
										{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
										{for $i=0 to count((array)$aVAR)-1}
											{$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count((array)$aVAR)-1} | {/if}
										{/for}<br>
										{$VAR.NUMBER}
									</td>
									<td>
										<table class="table" id="ul{$kART}{$kVAR}_sup" style="list-style:none;padding:0;">
											<thead>
												<tr>
													<th></th>
													<th>Hersteller</th>
													<th style="width:10%;">ANr</th>
													<th style="width:20%;">Titel</th>
													<th style="width:10%;">EK-Preis</th>
													<th style="width:5%;"></th>
													<th>Bestellt</th>
													<th>Kommentar</th>
												</tr>
											</thead>
											{*foreach from=$PLA.ARTICLE_TO_SUPPLIER['ArticleId'].D[$kVAR].D key="kA2S" item="A2S" name="A2S"*}
											
											{foreach from=$PLA.ARTICLE.D[$kVAR]['ARTICLE_SUPPLIER'].D key="kA2S" item="A2S" name="A2S"}
											
												<tr id="id{$D.PLATFORM_ID}{$kVAR}{$kA2S}">
													<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]" value="{$SUP.Active}">
														<button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('id{$D.PLATFORM_ID}{$kVAR}{$kA2S}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Active]').value = -2;}">&#xf056;</button></td>
													</td>
													<td>{$PLA.SUPPLIER.D[ $A2S.SUPPLIER ].Title}</td>
													<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Number]", 'value'=>$A2S.Number]}
													</td>
													<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Title]", 'value'=>$A2S.Title]}</td>
													<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Price]", 'type' => 'number', 'onchange' => "this.value = parseFloat(this.value.replace(/,/, '.'))", 'value'=>$A2S.Price]}</td>
													<td>{$PLA.SUPPLIER.D[ $A2S.SUPPLIER ].CurrencyId}</td>
													<td>-</td>
													<td>{input p=['type'=>'textarea','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_SUPPLIER][D][{$kA2S}][Comment]", 'value'=>$A2S.Comment]}</td>
												</tr>
											{/foreach}

											{*foreach from=$VAR['ARTICLE_SUPPLIER'].D key="kA2S" item="A2S" name="A2S"}
											<tr id="id{$D.PLATFORM_ID}{$kVAR}{$kA2S}">
												<td><input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Active]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Active]" value="{$SUP.Active}">
													<button type="button" class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('id{$D.PLATFORM_ID}{$kVAR}{$kA2S}').style.display = 'none'; document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Active]').value = -2;}">&#xf056;</button></td>
												</td>
												<td>{$PLA.SUPPLIER.D[$_ATS_to_SUP[$kA2S]]['Title']}</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Number]", 'value'=>$A2S.Number]}
												</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Title]", 'value'=>$A2S.Title]}</td>
												<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Price]", 'type' => 'number', 'onchange' => "this.value = parseFloat(this.value.replace(/,/, '.'))", 'value'=>$A2S.Price]}</td>
												<td>{$PLA.SUPPLIER.D[$_ATS_to_SUP[$kA2S]]['CurrencyId']}</td>
												<td>-</td>
												<td>{input p=[ 'type'=>'textarea','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][ARTICLE_TO_SUPPLIER][D][{$kA2S}][Comment]", 'value'=>$A2S.Comment]}</td>
											</tr>
											{/foreach*}

										</table>
										
										
									</td>
								</tr>
							</table>
							</div>
						{/foreach}
				
{*/if*}
						
				</div>
				{/if}
				{*Lieferant ENDE =============*}
{*/if*}
				{*Platform Attribute*}
				<div class="art-nav nav5" style="display:none;">
					<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kART}">
					<ul class="list-group">
					{foreach from=$D.PLATFORM.D key="kPLA2" item="PLA2"}
						{if $D.PLATFORM.D[$kPLA2].PARENT_ID == $D.PLATFORM_ID && $D.PLATFORM.D[$kPLA2].ACTIVE == 1 && $D.PLATFORM.D[$kPLA2].CATEGORIE.D}
							<li class="list-group-item">
								<div onclick="$('#PATT{$kPLA2}{$kART}').toggle();" style="cursor:pointer;"><h4 class="list-group-item-heading">{$D.PLATFORM.D[ $kPLA2 ].TITLE}</h4></div>
								{if $D.PLATFORM.D[$kPLA2].ATTRIBUTE}
								<table id="PATT{$kPLA2}{$kART}" style="display:none;" class="table">
									<thead>
										<tr>
											<th>ID</th>
											<th style="width:200px;">Title</th>
											{foreach from=$D.PLATFORM.D[$kPLA2].LANGUAGE.D key="kLAN" item="LAN" name="LAN"}
											<th>{$LAN.TITLE}</th>
											{/foreach}
										</tr>
									</thead>
									<tbody>
									{$is_value = 0}
									{foreach from=$D.PLATFORM.D[$kPLA2].ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATTT" item="ATTT" name="ATTT"}
										<thead><tr><td colspan={count($D.PLATFORM.D[$kPLA2].LANGUAGE.D)+3}><b>{$kATTT}</b></td></tr></thead>
										{foreach from=$D.PLATFORM.D[$kPLA2].ATTRIBUTE.PARENT.D[$kATTT].CHILD.D key="kATT" item="ATT" name="ATT"}
											{*if !isset($ATT.VALUE)*}
													{if $ATT.I18N}
													<tr style="att_pl_{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kLAN}{$kATT}">
														<td style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
														<td style="{if $ATT.REQUIRED}font-weight:bold;color:red;{/if}">{if $ATT.I18N}&#xf1ab;{/if} {if $ATT.LANGUAGE.D['DE'].TITLE}{$ATT.LANGUAGE.D['DE'].TITLE}{else}{$ATT.LANGUAGE.D[$kLAN].TITLE}{/if}</td>
														{foreach from=$D.PLATFORM.D[$kPLA2].LANGUAGE.D key="kLAN" item="LAN" name="LAN"}
															{$PATT = $ART.PLATFORM.D[$kPLA2].ATTRIBUTE.D[$kATT].LANGUAGE.D[$kLAN]}
															{if $ATT.LANGUAGE.D[$kLAN].VALUE || $PATT.VALUE}{$is_value = 1}{/if}
														<td>
															{if isset($PATT.VALUE)}{$VALUE = $PATT.VALUE}{else}{$VALUE = $ATT.LANGUAGE.D[$kLAN].VALUE}{/if}
															{input_att p=['type' => $ATT.TYPE, 'min' => $ATT.MIN, 'step' => $ATT.STEP, 'help' => $ATT.LANGUAGE.D[$kLAN].HELP, 'maxlength' => $ATT.MAXLENGTH, 'value' => $VALUE, 'option' => explode("\n",$ATT.LANGUAGE.D[$kLAN].VALUE_OPTION), 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][ATTRIBUTE][D][{$kATT}]{if $ATT.I18N}[LANGUAGE][D][{$kLAN}]{/if}"]}
														</td>
														{/foreach}
													</tr>
													{else}
													<tr style="att_pl_{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kLAN}{$kATT}">
														<td style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
														<td style="{if $ATT.REQUIRED}font-weight:bold;color:red;{/if}">{if $ATT.I18N}&#xf1ab;{/if} {$ATT.LANGUAGE.D['DE'].TITLE}</td>
														
															{$PATT = $ART.PLATFORM.D[$kPLA2].ATTRIBUTE.D[$kATT]}
															{if $ATT.LANGUAGE.D['DE'].VALUE || $PATT.VALUE}{$is_value = 1}{/if}
														<td colspan={count($D.PLATFORM.D[$kPLA2].LANGUAGE.D)+3}>
															{if isset($PATT.VALUE)}{$VALUE = $PATT.VALUE}{else}{$VALUE = $ATT.LANGUAGE.D['DE'].VALUE}{/if}
															{input_att p=['type' => $ATT.TYPE, 'min' => $ATT.MIN, 'step' => $ATT.STEP, 'help' => $ATT.LANGUAGE.D['DE'].HELP, 'maxlength' => $ATT.MAXLENGTH, 'value' => $VALUE, 'option' => explode("\n",$ATT.VALUE_OPTION), 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][PLATFORM][D][{$kPLA2}][ATTRIBUTE][D][{$kATT}]"]}
														</td>
													</tr>
													{/if}
												
											{*else}
												<tr style="att_pl_{$D.PLATFORM_ID}{$kART}{$kPLA2}{$kLAN}{$kATT}">
													<td style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
													{foreach from=$D.PLATFORM.D[$kPLA2].LANGUAGE.D key="kLAN" item="LAN" name="LAN"}
													<td style="{if $ATT.REQUIRED}font-weight:bold;color:red;{/if}">{$ATT.LANGUAGE.D[$kLAN].TITLE}<br>{$ATT.LANGUAGE.D[$kLAN].HELP}</td>
													{/foreach}
													<td></td>
												</tr>
											{/if*}
										{/foreach}
									{/foreach}
									</tbody>
								</table>
								{else}
									<span class="alert alert-warning">Kategorie ist nicht zugeordnet. Die Kategorie abhängige Attribute können erst nach der Zuordnung der Kategorie angezeigt werden.</span>
								{/if}
								<script>{if $is_value}$('#PATT{$kPLA2}{$kART}').show();{/if}</script>
							</li>
						{/if}
					{/foreach}
					</ul>
					</div>
					{*Varianten*}

					{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
						{$VAR = $PLA.ARTICLE.D[$kVAR]}
						<div class="a{$D.PLATFORM_ID}{$kART} art_all art_{$kVAR}">
						<ul class="list-group">
						{foreach from=$D.PLATFORM.D key="kPLA2" item="PLA2"}
							{if $D.PLATFORM.D[$kPLA2].PARENT_ID == $D.PLATFORM_ID && $D.PLATFORM.D[$kPLA2].ACTIVE == 1 && $D.PLATFORM.D[$kPLA2].CATEGORIE.D}
							<li class="list-group-item">
									<div onclick="$('#PATT{$kPLA2}{$kVAR}').toggle();" style="cursor:pointer;"><h4 class="list-group-item-heading">{$D.PLATFORM.D[ $kPLA2 ].TITLE}</h4></div>
									<table id="PATT{$kPLA2}{$kVAR}" style="display:none;" class="table">
										<thead>
											<tr>
												<th>ID</th>
												<th style="width:200px;">Title</th>
												{foreach from=$D.PLATFORM.D[$kPLA2].LANGUAGE.D key="kLAN" item="LAN" name="LAN"}
												<th>{$LAN.TITLE}</th>
												{/foreach}
											</tr>
										</thead>
										<tbody>
										{$is_value = 0}	
										
										{foreach from=$D.PLATFORM.D[$kPLA2].ATTRIBUTE.PARENT.D[NULL].CHILD.D key="kATTT" item="ATTT" name="ATTT"}
											<thead><tr><th colspan=3><b>{$kATTT}</b></th></tr></thead>
											{foreach from=$D.PLATFORM.D[$kPLA2].ATTRIBUTE.PARENT.D[$kATTT].CHILD.D key="kATT" item="ATT" name="ATT"}
												{if $ATT.TO_PIN != 'parent'}
													{if !isset($ATT.VALUE)}
														{foreach from=$D.PLATFORM.D[$kPLA2].LANGUAGE.D key="kLAN" item="LAN" name="LAN"}
															{if $ATT.I18N}
																{$PATT = $VAR.PLATFORM.D[$kPLA2].ATTRIBUTE.D[$kATT].LANGUAGE.D[$kLAN]}
															{else}
																{$PATT = $VAR.PLATFORM.D[$kPLA2].ATTRIBUTE.D[$kATT]}
															{/if}
															{if $LAN.VALUE || $PATT.VALUE}{$is_value = 1}{/if}
															<tr style="att_pl_{$D.PLATFORM_ID}{$kART}{$kVAR}{$kPLA2}{$kLAN}{$kATT}">
																<td style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
																<td style="{if $ATT.REQUIRED}font-weight:bold;color:red;{/if}">{if $ATT.I18N}&#xf1ab;{/if} {$ATT.LANGUAGE.D['DE'].TITLE}</td>
																<td>
																	{if isset($PATT.VALUE)}{$VALUE = $PATT.VALUE}{else}{$VALUE = $LAN.VALUE}{/if}
																	{input_att p=['type' => $ATT.TYPE, 'min' => $ATT.MIN, 'step' => $ATT.STEP, 'help' => $LAN.HELP, 'maxlength' => $ATT.MAXLENGTH, 'value' => $VALUE, 'option' => explode("\n",$ATT.OPTION), 'name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kVAR}][PLATFORM][D][{$kPLA2}][ATTRIBUTE][D][{$kATT}]{if $ATT.I18N}[LANGUAGE][D][{$kLAN}]{/if}"]}
																</td>
															</tr>
														{/foreach}
													{else}
														<tr style="att_pl_{$D.PLATFORM_ID}{$kART}{$kVAR}{$kPLA2}{$kLAN}{$kATT}">
															<td style="cursor:pointer;" title="{$kATT}" onclick="navigator.clipboard.writeText('{$kATT}');">&#xf0c5;</td>
															<td style="{if $ATT.REQUIRED}font-weight:bold;color:red;{/if}">{$ATT.LANGUAGE.D['DE'].TITLE}<br>{$LAN.HELP}</td>
															<td></td>
														</tr>
													{/if}
												{/if}
											{/foreach}
										{/foreach}
										</tbody>
									</table>
									<script>
									{if $is_value}$('#PATT{$kPLA2}{$kVAR}').show();{/if}
									</script>
							</li>
							{/if}
						{/foreach}
						</ul>
						</div>
					{/foreach}
				</div>
				
				{*SHIPPING*}
{*
				<div class="art-nav nav6" style="display:none">

				<table class="table">
					<thead>
						<tr>
							<th>Versandart</th>
							<th>Preis</th>
							<th>Lieferland</th>
						</tr>
					</thead>
					{foreach from=$ART.SHIPPING.D key="kSHI" item="SHI" name="SHI"}
						{foreach from=$SHI.COST.D key="kCOST" item="COST" name="COST"}
						<tr>
							<td>{i18n id="shipping_{$kSHI}"</td>
							<td style="text-align:right;">{$COST.PRICE} €</td>
							<td>
								{foreach from=$COST.COUNTRY.D key="kCOUNTRY" item="COUNTRY" name="COUNTRY"}
									<nobr>[<b>{$kCOUNTRY}</b> {$COUNTRY.TITLE}]</nobr>
								{/foreach}
							</td>
						</tr>
						{/foreach}
					{/foreach}
				</table>	
					
				</div>
*}
			</div>
			
	</td></tr>
</table>
		</form>
			<script>
			tabs = function(artID, nav,nr)
			{
				$.each($("."+artID+" ."+nav), function() {
					$(this).hide();
				});
				$('.'+artID+' .'+nav+'.nav'+nr).show();

				$.each($("."+artID+" ."+nav+'-bar li'), function() {
					$(this).removeClass('active');
				});
				$("."+artID+" ."+nav+'-bar li.bar'+nr).addClass('active');
			}
			tabs('a{$D.PLATFORM_ID}{$kART}','art-nav',0);

			SAVEART{$D.PLATFORM_ID}{$kART} = function()
			{
				$('#form{$D.PLATFORM_ID}{$kART}').find(':input').each(function(){
					if( $(this).attr( "ischanged" ) == 0 )
						$(this).attr( "disabled", true );
				});
				
				wp.ajax({
				'url' : '?D[PAGE]=platform.article&D[ACTION]=set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.PLATFORM_ID}{$kART}').serialize()
				});

				$('#form{$D.PLATFORM_ID}{$kART}').find(':input').each(function(){
					$(this).attr( "disabled", false );
				});
			}

			//damit werden sämptliche Bereiche für das Artikel eingeblendet und restliche ausgeblendet
		select_article = function(id, article_id)
		{
			$('.'+id+' .art_all').hide();
			$('.art_'+article_id).show();

			$.each($('.art-nav-left'+id+'bar li'), function() {
					$(this).removeClass('active');
			});
			$('.art-nav-left'+id+'bar .left'+id+'bar_'+article_id).addClass('active');
		}
		
			select_article('a{$D.PLATFORM_ID}{$kART}','{$kART}');//Setze Auf Parent
		  </script>
		{/foreach}
		
		<script>


		{*$Art_Cats = array_keys((array)$ART.CATEGORIE.D)*}
		{$Art_Cats[0] = $ART.ATTRIBUTE.D['CategorieId'].VALUE}
		
		{$TITLE = $CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,['DELIMITER'=>['LEFT' => '[('],'RIGHT' => ')]'])}
		wp.window.bar('ART{$D.PLATFORM_ID}{$kART}',{ 'Breadcrumb' : '{str_replace('|',' / ',$PLA.CATEGORIE.D[$Art_Cats[0]].LANGUAGE.D['DE'].BREADCRUMB_TITLE)} / {$PLA.CATEGORIE.D[$Art_Cats[0]].LANGUAGE.D['DE'].TITLE} / {$TITLE|replace:'\'':'\\\''|truncate:80:"":false}'});

		add_article_set = function(kART)
		{
			contain_article_id = window.prompt("Bitte hier Artikel ID eingeben","");
			if(contain_article_id)
			{
				wp.ajax({
					'url' : '?D[PAGE]=platform.article&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]='+kART,
					'div' : 'SET{$D.PLATFORM_ID}'+kART,
					'INSERT' : 'append',
					'data': {
						'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : contain_article_id,
						'D[SET][QUANTITY]' : '{$SET.QUANTITY}',
						'D[SET][ACTIVE]' : '{$SET.ACTIVE}'
						}
					});
			}
		}

		add_reference = function(from_pl_id,from_art_id,to_pl_id)
		{
			to_art_id = window.prompt("Bitte hier die E-Bay Artikelnummer eingeben","");
			if(to_art_id)
			{ 
				h = "<tr id='REF"+from_pl_id+from_art_id+to_pl_id+to_art_id+"'>"
					+ "<td style='text-align:center;'></td>"
					+ "<td><button type='button' class='del' onclick=\"if(confirm('Wirklich löschen?')){ document.getElementById('REF"+from_pl_id+from_art_id+to_pl_id+to_art_id+"').style.display = 'none'; document.getElementById('D[PLATFORM][D]["+from_pl_id+"][ARTICLE][D]["+from_art_id+"][PLATFORM][D]["+to_pl_id+"][REFERENCE][D]["+to_art_id+"][ACTIVE]').value = -2;}\">-</button></td>"
					+ "<td><input type='hidden' id=\"D[PLATFORM][D]["+from_pl_id+"][ARTICLE][D]["+from_art_id+"][PLATFORM][D]["+to_pl_id+"][REFERENCE][D]["+to_art_id+"][ACTIVE]\" name=\"D[PLATFORM][D]["+from_pl_id+"][ARTICLE][D]["+from_art_id+"][PLATFORM][D]["+to_pl_id+"][REFERENCE][D]["+to_art_id+"][ACTIVE]\" value='1'>"
					+ "	<input type='checkbox' onclick=\"document.getElementById('D[PLATFORM][D]["+from_pl_id+"][ARTICLE][D]["+from_art_id+"][PLATFORM][D]["+to_pl_id+"][REFERENCE][D]["+to_art_id+"][ACTIVE]').value = (this.checked)?1:0;\" checked>"
					+ "</td>"
					+ "<td>"+to_art_id+"</td>"
					+ "<td><a href='http://www.ebay.de/itm/"+to_art_id+"' target='_blank'>"+to_art_id+"</a></td>"
					+ "<td>---</td>"
				+ "</tr>";
				$("#REF"+from_pl_id+from_art_id+to_pl_id).append(h);
			}
		}

		</script>
		
			<script>
				$(document).ready(function() {
					$('#form{$D.PLATFORM_ID}{$kART} select[multiple]').select2({
						minimumResultsForSearch: 15
						,closeOnSelect: false
						//,theme: "bootstrap"
						//,tags : true
					});

					$('.select2').addClass('form-control');
					$('.select2').css('width','min-content');
					showLang('DE','{$D.PLATFORM_ID}','{$kART}');
				});
			</script>
{/switch}



