{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<!doctype html>
<html lang="de">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		{*mobile Start*}
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#6c757d">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="light-content">
		<meta name="apple-mobile-web-app-title" content="Hexcon">
		<meta name="viewport" content="width = device-width, height = device-height, initial-scale = 1, user-scalable = no">
		<meta name="msapplication-TileColor" content="#f9f9da">
		<link rel="shortcut icon" href="view/account/template/core/favicon/favicon.ico">
		{*
		<link rel="manifest" href="view/account/template/core/manifest.json">
		<link rel="favicon" href="view/account/template/core/favicon/favicon.ico" sizes="16x16">
		<link rel="icon" href="view/account/template/core/favicon/icon-16x16.png" size="16x16">
		<link rel="icon" href="view/account/template/core/favicon/icon-32x32.png" size="32x32">
		<link rel="mask-icon" href="view/account/template/core/favicon/safari-pinned-tab.svg" color="#f9f9da">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/apple-touch-icon.png">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/apple-touch-icon.png">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_640.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" size="640x1136">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_750.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" size="750x1294">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_1125.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" size="1125x2436">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_1242.png" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" size="1242x2148">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_1536.png" media="(min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)" size="1536x2048">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_1668.png" media="(min-device-width: 834px) and (max-device-width: 834px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)" size="1668x2224">
		<link rel="apple-touch-startup-image" href="view/account/template/core/favicon/apple_splash_2048.png" media="(min-device-width: 1024px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)" size="2048x2732">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-57x57.png" size="57x57">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-72x72.png" size="72x72">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-114x114.png" size="114x114">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-144x144.png" size="144x144">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-152x152.png" size="152x152">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-167x167.png" size="167x167">
		<link rel="apple-touch-icon" href="view/account/template/core/favicon/icon-180x180.png" size="180x180">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-57x57.png" size="57x57">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-72x72.png" size="72x72">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-114x114.png" size="114x114">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-144x144.png" size="144x144">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-152x152.png" size="152x152">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-167x167.png" size="167x167">
		<link rel="apple-touch-icon-precomposed" href="view/account/template/core/favicon/icon-180x180.png" size="180x180">*}
		{*mobile Ende*}
		<title>hexcon.de</title>
		<meta name="author" content="Waldemar Preis">
		<link rel="shortcut icon" href="view/account/template/core/icon.png">

		{*<script src="//code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>*}
		<script src="//code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
		<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		{*<script src="//cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>*}
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
		<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
		<script src="view/account/template/core/framework/jquery.md5.js" crossorigin="anonymous"></script>

		<!--Table-->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.0/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js"></script>
		
		<!--file Upload Start-->
		<link rel="stylesheet" href="view/account/template/core/framework/file_upload/css/jquery.fileupload.css" media="bogus">
		<script src="view/account/template/core/framework/file_upload/js/jquery.iframe-transport.js"></script>
		<script src="view/account/template/core/framework/file_upload/js/jquery.fileupload.js"></script>
		<!--file Upload End-->

		<!--file Upload2 Start-->
		<link href="https://hayageek.github.io/jQuery-Upload-File/4.0.11/uploadfile.css" rel="stylesheet">
		<script src="view/account/template/core/framework/jquery.uploadfile.js"></script>
		<!--file Upload2 End-->

		<script src="view/account/template/core/framework/chart.min.js"></script>

		<!--jsTree-->
		{*<script src="view/account/template/core/framework/jstree/dist/jstree.js"></script>
		<link rel="stylesheet" href="view/account/template/core/framework/jstree/dist/themes/default/style.css" media="bogus">*}
		<!--<link rel="stylesheet" href="view/account/template/core/bootstrap.micro.css">-->

		{*<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" crossorigin="anonymous">*}
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
		
		
		<!--select2 start-->
		{*https://select2.org/selections*}
		{*https://github.com/select2/select2-bootstrap-theme*}
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
		{*<link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet" />*}
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
		
		
		<!--select2 end-->

		<style>{*Font Awasom 5*}
		@font-face {
			font-family: 'FontAwesome';
			src: url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot?v=4.7.0');
			src: url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff2?v=4.7.0') format('woff2'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff?v=4.7.0') format('woff'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.ttf?v=4.7.0') format('truetype'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.svg?v=4.7.0#fontawesomeregular') format('svg');
			font-weight: normal;
			font-style: normal;
		}
		</style>
		<link rel="stylesheet" href="view/account/template/core/framework/font-awesome/css/font-awesome.css">

		<link rel="stylesheet" href="view/account/template/core/main.css?v=1" media="bogus">
		<script src="view/account/template/core/wp.js?1=1"></script>
		<script src="view/account/template/core/ejs.js?1=1"></script>
	</head>
	<body oncontextmenu="return false;" onclick="wp.contextmenu.close();">
		<div id="frame_index"></div>

		<script>
		//Lade Account
		 //wp.ajax({ 'div' : 'frame_index', 'url' : '?D[PAGE]=account' });
		 {foreach from=$D.PLATFORM.D key="kPL" item="PL"}
			{if $PL.ACTIVE && !$FIND}
				wp.ajax({ 'div' : 'frame_index', 'url' : '?D[PAGE]=platform&D[PLATFORM_ID]={$kPL}' });
				{$FIND =1}
			{/if}
		{/foreach}
		 

		//Tooltip
		  $(function() {
			$( document ).tooltip({
			 content: function() {
				if(this.hasAttribute('title'))
					return  $( this ).attr( "title" );
				else
					return  $( this ).attr( "placeholder" );
			},
			items : "*[placeholder], *[title]",
			track: true
			});
		  });

		</script>

		<!--<link rel="stylesheet" href="view/account/template/core/framework/jquery-ui-1.11.0.custom/jquery-ui.css">-->
		<link rel="stylesheet" href="view/account/template/core/framework/file_upload/css/jquery.fileupload.css">
		{*<link rel="stylesheet" href="view/account/template/core/framework/jstree/dist/themes/default/style.css">*}
		<link rel="stylesheet" href="view/account/template/core/main.css">
	</body>
</html>