<script type="text/html" id="tmpl_add_platform">
	<% for ( var i = 0; i <  Object.keys(D).length; i++ ) { %>
	<% kD =  Object.keys(D)[i];%>
	<tr id='tr_<%=kD%>'>
		<td></td>
		<td><%=kD%></td>
		
		<td><input type="hidden" id="D[PLATFORM][D][<%=kD%>][ACTIVE]" name="D[PLATFORM][D][<%=kD%>][ACTIVE]" value="<%=D[kD].ACTIVE%>">
			<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][<%=kD%>][ACTIVE]').value = (this.checked)?1:0" <% if(D[kD].ACTIVE == 1) { %>checked<%}%>></td>
		<td></td>
		<td></td>
		<td><select name="D[PLATFORM][D][<%=kD%>][CLASS_ID]">
			{foreach from=$D.CLASS key="kPLA" item="PLA"}
				<option value="{$kPLA}">{$D.CLASS[$kPLA].NAME}</option>
			{/foreach}
			</select>
		</td>
		<td><%=tmpl.input({ type : 'text', name : 'D[PLATFORM][D]['+kD+'][TITLE]',	'value' : D[kD].TITLE })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[PLATFORM][D]['+kD+'][SORT]',	'value' : D[kD].SORT })%></td>
	</tr>
	<% } %>
</script>

<script>
var get_template = function(ContainerID, TemplateID, d) {
	$('#'+ContainerID).append(tmpl.fetch(TemplateID, [D = d] ));
}
</script>
<form id="formr{$D.ACCOUNT_ID}" method="post">

	<div class="alert alert-warning" role="alert">
	Parent Platform ist federführend und übersendet Artikel zur Kind Plattformen so wie Bestellungen werden alle zum Parent Plattform getragen. In der Regel sollte nur eine Parent Plattform existieren.
	</div>
			<table class="table">
				<thead>
					<tr>
						<th><button class="btn" onclick='get_template("tbodyPlatform", "tmpl_add_platform",{ [wp.get_genID()] : { "ACTIVE" : "0" }  });' type="button">+</button></th>
						<th>ID</th>
						<th>Aktive</th>
						<th>Parent</th>
						<th style="width:20px"></th>
						<th>Klasse</th>
						<th>Title</th>
						<th>Sorrt</th>
					</tr>
				</thead>
				<tbody id="tbodyPlatform">
					{foreach from=$D.PLATFORM.PARENT.D[NULL].CHILD.D key="kPLA" item="PLA"}
					<tr style="background:#ffc107">
						<td></td>
						<td>{$kPLA}</td>
						
						<td><input type="hidden" id="D[PLATFORM][D][{$kPLA}][ACTIVE]" name="D[PLATFORM][D][{$kPLA}][ACTIVE]" value="{$PLA.ACTIVE}">
							<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$kPLA}][ACTIVE]').value = (this.checked)?1:0" {if $PLA.ACTIVE}checked{/if}></td>
						<td>{if count((array)$D.PLATFORM.PARENT.D[$kPLA].CHILD.D) == 0}
							<select name="D[PLATFORM][D][{$kPLA}][PARENT_ID]">
									<option value=''>Parent</option>
									{foreach from=$D.PLATFORM.PARENT.D[NULL].CHILD.D key="kPLA3" item="PLA3"}
										{if $kPLA != $kPLA3}
									<option value='{$kPLA3}' {if $kPLA3 == $PLA.PARENT_ID}selected{/if}>{$PLA3.TITLE}</option>
										{/if}
									{/foreach}
								</select>
							{/if}
						</td>
						<td><img src="core/platform/{$PLA.CLASS_ID}/{$D.CLASS[$PLA.CLASS_ID].ICON}"></td>
						<td>{$PLA.CLASS_ID}</td>
						<td><input type="text" name="D[PLATFORM][D][{$kPLA}][TITLE]" value="{$PLA.TITLE}"></td>
						<td><input type="text" name="D[PLATFORM][D][{$kPLA}][SORT]" value="{$PLA.SORT}"></td>
					</tr>

						{foreach from=$D.PLATFORM.PARENT.D[$kPLA].CHILD.D key="kPLA2" item="PLA2"}
						<tr>
							<td>-></td>
							<td>{$kPLA2}</td>
							
							<td><input type="hidden" id="D[PLATFORM][D][{$kPLA2}][ACTIVE]" name="D[PLATFORM][D][{$kPLA2}][ACTIVE]" value="{$PLA2.ACTIVE}">
								<input type="checkbox" onclick="document.getElementById('D[PLATFORM][D][{$kPLA2}][ACTIVE]').value = (this.checked)?1:0" {if $PLA2.ACTIVE}checked{/if}></td>
							<td>
								<select name="D[PLATFORM][D][{$kPLA2}][PARENT_ID]">
									<option value=''>Parent</option>
									{foreach from=$D.PLATFORM.PARENT.D[NULL].CHILD.D key="kPLA3" item="PLA3"}
									<option value='{$kPLA3}' {if $kPLA3 == $PLA2.PARENT_ID}selected{/if}>{$PLA3.TITLE}</option>
									{/foreach}
								</select>
							</td>
							<td><img src="core/platform/{$PLA2.CLASS_ID}/{$D.CLASS[$PLA2.CLASS_ID].ICON}"></td>
							<td>{$PLA2.CLASS_ID}</td>
							<td><input type="text" name="D[PLATFORM][D][{$kPLA2}][TITLE]" value="{$PLA2.TITLE}"></td>
							<td><input type="text" name="D[PLATFORM][D][{$kPLA2}][SORT]" value="{$PLA2.SORT}"></td>
						</tr>
						{/foreach}
					{/foreach}
				</tbody>
			</table>
</form>