{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<form id="fsearch">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-4">
			<div class="input-group">
				<input class="form-control" placeholder="Suche" onkeyup="$( this ).keypress( function(e){ if(e.which == 13){ SEARCHART();return false;}} );" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|TITLE:LIKE]" value="{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.W['NUMBER:LIKE|TITLE:LIKE']}" title="Suche in:<br>Nummer<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben">
				<button class="btn" onclick="SEARCHART();" type="button"><i class="fa fa-search"></i></button>
			</div>
		</div>
	</div>
</div>
</form>
<form id="form{$D.SESSION.ACCOUNT_ID}STO" method="post">
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th>BARCODE</th>
			<th>Nummer</th>
			<th>Bild</th>
			<th>Title</th>
			<th>Gewicht (g)</th>
			<th>Lager</th>
		</tr>
		</thead>
		<tbody>
	{foreach from=$PLA.ARTICLE.PARENT.D[{NULL}].CHILD.D key="kA" item="A"}
		{$ART = $PLA.ARTICLE.D[$kA]}
		{if !$ART.SET.ARTICLE.D}
			{if !$PLA.ARTICLE.PARENT.D[$kA].CHILD.D && ($ART.ACTIVE > 0 || $ARt.STOCK > 0)}
			<tr>
				<td>{$kA}</td>
				<td><img src="barcode/{$ART.NUMBER}.C128.1.10.jpg"></td>
				<td>{$ART.NUMBER}</td>
				<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kA}_0_200x200.jpg'>" style="width:20px;height:20px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kA}_0_20x20.jpg') center no-repeat;"></div></td>
				<td>{$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}</td>
				<td><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kA}][WEIGHT]" style="max-width:50px;text-align:right;" value="{$ART.WEIGHT}"></td>
				<td>
					{foreach from=$ART.WAREHOUSE.D key="kWAR" item="WAR"}
						{foreach from=$WAR.STORAGE.D key="kSTO" item="STO"}
							{$kSTO} {$STO.STOCK}<br>
						{/foreach}
					{/foreach}
				</td>
			</tr>
			{/if}
			{foreach from=$PLA.ARTICLE.PARENT.D[$kA].CHILD.D key="kV" item="V"}
				{$VAR = $PLA.ARTICLE.D[$kV]}
				{if !$VAR.SET.ARTICLE.D && ($VAR.ACTIVE > 0 || $VAR.STOCK > 0)}
				<tr>
					<td>{$kV}</td>
					<td><img src="barcode/{$VAR.NUMBER}.C128.1.10.jpg"></td>
					<td>{$VAR.NUMBER}</td>
					<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kV}_0_200x200.jpg'>" style="width:20px;height:20px;background:url('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kV}_0_20x20.jpg') center no-repeat;"></div></td>
					<td>
					{if $ART.VARIANTE_GROUP_ID}
						{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
						{for $i=0 to count($aVAR)-1}
							<b>{$VAR.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].VALUE}</b>{if $i <count($aVAR)-1},{/if}
						{/for}
					{/if}
					{*$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE*}
						
					</td>
					<td><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kV}][WEIGHT]" style="max-width:50px;text-align:right;" value="{$VAR.WEIGHT}"></td>
					<td>
						{foreach from=$VAR.WAREHOUSE.D[$D.WAREHOUSE.W.ID ].STORAGE.D key="kSTO" item="STO"}
							{$kSTO}: {$STO.STOCK}<br>
						{/foreach}
					</td>
				</tr>
				{/if}
			{/foreach}
		{/if}
	{/foreach}
		</tbody>
	</table>

	<script>
	SEARCHART = function()
	{
		wp.ajax({
		'url' : '?D[PAGE]=warehouse.article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
		'div' : 'fPLA{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
		'data': $('#fsearch').serialize()
		});
	}

	SAVE{$D.SESSION.ACCOUNT_ID} = function()
	{
		wp.ajax({
		'url' : '?D[PAGE]=warehouse.article&D[ACTION]=set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}',
		'div' : 'ajax',
		'data': $('#form{$D.SESSION.ACCOUNT_ID}STO').serialize()
		});
	}
	</script>
	<button type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}();">Speichern</button>
</form>