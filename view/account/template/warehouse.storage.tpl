

<form id="form{$D.SESSION.ACCOUNT_ID}STORAGE" method="post">
	{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
		<table class="list">
			<thead>
				<tr>
					<td></td>
					<td>ID</td>
					<td>Aktive</td>
					<td>Title</td>
				</tr>
			</thead>
			<tbody>
				{foreach from=$WAR.STORAGE.D key="kSTO" item="STO"}
				<tr>
					<td>
						<input type="hidden" id="D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][ACTIVE]" name=D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][ACTIVE]" value="{$STO.ACTIVE}">
					</td>
					<td>{$kSTO}</td>
					<td>
						<input onclick="document.getElementById('D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][ACTIVE]').value = (this.checked)?1:0;" type="checkbox" {if $STO.ACTIVE}checked{/if}></td>
					<td><input name="D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][TITLE]" value="{$STO.TITLE}"></td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	{/foreach}
</form>

<script>
 SAVESTO{$D.SESSION.ACCOUNT_ID} = function()
			{
				
				wp.ajax({
				'url' : '?D[PAGE]=warehouse.storage&D[ACTION]=set_warehouse&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}STORAGE').serialize()
				});
			
			}
</script>