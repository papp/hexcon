	{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
	{function name="get_article"}
		<div id="tr{$D.STORAGE_ID}{$D.ARTICLE_ID}" class="card m-1" style="width: 20rem;float:left;{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SET}background:#d4eaff{elseif $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].VARIANTE_GROUP_ID}background:red{/if}">
			{*<img class="card-img-top" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg?{$FILE[0]}">*}
			<div class="card-header p-1" style="height:20px;overflow:hidden;">
				<div title="{$CWP->rand_choice_str($D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ]])}">{$CWP->rand_choice_str($D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ]])}</div>
			</div>
			<div class="card-body p-0">

					<div class="row p-0 m-0">
						<div class="col-4 p-0">
							<div style="height:100px;width:100%;background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_160x160.jpg?{$FILE[0]}) center center no-repeat;"></div>
						</div>
						<div class="col-6 p-1">
							<div title="ID: {$D.ARTICLE_ID}">{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].NUMBER}</div>

							<div>
								{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SUPPLIER.D}
									{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SUPPLIER.D  name="SUP" key="kSUP" item="SUP"}
										<span title="Herstellernummer:{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE} {$SUP.NUMBER}" style="font-size:10px;" class="badge badge-warning">{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE}: {$SUP.NUMBER}</span>
									{/foreach}
								{/if}
							</div>
							
							<div style="color:red;">
							{$PART = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$D.ARTICLE_ID].PARENT_ID}
							{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID}
								{$aVAR = explode('|',$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID)}
								{for $i=0 to count($aVAR)-1}
									{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
								{/for}
							{/if}
							</div>
						</div>
						<div class="col-2 p-1">
							{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].ATTRIBUTE.D['discontinued'].VALUE}
							<span style="font-size:10px;" class="badge badge-warning" title="Dieser Artikel wird nicht mehr nachbestellt!">Auslauf</span>
							{/if}
							{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D  name="STO" key="kSTO" item="STO"}
								<span style="font-size:10px;" class="badge {if $D.STORAGE_ID == $kSTO}badge-primary{else}badge-secondary{/if}">{$D.WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D[$kSTO].TITLE} [{$STO.STOCK}]</span><br>
							{/foreach}
						</div>
					</div>
				
			</div>
			<div class="card-footer p-1" style="height:30px;">
				<div style="float:left;">
					<input type="hidden" id="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]" name="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]" value="{$ART.ACTIVE}">
					<button class="btn btn-danger" type="button" title="Löschen" onclick="document.getElementById('D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2'; document.getElementById('tr{$D.STORAGE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
				</div>
				{if !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].SET && !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$D.ARTICLE_ID].VARIANTE_GROUP_ID}
					{*<input class="form-control" style="text-align:right;width:50px;display:unset;" name="D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]" value="{if $D.STOCK_TRANSFER>0}{$D.STOCK_TRANSFER}{else}{if $ART.STOCK}{$ART.STOCK}{else}0{/if}{/if}">*}
					
					<div style="float:left;">
						{if $D.STOCK_TRANSFER>0}{$_STOCK = $D.STOCK_TRANSFER}{elseif $ART.STOCK>0}{$_STOCK = $ART.STOCK}{else}{$_STOCK = 0}{/if}
						{input p=['name'=>"D[WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$D.STORAGE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]", 'value'=>$_STOCK, 'style'=>'text-align:right;width:50px;display:unset;']}
					</div>
					<div style="float:right;">
						<button class="btn" type="button" title="Umbuchen" onclick="new_article('{$D.WAREHOUSE_ID}',$('#WAREHOUSE_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}').val(),'{$D.ARTICLE_ID}',$('#STOCK_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}').val() )">-></button>
						<select class="form-control" id="WAREHOUSE_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}" style="width:50px;display:unset;">
							{foreach from=$D.WAREHOUSE.D[ $D.WAREHOUSE_ID ].STORAGE.D  name="STO" key="kSTO" item="STO"}
							<option value="{$kSTO}">{$STO.TITLE}</option>
							{/foreach}
						</select>
						<input class="form-control" id="STOCK_TRANSFER_{$D.WAREHOUSE_ID}{$D.STORAGE_ID}{$D.ARTICLE_ID}" style="text-align:right;width:50px;display:unset;" value="0">
					</div>
				{else}
					Dieses Artikel kann nicht gelagert werden!
				{/if}
			</div>
		</div>
	{/function}
	
	{switch $D.ACTION}
		{case 'get_article'}
			{get_article D=$D}
		{/case}
		{case 'get_storage'}
					{$ID = md5(time())}
					<div id="row{$D.WAREHOUSE.W.ID}{$ID}" class="row p-1 m-1">
						<div class="col-7 p-0">
							<div class="input-group mb-0">
 								<div class="input-group-prepend">
									<button class="btn btn-danger" type="button" title="Löschen" onclick="if(confirm('Eintrag löschen?')){ $('#t{$D.WAREHOUSE.W.ID}{$ID}active').val('-2').trigger('change');document.getElementById('row{$D.WAREHOUSE.W.ID}{$ID}').style.display = 'none';}">-</button>
								</div>
								<select class="form-control" onclick="changeStorage('t{$D.WAREHOUSE.W.ID}{$ID}',this.value)">
									<option></option>
									{foreach from=$D.WAREHOUSE.D[ $D.WAREHOUSE.W.ID ].STORAGE.D name="STO" key="kSTO" item="STO"}
										<option value="{$kSTO}">{$STO.TITLE}</option>
									{/foreach}
								</select>
							</div>
						</div>
						<div class="col-5 p-0">
							<div class="input-group">
							{input p=['type'=> 'hidden', 'id' => "t{$D.WAREHOUSE.W.ID}{$ID}active", 'name'=>"t{$D.WAREHOUSE.W.ID}{$ID}active",'value'=>$STO.ACTIVE]}
							{input p=['type'=> 'number', 'id' => "t{$D.WAREHOUSE.W.ID}{$ID}stock", 'name'=>"t{$D.WAREHOUSE.W.ID}{$ID}stock",'value'=>$STO.STOCK]}
							</div>
						</div>
					</div>
					<script>
						changeStorage = function(id,storage_id){
							if(storage_id)
							{
								document.getElementById(id+'active').setAttribute('name', 'D[WAREHOUSE][D][{$D.WAREHOUSE.W.ID}][STORAGE][D]['+storage_id+'][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]');
								document.getElementById(id+'stock').setAttribute('name', 'D[WAREHOUSE][D][{$D.WAREHOUSE.W.ID}][STORAGE][D]['+storage_id+'][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]');
							}
							else
							{
								document.getElementById(id+'active').setAttribute('name', 't{$D.WAREHOUSE.W.ID}{$ID}active');
							document.getElementById(id+'stock').setAttribute('name', 't{$D.WAREHOUSE.W.ID}{$ID}stock');
							}
						}
					</script>
		{/case}
		{default}
			
			<form id="form{$D.SESSION.ACCOUNT_ID}STO" method="post">
				<div style="background:#ddd;position:fixed;width:100%;z-index:1">
					<button class="btn btn-primary" type="button" id="btnSave" onclick="SAVE{$D.SESSION.ACCOUNT_ID}();">Speichern</button>
					
					<button class="btn btn-primary" onclick="$('#funcDDL').toggle();" type="button">Funktionen</button>
					<div id="funcDDL" style="display:none;width:100%;position:absolute;background:#fff;padding:5px;border:solid 1px #ddd;overflow-y:scroll;">
					{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
						{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
						<button class="btn btn-primary" type="button" onclick="IDs = getArticleIds();url = (IDs)?'&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID:IN]='+IDs:''; window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W.ID}'+url,'_blank');" title="Lager">
						{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
						</button>
						{/if}
					{/foreach}
					</div>
					<script>
					getArticleIds = function()
					{
						var IDs = '';
						$("input:checkbox[group='ARTICLE_ID']").each(function(index, element){
						
							if( $(this).is(':checked') )
								IDs = IDs+((IDs)?',':'')+'\''+$(this).val()+'\'';
						});
						return IDs;
					}
					</script>
				</div>
				<div style="height:30px;"></div>
						<script>
						function myFunction() {
							// Declare variables 
							var input, filter, table, tr, td, i;
							filter2 = document.getElementById("supplier").value.toUpperCase();
							filter = document.getElementById("myInput").value.toUpperCase();
							table = document.getElementById("myTable");
							tr = table.getElementsByTagName("tr");

							// Loop through all table rows, and hide those who don't match the search query
							for (i = 0; i < tr.length; i++) {
								td = tr[i].getElementsByTagName("td")[1];
								td1 = tr[i].getElementsByTagName("td")[3];
								td2 = tr[i].getElementsByTagName("td")[5];
								td3 = tr[i].getElementsByTagName("td")[7];
								if (td || td1 || td2 || td3) {
									if (td2.innerHTML.toUpperCase().indexOf(filter2) > -1 && (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 )) {
										tr[i].style.display = "";
									} else {
										tr[i].style.display = "none";
									}
								}
							}
						}
						</script>
							<div class="input-group mb-3">
								<select  class="form-control" style="font-size:20px; max-width:20%;" onchange="myFunction()" id="supplier">
										<option value="">Lieferanten</option>
									{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D  name="SUP" key="kSUP" item="SUP"}
										<option value="{$SUP.TITLE}">{$SUP.TITLE}</option>
									{/foreach}
								</select>
								<input type="text" id="myInput" onkeyup="myFunction()" style="font-size:20px" class="form-control p-1 form-control-lg" placeholder="Suche" autocomplete="none">
							</div>
							<div style="height:600px;overflow-y:scroll;">
								<table id='myTable' class="table">
									<thead>
										<tr>
											<th style="width:40px;">Bild</th>
											<th style="width:100px;">Nummer</th>
											<th style="width:100px;">Funktion</th>
											<th style="width:30%;">Artikel</th>
											<th style="width:100px">Status</th>
											<th>Hersteller</th>
											<th style="width:50px;">Gewicht</th>
											<th style="width:200px;">Bestand</th>
										</tr>
									</thead>{*Erweitert, so dass wen Parent.Active = 0 ist, dass dennoch im Lager angezeigt wird wenn der Artikel nicht als Auslauf Artikel gekenzeichnet ist.*}
								{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
									{if 	(
												!$ART.VARIANTE_GROUP_ID
												&& ($ART.ACTIVE || $PLA.ARTICLE.D[ $ART.PARENT_ID ].ACTIVE)
												&& ($PLA.ARTICLE.D[ $ART.PARENT_ID ].ACTIVE || (!$ART.SET && !$ART.ATTRIBUTE.D['discontinued'].VALUE && !$PLA.ARTICLE.D[ $ART.PARENT_ID ].ATTRIBUTE.D['discontinued'].VALUE) )
												&& (!$ART.SET && !$PLA.ARTICLE.D[ $ART.PARENT_ID ].SET)
												&& (!$ART.ATTRIBUTE.D['discontinued'].VALUE && !$PLA.ARTICLE.D[ $ART.PARENT_ID ].ATTRIBUTE.D['discontinued'].VALUE)
												&& !$ART.WAREHOUSE.D
											)
											|| in_array($D.WAREHOUSE.W.ID, array_keys((array)$ART.WAREHOUSE.D))
									}
										<tr>
											<td><div title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{if $ART.FILE.D || !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{$kART}{else}{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{/if}_0_400x400.jpg'>" style="height:40px;width:40px;background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{if $ART.FILE.D || !$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{$kART}{else}{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{/if}_0_40x40.jpg) center center no-repeat;"></div></td>
											<td>
												<button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{else}{$kART}{/if}', 'TITLE' : '{$ART.NUMBER} | {$TITLE|replace:'\'':'\\\''|replace:'"':''|truncate:80:"...":false}' , 'WIDTH' : '1000px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}{else}{$kART}{/if}'});">{$ART.NUMBER}</button>
											</td>
											<td>
												<input group="ARTICLE_ID" value="{$kART}" type="checkbox">
												{*
												<button class="btn btn-primary btn-sm btn-block mb-0" type="button" onclick="window.open('https://{$D.SESSION.ACCOUNT_ID}.hexcon.de/file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/inv5_abc.pdf?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[WAREHOUSE][W][ID]={$D.WAREHOUSE.W.ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]={$kART}');">Label</button>
												*}
											</td>
											<td>
												{$PART = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.CHILD.D[$kART].PARENT_ID}
												<div>
													{if $PART}
														{$TITLE = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[ $PART ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}
													{else}
														{$TITLE = $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kART].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}
													{/if}
													{$CWP->rand_choice_str($TITLE,['DELIMITER' => [ 'LEFT' => '[(', 'RIGHT' => ')]' ]])}
												</div>
												<div style="color:red;">
												
												{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID}
													{$aVAR = explode('|',$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$PART].VARIANTE_GROUP_ID)}
													{for $i=0 to count($aVAR)-1}
														{$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
													{/for}
												{/if}
												</div>
									
												{if $ART.SET}
													<div class="p-2 m-1 alert-danger">
													Das ist ein Set Artikel und kann nicht gelagert werden.
													Bitte alle Lagerorte entfernen!
													</div>
												{/if}
												{if $ART.VARIANTE_GROUP_ID}
													<div class="p-2 m-1 alert-danger">
													Das ist ein Vater Artikel und kann nicht gelagert werden.
													Bitte alle Lagerorte entfernen!
													</div>
												{/if}
												{if !$ART.WAREHOUSE.D}
													<div class="p-2 m-1 alert-danger">
													Dieses Artikel hat kein Lagerort. Bitte Lagerort hinzufügen.
													</div>
												{/if}
												{if $ART.WEIGHT < 1}
													<div class="p-2 m-1 alert-warning">
													Gewicht wurde bei diesem Artikel nicht angegeben.
													</div>
												{/if}
												{if $ART.STOCK < 1 && ($ART.ATTRIBUTE.D['discontinued'].VALUE || $PLA.ARTICLE.D[ $ART.PARENT_ID ].ATTRIBUTE.D['discontinued'].VALUE)}
													<div class="p-2 m-1 alert-warning">
													Dieses Artikel ist ein Auslauf Artikel und hat keinen Bestand mehr. Bitte Lagerort freigeben.
													</div>
												{/if}
											</td>
											<td>
												{if $ART.ATTRIBUTE.D['discontinued'].VALUE || $PLA.ARTICLE.D[ $ART.PARENT_ID ].ATTRIBUTE.D['discontinued'].VALUE}
												<div class="p-2 m-1 alert-warning" title="Dieser Artikel wird nicht mehr nachbestellt!">
													Auslauf
												</div>
												{/if}
											</td>
											<td>
												<div>
													{if $D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kART].SUPPLIER.D}
														{foreach from=$D.PLATFORM.D[$D.PLATFORM_ID].ARTICLE.D[$kART].SUPPLIER.D  name="SUP" key="kSUP" item="SUP"}
															<div>{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE}: {$SUP.NUMBER} | <i style="color:#999">{$SUP.COMMENT}</i></div>
														{/foreach}
													{/if}
												</div>
											</td>
											<td>
												{if !$ART.SET}
													{input p=['type'=> 'number', 'required' => 1, 'pattern' => '[0-9]+', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][D][{$kART}][WEIGHT]", 'value'=>$ART.WEIGHT]}
												{/if}
											</td>
											<td>
												<div class="row p-0 m-0">
													<div class="col-2 p-0">
														<button class="btn btn-success" onclick="wp.ajax({
														'url'	:	'?D[PAGE]=warehouse.stocktaking2',
														'data'	:	{
																		'D[ACTION]' : 'get_storage',
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		'D[WAREHOUSE][W][ID]' : '{$D.WAREHOUSE.W.ID}',
																		'D[ARTICLE_ID]' : '{$kART}',
																		'D[WAREHOUSE][W][ACTIVE]' : 1
																	},
														'div'	:	's{$D.PLATFORM_ID}{$D.WAREHOUSE.W.ID}{$kART}',
														'INSERT' : 'prepend',
														'ASYNC'	: 1
														})" type="button" title="Lagerort hinzufügen.">+</button>
													</div>
												
													<div class="col-10 p-0">
														<div class="alert-success" id="s{$D.PLATFORM_ID}{$D.WAREHOUSE.W.ID}{$kART}">
														{foreach from=$ART.WAREHOUSE.D key="kWAR" item="WAR"}
															{foreach from=$WAR.STORAGE.D name="STO" key="kSTO" item="STO"}
																<div id="row{$kWAR}{$kSTO}{$kART}" class="row p-1 m-1">
																	<div class="col-7 p-0">
																		{if $D.WAREHOUSE.W.ID == $kWAR}
																			<button class="btn btn-danger" type="button" title="Löschen" onclick="if(confirm('Eintrag löschen?')){ $('#t{$kWAR}{$kSTO}{$kART}active').val('-2').trigger('change');document.getElementById('row{$kWAR}{$kSTO}{$kART}').style.display = 'none';}">-</button>
																			{$D.WAREHOUSE.D[ $D.WAREHOUSE.W.ID ].STORAGE.D[$kSTO].TITLE}
																		{else}{*fremdlager*}
																			Fremdlager{*$D.WAREHOUSE.D[$kWAR].TITLE*}
																		{/if}
																	</div>
																	<div class="col-5 p-0">
																		<div class="input-group">
																		{if $D.WAREHOUSE.W.ID == $kWAR}
																			{input p=['type'=> 'hidden', 'id' => "t{$kWAR}{$kSTO}{$kART}active", 'name'=>"D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][ACTIVE]",'value'=>$STO.ACTIVE]}
																			{input p=['type'=> 'number', 'name'=>"D[WAREHOUSE][D][{$kWAR}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]",'value'=>$STO.STOCK]}
																		{else}
																			{$STO.STOCK}
																		{/if}
																		</div>
																	</div>
																</div>
															{/foreach}
														{/foreach}
														</div>
													</div>

												</div>
											</td>
										</tr>
										{$Artikel_count = $Artikel_count+1}
									{/if}
								{/foreach}
								</table>
							</div>
							<div></div>
				
					<div>Anzahl Artikel: {$Artikel_count}</div>


				<script>
					get_article = function(WID,SID,AID,STOCK)
					{
						if(!AID)
							AID = window.prompt("Bitte hier Artikel ID eingeben","");
					
						if(AID)
						{
							wp.ajax({ 
							'div' : 'list_'+WID+SID, 
							INSERT : 'append', 
							'url' : '?D[PAGE]=warehouse.stocktaking2&D[ACTION]=get_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]='+AID,
							});
						}
					}
					btnBg = $('#btnSave').css('background-color');
					SAVE{$D.SESSION.ACCOUNT_ID} = function()
					{
						$('#btnSave').css('background-color','red');
						$('#form{$D.SESSION.ACCOUNT_ID}STO').find(':input').each(function(){
							if( $(this).attr( "ischanged" ) == 0 )
								$(this).attr( "disabled", true );
						});

						wp.ajax({
						'url' : '?D[PAGE]=warehouse.stocktaking2&D[ACTION]=set_warehouse&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
						'div' : 'ajax',
						'data': $('#form{$D.SESSION.ACCOUNT_ID}STO').serialize(),
						'done': function(){ $('#btnSave').css('background-color',btnBg);}
						});

						$('#form{$D.SESSION.ACCOUNT_ID}STO').find(':input').each(function(){
							$(this).attr( "disabled", false);
						});
					}
				</script>
				
				
			</form>
{/switch}