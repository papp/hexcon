{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{function name="get_attribute"}

			{foreach from=$PLA.ATTRIBUTE.D key="kATT" item="ATT"}
				<tr id="tr{$D.PLATFORM_ID}{$kATT}">
					<td>
						<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]" value="{if $ATT.ACTIVE}1{else}0{/if}">
						<button class="btn btn-danger" onclick="if(confirm('Wirklich löschen?')){ document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]').value = '-2';document.getElementById('tr{$D.PLATFORM_ID}{$kATT}').style.display ='none';}" type="button" class="del">&#xf056;</button>
					</td>
					<td>{$kATT}
						<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][SORT]" value="{$ATT.SORT}">
					</td>
					<td><input onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][ACTIVE]').value = (this.checked)?1:0;" type="checkbox" {if $ATT.ACTIVE}checked{/if}></td>
					<td><select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][TYPE]">
						<option value="">Text</option>
						<option {if $ATT.TYPE == 'textarea'}selected{/if} value="textarea">Textarea</option>
						<option {if $ATT.TYPE == 'select'}selected{/if} value="select">Drop Down List</option>
						<option {if $ATT.TYPE == 'multiselect'}selected{/if} value="multiselect">Multi Drop Down List</option>
						<option {if $ATT.TYPE == 'checkbox'}selected{/if} value="checkbox">checkbox</option>
						<option {if $ATT.TYPE == 'wysiwyg'}selected{/if} value="wysiwyg">wysiwyg</option>
					</select></th>
					<td><select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][TOPIN]">
						<option value="">-</option>
						<option {if $ATT.TOPIN == 'parent'}selected{/if} value="parent">Parent</option>
						<option {if $ATT.TOPIN == 'child'}selected{/if} value="child">Child</option>
					</select></td>
					<td><input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][SORT]" type="text" value="{$ATT.SORT}"></td>
					<td title="Mulit Language"><input name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][I18N]" type="checkbox" value="1" {if $ATT.I18N}checked{/if}></td>
					
					{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
						{if $LAN.ACTIVE}
							<td>
								<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][{$kLAN}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][{$kLAN}][ACTIVE]" value="{if $ATT.LANGUAGE.D[$kLAN].TITLE}1{else}-2{/if}">
								<div class="input-group">
									<input class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][{$kLAN}][TITLE]" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][LANGUAGE][D][{$kLAN}][ACTIVE]').value = (this.value)?1:-2;" value="{$ATT.LANGUAGE.D[$kLAN].TITLE}">
									<span class="input-group-btn">
										<button class="btn btn-default" onclick="$('.OPT{$D.PLATFORM_ID}{$kATT}').toggle();" type="button"><i class="fa fa-caret-down"></i></button>
									</span>
								</div>
								<div class="OPT{$D.PLATFORM_ID}{$kATT}" style="display:none;">
								{if $kATT != 'HSNTSN' && ( $ATT.I18N || (!$ATT.I18N && $kLAN == 'DE'))}
									<textarea class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}]{if $ATT.I18N}[LANGUAGE][D][{$kLAN}]{/if}[VALUE]" style="max-height:100px;">{if $ATT.I18N}{$ATT.LANGUAGE.D[$kLAN].VALUE}{else}{$ATT.VALUE}{/if}</textarea>
								{/if}
								</div>
							</td>
						{/if}
					{/foreach}
					<td><i class="fa fa-arrow-right"></i></td>
					{foreach from=$PLA.TO.PLATFORM.D key="kFROM_PLA" item="FROM_PLA"}
					
						{if $D.PLATFORM.D[$kFROM_PLA].ACTIVE && $D.PLATFORM.D[$kFROM_PLA].ATTRIBUTE.D}
						<td>
							<input id="att2att{$D.PLATFORM_ID}{$kATT}{$kFROM_PLA}" type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][TO][PLATFORM][D][{$kFROM_PLA}][ACTIVE]" value="{if $ATT.TO.PLATFORM.D[$kFROM_PLA].ACTIVE}1{else}-2{/if}">
							<select class="form-control" onchange="$('#att2att{$D.PLATFORM_ID}{$kATT}{$kFROM_PLA}').val(this.value?1:-2)" name="D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D][{$kATT}][TO][PLATFORM][D][{$kFROM_PLA}][ATTRIBUTE_ID]">
								<option value=""></option>
							
								{foreach from=$D.PLATFORM.D[$kFROM_PLA].ATTRIBUTE.D[$kATTT].ATTRIBUTE.D key="kATT2" item="ATT2"}
									<option value="{$kATT2}" {if $kATT2 == $ATT.TO.PLATFORM.D[$kFROM_PLA].ATTRIBUTE_ID}selected{/if}>{if $ATT2.I18N}&#xf1ab;{/if} {$ATT2.LANGUAGE.D['DE'].TITLE}</option>
								{/foreach}
							
							</select>
						</td>
						{/if}
					
					{/foreach}
				</tr>
			{/foreach}
	
	
{/function}

{switch $D.ACTION}
	{case 'add_attribute'}
	{case 'get_attribute'}
		{foreach from=$PLA.ATTRIBUTE.D key="kATTT" item="ATTT"}
		{get_attribute D=$D}
		{/foreach}
		{/case}
	{default}
<form id="form{$D.PLATFORM_ID}ATT" method="post">

		<table style="width:100%;height:100%;">
			<tr>
				<td>
					{*ATTRIBUTE START*}
						{*foreach from=$PLA.ATTRIBUTE.D key="kATTT" item="ATTT"*}
						{$kATTT}
						<table class="table">
							<thead>
								<tr>
									<th><button class="btn btn-success" onclick="ID = wp.get_genID(); wp.ajax({ div: 'list{$D.PLATFORM_ID}{$kATTT}_attribute',url:'?D[PAGE]=platform.attribute&D[ACTION]=add_attribute&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ATTRIBUTE][D]['+ID+'][ACTIVE]=1', INSERT:'prepend'});" type="button">&#xf055;</button></th>
									<th style="width:50px;">ID</th>
									<th title="Aktive"><i class="fa fa-toggle-on"></i></th>
									<th>Type</th>
									<th>Anhängen</th>
									<th style="width:50px;">Sort</th>
									<th title="Mulit Language Attribute"><i class="fa fa-language"></i></th>
									{foreach from=$PLA.LANGUAGE.D key="kLAN" item="LAN"}
										{if $LAN.ACTIVE}
											<th>{$kLAN} Title / Value</th>
										{/if}
									{/foreach}
									<th title="Mappen mit Platform Attribute"><i class="fa fa-map-signs"></i></th>
									{foreach from=$PLA.TO.PLATFORM.D key="kFROM_PLA" item="FROM_PLA"}
										{if $D.PLATFORM.D[$kFROM_PLA].ACTIVE && $D.PLATFORM.D[$kFROM_PLA].ATTRIBUTE.D}
											<th>{$D.PLATFORM.D[$kFROM_PLA].TITLE}</th>
										{/if}
									{/foreach}
								</tr>
							</thead>
							<tbody id="list{$D.PLATFORM_ID}{$kATTT}_attribute">
								{*$D['ACTION'] = 'get_attribute'*}
								{get_attribute D=$D}
							</tbody>
						</table>
						{*/foreach*}
					{*ATTRIBUTE END*}
				</td>
			</tr>
		</table>

</form>
<script>
SAVEATT{$D.PLATFORM_ID} = function()
{
	wp.ajax({
	'url' : '?D[PAGE]=platform.attribute&D[ACTION]=set_attribute&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
	'div' : 'ajax',
	'data': $('#form{$D.PLATFORM_ID}ATT').serialize()
	});
}
</script>
{/switch}