<script type="text/html" id="tmpl_add_user">
	<% for ( var i = 0; i <  Object.keys(D).length; i++ ) { %>
	<% kD =  Object.keys(D)[i];%>
	<tr id='tr_<%=kD%>'>
		<td><button class="btn btn-danger" onclick="document.getElementById('D[USER][D][<%=kD%>][ACTIVE]').value = '-2'; $('#tr_<%=kD%>').hide();" type="button">-</button></td>
		<td><%=kD%></td>
		<td><input name="D[USER][D][<%=kD%>][ACTIVE]" id="D[USER][D][<%=kD%>][ACTIVE]" type="hidden" value="<%=D[kD].ACTIVE %>">
			<input onclick="document.getElementById('D[USER][D][<%=kD%>][ACTIVE]').value = (this.checked)?1:0;" type="checkbox" <% if(D[kD].ACTIVE == 1) { %>checked<%}%> >
		</td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][NICKNAME]',	'value' : D[kD].NICKNAME })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][NAME]',		'value' : D[kD].NAME })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][FNAME]',		'value' : D[kD].FNAME })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][EMAIL]',		'value' : D[kD].EMAIL })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][PASSWORD]' })%></td>
		<td><%=tmpl.input({ type : 'text', name : 'D[USER][D]['+kD+'][LOGIN_FAIL]',	'value' : D[kD].LOGIN_FAIL })%></td>
	</tr>
	<% } %>
</script>

<script>
var get_template = function(ContainerID, TemplateID, d) {
	$('#'+ContainerID).append(tmpl.fetch(TemplateID, [D = d] ));
}
</script>

<form id="form{$D.ACCOUNT_ID}" method="post">
	<table class="table">
		<thead>
			<tr>
				<td><button class="btn" onclick='get_template("tbodyUser", "tmpl_add_user",{ [wp.get_genID()] : { "ACTIVE" : "0" }  });' type="button">+</button></td>
				<td>ID</td>
				<td>Aktive</td>
				<td>NickName</td>
				<td>VorName</td>
				<td>Name</td>
				<td>E-Mail</td>
				<td>Password</td>
				<td>login Fail</td>
			</tr>
		</thead>
		<tbody id="tbodyUser">
			<script>
			/*
				$.ajax({ url : "?D[PAGE]=account.user&D[ACTION]=get_user", dataType: "json", success: function(data) {
						get_template("tbodyUser","tmpl_add_user",data);
					}});*/
					{foreach from=$D.USER.D key="kUSE" item="USE"}
						get_template("tbodyUser","tmpl_add_user",{
							{$kUSE} : { 
								ACTIVE		: '{$USE.ACTIVE}',
								NICKNAME	: '{$USE.NICKNAME}',
								FNAME		: '{$USE.FNAME}',
								NAME		: '{$USE.NAME}',
								EMAIL		: '{$USE.EMAIL}',
								LOGIN_FAIL	: '{$USE.LOGIN_FAIL}',
							 }
						});
					{/foreach}
			</script>
		</tbody>
	</table>
</form>
