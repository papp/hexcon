{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'load_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}


			{if $kART == $PLA.ARTICLE.W.ID}{*PARENT*}
				<table style="font-size:15px;">
					<tr>
						<td>Anzahl</td>
						<td>Bild</td>
						<td>Num</td>
						<td>Title</td>
					</tr>
					<tbody id="SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}"></tbody>
				</table>
				{foreach from=$ART.SET.ARTICLE.D key="kSET" item="SET"}
			<script>
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
				'div' : 'SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}',
				'INSERT' : 'append',
				'data'	: {
					'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
					'D[SET][QUANTITY]' : '{$SET.QUANTITY}'
				}
				});
			</script>
				{/foreach}
			{else}{*VARIANTE*}
				{foreach from=$ART.VARIANTE.D key="kVAR" item="VAR"}
					{if $kVAR == $PLA.ARTICLE.W.ID}
						<table style="font-size:15px;">
							<tr>
								<td>Anzahl</td>
								<td>Bild</td>
								<td>Num</td>
								<td>Title</td>
							</tr>
							<tbody id="SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}"></tbody>
						</table>
						{foreach from=$VAR.SET.ARTICLE.D key="kSET" item="SET"}
							<script>
								wp.ajax({
								'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[ARTICLE_ID]={$kART}',
								'div' : 'SET_LIST{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}',
								'INSERT' : 'append',
								'data'	: {
									'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
									'D[SET][QUANTITY]' : '{$SET.QUANTITY}'
								}
								});
							</script>
						{/foreach}
					{/if}
				{/foreach}
			{/if}
		{/foreach}
	{/case}
	{case 'load_set_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			{if $kART == $PLA.ARTICLE.W.ID}{*PARENT*}
				<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
					<td style="font-size:20px;text-align:right;">{$D.SET.QUANTITY}</td>
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_25x25.jpg"></td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
					<td>{$PLA.ARTICLE.D[$PLA.ARTICLE.W.ID].LANGUAGE.D.DE.DESCRIPTION.TITLE}</td>
				</tr>
				{else}{*VARIANTE*}
					<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
						<td style="font-size:20px;text-align:right;">{$D.SET.QUANTITY}</td>
						<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$PLA.ARTICLE.W.ID}_0_25x25.jpg"></td>
						<td>{$PLA.ARTICLE.D[$kART].VARIANTE.D[$PLA.ARTICLE.W.ID].NUMBER}</td>
						<td>{$PLA.ARTICLE.D[$kART].LANGUAGE.D.DE.DESCRIPTION.TITLE}</td>
					</tr>
				{/if}
		{/foreach}
	{/case}
	{default}
	<script>
	SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID} = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=set_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize()
				});
			
			}
	</script>
	
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
		<input type="hidden" name="D['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
		<input type="hidden" name="D['PLATFORM_ID']" value="{$D.PLATFORM_ID}">
		<button type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>

		Bestellungen: {$PLA.INVOICE.D|@count}

		{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
			{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
			<button type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]=20&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][O][NUMBER]=ASC&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]={$D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.W.WAREHOUSE_ID}','_blank');" title="Export Inv:{$INV.NUMBER}">
			{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
			</button>
			{/if}
		{/foreach}

		<table class='list' style='width:100%;'>
			<thead>
				<tr>
					<td style="width:30px;"></td>
					<td style="width:100px;">ID</td>
					<td style="width:60px;">Nummer</td>
					<td style="width:60px;">Datum</td>
					<td style="width:60px;">Status</td>
					<td style="width:60px;">Nickname</td>
					<td>Rechnungsadresse</td>
					<td>Lieferadresse</td>
					<td style="width:150px;">Versand</td>
					<td style="width:150px;">Kommentar</td>
					<td style="width:200px;"></td>
				</tr>
			</thead>
			<tbody>
				{foreach from=$PLA.INVOICE.D key="kINV" item="INV"}
					
					<tr >
						<td></td>
						<td>{$kINV}
							{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
								{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'order' && $SET.VARIANTE.D['ACTIVE'].VALUE}
								<button type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kINV}','_blank');" title="Export Inv:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
								{/if}
							{/foreach}
						</td>
						<td style="text-align:right;">{$INV.NUMBER}</td>
						<td style="text-align:center;">{$INV.DATE|date_format:"%d.%m.%Y"}</td>
						<td style="text-align:ceter;width:100px;background:{if $INV.STATUS >=40}green{else if $INV.STATUS >=20}yellow{else}red{/if}">
							<input type="radio" value='0' {if $INV.STATUS ==0}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">offen<br>
							<input type="radio" value='20' {if $INV.STATUS ==20}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">Versandfreigabe<br>
							<input type="radio" value='40' {if $INV.STATUS ==40}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">Fertig<br>
							{*
							<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">
								<option value='0' {if $INV.STATUS ==0}selected{/if}>offen</option>
								<option value='20' {if $INV.STATUS ==20}selected{/if}>Versandfreigabe</option>
								<option value='40' {if $INV.STATUS ==40}selected{/if}>Fertig</option>
								</select>*}
								</td>
						<td style="text-align:center;">{$INV.CUSTOMER_NICKNAME}<br>({$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE})</td>
						<td style="background:#fff;font-size:13px;">
							{$INV.BILLING.COMPANY}<br>
							{$INV.BILLING.FNAME} {$INV.BILLING.NAME}<br>
							{$INV.BILLING.STREET} {$INV.BILLING.STREET_NO}<br>
							{if $INV.BILLING.ADDITION}{$INV.BILLING.ADDITION}<br>{/if}
							{$INV.BILLING.ZIP} {$INV.BILLING.CITY}<br>
							{$D.COUNTRY.D[$INV.BILLING.COUNTRY_ID].TITLE}
						</td>
						<td style="background:#fff;font-size:13px;">{$INV.DELIVERY.COMPANY}<br>
							{$INV.DELIVERY.FNAME} {$INV.DELIVERY.NAME}<br>
							{$INV.DELIVERY.STREET} {$INV.DELIVERY.STREET_NO}<br>
							{if $INV.DELIVERY.ADDITION}{$INV.DELIVERY.ADDITION}<br>{/if}
							{$INV.DELIVERY.ZIP} {$INV.DELIVERY.CITY}<br>
							{$D.COUNTRY.D[$INV.DELIVERY.COUNTRY_ID].TITLE}
							</td>
						<td style="text-align:right;">
							</select>
							Traking:<input type='text' style="{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}background:yellow;{/if}" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRACKING_NO]" value='{$INV.TRACKING_NO}'>{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}<-mit Tracking !!!{/if}<br>
							Datum:<input type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPED_DATE]" value='{$smarty.now|date_format:"%Y-%m-%d"}'><br>
							Versandfirma:<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPING_ID]">
								<option value=""></option>
								{foreach from=$PLA.SHIPPING.D key="kSHI" item="SHI"}
									<option value='{$kSHI}' {if $INV.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
								{/foreach}
							</select>
						
						</td>
						<td style="text-align:right;">
							
								
						<textarea name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]' style='width:200px;height:100px;'>{$INV.COMMENT}</textarea>
						
						</td>
						<td rowspan="2" valign="top">
						{*MESSAGE START ================*}
							{if $INV.CUSTOMER_ID}
							<div style="height:180px;"  id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}"></div>
								<script>
									wp.ajax({
														'url'	:	'?D[PAGE]=message',
														'data'	:	{
																		'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		'D[MESSAGE][W][GROUP_ID]' : '{$INV.CUSTOMER_ID}', //group_id,
																		'D[MESSAGE][O][DATETIME]' : 'DESC',
																		'D[MESSAGE][W][INTERNALLY]' : '1'
																	},
														'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}'
														});
								</script>
							{/if}
						{*MESSAGE END ================*}
						</td>
					</tr>
					<tr>
						<td colspan="10">
							<table style="width:100%;">
								<tr>
									<td style="width:50px;">ANZ</td>
									<td style="width:50px;">Bild</td>
									<td style="width:50px;">NR</td>
									<td>Title</td>
									<td>Lagerort</td>
									<td>SET</td>
								</tr>
							{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
								<tr>
									<td style="text-align:center;font-size:25px;">{$ART.STOCK}</td>
									<td title="<nobr>
										<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
										<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
										<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
									</nobr>">
										<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_50x50.jpg">
									</td>
									<td style="font-size:15px;">{$ART.NUMBER}</td>
									<td style="font-size:15px;">{$ART.TITLE}<br>
										Lagerort: 
										{foreach from=$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
											{if $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
												<b>{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</b> ({$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}) |
											{/if}
										{/foreach}
									</td>
									<td>
										<input type="hidden" value="{$ART.STOCK}" id="SOL_{$kART}">
										
										{$_SOL = $ART.STOCK}
										{*foreach from=$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
											{if $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
												{$_IST = $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}
												<input type="hidden" value="{$_IST}" id="IST_{$kART}_{$kSTO}">
												{if $_IST >= $_SOL}
													{$_IST = $_IST - $_SOL}
													{$SOL = $_SOL}
													{$_SOL = 0}
												{else}
													{$SOL = $_IST}
													{$_SOL = $_SOL - $_IST}
													{$_IST=0}
												{/if}
											<div>
												<input type="hidden" name="D[WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" value="{$_IST}">
												<input style="width:30px;text-align:right;" value="{$SOL}">/{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK} {$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}
											</div>
											{/if}
										{/foreach*}
									</td>
									<td id="SET{$D.PLATFORM_ID}{$kINV}{$kART}">
										

									</td>
								</tr>
								
								{if $PLA.ARTICLE.D[$kART].SET.ARTICLE.D|@count}
								<script>
								wp.ajax({
								'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[INVOICE_ID]={$kINV}&D[ARTICLE_ID]={$kART}',
								'div' : 'SET{$D.PLATFORM_ID}{$kINV}{$kART}',
								'data'	: {
									'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kART}',
									'D[WAREHOUSE_ID]'	: '{$PLA.INVOICE.W.WAREHOUSE_ID}'

											}
								});
								</script>
								{/if}
							{/foreach}

							</table>
						</td>
					</tr>
					<tr>
						<td colspan="11" style="border-top:5px red solid;">&nbsp;</td>
					</tr>
				{/foreach}
					
			</tbody>
		</table>
		<button type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>
	</form>
{/switch}
