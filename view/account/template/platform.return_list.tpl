{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<div style="max-height:300px;overflow-y:scroll;">
	<!--INVOICE-->
	<table class='list' style='width:100%;'>
			<thead>
				<tr>
					<td style="width:30px;"></td>
					<td style="width:100px;">ID</td>
					<td style="width:60px;">Platform</td>
					<td style="width:60px;">Datum</td>
					<td>Name</td>
					<td style="width:100px;">Nickname</td>
					<td style="width:60px;">Gewicht</td>
					<td style="width:60px;">Netto</td>
					<td style="width:60px;">MwSt</td>
					<td style="width:60px;">Brutto</td>
				</tr>
			</thead>
			<tbody>
				
					{foreach from=$PLA.INVOICE.D key="kINV" item="INV"}
						<tr style="{if $PLA.RETURN.D[$kINV]}color:#777;{/if}">
							<td>
								<form id="frm{$D.PLATFORM_ID}{$kINV}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][PLATFORM_ID]" value="{$INV.PLATFORM_ID}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][PAYMENT_ID]" value="{$INV.PAYMENT_ID}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_ID]" value="{$INV.CUSTOMER_ID}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_NICKNAME]" value="{$INV.CUSTOMER_NICKNAME}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CUSTOMER_EMAIL]" value="{$INV.CUSTOMER_EMAIL}">

									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][NAME]" value="{$INV.BILLING.NAME}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][FNAME]" value="{$INV.BILLING.FNAME}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][STREET]" value="{$INV.BILLING.STREET}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][STREET_NO]" value="{$INV.BILLING.STREET_NO}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ZIP]" value="{$INV.BILLING.ZIP}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][CITY]" value="{$INV.BILLING.CITY}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][COUNTRY_ID]" value="{$INV.BILLING.COUNTRY_ID}">
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ADDITION]" value="{$INV.BILLING.ADDITION}">
								
									<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][INVOICE_ID]" value="{$kINV}">
									
									<textarea style="display:none;" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][COMMENT]">{$INV.COMMENT}</textarea>
									{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
										<img title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>" width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg">
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][NUMBER]" value="{if $ART.PARENT_ID}{$PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].NUMBER}{else}{$PLA.ARTICLE.D[$kART].NUMBER}{/if}">
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][TITLE]" value="{$ART.TITLE}">
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][PRICE]" value="-{$ART.PRICE}">
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][VAT]" value="{$ART.VAT}">
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][STOCK]" value="{$ART.STOCK}">
										
										<!--<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][WEIGHT]" value="{$ART.WEIGHT}">-->
										<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D][{$kINV}][ARTICLE][D][{$kART}][WEIGHT]" value="{if $ART.PARENT_ID}{$PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].WEIGHT}{else}{$PLA.ARTICLE.D[$kART].WEIGHT}{/if}">
										
										{if $ART.PARENT_ID}
											{$INVOICE_WEIGHT[$kINV] = $INVOICE_WEIGHT[$kINV] + $PLA.ARTICLE.D[$ART.PARENT_ID].VARIANTE.D[$kART].WEIGHT}
										{else}
											{$INVOICE_WEIGHT[$kINV] = $INVOICE_WEIGHT[$kINV] + $PLA.ARTICLE.D[$kART].WEIGHT}
										{/if}
									
									{/foreach}
								
								</form>
							</td>
							<td><button type="button" onclick="wp.window.open({ 'ID' : 'R{$kINV}', 'TITLE' : '{$INV.NUMBER}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kINV}', 'DATA' : $('#frm{$D.PLATFORM_ID}{$kINV}').serialize()});">RN{$INV.NUMBER}</button></td>
							<td style="text-align:center;">{$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}</td>
							<td style="text-align:center;">{$INV.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}</td>
							<td>{$INV.BILLING.FNAME}, {$INV.BILLING.NAME}</td>
							<td>{$INV.CUSTOMER_NICKNAME}</td>
							<td style="text-align:right;">{$INVOICE_WEIGHT[$kINV]|number_format:2:",":"."}</td>
							<td style="text-align:right;">{$INV.ARTICLE.PRICE|number_format:2:",":"."}</td>
							<td style="text-align:right;">{$INV.ARTICLE.VAT|number_format:2:",":"."}</td>
							<td style="text-align:right;">{($INV.ARTICLE.PRICE + $INV.ARTICLE.VAT)|number_format:2:",":"."}</td>
					
							{$INVOICE_WEIGHT_SUM = $INVOICE_WEIGHT_SUM + $INVOICE_WEIGHT[$kINV]}
							{$INVOICE_PRICE = $INVOICE_PRICE + $RET.ARTICLE.PRICE}
							{$INVOICE_VAT = $INVOICE_VAT + $INV.ARTICLE.VAT}
							{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO +$INV.ARTICLE.PRICE + $INV.ARTICLE.VAT}
						</tr>
					{/foreach}
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right;">{$INVOICE_WEIGHT_SUM|number_format:2:",":"."}g</td>
					<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_VAT|number_format:2:",":"."}</td>
					<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:",":"."}</td>
				</tr>
			</tfoot>
		</table>
</div>



	<!--Inoice-->
	<div>
		<ul class='menu h'>
			{$now_y = $smarty.now|date_format:"%Y"}
			{for $y=2012 to $now_y}
				<li>
					<button {if ($PLA.RETURN.W.DATE|replace:"%":'') == $y}class="active"{/if} onclick="wp.ajax({
											'url'	:	'?D[PAGE]=platform.return_list',
											'data'	:	{
															'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
															'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][DATE]' : '{$y}%'
														},
											'div'	:	'fPLA{$D.PLATFORM_ID}'
											})">{$y}</button>
			
					<ul class='menu h'>
						{for $i=1 to 12}
							<li><button {if  ($PLA.RETURN.W.DATE|replace:"%":''|date_format: "%Y") == $y && ($PLA.RETURN.W.DATE|replace:"%":''|date_format: "%m") == $i}class="active"{/if} onclick="wp.ajax({
											'url'	:	'?D[PAGE]=platform.return_list',
											'data'	:	{
															'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
															'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
															'D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][DATE]' : '{$y}-{if $i < 10}0{/if}{$i}%'
														},
											'div'	:	'fPLA{$D.PLATFORM_ID}'
											})">{$i}</button></li>
						{/for}
					</ul>
				</li>
			{/for}
		</ul>
		<div style="clear:both;"></div>
		<form method="post">
			<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}]['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
			<!--<input type="hidden" name="D[PLATFORM][D][{$D.PLATFORM_ID}]['PLATFORM_ID']" value="{$D.PLATFORM_ID}">-->
			<table class='list' style='width:100%;'>
				<thead>
					<tr>
						<td style="width:50px;">Bilanz</td>
						<td style="width:30px;"><button type="button" onclick="id = wp.get_genID(); wp.window.open({ 'ID' : id, 'TITLE' : 'Neu Storno' , 'WIDTH' : '800px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][D]['+id+'][ACTIVE]=1'});">+</button></td>
						<td style="width:200px;">ID</td>
						<td style="width:60px;">Nummer</td>
						<td style="width:60px;">Platform</td>
						<td style="width:60px;">Datum</td>
						<td style="width:60px;">Status</td>
						<td style="width:60px;">Lager</td>
						<td style="width:200px;">Name</td>
						<td style="width:100px;">Nickname</td>
						<td style="width:60px;">Gewicht</td>
						<td style="width:60px;">Netto</td>
						<td style="width:60px;">MwSt</td>
						<td style="width:60px;">Brutto</td>
						<td>
							{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
								{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'return' && $SET.VARIANTE.D['ACTIVE'].VALUE}
								<button type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][DATE]={$PLA.RETURN.W.DATE}','_blank');" title="Export Inv:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
								{/if}
							{/foreach}
						</td>
					</tr>
				</thead>
				<tbody>
				
						{foreach from=$PLA.RETURN.D key="kINV" item="INV" name="INV"}
							<tr>
							{if $LAST_DAY == NULL}
								{$LAST_DAY = $INV.DATE|date_format:"%d"}
							{/if}
							{if $group_D != $INV.DATE}
								<td style="text-align:center;height:100px;width:50px;position:relative;" rowspan="{$PLA.RETURN.DATE.D[ $INV.DATE ].COUNT}"><div style="transform:rotate(270deg);width:100px;position:absolute;right:-24px;">{$PLA.RETURN.DATE.D[$INV.DATE].COUNT}St. <br> {($PLA.RETURN.DATE.D[$INV.DATE].PRICE+$PLA.RETURN.DATE.D[$INV.DATE].VAT)|number_format:2:",":"."}€</div></td>
							{/if}
							{$group_D = $INV.DATE}

								<td style="text-align:right;">{$smarty.foreach.INV.iteration}</td>
								<td>{$kINV}</td>
								<td style="text-align:right;"><button type="button" onclick="wp.window.open({ 'ID' : '{$kINV}', 'TITLE' : '{$INV.NUMBER}' , 'WIDTH' : '800px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.return&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kINV}&&D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][ACTIVE]'});">{$INV.NUMBER}</button></td>
								<td style="text-align:center;">{$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}</td>
								<td style="text-align:center;">{$INV.DATE|date_format:"%d.%m.%Y"}</td>
								<td style="text-align:ceter;background:{if $INV.STATUS >=40}green{else if $INV.STATUS >=20}yellow{else}red{/if}">{$INV.STATUS}</td>
								<td style="text-align:center;">{$PLA.WAREHOUSE.D[$INV.WAREHOUSE_ID].TITLE}</td>
								<td>{$INV.FNAME}, {$INV.NAME}</td>
								<td>{$INV.CUSTOMER_NICKNAME}</td>
								<td style="text-align:right;">{$INV.ARTICLE.WEIGHT|number_format:0:",":"."}g</td>
								<td style="text-align:right;">{$INV.ARTICLE.PRICE|number_format:2:",":"."}</td>
								<td style="text-align:right;">{$INV.ARTICLE.VAT|number_format:2:",":"."}</td>
								<td style="text-align:right;">{($INV.ARTICLE.PRICE + $INV.ARTICLE.VAT)|number_format:2:",":"."}</td>
								<td>
									{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
										{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'return' && $SET.VARIANTE.D['ACTIVE'].VALUE}
										<button type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][RETURN][W][ID]={$kINV}','_blank');" title="Export Inv:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
										{/if}
									{/foreach}
								</td>
								{$RETURN_WEIGHT = $RETURN_WEIGHT + $INV.ARTICLE.WEIGHT}
								{$RETURN_PRICE = $RETURN_PRICE + $INV.ARTICLE.PRICE}
								{$RETURN_VAT = $RETURN_VAT + $INV.ARTICLE.VAT}
								{$RETURN_PRICE_BRUTTO = $RETURN_PRICE_BRUTTO + $INV.ARTICLE.PRICE + $INV.ARTICLE.VAT}
							
							</tr>
						{/foreach}
				
				</tbody>
				<tfoot>
					<tr>
						<td style="text-align:left;" colspan="3">{if $LAST_DAY > 0}Ø {($PLA.RETURN.D|COUNT/$LAST_DAY)|number_format:0:",":"."}St./Tag | {(($PLA.RETURN.PRICE+$PLA.RETURN.VAT)/$LAST_DAY)|number_format:0:",":"."}€/Tag | ({$LAST_DAY} Tage){/if}</td>
						
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align:right;">{$RETURN_WEIGHT|number_format:0:",":"."}g</td>
						<td style="text-align:right;">{$RETURN_PRICE|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$RETURN_VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$RETURN_PRICE_BRUTTO|number_format:2:",":"."}</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</form>
	</div>
{*/foreach*}