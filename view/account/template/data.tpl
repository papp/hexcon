{*assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]*}
{switch $D.ACTION}
	{case 'set_supplier'}
	{/case}
	{default}

	{function name="tbody" D=null LEVEL = null}
		
		{foreach from=$D.F key="kDAT" item="DAT"}
			
			{if $D.PATTERN[$kDAT]}
				
				{$_nextPossible = 0}
				{foreach from=$D.F[$kDAT] key='kNext' item='Next'}
					
					{if $D.PATTERN[$kDAT].D[$kNext] && isset($D[$kDAT].D )}
						{$_nextPossible = 1}
						{foreach from=$D[$kDAT].D key='kNext2' item='Next2'}
							
							{if $D[$kDAT].D[ $kNext2 ][ $kNext ]}
								{*$d[ $kNext ] = array_replace_recursive((array)$D[$kDAT].D[ $kNext2 ][ $kNext ], (array)$D[$kDAT][ $kNext ] )*}
								{$d[ $kNext ] = $D[$kDAT].D[ $kNext2 ][ $kNext ]}
								{$d.F[ $kNext ] = $D.F[$kDAT][ $kNext ] }
								{$PFAD2 = "{$PFAD2}[{$kDAT}][D][{$kNext2}]"}
							{/if}
						{/foreach}
					{/if}
				{/foreach}
				{$d.PLATFORM_ID = $D.PLATFORM_ID}
				
				{$d.PATTERN = $D.PATTERN[$kDAT].D}
				
				{$LEVEL[] = ['TYPE' => $kDAT, 'ID' => $D.F[$kDAT].W.0.ID]}
				
				{$LastLevel = end($LEVEL)}
				
				{if $D.F[$kDAT].W.0.ID && !$_nextPossible}
					
					
					{foreach from=$D[ $kDAT ].D key="kDATA" item="DATA" name="DATA"}
						{if $kDATA == $D.F[$kDAT].W.0.ID}
							{foreach from=$D.PATTERN[ $kDAT] key="kPAT" item="PAT" name="PAT"}
								{if $kPAT != 'D'}
									<tr>
										<td>{$kPAT}:</td>
										
										<td>
											{input p=['type' => $PAT.Type, 'option' => $PAT.Option, 'readonly' => $PAT.Readonly, 'name'=>"D{$PFAD2}[{$kDAT}][D][{$kDATA}][{$kPAT}]", 
											'value'=>htmlspecialchars($DATA[$kPAT]) ]}
										
										</td>
									</tr>
								{/if}
							{/foreach}
						{/if}
					{/foreach}





{*
					{foreach from=$D[ $kDAT ].D key="kDATA" item="DATA" name="DATA"}
						{if $kDATA == $D.F[$kDAT].W.0.ID}
							{foreach from=$D.PATTERN[ $kDAT] key="kPAT" item="PAT" name="PAT"}
								{if $kPAT != 'D'}
									<tr>
										<td>{$kPAT}:</td>
										
										<td>
										{if is_array($DATA[$kPAT])}
											{foreach from=$DATA[$kPAT] key='kLAN' item='LAN'}
												<div class="input-group mb-1">
													<div class="input-group-prepend"><span class="input-group-text">{$kLAN}</span></div>
													{input p=['type' => $PAT.Type, 'option' => $PAT.Option, 'readonly' => $PAT.Readonly, 'name'=>"D{$PFAD2}[{$kDAT}][D][{$kDATA}][{$kPAT}][{$kLAN}]", 'value'=>htmlspecialchars($LAN) ]}
												</div>
											{/foreach}
										{else}
											{input p=['type' => $PAT.Type, 'option' => $PAT.Option, 'readonly' => $PAT.Readonly, 'name'=>"D{$PFAD2}[{$kDAT}][D][{$kDATA}][{$kPAT}]", 'value'=>htmlspecialchars($DATA[$kPAT]) ]}
										{/if}
										</td>
									</tr>
								{/if}
							{/foreach}
						{/if}
					{/foreach}
*}

				{else}
					{tbody D=$d LEVEL=$LEVEL}
	
				{/if}

			{/if}	
		{/foreach}

	{/function}

	<form method="post">
		<table class="table">
		{tbody D=$D}
		</table>
	</form>


{/switch}