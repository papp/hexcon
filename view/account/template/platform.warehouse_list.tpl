{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'load_set_article'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			
			{if $kART == $PLA.ARTICLE.W.ID}
				{$SOLL = $D.BAY_STOCK * $D.SET.QUANTITY}
				<tr id="SET{$D.PLATFORM_ID}{$D.ARTICLE_ID}{$PLA.ARTICLE.W.ID}">
					<td style="text-align:center;font-size:30px;{if $D.SET.QUANTITY > 1}animation: blink 1s steps(2, start) infinite;{/if}">{$D.SET.QUANTITY}</td>
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_150x150.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_100x100.jpg"></td>
					<td>{$ART.NUMBER}<br><br>
						{if $PLA.ARTICLE.D[$kART].SUPPLIER.D}
							<b>Lieferantennummer:</b><br>
							{foreach from=$PLA.ARTICLE.D[$kART].SUPPLIER.D key="kSUP" item="SUP"}
								{$SUP.NUMBER}<br>
							{/foreach}
						{/if}
					</td>
					<td>
						{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
							<div class="alert alert-warning p-1" style="color:red;">
								{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
								<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
								{for $i=0 to count($aVAR)-1}
									{$PLA.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].TITLE}: {$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} <br> {/if}
								{/for}</b>
								{/if}
							</div>
							{$RCS['DELIMITER']['LEFT'] = '[('}
							{$RCS['DELIMITER']['RIGHT'] = ')]'}
							{$CWP->rand_choice_str($PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}
						{else}
							{$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}<br>
						{/if}
						
						{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
							<div class="alert alert-danger p-1">FEHLER: keine Zuordnung! Bitte Information im Kommentar beachten oder nachfragen!</div>
						{/if}
					</td>
					<td><label style="{if !$ART.WEIGHT}color:red{/if}">{$ART.WEIGHT*$SOLL} g  (e.{$ART.WEIGHT} g)</label></td>
					<td style="text-align:right;">
						{foreach from=$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
							{if isset($D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK)}
								<div><b>{$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</b> ({$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}) <input style="width:50px;text-align:right;" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$D.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" value="{if $SOLL > $D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{$SOLL = $SOLL-$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{$D.WAREHOUSE.D[$D.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{else}{$SOLL}{$SOLL = 0}{/if}"></div>
							{/if}
						{/foreach}
						{if $SOLL}
							<div style="animation: blink 1s steps(2, start) infinite;">Überbuchung: {$SOLL}St. fehlen</div>
						{/if}
							
					</td>
					<td>
						<div style="font-size:30px;text-align:center;{if $SOLL}color:red{else}color:green;{/if}"><b>{$D.BAY_STOCK * $D.SET.QUANTITY}</b></div>
					</td>
				</tr>
			{/if}
		{/foreach}
	{/case}
	{default}
	{*
	<button type="button" onclick="wp.ajax({
												'url'	:	'?D[PAGE]=platform.warehouse_list2',
												'data'	:	{
																'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['WAREHOUSE_ID']}',
																'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]' : '{$D['PLATFORM']['D'][$D.PLATFORM_ID]['INVOICE']['W']['STATUS']}',
															},
												'div'	:	'fPLA{$D.PLATFORM_ID}',ASYNC:1
												})">NEUE LAGER</button>
												*}
	<script>
	SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID} = function()
			{
				wp.ajax({
				'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=set_invoice&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'div' : 'ajax',
				'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize()
				});
			
			}
	</script>
	
	<style>@keyframes blink { to { color: red; }}</style>
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
	
		<div style="background:#ddd;position:fixed;width:100%;">
			<input type="hidden" name="D['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
			<input type="hidden" name="D['PLATFORM_ID']" value="{$D.PLATFORM_ID}">
			<button class="btn btn-primary" type="button" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();">Speichern</button>

			Bestellungen: {$PLA.INVOICE.D|@count}

			{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
				{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'warehouse' && $SET.VARIANTE.D['ACTIVE'].VALUE}
				<button class="btn btn-primary" type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][STATUS]=20&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][O][NUMBER]=ASC&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][WAREHOUSE_ID]={$D.PLATFORM.D[$D.PLATFORM_ID].INVOICE.W.WAREHOUSE_ID}','_blank');" title="Export Inv:{$INV.NUMBER}">
				{$SET.VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}
				</button>
				{/if}
			{/foreach}
		</div>
		<div style="height:20px;"></div>
		{foreach from=$PLA.INVOICE.D key="kINV" item="INV"}
			<table class='list' style='width:100%;'>
				<thead>
					<tr>
						<td style="width:30px;"></td>
						<td style="width:100px;">ID</td>
						<td style="width:60px;">Nummer</td>
						<td style="width:60px;">Datum</td>
						<td style="width:60px;">Status</td>
						<td>Rechnungsadresse</td>
						<td>Lieferadresse</td>
						<td style="width:150px;">Versand</td>
						<td style="width:150px;">Kommentar</td>
						<td style="width:200px;"></td>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td><input type="hidden" value='{$INV.PLATFORM_ID}' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][PLATFORM_ID]"></td>
							<td>ID: {$kINV}<br>
								NICK: {$INV.CUSTOMER_NICKNAME}<br>
								E-Mail: {$INV.CUSTOMER_EMAIL}<br>
								PF: {$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}<br>
								{foreach from=$PLA.SETTING.D key="kSET" item="SET" name="SET"}
									{if $SET.VARIANTE.D['SCRIPT_TYP'].VALUE == 'order' && $SET.VARIANTE.D['ACTIVE'].VALUE}
									<button class="btn btn-primary" type="button" onclick="window.open('file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kSET}_{$SET.VARIANTE.D['SECURITY_KEY'].VALUE}.{$SET.VARIANTE.D['FILE_TYP'].VALUE}?D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kINV}','_blank');" title="Export Inv:{$INV.NUMBER}">{$SET.VARIANTE.D['FILE_TYP'].VALUE}</button>
									{/if}
								{/foreach}
							</td>
							<td style="text-align:center;"><button class="btn btn-primary" type="button" onclick="wp.window.open({ 'ID' : 'WAR{$D.PLATFORM_ID}', 'TITLE' : 'Warehouse' , 'WIDTH' : '900px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.warehouse&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][W][ID]={$kINV}'});">{$INV.NUMBER}</button></td>
							<td style="text-align:center;">{$INV.DATE|date_format:"%d.%m.%Y"}</td>
							<td style="text-align:ceter;width:100px;background:{if $INV.STATUS >=40}green{else if $INV.STATUS >=20}yellow{else}red{/if}">
								<input type="radio" value='0' {if $INV.STATUS ==0}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">offen<br>
								<input type="radio" value='9' {if $INV.STATUS ==9}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">Klärung<br>
								<input type="radio" value='20' {if $INV.STATUS ==20}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">Versandfreigabe<br>
								<input type="radio" value='40' {if $INV.STATUS ==40}checked{/if} name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">Fertig<br>
								{*
								<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">
									<option value='0' {if $INV.STATUS ==0}selected{/if}>offen</option>
									<option value='20' {if $INV.STATUS ==20}selected{/if}>Versandfreigabe</option>
									<option value='40' {if $INV.STATUS ==40}selected{/if}>Fertig</option>
									</select>*}
							</td>
							<td style="background:#fff;font-size:13px;">
								{$INV.BILLING.COMPANY}<br>
								{$INV.BILLING.FNAME} {$INV.BILLING.NAME}<br>
								{$INV.BILLING.STREET} {$INV.BILLING.STREET_NO}<br>
								{if $INV.BILLING.ADDITION}{$INV.BILLING.ADDITION}<br>{/if}
								{$INV.BILLING.ZIP} {$INV.BILLING.CITY}<br>
								{$D.COUNTRY.D[$INV.BILLING.COUNTRY_ID].TITLE}
							</td>
							<td style="background:#fff;font-size:13px;">
								{if strpos(strtolower($INV.DELIVERY.STREET), "packstation") !== false || strpos(strtolower($INV.DELIVERY.ADDITION), "packstation") !== false}
								<style>
								@keyframes blinker {  
									50% { opacity: 0; }
									}
								</style>
								<div style="animation: blinker 1s linear infinite;color:Red;"><b>PACKSTATION</b></div>{/if}
									{$INV.DELIVERY.COMPANY}<br>
									{$INV.DELIVERY.FNAME} {$INV.DELIVERY.NAME}<br>
									{$INV.DELIVERY.STREET} {$INV.DELIVERY.STREET_NO}<br>
									{if $INV.DELIVERY.PHONE}Tel:{$INV.DELIVERY.PHONE}<br>{/if}
									{if $INV.DELIVERY.ADDITION}{$INV.DELIVERY.ADDITION}<br>{/if}
									{$INV.DELIVERY.ZIP} {$INV.DELIVERY.CITY}<br>
									{$D.COUNTRY.D[$INV.DELIVERY.COUNTRY_ID].TITLE}
							</td>
							<td style="text-align:right;">
								</select>
								<div style="{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}background:yellow;{/if}">Traking:{*<input type='text' style="{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}background:yellow;{/if}" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRACKING_NO]" value='{$INV.TRACKING_NO}'>*}
								{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRACKING_NO]", 'value'=>$INV.TRACKING_NO]}</div>
								
								Datum:<input type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPED_DATE]" value='{$smarty.now|date_format:"%Y-%m-%d"}'>
								Versandfirma:<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPING_ID]">
									<option value=""></option>
									{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
										<option value='{$kSHI}' {if $INV.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
									{/foreach}
								</select>
								<div style="{if !$INV.ARTICLE.WEIGHT}color:red{/if}">Gewicht: {round($INV.ARTICLE.WEIGHT/1000,2)} kg</div>
							</td>
							<td style="text-align:right;">	
								{*<textarea class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]' style='width:200px;height:100px;'>{$INV.COMMENT}</textarea>*}
								{input p=['type'=> 'textarea', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]", 'value'=>$INV.COMMENT, 'style'=>'width:200px;height:100px;']}
											
							</td>
							<td rowspan="2" valign="top">
							{*MESSAGE START ================*}
								{if $INV.CUSTOMER_ID}
								<div style="height:180px;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}"></div>
									<script>
										wp.ajax({
															'url'	:	'?D[PAGE]=message',
															'data'	:	{
																			'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																			'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																			'D[MESSAGE][W][GROUP_ID]' : '{$INV.CUSTOMER_ID}', //group_id,
																			'D[MESSAGE][O][DATETIME]' : 'DESC',
																			'D[MESSAGE][W][INTERNALLY]' : '1'
																		},
															'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}'
															});
									</script>
								{/if}
							{*MESSAGE END ================*}
							</td>
						</tr>
						<tr style="border-bottom:5px red solid;">
							<td colspan="9">
								<table  style="width:100%;">
									<thead>
										<tr>
											<td style="width:15%"></td>
											<td style="width:10%">Anz</td>
											<td style="width:10%">Bild</td>
											<td style="width:10%">Num</td>
											<td>Title</td>
											<td style="width:10%">Gewicht</td>
											<td style="width:15%">Lager</td>
											<td style="width:10%">Soll Summe</td>
										</tr>
									</thead>
									
								{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
									
												<tbody id="SET{$D.PLATFORM_ID}{$kINV}{$kART}" style="border-bottom:solid 2px red;">
													<tr style="height:1px;">
														{if $PLA.ARTICLE.D[$kART].SET}
															<td style="text-align:center;vertical-align:middle;width:150px;border-right:dotted 2px blue;" rowspan={count($PLA.ARTICLE.D[$kART].SET.ARTICLE.D)+1}>
																<table style="width:100%;"><tr><td style="border:none;">
																<h2>SET</h2>
																<div title="<nobr>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
																</nobr>">
																	<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_50x50.jpg">
																</div>
																{$ART.NUMBER}<br>
																{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																	<div style="color:red;">
																	{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																	<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
																	{for $i=0 to count($aVAR)-1}
																		{$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} | {/if}
																	{/for}</b></div>
																	{/if}
																{else}
																	{$ART.TITLE}<br>
																{/if}
																</td>
																<td style="border:none;text-align:center;font-size:30px;vertical-align:middle;{if $ART.STOCK > 1}animation: blink 1s steps(2, start) infinite;{/if}">{if $PLA.ARTICLE.D[$kART].SET}{$ART.STOCK}x{/if}
																</td></tr></table>
															</td>
															{foreach from=$PLA.ARTICLE.D[$kART].SET.ARTICLE.D key="kSET" item="SET"}
																<script>
																wp.ajax({
																'url' : '?D[PAGE]=platform.warehouse_list&D[ACTION]=load_set_article&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[INVOICE_ID]={$kINV}&D[ARTICLE_ID]={$kART}',
																'div' : 'SET{$D.PLATFORM_ID}{$kINV}{$kART}',
																
																'INSERT' : 'append',
																'data'	: {
																	//'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]' : '{$kART}',
																	'D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID]' : '{$kSET}',
																	'D[SET][QUANTITY]' : '{$SET.QUANTITY}',
																	'D[BAY_STOCK]' : '{$ART.STOCK}',
																	'D[WAREHOUSE_ID]'	: '{$PLA.INVOICE.W.WAREHOUSE_ID}',
																	'D[INVOICE_ID]' : '{$kINV}'
																			}
																});
																</script>
															{/foreach}
														{else}
																<td></td>
																<td style="text-align:center;font-size:30px;vertical-align:middle;{if $ART.STOCK > 1}animation: blink 1s steps(2, start) infinite;{/if}">{if !$PLA.ARTICLE.D[$kART].SET}{$ART.STOCK}{/if}</td>
																<td title="<nobr>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_1_200x200.jpg'>
																	<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_3_200x200.jpg'>
																</nobr>">
																	<img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_100x100.jpg">
																</td>
																<td>{$ART.NUMBER}<br><br>
																{if $PLA.ARTICLE.D[$kART].SUPPLIER.D}
																	<b>Lieferantennummer:</b><br>
																	{foreach from=$PLA.ARTICLE.D[$kART].SUPPLIER.D key="kSUP" item="SUP"}
																	<div class="alert-warning p-1 m-1">
																		{$D.PLATFORM.D[$D.PLATFORM_ID].SUPPLIER.D[$kSUP].TITLE}: <b>{$SUP.NUMBER}</b>
																	</div>
																	{/foreach}
																{/if}
																</td>
																<td>
																	{if $PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID}
																		<div class="alert alert-warning p-1" style="color:red;">
																			<b>{$aVAR = explode('|',$PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].VARIANTE_GROUP_ID)}
																			{for $i=0 to count($aVAR)-1}
																				{$PLA.ATTRIBUTE.D[ $aVAR[$i] ].LANGUAGE.D['DE'].TITLE}: {$PLA.ARTICLE.D[$kART].ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE}{if $i < count($aVAR)-1} <br> {/if}
																			{/for}</b>
																		</div>
																		{if $ART.TITLE}
																			{$ART.TITLE}
																		{else}
																			{$RCS['DELIMITER']['LEFT'] = '[('}
																			{$RCS['DELIMITER']['RIGHT'] = ')]'}
																			{$CWP->rand_choice_str($PLA.ARTICLE.D[ $PLA.ARTICLE.CHILD.D[$kART].PARENT_ID ].ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}
																		{/if}
																	{else}
																		{$ART.TITLE}<br>
																	{/if}
																	{if $PLA.ARTICLE.PARENT.D[$kART].CHILD.D}
																		<div class="alert alert-danger p-1">FEHLER: keine Zuordnung! Bitte Information im Kommentar beachten oder nachfragen!</div>
																	{/if}
																</td>
																<td><label style="{if !$ART.WEIGHT}color:red{/if}">{$ART.WEIGHT*$ART.STOCK} g (e.{$ART.WEIGHT} g)</label></td>
																<td style="text-align:right;">
																	{if !$PLA.ARTICLE.D[$kART].SET}
																		{$SOLL = $ART.STOCK}
																		{foreach from=$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D key="kSTO" item="STO"}
																			{if isset($D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK)}
																				<div><b>{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].TITLE}</b> ({$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}) <input style="width:50px;text-align:right;" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRANSACTION][D][{date('YmdHis')}][WAREHOUSE][D][{$PLA.INVOICE.W.WAREHOUSE_ID}][STORAGE][D][{$kSTO}][ARTICLE][D][{$kART}][STOCK]" value="{if $SOLL > $D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{$SOLL = $SOLL-$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{$D.WAREHOUSE.D[$PLA.INVOICE.W.WAREHOUSE_ID].STORAGE.D[$kSTO].ARTICLE.D[$kART].STOCK}{else}{$SOLL}{$SOLL = 0}{/if}"><div>
																			{/if}
																		{/foreach}
																		{if $SOLL}
																			<div style="animation: blink 1s steps(2, start) infinite;">Überbuchung: {$SOLL}St. fehlen</div>
																		{/if}
																		
																	{/if}
																</td>
																<td>
																	<div style="font-size:30px;text-align:center;{if $SOLL}color:red{else}color:green;{/if}"><b> {$ART.STOCK}</b></div>
																</td>
															
															{/if}
													</tr>
									</tbody>
								{/foreach}
									
								</table>
							</td>
						</tr>
					
							
					</tbody>
				</table>
			{/foreach}
				
			</form>
		{/switch}
