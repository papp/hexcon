
	{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}

	<script>
		SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID} = function()
		{
			$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').find(':input').each(function(){
				if( $(this).attr( "ischanged" ) == 0 )
					$(this).attr( "disabled", true );
			});
			wp.ajax({
			'url' : '?D[PAGE]=warehouse.setting&D[ACTION]=set_warehouse&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
			'div' : 'ajax',
			'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').serialize()
			});
			//set_soll_stock_zero_after_save('{$PLA.INVOICE.W.WAREHOUSE_ID}');
			$('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}').find(':input').each(function(){
				$(this).attr( "disabled", false );
			});
		}
	</script>
	<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" method="post">
		<div style="background:#ddd;position:fixed;width:100%;">
			<input type="hidden" name="D['ACCOUNT_ID']" value="{$D.SESSION.ACCOUNT_ID}">
			<input type="hidden" name="D['PLATFORM_ID']" value="{$D.PLATFORM_ID}">
			<button class="btn btn-primary" type="button" id="btnSave" onclick="SAVE{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}();this.disabled = true;">Speichern</button>
		</div>
		<div style="height:20px;"></div>
		<div>
		<table class="table">
			<tr>
				<th>id</th>
				<th>Ref-id</th>
				<th>Lagerort Name</th>
				<th>Sortierung</th>
			</tr>
		{foreach from=$D.WAREHOUSE.D[$D.WAREHOUSE.W.ID].STORAGE.D item=STO key=kSTO name=STO}
			<tr>
				<th>{$kSTO}</th>
				<td>{input p=['name'=>"D[WAREHOUSE][D][{$D.WAREHOUSE.W.ID}][STORAGE][D][{$kSTO}][REFID]", 'value'=>$STO.REFID]}</td>
				<td>{input p=['name'=>"D[WAREHOUSE][D][{$D.WAREHOUSE.W.ID}][STORAGE][D][{$kSTO}][TITLE]", 'value'=>$STO.TITLE]}</td>
				<td>{input p=['name'=>"D[WAREHOUSE][D][{$D.WAREHOUSE.W.ID}][STORAGE][D][{$kSTO}][SORT]", 'value'=>$STO.SORT]}</td>
			</tr>
		{/foreach}
		</table>
		</div>
	</form>
