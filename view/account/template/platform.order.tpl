{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'search_article'}
		<table class='table' style='width:100%;'>
		{$RCS['DELIMITER']['LEFT'] = '[('}
		{$RCS['DELIMITER']['RIGHT'] = ')]'}
		{foreach from=$PLA.ARTICLE.PARENT.D[NULL].CHILD.D key="kART" item="ART"}
			{$ART = $PLA.ARTICLE.D[$kART]}
				<thead>
				<tr>
					<th style="cursor:pointer;"  colspan=5 onclick="wp.ajax({ 'div' : 'art_list{$D.INVOICE_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article&D[ARTICLE_ID]={$kART}&D[INVOICE_ID]={$D.INVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'data' : {
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][NUMBER]' : '{$ART.NUMBER}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][PRICE]' : '{($ART.PRICE - ($ART.PRICE/(100+$ART.VAT)*$ART.VAT))}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][STOCK]' : '0',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][VAT]' : '{$ART.VAT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][WEIGHT]' : '{$ART.WEIGHT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kART}][TITLE]' : '{$CWP->rand_choice_str($TITLE,$RCS)}'
						}});
						document.getElementById('search{$D.INVOICE_ID|replace:'-':''}box').style.display = 'none';">{$ART.NUMBER} - {$CWP->rand_choice_str($ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)}</th>
				</tr>
				</thead>
			{foreach from=$PLA.ARTICLE.PARENT.D[$kART].CHILD.D key="kVAR" item="VAR"}
				{$VAR = $PLA.ARTICLE.D[$kVAR]}
				{$TITLE = ''}
				{if $ART.VARIANTE_GROUP_ID}
					{$aVAR = explode('|',$ART.VARIANTE_GROUP_ID)}
					{for $i=0 to count($aVAR)-1}
						{$TITLE = "{$TITLE} {$VAR.ATTRIBUTE.D[$aVAR[$i]].LANGUAGE.D['DE'].VALUE} |"}
					{/for}
				{/if}
				{$TITLE2 = ($VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE)?"{$TITLE} {$VAR.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}":"{$TITLE} {$ART.ATTRIBUTE.D['TITLE'].LANGUAGE.D['DE'].VALUE}" }
				
				<tr style="cursor:pointer;" onclick="wp.ajax({ 'div' : 'art_list{$D.INVOICE_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=load_article&D[ARTICLE_ID]={$kVAR}&D[INVOICE_ID]={$D.INVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}',
				'data' : {
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][NUMBER]' : '{$VAR.NUMBER}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][PRICE]' : '{($VAR.PRICE - ($VAR.PRICE/(100+$VAR.VAT)*$VAR.VAT))}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][STOCK]' : '0',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][VAT]' : '{$VAR.VAT}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][WEIGHT]' : '{if $VAR.WEIGHT}{$VAR.WEIGHT}{else}{$ART.WEIGHT}{/if}',
						'D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$kVAR}][TITLE]' : '{$CWP->rand_choice_str($TITLE2,$RCS)}'
						}});
						document.getElementById('search{$D.INVOICE_ID|replace:'-':''}box').style.display = 'none';">
					<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kVAR}_0_25x25.jpg"></td>
					<td>{$VAR.NUMBER}</td>
					<td>{$CWP->rand_choice_str($TITLE,$RCS)}</td>
					<td style="text-align:right;">{$VAR.PRICE|number_format:2:".":""}€</td>
					<td title="Bestand" style="text-align:right;{if !$VAR.STOCK>0}color:red;{/if}">{$VAR.STOCK*1}</td>
				</tr>
			{/foreach}
		{/foreach}
		</table>
		
	{/case}
	{case 'load_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.ORDER_ID}{$D.ARTICLE_ID}" {if $PLA.ARTICLE.D[ $D.ARTICLE_ID ].VARIANTE_GROUP_ID || !$PLA.ARTICLE.D[ $D.ARTICLE_ID ]}style="background:#fcc"{/if}>
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button class="btn" type="button" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.ORDER_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;cursor:pointer;" onclick="wp.window.open({ 'ID' : 'ART{$D.PLATFORM_ID}{$D.ARTICLE_ID}', 'TITLE' : '{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER} | {$TITLE|replace:'"':''|truncate:80:"...":true}' , 'WIDTH' : '920px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.article&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][ID|ID2PARENT]={$D.ARTICLE_ID}'});" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input class="form-control" name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input class="form-control" style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input class="form-control" style="width:60px;text-align:right;" onchange="this.value = parseFloat(this.value.replace(/,/, '.'))" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value.replace(/,/, '.')/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input class="form-control" style="width:60px;text-align:right; {if $D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK>$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}color:red;{/if}" name='D[PLATFORM][D][{$D.PLATFORM_ID}][ORDER][D][{$D.ORDER_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].ORDER.D[{$D.ORDER_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
			<td style="text-align:right;">{$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}</td>
		</tr>
	{/case}
	{case 'load_return_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}">
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button type="button" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.INVOICE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td style="width:25px;" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$D.ARTICLE_ID}_0_25x25.jpg"></td>
			<td style="width:100px;"><input name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER}'></td>
			<td><input style="width:100%;" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE}'></td>
			<td><input style="width:50px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][WEIGHT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].WEIGHT}'>g</td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}'></td>
			<td><input style="width:60px;text-align:right;" id='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}'></td>
			<td><input style="width:60px;text-align:right;" onkeyup="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*100" value='{($D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE + $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE/100*$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT)|number_format:2:".":""}'></td>
			<td><input style="width:60px;text-align:right;{if $D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK>$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}color:red;{/if}" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$D.INVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]' value='{$D.PLATFORM.D[{$D.PLATFORM_ID}].INVOICE.D[{$D.INVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].STOCK}'></td>
			<td style="text-align:right;">{$PLA.ARTICLE.D[ $D.ARTICLE_ID ].STOCK}</td>
		</tr>
	{/case}
	{default}
	{*foreach from=$D.PLATFORM.D key="kPL" item="PL"*}
		{foreach from=$PLA.ORDER.D key="kINV" item="INV"}
		
				{*
				<!--CUSTOMER START-->
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ACTIVE]" value='1'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][NICKNAME]" value='{$INV.CUSTOMER_NICKNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][EMAIL]" value='{$INV.CUSTOMER_EMAIL}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][NAME]" value='{$INV.BILLING.NAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][FNAME]" value='{$INV.BILLING.FNAME}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][COMPANY]" value='{$INV.BILLING.COMPANY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][STREET]" value='{$INV.BILLING.STREET}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][STREET_NO]" value='{$INV.BILLING.STREET_NO}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ZIP]" value='{$INV.BILLING.ZIP}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][CITY]" value='{$INV.BILLING.CITY}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][COUNTRY_ID]" value='{$INV.BILLING.COUNTRY_ID}'>
				<input type='hidden' name="D[CUSTOMER][D][{$INV.CUSTOMER_ID}][ADDITION]" value='{$INV.BILLING.ADDITION}'>
				<!--CUSTOMER END-->
				*}

			
				<table style="width:100%;">
					<tr>
						<td valign="top">
							<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINV}" method="post">
		
								<input type='hidden' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_ID]" value='{$INV.CUSTOMER_ID}'>
								<table>
									<tr>
										<td style="width:10%">Rechnung</td>
										<td style="width:20%">&#xf2bb; Rechnungsadresse</td>
										<td style="width:20%">&#xf2bb; Lieferadresse</td>
										<td>&#xf1d8; Lieferungen</td>
									</tr>
									<tr>
										<td style='min-width:150px;'>
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td>KNr.</td>
													<td><button class="btn btn-light" type="button" onclick="wp.window.open({ 'ID' : 'C{$INV.CUSTOMER_ID}', 'TITLE' : 'Kunde {$INV.CUSTOMER_ID}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.customer&D[PLATFORM_ID]={$D.PLATFORM_ID}'});">{$INV.CUSTOMER_ID}</button></td>
												</tr>
												<tr>
													<td>RNr:</td>
													<td>{if $INV.NUMBER}{$INV.NUMBER}{else}-neu-{/if}
														<!--<input style='width:50px;text-align:right;' type='hidden' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][NUMBER]" value='{$INV.NUMBER}'>-->
													</td>
												</tr>
												<tr>
													<td>Datum:</td>
													<td><input class="form-control" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DATE]" value='{if $INV.DATE}{$INV.DATE}{else}{$smarty.now|date_format: "%Y-%m-%d"}{/if}'></td>
												</tr>
												<tr>
													<td>Status:</td>
													<td>
														<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">
															{foreach from=$D.INVOICE.STATUS.D key="kSTA" item="STA"}
															<option style="background:{$STA.COLOR}" value='{$kSTA}' {if $INV.STATUS ==$kSTA}selected{/if}>{$STA.TITLE}</option>
															{/foreach}
														</select>
													</td>
												</tr>
												<tr>
													<td>E-Mail:</td>
													<td><input class="form-control" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_EMAIL]" value='{$INV.CUSTOMER_EMAIL}'></td>
												</tr>
												<tr>
													<td>UserNick:</td>
													<td><input class="form-control" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][CUSTOMER_NICKNAME]" value='{$INV.CUSTOMER_NICKNAME}'></td>
												</tr>
												<tr>
													<td>Zahlart:</td>
													<td>
														<select title='Zahlungsart' class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][PAYMENT_ID]">
																<option value=""></option>
															{foreach from=$D.PAYMENT.D key="kPAY" item="PAY"}
																<option value='{$kPAY}' {if $INV.PAYMENT_ID == $kPAY}selected{/if}>{$PAY.TITLE}</option>
															{/foreach}
														</select>
													</td>
												</tr>
												<tr>
													<td>Platform:</td>
													<td>{$D.PLATFORM.D[$INV.PLATFORM_ID].TITLE}</td>
												</tr>
											</table>

										</td>
										<td style='background:#eee;min-width:150px;'>
											<div class="input-group">
												<input style='width:70%;' class="form-control" placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][COMPANY]" value='{$INV.BILLING.COMPANY}'>
												<input style='width:30%;' class="form-control" placeholder="UstID" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][VATID]" value='{$INV.BILLING.VATID}'>
											</div>
											<div class="input-group">
												<input style='width:50%;' class="form-control" placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][FNAME]" value='{$INV.BILLING.FNAME}'>
												<input style='width:50%;' class="form-control" placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][NAME]" value='{$INV.BILLING.NAME}'>
											</div>
											<input class="form-control" placeholder="Telefon" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][PHONE]" value='{$INV.BILLING.PHONE}'>
											<div class="input-group">
												<input style='width:70%;' class="form-control" placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][STREET]" value='{$INV.BILLING.STREET}'>
												<input style='width:30%;' class="form-control" placeholder="Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][STREET_NO]" value='{$INV.BILLING.STREET_NO}'>
											</div>
											<input class="form-control" placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][ADDITION]" value='{$INV.BILLING.ADDITION}'>
											<div class="input-group">
												<input style='width:30%;' class="form-control" placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][ZIP]" value='{$INV.BILLING.ZIP}'>
												<input style='width:70%;' class="form-control" placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][CITY]" value='{$INV.BILLING.CITY}'>
											</div>
											<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][BILLING][COUNTRY_ID]">
												<option value=""></option>
											{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
												<option value='{$kCOU}' {if $INV.BILLING.COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
											{/foreach}
											</select>
										</td>
										<td style='background:#eee;min-width:150px;'>
											<input class="form-control" placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][COMPANY]" value='{$INV.DELIVERY.COMPANY}'>
											<div class="input-group">
												<input style='width:50%;' class="form-control" placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][FNAME]" value='{$INV.DELIVERY.FNAME}'>
												<input style='width:50%;' class="form-control" placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][NAME]" value='{$INV.DELIVERY.NAME}'>
											</div>
											<input class="form-control" placeholder="Telefon" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][PHONE]" value='{$INV.DELIVERY.PHONE}'>
											<div class="input-group">
												<input style='width:70%;' class="form-control" placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][STREET]" value='{$INV.DELIVERY.STREET}'>
												<input style='width:30%;' class="form-control" placeholder="Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][STREET_NO]" value='{$INV.DELIVERY.STREET_NO}'>
											</div>
											<input class="form-control" placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][ADDITION]" value='{$INV.DELIVERY.ADDITION}'>
											<div class="input-group">
												<input style='width:30%;' class="form-control" placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][ZIP]" value='{$INV.DELIVERY.ZIP}'>
												<input style='width:70%;' class="form-control" placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][CITY]" value='{$INV.DELIVERY.CITY}'>
											</div>
											<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][DELIVERY][COUNTRY_ID]">
												<option value=""></option>
											{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
												<option value='{$kCOU}' {if $INV.DELIVERY.COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
											{/foreach}
											</select>
										</td>
										<td valign="top" style="min-width:100px;">
											
												<div style="border:solid 1px red;overflow-y:scroll;">
													<table class="table">
														<tr>
															<td></td>
															<td>Status</td>
															<td>Lager</td>
															<td>Trecking</td>
															<td>Firma</td>
															<td>Versand Datum</td>
														</tr>
														
														{foreach from=$INV.DELIVERY.D key="kDEL" item="DEL"}
														<tr>
															<td>▼</td>
															<td>{$DEL.ACTIVE}</td>
															<td>
																<select class="form-control" title="Lager" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][WAREHOUSE_ID]">
																{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
																	{if $WAR.ACTIVE == 1}
																	<option value='{$kWAR}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].WAREHOUSE_ID == $kWAR}selected{/if}>{$WAR.TITLE}</option>
																	{/if}
																{/foreach}
																</select>
															</td>
															<td>{input p=['placeholder'=>'Tracking Nummer', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][TRACKING_NO]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].TRACKING_NO]}</td>
															<td>
																<select class="form-control" placeholder="Versandfirma" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][SHIPPING_ID]">
																	<option value=""></option>
																	{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
																		<option value='{$kSHI}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
																	{/foreach}
																</select>
															</td>
															<td>{input p=['placeholder'=>'Datum', 'name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][SHIPPED_DATE]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].SHIPPED_DATE]}</td>
														</tr>
														<tr>
															<td colspan=6>
																<table class="table">
																	<tr>
																		<td></td>
																		<td>Absender</td>
																		<td>Emfänger</td>
																	</tr>
																	<tr>
																		<td></td>
																		<td>
																			<div class="input-group">
																				<input style='width:70%;' class="form-control" placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_COMPANY]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_COMPANY}'>
																			</div>
																			<div class="input-group">
																				<input style='width:50%;' class="form-control" placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_FNAME]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_FNAME}'>
																				<input style='width:50%;' class="form-control" placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_NAME]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_NAME}'>
																			</div>
																			<input class="form-control" placeholder="Telefon" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_PHONE]" value='{$INV.RETURN_PHONE}'>
																			<div class="input-group">
																				<input style='width:70%;' class="form-control" placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_STREET]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_STREET}'>
																				<input style='width:30%;' class="form-control" placeholder="Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_STREET_NO]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_STREET_NO}'>
																			</div>
																			<input class="form-control" placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_ADDITION]" value='{$DEL.RETURN_ADDITION}'>
																			<div class="input-group">
																				<input style='width:30%;' class="form-control" placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_ZIP]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_ZIP}'>
																				<input style='width:70%;' class="form-control" placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_CITY]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_CITY}'>
																			</div>
																			<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][DELIVERY][D][{$kDEL}][RETURN_COUNTRY_ID]">
																				<option value=""></option>
																			{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
																				<option value='{$kCOU}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].RETURN_COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
																			{/foreach}
																			</select>
																		</td>
																		<td>
																			<div class="input-group">
																				<input style='width:70%;' class="form-control" placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_COMPANY]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_COMPANY}'>
																			</div>
																			<div class="input-group">
																				<input style='width:50%;' class="form-control" placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_FNAME]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_FNAME}'>
																				<input style='width:50%;' class="form-control" placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_NAME]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_NAME}'>
																			</div>
																			<input class="form-control" placeholder="Telefon" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_PHONE]" value='{$DEL.DELIVERY_PHONE}'>
																			<div class="input-group">
																				<input style='width:70%;' class="form-control" placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_STREET]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_STREET}'>
																				<input style='width:30%;' class="form-control" placeholder="Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_STREET_NO]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_STREET_NO}'>
																			</div>
																			<input class="form-control" placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_ADDITION]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_ADDITION}'>
																			<div class="input-group">
																				<input style='width:30%;' class="form-control" placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_ZIP]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_ZIP}'>
																				<input style='width:70%;' class="form-control" placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_CITY]" value='{$D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_CITY}'>
																			</div>
																			<select class="form-control" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kDEL}][DELIVERY_COUNTRY_ID]">
																				<option value=""></option>
																			{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
																				<option value='{$kCOU}' {if $D.PLATFORM.D[$D.PLATFORM_ID].DELIVERY.D[$kDEL].DELIVERY_COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
																			{/foreach}
																			</select>
																		</td>
																	</tr>
																</table>

																<table class="table">
																	<tr>
																		<td>Nummer</td>
																		<td>Title</td>
																		<td>Gewicht</td>
																		<td>Stück</td>
																	</tr>
																	{foreach from=$DEL.ARTICLE.D key="kART" item="ART"}
																	<tr>
																		<td>{$ART.NUMBER}</td>
																		<td>{$ART.TITLE}</td>
																		<td>{$ART.WEIGHT}</td>
																		<td>{$ART.STOCK} ({$D.PLANTFORM.D[ $D.PLATFORM_ID ]['ARTICLE']['D'][$kART]['STOCK']})</td>
																	</tr>
																	{/foreach}
																</table>
															</td>
														</tr>
														{/foreach}
													</table>
													
												
												</div>
											

										</td>
									
				</tr>
			</table>

								<table class='table'>
									<thead>
										<tr>
											<td style="width:10px;"><!--<button type="button" onclick="new_article('{$kINV}');">+</button>--></td>
											<td></td>
											<td style="width:100px;">Nummer</td>
											<td>Title</td>
											<td style="width:60px;text-align:right;">Netto</td>
											<td style="width:60px;text-align:right;">MwSt</td>
											<td style="width:60px;text-align:right;">Brutto</td>
											<td style="width:60px;text-align:right;">Stück</td>
										</tr>
									</thead>
									<tbody id="art_list{$kINV|replace:'-':''}">
									{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
										<tr>
											<td></td>
											<td></td>
											<td>{$ART.NUMBER}</td>
											<td>{$ART.TITLE}</td>
											<td style="text-align:right;">{$ART.PRICE|number_format:2:".":""}</td>
											<td style="text-align:right;">{$ART.VAT|number_format:2:".":""}</td>
											<td style="text-align:right;">{($ART.PRICE/100*$ART.VAT)|number_format:2:".":""}</td>
											<td style="text-align:right;">{$ART.STOCK}</td>
										</tr>
										{$INVOICE_PRICE = $INVOICE_PRICE + $ART.PRICE*$ART.STOCK}
										{$INVOICE_VAT = $INVOICE_VAT + $ART.PRICE/100*$ART.VAT*$ART.STOCK}
										{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + ($ART.PRICE + $ART.PRICE/100*$ART.VAT)*$ART.STOCK}
									{/foreach}
									{*foreach from=$INV.ARTICLE.D key="kART" item="ART"}
										{$D['ACTION'] = 'load_article'}
										{$D['ORDER_ID'] = $kINV}
										{$D['ARTICLE_ID'] = $kART}
										
										{include file="platform.order.tpl" D=$D}

										{$INVOICE_PRICE = $INVOICE_PRICE + $ART.PRICE*$ART.STOCK}
										{$INVOICE_VAT = $INVOICE_VAT + $ART.PRICE/100*$ART.VAT*$ART.STOCK}
										{$INVOICE_PRICE_BRUTTO = $INVOICE_PRICE_BRUTTO + ($ART.PRICE + $ART.PRICE/100*$ART.VAT)*$ART.STOCK}
									{/foreach*}
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td style="text-align:right;">{$INVOICE_PRICE|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INVOICE_VAT|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INVOICE_PRICE_BRUTTO|number_format:2:".":""}</td>
											<td></td>
										</tr>
									</tfoot>
								</table>

			{/foreach}
		{*/foreach*}
{/switch}

