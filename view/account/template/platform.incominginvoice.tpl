{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'search_article'}
		<div onclick="document.getElementById('search{$D.INCOMINGINVOICE_ID|replace:'-':''}').style.display = 'none';" style="border-bottom:solid 2px #ddd;text-align:center;cursor:pointer;">Schließen</div>
		<table class='list' style='width:100%;'>
		{$RCS['DELIMITER']['LEFT'] = '[('}
		{$RCS['DELIMITER']['RIGHT'] = ')]'}
		{foreach from=$PLA.ARTICLE.D key="kART" item="ART"}
			<tr style="cursor:pointer;" onclick="wp.ajax({ 'div' : 'art_list{$D.INCOMINGINVOICE_ID|replace:'-':''}', INSERT : 'append', 'url' : '?D[PAGE]=platform.incominginvoice&D[ACTION]=load_article&D[ARTICLE_ID]={$kART}&D[INVOICE_ID]={$D.INCOMINGINVOICE_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}',
			'data' : {
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][NUMBER]' : '{$ART.NUMBER}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][PRICE]' : '{($ART.PRICE - ($ART.PRICE/(100+$ART.VAT)*$ART.VAT))}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][STOCK]' : '0',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][VAT]' : '{$ART.VAT}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][WEIGHT]' : '{$ART.WEIGHT}',
					'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$kART}][TITLE]' : '{$CWP->rand_choice_str($ART.LANGUAGE.D.DE.DESCRIPTION.TITLE,$RCS)}'
					}});
					document.getElementById('search{$D.INCOMINGINVOICE_ID|replace:'-':''}').style.display = 'none';">
				<td title="<img src='file/{$D.PLATFORM_ID}/{$kART}_0_200x200.jpg'>"><img width="25px" src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_25x25.jpg"></td>
				<td>{$ART.NUMBER}</td>
				
				<td>{$CWP->rand_choice_str($ART.LANGUAGE.D.DE.DESCRIPTION.TITLE,$RCS)}</td>
				<td style="text-align:right;">{$ART.PRICE|number_format:2:".":""}€</td>
			</tr>
		{/foreach}
		</table>
		
	{/case}
	{case 'load_article'}
		<tr id="wp{$D.PLATFORM_ID}{$D.INCOMINGINVOICE_ID}{$D.ARTICLE_ID}">
			<td><input id='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' type="hidden" name='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]' value='{$ART.ACTIVE}'>
				<button type="button" class="del" onclick="document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][ACTIVE]').value = '-2';document.getElementById('wp{$D.PLATFORM_ID}{$D.INCOMINGINVOICE_ID}{$D.ARTICLE_ID}').style.display = 'none';">-</button>
			</td>
			<td></td>
			<td style="width:100px;">{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][NUMBER]", 'value'=>$D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].NUMBER]}</td>
			<td>{input p=['name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][TITLE]", 'value'=>$D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].TITLE]}</td>
			
			{$VAT = $D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT}
			{$PRICE = $D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE}
			<td>{input p=['type'=>'number','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]", 'value'=>($PRICE - ($PRICE/(100+$VAT)*$VAT))|number_format:2:".":""]}</td>
			<td>{input p=['type'=>'number','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]", 'value'=>$D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].VAT]}</td>
			<td>{input p=['type'=>'number','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]", 'onkeyup'=>"document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][PRICE]').value = this.value - this.value/(100+parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value))*parseFloat(document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][VAT]').value)", 'value'=>$D.PLATFORM.D[{$D.PLATFORM_ID}].INCOMINGINVOICE.D[{$D.INCOMINGINVOICE_ID}].ARTICLE.D[{$D.ARTICLE_ID}].PRICE]}</td>
			<td>{input p=['type'=>'number','name'=>"D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$D.INCOMINGINVOICE_ID}][ARTICLE][D][{$D.ARTICLE_ID}][STOCK]", 'value'=>$D.PLATFORM.D[$D.PLATFORM_ID].INCOMINGINVOICE.D[$D.INCOMINGINVOICE_ID].ARTICLE.D[$D.ARTICLE_ID].STOCK]}</td>
		</tr>
	{/case}
	{default}
		{foreach from=$PLA.INCOMINGINVOICE.D key="kINC" item="INC"}
				<table >
					<tr>
						<td valign="top">
							<form id="form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINC}" method="post">
		
								<table>
									<tr>
										<td>Rechnung</td>
										{*
										<td>Rechnungsadresse</td>
										<td>Lieferadresse</td>
										<td>Versendet</td>
										*}
										<td>Kommentar</td>
										<td>Anhang</td>
									</tr>
									<tr>
										<td style='width:150px;'>
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td>NR:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][NUMBER]" value='{$INC.NUMBER}'></td>
												</tr>
												<tr>
													<td>Hersteller</td>
													<td>
														<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][SUPPLIER_ID]">
															<option value="">---</option>
														{foreach from=$PLA.SUPPLIER.D key="kSUP" item="SUP"}
															<option value="{$kSUP}" {if $INC.SUPPLIER_ID == $kSUP}selected{/if}>{$SUP.Title}</option>
														{/foreach}
														<select>
													</td>
												</tr>
												<tr>
													<td>Typ:</td>
													<td>
														<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][GROUP_ID]">
															{foreach from=$PLA.GROUP.D key="kGRO" item="GRO"}
															<option value='{$kGRO}' {if $INC.GROUP_ID == $kGRO}selected{/if}>{$GRO.TITLE}</option>
															{/foreach}
														</select>
													</td>
												</tr>
												<tr>
													<td>Datum:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DATE]" value='{if $INC.DATE}{$INC.DATE}{else}{$smarty.now|date_format: "%Y-%m-%d"}{/if}'></td>
												</tr>
												<tr>
													<td>Status:</td>
													<td>
														<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][STATUS]">
															<option value='0' {if $INC.STATUS ==0}selected{/if}>offen</option>
															<option value='20' {if $INC.STATUS ==20}selected{/if}>Versandfreigabe</option>
															<option value='40' {if $INC.STATUS ==40}selected{/if}>Fertig</option>
														</select>
													</td>
												</tr>
												<tr>
													<td>Zahlart:</td>
													<td>
														<select title='Zahlungsart' style='width:100px;' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][PAYMENT_ID]">
																<option value=""></option>
															{foreach from=$PLA.PAYMENT.D key="kPAY" item="PAY"}
																<option value='{$kPAY}' {if $INC.PAYMENT_ID == $kPAY}selected{/if}>{$PAY.TITLE}</option>
															{/foreach}
														</select>
													</td>
												</tr>
												<tr>
													<td>Paid-Datum:</td>
													<td><input style='width:100px;' type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DATE_PAID]" value='{if $INC.DATE_PAID}{$INC.DATE_PAID}{else}{$smarty.now|date_format: "%Y-%m-%d"}{/if}'></td>
												</tr>
											</table>
										</td>
{*
										<td style='background:#eee;width:150px;'>
											<input style='width:140px;' placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][COMPANY]" value='{$INC.BILLING.COMPANY}'><br>
											<input style='width:70px;' placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][FNAME]" value='{$INC.BILLING.FNAME}'><input style='width:70px;' placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][BILLING][NAME]" value='{$INC.BILLING.NAME}'><br>
											<input style='width:100px;' placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][STREET]" value='{$INC.BILLING.STREET}'><input style='width:40px;' placeholder="Straßen Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][BILLING][STREET_NO]" value='{$INC.BILLING.STREET_NO}'><br>
											<input style='width:140px;' placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][ADDITION]" value='{$INC.BILLING.ADDITION}'><br>
											<input style='width:40px;' placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][ZIP]" value='{$INC.BILLING.ZIP}'><input style='width:100px;' placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][BILLING][CITY]" value='{$INC.BILLING.CITY}'><br>
					
											<select style='width:140px;' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][BILLING][COUNTRY_ID]">
												<option value=""></option>
											{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
												<option value='{$kCOU}' {if $INC.BILLING.COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
											{/foreach}
											</select>
										</td>
										<td style='background:#eee;width:150px;'>
											<input style='width:140px;' placeholder="Firma" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][COMPANY]" value='{$INC.DELIVERY.COMPANY}'><br>
											<input style='width:70px;' placeholder="Vorname" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][FNAME]" value='{$INC.DELIVERY.FNAME}'><input style='width:70px;' placeholder="Name" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][DELIVERY][NAME]" value='{$INC.DELIVERY.NAME}'><br>
											<input style='width:100px;' placeholder="Straße" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][STREET]" value='{$INC.DELIVERY.STREET}'><input style='width:40px;' placeholder="Straßen Nummer" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][DELIVERY][STREET_NO]" value='{$INC.DELIVERY.STREET_NO}'><br>
											<input style='width:140px;' placeholder="Zusatz" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][ADDITION]" value='{$INC.DELIVERY.ADDITION}'><br>
											<input style='width:40px;' placeholder="PLZ" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][ZIP]" value='{$INC.DELIVERY.ZIP}'><input style='width:100px;' placeholder="Ort" type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINC}][DELIVERY][CITY]" value='{$INC.DELIVERY.CITY}'><br>
					
											<select style='width:140px;' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][DELIVERY][COUNTRY_ID]">
												<option value=""></option>
											{foreach from=$D.COUNTRY.D key="kCOU" item="COU"}
												<option value='{$kCOU}' {if $INC.DELIVERY.COUNTRY_ID == $kCOU}selected{/if}>{$COU.TITLE}</option>
											{/foreach}
											</select>
										</td>
										<td valign="top">
											Lager:<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][WAREHOUSE_ID]">
											{foreach from=$D.WAREHOUSE.D key="kWAR" item="WAR"}
												{if $WAR.ACTIVE == 1}
												<option value='{$kWAR}' {if $INC.WAREHOUSE_ID == $kWAR}selected{/if}>{$WAR.TITLE}</option>
												{/if}
											{/foreach}
											</select>
											<input type='text'placeholder="Tracking Nummer" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][TRACKING_NO]" value='{$INC.TRACKING_NO}'><br>
											<input type='text' placeholder="Datum" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][SHIPPED_DATE]" value='{if $INC.SHIPPED_DATE}{$INC.SHIPPED_DATE}{/if}'><br>
											<select placeholder="Versandfirma" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][SHIPPING_ID]">
												<option value=""></option>
												{foreach from=$D.SHIPPING.D key="kSHI" item="SHI"}
													<option value='{$kSHI}' {if $INC.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
												{/foreach}
											</select><br>
											Platform: {$D.PLATFORM.D[$INC.PLATFORM_ID].TITLE}
										</td>
*}
										<td valign="top"><textarea name='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][COMMENT]' style='width:200px;height:100px;'>{$INC.COMMENT}</textarea></td>
										<td valign="top">
											<div class="fileupload_{$D.PLATFORM_ID}_{$kINC} list">
											
												<div style="overflow:hidden;">
													<div style="float:right;">
														<span class="btn btn-success fileinput-button">
															<label id="progress" style="display:none;">100%</label><button type="button" class="add">+</button>
															<input id="fileupload" type="file" name="files[]" multiple>
														</span>
													</div>
												</div>
												
												<div style="clear:both;">
													<ul id="file_{$D.PLATFORM_ID}_{$kINC}">
														{foreach from=$INC.FILE.D key="kFIL" item="FIL" name="FIL"}
															<li id="img_li{$D.PLATFORM_ID}{$kINC}{$kFIL}">
																<button class="del" type="button" onclick="if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}{$kINC}{$kFIL}').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D][{$kFIL}][ACTIVE]').value='-2';}">-</button>
																<input type="text" id="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D][{$kFIL}][TITLE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D][{$kFIL}][TITLE]" value="{$FIL.TITLE}">.{$FIL.EXTENDSION}
																<a href="file/{$D.PLATFORM_ID}/{$kFIL}/{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}{*$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.GROUP.D[$INC.GROUP_ID].TITLE*}_{$FIL.TITLE}.{$FIL.EXTENDSION}" target='_blank'>{$FIL.TITLE}.{$FIL.EXTENDSION}</a>
																<input type="hidden" id="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D][{$kFIL}][ACTIVE]" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D][{$kFIL}][ACTIVE]" value="{$FIL.ACTIVE}">
															</li>
														{/foreach}
													</ul>
												</div>
												
											</div>
											<script>
												$(function () {
													'use strict';
													$('.fileupload_{$D.PLATFORM_ID}_{$kINC}').fileupload({
														dropZone: $('.fileupload_{$D.PLATFORM_ID}_{$kINC}'),
														maxFileSize: 5000000,
														url: '?D[PAGE]=platform.incominginvoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=upload',
														dataType: 'json',
														done: function (e, data) {
															//console.log(data);
															html = ""
															+"<li id='img_li{$D.PLATFORM_ID}{$kINC}"+data.result.file_id+"'>"
															+"	<button class='del' type='button' onclick=\"if(confirm('Wirklich löschen?')){ $('#img_li{$D.PLATFORM_ID}{$kINC}"+data.result.file_id+"').hide(); document.getElementById('D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D]["+data.result.file_id+"][ACTIVE]').value='-2';}\">-</button>"
															+"	<input type='text' id='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D]["+data.result.file_id+"][TITLE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D]["+data.result.file_id+"][TITLE]' value='"+data.result.file_title+"'>"
															+"	<a href=\"file/{$D.PLATFORM_ID}/"+data.result.file_name+"/{$INC.DATE|date_format:"%Y.%m.%d"}_"+data.result.file_title+"\" target='_blank'>"+data.result.file_title+"</a>"
															+"	<input type='hidden' id='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D]["+data.result.file_id+"][ACTIVE]' name='D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D][{$kINC}][FILE][D]["+data.result.file_id+"][ACTIVE]' value='1'>"
															+"</li>";
															$('#file_{$D.PLATFORM_ID}_{$kINC}').append(html);

															$('#progress').hide();
														},
														progressall: function (e, data) {
															var progress = parseInt(data.loaded / data.total * 100, 10);
															$('#progress').show();
															$('#progress').text(progress+'%');
														}
													}).prop('disabled', !$.support.fileInput)
														.parent().addClass($.support.fileInput ? undefined : 'disabled');
												});
											 </script>
										</td>
									</tr>
								</table>
								{*
								<div style="background:#eee;height:20px;padding:2px;width:240px;"><input title="Suche in:<br>Nummer<br>EAN<br>Title<br><br>Platzhalter:<br>* für mehrere Buchstaben<br>_ für einen Buchstaben" id="tbSearch{$kINC|replace:'-':''}"><button onclick="wp.ajax({ 'div' : 'search{$kINC|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.invoice&D[ACTION]=search_article&D[INVOICE_ID]={$kINC}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][NUMBER:LIKE|EAN:LIKE|TITLE:LIKE]='+$('#tbSearch{$kINC|replace:'-':''}').val()+'&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}'});document.getElementById('search{$kINC|replace:'-':''}').style.display = '';" type="button">Suchen</button></div>
								<div id='search{$kINC|replace:'-':''}' style="position:absolute;min-height:100px;background:#fff; max-height:300px;overflow-y:scroll;border:solid 1px #ddd;display:none;"></div>
								*}
								<table class='table'>
									<thead>
										<tr>
											<td style="width:50px;">
												<button class="btn" title="leere Position" type="button" onclick="new_article('{$kINC}');">+</button>
												<button class="btn" title="artikel suchen" type="button" onclick="wp.window.open({ 'ID' : 'ARTSEARCH', 'TITLE' : 'Artikel Suche' , 'WIDTH' : '600px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=popup.search&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][SUPPLIER_ID:IN]=2&D[DONE]=new_article(\'{$kINC}\',2,ID)'});">+</button>			
											</td>
											<td></td>
											<td style="width:100px;">Nummer</td>
											<td>Title</td>
											<td style="width:60px;">Netto</td>
											<td style="width:60px;">MwSt</td>
											<td style="width:60px;">Brutto</td>
											<td style="width:60px;">Stück</td>
										</tr>
									</thead>
									<tbody id="art_list{$kINC|replace:'-':''}">
									{foreach from=$INC.ARTICLE.D key="kART" item="ART"}
										{$D['ACTION'] = 'load_article'}
										{$D['INCOMINGINVOICE_ID'] = $kINC}
										{$D['ARTICLE_ID'] = $kART}
										
										{include file="platform.incominginvoice.tpl" D=$D}

									{/foreach}
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td style="text-align:right;">{($INC.ARTICLE.PRICE-$INC.ARTICLE.VAT)|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INC.ARTICLE.VAT|number_format:2:".":""}</td>
											<td style="text-align:right;">{$INC.ARTICLE.PRICE|number_format:2:".":""}</td>
											<td></td>
										</tr>
									</tfoot>
								</table>

							</form>
						</td>
					<td valign="top">
							<script>
								new_article = function(IID,SID,ArtID)
								{
									id = (ArtID)?ArtID:wp.get_genID();
									wp.ajax({ 'div' : 'art_list'+IID, INSERT : 'append', 'url' : '?D[PAGE]=platform.incominginvoice&D[ACTION]=load_article&D[ARTICLE_ID]='+id+'&D[SUPPLIER_ID]='+SID+'&D[INCOMINGINVOICE_ID]={$kINC}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});
									/*
									$('#art_list'+IID).append("<td id='new{$kINC|replace:'-':''}' colspan='9'><input onclick=\"" 
									+"wp.ajax({ 'div' : 'search{$kINC|replace:'-':''}', INSERT : 'replace', 'url' : '?D[PAGE]=platform.incominginvoice&D[ACTION]=search_article&D[INCOMINGINVOICE_ID]={$kINC}&D[PLATFORM][D][{$D.PLATFORM_ID}][ARTICLE][W][TITLE]='+this.value+'&D[INCOMINGINVOICE_ID]={$kINC|replace:'-':''}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});"
									+"\" ><div id='search"+IID+"'></div></td>");
									*/
								}
							</script>
						{*MESSAGE START ================*}
						{*
								<div style="height:300px;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINC}"></div>
								<script>
									wp.ajax({
										'url'	:	'?D[PAGE]=message',
										'data'	:	{
														'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[MESSAGE][W][GROUP_ID]' : 'inc{$kINC}', //group_id,
														'D[MESSAGE][O][DATETIME]' : 'DESC'
													},
										'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINC}'
										});
								</script>
						*}
						{*MESSAGE END ================*}
					</td>
				</tr>
			</table>
				
			<script>
				SAVE{$kINC|replace:"-":"_"} = function()
				{
					wp.ajax({
					'url' : '?D[PAGE]=platform.incominginvoice&D[ACTION]=set_incominginvoice&D[PLATFORM_ID]={$D.PLATFORM_ID}',
					'div' : 'ajax',
					'data': $('#form{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$kINC}').serialize()
					});
				}
			</script>
		{/foreach}
{/switch}