{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<div style="overflow-y:scroll;">

	<div id="tab{$D.PLATFORM_ID}TimeLine">
		<ul>
			{$now_y = $smarty.now|date_format:"%Y"}
			{for $y=2012 to $now_y}
				<li onclick="wp.ajax({
									'url'	:	'?D[PAGE]=platform.incominginvoice_list',
									'data'	:	{
													'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
													'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][W][DATE_PAID:LIKE]' : '{$y}-01%'
												},
									'div'	:	'fPLA{$D.PLATFORM_ID}'
									})"><a href="#tab{$D.PLATFORM_ID}TimeLine-{$y}">{$y}</a></li>
			{/for}
		</ul>
		{$y = $PLA.INCOMINGINVOICE.W['DATE_PAID:LIKE']|truncate:4:''}
		{for $i=1 to 12}
			<button class="btn" {if  ($PLA.INCOMINGINVOICE.W['DATE_PAID:LIKE']|replace:"%":''|date_format: "%m") == $i}class="active"{/if} onclick="wp.ajax({
						'url'	:	'?D[PAGE]=platform.incominginvoice_list',
						'data'	:	{
										'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
										'D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][W][DATE_PAID:LIKE]' : '{$y}-{if $i < 10}0{/if}{$i}%'
									},
						'div'	:	'fPLA{$D.PLATFORM_ID}'
						})">{$i}</button>
		{/for}
	

		<form id="frm{$D.PLATFORM_ID}">
			<table class='table'>
				<thead>
					<tr>
						<td style="width:30px;"><button type="button" class="btn" onclick="id = wp.get_genID(); wp.window.open({ 'ID' : id, 'TITLE' : 'Neue Rechnung' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.incominginvoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][W][ID:IN]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][D]['+id+'][ACTIVE]=1'});">+</button></td>
						<td style="width:100px;">ID</td>
						<td style="width:60px;">Status</td>
						<td style="width:60px;">Nummer</td>
						<td style="width:60px;">Typ</td>
						<td style="width:60px;">Paid Date</td>
						<td style="width:60px;">Paid Typ</td>
						<td>Kommentar</td>
						<td style="width:100px;">Datei</td>
						<td style="width:60px;">Netto</td>
						<td style="width:60px;">MwSt</td>
						<td style="width:60px;">Brutto</td>
						<td style="width:60px;">EMail</td>
					</tr>
				</thead>
				<tbody>
					{foreach from=$PLA.INCOMINGINVOICE.D key="kINC" item="INC"}
					<tr>
						<td></td>
						<td><button class="btn" type="button" onclick="wp.window.open({ 'ID' : '{$kINC}', 'TITLE' : '{$kINC}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.incominginvoice&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][W][ID:IN]={$kINC}&D[PLATFORM][D][{$D.PLATFORM_ID}][INCOMINGINVOICE][ACTIVE]'});">{$kINC}</button></td>
						<td>{$INC.STATUS}</td>
						<td>{$INC.NUMBER}</td>
						<td>{$PLA.GROUP.D[$INC.GROUP_ID].TITLE}</td>
						<td style="text-align:center;">{$INC.DATE_PAID|date_format:"%d.%m.%Y"}</td>
						<td>{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}</td>
						<td>{$INC.COMMENT}</td>
						<td style="{if !$INC.FILE.D && $INC.STATUS != 40}background:#f99;{/if}">
							<ul>
								{foreach from=$INC.FILE.D key="kFIL" item="FIL"}
									<input type="hidden" name="D[ZIP][{$kFILE}][URL]" value="https://{$D.SESSION.ACCOUNT_ID}.hexcon.de/file/{$D.PLATFORM_ID}/{$kFIL}/{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}_{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.GROUP.D[$INC.GROUP_ID].TITLE}_{$FIL.TITLE}.{$FIL.EXTENDSION}">
									<li><a href="file/{$D.PLATFORM_ID}/{$kFIL}/{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}{*$PLA.GROUP.D[$INC.GROUP_ID].TITLE*}_{$FIL.TITLE}.{$FIL.EXTENDSION}" target='_blank'>{$FIL.TITLE}.{$FIL.EXTENDSION}</a></li>
									{$_countFile[$FIL.EXTENDSION] = $_countFile[$FIL.EXTENDSION]+1}
								{/foreach}
							</ul>
						</td>
						<td style="text-align:right;">{($INC.ARTICLE.PRICE - $INC.ARTICLE.VAT)|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$INC.ARTICLE.VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{($INC.ARTICLE.PRICE)|number_format:2:",":"."}</td>
						{$SUM_NET = $SUM_NET + ($INC.ARTICLE.PRICE - $INC.ARTICLE.VAT)}
						{$SUM_VAT = $SUM_VAT + $INC.ARTICLE.VAT}
						{$SUM_BRT = $SUM_BRT + $INC.ARTICLE.PRICE}
						<td>
						<a href="{strip}mailto:an@domain.de?subject=Order&amp;body=Hello,%0D%0A
						{foreach from=$INC.ARTICLE.D key="kART" item="ART"}
							{$ART.STOCK}x%20{$ART.NUMBER}%20-%20{$ART.TITLE}%0D%0A
						{/foreach}{/strip}">E-Mail</a>
						</td>
					</tr>
					{/foreach}
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td title="
						{foreach from=$_countFile key="kFIL" item="FIL"}
							{$kFIL}:{$FIL} |
						{/foreach}
						"><button class="btn" type="button" onclick="wp.ajax({
							'url'	:	'?D[PAGE]=platform.incominginvoice_list&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[ACTION]=download',
							'data'	:	$('#frm{$D.PLATFORM_ID}').serialize(),
							'div'	:	'ajax'
							});">erstellen</button> <a target="_blank" href="tmp/{$D.PLATFORM_ID}_download.zip">download</a></td>
						<td style="text-align:right;">{$SUM_NET|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$SUM_VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$SUM_BRT|number_format:2:",":"."}</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</form>
		<script>
				$(function() {
					var index = $('#tab{$D.PLATFORM_ID}TimeLine a[href="#tab{$D.PLATFORM_ID}TimeLine-{$PLA.INCOMINGINVOICE.W['DATE_PAID:LIKE']|truncate:4:''}"]').parent().index();
					//var index = $("#tab{$D.PLATFORM_ID}TimeLine>ul").index( $("#tab{$D.PLATFORM_ID}TimeLine-2014") );
					//alert(index);
					$( "#tab{$D.PLATFORM_ID}TimeLine" ).tabs({ active: index });
				});
				</script>
	</div>
</div>