{*assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]*}
{switch $D.ACTION}
	{case 'get_message'}
		<table>
			<tr>
				<td style="width:600px;" valign="top">
						<div>
							<table>
								{if $D.CUSTOMER.D[$D.MESSAGE.W.GROUP_ID].NAME}
								<tr>
									<td>Vorname, Name:</td>
									<td>{$D.CUSTOMER.D[$D.MESSAGE.W.GROUP_ID].FNAME}, {$D.CUSTOMER.D[$D.MESSAGE.W.GROUP_ID].NAME}</td>
								</tr>
								{/if}
							</table>
						</div>

						{*MESSAGE START ================*}
							<div style="height:600px;" id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$D.MESSAGE.W.GROUP_ID}"></div>
								<script>
									wp.ajax({
														'url'	:	'?D[PAGE]=message',
														'data'	:	{
																		'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
																		'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
																		'D[MESSAGE][W][GROUP_ID]' : '{$D.MESSAGE.W.GROUP_ID}', //group_id,
																		'D[MESSAGE][O][DATETIME]' : 'DESC'
																	},
														'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}{$D.MESSAGE.W.GROUP_ID}'
														});
								</script>
						{*MESSAGE END ================*}
				</td>
				<td valign="top">
					<div style="height:600px;overflow-y:scroll;">
						<table class='list' style='width:100%;'>
							<thead>
								<tr>
									<td style="width:30px;"></td>
									<td style="width:60px;">Nummer</td>
									<td style="width:60px;">Datum</td>
									<td style="width:60px;">Status</td>
									<td style="width:60px;">Nickname</td>
									<td>Rechnungsadresse</td>
									<td>Lieferadresse</td>
									<td style="width:150px;">Versand</td>
									<td style="width:150px;">Kommentar</td>
								</tr>
							</thead>
							<tbody>
								{foreach from=$D.INVOICE.D key="kINV" item="INV"}
									<tr >
										<td></td>
										<td style="text-align:right;">{$INV.NUMBER}</td>
										<td style="text-align:center;">{$INV.DATE|date_format:"%d.%m.%Y"}</td>
										<td style="text-align:ceter;background:{if $INV.STATUS >=40}green{else if $INV.STATUS >=20}yellow{else}red{/if}">
											<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][STATUS]">
												<option value='0' {if $INV.STATUS ==0}selected{/if}>offen</option>
												<option value='20' {if $INV.STATUS ==20}selected{/if}>Versandfreigabe</option>
												<option value='40' {if $INV.STATUS ==40}selected{/if}>Fertig</option>
												</select>
												</td>
										<td style="text-align:center;">{$INV.CUSTOMER_NICKNAME}</td>
										<td style="background:#fff;font-size:13px;">
											{$INV.BILLING.COMPANY}
											{$INV.BILLING.FNAME}, {$INV.BILLING.NAME}<br>
											{$INV.BILLING.STREET}, {$INV.BILLING.STREET_NO}<br>
											{if $INV.BILLING.ADDITION}{$INV.BILLING.ADDITION}<br>{/if}
											{$INV.BILLING.ZIP}, {$INV.BILLING.CITY}<br>
											{$D.COUNTRY.D[$INV.BILLING.COUNTRY_ID].TITLE}
										</td>
										<td style="background:#fff;font-size:13px;">{$INV.DELIVERY.COMPANY}
											{$INV.DELIVERY.FNAME}, {$INV.DELIVERY.NAME}<br>
											{$INV.DELIVERY.STREET}, {$INV.DELIVERY.STREET_NO}<br>
											{if $INV.DELIVERY.ADDITION}{$INV.DELIVERY.ADDITION}<br>{/if}
											{$INV.DELIVERY.ZIP}, {$INV.DELIVERY.CITY}<br>
											{$D.COUNTRY.D[$INV.DELIVERY.COUNTRY_ID].TITLE}
											</td>
										<td style="text-align:right;">
											</select>
											Traking:<input type='text' style="{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}background:yellow;{/if}" name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][TRACKING_NO]" value='{$INV.TRACKING_NO}'>{if $INV.ARTICLE.PRICE > 25 || $INV.DELIVERY.COUNTRY_ID <> 'DE'}<-mit Tracking !!!{/if}<br>
											Datum:<input type='text' name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPED_DATE]" value='{$smarty.now|date_format:"%Y-%m-%d"}'><br>
											Versandfirma:<select name="D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][SHIPPING_ID]">
												<option value=""></option>
												{foreach from=$PLA.SHIPPING.D key="kSHI" item="SHI"}
													<option value='{$kSHI}' {if $INV.SHIPPING_ID == $kSHI}selected{/if}>{$SHI.TITLE}</option>
												{/foreach}
											</select>
						
										</td>
										<td style="text-align:right;"><textarea name='D[PLATFORM][D][{$D.PLATFORM_ID}][INVOICE][D][{$kINV}][COMMENT]' style='width:200px;height:100px;'>{$INV.COMMENT}</textarea></td>
									</tr>
									<tr>
										<td colspan="10">
											<table style="width:100%;">
												<tr>
													<td style="width:50px;">ANZ</td>
													<td style="width:50px;">Bild</td>
													<td style="width:50px;">NR</td>
													<td>Title</td>
												</tr>
											{foreach from=$INV.ARTICLE.D key="kART" item="ART"}
												<tr>
													<td style="text-align:center;font-size:25px;">{$ART.STOCK}</td>
													<td title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_400x400.jpg'>"><img src="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kART}_0_50x50.jpg"></td>
													<td style="font-size:15px;">{$ART.NUMBER}</td>
													<td style="font-size:15px;">{$ART.TITLE}</td>
												
												</tr>
											{/foreach}

											</table>
										</td>
									</tr>
									<tr>
										<td colspan="10" style="border-top:5px red solid;">&nbsp;</td>
									</tr>
								{/foreach}
				
							</tbody>
						</table>


					{foreach from=$D.INVOICE.D key="kINV" item="INV"}
						<div>
						
						</div>
					{/foreach}
					</div>
				</td>
			</tr>
		</table>

	{/case}
	{default}

		<table style="width:100%;">
			<tr>
				<td style="width:250px;">Kunde</td>
				<td>Nachricht</td>
			</tr>
			<tr>
				<td valign="top" style="border-right:1px solid #ddd;">
					<div style="height:600px;overflow-y:scroll;">
					<table class="list">
						<thead>
							<tr>
								<td>Pl.</td>
								<td style="width:150px;">Nickname</td>
								<td style="text-align:right;">PN</td>
								<td style="text-align:right;">Inv.</td>
							</tr>
						</thead>
						<tbody>
							{foreach from=$D.CUSTOMER.D key="kCUS" item="CUS"}
								{if $CUS.COUNT_MESSAGE || $CUS.COUNT_INVOICE}
								<tr onclick="get_message('{$kCUS}','{$CUS.MESSAGE_GROUP_ID}');" style="cursor:pointer;">
									<td>{$D.PLATFORM.D[$CUS.PLATFORM_ID].TITLE}</td>
									<td>{$CUS.NICKNAME}</td>
									<td style="text-align:right;">{if $CUS.COUNT_MESSAGE}{$CUS.COUNT_MESSAGE}{/if}</td>
									<td style="text-align:right;">{if $CUS.COUNT_INVOICE}{$CUS.COUNT_INVOICE}{/if}</td>
								</tr>
								{/if}
							{/foreach}
						</tbody>
					</table>
					</div>
				</td>
				<td id="fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}" valign="top"></td>
			</tr>
		</table>
		<script>
		get_message = function(customer_id,group_id)
		{
		wp.ajax({
							'url'	:	'?D[PAGE]=support.message',
							'data'	:	{
											'D[ACCOUNT_ID]' : '{$D.SESSION.ACCOUNT_ID}',
											//'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
											'D[MESSAGE][W][GROUP_ID]' : group_id,
											'D[MESSAGE][O][DATETIME]' : 'DESC',
											'D[INVOICE][W][CUSTOMER_ID]' : customer_id,
											'D[ACTION]' : 'get_message'
										},
							'div'	:	'fmessage{$D.SESSION.ACCOUNT_ID}{$D.PLATFORM_ID}',
							'done'	:	function(){  }
							});
		}
		</script>
{/switch}