{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
{switch $D.ACTION}
	{case 'get_feed'}
		{foreach from=$PLA.FEED.D key="kFEED" item="FEED"}
			<table class="table">
				<thead>
					<tr>
						<th style="width:100px;">ID</th>
						<th>{$kFEED}</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Active</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][ACTIVE]", 'type'=>'checkbox', 'option' => [1,0], 'value'=>$FEED.ACTIVE ]}</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][NAME]", 'value'=>$FEED.NAME ]}</td>
					</tr>
					<tr>
						<td>Filename</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][FILENAME]", 'value'=>$FEED.FILENAME ]}</td>
					</tr>
					<tr>
						<td>File Type</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][FILETYPE]", 'type'=>'select', 'option'=>['html', 'xml', 'txt', 'pdf', 'csv', 'io', 'task'], 'value'=>$FEED.FILETYPE ]}</td>
					</tr>
					<tr>
						<td>Key</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][KEY]", 'value'=>$FEED.KEY ]}</td>
					</tr>
					<tr>
						<td>Code</td>
						<td>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D][{$kFEED}][TEXT]", "type"=>"textarea", "style" => "height:300px;", 'value'=>$FEED.TEXT ]}</td>
					</tr>
					<tr>
						<td>Feed Link</td>
						<td><a href="{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEED}/{$FEED.KEY}/{$FEED.FILENAME}.{$FEED.FILETYPE}" target="_blank">{$CWP->base_url()}/file/{$D.PLATFORM_ID}/{$kFEED}/{$FEED.KEY}/{$FEED.NAME}.{$FEED.FILETYPE}</a></td>
					</tr>
				
					<tr>
						<td>Cronjob</td>
						<td>
						
							<table class="table">
								<thead>
									<tr>
										<th>M/H</th>
										{for $H=0 to 23}
										<th><center>{$H}</center></th>
										{/for}
									</tr>
								</thead>
								<tbody>
									{for $M=0 to 3}
									<tr>
										<td><center>{$M*15}</center></td>
										{for $H=0 to 23}
										<td style="background:{if $D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].FAIL > 3}red{elseif $D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].FAIL > 0}yellow{elseif isset($D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].FAIL) && date('Ymd',strtotime($D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].UTIMESTAMP)) == date('Ymd')}#5bcc46{/if}"><center>{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][TASK][D][{$kFEED}{$H}{$M}][ACTIVE]", 'value'=>(($D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].ACTIVE)?$D.PLATFORM.D[$D.PLATFORM_ID].TASK.D["{$kFEED}{$H}{$M}"].ACTIVE:-2), 'type'=>'checkbox', 'option' => [1,-2] ]}</center>
											{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][TASK][D][{$kFEED}{$H}{$M}][START_TIME]", 'value'=>"{$H}{str_pad($M*15,2,0,STR_PAD_LEFT)}", 'type'=>'hidden', 'ischanged' => 1 ]}
											{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][TASK][D][{$kFEED}{$H}{$M}][PLATFORM_ID]", 'value'=>$D.PLATFORM_ID, 'type'=>'hidden', 'ischanged' => 1 ]}
											{input p=['name' => "D[PLATFORM][D][{$D.PLATFORM_ID}][TASK][D][{$kFEED}{$H}{$M}][FEED_ID]", 'value'=>$kFEED, 'type'=>'hidden', 'ischanged' => 1 ]}
										</td>
										{/for}
									</tr>
									{/for}
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td>Cronjob Link</td>
						<td>https://[domain.de]/cron.php</td>
					</tr>
				</tbody>
			</table>
			
			
			
		{/foreach}
	{/case}
	{default}
<form id="form{$D.PLATFORM_ID}FEED" name="form{$D.PLATFORM_ID}FEED" method="post">
	
	<div class="container">
		<div class="row">
			<div class="col-md-auto p-0">
			
				<ul class="nav flex-column">
					{foreach from=$PLA.FEED.D key="kFEED" item="FEED"}
					<li class="nav-item nav-pills">
						<a class="nav-link xactive" href="#" onclick="
							wp.ajax({
										'url'	:	'?D[PAGE]=platform.setting.feed&D[ACTION]=get_feed',
										'data'	:	{
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][W][ID:IN]' : '{$kFEED}'
													},
										'div'	:	'conFeed{$D.PLATFORM_ID}','ASYNC':1
									});return false;

						">{if $FEED.ACTIVE}<b>{/if} {$FEED.NAME}{if $FEED.ACTIVE}</b>{/if}</a>
					</li>
					{/foreach}
					<li class="nav-item nav-pills">
						<a class="nav-link xactive" href="#" onclick="
							var newID = wp.get_genID();
							wp.ajax({
										'url'	:	'?D[PAGE]=platform.setting.feed&D[ACTION]=get_feed&D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][D]['+newID+'][ACTIVE]',
										'data'	:	{
														'D[PLATFORM_ID]' : '{$D.PLATFORM_ID}',
														'D[PLATFORM][D][{$D.PLATFORM_ID}][FEED][W][ID:IN]' : newID
													},
										'div'	:	'conFeed{$D.PLATFORM_ID}','ASYNC':1
									});return false;

						">NEU</a>
					</li>
				</ul>

			</div>
			<div id="conFeed{$D.PLATFORM_ID}" class="col col-lg p-0">
		
			</div>
		</div>
	</div>
</form>
{/switch}