

<div class="tree">
	<ul>
	{foreach from=$TREE key="k" item="CAT"}
		<li style="margin-left:15px;">
			<nobr>
				{*<ins id="ins{$k}{$ID}" class="fa {if $CAT.COUNT}plus{else}end{/if}"  onclick="get_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');"></ins>*}
				
				<div class="node" oncontextmenu="wp.contextmenu.open({ ID:'{$kART}', MENU: [ { TITLE: '<i class=\'fa fa-folder-open-o\'></i> <b>Öffnen</b>', ONCLICK : 'get_tree_object(\'{$ID}\', \'{$k}\', \'{$CAT.COUNT}\');' },{ CLASS: 'rename', TITLE: 'Umbennenen', ONCLICK : 'rename_tree_object(\'{$ID}\', \'{$k}\', \'{$CAT.COUNT}\');' }, {if !$CAT.ARTICLE.COUNT}{ CLASS: 'new', TITLE: '<i class=\'fa fa-plus-circle\'></i> Neu Ordner', ONCLICK : 'new_tree_object(\'{$ID}\', \'{$k}\', \'{$CAT.COUNT}\');' }, {if !$CAT.COUNT}{ CLASS: 'del', TITLE: 'Löschen', ONCLICK : 'remove_tree_object(\'{$ID}\', \'{$k}\', \'{$CAT.COUNT}\');' },{/if}{/if} { CLASS: 'setting', TITLE: '<i class=\'fa fa-cog\'></i> Einstellungen', ONCLICK : 'wp.window.open({ \'ID\' : \'{md5("CAT{$D.PLATFORM_ID}{$k}")}\', \'TITLE\' : \'Kategorie {$CAT.LANGUAGE.D.DE.TITLE} Einstellungen\' , \'WIDTH\' : \'920px\', \'HEIGHT\' : \'620px\', \'URL\' : \'?D[PAGE]=platform.setting.categorie&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][W][ID]={$k}&D[PLATFORM_ID]={$D.PLATFORM_ID}\'});' } ] });">
					<div class="title" onclick="get_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');">
					{*if $CAT.COUNT}&#xf078;{else}&#xf054;{/if*} <i class="fa fa-folder{if !$CAT.ARTICLE.COUNT && $CAT.COUNT == 0}-o{/if}" style="{if $CAT.ARTICLE.COUNT}color:orange;{/if}"></i>
					{$CAT.LANGUAGE.D.DE.TITLE} 
					</div>
					<div style="float:right;background:#fff;position:relative;padding-left:1px;">{if $CAT.ARTICLE.COUNT}{$CAT.ARTICLE.COUNT}{/if}
					
						<div class="btn-group">
							<button class="btn btn-xs dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								&#xf1de;
							</button>
							<div class="dropdown-menu">
								<ul>
									<li onclick="get_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');" class="dropdown-item">&#xf115; Öffnen</li>
									<li onclick="rename_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');" class="dropdown-item"> Umbennenen</li>
									{if !$CAT.ARTICLE.COUNT}<li onclick="new_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');" class="dropdown-item">&#xf055; Neuer Ordner</li>{/if}
									{if !$CAT.COUNT && !$CAT.ARTICLE.COUNT}<li onclick="remove_tree_object('{$ID}', '{$k}', '{$CAT.COUNT}');" class="dropdown-item">&#xf056; Löschen</li>{/if}
									<li onclick="wp.window.open({ 'ID' : '{md5("CAT{$D.PLATFORM_ID}{$k}")}', 'TITLE' : 'Kategorie {$CAT.LANGUAGE.D.DE.TITLE} Einstellungen' , 'WIDTH' : '920px', 'HEIGHT' : '620px', 'URL' : '?D[PAGE]=platform.setting.categorie&D[PLATFORM][D][{$D.PLATFORM_ID}][CATEGORIE][W][ID]={$k}&D[PLATFORM_ID]={$D.PLATFORM_ID}'});" class="dropdown-item">&#xf1de; Einstellungen</li>
								</ul>
							</div>
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
				<ul id="{$ID}{$k}" style="clear:both;display:none;"></ul>
			</nobr>
		</li>
	{/foreach}
	</ul>
</div>