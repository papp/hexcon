{*ToDo: input_att ist ein Sonderfall bei dem Active ggf auf -2 gesetzt wird wenn leer. Soll spätter zur input angepasst werden*}
{function name=input_att p=null}
	<input group="ATT_ACTIVE{$D.PLATFORM_ID}{$kART}{$kATT}" type="hidden" id="id{md5($p.name)}ACTIVE" name="{$p.name}[ACTIVE]" value="{if $p.value|@strlen > 0 }1{else}-2{/if}">
	{switch $p.type}
		{case 'file'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				 <div class="input-group-prepend">
					<span class="input-group-text" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$p.value}_100x100.jpg'>" style="background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$p.value}_30x30.jpg) center center no-repeat;height:30px;width:30px;"></span>
				</div>
				<input class="form-control" onkeyup="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" name="{$p.name}[VALUE]" value="{$p.value}">
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
		{/case}
		{case 'multiselect'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				<input id="{md5($p.name)}VALUE" value="{$p.value}" name="{$p.name}[VALUE]" type="hidden">
				<select multiple class="form-control" ischanged="0" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" onchange="$('#{md5($p.name)}VALUE').val( $(this).val() );$('#{md5($p.name)}VALUE').val($('#{md5($p.name)}VALUE').val().replace(/,/g, '|'));document.getElementById('id{md5($p.name)}ACTIVE').value = ($('#{md5($p.name)}VALUE').val().length >0)?1:-2;" >
					<option value="">---</option>
					{$FIND = 0}
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
						{if $value}
							{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
								<optgroup label="{$value}">
							{else}
								<option {if strpos("|{$p.value}|","|{$value}|") !== false}selected{/if}>{$value}</option>
							{/if}
							{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
								</optgroup>
							{/if}
						{/if}
					{/foreach}
				</select>
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
		{/case}
		{case 'select'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				<select class="form-control" ischanged="0" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');"  onchange="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" name="{$p.name}[VALUE]">
					<option value="">---</option>
					{$FIND = 0}
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							<optgroup label="{$value}">
						{else}
							<option {if $value == $p.value}selected{/if}>{$value}</option>
							{if $value == $p.value}{$FIND = 1}{/if}
						{/if}
						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							</optgroup>
						{/if}
					{/foreach}
					{if !$FIND && $p.value != ''}
						<option style="color:red;background:#fcc;" value="{$p.value}" selected>{$p.value}</option>
					{/if}
				</select>
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
		{/case}
		{case 'textarea'}
			<div class="input-group mb-0">
				<textarea id="{$p.name}[VALUE]" ischanged="0" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} placeholder="{$p.placeholder}" class="form-control" onkeyup="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" name="{$p.name}[VALUE]">{$p.value}</textarea>
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
			{/case}
		{case 'wysiwyg'}
		{*https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand*}
			<div style="background:#eee;">
				<div class="btn-group">
					<button type="button" class="btn"  onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('undo', false, null);"><i class="fa fa-undo" ></i></button>
					<button type="button" class="btn"  onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('redo', false, null);"><i class="fa fa-repeat" ></i></button>
				</div>
				
				<div class="btn-group">
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('bold', false, null);"><i class="fa fa-bold" ></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('italic', false, null);"><i class="fa fa-italic" ></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('underline', false, null);"><i class="fa fa-underline"></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('strikeThrough', false, null);"><i class="fa fa-strikethrough"></i></button>
				</div>
				
				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-align-left'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('justifyLeft', false, null);"><i class='fa fa-align-left'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('justifyCenter', false, null);"><i class='fa fa-align-center'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('justifyRight', false, null);"><i class='fa fa-align-right'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('justifyFull', false, null);"><i class='fa fa-align-justify'></i></button>
					</div>
				</div>

				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-indent'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('indent', false, null);"><i class='fa fa-indent'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('outdent', false, null);"><i class='fa fa-outdent'></i></button>
					</div>
				</div>

				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-list-ul'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('insertUnorderedList', false, null);"><i class='fa fa-list-ul'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('insertOrderedList', false, null);"><i class='fa fa-list-ol'></i></button>
					</div>
				</div>

				
				<button type="button" class="btn" title="Formatierung entfernen" onclick="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.execCommand('removeFormat', false, null);"><i class='fa fa-eraser'></i></button>

				<div class="btn-group">
					<button type="button" class="btn" style="height:max-content;padding:0 auto;"  onclick="$('.preview').toggle();$('.code').toggle();"><i class="fa fa-code"></i></button>
				</div>

				{if $p.help}
				<span class="btn" title="{$p.help}">&#xf128;</span>
				{/if}
			</div>
			<div class="resizable" style="padding:0px;margin:0px;height:100px;position:relative;">
				
				<iframe style="min-height:100px;height:100%;display:;width:100%;overflow:auto;" width="100%" height="100%" scrolling="auto" id="{$p.name}[VALUE]edit" class="form-control code" onload="this.contentWindow.document.write('<body style=\'background:#fff;\' contenteditable onkeyup=&quot;parent.document.getElementById(\'{$p.name}[VALUE]\').value = this.innerHTML;parent.document.getElementById(\'id{md5($p.name)}ACTIVE\').value = (this.innerHTML.length >0)?1:-2;&quot;>{str_replace(['"',"'","\r","\n"],['&quot;',"\\'",'\\r','\\n'],$p.value)}</body>')"></iframe>
				<textarea style="min-height:100px;width:100%;height:100%;display:none;background-color:#ffe;color:blue;" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} class="form-control preview" onkeyup="document.getElementById('{$p.name}[VALUE]edit').contentWindow.document.body.innerHTML = this.value;document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" id="{$p.name}[VALUE]"  name="{$p.name}[VALUE]">{$p.value}</textarea>

			</div>
			<script>
				$( function() {
					$( ".resizable" ).resizable({ minHeight: 100,minWidth: 150});
				} );
			</script>
			{/case}
		{case 'checkbox'}
			<input id="{$p.name}[VALUE]" type="hidden" name="{$p.name}[VALUE]" value="{$p.value}">
			<input onclick="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.checked)?1:-2;document.getElementById('{$p.name}[VALUE]').value = (this.checked)?1:0;" type="checkbox" {if $p.value}checked{/if}>
			{/case}
		{case 'number'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
                <input type="number" {if $p.min != ''}min="{$p.min}"{/if} {if $p.step != ''}step="{$p.step}"{/if}  lang="en-150" ischanged="0" 
				onchange="this.value = parseFloat(this.value.replace(/,/, '.'));{$p.onchange}"
				onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" id="{$p.name}[VALUE]" class="form-control" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} style="text-align:right;" placeholder="{$p.placeholder}" onkeyup="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" name="{$p.name}[VALUE]" value="{$p.value}">
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
			{/case}
		{default}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				
                <input ischanged="0" list="{md5($p.name)}" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" id="{$p.name}[VALUE]" class="form-control" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} {if $p.placeholder}placeholder="{$p.placeholder}"{/if} onkeyup="document.getElementById('id{md5($p.name)}ACTIVE').value = (this.value.length >0)?1:-2;" name="{$p.name}[VALUE]" {if isset($p.value)}value="{$p.value}"{/if}>
				{if $p.option}
				<datalist id="{md5($p.name)}">
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
						<option value="{$value}"></option>
					{/foreach}
				</datalist>
				{/if}
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
	{/switch}

    <script>
    input_onmouseout = function(el,old_Val_md5) 
    {
        if(old_Val_md5 == $.md5(el.value))
        {
            el.style.color = '';
			el.style.backgroundColor = "";
			el.setAttribute('ischanged', '0');
        }
        else
        {
            el.style.color = 'blue';
			el.style.backgroundColor = "#fffedd";
			el.setAttribute('ischanged', '1');
        }
    }
    </script>
{/function}
{*========================================*}
{function name=input p=null}
	{*$p.ischanged = ($p.ischanged == 1)?$p.ischanged:0*}
	{switch $p.type}
		{case 'file'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				 <div class="input-group-prepend">
					<span class="input-group-text" title="<img src='file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$p.value}_100x100.jpg'>" style="background: url(file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$p.value}_30x30.jpg) center center no-repeat;height:30px;width:30px;"></span>
				</div>
				<input class="form-control" name="{$p.name}" value="{$p.value}">
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
			{/case}
		{case 'radio'}
		<section class="section-preview">
			{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
			<div class="custom-control custom-radio">
				<input type="radio" ischanged="0" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" class="custom-control-input" value="{$kVAL}" onclick="{$p.onclick}" {if $kVAL == $p.value}checked{/if} {if $p.group}group="{$p.group}"{/if} id="radio{md5($p.name)}_{$smarty.foreach.nVAL.index}" name="{$p.name}">
				<label class="custom-control-label" for="radio{md5($p.name)}_{$smarty.foreach.nVAL.index}">{$VAL}</label>
			</div>
			{/foreach}
		</section>
		{/case}
		{case 'multiselect'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				<input id="{md5($p.name)}VALUE" value="{$p.value}" name="{$p.name}" type="hidden">
				<select multiple {if $p.readonly}readonly{/if} class="form-control" ischanged="0" 
				{if !$p.readonly}
				onmouseout="input_onmouseout(this,'{md5($p.value)}');" 
				onfocusout="input_onmouseout(this,'{md5($p.value)}');" style="width:100%;" 
				onchange="$('#{md5($p.name)}VALUE').val( $(this).val() );$('#{md5($p.name)}VALUE').val($('#{md5($p.name)}VALUE').val().replace(/,/g, '|'));/*document.getElementById('id{md5($p.name)}ACTIVE').value = ($('#{md5($p.name)}VALUE').val().length >0)?1:-2;*/"
				{else}
				readonly="readonly"
				{/if}>
					<option value="">---</option>
					{$FIND = 0}
					
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						
						
						{*$value = ($p.optionkey)?str_replace("|",'',$p.optionkey[$smarty.foreach.nVAL.index]):$value}
						{$valueValue = ($p.optionvalue)?str_replace("\r",'',$p.optionvalue[$smarty.foreach.nVAL.index]):$value*}
						{$p.value = trim($p.value)}
						{if strpos($p.option[$smarty.foreach.nVAL.index],'|') !== false}
							{$val = explode('|',$p.option[$smarty.foreach.nVAL.index])}
							{$value = $val[1]|replace:"\r":""}
							{$valueKey = trim($val[0])|replace:"\r":""}
						{else}
							{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
							{$valueKey = trim($p.option[$smarty.foreach.nVAL.index])|replace:"\r":""}
						{/if}

						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							<optgroup label="{$value}">
						{else}
							<option {if strpos($p.value, $valueKey) !== false}selected{/if} value="{$valueKey}">{$value}</option>
							{if strpos($p.value, $valueKey) !== false}{$FIND = 1}{/if}
						{/if}
						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							</optgroup>
						{/if}
					{/foreach}
					{if !$FIND && $p.value != ''}
						<option style="color:red;background:#fcc;" value="{trim($p.value)}" selected>{$p.value}</option>
					{/if}
				</select>
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
		{/case}
		{case 'select'}
			<div class="input-group mb-0 {if $p.required}{if !$p.name}has-error{else}has-success{/if}{/if}">
				<select {if $p.readonly}readonly{/if} class="form-control" ischanged="0" 
				{if $p.onchange}onchange="{$p.onchange}"{/if}
				onmouseout="input_onmouseout(this,'{md5($p.value)}');" 
				onfocusout="input_onmouseout(this,'{md5($p.value)}');" style="width:100%;" name="{$p.name}">
					<option value="">---</option>
					{$FIND = 0}
					
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						
						
						{*$value = ($p.optionkey)?str_replace("|",'',$p.optionkey[$smarty.foreach.nVAL.index]):$value}
						{$valueValue = ($p.optionvalue)?str_replace("\r",'',$p.optionvalue[$smarty.foreach.nVAL.index]):$value*}
						{$p.value = trim($p.value)}
						{if strpos($p.option[$smarty.foreach.nVAL.index],'|') !== false}
							{$val = explode('|',$p.option[$smarty.foreach.nVAL.index])}
							{$value = $val[1]|replace:"\r":""}
							{$valueKey = trim($val[0])|replace:"\r":""}
						{else}
							{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
							{$valueKey = trim($p.option[$smarty.foreach.nVAL.index])|replace:"\r":""}
						{/if}

						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							<optgroup label="{$value}">
						{else}
							<option {if $valueKey == $p.value}selected{/if} value="{$valueKey}">{$value}</option>
							{if $valueKey == $p.value}{$FIND = 1}{/if}
						{/if}
						{if $p.option[$smarty.foreach.nVAL.index]|strstr:"["}
							</optgroup>
						{/if}
					{/foreach}
					{if !$FIND && $p.value != ''}
						<option style="color:red;background:#fcc;" value="{$p.value}" selected>{$p.value}</option>
					{/if}
				</select>
				{if $p.help}
				<div class="input-group-append">
					<span class="input-group-text p-1" style="cursor:pointer;" title="{$p.help}">&#xf128;</span>
				</div>
				{/if}
			</div>
		{/case}
		{case 'json'}
		{case 'textarea'}
			<textarea id="{$p.name}" ischanged="0" onmouseout="input_onmouseout(this,'{md5($p.value)}');" onfocusout="input_onmouseout(this,'{md5($p.value)}');" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} placeholder="{$p.placeholder}" class="form-control" style="{$p.style}" name="{$p.name}">{$p.value}</textarea>
		{/case}
		{case 'wysiwyg'}
		{*https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand*}
			<div style="background:#eee;">
				<div class="btn-group">
					<button type="button" class="btn"  onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('undo', false, null);"><i class="fa fa-undo" ></i></button>
					<button type="button" class="btn"  onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('redo', false, null);"><i class="fa fa-repeat" ></i></button>
				</div>
				
				<div class="btn-group">
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('bold', false, null);"><i class="fa fa-bold" ></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('italic', false, null);"><i class="fa fa-italic" ></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('underline', false, null);"><i class="fa fa-underline"></i></button>
					<button type="button" class="btn" style="height:max-content;padding:0 auto;" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('strikeThrough', false, null);"><i class="fa fa-strikethrough"></i></button>
				</div>
				
				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-align-left'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('justifyLeft', false, null);"><i class='fa fa-align-left'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('justifyCenter', false, null);"><i class='fa fa-align-center'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('justifyRight', false, null);"><i class='fa fa-align-right'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('justifyFull', false, null);"><i class='fa fa-align-justify'></i></button>
					</div>
				</div>

				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-indent'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('indent', false, null);"><i class='fa fa-indent'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('outdent', false, null);"><i class='fa fa-outdent'></i></button>
					</div>
				</div>

				<div class="btn-group">
					<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class='fa fa-list-ul'></i>
					</button>
					<div class="dropdown-menu text-center">
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('insertUnorderedList', false, null);"><i class='fa fa-list-ul'></i></button>
						<button type="button" class="btn" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('insertOrderedList', false, null);"><i class='fa fa-list-ol'></i></button>
					</div>
				</div>

				
				<button type="button" class="btn" title="Formatierung entfernen" onclick="document.getElementById('{$p.name}edit').contentWindow.document.execCommand('removeFormat', false, null);"><i class='fa fa-eraser'></i></button>

				<div class="btn-group">
					<button type="button" class="btn" style="height:max-content;padding:0 auto;"  onclick="$('.preview').toggle();$('.code').toggle();"><i class="fa fa-code"></i></button>
				</div>

				{if $p.help}
				<span class="btn" title="{$p.help}">&#xf128;</span>
				{/if}
			</div>
			<div class="resizable" style="padding:0px;margin:0px;height:100px;position:relative;">
				
				<iframe style="min-height:100px;height:100%;display:;width:100%;overflow:auto;" width="100%" height="100%" scrolling="auto" id="{$p.name}edit" class="form-control code" onload="this.contentWindow.document.write('<body style=\'background:#fff;\' contenteditable onkeyup=&quot;parent.document.getElementById(\'{$p.name}\').value = this.innerHTML;&quot;>{str_replace(['"',"'","\r","\n"],['&quot;',"\\'",'\\r','\\n'],$p.value)}</body>')"></iframe>
				<textarea style="min-height:100px;width:100%;height:100%;display:none;background-color:#ffe;color:blue;" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} class="form-control preview" onkeyup="document.getElementById('{$p.name}edit').contentWindow.document.body.innerHTML = this.value; id="{$p.name}"  name="{$p.name}">{$p.value}</textarea>

			</div>
			<script>
				$( function() {
					$( ".resizable" ).resizable({ minHeight: 100,minWidth: 150});
				} );
			</script>
			{/case}
		{case 'checkbox'}{*ToDo: Active Box muss gesetzt werden, auch nicht bewust ob 0/1 oder 1/-2 sein soll*}
			<input id="{$p.name}" type="hidden" name="{$p.name}" value="{$p.value}">
				{if !$p.option}{$p.option = [0 => 1, 1 => 0 ]}{/if}
			<input onclick="{if $p.option}document.getElementById('{$p.name}').value = (this.checked)?'{$p.option[0]}':'{$p.option[1]}';{else}document.getElementById('id{md5($p.name)}ACTIVE').value = (this.checked)?1:-2;document.getElementById('{$p.name}').value = (this.checked)?1:0;{/if}" type="checkbox" {if $p.value && $p.value > 0}checked{/if}>
			{/case}
		{case 'number'}
			<input type="number" ischanged="0" 
			{if $p.step}step="{$p.step}"{/if}
			{if $p.pattern}pattern="{$p.pattern}"{*onkeyup="this.value = this.value.replace(/[A-Za-z]{3}/g, '')"*}{/if}
			{if $p.max}pattern="{$p.max}"{/if}
			{if $p.min}pattern="{$p.min}"{/if}
			{if $p.required}required{/if}
				{if !$p.readonly}
			onmouseout="input_onmouseout(this,'{md5($p.value)}');{$p.onmouseout}" 
			onfocusout="input_onmouseout(this,'{md5($p.value)}');{$p.onfocusout}" 
			onchange="this.value = parseFloat(this.value.replace(/,/, '.'));{$p.onchange}"
			{else}
				readonly="readonly"
			{/if}
			lang="en-150"
			onkeyup="{$p.onkeyup}"
			id="{$p.name}" class="form-control" style="text-align:right;{$p.style}" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} placeholder="{$p.placeholder}" name="{$p.name}" value="{$p.value}">
			<style>
			input[type=number]::-webkit-outer-spin-button,input[type=number]::-webkit-inner-spin-button { -webkit-appearance: none;margin: 0;}
			input[type=number] { -moz-appearance:textfield;}
			</style>
			{/case}
		{case 'label'}
			<label>{$p.value}</label>
		{/case}
		{case 'hidden'}{*Change muss getriggert werden um ischanged zu prüfen: $('#').val('-2').trigger('change');*}
			<input ischanged="{$p.ischanged}" type="hidden" onchange="input_onmouseout(this,'{md5($p.value)}');" id="{if $p.id}{$p.id}{else}{$p.name}{/if}" name="{$p.name}" value="{$p.value}">
			{/case}
		{default}
			<input ischanged="{$p.ischanged}" {if $p.onchange}onchange="{$p.onchange}"{/if} style="{if $p.ischanged}background-color:#fffedd;{/if}" list="{md5($p.name)}" 
			{if !$p.readonly}
				onmouseout="input_onmouseout(this,'{if $p.ischanged}{md5('')}{else}{md5($p.value)}{/if}');" 
				onfocusout="input_onmouseout(this,'{if $p.ischanged}{md5('')}{else}{md5($p.value)}{/if}');" 
			{else}
				readonly="readonly"
			{/if}
			id="{if $p.id}{$p.id}{else}{$p.name}{/if}" class="form-control" style="{$p.style}" {if $p.maxlength}maxlength='{$p.maxlength}'{/if} placeholder="{$p.placeholder}" name="{$p.name}" {if isset($p.value)}value="{$p.value}"{/if}>
			{*if $p.option}
				<datalist id="{md5($p.name)}">
					{foreach name=nVAL from=$p.option key="kVAL" item="VAL"}
						{$value = $p.option[$smarty.foreach.nVAL.index]|replace:"\r":""}
						<option value="{$value}"></option>
					{/foreach}
				</datalist>
			{/if*}
	{/switch}

    <script>
    input_onmouseout = function(el,old_Val_md5) 
    {
        if(old_Val_md5 == $.md5(el.value))
        {
            el.style.color = '';
			el.style.backgroundColor = "";
			el.setAttribute('ischanged', '0');
        }
        else
        {
            el.style.color = 'blue';
			el.style.backgroundColor = "#fffedd";
			el.setAttribute('ischanged', '1');
        }
    }
    </script>

{/function}


{function name=i18n id='' lang=null}
{strip}
	{if $D.SYSTEM.LANGUAGE_ID && !isset($lang)}{$lang = $D.SYSTEM.LANGUAGE_ID}{/if}
	{if $D.LANGUAGE.D[$lang].I18N.D[$id].VALUE}
		{$D.LANGUAGE.D[$lang].I18N.D[$id].VALUE}
	{else}
		#{$id}#
	{/if}
{/strip}
{/function}