{if !$TDATA}
	<table class="list" style="width:100%;" cellpadding="0" cellspacing="0">
		<thead>
			{foreach from=$DATA.HEAD.D key="kR" item="R" name="R"}
				<tr>
					{foreach from=$R.D key="kT" item="T" name="T"}
						<td>{$T.VALUE}</td>
					{/foreach}
				</tr>
			{/foreach}
		</thead>
		<tbody>
			{include file="include/table.tpl" TDATA=$DATA}
		</tbody>
		<tfoot>
			{foreach from=$DATA.FOOT.D key="kR" item="R" name="R"}
				<tr>
					{foreach from=$R.D key="kT" item="T" name="T"}
						<td>{$T.VALUE}</td>
					{/foreach}
				</tr>
			{/foreach}
		</tfoot>
	</table>
{else}
		{foreach from=$TDATA.BODY.D key="kR" item="R" name="R"}
			<tr>
				{foreach from=$R.D key="kT" item="T" name="T"}
					<td>{$T.VALUE}</td>
				{/foreach}
			</tr>
		{/foreach}
{/if}