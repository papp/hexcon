{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<table class="table">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">User_id</th>
			<th scope="col">ref_ID</th>
			<th scope="col">ref_type</th>
			<th scope="col">Type</th>
			<th scope="col">Title</th>
			<th scope="col">Datum</th>
		</tr>
	</thead>
	<tbody>
{foreach from=$PLA.LOG.D key="kLOG" item="LOG"}
	<tr>
      <th scope="row">{$kLOG}</th>
	  <td>{$LOG.USER_ID}</td>
	  <td>{$LOG.RID}</td>
	  <td>{$LOG.RTYPE}</td>
      <td><span class="badge badge-{if $LOG.TYPE == 'error'}danger{else}info{/if}">{$LOG.TYPE}</span></td>
      <td>{$LOG.TITLE}</td>
	  <td>{$LOG.ITIMESTAMP|date_format:'Y.m.d H:i:s'}</td>
    </tr>
	{*$LOG.TEXT*}
{/foreach}
	</tbody>
</table>