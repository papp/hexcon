{assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]}
<div style="overflow-y:scroll;">

		<form id="frm{$D.PLATFORM_ID}">
			<table class='table'>
				<thead>
					<tr>
						<td style="width:30px;"><button type="button" class="btn" onclick="id = wp.get_genID(); wp.window.open({ 'ID' : id, 'TITLE' : 'Neue Rechnung' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.buying&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][ID:IN]='+id+'&D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][D]['+id+'][ACTIVE]=1'});">+</button></td>
						<td style="width:100px;">ID</td>
						<td style="width:60px;">Status</td>
						<td style="width:60px;">Nummer</td>
						<td style="width:60px;">Hersteller</td>
						<td style="width:60px;">Paid Date</td>
						<td style="width:60px;">Paid Typ</td>
						<td>Kommentar</td>
						<td style="width:100px;">Datei</td>
						<td style="width:60px;">Netto</td>
						<td style="width:60px;">MwSt</td>
						<td style="width:60px;">Brutto</td>
						<td style="width:60px;">EMail</td>
					</tr>
				</thead>
				<tbody>
					{foreach from=$PLA.BUYING.D key="kINC" item="INC"}
					<tr>
						<td></td>
						<td><button class="btn" type="button" onclick="wp.window.open({ 'ID' : '{$kINC}', 'TITLE' : '{$kINC}' , 'WIDTH' : '1000px', 'HEIGHT' : '400px', 'URL' : '?D[PAGE]=platform.buying&D[ACCOUNT_ID]={$D.SESSION.ACCOUNT_ID}&D[PLATFORM_ID]={$D.PLATFORM_ID}&D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][W][ID:IN]={$kINC}&D[PLATFORM][D][{$D.PLATFORM_ID}][BUYING][ACTIVE]'});">{$kINC}</button></td>
						<td>{$INC.STATUS}</td>
						<td>{$INC.NUMBER}</td>
						<td>{$PLA.SUPPLIER.D[$INC.SUPPLIER_ID].TITLE}</td>
						<td style="text-align:center;">{$INC.DATE_PAID|date_format:"%d.%m.%Y"}</td>
						<td>{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}</td>
						<td>{$INC.COMMENT}</td>
						<td style="{if !$INC.FILE.D && $INC.STATUS != 40}background:#f99;{/if}">
							<ul>
								{foreach from=$INC.FILE.D key="kFIL" item="FIL"}
									<input type="hidden" name="D[ZIP][{$kFILE}][URL]" value="https://{$D.SESSION.ACCOUNT_ID}.hexcon.de/file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}/{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}_{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.GROUP.D[$INC.GROUP_ID].TITLE}_{$FIL.TITLE}.{$FIL.EXTENDSION}">
									<li><a href="file/{$D.SESSION.ACCOUNT_ID}/{$D.PLATFORM_ID}/{$kFIL}/{$INC.DATE_PAID|date_format:"%Y.%m.%d"}_{$PLA.PAYMENT.D[$INC.PAYMENT_ID].TITLE}{*$PLA.GROUP.D[$INC.GROUP_ID].TITLE*}_{$FIL.TITLE}.{$FIL.EXTENDSION}" target='_blank'>{$FIL.TITLE}.{$FIL.EXTENDSION}</a></li>
								{/foreach}
							</ul>
						</td>
						<td style="text-align:right;">{($INC.ARTICLE.PRICE - $INC.ARTICLE.VAT)|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$INC.ARTICLE.VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{($INC.ARTICLE.PRICE)|number_format:2:",":"."}</td>
						{$SUM_NET = $SUM_NET + ($INC.ARTICLE.PRICE - $INC.ARTICLE.VAT)}
						{$SUM_VAT = $SUM_VAT + $INC.ARTICLE.VAT}
						{$SUM_BRT = $SUM_BRT + $INC.ARTICLE.PRICE}
						<td>
						<a href="{strip}mailto:an@domain.de?subject=Order&amp;body=Hello,%0D%0A
						{foreach from=$INC.ARTICLE.D key="kART" item="ART"}
							{$ART.STOCK}x%20{$ART.NUMBER}%20-%20{$ART.TITLE}%0D%0A
						{/foreach}{/strip}">E-Mail</a>
						</td>
					</tr>
					{/foreach}
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align:right;">{$SUM_NET|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$SUM_VAT|number_format:2:",":"."}</td>
						<td style="text-align:right;">{$SUM_BRT|number_format:2:",":"."}</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</form>
		<script>
				$(function() {
					var index = $('#tab{$D.PLATFORM_ID}TimeLine a[href="#tab{$D.PLATFORM_ID}TimeLine-{$PLA.BUYING.W['DATE_PAID:LIKE']|truncate:4:''}"]').parent().index();
					//var index = $("#tab{$D.PLATFORM_ID}TimeLine>ul").index( $("#tab{$D.PLATFORM_ID}TimeLine-2014") );
					//alert(index);
					$( "#tab{$D.PLATFORM_ID}TimeLine" ).tabs({ active: index });
				});
				</script>
	
</div>