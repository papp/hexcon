<?php
switch($D['ACTION'])
{
	case 'load_article':
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D']['W']['ID'] = $D['ARTICLE_ID'];
		###$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D); #ToDo: Beschleunigt das Laden. Wurde vorüber auskommentiert weil an dieser Stellew erden Artikel nicht benötigt.

		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['INCOMINGINVOICE']['D'][ $D['INCOMINGINVOICE_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['NUMBER'] = 
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['NUMBER'];
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['INCOMINGINVOICE']['D'][ $D['INCOMINGINVOICE_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['TITLE'] =
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['TITLE'];
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['INCOMINGINVOICE']['D'][ $D['INCOMINGINVOICE_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['PRICE'] =
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['ARTICLE']['D'][ $D['ARTICLE_ID'] ]['SUPPLIER']['D'][ $D['SUPPLIER_ID'] ]['PRICE'];
		
		break;
	case 'upload':
		$CFile->copy($_FILES['files'],'tmp');
		$ID = md5_file("tmp/{$_FILES['files']['name'][0]}");
		$pathinfo = pathinfo("tmp/{$_FILES['files']['name'][0]}");
		$size = filesize("tmp/{$_FILES['files']['name'][0]}");
		$pathinfo['extension'] = strtolower($pathinfo['extension']);
		$CFile->move("tmp/{$_FILES['files']['name'][0]}","data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM_ID']}/FILE/".date("Ym")."/{$ID}.{$pathinfo['extension']}");

		
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['FILE']['D'][ $ID ] = array(
		'ACTIVE'	=> 1,
		'NAME'		=> "{$ID}.{$pathinfo['extension']}",
		'SIZE'		=> $size,
		);
		$D = $PLATFORM[ $D['PLATFORM_ID'] ]->set_file($D);
		
		#$s = $_SESSION['upload_progress_'.intval($_GET['PHP_SESSION_UPLOAD_PROGRESS'])];
		$progress = array(
			'file_id' => $ID,
			'file_name'	=> "{$ID}.{$pathinfo['extension']}",
			'file_title'=>	$pathinfo['filename'],
			#'lengthComputable' => true,
			#'loaded' => $s['bytes_processed'],
			#'total' => $s['content_length'],
			);
		exit(json_encode($progress));
		break;
	case 'search_article':
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_article($D);
		break;
	case "save":
	case 'set_incominginvoice':
		$PLATFORM[ $D['PLATFORM_ID'] ]->set_incominginvoice($D);
		exit();
		break;
	default:
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_incominginvoice($D);
		$PLATFORM[ $D['PLATFORM_ID'] ]->get_payment($D);
		##$PLATFORM[ $D['PLATFORM_ID'] ]->get_supplier($D);

		$F['PLATFORM']['W']['ID'] = $D['PLATFORM_ID'];
		$F['PLATFORM']['SUPPLIER']['W'][0]['Active'] = 1;
		##$PLATFORM[ $D['PLATFORM_ID'] ]->get_object_reqursive($D, $F);
		

		#$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['W']['TYPE'] = 'INCOMINGINVOICE';
		#$PLATFORM[ $D['PLATFORM_ID'] ]->get_group($D);

		#Fix: Gruppe
		$D['PLATFORM']['D'][ $D['PLATFORM_ID'] ]['GROUP']['D'] = [
			'office'		=> [ 'TITLE' => "Büro"],
			'import_tax'	=> [ 'TITLE' => "Einfuhrsteuer"],
			'buying'		=> [ 'TITLE' => "Einkauf"],
			'salary'		=> [ 'TITLE' => "Gehalt"],
			'others'		=> [ 'TITLE' => "Sonstiges"],
			'tax'			=> [ 'TITLE' => "Steuer"],
			'shipping'		=> [ 'TITLE' => "Versand"],
		];
		break;
}
$smarty->assign('D',$D);
$smarty->display('extends:platform.incominginvoice.tpl|include/input.tpl');