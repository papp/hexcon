<?php
include_once("core/account.php");

switch ($D['ACTION'])
{
	case 'set_account':
		foreach((array)$D['ACCOUNT']['D'] AS $kACC => $vACC) {
			if($vACC['NEW'] && !is_dir("data/ACCOUNT/{$kACC}") ) {
				$CFile::mkdir("data/ACCOUNT/{$kACC}");
				copy('data/ACCOUNT/default/data.db',"data/ACCOUNT/{$kACC}/data.db");
			}
			$SQL = new SQLite3("data/ACCOUNT/{$kACC}/data.db",SQLITE3_OPEN_READWRITE);
			$SQL->exec('
				PRAGMA busy_timeout = 5000;
				PRAGMA cache_size = -2000;
				PRAGMA synchronous = 1;
				PRAGMA foreign_keys = ON;
				PRAGMA temp_store = MEMORY;
				PRAGMA default_temp_store = MEMORY;
				PRAGMA read_uncommitted = true;
				PRAGMA journal_mode = wal;
				PRAGMA wal_autocheckpoint=1000;
			');
			$ACCOUNT['D'][$kACC]['CLASS'] = new account($kACC);
			
			foreach((array)$D['ACCOUNT']['D'][$kACC]['USER']['D'] AS $kUSR => $USR) {
				if($USR['PASSWORD']) {
					$D['ACCOUNT']['D'][$kACC]['USER']['D'][$kUSR]['PASSWORD'] = password_hash($USR['PASSWORD'],PASSWORD_DEFAULT);
				}
			}
			$ACCOUNT['D'][$kACC]['CLASS']->set_user($D['ACCOUNT']['D'][$kACC]);
		}
		
		$MAIN->set_account($D);
		exit;
		break;
	default:
		$D['ACCOUNT'] = null; #Todo: Hotfix, weil in !config.php Account = admin geladen wird
		$MAIN->get_account($D);
		
		foreach((array)$D['ACCOUNT']['D'] AS $kACC => $vACC) {
			if(is_file("data/ACCOUNT/{$kACC}/data.db")) {
				$SQL = new SQLite3("data/ACCOUNT/{$kACC}/data.db",SQLITE3_OPEN_READWRITE);
				$SQL->exec('
					PRAGMA busy_timeout = 5000;
					PRAGMA cache_size = -2000;
					PRAGMA synchronous = 1;
					PRAGMA foreign_keys = ON;
					PRAGMA temp_store = MEMORY;
					PRAGMA default_temp_store = MEMORY;
					PRAGMA read_uncommitted = true;
					PRAGMA journal_mode = wal;
					PRAGMA wal_autocheckpoint=1000;
				');
				$ACCOUNT['D'][$kACC]['CLASS'] = new account($kACC);
				$ACCOUNT['D'][$kACC]['CLASS']->get_user($D['ACCOUNT']['D'][$kACC]);
			}
		}
		break;
}

$smarty->assign('D',$D);
$smarty->display('admin.account.list.tpl');