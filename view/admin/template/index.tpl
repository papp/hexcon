{*assign var="PLA" value=$D.PLATFORM.D[$D.PLATFORM_ID]*}
<!doctype html>
<html lang="de">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>hexcon.de</title>
		<meta name="author" content="Waldemar Preis">
		<link rel="shortcut icon" href="view/admin/template/core/icon.png">

		{*<script src="//code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>*}
		<script src="//code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
		<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		{*<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>*}
		<script src="//cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
		<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
		<script src="view/admin/template/core/framework/jquery.md5.js" crossorigin="anonymous"></script>

		<!--Table-->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.0/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js"></script>
		
		<!--file Upload Start-->
		<link rel="stylesheet" href="view/admin/template/core/framework/file_upload/css/jquery.fileupload.css" media="bogus">
		<script src="view/admin/template/core/framework/file_upload/js/jquery.iframe-transport.js"></script>
		<script src="view/admin/template/core/framework/file_upload/js/jquery.fileupload.js"></script>
		<!--file Upload End-->

		<!--file Upload2 Start-->
		<link href="https://hayageek.github.io/jQuery-Upload-File/4.0.11/uploadfile.css" rel="stylesheet">
		<script src="view/admin/template/core/framework/jquery.uploadfile.js"></script>
		<!--file Upload2 End-->

		<script src="view/admin/template/core/framework/Chart.min.js"></script>

		<!--jsTree-->
		{*<script src="view/admin/template/core/framework/jstree/dist/jstree.js"></script>
		<link rel="stylesheet" href="view/admin/template/core/framework/jstree/dist/themes/default/style.css" media="bogus">*}
		<!--<link rel="stylesheet" href="view/admin/template/core/bootstrap.micro.css">-->

		{*<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">*}
		<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" crossorigin="anonymous">
		
		<!--select2 start-->
		{*https://select2.org/selections*}
		{*https://github.com/select2/select2-bootstrap-theme*}
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
		{*<link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet" />*}
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
		
		
		<!--select2 end-->

		<style>{*Font Awasom 5*}
		@font-face {
			font-family: 'FontAwesome';
			src: url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot?v=4.7.0');
			src: url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff2?v=4.7.0') format('woff2'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff?v=4.7.0') format('woff'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.ttf?v=4.7.0') format('truetype'), url('https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.svg?v=4.7.0#fontawesomeregular') format('svg');
			font-weight: normal;
			font-style: normal;
		}
		</style>
		<link rel="stylesheet" href="view/admin/template/core/framework/font-awesome/css/font-awesome.css">

		<link rel="stylesheet" href="view/admin/template/core/main.css?v=1" media="bogus">
		<script src="view/admin/template/core/wp.js?1=1"></script>
	</head>
	<body oncontextmenu="return false;" onclick="wp.contextmenu.close();">
		<div id="frame_index"></div>

<script>
//Lade Admin Seite
 wp.ajax({ 'div' : 'frame_index', 'url' : '?D[PAGE]=admin' });

 

//Tooltip
  $(function() {
    $( document ).tooltip({
     content: function() {
		if(this.hasAttribute('title'))
			return  $( this ).attr( "title" );
		else
			return  $( this ).attr( "placeholder" );
	},
	items : "*[placeholder], *[title]",
	track: true
    });
  });

</script>

	<link rel="stylesheet" href="view/admin/template/core/framework/file_upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="view/admin/template/core/main.css">
	</body>
</html>