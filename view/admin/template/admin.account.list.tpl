<form id="form_admin_account_list" name="form_admin_account_list" method="post">
	<button class="btn btn-success" onclick="newacc()" type="button">Neues Account Anlegen</button>
	<table class="table">
		<thead>
			<tr>
				<td>ID / SubDomain</td>
				<td>Aktive</td>
				<td>Title</td>
				<td>Erstellungsdatum</td>
				<td>Benutzer</td>
			</tr>
		</thead>
		<tbody id="form_admin_account_list_body">
	{foreach from=$D.ACCOUNT.D key="kACC" item="ACC"}
		<tr>
			<td>{$kACC}</td>
			<td><input name="D[ACCOUNT][D][{$kACC}][ACTIVE]" value="{$ACC.ACTIVE}"></td>
			<td><input name="D[ACCOUNT][D][{$kACC}][TITLE]" value="{$ACC.TITLE}"></td>
			<td>{$ACC.itimestamp}</td>
			<td>
				<table class="table">
					<thead>
						<tr>
							<td><button class="btn btn-success" onclick="newUser('{$kACC}')" type="button">+</button></td>
							<td>id</td>
							<td>Aktiv</td>
							<td>Nickname</td>
							<td>Passwort</td>
						</tr>
					</thead>
					<tbody id="form_admin_account_list_body_{$kACC}_user_body">
					
					{foreach from=$D.ACCOUNT.D[$kACC].USER.D key="kUSR" item="USR"}
						<tr>
							<td><!--<button class="btn btn-danger" onclick="delUser('{$kACC}','{$kUSR}')" type="button">-</button>--></td>
							<td>{$kUSR}</td>
							<td><input name="D[ACCOUNT][D][{$kACC}][USER][D][{$kUSR}][ACTIVE]" value="{$USR.ACTIVE}"></td>
							<td><input name="D[ACCOUNT][D][{$kACC}][USER][D][{$kUSR}][NICKNAME]" value="{$USR.NICKNAME}"></td>
							<td><input name="D[ACCOUNT][D][{$kACC}][USER][D][{$kUSR}][PASSWORD]" value=""></td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</td>
		</tr>
	{/foreach}
		</tbody>
	</table>
	
	<script>
		save = function() {
			wp.ajax({
					'url' : '?D[PAGE]=admin.account.list&D[ACTION]=set_account',
					'div' : 'ajax',
					'data': $('#form_admin_account_list').serialize()
					});
		}
		newUser = function(acc) {
			
				id = wp.get_genID();
				h = "<tr>"
						+"<td></td>"
						+"<td>"+id+"</td>"
						+"<td><input name='D[ACCOUNT][D][{$kACC}][USER][D]["+id+"][ACTIVE]' value='0'></td>"
						+"<td><input name='D[ACCOUNT][D][{$kACC}][USER][D]["+id+"][NICKNAME]' value=''></td>"
						+"<td><input name='D[ACCOUNT][D][{$kACC}][USER][D]["+id+"][PASSWORD]' value=''></td>"
					+"</tr>";
				$("#form_admin_account_list_body_"+acc+"_user_body").append(h);
			
		}
		
		newacc = function() {
			sign = prompt("Bitte die Subdomain für das neue Account eingeben.");
			if(sign)
			{ 
				h = "<tr>"
						+"<td>"+sign+"<input type='hidden' name='D[ACCOUNT][D]["+sign+"][NEW]' value='1'></td>"
						+"<td><input name='D[ACCOUNT][D]["+sign+"][ACTIVE]' value='0'></td>"
						+"<td><input name='D[ACCOUNT][D]["+sign+"][TITLE]' value='"+sign+"'></td>"
						+"<td></td>"
						+"<td></td>"
					+"</tr>";
				$("#form_admin_account_list_body").append(h);
			}
		}
	</script>
	<button class="btn btn-primary" onclick="save();" type="button">Speichern</button>
	
</form>