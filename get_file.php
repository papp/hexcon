<?php
#Generiert Artikel bilder
include('!config.php');

if(isset($D['SORT'])) #Nach Artikel Datei Ausgeben
{
	$PLATFORM[ $D['PLATFORM']['W']['ID'] ]->get_article($D);

	#print_r($D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['W']['ID'] ]['FILE']['D']);
	$kFILE = array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['W']['ID'] ]['FILE']['D']);
	if(!$kFILE) #Varianten File
	{
		$kART = array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['D']);
		for($a=0; $a <count($kART);$a++)
		{
			$kFILE = array_keys((array)$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['D'][ $kART[$a] ]['VARIANTE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['W']['ID'] ]['FILE']['D']);
		}
	}

	#$FILE = $D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['D'][ $D['ARTICLE']['W']['ID'] ]['FILE']['D'][ $kFILE[$D['SORT']] ];

	$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['FILE']['W']['ID'] = $kFILE[$D['SORT']];
}

$PLATFORM[ $D['PLATFORM']['W']['ID'] ]->get_file($D);

$D['IMAGE'] = [
	'SOURCE_FILE'	=> "data/ACCOUNT/{$D['ACCOUNT_ID']}/PLATFORM/{$D['PLATFORM']['W']['ID']}/FILE/{$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['FILE']['D'][ $D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['FILE']['W']['ID'] ]['URL']}",
	'TARGET_DIR'	=> "data_tmp/ACCOUNT/{$D['ACCOUNT_ID']}/data/",
	'TARGET_FILE'	=> "file.{$D['ACCOUNT_ID']}.{$D['PLATFORM']['W']['ID']}.{$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['FILE']['W']['ID']}_{$D['X']}x{$D['Y']}.{$D['TYPE']}",
	'X'				=> $D['X'],
	'Y'				=> $D['Y'],
	'SHOW'			=> true, #gibt das Bild sofort aus
	'QUALITY'		=> 70,
];
#print_r($D['IMAGE']);

$CFile->image($D['IMAGE']);
/*
if(isset($D['SORT']) && is_file("{$D['IMAGE']['TARGET_DIR']}{$D['IMAGE']['TARGET_FILE']}.jpg"))#Bild wurde generiert? erstelle Sumlink vom Artikel Bild
{
	#erstellt ein symlink des Artikels auf die original File Datei
	#Achtung: bei all-inkl funktioniert symlink nur mit PHP CGI Modus!
	$link = "{$D['IMAGE']['TARGET_FILE']}.jpg";
	$symlink = "tmp/ACCOUNT/{$WP['ACCOUNT']['ID']}/data/file.{$WP['ACCOUNT']['ID']}.{$D['PLATFORM']['W']['ID']}.{$D['PLATFORM']['D'][ $D['PLATFORM']['W']['ID'] ]['ARTICLE']['W']['ID']}_{$D['SORT']}_{$D['X']}x{$D['Y']}.jpg";
	symlink($link,$symlink);
}
*/