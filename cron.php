<?php

/*
cron.php ist ein Account unabhängiger Cronjob und kann für alle Accounts genutzt werden.
cron.php muss einmal per Cronjob je 2-5min ausgeführt werden.
*/
ini_set('max_execution_time', 300);
ignore_user_abort(true); #Ignoriert den abruch seitens Client
#error_reporting(E_ALL ^E_NOTICE ^E_WARNING ^E_DEPRECATED);

include('!config.php');
echo "Start<br>";
$D['ACCOUNT']['W']['ACTIVE'] = 1;
$MAIN->get_account($D);
foreach($D['ACCOUNT']['D'] AS $kACC => $ACC) {
	$ACCOUNT = new account($kACC);
	echo "ACCOUNT:{$kACC}<br>";
	$D['TASK']['W']['ACTIVE'] = 1;
	$D['TASK']['W']['FAIL:<'] = 4;
	$D['TASK']['W']['START_TIME:<='] = date('Hi');
	$D['TASK']['W']['START_TIME:>'] = date('Hi',time()-14*60);
	$D['TASK']['W']['UTIMESTAMP:<'] = date('Y-m-d H:i:s',time()-15*60);
	$D['TASK']['O']['UTIMESTAMP'] = 'ASC';
	$D['TASK']['L'] = ['START' => 0, 'STEP' => 1];
	$ACCOUNT->get_task($D);
	#print_R($D['TASK']);

	if(count((array)$D['TASK']['D']) > 0) {
		echo "TASKs:".count((array)$D['TASK']['D'])."<br>";
		#sqlite ACCOUNT-------------------
		#ToDo: DB Instanz vereerben!
		$SQL = new SQLite3("data/ACCOUNT/{$kACC}/data.db", SQLITE3_OPEN_READWRITE);
		$SQL->exec('
			PRAGMA busy_timeout = 5000;
			PRAGMA cache_size = -2000;
			PRAGMA synchronous = 1;
			PRAGMA foreign_keys = ON;
			PRAGMA temp_store = MEMORY;
			PRAGMA default_temp_store = MEMORY;
			PRAGMA read_uncommitted = true;
			PRAGMA journal_mode = wal;
			PRAGMA wal_autocheckpoint=1000;
		');
		#sqlite-----------------

		#Data SQL --------------#ToDo: Hotfix, weil dies wird aus !config nicht geladen, weil die Session mit ACCOUT_ID fehlt
		$SQL_Data = new wp\CData(['PATTERN' => '', 
			'DB' => ['FILENAME' => "data/ACCOUNT/{$kACC}data.db"], 
			'BACKUP' => ['DestinationPath' => "data/ACCOUNT/{$kACC}/backup/", 'BackupPassword' => null ],
		]);

		#Lade aktive Platformen Start ================
		#Include Platform Manager
		$ACCOUNT->get_platform($D);
		foreach((array)$D['PLATFORM']['D'] AS $kP => $P) {
			if($P['ACTIVE']) {
				echo "include: core/platform/{$P['CLASS_ID']}/{$P['CLASS_ID']}.php<br>";
				include_once("core/platform/{$P['CLASS_ID']}/{$P['CLASS_ID']}.php");
				echo "new {$P['CLASS_ID']}($kACC, $kP)<br>";
				$PLATFORM[ $kP ] = new $P['CLASS_ID']( $kACC, $kP );
				$PLATFORM[ $kP ]->info($D);
			}
		}
		#Lade aktive Platformen Ende ================
	
	
		foreach($D['TASK']['D'] AS $kCRN => $CRN) {
			#$D['TASK']['D'][$kCRN]['UTIMESTAMP'] = date('Y-m-d H:i:s');
			$D['TASK']['D'][$kCRN]['FAIL']++;
			$ACCOUNT->set_task($D);
			
			if($D['TASK']['D'][$kCRN]['FAIL'] < 4) {
				echo "PLATFORM:{$CRN['PLATFORM_ID']}<br>";
				echo "TASK:{$kCRN}<br>";
				$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['W']['ID:IN'] = $CRN['FEED_ID'];
				$PLATFORM[$CRN['PLATFORM_ID']]->get_feed($D);
				$path_parts = pathinfo($_SERVER['REQUEST_URI']);
				echo "{$_SERVER['REQUEST_SCHEME']}://{$kACC}.{$_SERVER['HTTP_HOST']}{$path_parts['dirname']}/file/{$CRN['PLATFORM_ID']}/{$CRN['FEED_ID']}/{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['KEY']}/{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['FILENAME']}.{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['FILETYPE']}<br>";
				$_ret = file_get_contents("{$_SERVER['REQUEST_SCHEME']}://{$kACC}.{$_SERVER['HTTP_HOST']}{$path_parts['dirname']}/file/{$CRN['PLATFORM_ID']}/{$CRN['FEED_ID']}/{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['KEY']}/{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['FILENAME']}.{$D['PLATFORM']['D'][$CRN['PLATFORM_ID']]['FEED']['D'][$CRN['FEED_ID']]['FILETYPE']}");
				if($_ret !== false) {
					echo $_ret;
				}
				else {
					echo "ERROR: file_get_contents konnte nicht ausgeführt werden";
				}
				###echo file_get_contents($CRN['URL']);
			}
			else {
				$D['TASK']['D'][$kCRN]['ACTIVE'] = 0; 
			}
			
			$D['TASK']['D'][$kCRN]['FAIL'] = 0;
			$ACCOUNT->set_task($D);
		}
	}
}
echo "Ende<br>";