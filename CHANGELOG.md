# Changelog


## ToDo Funktionsbeschreibung:
- Rechnungen
- Angebote
- Zahlungserinnerung
- Gutschrift / Stornorechnungen
- Lieferschein

 ---
 =>wp_shipping,wp_shipping_class, wp_shipping_class_to_cost, wp_shipping_class_to_country, wp_shipping_conditions => Löschen und durch DeliveryClass und Einstellung Delivery ersentzen.
 -> soll alles in der einstellung als json gespeichert werde.

 

- Login Sperre bei falschen logins
- Passwort vergessen Funktion
- Registrieren Funktion
- Amazon Attribute je Kategorie importieren
- Amazon automatische erkennung von Attribute Type
- Deepl Intergration
- EAN, gewicht, Kategorie als Attribut hinzufügen
- Flexible Statistik im Dashboard
- Software Update funktion => soll mit dem Installer realisiert werden!
- neue User Rechteverwaltung
- 2-Faktor-Authentifizierung

- ToDo: Neuer DB Filter Sturktur:
----------------------
AND
W[0][FELD][IN] = '1,2';
W[0][FELD][>] = 0
OR
W[1][FELD][=] = 1234
----------------------

## [8.01]
* ~ CData auf Version 2.09 aktuallisiert.
* ~ Lieferanten Maske wieder hergestellt.

## [8.00]
* ~ Amazon MWS zur SP-API Umgestellt
* ~ platform.supplier.php, platform.supplier.list.php, template/platform.supplier.tpl, template/platform.supplier_list.tpl entfernt
* ~ wp_supplier, wp_supplier_attribute, wp_supplier_to_article entfernt
* ~ wp_warehouse, wp_warehouse_storage entfernt
* ~ Lieferantenpflege auf die neue Universalle Data umgestellt
* ~ Lagerpflege auf die neue Universalle Data umgestellt
* ~ Universelle data Verarbeitung verbessert. 
* + Universelle data_list & data Seite hinzufügt für die Daten Verarbeitung.
* + Neue Datenbank Struktur wp_data und wp_data_cache Ermöglicht Flexible Daten zu speichern und zu Cachen.
* ~ Drag & Drop für Mobile realisiert.
* ~ Lieferant Maske erstellt
* ~ Cron Fix. Cron.php kann nun von der haupt Domain ausgeführt werden.
* ~ Security Fix: Klasse CWP wird nicht mehr für Feed zur verfügung gestellt, statt dessen wird CSExt explizit mit sicheren Funktionen zur verfügung gestellt.
* ~ Navigation: Order und Einstellungen entfernt. Einstellungen Attribute und Feed sind in die oberste Navigation gewandert.
* ~ Attributpflege Maske reaktivert
* + Angabe zur Firmenanschrift - Einstellungen hinzugefügt
* + Maske zur bearbeiten von Platformen hinzugefügt
* ~ Rechnungen werden nun in Original Währung erstellt mit Tagesaktueller Umrechnungsratte zum Euro
* ~ dompdf Update von 0.8.2 auf 2.0.0 Version
* + Smarty Security für Feeds aktiviert. Es können nur sichere PHP Funktionen verwendet werden, diese keinen Zugriff auf Dateien erfordert.
* + Automatische Abfrage des Währungskurs EU weit.
* ~ DB: Tabelle wp_log entfernt.
* ~ DB: Tabellen tax_class und tax_class_to_country entfernt. Wurde durch die TaxClassToCountry und StandardTaxClass Einstellungen realisiert.
* ~ DB: Tabelle wp_categorie_article entfernt. Wird beim Artikel nun als Attribut CategorieId hinterlegt.
* ~ DB: Tabelle wp_platform_language entfernt. Die Platfrom Verfügbare Sprachen werden nun bei der AvailableContentLanguage-Einstellung hinterlegt.
* ~ DB: Tabelle wp_platform_platform entfernt. Die zuordnung wurde durch die Spalte parent_id in wp_platform Tabelle ersetzt.
* ~ Steuertabelle ersetzt durch Einstellungsvariablen
* ~ Überarbeitung der Artikel Make, verbesserung und Fehler behebung
* ~ Entfernung von ATTRIBUTETYPE, Einführung von Attribute Parent_id 
* + Passwort ändern Funktion
* + Neue Maske zur verwaltung von Benutzer hinzugefügt.
* + eJS JS Template Eingeführt
* ~ Einstellungen: Globale Einstellungen mit Platform Einstellungne kombiniert
* + Steuer Tabelle hinzugefügt.
* + Cronjob Maske je Export erweitert
* + cron.php für Cronjob hinzugefügt
* ~ Amazon Bestellung Import Feed erstellt
* ~ Automatische erstellung von Rechnung Feed erstellt
* ! Fix diverse Funktionen
* ~ Funktion der Storno Rechnung korrigiert
* ~ Rechnung und Storno Rechnung Export Button korrigiert
* ~ Feed mit i18n erweitert. Sprachvariablen können im Feed ebenfalls genutzt werden.
* + Standard Daten für haupt Platform erstellen
* + Verwendung von webp Formant hinzugefügt. Bilder werden automatisch je nach angabe der Endung in das format vormatiert.
* + Administrationsbereich hinzugefügt
* + Überprüfung des Accounts auf exsistenz
* + Script für PHP 8 aktuallisiert
* ~ Einstellungen (select und multiselect) werden ausgeblendet wenn keine Auswahl zur verfügung steht.
* ~ Einheitliche UUID verbessert und vereinheiticht.
* ~ Kategorie Zuordnung verbessert. Es werden nun auch Kategoriezuordnugen aus der ober Kategorien mit angezeigt um das Vererben Verhalten besser nachvolziehen zu können.
* ~ Amazon Kategorien aktuallisiert
* + Passwort Verschlüsselung
* ~ Speicherung von Einstellungen
* + Datenbank Umstellung von MySQL auf SQLite - Performace optimierung
* + Neue Struktur einführung system - main - account - platformen
* + Einführung der Multilanguage der Software
* + Verwaltung von Import/Exporten überarbeitet Bessere Darstellung
* ~ Login Seite überarbeitet
* ! Alte Rechteverwaltung entfernt
* + Installer hinzugefügt

## [7.0.4]
* + Attribute import (ebay)
* + Attribute Mapping je Kategorie
* ! Warnings Ausgaben in der artikel_list und Artikel - Maske behoben.
* + Platform Kategorie Zuordnung kann unter je Kategorie Einstellungen eingestellt werden.
* + Kategorien müssen nun nicht umbedingt mit der letzten Ebene verknüpft werden.

## [7.0.3]
* ~ eBay - Zahlungsart Überweisung aufgenohmen.
* ~ Select-Box durch select2-Box erweitert.
* ~ Attribute erweiterung. Multiauswahl kann nun bei den Attributen verwendet werden. So können mehrere Werte je Attribute gewählt werden.
* ~ bootstrap: Update auf die aktuelle 4.3.1 Version.
* ~ Kattegorie Einstellungen: Attribute werden nun deutlich angezeigtt aus welchem Vater-Kategorie diese herkommen.
* ~ Weitere Standard Attribute hinzugefügt.
* ~ Dashboard: Errechnung des Durschnits von (Bestellungen, Rechhnungen, Strorno und Waren Einsatz) eingefügt.
* ~ "Lieferumfang"-Attribute als fester Bestandteil Attribute beim Artikel hinzugefügt.
* + Attribute als Platzhalter im Text können nun verwendet werden. ([AttributeName])
* + Attribute Optionen für die "Drop Down List"-Attribute können nun über die Kategorie Einstellungen hinzugefügt werden. Dadurch ist es möglich spezifische Auswahl Optionen je Kategorie zu setzen.
* ~ CrossSave Unterstützung hinzugefügt (testweise nur für Artikel-Editor). Dadurch werden nur die Felder an den Server gesendet die auch geändert wurden. Dadurch wird verhindert das Werte fälschlicherweise überschrieben werden die zur gleichen Zeit vom anderen geändert wurden.
* ~ CrossSave in der Rechnungsmaske, Versandmaske und Inventur hinzugefügt
* + Neue Inventurmaske (v2) erstellt (alpha)
* ~ Amazon - Es werden keine Bilder mehr an Variationen vom Vater angehängt wen bei diesen Bilder gepflegt wurden.
* ~ Amazon - Es wird für restliche Positionen delete für bilder gesendet, falls die Bilder reduziert wurden und diese bei Amazon wieder entfernt werden müssen.
* ~ Inventur (v2) - Überarbeitete Inventurmaske in dieser alle Artikel trotz fehlenden Lagerorte sichbar sind.
* + Paid Status bei den Bestellungen hinzugefügt. Dieser dient dazu um eine Bestellung beim Zahlungseingang entsprechend zu makieren.
* ~ eBay - Es werden nur Bestellungen die bezahlt oder auf Zahlungwartend importiert, andere werden geblockt.
* ~ Versandmaske - E-Mail Ausgabe des Kunden hinzugefügt.

## [7.0.2]
* + Changelog
* ! Einkauf>Bestellvorschläge: Einfache Artikel werden in der Liste nicht dargestellt
* ~ Delete bei set_log hinzugefügt
* ~ Ebay - Fehlerlog je Artikel wird bei erfolgreichen Übertragung wieder gelöscht

# Legende
+ = Funktion hinzugefügt
~ = Verbesserung
! = Bugfix