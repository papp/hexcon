<?php
include('!config.php');
$D = $_REQUEST['D'];

include('core/framework/barcode/BarcodeBar.php');
include('core/framework/barcode/Barcode.php');
include('core/framework/barcode/Types/TypeInterface.php');

include('core/framework/barcode/Types/TypeCode39.php');
include('core/framework/barcode/Types/TypeCode39Extended.php');
include('core/framework/barcode/Types/TypeCode128.php');


include('core/framework/barcode/BarcodeGenerator.php');
include('core/framework/barcode/BarcodeGeneratorPNG.php');
include('core/framework/barcode/BarcodeGeneratorSVG.php');
include('core/framework/barcode/BarcodeGeneratorJPG.php');
include('core/framework/barcode/BarcodeGeneratorHTML.php');


if($D['BARCODE']) {
	#https://github.com/picqer/php-barcode-generator
	#Beispiel Aufruf: get_barcode.php?D[BARCODE][CODE]=X001GNCAXX&D[BARCODE][CODE_TYPE]=C128&D[BARCODE][WFACTOR]=2&D[BARCODE][HEIGHT]=100
	#/barcode/X001GNCAXX.C128.2.100.png
	#echo $generatorSVG::TYPE_CODE_128;
	switch ($D['BARCODE']['TYPE']) {
		case 'jpg':
			$generatorSVG = new Picqer\Barcode\BarcodeGeneratorJPG();
			break;
		case 'svg':
			$generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();
			break;
		case 'png':
			$generatorSVG = new Picqer\Barcode\BarcodeGeneratorPNG();
			break;
		case 'html':
			$generatorSVG = new Picqer\Barcode\BarcodeGeneratorHTML();
			break;
	}
	
	
	$img = $generatorSVG->getBarcode($D['BARCODE']['CODE'], $D['BARCODE']['CODE_TYPE'], $D['BARCODE']['WFACTOR'],$D['BARCODE']['HEIGHT']);
	$file_name = "barcode.{$D['BARCODE']['CODE']}.{$D['BARCODE']['CODE_TYPE']}.{$D['BARCODE']['WFACTOR']}.{$D['BARCODE']['HEIGHT']}.{$D['BARCODE']['TYPE']}";
	##file_put_contents("data_tmp/ACCOUNT/{$D['ACCOUNT_ID']}/data/{$file_name}",$img); #ToDo: soll für das Caching sein, damit per htaccess beim zweiten Aufruff dadrauf zugegriefen werden kann.
	$CFile = new CFile();
	$CFile->stream(['SOURCE' => [ 'CONTANT' => $img ], 'RETURN'=> ['FILE' => $file_name] ]);
}