<?php
/*
Diese Datei soll restlichen Cronjobs für Artikel export ersetzen.
Durch aufruf, werden alle geänderte Artikel von allen Accounts und Platformen übertragen
ToDo: je Account und Platform aus performance trennen.
*/
$D = $_REQUEST['D'];
include('!config.php');
ini_set('max_execution_time', 300);
ignore_user_abort(true); #Ignoriert den abruch seitens Client
#/*
#Einschrenkungen:
$D['ACCOUNT']['W']['ID:IN'] = $D['ACCOUNT_ID'];#account_id wird in der !config anhand Subdomain gesetzt!
$D['ACCOUNT']['W']['ACTIVE'] = 1;
$MAIN->get_account($D);#exit;

$ACCOUNT->get_platform($D);#erstelle für alle aktive Platformen eine Instantz. Weil es kann nicht garantiert werden das die Ziel Platfom bereits instaziiert wurde im Schleifen durchlauf!
foreach($D['PLATFORM']['D'] AS $kP => $P) {
	if($P['ACTIVE']) {
		$PLATFORM[ $kP ] = new $P['CLASS_ID']( $D['ACCOUNT_ID'], $kP ); #Platforms
	}
}

foreach((array)$D['ACCOUNT']['D'] AS $kA => $vA) {
	$LOG .= "START Account: {$kA}\n";
	#$D['SESSION']['ACCOUNT_ID'] = $kA; #ToDo: Prfen ob das erforder
	$ACCOUNT = new account($kA);
	$ACCOUNT->get_setting($D);

	foreach((array)$vA['PLATFORM']['D'] AS $kP => $vP) {
		if($vP['ACTIVE'] == 1) {#Nur Aktive Platformen behandeln
			$CONTROL[ $kP ] = new control($kA, $kP); #Platform Manager
			###$PLATFORM[ $kP ] = new $D['ACCOUNT']['D'][ $kA ]['PLATFORM']['D'][ $kP ]['CLASS_ID']( $kA, $kP ); #Platforms
			$PLATFORM[ $kP ]->get_setting($D);
			
			echo "<div style='border:solid 1px #ddd;padding:5px;background:#fefefe;margin:5px;'><b>Platform: {$kA} - {$vP['TITLE']}</b><br>";
			
			#set_article =============================
			if(in_array('set_article',(array)get_class_methods($PLATFORM[ $kP ]) ) 
				&& in_array($D['PLATFORM']['D'][ $kP ]['SETTING']['D']['set_article_ACTIVE']['VALUE'], ['ALL','SP'])
				&& time() > $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['set_article_EXECUTION_INTERVAL']['VALUE'] + $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['set_article_LAST_EXCEUTION_TIME']['VALUE']
			)
			{
				#ToDo: test: Next Time vor dem ausfüren setzen. Bei Fatal Error wird einfach die Zeit abgewartet.
				$SET[$kP]['set_article_LAST_EXCEUTION_TIME']['VALUE'] = time();
				$d['PLATFORM']['D'][ $kP ]['SETTING']['D'] = $SET[$kP]; 
				$PLATFORM[ $kP ]->set_setting($d);

				$D['PLATFORM']['D'][ $kP ]['ARTICLE']['L'] = ['START' => 0, 'STEP' => ($D['PLATFORM']['D'][ $kP ]['SETTING']['D']['set_article_PIECE_PRO_CRONJOB']['VALUE'])?$D['PLATFORM']['D'][ $kP ]['SETTING']['D']['set_article_PIECE_PRO_CRONJOB']['VALUE']:10 ]; #Aus Performance Grnden werden nur 10 Artikel pro Export ausgegeben
				###$D['PLATFORM']['D'][ $kP ]['ARTICLE']['ID'] = 'AB107649';####
				#$D['PLATFORM']['D'][ $kP ]['ARTICLE']['W']['GET_TO_UPDATE'] = 1;
				$D['PLATFORM']['D'][ $kP ]['ARTICLE']['W']['TO_PLATFORM_ID_UPDATE'] = $kP;#Gib alle aktuelle Artikel die an diese PLF zugeordnet sind
				$D['PLATFORM']['D'][ $kP ]['ARTICLE']['W']['ACTIVE'] = 1;
				$D['PLATFORM']['D'][ $kP ]['ARTICLE']['O']['PARENT_ID'] = 'ASC';#Soll die Parent Artikel dennoch als erte Ausgabe vorsortieren. Damit die Platform Atribute für Kinde vorgezogen werden.
				$D['PLATFORM']['D'][ $kP ]['ARTICLE']['O']['UTIMESTAMP'] = 'ASC';#Die ältesten Änderungen zuerst
				$D['UPDATE'] = 1; #Wichtig damit weiß ob title neu generiert werden muss oder nicht.
				$CONTROL[ $kP ]->get_article($D);
				
				if($D['PLATFORM']['D'][$kP]['ARTICLE']['PARENT']['D'][NULL]['CHILD']['D'])
				{
					#übertrage daten an die Platform
					##$PLATFORM[ $kP ] = new $D['ACCOUNT']['D'][ $kA ]['PLATFORM']['D'][ $kP ]['CLASS_ID']( $kA, $kP );
					$D['UPDATE'] = 1;#ToDo: automatisch erkennen / mit dem Cronjob soll generell nur aktuallisiert werden!
					$PLATFORM[ $kP ]->set_article($D);
					
					foreach((array)$vP['TO']['PLATFORM']['D'] AS $kPLA => $PLA) #Eigentlich gilt laut To->Type nur f�r Order, kann aber erstmal f�r Artikel interpretiert werden
					{
						if($PLA['ACTIVE'])
						{
							###$PLATFORM[ $kPLA ] = new $vP['CLASS_ID']( $kA, $kPLA );#Wird oben instanzieiert
							$PLATFORM[ $kPLA ]->set_article($D);
							###$PLATFORM[ $kPLA ]->set_log($D);#Speichert Errors falls die Platform übergeben hat
						}
					}
				}
				
				echo "set_article: Artikel wurden übertragen<br>";
				$SET[$kP]['set_article_LAST_EXCEUTION_TIME']['VALUE'] = time();
			}
			
			#==================================================================================================
			#Update Order
			if(in_array('get_order',get_class_methods($PLATFORM[ $kP ]))
				&& $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_ACTIVE']['VALUE'] == 'ON'
				&& time() > $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_EXECUTION_INTERVAL']['VALUE'] + $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_LAST_EXCEUTION_TIME']['VALUE']
			)
			{
				#ToDo: test: Next Time vor dem ausfüren setzen. Bei Fatal Error wird einfach die Zeit abgewartet.
				$SET[$kP]['get_order_LAST_EXCEUTION_TIME']['VALUE'] = time();
				$d['PLATFORM']['D'][ $kP ]['SETTING']['D'] = $SET[$kP];
				$PLATFORM[ $kP ]->set_setting($d);

				#Order automatisch in Rechnung umwandeln. ============
				$D['ORDER']['W']['STATUS:IN'] = "'20','40'"; #Status = fertig
				$D['ORDER']['W']['ID:NOT_IN'] = "SELECT id FROM wp_invoice WHERE platform_id = '{$kP}' AND account_id = '{$kA}' UNION SELECT id FROM wp_delivery WHERE platform_id = '{$kP}' AND account_id = '{$kA}'"; #gib alle aus die nicht in invoice vorhanden sind! IST Sehr Wichtig um Doppelte rechnungen zu vermeiden!
				#$D['ORDER']['W']['DATE:>'] = 20171123000000;#Alle Rechnungen ab 2017.11.23 fr Amazon FBA erstellen
				
				$PLATFORM[ $kP ]->get_order($D);#Bestellungen Import von Platform
				$CONTROL[ $kP ]->get_order($D);#Bestellung für die Ziel Platform ausgeben
				
				foreach((array)$D['PLATFORM']['D'][$kP]['ORDER']['D'] AS $kORD => $ORD)
				{
					$D['PLATFORM']['D'][$kP]['ORDER']['D'][$kORD]['DATE'] = date('Y-m-d'); #Rechnungsdatum setzt auf den import Tag
					
					$INV_ID = ($ORD['PAYMENT_ID'] == 'BP')?$ORD['CUSTOMER_ID'].'_'.date('Ym'):$kORD;
					$D['PLATFORM']['D'][$kP]['INVOICE']['D'][ $INV_ID ] = $D['PLATFORM']['D'][$kP]['ORDER']['D'][ $kORD ];
					if($ORD['PAYMENT_ID'] == 'BP')
					{
						$D['PLATFORM']['D'][$kP]['INVOICE']['D'][ $INV_ID ]['DATE'] = date('Y-m-t');
						$D['PLATFORM']['D'][$kP]['INVOICE']['D'][ $INV_ID ]['NUMBER'] = 'NULL';
						$D['PLATFORM']['D'][$kP]['ORDER']['D'][$kORD]['INVOICE_ID'] = $INV_ID;
					}

					if($ORD['PAYMENT_ID'] == 'AMAZON' && in_array($ORD['DELIVERY']['COUNTRY_ID'],['BE','BG','DK','DE','EE','FI','FR','GR','IE','IT','HR','LV','LT','LU','MT','NL','AT','PL','PT','RO','SE','SK','SI','ES','CZ','HU','GB','CY']))
					{
						$D['PLATFORM']['D'][$kP]['INVOICE']['D'][ $INV_ID ]['SEND_INVOICE'] = 1;
					}

				}
				
				if($D['PLATFORM']['D'][$kP]['ORDER']['D'])
				{
					#$D['PLATFORM']['D'][$kP]['INVOICE'] = $D['PLATFORM']['D'][$kP]['ORDER'];
					if(in_array('set_invoice',get_class_methods($PLATFORM[ $kP ])))
						$PLATFORM[ $kP ]->set_invoice($D);
					if(in_array('set_delivery',get_class_methods($PLATFORM[ $kP ])))
					{
						foreach((array)$D['PLATFORM']['D'][$kP]['ORDER']['D'] AS $kORD => $ORD)
						{
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD] = [
								'CUSTOMER_ID'			=> $ORD['CUSTOMER_ID'],
								'INVOICE_ID'			=> ($ORD['INVOICE_ID'])?$ORD['INVOICE_ID']:$kORD,
								'ORDER_ID'				=> $kORD,
								'WAREHOUSE_ID'			=> $ORD['WAREHOUSE_ID'],
								'SHIPPING_ID'			=> $ORD['SHIPPING_ID'],
								'ACTIVE'				=> $ORD['ACTIVE'],
								'STATUS'				=> $ORD['STATUS'],
								'DELIVERY_COMPANY'		=> $ORD['DELIVERY']['COMPANY'],
								'DELIVERY_FNAME'		=> $ORD['DELIVERY']['FNAME'],
								'DELIVERY_NAME'			=> $ORD['DELIVERY']['NAME'],
								'DELIVERY_PHONE'		=> $ORD['DELIVERY']['PHONE'],
								'DELIVERY_EMAIL'		=> $ORD['CUSTOMER_EMAIL'],
								'DELIVERY_STREET'		=> $ORD['DELIVERY']['STREET'],
								'DELIVERY_STREET_NO'	=> $ORD['DELIVERY']['STREET_NO'],
								'DELIVERY_ADDITION'		=> $ORD['DELIVERY']['ADDITION'],
								'DELIVERY_ZIP'			=> $ORD['DELIVERY']['ZIP'],
								'DELIVERY_CITY'			=> $ORD['DELIVERY']['CITY'],
								'DELIVERY_COUNTRY_ID'	=> $ORD['DELIVERY']['COUNTRY_ID'],
								'COMMENT'				=> $ORD['COMMENT'],
							];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['ARTICLE'] = $ORD['ARTICLE'];
						}

						if($ORD['CUSTOMER_ID'] == 'pxx')#ToDo: Hotfix
						{
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_COMPANY'] = $ORD['BILLING']['COMPANY'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_FNAME'] = $ORD['BILLING']['FNAME'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_NAME'] = $ORD['BILLING']['NAME'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_STREET'] = $ORD['BILLING']['STREET'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_STREET_NO'] = $ORD['BILLING']['STREET_NO'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_ZIP'] = $ORD['BILLING']['ZIP'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_CITY'] = $ORD['BILLING']['CITY'];
							$D['PLATFORM']['D'][$kP]['DELIVERY']['D'][$kORD]['RETURN_COUNTRY_ID'] = $ORD['BILLING']['COUNTRY_ID'];
						}
						
						$PLATFORM[ $kP ]->set_delivery($D);
					}
				}
				unset($D['PLATFORM']['D'][$kP]['ORDER']['D'][ $kORD ]);#ToDo: Hotfox: Aus irgend einen grund wird eine Order von PF: dropshipper mit anderen PF angelegt unter wp_order und wp_order_article. Vermuttung weil dropshipper nicht mit haupt Platform für Article verbunden ist?!
				echo "get_order: Bestellungen wurden abgeholt<br>";
				$SET[$kP]['get_order_LAST_EXCEUTION_TIME']['VALUE'] = time();
			}
			
			#ToDo: Update Order Trecking: Diese Variante soll die funktionalität aus cronjob_order.php übernehmen
			/*
			if(in_array('set_order',get_class_methods($PLATFORM[ $kP ])))
			{
				$D['ORDER']['L'] = ['START' => 0, 'STEP' => 10];#Die letzten 10 ausgeben
				$D['ORDER']['O']['ITIMESTAMP'] = 'DESC';
				$CONTROL[ $kP ]->get_order($D);
				$PLATFORM[ $kP ]->set_order($D);
			}
			*/
			
			#Update Message
			if(in_array('get_message',get_class_methods($PLATFORM[ $kP ]))
				&& $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_ACTIVE']['VALUE'] == 'ON'
				&& time() > $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_EXECUTION_INTERVAL']['VALUE'] + $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_LAST_EXCEUTION_TIME']['VALUE']
			)
			{
				$PLATFORM[ $kP ]->get_message($D);
				$CONTROL[ $kP ]->set_message($D);
				echo "get_massage: Nachrichten wurden abgeholt<br>";
			}
			
			#Update customer
			if(in_array('get_customer',get_class_methods($PLATFORM[ $kP ]))
				&& $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_ACTIVE']['VALUE'] == 'ON'
				&& time() > $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_EXECUTION_INTERVAL']['VALUE'] + $D['PLATFORM']['D'][ $kP ]['SETTING']['D']['get_order_LAST_EXCEUTION_TIME']['VALUE']
			)
			{
				$PLATFORM[ $kP ]->get_customer($D);
				$CONTROL[ $kP ]->set_customer($D);
				echo "set_customer: Kunden Daten wurden abgeholt<br>";
			}

			$D['PLATFORM']['D'][ $kP ]['SETTING']['D'] = $SET[$kP];
			$PLATFORM[ $kP ]->set_setting($D);
			echo "</div>";
		}
	}
}